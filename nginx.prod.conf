server {
    listen 80 default_server;
    listen [::]:80 default_server;

    server_name _;

    root /var/www/wp.sitechecker.pro/prod/;
    index index.php;

    location /exporter/export/download {
          proxy_pass http://crawler.sitechecker.pro/crawl/export/download;
    }

    location /exporter {
        proxy_pass http://crawler.sitechecker.pro/crawl/exporter;
        proxy_http_version 1.1;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection "Upgrade";
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    }

    # STATIC
    location ~ ^/assets {
        expires 168h;
        access_log off;
        add_header Pragma public;
        add_header Cache-Control "public, must-revalidate, proxy-revalidate";
        break;
    }
    location ~ ^/bundles {
        root /var/www/sitechecker.pro/prod/web;
        expires 168h;
        access_log off;
        add_header Pragma public;
        add_header Cache-Control "public, must-revalidate, proxy-revalidate";
        break;
    }

    # CACHE
    location ~* \.(jpg|jpeg|gif|png|svg|ico|css|js)$ {
        access_log off;
        add_header Pragma public;
        add_header Cache-Control "public, must-revalidate, proxy-revalidate";
        expires 168h;
        break;
    }

    # SYMFONY
    location ~ ^/(api|opcadm) {
        rewrite ^/(.*)$ /prod/web/$1 last;
    }
    location ^~ /prod/web {
        root /var/www/sitechecker.pro/;
        try_files $uri $uri/ /prod/web/app.php$is_args$args;
        location ~* \.php(/|$) {
            fastcgi_pass unix:/run/php/php7.1-fpm.sock;
            fastcgi_split_path_info ^(.+\.php)(/.*)$;
            include fastcgi_params;
            fastcgi_param SYMFONY_ENV main;
            fastcgi_param SYMFONY_DEBUG 0;
            fastcgi_param HTTPS on;
            fastcgi_param DOMAIN sitechecker.pro;
            fastcgi_param SCRIPT_FILENAME $realpath_root$fastcgi_script_name;
            fastcgi_param DOCUMENT_ROOT $realpath_root;
            internal;
        }
    }

    # WORDPRESS§
    location / {
        try_files $uri $uri/ /index.php?$args;
    }
    location ~ \.php$ {
        fastcgi_pass            unix:/run/php/php7.1-fpm.sock;
        fastcgi_index           index.php;
        include                 fastcgi_params;
        fastcgi_param           SCRIPT_FILENAME $request_filename;
        fastcgi_ignore_client_abort off;
        fastcgi_read_timeout    300;

        error_log /var/log/nginx/sitechecker.pro/wp-error.log;
        access_log /var/log/nginx/sitechecker.pro/wp-access.log;
    }

}

server {
    listen 80;
    listen [::]:80;

    server_name api1.sitechecker.pro api2.sitechecker.pro api3.sitechecker.pro images.sitechecker.pro;
    root /var/www/sitechecker.pro/prod/web;

    rewrite ^/(app|app_dev)\.php/?(.*)$ /$1 permanent;

    location / {
        index       app.php;
        try_files   $uri @rewriteapp;
    }

    location @rewriteapp {
        rewrite     ^(.*)$ /app.php/$1 last;
    }

    location ~ ^/(app|app_dev)\.php(/|$) {
        fastcgi_split_path_info ^(.+\.php)(/.+)$;
        fastcgi_pass unix:/run/php/php7.1-fpm.sock;
        fastcgi_param SYMFONY_ENV api_main;
        fastcgi_param SYMFONY_DEBUG 0;
        fastcgi_param HTTPS on;
        include fastcgi_params;
        fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
        fastcgi_intercept_errors off;
        internal;
    }

    error_log /var/log/nginx/sitechecker.pro/api-error.log;
    access_log /var/log/nginx/sitechecker.pro/api-access.log;
}