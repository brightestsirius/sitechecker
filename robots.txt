User-agent: *

#standart disallows
Disallow: /cgi-bin
Disallow: /wp-admin
Disallow: /wp-includes
Disallow: /wp-content/plugins
Disallow: /wp-content/cache
Disallow: /wp-content/themes
Disallow: /trackback
Disallow: */trackback
Disallow: */*/trackback
Disallow: /feed
Disallow: */feed
Disallow: /comments
Disallow: */comment-*
Disallow: /wp-login.php
Disallow: */comments
Disallow: /tag
Disallow: /*?*
Disallow: /*?

# Allow js and css
Allow: */uploads
Allow: /*/*.js
Allow: /*/*.css
Allow: /wp-*.png
Allow: /wp-*.jpg
Allow: /wp-*.jpeg
Allow: /wp-*.gif
Allow: /wp-*.svg

#disallow Analyze links
Disallow: */seo-report/*
Disallow: */terms-and-conditions/
Disallow: */privacy-policy/
Disallow: */lostpassword/
Disallow: */crawl-report-domain/*
Disallow: */crawl-report/

Allow: /wp-content/themes/sitechecker/out/*.css
Allow: /wp-content/themes/sitechecker/out/*.js
Allow: /wp-content/themes/sitechecker/out/*.png
Allow: /wp-content/themes/sitechecker/out/*.svg
Allow: /wp-content/themes/sitechecker/stylesheets/*.css

Disallow: /login/
Disallow: /404/
Disallow: /checkout/
Disallow: /register/
Disallow: /lostpassword/
Disallow: /resetpass/
Disallow: /logout/
Disallow: /account/