<?php
/**
 * Plugin Name: Smart Logout
 * Description: This plugin makes the logout function check to see if you are really logged in
 *   before issuing a logout command. This avoids the error that gets issued if you are already
 *   logged out (i.e. in another window or tab).
 * Author: Camden Ross
 * Version: 0.1
 *
 */

function smart_logout() {
    if (!is_user_logged_in()) {
        $smart_redirect_to = !empty( $_REQUEST['redirect_to'] ) ? $_REQUEST['redirect_to'] : '/';
        wp_safe_redirect( $smart_redirect_to );
        exit();
    } else {
        check_admin_referer('log-out');
        wp_logout();
        $smart_redirect_to = !empty( $_REQUEST['redirect_to'] ) ? $_REQUEST['redirect_to'] : '/';
        wp_safe_redirect( $smart_redirect_to );
        exit();
    }
}
add_action ( 'login_form_logout' , 'smart_logout' );

/* End of Plugin. Force uncomment for closing php tag. */
?>