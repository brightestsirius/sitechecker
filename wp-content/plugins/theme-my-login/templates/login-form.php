<?php
$meta = get_post_meta_all(get_option('page_on_front'));
$template_directory_uri = get_template_directory_uri();
$metaDesign = get_post_meta_all(url_to_postid( '/header-footer-design/' ));
?>
<p class="title"><?=t($metaDesign['sign_in']);?></p>
<div class="login__social">
    <?php echo do_shortcode('[TheChamp-Login]') ?>
</div>
<div class="login__popup-or">
    <span><?=t($metaDesign['or_email']);?></span>
</div>
<form name="loginform" id="loginform<?php $template->the_instance(); ?>" action="/?instance=1" method="post" class="form__signin">
    <div class="form__input">
        <svg width="13" height="16" viewBox="0 0 13 16" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M3.14346 7.82505C4.87942 7.82505 6.28683 6.07337 6.28683 3.91255C6.28683 1.75168 5.82476 0 3.14346 0C0.462162 0 1.63368e-07 1.75168 1.63368e-07 3.91255C1.63368e-07 6.07337 1.40741 7.82505 3.14346 7.82505Z" transform="translate(3.75278)" fill="#E6E6E6"/>
            <path d="M0.000624617 0.0677071C8.36982e-05 -0.0640415 -0.00045722 0.0305871 0.000624617 0.0677071V0.0677071Z" transform="translate(0.958618 13.7313)" fill="#E6E6E6"/>
            <path d="M-3.85175e-07 0.119676C0.00171252 0.0836072 0.00058561 -0.130564 -3.85175e-07 0.119676V0.119676Z" transform="translate(12.8336 13.7822)" fill="#E6E6E6"/>
            <path d="M11.8675 5.51575C11.8093 1.75154 11.3296 0.678948 7.65846 2.51116e-07C7.65846 2.51116e-07 7.14169 0.674789 5.93722 0.674789C4.73274 0.674789 4.21589 2.51116e-07 4.21589 2.51116e-07C0.58484 0.671543 0.0759266 1.72823 0.00901129 5.39346C0.00352423 5.69275 0.000981467 5.70848 4.22033e-08 5.67373C0.000223093 5.73883 0.000490655 5.85924 0.000490655 6.06921C0.000490655 6.06921 0.874494 7.87474 5.93722 7.87474C10.9999 7.87474 11.8739 6.06921 11.8739 6.06921C11.8739 5.9343 11.874 5.8405 11.8742 5.77668C11.8732 5.79817 11.8712 5.75652 11.8675 5.51575Z" transform="translate(0.959267 8.12527)" fill="#E6E6E6"/>
        </svg>
        <div class="label">
            <span><?=t($metaDesign['your_email']);?></span>
            <input type="text" name="log" class="email" value="<?php $template->the_posted_value('log'); ?>">
        </div>
        <p class="login_popup-error error_emailEmpty"><?php echo t($meta['error_emailempty'])?></p>
        <p class="login_popup-error error_emailIncorrect"><?php echo t($meta['error_emailincorrect'])?></p>
        <p class="login_popup-error error_emailNotExist"><?php echo t($meta['error_emailnotexist'])?></p>
    </div>
    <div class="form__input">
        <svg width="13" height="16" viewBox="0 0 13 16" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M11.546 6H10.5563V4.66666C10.5563 2.09344 8.48456 0 5.93793 0C3.3913 0 1.31955 2.09344 1.31955 4.66666V6H0.329895C0.147551 6 0 6.14909 0 6.33334V14.6667C0 15.402 0.591783 16 1.31955 16H10.5563C11.2841 16 11.8759 15.402 11.8759 14.6667V6.33334C11.8759 6.14909 11.7283 6 11.546 6ZM6.92567 12.9632C6.93597 13.0573 6.90603 13.1517 6.84353 13.2223C6.78102 13.293 6.69146 13.3333 6.59772 13.3333H5.27817C5.18443 13.3333 5.09487 13.293 5.03237 13.2223C4.96986 13.1517 4.93989 13.0573 4.95022 12.9632L5.15833 11.0723C4.82039 10.8239 4.61841 10.431 4.61841 10C4.61841 9.26466 5.21019 8.66666 5.93796 8.66666C6.66573 8.66666 7.25751 9.26462 7.25751 10C7.25751 10.431 7.05553 10.8239 6.71759 11.0723L6.92567 12.9632ZM8.577 6H3.29886V4.66666C3.29886 3.19628 4.48277 2 5.93793 2C7.3931 2 8.577 3.19628 8.577 4.66666V6Z" transform="translate(12.8345) scale(-1 1)" fill="#E6E6E6"/>
        </svg>
        <div class="label">
            <span><?=t($metaDesign['password']);?></span>
            <input type="password" name="pwd" value="" autocomplete="off" class="pass">
        </div>
        <p class="login_popup-error error_passNotExist"><?php echo t($meta['error_passnotexist'])?></p>
    </div>

    <?php do_action('login_form'); ?>

    <div class="tml-rememberme-submit-wrap">
        <p class="tml-submit-wrap">
            <input type="submit" name="wp-submit" class="popup__login-btn btn"
                   value="<?=t($metaDesign['sign_in']);?>"/>
            <span class="loader"></span>
            <?php
            $loginid = url_to_postid('login');
            $pageID = get_the_ID();

            $protocol = stripos($_SERVER['SERVER_PROTOCOL'],'https') === true ? 'https://' : 'http://';
            $host = $_SERVER['HTTP_HOST'];

            $base_url = $protocol.$host;
            $actual_link = $base_url."$_SERVER[REQUEST_URI]";
            $actual_link = str_replace("?checkemail=confirm","",$actual_link);
            $actual_link = str_replace("?resetpass=complete","",$actual_link);

            if (is_page($loginid)) {
                $redirect = '/';
            } else if($pageID == 709) {
                $redirect =  $actual_link;
            } else if (isset($_GET['redirect_to'])|| isset($_POST['redirect_to'])){
                $redirect = "tool/";
                // если в параметрах запроса есть параметр redirect_to ставим его в случае если он ссылкается на домен проекта или начинается со слеша
                $redirectToUrl = ($_GET['redirect_to']) ? $_GET['redirect_to']: $_POST['redirect_to'];
                $wpSiteUrlHost = parse_url(WP_SITEURL, PHP_URL_HOST);
                if($redirectToSiteUrlHost = parse_url($redirectToUrl, PHP_URL_HOST) AND (strpos($redirectToSiteUrlHost, $wpSiteUrlHost) !== false)){
                    $redirect = $redirectToUrl;
                }else{
                    $isOwnDomainUrl = strpos($redirectToUrl, WP_SITEURL) === 0;
                    $isStartsWithSlash = substr($redirectToUrl, 0, 1) === '/';
                    if($isOwnDomainUrl || $isStartsWithSlash){
                        $redirect = $redirectToUrl;
                    }
                }
            } else{
                $redirect =  $actual_link;
            }
            ?>
            <input type="hidden" name="redirect_to" value="<?php echo $redirect; ?>" />
            <input type="hidden" name="instance" value="<?php $template->the_instance(); ?>"/>
            <input type="hidden" name="action" value="login"/>
        </p>
    </div>
</form>