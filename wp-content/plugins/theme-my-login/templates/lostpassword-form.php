<?php
$meta = get_post_meta_all(get_option('page_on_front'));
$template_directory_uri = get_template_directory_uri();
$metaDesign = get_post_meta_all(url_to_postid( '/header-footer-design/' ));
?>
<p class="title"><?=t($metaDesign['reset_password']);?></p>
<p class="description"><?=t($metaDesign['reset_password_description']);?></p>
<form name="lostpasswordform" id="lostpasswordform<?php $template->the_instance(); ?>" action="/?instance=3" method="post" class="form__lostpass">
    <div class="form__input">
        <svg width="13" height="16" viewBox="0 0 13 16" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M3.14346 7.82505C4.87942 7.82505 6.28683 6.07337 6.28683 3.91255C6.28683 1.75168 5.82476 0 3.14346 0C0.462162 0 1.63368e-07 1.75168 1.63368e-07 3.91255C1.63368e-07 6.07337 1.40741 7.82505 3.14346 7.82505Z" transform="translate(3.75278)" fill="#E6E6E6"/>
            <path d="M0.000624617 0.0677071C8.36982e-05 -0.0640415 -0.00045722 0.0305871 0.000624617 0.0677071V0.0677071Z" transform="translate(0.958618 13.7313)" fill="#E6E6E6"/>
            <path d="M-3.85175e-07 0.119676C0.00171252 0.0836072 0.00058561 -0.130564 -3.85175e-07 0.119676V0.119676Z" transform="translate(12.8336 13.7822)" fill="#E6E6E6"/>
            <path d="M11.8675 5.51575C11.8093 1.75154 11.3296 0.678948 7.65846 2.51116e-07C7.65846 2.51116e-07 7.14169 0.674789 5.93722 0.674789C4.73274 0.674789 4.21589 2.51116e-07 4.21589 2.51116e-07C0.58484 0.671543 0.0759266 1.72823 0.00901129 5.39346C0.00352423 5.69275 0.000981467 5.70848 4.22033e-08 5.67373C0.000223093 5.73883 0.000490655 5.85924 0.000490655 6.06921C0.000490655 6.06921 0.874494 7.87474 5.93722 7.87474C10.9999 7.87474 11.8739 6.06921 11.8739 6.06921C11.8739 5.9343 11.874 5.8405 11.8742 5.77668C11.8732 5.79817 11.8712 5.75652 11.8675 5.51575Z" transform="translate(0.959267 8.12527)" fill="#E6E6E6"/>
        </svg>
        <div class="label">
            <span><?=t($metaDesign['your_email']);?></span>
            <input type="email" name="user_login" value="<?php $template->the_posted_value( 'user_login' ); ?>" class="email">
        </div>
        <p class="login_popup-error error_emailEmpty"><?php echo t($meta['error_emailempty'])?></p>
        <p class="login_popup-error error_emailIncorrect"><?php echo t($meta['error_emailincorrect'])?></p>
        <p class="login_popup-error error_emailNotExist"><?php echo t($meta['error_emailnotexist'])?></p>
    </div>
    <?php
    do_action( 'lostpassword_form' );
    if(get_the_ID() == 827){
        $redirectTo = '/account/?checkemail=confirm';
        $className = 'lostAccount';
    } else{
        $redirectTo = '?checkemail=confirm';
        $className = 'lost';
    }
    ?>
    <p class="tml-submit-wrap">
        <input class="btn" type="submit" name="wp-submit" value="<?=t($metaDesign['send_new_password']);?>" />
        <span class="loader"></span>
        <input type="hidden" name="redirect_to" value="<?php echo $redirectTo; ?>" />
        <input type="hidden" name="instance" value="<?php $template->the_instance(); ?>" />
        <input type="hidden" name="action" value="lostpassword" />
    </p>
</form>