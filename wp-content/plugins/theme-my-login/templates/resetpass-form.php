<?php
    $template_directory_uri = get_template_directory_uri();
    $metaDesign = get_post_meta_all(url_to_postid( '/header-footer-design/' ));
?>
<div class="newpass-block" id="theme-my-login<?php $template->the_instance(); ?>">
    <p class="title">Enter your new password below</p>
    <form class="newpass-form" name="resetpassform" id="resetpassform<?php $template->the_instance(); ?>" action="<?php $template->the_action_url( 'resetpass', 'login_post' ); ?>" method="post" autocomplete="off">
        <div class="form__input">
            <div class="error">
                <p>Password is incorrect</p>
                <img src="<?=$template_directory_uri; ?>/out/img_design/error_close.svg" alt="error" title="error">
            </div>
            <svg width="13" height="16" viewBox="0 0 13 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M11.546 6H10.5563V4.66666C10.5563 2.09344 8.48456 0 5.93793 0C3.3913 0 1.31955 2.09344 1.31955 4.66666V6H0.329895C0.147551 6 0 6.14909 0 6.33334V14.6667C0 15.402 0.591783 16 1.31955 16H10.5563C11.2841 16 11.8759 15.402 11.8759 14.6667V6.33334C11.8759 6.14909 11.7283 6 11.546 6ZM6.92567 12.9632C6.93597 13.0573 6.90603 13.1517 6.84353 13.2223C6.78102 13.293 6.69146 13.3333 6.59772 13.3333H5.27817C5.18443 13.3333 5.09487 13.293 5.03237 13.2223C4.96986 13.1517 4.93989 13.0573 4.95022 12.9632L5.15833 11.0723C4.82039 10.8239 4.61841 10.431 4.61841 10C4.61841 9.26466 5.21019 8.66666 5.93796 8.66666C6.66573 8.66666 7.25751 9.26462 7.25751 10C7.25751 10.431 7.05553 10.8239 6.71759 11.0723L6.92567 12.9632ZM8.577 6H3.29886V4.66666C3.29886 3.19628 4.48277 2 5.93793 2C7.3931 2 8.577 3.19628 8.577 4.66666V6Z" transform="translate(12.8345) scale(-1 1)" fill="#E6E6E6"/>
            </svg>
            <div class="label">
                <?php
                $userViewPosts = get_user_meta( get_current_user_id(), 'user_new_design', true );
                if($userViewPosts === "true"){
                    echo '<span>Password</span>';
                }else{
                    $placeholder = 'Password';
                }
                ?>
                <input type="password" data-reveal="1" data-pw="<?php echo esc_attr( wp_generate_password( 16 ) ); ?>" name="pass1" id="pass_resset" class="pass" size="20" value="" autocomplete="off" aria-describedby="pass-strength-result" placeholder="<?php echo $placeholder; ?>">
            </div>
        </div>

        <?php
        do_action( 'resetpassword_form' );
        ?>
        <div class="tml-submit-wrap">
            <input type="submit" name="wp-submit" id="wp-submit<?php $template->the_instance(); ?>" value="Reset password" class="btn" />
            <div class="loader"></div>
            <input type="hidden" id="user_login" value="<?php echo esc_attr( $GLOBALS['rp_login'] ); ?>" autocomplete="off" />
            <input type="hidden" name="rp_key" value="<?php echo esc_attr( $GLOBALS['rp_key'] ); ?>" />
            <input type="hidden" name="instance" value="<?php $template->the_instance(); ?>" />
            <input type="hidden" name="action" value="resetpass" />
        </div>
    </form>
</div>