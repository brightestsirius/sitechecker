<?php $frontpage_id = get_option( 'page_on_front' ); ?>
<main class="error_page">
    <div class="wrapper">
        <p class="error_page-title"><?php echo get_post_meta($frontpage_id, '404.title', true) ?></p>
        <img src="<?php echo get_template_directory_uri(); ?>/out/img/error.svg" alt="404" title="404">
        <p class="error_page-text"><?php echo get_post_meta($frontpage_id, '404.text', true) ?></p>
        <div class="home_first-search">
            <input id="sitechecker_input" type="text" placeholder="<?php echo get_post_meta($frontpage_id, 'main.input', true) ?>">
            <button id="sitechecker_check-landing" class="btn_gradient"><span><?php echo get_post_meta($frontpage_id, 'main.button', true) ?></span></button>
        </div>
    </div>
</main>