<?php
    $template_directory_uri = get_template_directory_uri();
?>
<main class="our_team-wrapper">
    <div class="wrapper">
        <h1><?php echo get_post_meta($post->ID, 'team.h1', true) ?></h1>
        <div class="our_team-text"><?php the_content(); ?></div>
        <div class="team_cards">
            <div class="team-card">
                <img src="<?php echo get_template_directory_uri(); ?>/out/img/anton-reva@2x.png" alt="anton-reva">
                <p><?php echo get_post_meta($post->ID, 'member1.name', true) ?></p>
                <p><?php echo get_post_meta($post->ID, 'member1.position', true) ?></p>
                <div class="team-card_social">
                    <a href="<?php echo get_post_meta($post->ID, 'member1.linkedin', true) ?>" target="_blank">
                        <svg xmlns="http://www.w3.org/2000/svg" width="34" height="34" viewBox="0 0 34 34">
                            <g fill="none" fill-rule="evenodd" transform="translate(1 1)">
                                <circle cx="16" cy="16" r="16" stroke="#0286BF"/>
                                <g fill="#0286BF" fill-rule="nonzero">
                                    <path d="M11.724 13.147H8.512a.258.258 0 0 0-.258.258v10.32c0 .142.116.257.258.257h3.212a.258.258 0 0 0 .258-.258V13.405a.258.258 0 0 0-.258-.258zM10.12 8.018A2.12 2.12 0 0 0 8 10.135c0 1.168.95 2.118 2.12 2.118a2.12 2.12 0 0 0 2.117-2.118 2.12 2.12 0 0 0-2.117-2.117zM19.894 12.89c-1.29 0-2.243.555-2.822 1.186v-.67a.258.258 0 0 0-.258-.259h-3.076a.258.258 0 0 0-.258.258v10.32c0 .142.116.257.258.257h3.205a.258.258 0 0 0 .258-.258V18.62c0-1.72.467-2.39 1.667-2.39 1.306 0 1.41 1.074 1.41 2.478v5.017c0 .143.115.258.258.258h3.206a.258.258 0 0 0 .258-.258v-5.66c0-2.558-.488-5.173-4.106-5.173z"/>
                                </g>
                            </g>
                        </svg>
                    </a>
                    <a href="<?php echo get_post_meta($post->ID, 'member1.facebook', true) ?>" target="_blank">
                        <svg xmlns="http://www.w3.org/2000/svg" width="34" height="34" viewBox="0 0 34 34">
                            <g fill="none" fill-rule="evenodd" transform="translate(1 1)">
                                <circle cx="16" cy="16" r="16" stroke="#0286BF"/>
                                <path fill="#0286BF" fill-rule="nonzero" d="M20.469 8.003L18.358 8c-2.371 0-3.904 1.546-3.904 3.938v1.815h-2.122a.33.33 0 0 0-.332.327v2.63c0 .18.149.326.332.326h2.122v6.638c0 .18.149.326.332.326h2.769a.33.33 0 0 0 .332-.326v-6.638h2.481a.329.329 0 0 0 .332-.326l.001-2.63a.324.324 0 0 0-.097-.231.335.335 0 0 0-.235-.096h-2.482v-1.539c0-.74.18-1.115 1.16-1.115h1.421a.33.33 0 0 0 .332-.327V8.33a.33.33 0 0 0-.331-.327z"/>
                            </g>
                        </svg>
                    </a>
                </div>
            </div>
            <div class="team-card">
                <img src="<?php echo get_template_directory_uri(); ?>/out/img/dima_bondar@2x.png" alt="dima_bondar">
                <p><?php echo get_post_meta($post->ID, 'member2.name', true) ?></p>
                <p><?php echo get_post_meta($post->ID, 'member2.position', true) ?></p>
                <div class="team-card_social">
                    <a href="<?php echo get_post_meta($post->ID, 'member2.linkedin', true) ?>" target="_blank">
                        <svg xmlns="http://www.w3.org/2000/svg" width="34" height="34" viewBox="0 0 34 34">
                            <g fill="none" fill-rule="evenodd" transform="translate(1 1)">
                                <circle cx="16" cy="16" r="16" stroke="#0286BF"/>
                                <g fill="#0286BF" fill-rule="nonzero">
                                    <path d="M11.724 13.147H8.512a.258.258 0 0 0-.258.258v10.32c0 .142.116.257.258.257h3.212a.258.258 0 0 0 .258-.258V13.405a.258.258 0 0 0-.258-.258zM10.12 8.018A2.12 2.12 0 0 0 8 10.135c0 1.168.95 2.118 2.12 2.118a2.12 2.12 0 0 0 2.117-2.118 2.12 2.12 0 0 0-2.117-2.117zM19.894 12.89c-1.29 0-2.243.555-2.822 1.186v-.67a.258.258 0 0 0-.258-.259h-3.076a.258.258 0 0 0-.258.258v10.32c0 .142.116.257.258.257h3.205a.258.258 0 0 0 .258-.258V18.62c0-1.72.467-2.39 1.667-2.39 1.306 0 1.41 1.074 1.41 2.478v5.017c0 .143.115.258.258.258h3.206a.258.258 0 0 0 .258-.258v-5.66c0-2.558-.488-5.173-4.106-5.173z"/>
                                </g>
                            </g>
                        </svg>
                    </a>
                    <a href="<?php echo get_post_meta($post->ID, 'member2.facebook', true) ?>" target="_blank">
                        <svg xmlns="http://www.w3.org/2000/svg" width="34" height="34" viewBox="0 0 34 34">
                            <g fill="none" fill-rule="evenodd" transform="translate(1 1)">
                                <circle cx="16" cy="16" r="16" stroke="#0286BF"/>
                                <path fill="#0286BF" fill-rule="nonzero" d="M20.469 8.003L18.358 8c-2.371 0-3.904 1.546-3.904 3.938v1.815h-2.122a.33.33 0 0 0-.332.327v2.63c0 .18.149.326.332.326h2.122v6.638c0 .18.149.326.332.326h2.769a.33.33 0 0 0 .332-.326v-6.638h2.481a.329.329 0 0 0 .332-.326l.001-2.63a.324.324 0 0 0-.097-.231.335.335 0 0 0-.235-.096h-2.482v-1.539c0-.74.18-1.115 1.16-1.115h1.421a.33.33 0 0 0 .332-.327V8.33a.33.33 0 0 0-.331-.327z"/>
                            </g>
                        </svg>
                    </a>
                </div>
            </div>
            <div class="team-card">
                <img src="<?php echo get_template_directory_uri(); ?>/out/img/alexei-prokashev@2x.png" alt="alexei-prokashev">
                <p><?php echo get_post_meta($post->ID, 'member3.name', true) ?></p>
                <p><?php echo get_post_meta($post->ID, 'member3.position', true) ?></p>
                <div class="team-card_social">
                    <a href="<?php echo get_post_meta($post->ID, 'member3.linkedin', true) ?>" target="_blank">
                        <svg xmlns="http://www.w3.org/2000/svg" width="34" height="34" viewBox="0 0 34 34">
                            <g fill="none" fill-rule="evenodd" transform="translate(1 1)">
                                <circle cx="16" cy="16" r="16" stroke="#0286BF"/>
                                <g fill="#0286BF" fill-rule="nonzero">
                                    <path d="M11.724 13.147H8.512a.258.258 0 0 0-.258.258v10.32c0 .142.116.257.258.257h3.212a.258.258 0 0 0 .258-.258V13.405a.258.258 0 0 0-.258-.258zM10.12 8.018A2.12 2.12 0 0 0 8 10.135c0 1.168.95 2.118 2.12 2.118a2.12 2.12 0 0 0 2.117-2.118 2.12 2.12 0 0 0-2.117-2.117zM19.894 12.89c-1.29 0-2.243.555-2.822 1.186v-.67a.258.258 0 0 0-.258-.259h-3.076a.258.258 0 0 0-.258.258v10.32c0 .142.116.257.258.257h3.205a.258.258 0 0 0 .258-.258V18.62c0-1.72.467-2.39 1.667-2.39 1.306 0 1.41 1.074 1.41 2.478v5.017c0 .143.115.258.258.258h3.206a.258.258 0 0 0 .258-.258v-5.66c0-2.558-.488-5.173-4.106-5.173z"/>
                                </g>
                            </g>
                        </svg>
                    </a>
                    <a href="<?php echo get_post_meta($post->ID, 'member3.facebook', true) ?>" target="_blank">
                        <svg xmlns="http://www.w3.org/2000/svg" width="34" height="34" viewBox="0 0 34 34">
                            <g fill="none" fill-rule="evenodd" transform="translate(1 1)">
                                <circle cx="16" cy="16" r="16" stroke="#0286BF"/>
                                <path fill="#0286BF" fill-rule="nonzero" d="M20.469 8.003L18.358 8c-2.371 0-3.904 1.546-3.904 3.938v1.815h-2.122a.33.33 0 0 0-.332.327v2.63c0 .18.149.326.332.326h2.122v6.638c0 .18.149.326.332.326h2.769a.33.33 0 0 0 .332-.326v-6.638h2.481a.329.329 0 0 0 .332-.326l.001-2.63a.324.324 0 0 0-.097-.231.335.335 0 0 0-.235-.096h-2.482v-1.539c0-.74.18-1.115 1.16-1.115h1.421a.33.33 0 0 0 .332-.327V8.33a.33.33 0 0 0-.331-.327z"/>
                            </g>
                        </svg>
                    </a>
                </div>
            </div>
            <div class="team-card">
                <img src="<?php echo get_template_directory_uri(); ?>/out/img/maxim-ponomarenko@2x.png" alt="maxim-ponomarenko">
                <p><?php echo get_post_meta($post->ID, 'member4.name', true) ?></p>
                <p><?php echo get_post_meta($post->ID, 'member4.position', true) ?></p>
                <div class="team-card_social">
                    <a href="<?php echo get_post_meta($post->ID, 'member4.linkedin', true) ?>" target="_blank">
                        <svg xmlns="http://www.w3.org/2000/svg" width="34" height="34" viewBox="0 0 34 34">
                            <g fill="none" fill-rule="evenodd" transform="translate(1 1)">
                                <circle cx="16" cy="16" r="16" stroke="#0286BF"/>
                                <g fill="#0286BF" fill-rule="nonzero">
                                    <path d="M11.724 13.147H8.512a.258.258 0 0 0-.258.258v10.32c0 .142.116.257.258.257h3.212a.258.258 0 0 0 .258-.258V13.405a.258.258 0 0 0-.258-.258zM10.12 8.018A2.12 2.12 0 0 0 8 10.135c0 1.168.95 2.118 2.12 2.118a2.12 2.12 0 0 0 2.117-2.118 2.12 2.12 0 0 0-2.117-2.117zM19.894 12.89c-1.29 0-2.243.555-2.822 1.186v-.67a.258.258 0 0 0-.258-.259h-3.076a.258.258 0 0 0-.258.258v10.32c0 .142.116.257.258.257h3.205a.258.258 0 0 0 .258-.258V18.62c0-1.72.467-2.39 1.667-2.39 1.306 0 1.41 1.074 1.41 2.478v5.017c0 .143.115.258.258.258h3.206a.258.258 0 0 0 .258-.258v-5.66c0-2.558-.488-5.173-4.106-5.173z"/>
                                </g>
                            </g>
                        </svg>
                    </a>
                    <a href="<?php echo get_post_meta($post->ID, 'member4.facebook', true) ?>" target="_blank">
                        <svg xmlns="http://www.w3.org/2000/svg" width="34" height="34" viewBox="0 0 34 34">
                            <g fill="none" fill-rule="evenodd" transform="translate(1 1)">
                                <circle cx="16" cy="16" r="16" stroke="#0286BF"/>
                                <path fill="#0286BF" fill-rule="nonzero" d="M20.469 8.003L18.358 8c-2.371 0-3.904 1.546-3.904 3.938v1.815h-2.122a.33.33 0 0 0-.332.327v2.63c0 .18.149.326.332.326h2.122v6.638c0 .18.149.326.332.326h2.769a.33.33 0 0 0 .332-.326v-6.638h2.481a.329.329 0 0 0 .332-.326l.001-2.63a.324.324 0 0 0-.097-.231.335.335 0 0 0-.235-.096h-2.482v-1.539c0-.74.18-1.115 1.16-1.115h1.421a.33.33 0 0 0 .332-.327V8.33a.33.33 0 0 0-.331-.327z"/>
                            </g>
                        </svg>
                    </a>
                </div>
            </div>
            <div class="team-card">
                <img src="<?php echo get_template_directory_uri(); ?>/out/img/ivan-palii@2x.png" alt="ivan-palii">
                <p><?php echo get_post_meta($post->ID, 'member5.name', true) ?></p>
                <p><?php echo get_post_meta($post->ID, 'member5.position', true) ?></p>
                <div class="team-card_social">
                    <a href="<?php echo get_post_meta($post->ID, 'member5.quora', true) ?>" class="quora" target="_blank">
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 478.148 478.148" xml:space="preserve" width="17px" height="17px">
							<g>
                                <path id="Quora" d="M350.715,398.749c60.627-37.895,100.873-105.894,100.873-183.277C451.568,96.45,356.354,0,239.084,0   C121.694,0,26.56,96.45,26.56,215.453c0,118.924,95.115,215.453,212.524,215.453c17.254,0,34.089-2.251,50.068-6.057   c20.103,36.759,50.885,62.62,116.194,50.108v-36.46C405.326,438.497,363.407,428.097,350.715,398.749z M349.281,243.964   c0,35.862-11.576,68.497-29.985,92.664c-23.57-24.765-60.129-45.266-106.571-42.577v4.284v37.914c0,0,31.519,1.335,52.997,38.293   c-7.989,2.331-16.417,3.686-25.044,3.686c-59.99,0-108.563-60.189-108.563-134.285c0-9.563,0-46.003,0-55.547   c0-74.215,48.574-134.404,108.563-134.404s108.563,60.189,108.563,134.404C349.281,197.96,349.281,234.4,349.281,243.964z" fill="#0286bf"/>
                            </g>
						</svg>
                    </a>
                    <a href="<?php echo get_post_meta($post->ID, 'member5.linkedin', true) ?>" target="_blank">
                        <svg xmlns="http://www.w3.org/2000/svg" width="34" height="34" viewBox="0 0 34 34">
                            <g fill="none" fill-rule="evenodd" transform="translate(1 1)">
                                <circle cx="16" cy="16" r="16" stroke="#0286BF"/>
                                <g fill="#0286BF" fill-rule="nonzero">
                                    <path d="M11.724 13.147H8.512a.258.258 0 0 0-.258.258v10.32c0 .142.116.257.258.257h3.212a.258.258 0 0 0 .258-.258V13.405a.258.258 0 0 0-.258-.258zM10.12 8.018A2.12 2.12 0 0 0 8 10.135c0 1.168.95 2.118 2.12 2.118a2.12 2.12 0 0 0 2.117-2.118 2.12 2.12 0 0 0-2.117-2.117zM19.894 12.89c-1.29 0-2.243.555-2.822 1.186v-.67a.258.258 0 0 0-.258-.259h-3.076a.258.258 0 0 0-.258.258v10.32c0 .142.116.257.258.257h3.205a.258.258 0 0 0 .258-.258V18.62c0-1.72.467-2.39 1.667-2.39 1.306 0 1.41 1.074 1.41 2.478v5.017c0 .143.115.258.258.258h3.206a.258.258 0 0 0 .258-.258v-5.66c0-2.558-.488-5.173-4.106-5.173z"/>
                                </g>
                            </g>
                        </svg>
                    </a>
                    <a href="<?php echo get_post_meta($post->ID, 'member5.facebook', true) ?>" target="_blank">
                        <svg xmlns="http://www.w3.org/2000/svg" width="34" height="34" viewBox="0 0 34 34">
                            <g fill="none" fill-rule="evenodd" transform="translate(1 1)">
                                <circle cx="16" cy="16" r="16" stroke="#0286BF"/>
                                <path fill="#0286BF" fill-rule="nonzero" d="M20.469 8.003L18.358 8c-2.371 0-3.904 1.546-3.904 3.938v1.815h-2.122a.33.33 0 0 0-.332.327v2.63c0 .18.149.326.332.326h2.122v6.638c0 .18.149.326.332.326h2.769a.33.33 0 0 0 .332-.326v-6.638h2.481a.329.329 0 0 0 .332-.326l.001-2.63a.324.324 0 0 0-.097-.231.335.335 0 0 0-.235-.096h-2.482v-1.539c0-.74.18-1.115 1.16-1.115h1.421a.33.33 0 0 0 .332-.327V8.33a.33.33 0 0 0-.331-.327z"/>
                            </g>
                        </svg>
                    </a>
                </div>
            </div>
            <div class="team-card">
                <img src="<?php echo get_template_directory_uri(); ?>/out/img/designer@2x.png" alt="designer">
                <p><?php echo get_post_meta($post->ID, 'member6.name', true) ?></p>
                <p><?php echo get_post_meta($post->ID, 'member6.position', true) ?></p>
                <div class="team-card_social">
                    <a href="<?php echo get_post_meta($post->ID, 'member6.linkedin', true) ?>" target="_blank">
                        <svg xmlns="http://www.w3.org/2000/svg" width="34" height="34" viewBox="0 0 34 34">
                            <g fill="none" fill-rule="evenodd" transform="translate(1 1)">
                                <circle cx="16" cy="16" r="16" stroke="#0286BF"/>
                                <g fill="#0286BF" fill-rule="nonzero">
                                    <path d="M11.724 13.147H8.512a.258.258 0 0 0-.258.258v10.32c0 .142.116.257.258.257h3.212a.258.258 0 0 0 .258-.258V13.405a.258.258 0 0 0-.258-.258zM10.12 8.018A2.12 2.12 0 0 0 8 10.135c0 1.168.95 2.118 2.12 2.118a2.12 2.12 0 0 0 2.117-2.118 2.12 2.12 0 0 0-2.117-2.117zM19.894 12.89c-1.29 0-2.243.555-2.822 1.186v-.67a.258.258 0 0 0-.258-.259h-3.076a.258.258 0 0 0-.258.258v10.32c0 .142.116.257.258.257h3.205a.258.258 0 0 0 .258-.258V18.62c0-1.72.467-2.39 1.667-2.39 1.306 0 1.41 1.074 1.41 2.478v5.017c0 .143.115.258.258.258h3.206a.258.258 0 0 0 .258-.258v-5.66c0-2.558-.488-5.173-4.106-5.173z"/>
                                </g>
                            </g>
                        </svg>
                    </a>
                    <a href="<?php echo get_post_meta($post->ID, 'member6.facebook', true) ?>" target="_blank">
                        <svg xmlns="http://www.w3.org/2000/svg" width="34" height="34" viewBox="0 0 34 34">
                            <g fill="none" fill-rule="evenodd" transform="translate(1 1)">
                                <circle cx="16" cy="16" r="16" stroke="#0286BF"/>
                                <path fill="#0286BF" fill-rule="nonzero" d="M20.469 8.003L18.358 8c-2.371 0-3.904 1.546-3.904 3.938v1.815h-2.122a.33.33 0 0 0-.332.327v2.63c0 .18.149.326.332.326h2.122v6.638c0 .18.149.326.332.326h2.769a.33.33 0 0 0 .332-.326v-6.638h2.481a.329.329 0 0 0 .332-.326l.001-2.63a.324.324 0 0 0-.097-.231.335.335 0 0 0-.235-.096h-2.482v-1.539c0-.74.18-1.115 1.16-1.115h1.421a.33.33 0 0 0 .332-.327V8.33a.33.33 0 0 0-.331-.327z"/>
                            </g>
                        </svg>
                    </a>
                </div>
            </div>
            <div class="team-card">
                <div class="team-card_img" style="background: url(<?php echo get_template_directory_uri(); ?>/out/img/Edward-Petrossi.jpg);background-size: cover; background-position: center center"></div>
                <p><?php echo get_post_meta($post->ID, 'member7.name', true) ?></p>
                <p><?php echo get_post_meta($post->ID, 'member7.position', true) ?></p>
                <div class="team-card_social">
                    <a href="<?php echo get_post_meta($post->ID, 'member7.linkedin', true) ?>" target="_blank">
                        <svg xmlns="http://www.w3.org/2000/svg" width="34" height="34" viewBox="0 0 34 34">
                            <g fill="none" fill-rule="evenodd" transform="translate(1 1)">
                                <circle cx="16" cy="16" r="16" stroke="#0286BF"/>
                                <g fill="#0286BF" fill-rule="nonzero">
                                    <path d="M11.724 13.147H8.512a.258.258 0 0 0-.258.258v10.32c0 .142.116.257.258.257h3.212a.258.258 0 0 0 .258-.258V13.405a.258.258 0 0 0-.258-.258zM10.12 8.018A2.12 2.12 0 0 0 8 10.135c0 1.168.95 2.118 2.12 2.118a2.12 2.12 0 0 0 2.117-2.118 2.12 2.12 0 0 0-2.117-2.117zM19.894 12.89c-1.29 0-2.243.555-2.822 1.186v-.67a.258.258 0 0 0-.258-.259h-3.076a.258.258 0 0 0-.258.258v10.32c0 .142.116.257.258.257h3.205a.258.258 0 0 0 .258-.258V18.62c0-1.72.467-2.39 1.667-2.39 1.306 0 1.41 1.074 1.41 2.478v5.017c0 .143.115.258.258.258h3.206a.258.258 0 0 0 .258-.258v-5.66c0-2.558-.488-5.173-4.106-5.173z"/>
                                </g>
                            </g>
                        </svg>
                    </a>
                    <a href="<?php echo get_post_meta($post->ID, 'member7.facebook', true) ?>" target="_blank">
                        <svg xmlns="http://www.w3.org/2000/svg" width="34" height="34" viewBox="0 0 34 34">
                            <g fill="none" fill-rule="evenodd" transform="translate(1 1)">
                                <circle cx="16" cy="16" r="16" stroke="#0286BF"/>
                                <path fill="#0286BF" fill-rule="nonzero" d="M20.469 8.003L18.358 8c-2.371 0-3.904 1.546-3.904 3.938v1.815h-2.122a.33.33 0 0 0-.332.327v2.63c0 .18.149.326.332.326h2.122v6.638c0 .18.149.326.332.326h2.769a.33.33 0 0 0 .332-.326v-6.638h2.481a.329.329 0 0 0 .332-.326l.001-2.63a.324.324 0 0 0-.097-.231.335.335 0 0 0-.235-.096h-2.482v-1.539c0-.74.18-1.115 1.16-1.115h1.421a.33.33 0 0 0 .332-.327V8.33a.33.33 0 0 0-.331-.327z"/>
                            </g>
                        </svg>
                    </a>
                </div>
            </div>
            <div class="team-card">
                <div class="team-card_img" style="background: url(https://scontent-waw1-1.cdninstagram.com/vp/d9a438bb2ae68d88765753e5c462402f/5C5B275B/t51.2885-15/sh0.08/e35/s750x750/28430578_584447681892275_5302604144475897856_n.jpg);background-size: cover; background-position: center center"></div>
                <p>Katherina</p>
                <p>Front-end developer</p>
                <div class="team-card_social">
                    <a href="https://www.linkedin.com/in/katherina-shypovskaya-8a246b103/" target="_blank">
                        <svg xmlns="http://www.w3.org/2000/svg" width="34" height="34" viewBox="0 0 34 34">
                            <g fill="none" fill-rule="evenodd" transform="translate(1 1)">
                                <circle cx="16" cy="16" r="16" stroke="#0286BF"/>
                                <g fill="#0286BF" fill-rule="nonzero">
                                    <path d="M11.724 13.147H8.512a.258.258 0 0 0-.258.258v10.32c0 .142.116.257.258.257h3.212a.258.258 0 0 0 .258-.258V13.405a.258.258 0 0 0-.258-.258zM10.12 8.018A2.12 2.12 0 0 0 8 10.135c0 1.168.95 2.118 2.12 2.118a2.12 2.12 0 0 0 2.117-2.118 2.12 2.12 0 0 0-2.117-2.117zM19.894 12.89c-1.29 0-2.243.555-2.822 1.186v-.67a.258.258 0 0 0-.258-.259h-3.076a.258.258 0 0 0-.258.258v10.32c0 .142.116.257.258.257h3.205a.258.258 0 0 0 .258-.258V18.62c0-1.72.467-2.39 1.667-2.39 1.306 0 1.41 1.074 1.41 2.478v5.017c0 .143.115.258.258.258h3.206a.258.258 0 0 0 .258-.258v-5.66c0-2.558-.488-5.173-4.106-5.173z"/>
                                </g>
                            </g>
                        </svg>
                    </a>
                    <a href="https://www.facebook.com/shypovskaya" target="_blank">
                        <svg xmlns="http://www.w3.org/2000/svg" width="34" height="34" viewBox="0 0 34 34">
                            <g fill="none" fill-rule="evenodd" transform="translate(1 1)">
                                <circle cx="16" cy="16" r="16" stroke="#0286BF"/>
                                <path fill="#0286BF" fill-rule="nonzero" d="M20.469 8.003L18.358 8c-2.371 0-3.904 1.546-3.904 3.938v1.815h-2.122a.33.33 0 0 0-.332.327v2.63c0 .18.149.326.332.326h2.122v6.638c0 .18.149.326.332.326h2.769a.33.33 0 0 0 .332-.326v-6.638h2.481a.329.329 0 0 0 .332-.326l.001-2.63a.324.324 0 0 0-.097-.231.335.335 0 0 0-.235-.096h-2.482v-1.539c0-.74.18-1.115 1.16-1.115h1.421a.33.33 0 0 0 .332-.327V8.33a.33.33 0 0 0-.331-.327z"/>
                            </g>
                        </svg>
                    </a>
                </div>
            </div>
            <div class="team-card">
                <div class="team-card_img" style="background: url(http://boosta.biz/wp-content/themes/html5blank-stable/img/image.png);background-size: 227px; background-position: 57% 6%;"></div>
                <p>Oleh</p>
                <p>QA Team Lead</p>
                <div class="team-card_social">
                    <a href="https://www.linkedin.com/in/%D0%BE%D0%BB%D0%B5%D0%B3-%D0%BA%D0%B0%D0%BB%D1%83%D0%B6%D0%B8%D0%BD-a354bb119/" target="_blank">
                        <svg xmlns="http://www.w3.org/2000/svg" width="34" height="34" viewBox="0 0 34 34">
                            <g fill="none" fill-rule="evenodd" transform="translate(1 1)">
                                <circle cx="16" cy="16" r="16" stroke="#0286BF"/>
                                <g fill="#0286BF" fill-rule="nonzero">
                                    <path d="M11.724 13.147H8.512a.258.258 0 0 0-.258.258v10.32c0 .142.116.257.258.257h3.212a.258.258 0 0 0 .258-.258V13.405a.258.258 0 0 0-.258-.258zM10.12 8.018A2.12 2.12 0 0 0 8 10.135c0 1.168.95 2.118 2.12 2.118a2.12 2.12 0 0 0 2.117-2.118 2.12 2.12 0 0 0-2.117-2.117zM19.894 12.89c-1.29 0-2.243.555-2.822 1.186v-.67a.258.258 0 0 0-.258-.259h-3.076a.258.258 0 0 0-.258.258v10.32c0 .142.116.257.258.257h3.205a.258.258 0 0 0 .258-.258V18.62c0-1.72.467-2.39 1.667-2.39 1.306 0 1.41 1.074 1.41 2.478v5.017c0 .143.115.258.258.258h3.206a.258.258 0 0 0 .258-.258v-5.66c0-2.558-.488-5.173-4.106-5.173z"/>
                                </g>
                            </g>
                        </svg>
                    </a>
                </div>
            </div>
        </div>
    </div>
</main>