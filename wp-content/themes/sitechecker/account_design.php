<?php
/*
	Template Name: Account Design
*/
get_header('tmp_design');
?>
    <main>
        <div class="main account__page">
            <?php if (have_posts()): while (have_posts()) : the_post(); ?>
                <?php the_content(); ?>
            <?php endwhile; ?>
            <?php endif; ?>
        </div>
    </main>
<?php get_footer('tmp_design'); ?>