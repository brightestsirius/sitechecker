<?php get_header(); ?>

<main>
	<div class="wrapper tags-page">
		<h3><?php _e( 'Archive: ', 'html5blank' ); echo single_tag_title('', false); ?></h3>
		<div class="blog_container">
			<div class="blog_container-posts">
				<?php get_template_part('loop'); ?>
				<?php get_template_part('pagination'); ?>
			</div>
			<?php get_sidebar(); ?>
		</div>
	</div>
</main>

<?php get_footer(); ?>