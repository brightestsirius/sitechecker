<?php
    $meta = get_post_meta_all(get_option('page_on_front'));
    $template_directory_uri = get_template_directory_uri();
?>
<main class="home knowledge">
    <div class="wrapper">
        <div class="pages_breadcrumbs" style="margin-bottom: 30px">
            <div class="wrapper">
                <a href="<?=home_url();?>/">Home</a>
                <span>/</span>
                <a href="/knowledge-base/"><?=t($meta['user.header.knowledge.base']);?></a>
            </div>
        </div>
        <h1 style="margin-top: 0;"><?php _e( 'Category: ', 'html5blank' ); echo single_tag_title('', false);?></h1>
        <?php
        $paged = get_query_var( 'paged' ) ? absint( get_query_var( 'paged' ) ) : 1;
        $args = array(
            'posts_per_page' => 12,
            'cat' => get_queried_object()->term_id,
            'post_type' => array('page','post'),
            'paged' => $paged,
        );
        $query = new WP_Query($args);
        ?>
        <?php if ( $query->have_posts() ) : ?>
            <div class="pages_more-wrap">
                <!-- begin loop -->
                <?php while ( $query->have_posts() ) : $query->the_post(); ?>
                    <div class="block">
                        <div class="block_img" style="background: url(<?php the_post_thumbnail_url(); ?>);background-size: cover;background-position: center center;"></div>
                        <a href="<?php the_permalink(); ?>"><?php echo get_post_meta( get_the_ID(), 'page.h1', true); ?></a>
                        <?php
                        $string = get_post_meta( get_the_ID(), 'page.about', true);
                        $maxLength = 200;
                        if (strlen($string) > $maxLength) {
                            $stringCut = substr($string, 0, $maxLength);
                            $string = substr($stringCut, 0, strrpos($stringCut, ' '));
                        }
                        ?>
                        <p><?php echo $string ?></p>
                    </div>
                <?php endwhile; ?>
                <!-- end loop -->
            </div>
            <div class="pagination">
                <?php
                echo paginate_links( array(
                    'base'         => str_replace( 999999999, '%#%', esc_url( get_pagenum_link( 999999999 ) ) ),
                    'total'        => $query->max_num_pages,
                    'current'      => max( 1, get_query_var( 'paged' ) ),
                    'format'       => '?paged=%#%',
                    'show_all'     => false,
                    'type'         => 'plain',
                    'end_size'     => 2,
                    'mid_size'     => 1,
                    'prev_next'    => true,
                    'prev_text'    => sprintf( '<i class="fa fa-chevron-left"></i> %1$s', __('') ),
                    'next_text'    => sprintf( '%1$s <i class="fa fa-chevron-right"></i>', __('') ),
                    'add_args'     => false,
                    'add_fragment' => '',
                ) );
                ?>
            </div>
        <?php else : ?>
            <p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
        <?php endif; wp_reset_query(); ?>
    </div>
</main>