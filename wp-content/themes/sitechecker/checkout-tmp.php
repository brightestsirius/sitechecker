<?php
$pmpro_levels = pmpro_getAllLevels();
$customerMembershipLevel = $current_user->membership_level->id;

$template_directory_uri = get_template_directory_uri();
$meta = get_post_meta_all(get_option('page_on_front'));

global $gateway, $pmpro_review, $skip_account_fields, $pmpro_paypal_token, $wpdb, $current_user, $pmpro_msg, $pmpro_msgt, $pmpro_requirebilling, $pmpro_level, $pmpro_levels, $tospage, $pmpro_currency_symbol, $pmpro_show_discount_code;
global $discount_code, $username, $password, $password2, $bfirstname, $blastname, $baddress1, $baddress2, $bcity, $bstate, $bzipcode, $bcountry, $bphone, $bemail, $bconfirmemail, $CardType, $AccountNumber, $ExpirationMonth,$ExpirationYear;
?>

<div class="paypal_redirect">
    <p class="title">Thank you for registration!</p>
    <p>We are redirecting you to PayPal</p>
    <div class="spinWrap">
        <p class="spinnerImage"></p>
        <p class="loader"></p>
    </div>
</div>
<main class="levels_main">
    <div class="wrapper">
        <div class="upgrade_plans pdf_banner">
            <p>PDF only for Beginner, Basic or Pro level</p>
            <img src="<?= $template_directory_uri; ?>/out/img/popup__close.svg" alt="close" title="close"
                 class="pdf_banner-close">
        </div>
        <div id="pricing_first">
            <h1 class="levels_title"><?php echo the_field('plans_title'); ?></h1>
            <p class="pricing__second-description"><?php echo the_field('plans_description'); ?></p>
            <div class="crawl_report-notification" id="level_checkout">
                <label class="switch">
                    <input type="checkbox">
                    <span class="slider round">
                        <span><?php echo the_field('monthly_data'); ?></span>
                        <span><?php echo the_field('yearly_data'); ?></span>
                    </span>
                </label>
            </div>
            <div class="levels_block">
                <div class="levels_block-side">
                    <div class="card">
                        <?php
                        foreach ($pmpro_levels as $level) { ?>
                            <?php if ($level->id == 8) {
                                $_REQUEST['level'] = $level->id;
                                require(ABSPATH . "/wp-content/plugins/paid-memberships-pro/preheaders/checkout.php");
                                ?>
                                <form class="pmpro_form monthly levels_block-package" action="" method="post">
                                    <p class="title"><?php echo the_field('beginner_level_title') ?></p>
                                    <span class="description"><?php echo the_field('beginner_level_recommend') ?></span>
                                    <p class="price">$<?php echo intval($level->initial_payment); ?>
                                        <span> <?php echo the_field('per_month') ?></span>
                                    </p>
                                    <div class="plan_tab">
                                        <svg width="14" height="22" viewBox="0 0 14 22" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M13.4194 11.0403L8.47127 8.03611L10.9658 1.76489C11.0428 1.60111 11.086 1.41656 11.086 1.22222C11.086 0.547556 10.5667 0 9.9227 0C9.63218 0.00488889 9.3895 0.101444 9.19699 0.261556C9.15848 0.293333 9.13515 0.312889 9.11531 0.334889L0.364705 9.01267C0.0975201 9.27789 -0.0343224 9.65922 0.00768055 10.0418C0.0496835 10.4243 0.263198 10.7641 0.580554 10.9584L5.5299 13.9651L3.00622 20.3084C2.79271 20.8413 2.96655 21.4573 3.41925 21.7849C3.61993 21.9291 3.85328 22 4.08196 22C4.37132 22 4.66067 21.8863 4.88469 21.6651L13.6353 12.9849C13.9025 12.7197 14.0343 12.3383 13.9923 11.9558C13.9503 11.5732 13.7368 11.2334 13.4194 11.0403Z"
                                                  fill="#CCCCCC"/>
                                        </svg>
                                        <p>Live Check</p>
                                    </div>
                                    <div class="plan_about">
                                        <ul>
                                            <?php echo the_field('beginner_level_about') ?>
                                        </ul>
                                    </div>
                                    <div class="plan_tab">
                                        <svg width="24" height="23" viewBox="0 0 24 23" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M10.2347 8.37864L8.70534 6.88655C9.1759 6.19789 9.41117 5.39446 9.41117 4.59103C9.41117 2.06596 7.29366 0 4.70559 0C2.11751 0 0 2.06596 0 4.59103C0 7.1161 2.11751 9.18207 4.70559 9.18207C5.52906 9.18207 6.35254 8.95251 7.05838 8.49341L8.5877 9.9855C8.82298 10.215 9.52881 10.6742 10.2347 9.9855C10.7052 9.52639 10.7052 8.83774 10.2347 8.37864ZM4.70559 6.88655C3.41155 6.88655 2.35279 5.85357 2.35279 4.59103C2.35279 3.3285 3.41155 2.29552 4.70559 2.29552C5.99962 2.29552 7.05838 3.3285 7.05838 4.59103C7.05838 5.85357 5.99962 6.88655 4.70559 6.88655Z"
                                                  transform="translate(12.9403 12.6254)" fill="#CCCCCC"/>
                                            <path d="M17.646 0H1.1764C0.470559 0 0 0.459103 0 1.14776V21.8074C0 22.4961 0.470559 22.9552 1.1764 22.9552H13.7638C11.8816 21.6926 10.5876 19.6267 10.5876 17.2164C10.5876 13.4288 13.7638 10.3298 17.646 10.3298C17.9989 10.3298 18.4694 10.3298 18.8223 10.4446V1.14776C18.8223 0.459103 18.3518 0 17.646 0ZM9.41117 9.18207H3.52919C2.82335 9.18207 2.35279 8.60819 2.35279 8.03431C2.35279 7.34565 2.82335 6.88655 3.52919 6.88655H9.41117C10.117 6.88655 10.5876 7.34565 10.5876 8.03431C10.5876 8.60819 10.117 9.18207 9.41117 9.18207ZM15.2932 4.59103H3.52919C2.82335 4.59103 2.35279 4.01715 2.35279 3.44327C2.35279 2.75462 2.82335 2.29552 3.52919 2.29552H15.2932C15.999 2.29552 16.4696 2.75462 16.4696 3.44327C16.4696 4.01715 15.999 4.59103 15.2932 4.59103Z"
                                                  fill="#CCCCCC"/>
                                        </svg>
                                        <p>Audit</p>
                                    </div>
                                    <div class="data">
                                        <ul>
                                            <?php echo the_field('beginner_level_data') ?>
                                        </ul>
                                    </div>
                                    <div class="plan_tab">
                                        <svg width="20" height="23" viewBox="0 0 20 23" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M9.96031 22.8655C11.2493 22.8655 12.3039 21.8365 12.3039 20.5789H7.61671C7.61671 21.8365 8.67133 22.8655 9.96031 22.8655ZM17.577 16.0058V9.71784C17.577 6.17368 15.1162 3.3155 11.718 2.5152V1.71491C11.718 0.800292 10.8978 0 9.96031 0C9.02287 0 8.20261 0.800292 8.20261 1.71491V2.5152C4.80439 3.3155 2.3436 6.17368 2.3436 9.71784V16.0058L0 18.2924V19.4357H19.9206V18.2924L17.577 16.0058Z"
                                                  fill="#CCCCCC"/>
                                        </svg>
                                        <p>Monitoring</p>
                                    </div>
                                    <div class="data">
                                        <ul>
                                            <?php echo the_field('beginner_level_monitoring') ?>
                                        </ul>
                                    </div>
                                    <div class="button">
                                        <input type="hidden" name="level_id" value="<?php echo $level->id?>" />
                                        <input type="hidden" name="submit-checkout" value="<?php echo $level->id?>" />
                                        <?php if($gateway == "paypalexpress" && $pmpro_requirebilling) { ?>
                                            <input type="submit" class="pmpro_btn pmpro_btn-submit-checkout <?php echo $customerMembershipLevel == $level->id ? 'choosed' : '' ?>" value="<?php echo $customerMembershipLevel == $level->id ? the_field('level_button_checked') : the_field('level_button') ?>" name="<?php echo $level->id?>">
                                        <?php } else { ?>
                                            <span id="pmpro_submit_span">
<input type="submit" class="pmpro_btn pmpro_btn-submit-checkout" value="Submit and <?php if($pmpro_requirebilling) { ?>Checkout<?php } else { ?>Confirm<?php } ?> &raquo;" />
</span>
                                        <?php } ?>
                                    </div>
                                </form>
                            <?php } ?>
                        <?php } ?>
                        <?php
                        foreach ($pmpro_levels as $level) { ?>
                            <?php if ($level->id == 9) {
                                $_REQUEST['level'] = $level->id;
                                require(ABSPATH . "/wp-content/plugins/paid-memberships-pro/preheaders/checkout.php");
                                ?>
                                <form class="pmpro yearly levels_block-package" action="" method="post">
                                    <p class="title"><?php echo the_field('beginner_level_title') ?></p>
                                    <span class="description"><?php echo the_field('beginner_level_recommend') ?></span>
                                    <p class="price">$7.3<span> <?php echo the_field('per_month') ?></span>
                                    </p>
                                    <div class="sale">
                                        <p><?php echo the_field('price_save-beginner') ?></p>
                                        <p><?php echo the_field('price_yearly-beginner') ?></p>
                                    </div>
                                    <div class="plan_tab">
                                        <svg width="14" height="22" viewBox="0 0 14 22" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M13.4194 11.0403L8.47127 8.03611L10.9658 1.76489C11.0428 1.60111 11.086 1.41656 11.086 1.22222C11.086 0.547556 10.5667 0 9.9227 0C9.63218 0.00488889 9.3895 0.101444 9.19699 0.261556C9.15848 0.293333 9.13515 0.312889 9.11531 0.334889L0.364705 9.01267C0.0975201 9.27789 -0.0343224 9.65922 0.00768055 10.0418C0.0496835 10.4243 0.263198 10.7641 0.580554 10.9584L5.5299 13.9651L3.00622 20.3084C2.79271 20.8413 2.96655 21.4573 3.41925 21.7849C3.61993 21.9291 3.85328 22 4.08196 22C4.37132 22 4.66067 21.8863 4.88469 21.6651L13.6353 12.9849C13.9025 12.7197 14.0343 12.3383 13.9923 11.9558C13.9503 11.5732 13.7368 11.2334 13.4194 11.0403Z"
                                                  fill="#CCCCCC"/>
                                        </svg>
                                        <p>Live Check</p>
                                    </div>
                                    <div class="plan_about">
                                        <ul>
                                            <?php echo the_field('beginner_level_about') ?>
                                        </ul>
                                    </div>
                                    <div class="plan_tab">
                                        <svg width="24" height="23" viewBox="0 0 24 23" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M10.2347 8.37864L8.70534 6.88655C9.1759 6.19789 9.41117 5.39446 9.41117 4.59103C9.41117 2.06596 7.29366 0 4.70559 0C2.11751 0 0 2.06596 0 4.59103C0 7.1161 2.11751 9.18207 4.70559 9.18207C5.52906 9.18207 6.35254 8.95251 7.05838 8.49341L8.5877 9.9855C8.82298 10.215 9.52881 10.6742 10.2347 9.9855C10.7052 9.52639 10.7052 8.83774 10.2347 8.37864ZM4.70559 6.88655C3.41155 6.88655 2.35279 5.85357 2.35279 4.59103C2.35279 3.3285 3.41155 2.29552 4.70559 2.29552C5.99962 2.29552 7.05838 3.3285 7.05838 4.59103C7.05838 5.85357 5.99962 6.88655 4.70559 6.88655Z"
                                                  transform="translate(12.9403 12.6254)" fill="#CCCCCC"/>
                                            <path d="M17.646 0H1.1764C0.470559 0 0 0.459103 0 1.14776V21.8074C0 22.4961 0.470559 22.9552 1.1764 22.9552H13.7638C11.8816 21.6926 10.5876 19.6267 10.5876 17.2164C10.5876 13.4288 13.7638 10.3298 17.646 10.3298C17.9989 10.3298 18.4694 10.3298 18.8223 10.4446V1.14776C18.8223 0.459103 18.3518 0 17.646 0ZM9.41117 9.18207H3.52919C2.82335 9.18207 2.35279 8.60819 2.35279 8.03431C2.35279 7.34565 2.82335 6.88655 3.52919 6.88655H9.41117C10.117 6.88655 10.5876 7.34565 10.5876 8.03431C10.5876 8.60819 10.117 9.18207 9.41117 9.18207ZM15.2932 4.59103H3.52919C2.82335 4.59103 2.35279 4.01715 2.35279 3.44327C2.35279 2.75462 2.82335 2.29552 3.52919 2.29552H15.2932C15.999 2.29552 16.4696 2.75462 16.4696 3.44327C16.4696 4.01715 15.999 4.59103 15.2932 4.59103Z"
                                                  fill="#CCCCCC"/>
                                        </svg>
                                        <p>Audit</p>
                                    </div>
                                    <div class="data">
                                        <ul>
                                            <?php echo the_field('beginner_level_data') ?>
                                        </ul>
                                    </div>
                                    <div class="plan_tab">
                                        <svg width="20" height="23" viewBox="0 0 20 23" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M9.96031 22.8655C11.2493 22.8655 12.3039 21.8365 12.3039 20.5789H7.61671C7.61671 21.8365 8.67133 22.8655 9.96031 22.8655ZM17.577 16.0058V9.71784C17.577 6.17368 15.1162 3.3155 11.718 2.5152V1.71491C11.718 0.800292 10.8978 0 9.96031 0C9.02287 0 8.20261 0.800292 8.20261 1.71491V2.5152C4.80439 3.3155 2.3436 6.17368 2.3436 9.71784V16.0058L0 18.2924V19.4357H19.9206V18.2924L17.577 16.0058Z"
                                                  fill="#CCCCCC"/>
                                        </svg>
                                        <p>Monitoring</p>
                                    </div>
                                    <div class="data">
                                        <ul>
                                            <?php echo the_field('beginner_level_monitoring') ?>
                                        </ul>
                                    </div>
                                    <div class="button">
                                        <input type="hidden" name="level_id" value="<?php echo $level->id?>" />
                                        <input type="hidden" name="submit-checkout" value="<?php echo $level->id?>" />
                                        <?php if($gateway == "paypalexpress" && $pmpro_requirebilling) { ?>
                                            <input type="submit" class="pmpro_btn pmpro_btn-submit-checkout <?php echo $customerMembershipLevel == $level->id ? 'choosed' : '' ?>" value="<?php echo $customerMembershipLevel == $level->id ? the_field('level_button_checked') : the_field('level_button') ?>" name="<?php echo $level->id?>">
                                        <?php } else { ?>
                                            <span id="pmpro_submit_span">
<input type="submit" class="pmpro_btn pmpro_btn-submit-checkout" value="Submit and <?php if($pmpro_requirebilling) { ?>Checkout<?php } else { ?>Confirm<?php } ?> &raquo;" />
</span>
                                        <?php } ?>
                                    </div>
                                </form>
                            <?php } ?>
                        <?php } ?>
                    </div>
                </div>
                <div class="levels_block-side">
                    <div class="card">
                        <?php
                        foreach ($pmpro_levels as $level) { ?>
                            <?php if ($level->id == 2) {
                                $_REQUEST['level'] = $level->id;
                                require(ABSPATH . "/wp-content/plugins/paid-memberships-pro/preheaders/checkout.php");
                                ?>
                                <form class="pmpro_form middle monthly levels_block-package" action="" method="post">
                                    <div class="most__popular"><?php echo the_field('best_value') ?></div>
                                    <p class="title"><?php echo the_field('basic_level_title') ?></p>
                                    <span class="description"><?php echo the_field('basic_level_recommend') ?></span>
                                    <p class="price">$<?php echo intval($level->initial_payment); ?>
                                        <span> <?php echo the_field('per_month') ?></span>
                                    </p>
                                    <div class="plan_tab">
                                        <svg width="14" height="22" viewBox="0 0 14 22" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M13.4194 11.0403L8.47127 8.03611L10.9658 1.76489C11.0428 1.60111 11.086 1.41656 11.086 1.22222C11.086 0.547556 10.5667 0 9.9227 0C9.63218 0.00488889 9.3895 0.101444 9.19699 0.261556C9.15848 0.293333 9.13515 0.312889 9.11531 0.334889L0.364705 9.01267C0.0975201 9.27789 -0.0343224 9.65922 0.00768055 10.0418C0.0496835 10.4243 0.263198 10.7641 0.580554 10.9584L5.5299 13.9651L3.00622 20.3084C2.79271 20.8413 2.96655 21.4573 3.41925 21.7849C3.61993 21.9291 3.85328 22 4.08196 22C4.37132 22 4.66067 21.8863 4.88469 21.6651L13.6353 12.9849C13.9025 12.7197 14.0343 12.3383 13.9923 11.9558C13.9503 11.5732 13.7368 11.2334 13.4194 11.0403Z"
                                                  fill="#CCCCCC"/>
                                        </svg>
                                        <p>Live Check</p>
                                    </div>
                                    <div class="plan_about">
                                        <ul>
                                            <?php echo the_field('basic_level_about') ?>
                                        </ul>
                                    </div>
                                    <div class="plan_tab">
                                        <svg width="24" height="23" viewBox="0 0 24 23" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M10.2347 8.37864L8.70534 6.88655C9.1759 6.19789 9.41117 5.39446 9.41117 4.59103C9.41117 2.06596 7.29366 0 4.70559 0C2.11751 0 0 2.06596 0 4.59103C0 7.1161 2.11751 9.18207 4.70559 9.18207C5.52906 9.18207 6.35254 8.95251 7.05838 8.49341L8.5877 9.9855C8.82298 10.215 9.52881 10.6742 10.2347 9.9855C10.7052 9.52639 10.7052 8.83774 10.2347 8.37864ZM4.70559 6.88655C3.41155 6.88655 2.35279 5.85357 2.35279 4.59103C2.35279 3.3285 3.41155 2.29552 4.70559 2.29552C5.99962 2.29552 7.05838 3.3285 7.05838 4.59103C7.05838 5.85357 5.99962 6.88655 4.70559 6.88655Z"
                                                  transform="translate(12.9403 12.6254)" fill="#CCCCCC"/>
                                            <path d="M17.646 0H1.1764C0.470559 0 0 0.459103 0 1.14776V21.8074C0 22.4961 0.470559 22.9552 1.1764 22.9552H13.7638C11.8816 21.6926 10.5876 19.6267 10.5876 17.2164C10.5876 13.4288 13.7638 10.3298 17.646 10.3298C17.9989 10.3298 18.4694 10.3298 18.8223 10.4446V1.14776C18.8223 0.459103 18.3518 0 17.646 0ZM9.41117 9.18207H3.52919C2.82335 9.18207 2.35279 8.60819 2.35279 8.03431C2.35279 7.34565 2.82335 6.88655 3.52919 6.88655H9.41117C10.117 6.88655 10.5876 7.34565 10.5876 8.03431C10.5876 8.60819 10.117 9.18207 9.41117 9.18207ZM15.2932 4.59103H3.52919C2.82335 4.59103 2.35279 4.01715 2.35279 3.44327C2.35279 2.75462 2.82335 2.29552 3.52919 2.29552H15.2932C15.999 2.29552 16.4696 2.75462 16.4696 3.44327C16.4696 4.01715 15.999 4.59103 15.2932 4.59103Z"
                                                  fill="#CCCCCC"/>
                                        </svg>
                                        <p>Audit</p>
                                    </div>
                                    <div class="data">
                                        <ul>
                                            <?php echo the_field('basic_level_data') ?>
                                        </ul>
                                    </div>
                                    <div class="plan_tab">
                                        <svg width="20" height="23" viewBox="0 0 20 23" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M9.96031 22.8655C11.2493 22.8655 12.3039 21.8365 12.3039 20.5789H7.61671C7.61671 21.8365 8.67133 22.8655 9.96031 22.8655ZM17.577 16.0058V9.71784C17.577 6.17368 15.1162 3.3155 11.718 2.5152V1.71491C11.718 0.800292 10.8978 0 9.96031 0C9.02287 0 8.20261 0.800292 8.20261 1.71491V2.5152C4.80439 3.3155 2.3436 6.17368 2.3436 9.71784V16.0058L0 18.2924V19.4357H19.9206V18.2924L17.577 16.0058Z"
                                                  fill="#CCCCCC"/>
                                        </svg>
                                        <p>Monitoring</p>
                                    </div>
                                    <div class="data">
                                        <ul>
                                            <?php echo the_field('basic_level_monitoring') ?>
                                        </ul>
                                    </div>
                                    <div class="button">
                                        <input type="hidden" name="level_id" value="<?php echo $pmpro_level->id?>" />
                                        <input type="hidden" name="submit-checkout" value="1" />
                                        <?php if($gateway == "paypalexpress" && $pmpro_requirebilling) { ?>
                                            <input type="submit" class="pmpro_btn pmpro_btn-submit-checkout <?php echo $customerMembershipLevel == $level->id ? 'choosed' : '' ?>" value="<?php echo $customerMembershipLevel == $level->id ? the_field('level_button_checked') : the_field('level_button') ?>" name="<?php echo $level->id?>">
                                        <?php } else { ?>
                                            <span id="pmpro_submit_span">
<input type="submit" class="pmpro_btn pmpro_btn-submit-checkout" value="Submit and <?php if($pmpro_requirebilling) { ?>Checkout<?php } else { ?>Confirm<?php } ?> &raquo;" />
</span>
                                        <?php } ?>
                                    </div>
                                </form>
                            <?php } ?>
                        <?php } ?>
                        <?php
                        foreach ($pmpro_levels as $level) { ?>
                            <?php if ($level->id == 10) {
                                $_REQUEST['level'] = $level->id;
                                require(ABSPATH . "/wp-content/plugins/paid-memberships-pro/preheaders/checkout.php");
                                ?>
                                <form class="pmpro_form middle yearly levels_block-package" action="" method="post">
                                    <div class="most__popular"><?php echo the_field('best_value') ?></div>
                                    <p class="title"><?php echo the_field('basic_level_title') ?></p>
                                    <span class="description"><?php echo the_field('basic_level_recommend') ?></span>
                                    <p class="price">$17.4<span> <?php echo the_field('per_month') ?></span>
                                    </p>
                                    <div class="sale">
                                        <p><?php echo the_field('price_save-basic') ?></p>
                                        <p><?php echo the_field('price_yearly-basic') ?></p>
                                    </div>
                                    <div class="plan_tab">
                                        <svg width="14" height="22" viewBox="0 0 14 22" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M13.4194 11.0403L8.47127 8.03611L10.9658 1.76489C11.0428 1.60111 11.086 1.41656 11.086 1.22222C11.086 0.547556 10.5667 0 9.9227 0C9.63218 0.00488889 9.3895 0.101444 9.19699 0.261556C9.15848 0.293333 9.13515 0.312889 9.11531 0.334889L0.364705 9.01267C0.0975201 9.27789 -0.0343224 9.65922 0.00768055 10.0418C0.0496835 10.4243 0.263198 10.7641 0.580554 10.9584L5.5299 13.9651L3.00622 20.3084C2.79271 20.8413 2.96655 21.4573 3.41925 21.7849C3.61993 21.9291 3.85328 22 4.08196 22C4.37132 22 4.66067 21.8863 4.88469 21.6651L13.6353 12.9849C13.9025 12.7197 14.0343 12.3383 13.9923 11.9558C13.9503 11.5732 13.7368 11.2334 13.4194 11.0403Z"
                                                  fill="#CCCCCC"/>
                                        </svg>
                                        <p>Live Check</p>
                                    </div>
                                    <div class="plan_about">
                                        <ul>
                                            <?php echo the_field('basic_level_about') ?>
                                        </ul>
                                    </div>
                                    <div class="plan_tab">
                                        <svg width="24" height="23" viewBox="0 0 24 23" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M10.2347 8.37864L8.70534 6.88655C9.1759 6.19789 9.41117 5.39446 9.41117 4.59103C9.41117 2.06596 7.29366 0 4.70559 0C2.11751 0 0 2.06596 0 4.59103C0 7.1161 2.11751 9.18207 4.70559 9.18207C5.52906 9.18207 6.35254 8.95251 7.05838 8.49341L8.5877 9.9855C8.82298 10.215 9.52881 10.6742 10.2347 9.9855C10.7052 9.52639 10.7052 8.83774 10.2347 8.37864ZM4.70559 6.88655C3.41155 6.88655 2.35279 5.85357 2.35279 4.59103C2.35279 3.3285 3.41155 2.29552 4.70559 2.29552C5.99962 2.29552 7.05838 3.3285 7.05838 4.59103C7.05838 5.85357 5.99962 6.88655 4.70559 6.88655Z"
                                                  transform="translate(12.9403 12.6254)" fill="#CCCCCC"/>
                                            <path d="M17.646 0H1.1764C0.470559 0 0 0.459103 0 1.14776V21.8074C0 22.4961 0.470559 22.9552 1.1764 22.9552H13.7638C11.8816 21.6926 10.5876 19.6267 10.5876 17.2164C10.5876 13.4288 13.7638 10.3298 17.646 10.3298C17.9989 10.3298 18.4694 10.3298 18.8223 10.4446V1.14776C18.8223 0.459103 18.3518 0 17.646 0ZM9.41117 9.18207H3.52919C2.82335 9.18207 2.35279 8.60819 2.35279 8.03431C2.35279 7.34565 2.82335 6.88655 3.52919 6.88655H9.41117C10.117 6.88655 10.5876 7.34565 10.5876 8.03431C10.5876 8.60819 10.117 9.18207 9.41117 9.18207ZM15.2932 4.59103H3.52919C2.82335 4.59103 2.35279 4.01715 2.35279 3.44327C2.35279 2.75462 2.82335 2.29552 3.52919 2.29552H15.2932C15.999 2.29552 16.4696 2.75462 16.4696 3.44327C16.4696 4.01715 15.999 4.59103 15.2932 4.59103Z"
                                                  fill="#CCCCCC"/>
                                        </svg>
                                        <p>Audit</p>
                                    </div>
                                    <div class="data">
                                        <ul>
                                            <?php echo the_field('basic_level_data') ?>
                                        </ul>
                                    </div>
                                    <div class="plan_tab">
                                        <svg width="20" height="23" viewBox="0 0 20 23" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M9.96031 22.8655C11.2493 22.8655 12.3039 21.8365 12.3039 20.5789H7.61671C7.61671 21.8365 8.67133 22.8655 9.96031 22.8655ZM17.577 16.0058V9.71784C17.577 6.17368 15.1162 3.3155 11.718 2.5152V1.71491C11.718 0.800292 10.8978 0 9.96031 0C9.02287 0 8.20261 0.800292 8.20261 1.71491V2.5152C4.80439 3.3155 2.3436 6.17368 2.3436 9.71784V16.0058L0 18.2924V19.4357H19.9206V18.2924L17.577 16.0058Z"
                                                  fill="#CCCCCC"/>
                                        </svg>
                                        <p>Monitoring</p>
                                    </div>
                                    <div class="data">
                                        <ul>
                                            <?php echo the_field('basic_level_monitoring') ?>
                                        </ul>
                                    </div>
                                    <div class="button">
                                        <input type="hidden" name="level_id" value="<?php echo $pmpro_level->id?>" />
                                        <input type="hidden" name="submit-checkout" value="1" />
                                        <?php if($gateway == "paypalexpress" && $pmpro_requirebilling) { ?>
                                            <input type="submit" class="pmpro_btn pmpro_btn-submit-checkout <?php echo $customerMembershipLevel == $level->id ? 'choosed' : '' ?>" value="<?php echo $customerMembershipLevel == $level->id ? the_field('level_button_checked') : the_field('level_button') ?>" name="<?php echo $level->id?>">
                                        <?php } else { ?>
                                            <span id="pmpro_submit_span">
<input type="submit" class="pmpro_btn pmpro_btn-submit-checkout" value="Submit and <?php if($pmpro_requirebilling) { ?>Checkout<?php } else { ?>Confirm<?php } ?> &raquo;" />
</span>
                                        <?php } ?>
                                    </div>
                                </form>
                            <?php } ?>
                        <?php } ?>
                    </div>
                </div>
                <div class="levels_block-side">
                    <div class="card">
                        <?php
                        foreach ($pmpro_levels as $level) { ?>
                            <?php if ($level->id == 3) {
                                $_REQUEST['level'] = $level->id;
                                require(ABSPATH . "/wp-content/plugins/paid-memberships-pro/preheaders/checkout.php");
                                ?>
                                <form class="pmpro_form last monthly levels_block-package" action="" method="post">
                                    <p class="title"><?php echo the_field('pro_level_title') ?></p>
                                    <span class="description"><?php echo the_field('pro_level_recommend') ?></span>
                                    <p class="price">$<?php echo intval($level->initial_payment); ?>
                                        <span> <?php echo the_field('per_month') ?></span>
                                    </p>
                                    <div class="plan_tab">
                                        <svg width="14" height="22" viewBox="0 0 14 22" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M13.4194 11.0403L8.47127 8.03611L10.9658 1.76489C11.0428 1.60111 11.086 1.41656 11.086 1.22222C11.086 0.547556 10.5667 0 9.9227 0C9.63218 0.00488889 9.3895 0.101444 9.19699 0.261556C9.15848 0.293333 9.13515 0.312889 9.11531 0.334889L0.364705 9.01267C0.0975201 9.27789 -0.0343224 9.65922 0.00768055 10.0418C0.0496835 10.4243 0.263198 10.7641 0.580554 10.9584L5.5299 13.9651L3.00622 20.3084C2.79271 20.8413 2.96655 21.4573 3.41925 21.7849C3.61993 21.9291 3.85328 22 4.08196 22C4.37132 22 4.66067 21.8863 4.88469 21.6651L13.6353 12.9849C13.9025 12.7197 14.0343 12.3383 13.9923 11.9558C13.9503 11.5732 13.7368 11.2334 13.4194 11.0403Z"
                                                  fill="#CCCCCC"/>
                                        </svg>
                                        <p>Live Check</p>
                                    </div>
                                    <div class="plan_about">
                                        <ul>
                                            <?php echo the_field('pro_level_about') ?>
                                        </ul>
                                    </div>
                                    <div class="plan_tab">
                                        <svg width="24" height="23" viewBox="0 0 24 23" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M10.2347 8.37864L8.70534 6.88655C9.1759 6.19789 9.41117 5.39446 9.41117 4.59103C9.41117 2.06596 7.29366 0 4.70559 0C2.11751 0 0 2.06596 0 4.59103C0 7.1161 2.11751 9.18207 4.70559 9.18207C5.52906 9.18207 6.35254 8.95251 7.05838 8.49341L8.5877 9.9855C8.82298 10.215 9.52881 10.6742 10.2347 9.9855C10.7052 9.52639 10.7052 8.83774 10.2347 8.37864ZM4.70559 6.88655C3.41155 6.88655 2.35279 5.85357 2.35279 4.59103C2.35279 3.3285 3.41155 2.29552 4.70559 2.29552C5.99962 2.29552 7.05838 3.3285 7.05838 4.59103C7.05838 5.85357 5.99962 6.88655 4.70559 6.88655Z"
                                                  transform="translate(12.9403 12.6254)" fill="#CCCCCC"/>
                                            <path d="M17.646 0H1.1764C0.470559 0 0 0.459103 0 1.14776V21.8074C0 22.4961 0.470559 22.9552 1.1764 22.9552H13.7638C11.8816 21.6926 10.5876 19.6267 10.5876 17.2164C10.5876 13.4288 13.7638 10.3298 17.646 10.3298C17.9989 10.3298 18.4694 10.3298 18.8223 10.4446V1.14776C18.8223 0.459103 18.3518 0 17.646 0ZM9.41117 9.18207H3.52919C2.82335 9.18207 2.35279 8.60819 2.35279 8.03431C2.35279 7.34565 2.82335 6.88655 3.52919 6.88655H9.41117C10.117 6.88655 10.5876 7.34565 10.5876 8.03431C10.5876 8.60819 10.117 9.18207 9.41117 9.18207ZM15.2932 4.59103H3.52919C2.82335 4.59103 2.35279 4.01715 2.35279 3.44327C2.35279 2.75462 2.82335 2.29552 3.52919 2.29552H15.2932C15.999 2.29552 16.4696 2.75462 16.4696 3.44327C16.4696 4.01715 15.999 4.59103 15.2932 4.59103Z"
                                                  fill="#CCCCCC"/>
                                        </svg>
                                        <p>Audit</p>
                                    </div>
                                    <div class="data">
                                        <ul>
                                            <?php echo the_field('pro_level_data') ?>
                                        </ul>
                                    </div>
                                    <div class="plan_tab">
                                        <svg width="20" height="23" viewBox="0 0 20 23" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M9.96031 22.8655C11.2493 22.8655 12.3039 21.8365 12.3039 20.5789H7.61671C7.61671 21.8365 8.67133 22.8655 9.96031 22.8655ZM17.577 16.0058V9.71784C17.577 6.17368 15.1162 3.3155 11.718 2.5152V1.71491C11.718 0.800292 10.8978 0 9.96031 0C9.02287 0 8.20261 0.800292 8.20261 1.71491V2.5152C4.80439 3.3155 2.3436 6.17368 2.3436 9.71784V16.0058L0 18.2924V19.4357H19.9206V18.2924L17.577 16.0058Z"
                                                  fill="#CCCCCC"/>
                                        </svg>
                                        <p>Monitoring</p>
                                    </div>
                                    <div class="data">
                                        <ul>
                                            <?php echo the_field('pro_level_monitoring') ?>
                                        </ul>
                                    </div>
                                    <div class="button">
                                        <input type="hidden" name="level_id" value="<?php echo $pmpro_level->id?>" />
                                        <input type="hidden" name="submit-checkout" value="1" />
                                        <?php if($gateway == "paypalexpress" && $pmpro_requirebilling) { ?>
                                            <input type="submit" class="pmpro_btn pmpro_btn-submit-checkout <?php echo $customerMembershipLevel == $level->id ? 'choosed' : '' ?>" value="<?php echo $customerMembershipLevel == $level->id ? the_field('level_button_checked') : the_field('level_button') ?>" name="<?php echo $level->id?>">
                                        <?php } else { ?>
                                            <span id="pmpro_submit_span">
<input type="submit" class="pmpro_btn pmpro_btn-submit-checkout" value="Submit and <?php if($pmpro_requirebilling) { ?>Checkout<?php } else { ?>Confirm<?php } ?> &raquo;" />
</span>
                                        <?php } ?>
                                    </div>
                                </form>
                            <?php } ?>
                        <?php } ?>
                        <?php
                        foreach ($pmpro_levels as $level) { ?>
                            <?php if ($level->id == 11) {
                                $_REQUEST['level'] = $level->id;
                                require(ABSPATH . "/wp-content/plugins/paid-memberships-pro/preheaders/checkout.php");
                                ?>
                                <form class="pmpro_form last yearly levels_block-package" action="" method="post">
                                    <p class="title"><?php echo the_field('pro_level_title') ?></p>
                                    <span class="description"><?php echo the_field('pro_level_recommend') ?></span>
                                    <p class="price">$41.2<span> <?php echo the_field('per_month') ?></span>
                                    </p>
                                    <div class="sale">
                                        <p><?php echo the_field('price_save-pro') ?></p>
                                        <p><?php echo the_field('price_yearly-pro') ?></p>
                                    </div>
                                    <div class="plan_tab">
                                        <svg width="14" height="22" viewBox="0 0 14 22" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M13.4194 11.0403L8.47127 8.03611L10.9658 1.76489C11.0428 1.60111 11.086 1.41656 11.086 1.22222C11.086 0.547556 10.5667 0 9.9227 0C9.63218 0.00488889 9.3895 0.101444 9.19699 0.261556C9.15848 0.293333 9.13515 0.312889 9.11531 0.334889L0.364705 9.01267C0.0975201 9.27789 -0.0343224 9.65922 0.00768055 10.0418C0.0496835 10.4243 0.263198 10.7641 0.580554 10.9584L5.5299 13.9651L3.00622 20.3084C2.79271 20.8413 2.96655 21.4573 3.41925 21.7849C3.61993 21.9291 3.85328 22 4.08196 22C4.37132 22 4.66067 21.8863 4.88469 21.6651L13.6353 12.9849C13.9025 12.7197 14.0343 12.3383 13.9923 11.9558C13.9503 11.5732 13.7368 11.2334 13.4194 11.0403Z"
                                                  fill="#CCCCCC"/>
                                        </svg>
                                        <p>Live Check</p>
                                    </div>
                                    <div class="plan_about">
                                        <ul>
                                            <?php echo the_field('pro_level_about') ?>
                                        </ul>
                                    </div>
                                    <div class="plan_tab">
                                        <svg width="24" height="23" viewBox="0 0 24 23" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M10.2347 8.37864L8.70534 6.88655C9.1759 6.19789 9.41117 5.39446 9.41117 4.59103C9.41117 2.06596 7.29366 0 4.70559 0C2.11751 0 0 2.06596 0 4.59103C0 7.1161 2.11751 9.18207 4.70559 9.18207C5.52906 9.18207 6.35254 8.95251 7.05838 8.49341L8.5877 9.9855C8.82298 10.215 9.52881 10.6742 10.2347 9.9855C10.7052 9.52639 10.7052 8.83774 10.2347 8.37864ZM4.70559 6.88655C3.41155 6.88655 2.35279 5.85357 2.35279 4.59103C2.35279 3.3285 3.41155 2.29552 4.70559 2.29552C5.99962 2.29552 7.05838 3.3285 7.05838 4.59103C7.05838 5.85357 5.99962 6.88655 4.70559 6.88655Z"
                                                  transform="translate(12.9403 12.6254)" fill="#CCCCCC"/>
                                            <path d="M17.646 0H1.1764C0.470559 0 0 0.459103 0 1.14776V21.8074C0 22.4961 0.470559 22.9552 1.1764 22.9552H13.7638C11.8816 21.6926 10.5876 19.6267 10.5876 17.2164C10.5876 13.4288 13.7638 10.3298 17.646 10.3298C17.9989 10.3298 18.4694 10.3298 18.8223 10.4446V1.14776C18.8223 0.459103 18.3518 0 17.646 0ZM9.41117 9.18207H3.52919C2.82335 9.18207 2.35279 8.60819 2.35279 8.03431C2.35279 7.34565 2.82335 6.88655 3.52919 6.88655H9.41117C10.117 6.88655 10.5876 7.34565 10.5876 8.03431C10.5876 8.60819 10.117 9.18207 9.41117 9.18207ZM15.2932 4.59103H3.52919C2.82335 4.59103 2.35279 4.01715 2.35279 3.44327C2.35279 2.75462 2.82335 2.29552 3.52919 2.29552H15.2932C15.999 2.29552 16.4696 2.75462 16.4696 3.44327C16.4696 4.01715 15.999 4.59103 15.2932 4.59103Z"
                                                  fill="#CCCCCC"/>
                                        </svg>
                                        <p>Audit</p>
                                    </div>
                                    <div class="data">
                                        <ul>
                                            <?php echo the_field('pro_level_data') ?>
                                        </ul>
                                    </div>
                                    <div class="plan_tab">
                                        <svg width="20" height="23" viewBox="0 0 20 23" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M9.96031 22.8655C11.2493 22.8655 12.3039 21.8365 12.3039 20.5789H7.61671C7.61671 21.8365 8.67133 22.8655 9.96031 22.8655ZM17.577 16.0058V9.71784C17.577 6.17368 15.1162 3.3155 11.718 2.5152V1.71491C11.718 0.800292 10.8978 0 9.96031 0C9.02287 0 8.20261 0.800292 8.20261 1.71491V2.5152C4.80439 3.3155 2.3436 6.17368 2.3436 9.71784V16.0058L0 18.2924V19.4357H19.9206V18.2924L17.577 16.0058Z"
                                                  fill="#CCCCCC"/>
                                        </svg>
                                        <p>Monitoring</p>
                                    </div>
                                    <div class="data">
                                        <ul>
                                            <?php echo the_field('pro_level_monitoring') ?>
                                        </ul>
                                    </div>
                                    <div class="button">
                                        <input type="hidden" name="level_id" value="<?php echo $pmpro_level->id?>" />
                                        <input type="hidden" name="submit-checkout" value="1" />
                                        <?php if($gateway == "paypalexpress" && $pmpro_requirebilling) { ?>
                                            <input type="submit" class="pmpro_btn pmpro_btn-submit-checkout <?php echo $customerMembershipLevel == $level->id ? 'choosed' : '' ?>" value="<?php echo $customerMembershipLevel == $level->id ? the_field('level_button_checked') : the_field('level_button') ?>" name="<?php echo $level->id?>">
                                        <?php } else { ?>
                                            <span id="pmpro_submit_span">
<input type="submit" class="pmpro_btn pmpro_btn-submit-checkout" value="Submit and <?php if($pmpro_requirebilling) { ?>Checkout<?php } else { ?>Confirm<?php } ?> &raquo;" />
</span>
                                        <?php } ?>
                                    </div>
                                </form>
                            <?php } ?>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
        <style>
            .testimonials__pricing{
                display: flex;
                align-items: flex-start;
                justify-content: center;
                flex-wrap: wrap;
                margin: 50px auto -10px auto;
                width: 80%;
            }
            .testimonials__pricing div{
                width: 50%;
                padding: 10px;
                text-align: center;
            }
            .testimonials__pricing div iframe{
                max-width: 100%;
                height: 129px;
                overflow: hidden;
                border-bottom: 1px solid #e4e4e4!important;
                border-bottom-left-radius: 2px;
                border-bottom-right-radius: 2px;
            }
            .testimonials__pricing div twitterwidget{
                margin: 0 auto;
            }
            @media (max-width: 640px) {
                .testimonials__pricing div{
                    width: 100%;
                }
            }
        </style>
        <div class="testimonials__pricing">
            <div><blockquote class="twitter-tweet" data-cards="hidden" data-lang="en"><p lang="it" dir="ltr">RT commvill : Sitechecker è un tool per analisi <a href="https://twitter.com/hashtag/SEO?src=hash&amp;ref_src=twsrc%5Etfw">#SEO</a> estremamente efficace. Ecco come funziona e quali informazioni dà | <a href="https://twitter.com/hashtag/web?src=hash&amp;ref_src=twsrc%5Etfw">#web</a> <a href="https://twitter.com/hashtag/webdeveloper?src=hash&amp;ref_src=twsrc%5Etfw">#webdeveloper</a> <a href="https://twitter.com/hashtag/webmarketing?src=hash&amp;ref_src=twsrc%5Etfw">#webmarketing</a><a href="https://t.co/encuFQpfMT">https://t.co/encuFQpfMT</a> <a href="https://t.co/76CdM6ooSV">pic.twitter.com/76CdM6ooSV</a></p>&mdash; Pierpaolo Di Natale (@PdnSan) <a href="https://twitter.com/PdnSan/status/988684008832806912?ref_src=twsrc%5Etfw">April 24, 2018</a></blockquote>
                <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script></div>
            <div><blockquote class="twitter-tweet" data-cards="hidden" data-lang="en"><p lang="it" dir="ltr">Non possiamo essere tutti competenti <a href="https://twitter.com/hashtag/SEO?src=hash&amp;ref_src=twsrc%5Etfw">#SEO</a>, ma tutti possiamo fare un&#39;analisi rapida del nostro sito per capire se i motori di ricerca lo leggano bene o no. Basta usare SITECHECKER, <a href="https://twitter.com/hashtag/freetool?src=hash&amp;ref_src=twsrc%5Etfw">#freetool</a> che dà un report e suggerimenti su più di 100 parametri <a href="https://t.co/c505cizo1H">https://t.co/c505cizo1H</a> <a href="https://t.co/dGcyOzq5Un">pic.twitter.com/dGcyOzq5Un</a></p>&mdash; 📌 Giovanni Dalla Bona (@Imparafacile) <a href="https://twitter.com/Imparafacile/status/1027098891714539520?ref_src=twsrc%5Etfw">August 8, 2018</a></blockquote>
                <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script></div>
        </div>
        <div class="home_faq">
            <p class="home_block-title"><?php echo the_field('faq') ?></p>
            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingOne">
                        <div class="panel-title">
                            <h4 role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne"
                                aria-expanded="false" aria-controls="collapseOne">
                                <?php echo the_field('faq_title_1'); ?>
                            </h4>
                        </div>
                    </div>
                    <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                        <div class="panel-body">
                            <?php echo the_field('faq_text_1'); ?>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingTwo">
                        <div class="panel-title">
                            <h4 role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo"
                                aria-expanded="false" aria-controls="collapseTwo">
                                <?php echo the_field('faq_title_2'); ?>
                            </h4>
                        </div>
                    </div>
                    <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                        <div class="panel-body">
                            <?php echo the_field('faq_text_2'); ?>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingThree">
                        <div class="panel-title">
                            <h4 role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree"
                                aria-expanded="false" aria-controls="collapseThree">
                                <?php echo the_field('faq_title_3'); ?>
                            </h4>
                        </div>
                    </div>
                    <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                        <div class="panel-body">
                            <?php echo the_field('faq_text_3'); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
