<?php
/*
    Template Name: Buy
*/

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $_REQUEST['level'] = $_POST['level_id'];
    require(ABSPATH . "/wp-content/plugins/paid-memberships-pro/preheaders/checkout.php");
}

get_header('tmp_design');
get_template_part('design/checkout');
get_footer('tmp_design');
?>