<?php
    $template_directory_uri = get_template_directory_uri();
?>
<div class="popup_sent">
    <div class="popup_sent-block">
        <img src="<?php echo get_template_directory_uri(); ?>/out/img/img-close.svg" alt="close" title="close"
             class="popup_sent-close">
        <img src="<?php echo get_template_directory_uri(); ?>/out/img/letter.svg" alt="sent-mail" title="sent-mail"
             class="popup_sent-img">
        <p><?php echo get_post_meta(url_to_postid("/technical-seo-audit/"), 'sent.form', true) ?></p>
    </div>
</div>
<main class="contact_us">
    <div class="wrapper">
        <div class="contact_wrapper">
            <div class="contact_wrapper-about">
                <img src="<?php echo get_template_directory_uri(); ?>/out/img/contact_letter.svg" alt="contact_letter">
                <p><?php echo get_post_meta($post->ID, 'form_description', true) ?></p>
            </div>
            <div class="contact_wrapper-form">
                <p class="contact-form_title"><?php echo get_post_meta($post->ID, 'form.title', true) ?></p>
                <form id="contact__form">
                    <fieldset>
                        <span>
                            <input type="text" name="your-name" value="" size="40" placeholder="Your name" id="contact__form-name">
                        </span>
                        <label>Your name</label>
                        <div class="contact__form-error" name="name"></div>
                    </fieldset>
                    <fieldset>
                        <span>
                            <input type="email" name="your-email" value="" size="40" placeholder="Your E-mail" id="contact__form-email">
                        </span>
                        <label>Your Email</label>
                        <div class="contact__form-error" name="email"></div>
                    </fieldset>
                    <fieldset>
                        <span>
                            <textarea name="your-message" cols="40" rows="10" placeholder="Message" id="contact__form-mess"></textarea>
                        </span>
                        <label>Message</label>
                    </fieldset>
                    <p>
                        <input type="submit" value="Send" id="contact__form-btn">
                    </p>
                </form>
            </div>
        </div>
    </div>
</main>