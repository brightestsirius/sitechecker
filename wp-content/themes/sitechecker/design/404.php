<?php
    $template_directory_uri = get_template_directory_uri();
?>
<main class="errorPage">
    <div class="wrapper errorPage__tmp">
        <img src="<?php echo get_template_directory_uri(); ?>/out/img_design/404.svg" alt="404" title="404">
        <p>We can’t seem to find a page you’re looking for.</p>
        <a href="/" class="btn">Back to homepage</a>
    </div>
</main>
