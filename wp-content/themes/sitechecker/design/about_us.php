<?php
    $template_directory_uri = get_template_directory_uri();
?>
<main class="about_us">
    <div class="about_us-first">
        <h1>We are Sitechecker</h1>
        <h2>We are a team of specialists who can’t imagine their lives without shaking today’s world<br> of technology with innovative solutions. There are no limits for us in creativity and originality<br> along with design and functionality. We strongly believe that making complex things simple is our daily mission.</h2>
    </div>
    <div class="about_us-story">
        <p class="audit__title">Our story</p>
        <div class="wrapper story__container">
            <div class="story__text">
                <div class="story__text-block">
                    <p class="title">History</p>
                    <p class="description">Our journey had begun in 2016 when Sitechecker was firstly introduced to the world. Since then, we have been successfully helping millions of people to solve the exhausted problem IT specialists have since the internet was invented – how to make your website better. But that is not all: now thousands of ordinary people use Sitechecker to analyze their websites, find errors and weak spots. Using our service, they found the ways on how to fix them, and thereby improve website’s overall performance and Google ranking.</p>
                </div>
                <div class="story__text-block">
                    <p class="title">Team</p>
                    <p class="description">Today, Sitechecker is a team of professional back-end and front-end developers, creative designers and marketing specialists whose main aim is turning your life into new pro direction. We carefully select only the best experts to join our team who are true professionals in their spheres and who can deliver only top-class products.</p>
                </div>
                <div class="story__text-block">
                    <p class="title">Values</p>
                    <p class="description">We do not know what the word “impossible” means. We gathered the best and brightest under our roof, people who share one common value, and this value is offering a helpful hand to those in need. Our skilled team has a passion for building a world-class product which is made on trust, dedication and professionalism.</p>
                </div>
            </div>
            <div class="story__image">
                <img src="<?php echo get_template_directory_uri(); ?>/out/img_design/about__story.png" alt="about__story" title="about__story">
            </div>
        </div>
    </div>
    <div class="about_us-team">
        <div class="wrapper">
            <p class="audit__title">Meet the Team</p>
            <div class="about__team-container owl-carousel owl-theme">
                <div class="team__member item">
                    <div class="img" style="background-image: url('/wp-content/themes/sitechecker/out/img_design/Anton__Reva.png')"></div>
                    <p class="name">Anton</p>
                    <p class="position">Product Author and Ideologist</p>
                    <div class="team__member-social">
                        <a class="member-social" href="https://www.linkedin.com/in/anton-reva-2826512/" target="_blank">
                            <img src="<?php echo get_template_directory_uri(); ?>/out/img_design/linkedin__icon.svg" alt="linkedin" title="linkedin">
                        </a>
                        <a class="member-social" href="https://www.facebook.com/anton.reva.9" target="_blank">
                            <img src="<?php echo get_template_directory_uri(); ?>/out/img_design/fb__icon.svg" alt="facebook" title="facebook">
                        </a>
                    </div>
                </div>
                <div class="team__member item">
                    <div class="img" style="background-image: url('/wp-content/themes/sitechecker/out/img_design/Dima__Bondar.png')"></div>
                    <p class="name">Dima</p>
                    <p class="position">CEO</p>
                    <div class="team__member-social">
                        <a class="member-social" href="https://www.linkedin.com/in/dmitriybondar/" target="_blank">
                            <img src="<?php echo get_template_directory_uri(); ?>/out/img_design/linkedin__icon.svg" alt="linkedin" title="linkedin">
                        </a>
                        <a class="member-social" href="https://www.facebook.com/bo0mer" target="_blank">
                            <img src="<?php echo get_template_directory_uri(); ?>/out/img_design/fb__icon.svg" alt="facebook" title="facebook">
                        </a>
                    </div>
                </div>
                <div class="team__member item">
                    <div class="img" style="background-image: url('/wp-content/themes/sitechecker/out/img_design/Alexei__Prokashev.png')"></div>
                    <p class="name">Alexei</p>
                    <p class="position">CTO</p>
                    <div class="team__member-social">
                        <a class="member-social" href="https://www.linkedin.com/in/alexey-prokashev-94250630/" target="_blank">
                            <img src="<?php echo get_template_directory_uri(); ?>/out/img_design/linkedin__icon.svg" alt="linkedin" title="linkedin">
                        </a>
                        <a class="member-social" href="https://www.facebook.com/aleksej.prokasev" target="_blank">
                            <img src="<?php echo get_template_directory_uri(); ?>/out/img_design/fb__icon.svg" alt="facebook" title="facebook">
                        </a>
                    </div>
                </div>
                <div class="team__member item">
                    <div class="img" style="background-image: url('/wp-content/themes/sitechecker/out/img_design/Maxim__Ponomarenko.png')"></div>
                    <p class="name">Maxim</p>
                    <p class="position">Project Manager</p>
                    <div class="team__member-social">
                        <a class="member-social" href="https://www.linkedin.com/in/maximpnm/" target="_blank">
                            <img src="<?php echo get_template_directory_uri(); ?>/out/img_design/linkedin__icon.svg" alt="linkedin" title="linkedin">
                        </a>
                        <a class="member-social" href="https://www.facebook.com/maximpnm" target="_blank">
                            <img src="<?php echo get_template_directory_uri(); ?>/out/img_design/fb__icon.svg" alt="facebook" title="facebook">
                        </a>
                    </div>
                </div>
                <div class="team__member item">
                    <div class="img" style="background-image: url('/wp-content/themes/sitechecker/out/img_design/Ivan__Palii.png')"></div>
                    <p class="name">Ivan</p>
                    <p class="position">Marketing Manager</p>
                    <div class="team__member-social">
                        <a class="member-social" href="https://www.linkedin.com/in/ivanpalii/" target="_blank">
                            <img src="<?php echo get_template_directory_uri(); ?>/out/img_design/linkedin__icon.svg" alt="linkedin" title="linkedin">
                        </a>
                        <a class="member-social" href="https://www.facebook.com/ivann.palii" target="_blank">
                            <img src="<?php echo get_template_directory_uri(); ?>/out/img_design/fb__icon.svg" alt="facebook" title="facebook">
                        </a>
                    </div>
                </div>
                <div class="team__member item">
                    <div class="img" style="background-image: url('/wp-content/themes/sitechecker/out/img_design/natalia.jpg');background-position: top left;background-size: 150%;"></div>
                    <p class="name">Natalia</p>
                    <p class="position">SEO Specialist</p>
                    <div class="team__member-social">
                        <a class="member-social" href="https://www.linkedin.com/in/natalia-fialkovska-3322b614a/" target="_blank">
                            <img src="<?php echo get_template_directory_uri(); ?>/out/img_design/linkedin__icon.svg" alt="linkedin" title="linkedin">
                        </a>
                        <a class="member-social" href="https://www.facebook.com/natka.fia" target="_blank">
                            <img src="<?php echo get_template_directory_uri(); ?>/out/img_design/fb__icon.svg" alt="facebook" title="facebook">
                        </a>
                    </div>
                </div>
                <div class="team__member item">
                    <div class="img" style="background-image: url('/wp-content/themes/sitechecker/out/img_design/Edward-Petrossi.jpg')"></div>
                    <p class="name">Edward</p>
                    <p class="position">Full-stack developer</p>
                    <div class="team__member-social">
                        <a class="member-social" href="https://www.linkedin.com/in/edward-petrossi-b04269101/" target="_blank">
                            <img src="<?php echo get_template_directory_uri(); ?>/out/img_design/linkedin__icon.svg" alt="linkedin" title="linkedin">
                        </a>
                        <a class="member-social" href="https://www.facebook.com/edipetrossi" target="_blank">
                            <img src="<?php echo get_template_directory_uri(); ?>/out/img_design/fb__icon.svg" alt="facebook" title="facebook">
                        </a>
                    </div>
                </div>
                <div class="team__member item">
                    <div class="img" style="background-image: url('/wp-content/themes/sitechecker/out/img_design/Shypovskaya.jpg');background-size: 190%;"></div>
                    <p class="name">Katherina</p>
                    <p class="position">Front-end developer</p>
                    <div class="team__member-social">
                        <a class="member-social" href="https://www.linkedin.com/in/katherina-shypovskaya-8a246b103/" target="_blank">
                            <img src="<?php echo get_template_directory_uri(); ?>/out/img_design/linkedin__icon.svg" alt="linkedin" title="linkedin">
                        </a>
                        <a class="member-social" href="https://www.facebook.com/shypovskaya" target="_blank">
                            <img src="<?php echo get_template_directory_uri(); ?>/out/img_design/fb__icon.svg" alt="facebook" title="facebook">
                        </a>
                    </div>
                </div>
                <div class="team__member item">
                        <div class="img" style="background-image: url('/wp-content/themes/sitechecker/out/img_design/Oleh.png');background-size: auto 175%;background-position: -71px 4%;"></div>
                        <p class="name">Oleh</p>
                        <p class="position">QA Team Lead</p>
                        <div class="team__member-social">
                            <a class="member-social" href="https://www.linkedin.com/in/%D0%BE%D0%BB%D0%B5%D0%B3-%D0%BA%D0%B0%D0%BB%D1%83%D0%B6%D0%B8%D0%BD-a354bb119/" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/out/img_design/linkedin__icon.svg" alt="linkedin" title="linkedin"></a>
                        </div>
                    </div>
            </div>
        </div>
    </div>
    <div class="wrapper about__us-boosta">
        <img src="<?php echo get_template_directory_uri(); ?>/out/img_design/about__us-boosta.svg" alt="Boosta" title="Boosta">
    </div>
</main>
