<?php
    $template_directory_uri = get_template_directory_uri();
    $meta = get_post_meta_all($post->ID);
    $metaDesign = get_post_meta_all(url_to_postid( '/website-health-checker-design/' ));
    if(is_user_logged_in()){
        $userDomains = getDomainsByUserId(get_current_user_id());
        $availableLimit = getAvailableLimitByUser(get_current_user_id());
        $pagesLimits = $availableLimit->getAvailablePages();
        if($pagesLimits <= 0){
            $crawlButton = 'limits__btn';
            $monitoringButton = 'limits__btn';
        }else{
            $crawlButton = 'sitechecker_crawl';
            $monitoringButton = 'sitechecker_monitoring';
        }
    } else{
        $crawlButton = 'sitechecker_reg';
        $monitoringButton = 'sitechecker_reg-monitoring';
    }
?>
<main class="audit__landing">
    <div class="audit__first">
        <div class="wrapper audit__first-container">
            <h1><?=t($meta['main.h1']);?></h1>
            <h2><?=t($meta['main.h2']);?></h2>
            <form class="article__seo-search audit__form">
                <span class="error">Must be a valid URL with http:// or https://</span>
                <div class="error__limits">No limits! <a href="/plans/">Upgrade your account</a> to crawl this domain</div>
                <input type="text" placeholder="<?=t($metaDesign['health_checker_placeholder']);?>">
                <button type="submit" class="<?php echo $crawlButton; ?>"><img src="<?= $template_directory_uri; ?>/out/img_design/arrow.svg" alt="arrow" title="arrow" class="arrow"><img src="<?= $template_directory_uri; ?>/out/img_design/loading.svg" alt="arrow" title="arrow" class="loading"></button>
            </form>
        </div>
    </div>
    <?php if(t($metaDesign['website_health_matters']) !== ''){ ?>
        <div class="audit__second">
        <div class="wrapper">
            <p class="audit__title"><?=t($metaDesign['website_health_matters']);?></p>
            <div class="audit__second-content">
                <div class="audit__second-left">
                    <div class="audit__matter">
                        <img src="<?= $template_directory_uri; ?>/out/img_design/audit__matter-first.svg" alt="audit" title="audit">
                        <div>
                            <p class="audit__matter-title"><?=t($metaDesign['health_matters_1_title']);?></p>
                            <p class="audit__matter-description"><?=t($metaDesign['health_matters_1_description']);?></p>
                        </div>
                    </div>
                </div>
                <div class="audit__second-right">
                    <div class="audit__matter">
                        <img src="<?= $template_directory_uri; ?>/out/img_design/audit__matter-second.svg" alt="audit" title="audit">
                        <div>
                            <p class="audit__matter-title"><?=t($metaDesign['health_matters_2_title']);?></p>
                            <p class="audit__matter-description"><?=t($metaDesign['health_matters_2_description']);?></p>
                        </div>
                    </div>
                    <div class="audit__matter">
                        <img src="<?= $template_directory_uri; ?>/out/img_design/audit__matter-third.svg" alt="audit" title="audit">
                        <div>
                            <p class="audit__matter-title"><?=t($metaDesign['health_matters_3_title']);?></p>
                            <p class="audit__matter-description"><?=t($metaDesign['health_matters_3_description']);?></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php }?>
    <div class="audit_forWhom">
        <div class="wrapper">
            <p class="audit__title"><?=t($metaDesign['for_whom_title']);?></p>
            <div class="audit_forWhom-container">
                <div class="forWhom__block">
                    <img src="<?= $template_directory_uri; ?>/out/img_design/for__whom__1.svg" alt="cms">
                    <?=t($metaDesign['for_whom_block_1']);?>
                </div>
                <div class="forWhom__block">
                    <img src="<?= $template_directory_uri; ?>/out/img_design/for__whom__2.svg" alt="cms">
                    <?=t($metaDesign['for_whom_block_2']);?>
                </div>
                <div class="forWhom__block">
                    <img src="<?= $template_directory_uri; ?>/out/img_design/for__whom__3.svg" alt="cms">
                    <?=t($metaDesign['for_whom_block_3']);?>
                </div>
            </div>
        </div>
    </div>
    <div class="audit__third">
        <div class="wrapper">
            <p class="audit__title"><?=t($metaDesign['cms']);?></p>
            <div class="audit__third-content">
                <div class="audit__third-cont">
                    <div class="audit__third-block">
                        <img src="<?= $template_directory_uri; ?>/out/img_design/cms_1.svg" alt="cms" title="cms">
                    </div>
                </div>
                <div class="audit__third-cont">
                    <div class="audit__third-block">
                        <img src="<?= $template_directory_uri; ?>/out/img_design/cms_2.svg" alt="cms" title="cms">
                    </div>
                </div>
                <div class="audit__third-cont">
                    <div class="audit__third-block">
                        <img src="<?= $template_directory_uri; ?>/out/img_design/cms_3.svg" alt="cms" title="cms">
                    </div>
                </div>
                <div class="audit__third-cont">
                    <div class="audit__third-block">
                        <img src="<?= $template_directory_uri; ?>/out/img_design/cms_4.svg" alt="cms" title="cms">
                    </div>
                </div>
                <div class="audit__third-cont">
                    <div class="audit__third-block">
                        <img src="<?= $template_directory_uri; ?>/out/img_design/cms_5.svg" alt="cms" title="cms">
                    </div>
                </div>
                <div class="audit__third-cont">
                    <div class="audit__third-block">
                        <img src="<?= $template_directory_uri; ?>/out/img_design/cms_6.svg" alt="cms" title="cms">
                    </div>
                </div>
                <div class="audit__third-cont">
                    <div class="audit__third-block">
                        <img src="<?= $template_directory_uri; ?>/out/img_design/cms_7.svg" alt="cms" title="cms">
                    </div>
                </div>
                <div class="audit__third-cont">
                    <div class="audit__third-block">
                        <img src="<?= $template_directory_uri; ?>/out/img_design/cms_8.svg" alt="cms" title="cms">
                    </div>
                </div>
                <div class="audit__third-cont">
                    <div class="audit__third-block">
                        <img src="<?= $template_directory_uri; ?>/out/img_design/cms_9.svg" alt="cms" title="cms">
                    </div>
                </div>
                <div class="audit__third-cont">
                    <div class="audit__third-block">
                        <p>Your Сustom CMS</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php if(t($metaDesign['crawler_helps_1_title']) !== ''){ ?>
        <div class="audit__fourth">
        <div class="wrapper">
            <p class="audit__title"><?=t($metaDesign['crawler_helps_title']);?></p>
            <div id="audit__carousel" class="carousel slide carousel-fade audit__carousel" data-ride="carousel">
                <div class="carousel-inner">
                    <div class="carousel-item active">
                        <div class="audit__helps">
                            <div class="audit__helps-text">
                                <img src="<?= $template_directory_uri; ?>/out/img_design/audit__landing-icon.svg" alt="audit__helps" title="audit__helps" class="audit__helps-img">
                                <p class="audit__helps-title"><?=t($metaDesign['crawler_helps_1_title']);?></p>
                                <p class="audit__helps-descr"><?=t($metaDesign['crawler_helps_1_description']);?></p>
                            </div>
                            <div class="audit__helps__img">
                                <img src="<?= $template_directory_uri; ?>/out/img_design/audit__carousel-bg1.svg" alt="audit__helps" title="audit__helps" class="audit__carousel-bg1">
                                <img src="<?= $template_directory_uri; ?>/out/img_design/auditOne.svg" alt="audit__helps" title="audit__helps" class="audit__helps-img">
                                <img src="<?= $template_directory_uri; ?>/out/img_design/audit__carousel-bg2.svg" alt="audit__helps" title="audit__helps" class="audit__carousel-bg2">
                            </div>
                        </div>
                    </div>
                    <?php if(t($metaDesign['crawler_helps_2_title']) !== ''){ ?>
                        <div class="carousel-item">
                            <div class="audit__helps">
                                <div class="audit__helps-text">
                                    <img src="<?= $template_directory_uri; ?>/out/img_design/audit__landing-icon.svg" alt="audit__helps" title="audit__helps" class="audit__helps-img">
                                    <p class="audit__helps-title"><?=t($metaDesign['crawler_helps_2_title']);?></p>
                                    <p class="audit__helps-descr"><?=t($metaDesign['crawler_helps_2_description']);?></p>
                                </div>
                                <div class="audit__helps__img">
                                    <img src="<?= $template_directory_uri; ?>/out/img_design/audit__carousel-bg1.svg" alt="audit__helps" title="audit__helps" class="audit__carousel-bg1">
                                    <img src="<?= $template_directory_uri; ?>/out/img_design/auditTwo.svg" alt="audit__helps" title="audit__helps" class="audit__helps-img">
                                    <img src="<?= $template_directory_uri; ?>/out/img_design/audit__carousel-bg2.svg" alt="audit__helps" title="audit__helps" class="audit__carousel-bg2">
                                </div>
                            </div>
                        </div>
                    <?php }?>
                    <?php if(t($metaDesign['crawler_helps_3_title']) !== ''){ ?>
                        <div class="carousel-item">
                            <div class="audit__helps">
                                <div class="audit__helps-text">
                                    <img src="<?= $template_directory_uri; ?>/out/img_design/audit__landing-icon.svg" alt="audit__helps" title="audit__helps" class="audit__helps-img">
                                    <p class="audit__helps-title"><?=t($metaDesign['crawler_helps_3_title']);?></p>
                                    <p class="audit__helps-descr"><?=t($metaDesign['crawler_helps_3_description']);?></p>
                                </div>
                                <div class="audit__helps__img">
                                    <img src="<?= $template_directory_uri; ?>/out/img_design/audit__carousel-bg1.svg" alt="audit__helps" title="audit__helps" class="audit__carousel-bg1">
                                    <img src="<?= $template_directory_uri; ?>/out/img_design/auditThree.svg" class="audit__helps-img">
                                    <img src="<?= $template_directory_uri; ?>/out/img_design/audit__carousel-bg2.svg" alt="audit__helps" title="audit__helps" class="audit__carousel-bg2">
                                </div>
                            </div>
                        </div>
                    <?php }?>
                    <?php if(t($metaDesign['crawler_helps_4_title']) !== ''){ ?>
                        <div class="carousel-item">
                            <div class="audit__helps">
                                <div class="audit__helps-text">
                                    <img src="<?= $template_directory_uri; ?>/out/img_design/audit__landing-icon.svg" alt="audit__helps" title="audit__helps" class="audit__helps-img">
                                    <p class="audit__helps-title"><?=t($metaDesign['crawler_helps_4_title']);?></p>
                                    <p class="audit__helps-descr"><?=t($metaDesign['crawler_helps_4_description']);?></p>
                                </div>
                                <div class="audit__helps__img">
                                    <img src="<?= $template_directory_uri; ?>/out/img_design/audit__carousel-bg1.svg" alt="audit__helps" title="audit__helps" class="audit__carousel-bg1">
                                    <img src="<?= $template_directory_uri; ?>/out/img_design/auditFour.svg" class="audit__helps-img">
                                    <img src="<?= $template_directory_uri; ?>/out/img_design/audit__carousel-bg2.svg" alt="audit__helps" title="audit__helps" class="audit__carousel-bg2">
                                </div>
                            </div>
                        </div>
                    <?php }?>
                    <?php if(t($metaDesign['crawler_helps_5_title']) !== ''){ ?>
                        <div class="carousel-item">
                            <div class="audit__helps">
                                <div class="audit__helps-text">
                                    <img src="<?= $template_directory_uri; ?>/out/img_design/audit__landing-icon.svg" alt="audit__helps" title="audit__helps" class="audit__helps-img">
                                    <p class="audit__helps-title"><?=t($metaDesign['crawler_helps_5_title']);?></p>
                                    <p class="audit__helps-descr"><?=t($metaDesign['crawler_helps_5_description']);?></p>
                                </div>
                                <div class="audit__helps__img">
                                    <img src="<?= $template_directory_uri; ?>/out/img_design/audit__carousel-bg1.svg" alt="audit__helps" title="audit__helps" class="audit__carousel-bg1">
                                    <img src="<?= $template_directory_uri; ?>/out/img_design/auditFive.svg" class="audit__helps-img">
                                    <img src="<?= $template_directory_uri; ?>/out/img_design/audit__carousel-bg2.svg" alt="audit__helps" title="audit__helps" class="audit__carousel-bg2">
                                </div>
                            </div>
                        </div>
                    <?php }?>
                    <?php if(t($metaDesign['crawler_helps_6_title']) !== ''){ ?>
                        <div class="carousel-item">
                            <div class="audit__helps">
                                <div class="audit__helps-text">
                                    <img src="<?= $template_directory_uri; ?>/out/img_design/audit__landing-icon.svg" alt="audit__helps" title="audit__helps" class="audit__helps-img">
                                    <p class="audit__helps-title"><?=t($metaDesign['crawler_helps_6_title']);?></p>
                                    <p class="audit__helps-descr"><?=t($metaDesign['crawler_helps_6_description']);?></p>
                                </div>
                                <div class="audit__helps__img">
                                    <img src="<?= $template_directory_uri; ?>/out/img_design/audit__carousel-bg1.svg" alt="audit__helps" title="audit__helps" class="audit__carousel-bg1">
                                    <img src="<?= $template_directory_uri; ?>/out/img_design/auditSix.svg" class="audit__helps-img">
                                    <img src="<?= $template_directory_uri; ?>/out/img_design/audit__carousel-bg2.svg" alt="audit__helps" title="audit__helps" class="audit__carousel-bg2">
                                </div>
                            </div>
                        </div>
                    <?php }?>
                    <?php if(t($metaDesign['crawler_helps_7_title']) !== ''){ ?>
                        <div class="carousel-item">
                            <div class="audit__helps">
                                <div class="audit__helps-text">
                                    <img src="<?= $template_directory_uri; ?>/out/img_design/audit__landing-icon.svg" alt="audit__helps" title="audit__helps" class="audit__helps-img">
                                    <p class="audit__helps-title"><?=t($metaDesign['crawler_helps_7_title']);?></p>
                                    <p class="audit__helps-descr"><?=t($metaDesign['crawler_helps_7_description']);?></p>
                                </div>
                                <div class="audit__helps__img">
                                    <img src="<?= $template_directory_uri; ?>/out/img_design/audit__carousel-bg1.svg" alt="audit__helps" title="audit__helps" class="audit__carousel-bg1">
                                    <img src="<?= $template_directory_uri; ?>/out/img_design/auditSeven.svg" class="audit__helps-img">
                                    <img src="<?= $template_directory_uri; ?>/out/img_design/audit__carousel-bg2.svg" alt="audit__helps" title="audit__helps" class="audit__carousel-bg2">
                                </div>
                            </div>
                        </div>
                    <?php }?>
                    <?php if(t($metaDesign['crawler_helps_8_title']) !== ''){ ?>
                        <div class="carousel-item">
                            <div class="audit__helps">
                                <div class="audit__helps-text">
                                    <img src="<?= $template_directory_uri; ?>/out/img_design/audit__landing-icon.svg" alt="audit__helps" title="audit__helps" class="audit__helps-img">
                                    <p class="audit__helps-title"><?=t($metaDesign['crawler_helps_8_title']);?></p>
                                    <p class="audit__helps-descr"><?=t($metaDesign['crawler_helps_8_description']);?></p>
                                </div>
                                <div class="audit__helps__img">
                                    <img src="<?= $template_directory_uri; ?>/out/img_design/audit__carousel-bg1.svg" alt="audit__helps" title="audit__helps" class="audit__carousel-bg1">
                                    <img src="<?= $template_directory_uri; ?>/out/img_design/auditEight.svg" class="audit__helps-img">
                                    <img src="<?= $template_directory_uri; ?>/out/img_design/audit__carousel-bg2.svg" alt="audit__helps" title="audit__helps" class="audit__carousel-bg2">
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                </div>
                <ol class="carousel-indicators">
                    <li data-target="#audit__carousel" data-slide-to="0" class="active"></li>
                    <?php if(t($metaDesign['crawler_helps_2_title']) !== ''){ ?>
                        <li data-target="#audit__carousel" data-slide-to="1"></li>
                    <?php }?>
                    <?php if(t($metaDesign['crawler_helps_3_title']) !== ''){ ?>
                        <li data-target="#audit__carousel" data-slide-to="2"></li>
                    <?php }?>
                    <?php if(t($metaDesign['crawler_helps_4_title']) !== ''){ ?>
                        <li data-target="#audit__carousel" data-slide-to="3"></li>
                    <?php }?>
                    <?php if(t($metaDesign['crawler_helps_5_title']) !== ''){ ?>
                        <li data-target="#audit__carousel" data-slide-to="4"></li>
                    <?php }?>
                    <?php if(t($metaDesign['crawler_helps_6_title']) !== ''){ ?>
                        <li data-target="#audit__carousel" data-slide-to="5"></li>
                    <?php }?>
                    <?php if(t($metaDesign['crawler_helps_7_title']) !== ''){ ?>
                        <li data-target="#audit__carousel" data-slide-to="6"></li>
                    <?php }?>
                    <?php if(t($metaDesign['crawler_helps_8_title']) !== ''){ ?>
                        <li data-target="#audit__carousel" data-slide-to="7"></li>
                    <?php }?>
                </ol>
                <a class="carousel-control-prev" href="#audit__carousel" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                </a>
                <a class="carousel-control-next" href="#audit__carousel" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                </a>
            </div>
        </div>
    </div>
    <?php }?>
    <?php if(t($metaDesign['how_to_use_step_1']) !== ''){ ?>
        <div class="audit__five">
        <div class="wrapper">
            <p class="audit__title"><?=t($metaDesign['how_to_use']);?></p>
            <div class="audit__five-container">
                <div class="auditLanding-accordion" role="tablist" id="auditLanding-accordion">
                    <div class="panel panel-default active">
                        <div class="panel-heading" role="tab" id="auditLandingOne">
                            <div class="panel-title">
                                <h4 data-toggle="collapse" data-parent="#auditLanding-accordion" href="#collapseauditLandingOne" aria-expanded="true" aria-controls="collapseauditLandingOne"><?=t($metaDesign['how_to_use_step_1']);?></h4>
                            </div>
                        </div>
                        <div id="collapseauditLandingOne" class="panel-collapse collapse show" role="tabpanel" aria-labelledby="auditLandingOne">
                            <div class="panel-body">
                                <p class="title"><?=t($metaDesign['how_to_use_step_1_title']);?></p>
                                <p class="description"><?=t($metaDesign['how_to_use_step_1_description']);?></p>
                            </div>
                        </div>
                    </div>
                    <?php if(t($metaDesign['how_to_use_step_2']) !== ''){ ?>
                        <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="auditLandingTwo">
                            <div class="panel-title">
                                <h4 data-toggle="collapse" data-parent="#auditLanding-accordion" href="#collapseauditLandingTwo" aria-expanded="false" aria-controls="collapseauditLandingTwo"><?=t($metaDesign['how_to_use_step_2']);?></h4>
                            </div>
                        </div>
                        <div id="collapseauditLandingTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="auditLandingTwo">
                            <div class="panel-body">
                                <p class="title"><?=t($metaDesign['how_to_use_step_2_title']);?></p>
                                <p class="description"><?=t($metaDesign['how_to_use_step_2_description']);?></p>
                            </div>
                        </div>
                    </div>
                    <?php }?>
                    <?php if(t($metaDesign['how_to_use_step_3']) !== ''){ ?>
                        <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="auditLandingThree">
                            <div class="panel-title">
                                <h4 data-toggle="collapse" data-parent="#auditLanding-accordion" href="#collapseauditLandingThree" aria-expanded="false" aria-controls="collapseauditLandingThree"><?=t($metaDesign['how_to_use_step_3']);?></h4>
                            </div>
                        </div>
                        <div id="collapseauditLandingThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="auditLandingThree">
                            <div class="panel-body">
                                <p class="title"><?=t($metaDesign['how_to_use_step_3_title']);?></p>
                                <p class="description"><?=t($metaDesign['how_to_use_step_3_description']);?></p>
                            </div>
                        </div>
                    </div>
                    <?php }?>
                    <?php if(t($metaDesign['how_to_use_step_4']) !== ''){ ?>
                        <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="auditLandingFour">
                            <div class="panel-title">
                                <h4 data-toggle="collapse" data-parent="#auditLanding-accordion" href="#collapseauditLandingFour" aria-expanded="false" aria-controls="collapseauditLandingFour"><?=t($metaDesign['how_to_use_step_4']);?></h4>
                            </div>
                        </div>
                        <div id="collapseauditLandingFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="auditLandingFour">
                            <div class="panel-body">
                                <p class="title"><?=t($metaDesign['how_to_use_step_4_title']);?></p>
                                <p class="description"><?=t($metaDesign['how_to_use_step_4_description']);?></p>
                            </div>
                        </div>
                    </div>
                    <?php }?>
                    <?php if(t($metaDesign['how_to_use_step_5']) !== ''){ ?>
                        <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="auditLandingFive">
                            <div class="panel-title">
                                <h4 data-toggle="collapse" data-parent="#auditLanding-accordion" href="#collapseauditLandingFive" aria-expanded="false" aria-controls="collapseauditLandingFive"><?=t($metaDesign['how_to_use_step_5']);?></h4>
                            </div>
                        </div>
                        <div id="collapseauditLandingFive" class="panel-collapse collapse" role="tabpanel" aria-labelledby="auditLandingFive">
                            <div class="panel-body">
                                <p class="title"><?=t($metaDesign['how_to_use_step_5_title']);?></p>
                                <p class="description"><?=t($metaDesign['how_to_use_step_5_description']);?></p>
                            </div>
                        </div>
                    </div>
                    <?php }?>
                    <?php if(t($metaDesign['how_to_use_step_6']) !== ''){ ?>
                        <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="auditLandingSix">
                            <div class="panel-title">
                                <h4 data-toggle="collapse" data-parent="#auditLanding-accordion" href="#collapseauditLandingSix" aria-expanded="false" aria-controls="collapseauditLandingSix"><?=t($metaDesign['how_to_use_step_6']);?></h4>
                            </div>
                        </div>
                        <div id="collapseauditLandingSix" class="panel-collapse collapse" role="tabpanel" aria-labelledby="auditLandingSix">
                            <div class="panel-body">
                                <p class="title"><?=t($metaDesign['how_to_use_step_6_title']);?></p>
                                <p class="description"><?=t($metaDesign['how_to_use_step_6_description']);?></p>
                            </div>
                        </div>
                    </div>
                    <?php }?>
                </div>
                <div class="audit__five-images">
                    <img src="<?= $template_directory_uri; ?>/out/img_design/audit_landing-illustration.svg" alt="arrow" title="arrow" class="auditLandingOne">
                </div>
            </div>
        </div>
    </div>
    <?php }?>
    <div class="front__check audit__six">
        <div class="wrapper">
            <img src="<?= $template_directory_uri; ?>/out/img_design/logo__white.svg" alt="logo__white" title="logo__white" class="front__check-logo">
            <p class="title"><?=t($metaDesign['health_checker_banner']);?></p>
            <form class="article__seo-search audit__form">
                <span class="error">Must be a valid URL with http:// or https://</span>
                <div class="error__limits">No limits! <a href="/plans/">Upgrade your account</a> to crawl this domain</div>
                <input type="text" placeholder="<?=t($metaDesign['health_checker_placeholder']);?>">
                <button type="submit" class="<?php echo $crawlButton; ?>"><img src="<?= $template_directory_uri; ?>/out/img_design/arrow.svg" alt="arrow" title="arrow" class="arrow"><img src="<?= $template_directory_uri; ?>/out/img_design/loading.svg" alt="arrow" title="arrow" class="loading"></button>
            </form>
        </div>
    </div>
    <?php if(t($metaDesign['how_it_works']) !== ''){ ?>
        <div class="audit__seven">
        <div class="wrapper">
            <p class="audit__title"><?=t($metaDesign['how_it_works']);?></p>
            <div class="audit__seven-container">
                <div class="audit__hiw">
                    <img src="<?= $template_directory_uri; ?>/out/img_design/audit__hiw-icon1.svg" alt="how it works" title="how it works">
                    <p class="title"><?=t($metaDesign['how_it_works_1_title']);?></p>
                    <p class="description"><?=t($metaDesign['how_it_works_1_description']);?></p>
                </div>
                <div class="audit__hiw">
                    <img src="<?= $template_directory_uri; ?>/out/img_design/audit__hiw-icon2.svg" alt="how it works" title="how it works">
                    <p class="title"><?=t($metaDesign['how_it_works_2_title']);?></p>
                    <p class="description"><?=t($metaDesign['how_it_works_2_description']);?></p>
                </div>
                <div class="audit__hiw">
                    <img src="<?= $template_directory_uri; ?>/out/img_design/audit__hiw-icon3.svg" alt="how it works" title="how it works">
                    <p class="title"><?=t($metaDesign['how_it_works_3_title']);?></p>
                    <p class="description"><?=t($metaDesign['how_it_works_3_description']);?></p>
                </div>
            </div>
        </div>
    </div>
    <?php }?>
    <?php if(t($metaDesign['crawler_difference_title']) !== ''){ ?>
        <div class="audit__eight">
        <div class="wrapper">
            <p class="audit__eight-title">
                <?=t($metaDesign['crawler_difference_title']);?>
            </p>
            <p class="audit__eight-descript"><?=t($metaDesign['crawler_difference_description']);?></p>
            <div class="audit__eight-ill">
                <div class="audit__diff">
                    <img src="<?= $template_directory_uri; ?>/out/img_design/audit__diff1.svg" alt="audit" title="audit">
                    <p class="title"><?=t($metaDesign['crawler_difference_1_title']);?></p>
                    <p class="description"><?=t($metaDesign['crawler_difference_1_description']);?></p>
                </div>
                <div class="audit__diff">
                    <img src="<?= $template_directory_uri; ?>/out/img_design/audit__diff2.svg" alt="audit" title="audit">
                    <p class="title"><?=t($metaDesign['crawler_difference_2_title']);?></p>
                    <p class="description"><?=t($metaDesign['crawler_difference_2_description']);?></p>
                </div>
            </div>
        </div>
    </div>
    <?php }?>
    <div class="audit__nine">
        <div class="wrapper">
            <p class="audit__title"><?=t($metaDesign['stats_title']);?></p>
            <div class="audit__nine-container">
                <div class="audit__stat">
                    <p class="number" id="audit__stat1">7352</p>
                    <p class="description"><?=t($metaDesign['stats_text_1']);?></p>
                </div>
                <div class="audit__stat">
                    <p class="number" id="audit__stat2">2317931</p>
                    <p class="description"><?=t($metaDesign['stats_text_2']);?></p>
                </div>
                <div class="audit__stat">
                    <p class="number" id="audit__stat3">3064534</p>
                    <p class="description"><?=t($metaDesign['stats_text_3']);?></p>
                </div>
            </div>
        </div>
    </div>
    <?php if(t($metaDesign['health_checker_banner_title']) !== ''){ ?>
        <div class="audit__eleven">
        <div class="wrapper">
            <div class="audit__eleven-form">
                <div class="left">
                    <img src="<?= $template_directory_uri; ?>/out/img_design/audit__banner-img.svg" alt="audit" title="audit">
                </div>
                <div class="right">
                    <p class="audit__eleven-title"><?=t($metaDesign['health_checker_banner_title']);?></p>
                    <ul class="audit__eleven-list">
                        <?=t($metaDesign['health_checker_banner_list']);?>
                    </ul>
                    <form class="article__seo-search audit__form audit__form_monitoring">
                        <span class="error">Must be a valid URL with http:// or https://</span>
                        <div class="error__limits">No limits! <a href="/plans/">Upgrade your account</a> to start monitoring the website</div>
                        <input type="text" placeholder="<?=t($metaDesign['health_checker_banner_placeholder']);?>">
                        <button type="submit" class="<?php echo $monitoringButton; ?>"><img src="<?= $template_directory_uri; ?>/out/img_design/arrow.svg" alt="arrow" title="arrow" class="arrow"><img src="<?= $template_directory_uri; ?>/out/img_design/loading.svg" alt="arrow" title="arrow" class="loading"></button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <?php }?>
    <div class="stars__block">
        <?php if(function_exists('the_ratings')) { the_ratings(); } ?>
    </div>
</main>