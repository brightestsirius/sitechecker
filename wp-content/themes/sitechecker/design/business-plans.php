<?php
/*
	Template Name: Business Plans
*/
get_header('tmp_design');
?>
<?php

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $_REQUEST['level'] = $_POST['level_id'];
    require(ABSPATH . "/wp-content/plugins/paid-memberships-pro/preheaders/checkout.php");
}

$template_directory_uri = get_template_directory_uri();
$meta = get_post_meta_all(get_option('page_on_front'));
$metaDesign = get_post_meta_all(url_to_postid( '/pricing-design/' ));
$metaPlans = get_post_meta_all('833');


$pmpro_levels = pmpro_getAllLevels();
$customerMembershipLevel = $current_user->membership_level->id;
$meta = get_post_meta_all(get_option('page_on_front'));

global $gateway, $pmpro_review, $skip_account_fields, $pmpro_paypal_token, $wpdb, $current_user, $pmpro_msg, $pmpro_msgt, $pmpro_requirebilling, $pmpro_level, $pmpro_levels, $tospage, $pmpro_currency_symbol, $pmpro_show_discount_code;
global $discount_code, $username, $password, $password2, $bfirstname, $blastname, $baddress1, $baddress2, $bcity, $bstate, $bzipcode, $bcountry, $bphone, $bemail, $bconfirmemail, $CardType, $AccountNumber, $ExpirationMonth,$ExpirationYear;

?>

<main class="pricing">
    <div class="wrapper">
        <div class="banner__limit-domains free__limit-banner pdf_banner">
            <p>PDF only for Beginner, Basic or Pro level</p>
            <img src="<?= $template_directory_uri; ?>/out/img_design/close.svg" alt="close" title="close" class="pdf_banner-close">
        </div>
        <div class="banner__limit-domains free__limit-banner pdf_banner-report">
            <p>PDF only for Pro level</p>
            <img src="<?= $template_directory_uri; ?>/out/img_design/close.svg" alt="close" title="close" class="pdf_banner-close">
        </div>
        <h1><?=t($metaPlans['plans_title']);?></h1>
        <h2><?=t($metaPlans['plans_description']);?></h2>
        <label class="switch pricing__switch" id="level_checkout">
            <span class="pricing__switch-label"><?=t($metaDesign['month_free']);?></span>
            <input type="checkbox">
            <span class="slider round">
                <span><?=t($metaDesign['monthly']);?></span>
                <span><?=t($metaDesign['annual']);?></span>
            </span>
        </label>
    </div>

    <div class="pricing__content seo__platform">
        <div class="wrapper">
            <div class="pricing__labels">
                <div class="pricing__labels-block">
                    <p class="label pricing__label-platform">
                        <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                             viewBox="0 0 486.742 486.742" style="enable-background:new 0 0 486.742 486.742;" xml:space="preserve">
<g>
    <g>
        <path d="M33,362.371v78.9c0,4.8,3.9,8.8,8.8,8.8h61c4.8,0,8.8-3.9,8.8-8.8v-138.8l-44.3,44.3
			C57.9,356.071,45.9,361.471,33,362.371z"/>
        <path d="M142,301.471v139.8c0,4.8,3.9,8.8,8.8,8.8h61c4.8,0,8.8-3.9,8.8-8.8v-82.3c-13.9-0.3-26.9-5.8-36.7-15.6L142,301.471z"/>
        <path d="M251,350.271v91c0,4.8,3.9,8.8,8.8,8.8h61c4.8,0,8.8-3.9,8.8-8.8v-167.9l-69.9,69.9
			C257,345.971,254.1,348.271,251,350.271z"/>
        <path d="M432.7,170.171l-72.7,72.7v198.4c0,4.8,3.9,8.8,8.8,8.8h61c4.8,0,8.8-3.9,8.8-8.8v-265.6c-2-1.7-3.5-3.2-4.6-4.2
			L432.7,170.171z"/>
        <path d="M482.6,41.371c-2.9-3.1-7.3-4.7-12.9-4.7c-0.5,0-1.1,0-1.6,0c-28.4,1.3-56.7,2.7-85.1,4c-3.8,0.2-9,0.4-13.1,4.5
			c-1.3,1.3-2.3,2.8-3.1,4.6c-4.2,9.1,1.7,15,4.5,17.8l7.1,7.2c4.9,5,9.9,10,14.9,14.9l-171.6,171.7l-77.1-77.1
			c-4.6-4.6-10.8-7.2-17.4-7.2c-6.6,0-12.7,2.6-17.3,7.2L7.2,286.871c-9.6,9.6-9.6,25.1,0,34.7l4.6,4.6c4.6,4.6,10.8,7.2,17.4,7.2
			s12.7-2.6,17.3-7.2l80.7-80.7l77.1,77.1c4.6,4.6,10.8,7.2,17.4,7.2c6.6,0,12.7-2.6,17.4-7.2l193.6-193.6l21.9,21.8
			c2.6,2.6,6.2,6.2,11.7,6.2c2.3,0,4.6-0.6,7-1.9c1.6-0.9,3-1.9,4.2-3.1c4.3-4.3,5.1-9.8,5.3-14.1c0.8-18.4,1.7-36.8,2.6-55.3
			l1.3-27.7C487,49.071,485.7,44.571,482.6,41.371z"/>
    </g>
</svg>SEO Platform</p>
                    <p># of websites</p>
                    <p>max URLs per website</p>
                    <p>Sharing by link / email</p>
                    <p>Google Analytics integration</p>
                    <p>Google Search Console integration</p>
                </div>
                <div class="pricing__labels-block">
                    <p class="label">
                        Rank Tracker
                    </p>
                    <p># of tracked keywords per 1 domain</p>
                    <p>Update frequency</p>
                </div>
                <div class="pricing__labels-block">
                    <p class="label">
                        Backlink Tracker
                    </p>
                    <p># of tracked backlinks per 1 domain</p>
                    <p>Update frequency</p>
                </div>
                <div class="pricing__labels-block">
                    <p class="label pricing__label-audit">
                        <img src="<?= $template_directory_uri; ?>/out/img_design/audit__icon-active.svg"
                             alt="products" title="products">
                        <?=t($metaDesign['website_seo_audit']);?>
                    </p>
                    <p># of websites</p>
                    <p>max URLs per website</p>
                    <p>How to fix guides</p>
                    <p>Sitemap generator</p>
                    <p>Internal PageRank calculation</p>
                    <p>Silo structure visualization</p>
                    <p>Branded PDF reports</p>
                    <p>Crawling customization</p>
                </div>
                <div class="pricing__labels-block">
                    <p class="label pricing__label-monitoring">
                        <img src="<?= $template_directory_uri; ?>/out/img_design/monitoring__icon-active.svg"
                             alt="products" title="products">
                        <?=t($metaDesign['website_seo_monitoring']);?>
                    </p>
                    <p>Monitoring frequency</p>
                    <p>Monitoring customization</p>
                    <p>Activity history for each page</p>
                </div>
                <div class="pricing__labels-block">
                    <p class="label pricing__label-onpage">
                        <img src="<?= $template_directory_uri; ?>/out/img_design/check__icon-active.svg"
                             alt="products" title="products">
                        <?=t($metaDesign['on_page_seo_audit']);?>
                    </p>
                    <p># of On Page SEO Audits</p>
                    <p>How to fix guides</p>
                    <p>Branded PDF reports</p>
                    <p>Chrome extension</p>
                    <p>Mozilla extension</p>
                    <p>On Page SEO Report iFrame</p>
                </div>
            </div>
            <div class="levels_block-side">
                <div class="card">
                    <?php
                    foreach ($pmpro_levels as $level) {
                        if ($level->id == 12 || $level->id == 51) {
                            $_REQUEST['level'] = $level->id;
                            require(ABSPATH . "/wp-content/plugins/paid-memberships-pro/preheaders/checkout.php");
                            if($level->id == 12){
                                $levelClass = 'monthly';
                            }elseif ($level->id == 51){
                                $levelClass = 'yearly';
                            }
                            ?>
                            <form class="pmpro_form pricing__level <?php echo $levelClass; ?>"  action="" method="post">
                                <div class="pricing__level-price">
                                    <p class="name">Startup</p>
                                    <?php if($level->id == 12){
                                        echo '<p class="price">$<span>99</span></p><p class="description">per month</p>';
                                    }elseif ($level->id == 51){
                                        echo '<p class="price">$<span>495</span></p><p class="description">per 6-months</p>';
                                    }
                                    ?>
                                </div>
                                <div class="pricing__level-btn">
                                    <p class="name">For small business and SEO beginners</p>
                                    <div class="button">
                                        <input type="hidden" name="level_id" value="<?php echo $level->id?>" />
                                        <input type="hidden" name="submit-checkout" value="<?php echo $level->id?>" />
                                        <?php if($gateway == "paypalexpress" && $pmpro_requirebilling) { ?>
                                            <input type="submit" class="pmpro_btn-submit-checkout btn <?php if($customerMembershipLevel == $level->id){echo 'your__level';}?>" value="<?php echo $customerMembershipLevel == $level->id ? 'Your level' : 'Subscribe' ?>" name="<?php echo $level->id?>">
                                        <?php } else { ?>
                                            <span id="pmpro_submit_span"><input type="submit" class="pmpro_btn-submit-checkout btn <?php if($customerMembershipLevel == $level->id){echo 'your__level';}?>" value="<?php echo $customerMembershipLevel == $level->id ? 'Your level' : 'Subscribe' ?>" /></span>
                                        <?php } ?>
                                    </div>
                                </div>
                                <div class="pricing__level-values">
                                    <p class="label valuesDescriptionMobile pricing__label-platform">
                                        <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                             viewBox="0 0 486.742 486.742" style="enable-background:new 0 0 486.742 486.742;" xml:space="preserve">
<g>
    <g>
        <path d="M33,362.371v78.9c0,4.8,3.9,8.8,8.8,8.8h61c4.8,0,8.8-3.9,8.8-8.8v-138.8l-44.3,44.3
			C57.9,356.071,45.9,361.471,33,362.371z"/>
        <path d="M142,301.471v139.8c0,4.8,3.9,8.8,8.8,8.8h61c4.8,0,8.8-3.9,8.8-8.8v-82.3c-13.9-0.3-26.9-5.8-36.7-15.6L142,301.471z"/>
        <path d="M251,350.271v91c0,4.8,3.9,8.8,8.8,8.8h61c4.8,0,8.8-3.9,8.8-8.8v-167.9l-69.9,69.9
			C257,345.971,254.1,348.271,251,350.271z"/>
        <path d="M432.7,170.171l-72.7,72.7v198.4c0,4.8,3.9,8.8,8.8,8.8h61c4.8,0,8.8-3.9,8.8-8.8v-265.6c-2-1.7-3.5-3.2-4.6-4.2
			L432.7,170.171z"/>
        <path d="M482.6,41.371c-2.9-3.1-7.3-4.7-12.9-4.7c-0.5,0-1.1,0-1.6,0c-28.4,1.3-56.7,2.7-85.1,4c-3.8,0.2-9,0.4-13.1,4.5
			c-1.3,1.3-2.3,2.8-3.1,4.6c-4.2,9.1,1.7,15,4.5,17.8l7.1,7.2c4.9,5,9.9,10,14.9,14.9l-171.6,171.7l-77.1-77.1
			c-4.6-4.6-10.8-7.2-17.4-7.2c-6.6,0-12.7,2.6-17.3,7.2L7.2,286.871c-9.6,9.6-9.6,25.1,0,34.7l4.6,4.6c4.6,4.6,10.8,7.2,17.4,7.2
			s12.7-2.6,17.3-7.2l80.7-80.7l77.1,77.1c4.6,4.6,10.8,7.2,17.4,7.2c6.6,0,12.7-2.6,17.4-7.2l193.6-193.6l21.9,21.8
			c2.6,2.6,6.2,6.2,11.7,6.2c2.3,0,4.6-0.6,7-1.9c1.6-0.9,3-1.9,4.2-3.1c4.3-4.3,5.1-9.8,5.3-14.1c0.8-18.4,1.7-36.8,2.6-55.3
			l1.3-27.7C487,49.071,485.7,44.571,482.6,41.371z"/>
    </g>
</svg>SEO Platform</p>
                                    <div><p class="valuesDescriptionMobile"># of websites: </p><p>5</p></div>
                                    <div><p class="valuesDescriptionMobile">max URLs per website: </p><p>Unlimited</p></div>
                                    <div><p class="valuesDescriptionMobile">Sharing by link / email: </p><img src="<?= $template_directory_uri; ?>/out/img_design/pricing__exist.svg" alt="pricing" title="pricing"></div>
                                    <div><p class="valuesDescriptionMobile">Google Analytics integration: </p><img src="<?= $template_directory_uri; ?>/out/img_design/pricing__exist.svg" alt="pricing" title="pricing"></div>
                                    <div><p class="valuesDescriptionMobile">Google Search Console integration: </p><img src="<?= $template_directory_uri; ?>/out/img_design/pricing__exist.svg" alt="pricing" title="pricing"></div>
                                </div>
                                <div class="pricing__level-values">
                                    <p class="label valuesDescriptionMobile">
                                        Rank Tracker
                                    </p>
                                    <div><p class="valuesDescriptionMobile"># of tracked keywords per 1 domain: </p><p>500</p></div>
                                    <div><p class="valuesDescriptionMobile">Update frequency: </p><p>every day</p></div>
                                </div>
                                <div class="pricing__level-values">
                                    <p class="label valuesDescriptionMobile">
                                        Backlink Tracker
                                    </p>
                                    <div><p class="valuesDescriptionMobile"># of tracked backlinks per 1 domain: </p><p>1000</p></div>
                                    <div><p class="valuesDescriptionMobile">Update frequency: </p><p>7 days</p></div>
                                </div>
                                <div class="pricing__level-values">
                                    <p class="label valuesDescriptionMobile pricing__label-audit">
                                        <img src="<?= $template_directory_uri; ?>/out/img_design/audit__icon-active.svg"
                                             alt="products" title="products">
                                        <?=t($metaDesign['website_seo_audit']);?>
                                    </p>
                                    <div><p class="valuesDescriptionMobile"># of websites: </p><p>100</p></div>
                                    <div><p class="valuesDescriptionMobile">max URLs per website: </p><p>10.000</p></div>
                                    <div><p class="valuesDescriptionMobile">How to fix guides: </p><img src="<?= $template_directory_uri; ?>/out/img_design/pricing__exist.svg" alt="pricing" title="pricing"></div>
                                    <div><p class="valuesDescriptionMobile">Sitemap generator: </p><img src="<?= $template_directory_uri; ?>/out/img_design/pricing__exist.svg" alt="pricing" title="pricing"></div>
                                    <div><p class="valuesDescriptionMobile">Internal PageRank calculation: </p><img src="<?= $template_directory_uri; ?>/out/img_design/pricing__exist.svg" alt="pricing" title="pricing"></div>
                                    <div><p class="valuesDescriptionMobile">Silo structure visualization: </p><img src="<?= $template_directory_uri; ?>/out/img_design/pricing__exist.svg" alt="pricing" title="pricing"></div>
                                    <div><p class="valuesDescriptionMobile">Branded PDF reports: </p><img src="<?= $template_directory_uri; ?>/out/img_design/pricing__exist.svg" alt="pricing" title="pricing"></div>
                                    <div><p class="valuesDescriptionMobile">Crawling customization: </p><img src="<?= $template_directory_uri; ?>/out/img_design/pricing__exist.svg" alt="pricing" title="pricing"></div>
                                </div>
                                <div class="pricing__level-values">
                                    <p class="label valuesDescriptionMobile pricing__label-monitoring">
                                        <img src="<?= $template_directory_uri; ?>/out/img_design/monitoring__icon-active.svg"
                                             alt="products" title="products">
                                        <?=t($metaDesign['website_seo_monitoring']);?>
                                    </p>
                                    <div><p class="valuesDescriptionMobile">Monitoring frequency: </p><p>Hourly</p></div>
                                    <div><p class="valuesDescriptionMobile">Monitoring customization: </p><img src="<?= $template_directory_uri; ?>/out/img_design/pricing__exist.svg" alt="pricing" title="pricing"></div>
                                    <div><p class="valuesDescriptionMobile">Activity history for each page: </p><img src="<?= $template_directory_uri; ?>/out/img_design/pricing__exist.svg" alt="pricing" title="pricing"></div>
                                </div>
                                <div class="pricing__level-values">
                                    <p class="label valuesDescriptionMobile pricing__label-onpage">
                                        <img src="<?= $template_directory_uri; ?>/out/img_design/check__icon-active.svg"
                                             alt="products" title="products">
                                        <?=t($metaDesign['on_page_seo_audit']);?>
                                    </p>
                                    <div><p class="valuesDescriptionMobile"># of On Page SEO Audits: </p><p>Unlimited</p></div>
                                    <div><p class="valuesDescriptionMobile">How to fix guides: </p><img src="<?= $template_directory_uri; ?>/out/img_design/pricing__exist.svg" alt="pricing" title="pricing"></div>
                                    <div><p class="valuesDescriptionMobile">Branded PDF reports: </p><img src="<?= $template_directory_uri; ?>/out/img_design/pricing__exist.svg" alt="pricing" title="pricing"></div>
                                    <div><p class="valuesDescriptionMobile">Chrome extension: </p><img src="<?= $template_directory_uri; ?>/out/img_design/pricing__exist.svg" alt="pricing" title="pricing"></div>
                                    <div><p class="valuesDescriptionMobile">Mozilla extension: </p><img src="<?= $template_directory_uri; ?>/out/img_design/pricing__exist.svg" alt="pricing" title="pricing"></div>
                                    <div><p class="valuesDescriptionMobile">On Page SEO Report iFrame: </p><img src="<?= $template_directory_uri; ?>/out/img_design/pricing__exist.svg" alt="pricing" title="pricing"></div>
                                </div>
                                <div class="pricing__level-submit">
                                    <div class="button">
                                        <input type="hidden" name="level_id" value="<?php echo $level->id?>" />
                                        <input type="hidden" name="submit-checkout" value="<?php echo $level->id?>" />
                                        <?php if($gateway == "paypalexpress" && $pmpro_requirebilling) { ?>
                                            <input type="submit" class="pmpro_btn-submit-checkout btn <?php if($customerMembershipLevel == $level->id){echo 'your__level';}?>" value="<?php echo $customerMembershipLevel == $level->id ? 'Your level' : 'Subscribe' ?>" name="<?php echo $level->id?>">
                                        <?php } else { ?>
                                            <span id="pmpro_submit_span"><input type="submit" class="pmpro_btn-submit-checkout btn <?php if($customerMembershipLevel == $level->id){echo 'your__level';}?>" value="<?php echo $customerMembershipLevel == $level->id ? 'Your level' : 'Subscribe' ?>" /></span>
                                        <?php } ?>
                                    </div>
                                </div>
                            </form>
                        <?php }
                    } ?>
                </div>
            </div>
            <div class="levels_block-side">
                <div class="card">
                    <?php
                    foreach ($pmpro_levels as $level) {
                        if ($level->id == 13 || $level->id == 52) {
                            $_REQUEST['level'] = $level->id;
                            require(ABSPATH . "/wp-content/plugins/paid-memberships-pro/preheaders/checkout.php");
                            if($level->id == 13){
                                $levelClass = 'monthly';
                            }elseif ($level->id == 52){
                                $levelClass = 'yearly';
                            }
                            ?>
                            <form class="pmpro_form pricing__level <?php echo $levelClass; ?>"  action="" method="post">
                                <div class="pricing__level-price">
                                    <div class="name">Business <div class="popular"><img src="<?= $template_directory_uri; ?>/out/img_design/price__star.svg" alt="pricing" title="pricing">Best Value</div></div>
                                    <?php if($level->id == 13){
                                        echo '<p class="price">$<span>459</span></p><p class="description">per month</p>';
                                    }elseif ($level->id == 52){
                                        echo '<p class="price">$<span>2295</span></p><p class="description">per 6-months</p>';
                                    }
                                    ?>
                                </div>
                                <div class="pricing__level-btn">
                                    <p class="name">For experienced webmasters and growing agencies</p>
                                    <div class="button">
                                        <input type="hidden" name="level_id" value="<?php echo $level->id?>" />
                                        <input type="hidden" name="submit-checkout" value="<?php echo $level->id?>" />
                                        <?php if($gateway == "paypalexpress" && $pmpro_requirebilling) { ?>
                                            <input type="submit" class="pmpro_btn-submit-checkout btn popular <?php if($customerMembershipLevel == $level->id){echo 'your__level';}?>" value="<?php echo $customerMembershipLevel == $level->id ? 'Your level' : 'Subscribe' ?>" name="<?php echo $level->id?>">
                                        <?php } else { ?>
                                            <span id="pmpro_submit_span"><input type="submit" class="pmpro_btn-submit-checkout btn popular <?php if($customerMembershipLevel == $level->id){echo 'your__level';}?>" value="<?php echo $customerMembershipLevel == $level->id ? 'Your level' : 'Subscribe' ?>" /></span>
                                        <?php } ?>
                                    </div>
                                </div>
                                <div class="pricing__level-values">
                                    <p class="label valuesDescriptionMobile pricing__label-platform">
                                        <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                             viewBox="0 0 486.742 486.742" style="enable-background:new 0 0 486.742 486.742;" xml:space="preserve">
<g>
    <g>
        <path d="M33,362.371v78.9c0,4.8,3.9,8.8,8.8,8.8h61c4.8,0,8.8-3.9,8.8-8.8v-138.8l-44.3,44.3
			C57.9,356.071,45.9,361.471,33,362.371z"/>
        <path d="M142,301.471v139.8c0,4.8,3.9,8.8,8.8,8.8h61c4.8,0,8.8-3.9,8.8-8.8v-82.3c-13.9-0.3-26.9-5.8-36.7-15.6L142,301.471z"/>
        <path d="M251,350.271v91c0,4.8,3.9,8.8,8.8,8.8h61c4.8,0,8.8-3.9,8.8-8.8v-167.9l-69.9,69.9
			C257,345.971,254.1,348.271,251,350.271z"/>
        <path d="M432.7,170.171l-72.7,72.7v198.4c0,4.8,3.9,8.8,8.8,8.8h61c4.8,0,8.8-3.9,8.8-8.8v-265.6c-2-1.7-3.5-3.2-4.6-4.2
			L432.7,170.171z"/>
        <path d="M482.6,41.371c-2.9-3.1-7.3-4.7-12.9-4.7c-0.5,0-1.1,0-1.6,0c-28.4,1.3-56.7,2.7-85.1,4c-3.8,0.2-9,0.4-13.1,4.5
			c-1.3,1.3-2.3,2.8-3.1,4.6c-4.2,9.1,1.7,15,4.5,17.8l7.1,7.2c4.9,5,9.9,10,14.9,14.9l-171.6,171.7l-77.1-77.1
			c-4.6-4.6-10.8-7.2-17.4-7.2c-6.6,0-12.7,2.6-17.3,7.2L7.2,286.871c-9.6,9.6-9.6,25.1,0,34.7l4.6,4.6c4.6,4.6,10.8,7.2,17.4,7.2
			s12.7-2.6,17.3-7.2l80.7-80.7l77.1,77.1c4.6,4.6,10.8,7.2,17.4,7.2c6.6,0,12.7-2.6,17.4-7.2l193.6-193.6l21.9,21.8
			c2.6,2.6,6.2,6.2,11.7,6.2c2.3,0,4.6-0.6,7-1.9c1.6-0.9,3-1.9,4.2-3.1c4.3-4.3,5.1-9.8,5.3-14.1c0.8-18.4,1.7-36.8,2.6-55.3
			l1.3-27.7C487,49.071,485.7,44.571,482.6,41.371z"/>
    </g>
</svg>SEO Platform</p>
                                    <div><p class="valuesDescriptionMobile"># of websites: </p><p>25</p></div>
                                    <div><p class="valuesDescriptionMobile">max URLs per website: </p><p>Unlimited</p></div>
                                    <div><p class="valuesDescriptionMobile">Sharing by link / email: </p><img src="<?= $template_directory_uri; ?>/out/img_design/pricing__exist.svg" alt="pricing" title="pricing"></div>
                                    <div><p class="valuesDescriptionMobile">Google Analytics integration: </p><img src="<?= $template_directory_uri; ?>/out/img_design/pricing__exist.svg" alt="pricing" title="pricing"></div>
                                    <div><p class="valuesDescriptionMobile">Google Search Console integration: </p><img src="<?= $template_directory_uri; ?>/out/img_design/pricing__exist.svg" alt="pricing" title="pricing"></div>
                                </div>
                                <div class="pricing__level-values">
                                    <p class="label valuesDescriptionMobile">
                                        Rank Tracker
                                    </p>
                                    <div><p class="valuesDescriptionMobile"># of tracked keywords per 1 domain: </p><p>500</p></div>
                                    <div><p class="valuesDescriptionMobile">Update frequency: </p><p>every day</p></div>
                                </div>
                                <div class="pricing__level-values">
                                    <p class="label valuesDescriptionMobile">
                                        Backlink Tracker
                                    </p>
                                    <div><p class="valuesDescriptionMobile"># of tracked backlinks per 1 domain: </p><p>1000</p></div>
                                    <div><p class="valuesDescriptionMobile">Update frequency: </p><p>7 days</p></div>
                                </div>
                                <div class="pricing__level-values">
                                    <p class="label valuesDescriptionMobile pricing__label-audit">
                                        <img src="<?= $template_directory_uri; ?>/out/img_design/audit__icon-active.svg"
                                             alt="products" title="products">
                                        <?=t($metaDesign['website_seo_audit']);?>
                                    </p>
                                    <div><p class="valuesDescriptionMobile"># of websites: </p><p>100</p></div>
                                    <div><p class="valuesDescriptionMobile">max URLs per website: </p><p>10.000</p></div>
                                    <div><p class="valuesDescriptionMobile">How to fix guides: </p><img src="<?= $template_directory_uri; ?>/out/img_design/pricing__exist.svg" alt="pricing" title="pricing"></div>
                                    <div><p class="valuesDescriptionMobile">Sitemap generator: </p><img src="<?= $template_directory_uri; ?>/out/img_design/pricing__exist.svg" alt="pricing" title="pricing"></div>
                                    <div><p class="valuesDescriptionMobile">Internal PageRank calculation: </p><img src="<?= $template_directory_uri; ?>/out/img_design/pricing__exist.svg" alt="pricing" title="pricing"></div>
                                    <div><p class="valuesDescriptionMobile">Silo structure visualization: </p><img src="<?= $template_directory_uri; ?>/out/img_design/pricing__exist.svg" alt="pricing" title="pricing"></div>
                                    <div><p class="valuesDescriptionMobile">Branded PDF reports: </p><img src="<?= $template_directory_uri; ?>/out/img_design/pricing__exist.svg" alt="pricing" title="pricing"></div>
                                    <div><p class="valuesDescriptionMobile">Crawling customization: </p><img src="<?= $template_directory_uri; ?>/out/img_design/pricing__exist.svg" alt="pricing" title="pricing"></div>
                                </div>
                                <div class="pricing__level-values">
                                    <p class="label valuesDescriptionMobile pricing__label-monitoring">
                                        <img src="<?= $template_directory_uri; ?>/out/img_design/monitoring__icon-active.svg"
                                             alt="products" title="products">
                                        <?=t($metaDesign['website_seo_monitoring']);?>
                                    </p>
                                    <div><p class="valuesDescriptionMobile">Monitoring frequency: </p><p>Hourly</p></div>
                                    <div><p class="valuesDescriptionMobile">Monitoring customization: </p><img src="<?= $template_directory_uri; ?>/out/img_design/pricing__exist.svg" alt="pricing" title="pricing"></div>
                                    <div><p class="valuesDescriptionMobile">Activity history for each page: </p><img src="<?= $template_directory_uri; ?>/out/img_design/pricing__exist.svg" alt="pricing" title="pricing"></div>
                                </div>
                                <div class="pricing__level-values">
                                    <p class="label valuesDescriptionMobile pricing__label-onpage">
                                        <img src="<?= $template_directory_uri; ?>/out/img_design/check__icon-active.svg"
                                             alt="products" title="products">
                                        <?=t($metaDesign['on_page_seo_audit']);?>
                                    </p>
                                    <div><p class="valuesDescriptionMobile"># of On Page SEO Audits: </p><p>Unlimited</p></div>
                                    <div><p class="valuesDescriptionMobile">How to fix guides: </p><img src="<?= $template_directory_uri; ?>/out/img_design/pricing__exist.svg" alt="pricing" title="pricing"></div>
                                    <div><p class="valuesDescriptionMobile">Branded PDF reports: </p><img src="<?= $template_directory_uri; ?>/out/img_design/pricing__exist.svg" alt="pricing" title="pricing"></div>
                                    <div><p class="valuesDescriptionMobile">Chrome extension: </p><img src="<?= $template_directory_uri; ?>/out/img_design/pricing__exist.svg" alt="pricing" title="pricing"></div>
                                    <div><p class="valuesDescriptionMobile">Mozilla extension: </p><img src="<?= $template_directory_uri; ?>/out/img_design/pricing__exist.svg" alt="pricing" title="pricing"></div>
                                    <div><p class="valuesDescriptionMobile">On Page SEO Report iFrame: </p><img src="<?= $template_directory_uri; ?>/out/img_design/pricing__exist.svg" alt="pricing" title="pricing"></div>
                                </div>
                                <div class="pricing__level-submit">
                                    <div class="button">
                                        <input type="hidden" name="level_id" value="<?php echo $level->id?>" />
                                        <input type="hidden" name="submit-checkout" value="<?php echo $level->id?>" />
                                        <?php if($gateway == "paypalexpress" && $pmpro_requirebilling) { ?>
                                            <input type="submit" class="pmpro_btn-submit-checkout btn popular <?php if($customerMembershipLevel == $level->id){echo 'your__level';}?>" value="<?php echo $customerMembershipLevel == $level->id ? 'Your level' : 'Subscribe' ?>" name="<?php echo $level->id?>">
                                        <?php } else { ?>
                                            <span id="pmpro_submit_span"><input type="submit" class="pmpro_btn-submit-checkout btn popular <?php if($customerMembershipLevel == $level->id){echo 'your__level';}?>" value="<?php echo $customerMembershipLevel == $level->id ? 'Your level' : 'Subscribe' ?>" /></span>
                                        <?php } ?>
                                    </div>
                                </div>
                            </form>
                        <?php }
                    } ?>
                </div>
            </div>
            <div class="levels_block-side">
                <div class="card">
                    <?php
                    foreach ($pmpro_levels as $level) {
                        if ($level->id == 14 || $level->id == 53) {
                            $_REQUEST['level'] = $level->id;
                            require(ABSPATH . "/wp-content/plugins/paid-memberships-pro/preheaders/checkout.php");
                            if($level->id == 14){
                                $levelClass = 'monthly';
                            }elseif ($level->id == 53){
                                $levelClass = 'yearly';
                            }
                            ?>
                            <form class="pmpro_form pricing__level <?php echo $levelClass; ?>"  action="" method="post">
                                <div class="pricing__level-price">
                                    <p class="name">Enterprise</p>
                                    <?php if($level->id == 14){
                                        echo '<p class="price">$<span>1299</span></p><p class="description">per month</p>';
                                    }elseif ($level->id == 53){
                                        echo '<p class="price">$<span>6495</span></p><p class="description">per 6-months</p>';
                                    }
                                    ?>
                                </div>
                                <div class="pricing__level-btn">
                                    <p class="name">For agencies and large websites</p>
                                    <div class="button">
                                        <input type="hidden" name="level_id" value="<?php echo $level->id?>" />
                                        <input type="hidden" name="submit-checkout" value="<?php echo $level->id?>" />
                                        <?php if($gateway == "paypalexpress" && $pmpro_requirebilling) { ?>
                                            <input type="submit" class="pmpro_btn-submit-checkout btn <?php if($customerMembershipLevel == $level->id){echo 'your__level';}?>" value="<?php echo $customerMembershipLevel == $level->id ? 'Your level' : 'Subscribe' ?>" name="<?php echo $level->id?>">
                                        <?php } else { ?>
                                            <span id="pmpro_submit_span"><input type="submit" class="pmpro_btn-submit-checkout btn <?php if($customerMembershipLevel == $level->id){echo 'your__level';}?>" value="<?php echo $customerMembershipLevel == $level->id ? 'Your level' : 'Subscribe' ?>" /></span>
                                        <?php } ?>
                                    </div>
                                </div>
                                <div class="pricing__level-values">
                                    <p class="label valuesDescriptionMobile pricing__label-platform">
                                        <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                             viewBox="0 0 486.742 486.742" style="enable-background:new 0 0 486.742 486.742;" xml:space="preserve">
<g>
    <g>
        <path d="M33,362.371v78.9c0,4.8,3.9,8.8,8.8,8.8h61c4.8,0,8.8-3.9,8.8-8.8v-138.8l-44.3,44.3
			C57.9,356.071,45.9,361.471,33,362.371z"/>
        <path d="M142,301.471v139.8c0,4.8,3.9,8.8,8.8,8.8h61c4.8,0,8.8-3.9,8.8-8.8v-82.3c-13.9-0.3-26.9-5.8-36.7-15.6L142,301.471z"/>
        <path d="M251,350.271v91c0,4.8,3.9,8.8,8.8,8.8h61c4.8,0,8.8-3.9,8.8-8.8v-167.9l-69.9,69.9
			C257,345.971,254.1,348.271,251,350.271z"/>
        <path d="M432.7,170.171l-72.7,72.7v198.4c0,4.8,3.9,8.8,8.8,8.8h61c4.8,0,8.8-3.9,8.8-8.8v-265.6c-2-1.7-3.5-3.2-4.6-4.2
			L432.7,170.171z"/>
        <path d="M482.6,41.371c-2.9-3.1-7.3-4.7-12.9-4.7c-0.5,0-1.1,0-1.6,0c-28.4,1.3-56.7,2.7-85.1,4c-3.8,0.2-9,0.4-13.1,4.5
			c-1.3,1.3-2.3,2.8-3.1,4.6c-4.2,9.1,1.7,15,4.5,17.8l7.1,7.2c4.9,5,9.9,10,14.9,14.9l-171.6,171.7l-77.1-77.1
			c-4.6-4.6-10.8-7.2-17.4-7.2c-6.6,0-12.7,2.6-17.3,7.2L7.2,286.871c-9.6,9.6-9.6,25.1,0,34.7l4.6,4.6c4.6,4.6,10.8,7.2,17.4,7.2
			s12.7-2.6,17.3-7.2l80.7-80.7l77.1,77.1c4.6,4.6,10.8,7.2,17.4,7.2c6.6,0,12.7-2.6,17.4-7.2l193.6-193.6l21.9,21.8
			c2.6,2.6,6.2,6.2,11.7,6.2c2.3,0,4.6-0.6,7-1.9c1.6-0.9,3-1.9,4.2-3.1c4.3-4.3,5.1-9.8,5.3-14.1c0.8-18.4,1.7-36.8,2.6-55.3
			l1.3-27.7C487,49.071,485.7,44.571,482.6,41.371z"/>
    </g>
</svg>SEO Platform</p>
                                    <div><p class="valuesDescriptionMobile"># of websites: </p><p>100</p></div>
                                    <div><p class="valuesDescriptionMobile">max URLs per website: </p><p>Unlimited</p></div>
                                    <div><p class="valuesDescriptionMobile">Sharing by link / email: </p><img src="<?= $template_directory_uri; ?>/out/img_design/pricing__exist.svg" alt="pricing" title="pricing"></div>
                                    <div><p class="valuesDescriptionMobile">Google Analytics integration: </p><img src="<?= $template_directory_uri; ?>/out/img_design/pricing__exist.svg" alt="pricing" title="pricing"></div>
                                    <div><p class="valuesDescriptionMobile">Google Search Console integration: </p><img src="<?= $template_directory_uri; ?>/out/img_design/pricing__exist.svg" alt="pricing" title="pricing"></div>
                                </div>
                                <div class="pricing__level-values">
                                    <p class="label valuesDescriptionMobile">
                                        Rank Tracker
                                    </p>
                                    <div><p class="valuesDescriptionMobile"># of tracked keywords per 1 domain: </p><p>500</p></div>
                                    <div><p class="valuesDescriptionMobile">Update frequency: </p><p>every day</p></div>
                                </div>
                                <div class="pricing__level-values">
                                    <p class="label valuesDescriptionMobile">
                                        Backlink Tracker
                                    </p>
                                    <div><p class="valuesDescriptionMobile"># of tracked backlinks per 1 domain: </p><p>1000</p></div>
                                    <div><p class="valuesDescriptionMobile">Update frequency: </p><p>7 days</p></div>
                                </div>
                                <div class="pricing__level-values">
                                    <p class="label valuesDescriptionMobile pricing__label-audit">
                                        <img src="<?= $template_directory_uri; ?>/out/img_design/audit__icon-active.svg"
                                             alt="products" title="products">
                                        <?=t($metaDesign['website_seo_audit']);?>
                                    </p>
                                    <div><p class="valuesDescriptionMobile"># of websites: </p><p>100</p></div>
                                    <div><p class="valuesDescriptionMobile">max URLs per website: </p><p>10.000</p></div>
                                    <div><p class="valuesDescriptionMobile">How to fix guides: </p><img src="<?= $template_directory_uri; ?>/out/img_design/pricing__exist.svg" alt="pricing" title="pricing"></div>
                                    <div><p class="valuesDescriptionMobile">Sitemap generator: </p><img src="<?= $template_directory_uri; ?>/out/img_design/pricing__exist.svg" alt="pricing" title="pricing"></div>
                                    <div><p class="valuesDescriptionMobile">Internal PageRank calculation: </p><img src="<?= $template_directory_uri; ?>/out/img_design/pricing__exist.svg" alt="pricing" title="pricing"></div>
                                    <div><p class="valuesDescriptionMobile">Silo structure visualization: </p><img src="<?= $template_directory_uri; ?>/out/img_design/pricing__exist.svg" alt="pricing" title="pricing"></div>
                                    <div><p class="valuesDescriptionMobile">Branded PDF reports: </p><img src="<?= $template_directory_uri; ?>/out/img_design/pricing__exist.svg" alt="pricing" title="pricing"></div>
                                    <div><p class="valuesDescriptionMobile">Crawling customization: </p><img src="<?= $template_directory_uri; ?>/out/img_design/pricing__exist.svg" alt="pricing" title="pricing"></div>
                                </div>
                                <div class="pricing__level-values">
                                    <p class="label valuesDescriptionMobile pricing__label-monitoring">
                                        <img src="<?= $template_directory_uri; ?>/out/img_design/monitoring__icon-active.svg"
                                             alt="products" title="products">
                                        <?=t($metaDesign['website_seo_monitoring']);?>
                                    </p>
                                    <div><p class="valuesDescriptionMobile">Monitoring frequency: </p><p>Hourly</p></div>
                                    <div><p class="valuesDescriptionMobile">Monitoring customization: </p><img src="<?= $template_directory_uri; ?>/out/img_design/pricing__exist.svg" alt="pricing" title="pricing"></div>
                                    <div><p class="valuesDescriptionMobile">Activity history for each page: </p><img src="<?= $template_directory_uri; ?>/out/img_design/pricing__exist.svg" alt="pricing" title="pricing"></div>
                                </div>
                                <div class="pricing__level-values">
                                    <p class="label valuesDescriptionMobile pricing__label-onpage">
                                        <img src="<?= $template_directory_uri; ?>/out/img_design/check__icon-active.svg"
                                             alt="products" title="products">
                                        <?=t($metaDesign['on_page_seo_audit']);?>
                                    </p>
                                    <div><p class="valuesDescriptionMobile"># of On Page SEO Audits: </p><p>Unlimited</p></div>
                                    <div><p class="valuesDescriptionMobile">How to fix guides: </p><img src="<?= $template_directory_uri; ?>/out/img_design/pricing__exist.svg" alt="pricing" title="pricing"></div>
                                    <div><p class="valuesDescriptionMobile">Branded PDF reports: </p><img src="<?= $template_directory_uri; ?>/out/img_design/pricing__exist.svg" alt="pricing" title="pricing"></div>
                                    <div><p class="valuesDescriptionMobile">Chrome extension: </p><img src="<?= $template_directory_uri; ?>/out/img_design/pricing__exist.svg" alt="pricing" title="pricing"></div>
                                    <div><p class="valuesDescriptionMobile">Mozilla extension: </p><img src="<?= $template_directory_uri; ?>/out/img_design/pricing__exist.svg" alt="pricing" title="pricing"></div>
                                    <div><p class="valuesDescriptionMobile">On Page SEO Report iFrame: </p><img src="<?= $template_directory_uri; ?>/out/img_design/pricing__exist.svg" alt="pricing" title="pricing"></div>
                                </div>
                                <div class="pricing__level-submit">
                                    <div class="button">
                                        <input type="hidden" name="level_id" value="<?php echo $level->id?>" />
                                        <input type="hidden" name="submit-checkout" value="<?php echo $level->id?>" />
                                        <?php if($gateway == "paypalexpress" && $pmpro_requirebilling) { ?>
                                            <input type="submit" class="pmpro_btn-submit-checkout btn <?php if($customerMembershipLevel == $level->id){echo 'your__level';}?>" value="<?php echo $customerMembershipLevel == $level->id ? 'Your level' : 'Subscribe' ?>" name="<?php echo $level->id?>">
                                        <?php } else { ?>
                                            <span id="pmpro_submit_span"><input type="submit" class="pmpro_btn-submit-checkout btn <?php if($customerMembershipLevel == $level->id){echo 'your__level';}?>" value="<?php echo $customerMembershipLevel == $level->id ? 'Your level' : 'Subscribe' ?>" /></span>
                                        <?php } ?>
                                    </div>
                                </div>
                            </form>
                        <?php }
                    } ?>
                </div>
            </div>
        </div>
    </div>
    <div class="pricing__testimonials">
        <div class="wrapper">
            <div class="testimonials__pricing">
                <?=t($metaPlans['testimonials_pricing']);?>
            </div>
        </div>
    </div>
    <div class="front__faq">
        <div class="wrapper">
            <p class="title"><?=t($metaPlans['faq']);?></p>
            <p class="description"><?=t($metaDesign['pricing_faq_description']);?></p>
            <div class="front__faq-block">
                <div class="panel-group" id="faq-accordion" role="tablist" aria-multiselectable="true">
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingFaqOne">
                            <div class="panel-title" data-toggle="collapse" data-parent="#faq-accordion" href="#collapseFaqOne" aria-expanded="false" aria-controls="collapseFaqOne">
                                <h3><?=t($metaPlans['faq_title_1']);?></h3>
                                <div class="faq__arrow">
                                    <img src="<?= $template_directory_uri; ?>/out/img_design/arrow__down-blue.svg" alt="arrow" title="arrow">
                                </div>
                            </div>
                        </div>
                        <div id="collapseFaqOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFaqOne">
                            <div class="panel-body"><?=t($metaPlans['faq_text_1']);?></div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingFaqTwo">
                            <div class="panel-title" data-toggle="collapse" data-parent="#faq-accordion" href="#collapseFaqTwo" aria-expanded="false" aria-controls="collapseFaqTwo">
                                <h3><?=t($metaPlans['faq_title_2']);?></h3>
                                <div class="faq__arrow">
                                    <img src="<?= $template_directory_uri; ?>/out/img_design/arrow__down-blue.svg" alt="arrow" title="arrow">
                                </div>
                            </div>
                        </div>
                        <div id="collapseFaqTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFaqTwo">
                            <div class="panel-body"><?=t($metaPlans['faq_text_2']);?></div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingFaqThree">
                            <div class="panel-title" data-toggle="collapse" data-parent="#faq-accordion" href="#collapseFaqThree" aria-expanded="false" aria-controls="collapseFaqThree">
                                <h3><?=t($metaPlans['faq_title_3']);?></h3>
                                <div class="faq__arrow">
                                    <img src="<?= $template_directory_uri; ?>/out/img_design/arrow__down-blue.svg" alt="arrow" title="arrow">
                                </div>
                            </div>
                        </div>
                        <div id="collapseFaqThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFaqThree">
                            <div class="panel-body"><?=t($metaPlans['faq_text_3']);?></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>

<div class="paypal_redirect">
    <p class="title">Thank you for registration!</p>
    <p>We are redirecting you to PayPal</p>
    <div class="spinWrap">
        <p class="spinnerImage"></p>
        <p class="loader"></p>
    </div>
</div>

<?php get_footer('tmp_design');?>