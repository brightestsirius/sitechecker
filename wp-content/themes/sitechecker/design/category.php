<?php
$meta = get_post_meta_all(get_option('page_on_front'));
$template_directory_uri = get_template_directory_uri();
?>
<main>
    <div class="main knowledgeBase">
        <div class="wrapper">
            <div class="navigation">
                <a href="<?=home_url();?>/" class="active">Home</a>
                <svg width="9" height="5" viewBox="0 0 9 5" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path fill-rule="evenodd" clip-rule="evenodd" d="M4.499 0L9 2.5V4.896L4.499 2.38L0 5V2.5L4.499 0Z" transform="translate(9 5) rotate(-180)" fill="#CCCCCC"/>
                </svg>
                <a href="/knowledge-base/" class="active"><?=t($meta['user.header.knowledge.base']);?></a>
                <svg width="9" height="5" viewBox="0 0 9 5" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path fill-rule="evenodd" clip-rule="evenodd" d="M4.499 0L9 2.5V4.896L4.499 2.38L0 5V2.5L4.499 0Z" transform="translate(9 5) rotate(-180)" fill="#CCCCCC"/>
                </svg>
                <p><?php echo single_tag_title('', false);?></p>
            </div>
            <h1 class="title category"><?php _e( 'Category: ', 'html5blank' ); echo single_tag_title('', false);?></h1>
            <div class="article__more-container">
                <?php
                $paged = get_query_var( 'paged' ) ? absint( get_query_var( 'paged' ) ) : 1;
                $args = array(
                    'posts_per_page' => 12,
                    'cat' => get_queried_object()->term_id,
                    'post_type' => array('page','post'),
                    'hide_empty' => 1,
                    'paged' => $paged,
                );
                $query = new WP_Query($args);
                ?>
                <?php if ( $query->have_posts() ) : ?>
                    <?php while ( $query->have_posts() ) : $query->the_post(); ?>
                        <div class="article__more-block">
                            <div class="img__block" style="background-image: url(<?php the_post_thumbnail_url(); ?>)"></div>
                            <a href="<?php the_permalink(); ?>" class="title"><?php echo get_post_meta( get_the_ID(), 'page.h1', true); ?></a>
                            <span class="info__block">
                                <div class="watches">
                                    <img src="<?= $template_directory_uri; ?>/out/img_design/article__watch.svg" alt="watches"
                                         title="watches">
                                    <span><?php setPostViews(get_the_ID()); echo getPostViews(get_the_ID());?></span>
                                </div>
                                <a href="" class="tag"><?php echo single_tag_title('', false);?></a>
                            </span>
                        </div>
                    <?php endwhile; ?>
                <?php else : ?>
                    <p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
                <?php endif; wp_reset_query(); ?>
            </div>
            <div class="pagination">
                <?php
                echo paginate_links( array(
                    'base'         => str_replace( 999999999, '%#%', esc_url( get_pagenum_link( 999999999 ) ) ),
                    'total'        => $query->max_num_pages,
                    'current'      => max( 1, get_query_var( 'paged' ) ),
                    'format'       => '?paged=%#%',
                    'show_all'     => false,
                    'type'         => 'plain',
                    'end_size'     => 2,
                    'mid_size'     => 1,
                    'prev_next'    => true,
                    'prev_text'    => sprintf( '<i class="fa fa-chevron-left"></i> %1$s', __('') ),
                    'next_text'    => sprintf( '%1$s <i class="fa fa-chevron-right"></i>', __('') ),
                    'add_args'     => false,
                    'add_fragment' => '',
                ) );
                ?>
            </div>
        </div>
    </div>
</main>