<?php
$meta = get_post_meta_all(get_option('page_on_front'));
$metaCategory = get_post_meta_all(url_to_postid( '/knowledge-base/' ));
$template_directory_uri = get_template_directory_uri();
?>
<main>
    <div class="main knowledgeBase">
        <div class="wrapper">
            <div class="navigation">
                <a href="<?=home_url();?>/" class="active">Home</a>
                <svg width="9" height="5" viewBox="0 0 9 5" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path fill-rule="evenodd" clip-rule="evenodd" d="M4.499 0L9 2.5V4.896L4.499 2.38L0 5V2.5L4.499 0Z" transform="translate(9 5) rotate(-180)" fill="#CCCCCC"/>
                </svg>
                <a href="/knowledge-base/" class="active"><?=t($meta['user.header.knowledge.base']);?></a>
                <svg width="9" height="5" viewBox="0 0 9 5" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path fill-rule="evenodd" clip-rule="evenodd" d="M4.499 0L9 2.5V4.896L4.499 2.38L0 5V2.5L4.499 0Z" transform="translate(9 5) rotate(-180)" fill="#CCCCCC"/>
                </svg>
                <p><?php echo single_tag_title('', false); ?></p>
            </div>
            <p class="cat__title"><?=t($metaCategory['category']);?></p>
            <div class="cat__name">
                <img src="<?= $template_directory_uri; ?>/out/img_design/cat_<?php echo get_queried_object()->term_id;?>_blue.svg" alt="category">
                <h1><?php echo single_tag_title('', false);?></h1>
            </div>
            <div class="cat__filters" id="<?php echo get_queried_object()->term_id;?>">
                <form class="search" method="get" action="<?php echo home_url(); ?>" role="search">
                    <svg width="22" height="22" viewBox="0 0 22 22" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M21.6654 20.0406L16.2402 14.5922C17.6332 12.9937 18.3987 10.9785 18.3987 8.88161C18.3987 3.98317 14.273 -3.05176e-05 9.19934 -3.05176e-05C4.12569 -3.05176e-05 0 3.98317 0 8.88161C0 13.78 4.12569 17.7633 9.19934 17.7633C11.1042 17.7633 12.92 17.209 14.4688 16.1562L19.9341 21.6433C20.1611 21.8711 20.4682 22 20.7975 22C21.1091 22 21.4028 21.884 21.6298 21.6777C22.1105 21.2351 22.1238 20.5004 21.6654 20.0406ZM9.19934 2.31599C12.9512 2.31599 15.9998 5.25934 15.9998 8.88161C15.9998 12.5039 12.9512 15.4472 9.19934 15.4472C5.44751 15.4472 2.39886 12.5039 2.39886 8.88161C2.39886 5.25934 5.45196 2.31599 9.19934 2.31599Z" fill="white"/>
                    </svg>
                    <input class="search-input" type="search" name="s" placeholder="<?=t($metaCategory['placeholder']);?>">
                </form>
                <div>
                    <label for="latest">
                        <input type="checkbox" id="latest" checked>
                        <i class="fa" aria-hidden="true"></i>
                        <?=t($metaCategory['the_latest']);?>
                    </label>
                </div>
                <div>
                    <label for="viewed">
                        <input type="checkbox" id="viewed">
                        <i class="fa" aria-hidden="true"></i>
                        <?=t($metaCategory['the_most_viewed']);?>
                    </label>
                </div>
                <div>
                    <label for="rated">
                        <input type="checkbox" id="rated">
                        <i class="fa" aria-hidden="true"></i>
                        <?=t($metaCategory['the_most_rated']);?>
                    </label>
                </div>
            </div>
            <div class="cat__container">
                <div class="cat__result">
                    <?php
                    $paged = get_query_var( 'paged' ) ? absint( get_query_var( 'paged' ) ) : 1;
                    $args = array(
                        'posts_per_page' => 10,
                        'cat' => get_queried_object()->term_id,
                        'post_type' => array('page','post'),
                        'hide_empty' => 1,
                        'paged' => $paged,
                    );
                    $query = new WP_Query($args);
                    ?>
                    <?php if ( $query->have_posts() ) : ?>
                        <?php while ( $query->have_posts() ) : $query->the_post(); ?>
                            <div class="cat__prev">
                                <div class="image" style="background-image: url(<?php the_post_thumbnail_url(); ?>)"></div>
                                <div class="content">
                                    <a href="<?php the_permalink(); ?>" class="title"><?php echo get_post_meta( get_the_ID(), 'page.h1', true); ?></a>
                                    <div class="data">
                                        <div class="article__info">
                                            <div class="date">
                                                <span><?php echo get_the_date('D, M j, Y'); ?></span>
                                            </div>
                                            <div class="watches">
                                                <svg width="22" height="16" viewBox="0 0 22 16" fill="none"
                                                     xmlns="http://www.w3.org/2000/svg">
                                                    <path fill-rule="evenodd" clip-rule="evenodd"
                                                          d="M10.999 4.5C9.339 4.5 8 5.84 8 7.5C8 9.161 9.339 10.501 10.999 10.501C12.66 10.501 14 9.161 14 7.5C14 5.84 12.66 4.5 10.999 4.5ZM10.999 12.5C8.241 12.5 6 10.26 6 7.5C6 4.74 8.241 2.5 10.999 2.5C13.76 2.5 16 4.74 16 7.5C16 10.26 13.76 12.5 10.999 12.5ZM10.999 0C6 0 1.73 3.11 0 7.5C1.73 11.89 6 15 10.999 15C16 15 20.271 11.89 22 7.5C20.271 3.11 16 0 10.999 0Z"
                                                          transform="translate(0 0.5)" fill="#E6E6E6"/>
                                                </svg>
                                                <span><?php echo getPostViews(get_the_ID()); ?></span>
                                            </div>
                                            <div class="stars">
                                                <svg width="19" height="17" viewBox="0 0 19 17" fill="none"
                                                     xmlns="http://www.w3.org/2000/svg">
                                                    <path d="M9.50018 0L12.4355 5.59599L19 6.49359L14.2499 10.8492L15.3712 17L9.50018 14.0962L3.62879 17L4.75009 10.8492L0 6.49359L6.56448 5.59599L9.50018 0Z"
                                                          fill="#E6E6E6"/>
                                                </svg>
                                                <div class="post__ratings"><?php echo the_ratings_results(get_the_ID()) ?></div>
                                                <span class="post__ratings-result"></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php endwhile; endif; wp_reset_query(); ?>
                </div>
                <div class="cat__list">
                    <p class="title"><?=t($metaCategory['all_categories']);?></p>
                    <ul>
                        <?php
                        $categories = get_categories(array(
                            'orderby' => 'name',
                            'order' => 'ASC',
                            'hide_empty' => 1
                        ));
                        foreach( $categories as $category ){
                            $query = new WP_Query($args);
                            $queried_object = get_queried_object();
                            $description = get_field('description',$category);
                            $image = get_field('image',$category);
                            $src = $image ? $image : $template_directory_uri.'/out/img_design/seo_cases.png';
                            if($category->name !== ''){
                            ?>
                            <li><a href="<?php echo get_category_link($category->term_id); ?>"><?php echo $category->name; ?></a></li>
                        <?php }}
                        ?>
                    </ul>
                </div>
            </div>
            <div class="pagination">
                <?php
                echo paginate_links( array(
                    'base'         => str_replace( 999999999, '%#%', esc_url( get_pagenum_link( 999999999 ) ) ),
                    'total'        => $query->max_num_pages,
                    'current'      => max( 1, get_query_var( 'paged' ) ),
                    'format'       => '?paged=%#%',
                    'show_all'     => false,
                    'type'         => 'plain',
                    'end_size'     => 2,
                    'mid_size'     => 1,
                    'prev_next'    => true,
                    'prev_text'    => sprintf( '<i class="fa fa-chevron-left"></i> %1$s', __('') ),
                    'next_text'    => sprintf( '%1$s <i class="fa fa-chevron-right"></i>', __('') ),
                    'add_args'     => false,
                    'add_fragment' => '',
                ) );
                ?>
            </div>
        </div>
    </div>
</main>