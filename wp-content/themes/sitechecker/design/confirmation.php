<?php
/*
	Template Name: Confirmation Page
*/
get_header('tmp_design');
$template_directory_uri = get_template_directory_uri();
?>
<div class="cong__page">
    <div class="wrapper">
        <img src="<?= $template_directory_uri; ?>/out/img_design/congratulations.svg" alt="congratulations" class="cong_img">
        <p class="cong_title">Congratulations!</p>
        <p class="cong_desc">Thank you for staying with us</p>
        <div class="cong_contatiner">
            <div class="cong_block">
                <p class="title">Dashboard</p>
                <p class="description">Add domains, monitor your websites, their score and activity, start audit or monitoring any time</p>
                <a href="/tool/crawl-report/" class="btn">Go to dashboard</a>
            </div>
            <div class="cong_block">
                <p class="title">On-page report</p>
                <p class="description">Go staight to the data we collected for you and start improving your site right away</p>
                <a href="/tool/crawl-report-domain/?id=18633" class="btn">View report</a>
            </div>
            <div class="cong_block">
                <p class="title">Product tour</p>
                <p class="description">Are you a newbie to Sitechecker? Take a quick tour around the most useful tools. Take the most out of your plan!</p>
                <a href="/tool/crawl-report/?product_tour" class="btn">Start the tour</a>
            </div>
        </div>
    </div>
</div>
<?php get_footer('tmp_design'); ?>