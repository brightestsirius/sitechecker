<?php
    $template_directory_uri = get_template_directory_uri();
?>
<main class="contacts__main">
    <div class="wrapper">
        <h1 class="contacts__title">Contact Us</h1>
        <h2 class="contacts__description">If you have questions or just want to get in touch, use the form below. We look forward to hearing from you!</h2>
        <form action="" class="contacts__form">
            <div class="contacts__form-input contacts__form_name">
                <input type="text" class="contacts__form-name" placeholder="Your name">
                <p class="contact__form-error" name="name">Please, enter your name</p>
            </div>
            <div class="contacts__form-input contacts__form_email">
                <input type="email" class="contacts__form-email" placeholder="Your email">
                <p class="contact__form-error" name="email">Please, enter valid email</p>
            </div>
            <textarea class="contacts__form-message" cols="30" rows="10" placeholder="Your message"></textarea>
            <button type="submit" class="btn"><span>send message</span><img src="<?= $template_directory_uri; ?>/out/img_design/loading.svg" alt="arrow" title="arrow" class="loading"></button>
        </form>
    </div>
</main>