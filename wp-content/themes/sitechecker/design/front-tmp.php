<?php $template_directory_uri = get_template_directory_uri();
$meta = get_post_meta_all(get_option('page_on_front'));
$metaDesign = get_post_meta_all(url_to_postid('/front-page-design/'));
$front_second = url_to_postid( '/website-crawler/' );
$front_third = url_to_postid( '/website-monitoring/' );
$front_fourth = url_to_postid( '/seo-platform/' );
$rank_checker = url_to_postid( '/rank-tracker/' );
$backlinks_tracker = url_to_postid( '/backlinks-tracker/' );
$white_label = url_to_postid( '/white-label/' );

$metaCrawlerDesign = get_post_meta_all(url_to_postid( '/website-health-checker-design/' ));
if(is_user_logged_in()){
    $userDomains = getDomainsByUserId(get_current_user_id());
    $availableLimit = getAvailableLimitByUser(get_current_user_id());
    $pagesLimits = $availableLimit->getAvailablePages();
    if($pagesLimits <= 0){
        $crawlButton = 'limits__btn';
    }else{
        $crawlButton = 'sitechecker_crawl';
    }
} else{
    $crawlButton = 'sitechecker_reg';
}

if(is_page($front_second)){
    get_template_part('design/audit_landing');
}else if(is_page($front_third)){
    get_template_part('design/monitoring_landing');
}else if(is_page($front_fourth)){
    get_template_part('design/seo-platform');
}else if(is_page($rank_checker)){
    get_template_part('design/rank_checker');
}else if(is_page($backlinks_tracker)){
    get_template_part('design/backlinks-tracker');
}else if(is_page($white_label)){
    get_template_part('design/white-label');
}else{
?>
<main>
    <div class="front__landing">
        <div class="wrapper">
            <div class="front__landing-text">
                <h1><?=t($meta['main.h1']);?></h1>
                <h2><?=t($meta['main.h2']);?></h2>
                <form class="article__seo-search form__Check" name="seo-report">
                    <span class="error">Must be a valid URL with http:// or https://</span>
                    <input id="sitechecker_input" type="text" placeholder="<?=t($metaDesign['enter_url_for_analysis']);?>">
                    <button type="submit"><img src="<?= $template_directory_uri; ?>/out/img_design/arrow.svg" alt="arrow" title="arrow" class="arrow"><img src="<?= $template_directory_uri; ?>/out/img_design/loading.svg" alt="arrow" title="arrow" class="loading"></button>
                </form>
                <div class="audit__testCase">
                    <form class="article__seo-search audit__form audit__form-blue">
                        <span class="error">Must be a valid URL with http:// or https://</span>
                        <input type="text" placeholder="<?=t($metaCrawlerDesign['health_checker_placeholder']);?>">
                        <button type="submit" class="<?php echo $crawlButton; ?>"><img src="<?= $template_directory_uri; ?>/out/img_design/arrow.svg" alt="arrow" title="arrow" class="arrow"><img src="<?= $template_directory_uri; ?>/out/img_design/loading.svg" alt="arrow" title="arrow" class="loading"></button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="front__tool">
        <div class="wrapper">
            <div class="front__tool-description">
                <svg class="description-img" width="14" height="22" viewBox="0 0 14 22" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M13.4194 11.0403L8.47127 8.03611L10.9658 1.76489C11.0428 1.60111 11.086 1.41656 11.086 1.22222C11.086 0.547556 10.5667 0 9.9227 0C9.63218 0.00488889 9.3895 0.101444 9.19699 0.261556C9.15848 0.293333 9.13515 0.312889 9.11531 0.334889L0.364705 9.01267C0.0975201 9.27789 -0.0343224 9.65922 0.00768055 10.0418C0.0496835 10.4243 0.263198 10.7641 0.580554 10.9584L5.5299 13.9651L3.00622 20.3084C2.79271 20.8413 2.96655 21.4573 3.41925 21.7849C3.61993 21.9291 3.85328 22 4.08196 22C4.37132 22 4.66067 21.8863 4.88469 21.6651L13.6353 12.9849C13.9025 12.7197 14.0343 12.3383 13.9923 11.9558C13.9503 11.5732 13.7368 11.2334 13.4194 11.0403Z" fill="#FF8B29"/></svg>
                <h2 class="description-title"><?=t($metaDesign['seo_audit_title']);?></h2>
                <p class="description-text"><?=t($metaDesign['seo_audit_description']);?></p>
                <div class="description-about" id="check-accordion" role="tablist">
                    <div class="panel panel-default active">
                        <div class="panel-heading" role="tab" id="checkOne">
                            <div class="panel-title">
                                <h3 data-toggle="collapse" data-parent="#check-accordion" href="#collapseCheckOne" aria-expanded="true" aria-controls="collapseCheckOne"><?=t($meta['seo.1.header']);?></h3>
                            </div>
                        </div>
                        <div id="collapseCheckOne" class="panel-collapse collapse show" role="tabpanel" aria-labelledby="checkOne">
                            <div class="panel-body"><?=t($meta['seo.1.content']);?></div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="checkTwo">
                            <div class="panel-title">
                                <h3 class="collapsed" data-toggle="collapse" data-parent="#check-accordion" href="#collapseCheckTwo" aria-expanded="false" aria-controls="collapseCheckTwo"><?=t($meta['seo.2.header']);?></h3>
                            </div>
                        </div>
                        <div id="collapseCheckTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="checkTwo">
                            <div class="panel-body"><?=t($meta['seo.2.content']);?></div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="checkThree">
                            <div class="panel-title">
                                <h3 class="collapsed" data-toggle="collapse" data-parent="#check-accordion" href="#collapseCheckThree" aria-expanded="false" aria-controls="collapseCheckThree"><?=t($meta['seo.3.header']);?></h3>
                            </div>
                        </div>
                        <div id="collapseCheckThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="checkThree">
                            <div class="panel-body"><?=t($meta['seo.3.content']);?></div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="checkFour">
                            <div class="panel-title">
                                <h3 class="collapsed" data-toggle="collapse" data-parent="#check-accordion" href="#collapseCheckFour" aria-expanded="false" aria-controls="collapseCheckFour"><?=t($meta['seo.4.header']);?></h3>
                            </div>
                        </div>
                        <div id="collapseCheckFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="checkFour">
                            <div class="panel-body"><?=t($meta['seo.4.content']);?></div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="checkFive">
                            <div class="panel-title">
                                <h3 class="collapsed" data-toggle="collapse" data-parent="#check-accordion" href="#collapseCheckFive" aria-expanded="false" aria-controls="collapseCheckFive"><?=t($meta['seo.5.header']);?></h3>
                            </div>
                        </div>
                        <div id="collapseCheckFive" class="panel-collapse collapse" role="tabpanel" aria-labelledby="checkFive">
                            <div class="panel-body"><?=t($meta['seo.5.content']);?></div>
                        </div>
                    </div>
                </div>
                <div class="front__tool-action">
                    <a href="/on-page-seo-checker/" class="btn"><?=t($metaDesign['learn_more_btn']);?></a>
                    <a href="/seo-report/https://sitechecker.pro/" class="btn action"><?=t($metaDesign['watch_demo_btn']);?></a>
                </div>
            </div>
            <div class="illustration illustrationCheck">
                <img class="checkOne" alt="arrow" title="arrow">
            </div>
        </div>
    </div>
    <?php if(t($metaDesign['health_checker_1_title']) !== ''){ ?>
        <div class="front__tool front__tool-audit">
        <div class="wrapper">
            <div class="illustration illustrationAudit">
                <img class="auditOne" alt="arrow" title="arrow">
            </div>
            <div class="front__tool-description">
                <svg class="description-img" width="24" height="23" viewBox="0 0 24 23" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M10.2347 8.37864L8.70534 6.88655C9.1759 6.19789 9.41117 5.39446 9.41117 4.59103C9.41117 2.06596 7.29366 0 4.70559 0C2.11751 0 0 2.06596 0 4.59103C0 7.1161 2.11751 9.18207 4.70559 9.18207C5.52906 9.18207 6.35254 8.95251 7.05838 8.49341L8.5877 9.9855C8.82298 10.215 9.52881 10.6742 10.2347 9.9855C10.7052 9.52639 10.7052 8.83774 10.2347 8.37864ZM4.70559 6.88655C3.41155 6.88655 2.35279 5.85357 2.35279 4.59103C2.35279 3.3285 3.41155 2.29552 4.70559 2.29552C5.99962 2.29552 7.05838 3.3285 7.05838 4.59103C7.05838 5.85357 5.99962 6.88655 4.70559 6.88655Z" transform="translate(12.9403 12.6254)" fill="#05CE7C"/>
                    <path d="M17.646 0H1.1764C0.470559 0 0 0.459103 0 1.14776V21.8074C0 22.4961 0.470559 22.9552 1.1764 22.9552H13.7638C11.8816 21.6926 10.5876 19.6267 10.5876 17.2164C10.5876 13.4288 13.7638 10.3298 17.646 10.3298C17.9989 10.3298 18.4694 10.3298 18.8223 10.4446V1.14776C18.8223 0.459103 18.3518 0 17.646 0ZM9.41117 9.18207H3.52919C2.82335 9.18207 2.35279 8.60819 2.35279 8.03431C2.35279 7.34565 2.82335 6.88655 3.52919 6.88655H9.41117C10.117 6.88655 10.5876 7.34565 10.5876 8.03431C10.5876 8.60819 10.117 9.18207 9.41117 9.18207ZM15.2932 4.59103H3.52919C2.82335 4.59103 2.35279 4.01715 2.35279 3.44327C2.35279 2.75462 2.82335 2.29552 3.52919 2.29552H15.2932C15.999 2.29552 16.4696 2.75462 16.4696 3.44327C16.4696 4.01715 15.999 4.59103 15.2932 4.59103Z" fill="#05CE7C"/>
                </svg>
                <h2 class="description-title"><?=t($metaDesign['health_checker_title']);?></h2>
                <p class="description-text"><?=t($metaDesign['health_checker_description']);?></p>
                <div class="description-about" role="tablist" id="audit-accordion">
                        <div class="panel panel-default active">
                        <div class="panel-heading" role="tab" id="auditOne">
                            <div class="panel-title">
                                <h3 data-toggle="collapse" data-parent="#audit-accordion" href="#collapseAuditOne" aria-expanded="true" aria-controls="collapseAuditOne"><?=t($metaDesign['health_checker_1_title']);?></h3>
                            </div>
                        </div>
                        <div id="collapseAuditOne" class="panel-collapse collapse show" role="tabpanel" aria-labelledby="auditOne">
                            <div class="panel-body"><?=t($metaDesign['health_checker_1_description']);?></div>
                        </div>
                    </div>
                    <?php if(t($metaDesign['health_checker_2_title']) !== ''){ ?>
                        <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="auditTwo">
                            <div class="panel-title">
                                <h3 class="collapsed" data-toggle="collapse" data-parent="#audit-accordion" href="#collapseAuditTwo" aria-expanded="false" aria-controls="collapseAuditTwo"><?=t($metaDesign['health_checker_2_title']);?></h3>
                            </div>
                        </div>
                        <div id="collapseAuditTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="auditTwo">
                            <div class="panel-body"><?=t($metaDesign['health_checker_2_description']);?></div>
                        </div>
                    </div>
                    <?php }?>
                    <?php if(t($metaDesign['health_checker_3_title']) !== ''){ ?>
                        <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="auditThree">
                            <div class="panel-title">
                                <h3 class="collapsed" data-toggle="collapse" data-parent="#audit-accordion" href="#collapseAuditThree" aria-expanded="false" aria-controls="collapseAuditThree"><?=t($metaDesign['health_checker_3_title']);?></h3>
                            </div>
                        </div>
                        <div id="collapseAuditThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="auditThree">
                            <div class="panel-body"><?=t($metaDesign['health_checker_3_description']);?></div>
                        </div>
                    </div>
                    <?php } ?>
                    <?php if(t($metaDesign['health_checker_4_title']) !== ''){ ?>
                        <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="auditFour">
                            <div class="panel-title">
                                <h3 class="collapsed" data-toggle="collapse" data-parent="#audit-accordion" href="#collapseAuditFour" aria-expanded="false" aria-controls="collapseAuditFour"><?=t($metaDesign['health_checker_4_title']);?></h3>
                            </div>
                        </div>
                        <div id="collapseAuditFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="auditFour">
                            <div class="panel-body"><?=t($metaDesign['health_checker_4_description']);?></div>
                        </div>
                    </div>
                    <?php }?>
                    <?php if(t($metaDesign['health_checker_5_title']) !== ''){ ?>
                        <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="auditFive">
                            <div class="panel-title">
                                <h3 class="collapsed" data-toggle="collapse" data-parent="#audit-accordion" href="#collapseAuditFive" aria-expanded="false" aria-controls="collapseAuditFive"><?=t($metaDesign['health_checker_5_title']);?></h3>
                            </div>
                        </div>
                        <div id="collapseAuditFive" class="panel-collapse collapse" role="tabpanel" aria-labelledby="auditFive">
                            <div class="panel-body"><?=t($metaDesign['health_checker_5_description']);?></div>
                        </div>
                    </div>
                    <?php }?>
                    <?php if(t($metaDesign['health_checker_6_title']) !== ''){ ?>
                        <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="auditSix">
                            <div class="panel-title">
                                <h3 class="collapsed" data-toggle="collapse" data-parent="#audit-accordion" href="#collapseAuditSix" aria-expanded="false" aria-controls="collapseAuditSix"><?=t($metaDesign['health_checker_6_title']);?></h3>
                            </div>
                        </div>
                        <div id="collapseAuditSix" class="panel-collapse collapse" role="tabpanel" aria-labelledby="auditSix">
                            <div class="panel-body"><?=t($metaDesign['health_checker_6_description']);?></div>
                        </div>
                    </div>
                    <?php }?>
                    <?php if(t($metaDesign['health_checker_7_title']) !== ''){ ?>
                        <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="auditSeven">
                            <div class="panel-title">
                                <h3 class="collapsed" data-toggle="collapse" data-parent="#audit-accordion" href="#collapseAuditSeven" aria-expanded="false" aria-controls="collapseAuditSeven"><?=t($metaDesign['health_checker_7_title']);?></h3>
                            </div>
                        </div>
                        <div id="collapseAuditSeven" class="panel-collapse collapse" role="tabpanel" aria-labelledby="auditSeven">
                            <div class="panel-body"><?=t($metaDesign['health_checker_7_description']);?></div>
                        </div>
                    </div>
                    <?php }?>
                    <?php if(t($metaDesign['health_checker_8_title']) !== ''){ ?>
                        <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="auditEight">
                            <div class="panel-title">
                                <h3 class="collapsed" data-toggle="collapse" data-parent="#audit-accordion" href="#collapseAuditEight" aria-expanded="false" aria-controls="collapseAuditEight"><?=t($metaDesign['health_checker_8_title']);?></h3>
                            </div>
                        </div>
                        <div id="collapseAuditEight" class="panel-collapse collapse" role="tabpanel" aria-labelledby="auditEight">
                            <div class="panel-body"><?=t($metaDesign['health_checker_8_description']);?></div>
                        </div>
                    </div>
                    <?php }?>
                </div>
                <div class="front__tool-action">
                    <a href="/website-crawler/" class="btn"><?=t($metaDesign['learn_more_btn']);?></a>
                    <?php
                    if(is_user_logged_in()){
                        echo '<a href="/crawl-report-domain/?id=16972" class="btn action">'.t($metaDesign['watch_demo_btn']).'</a>';
                    }else{
                        echo '<button class="btn action front__action-audit">'.t($metaDesign['watch_demo_btn']).'</button>';
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>
    <?php } ?>
    <?php if(t($metaDesign['seo_monitoring_1_title']) !== ''){ ?>
        <div class="front__tool front__tool-monitoring">
        <div class="wrapper">
            <div class="front__tool-description">
                <svg class="description-img" width="20" height="23" viewBox="0 0 20 23" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M9.96031 22.8655C11.2493 22.8655 12.3039 21.8365 12.3039 20.5789H7.61671C7.61671 21.8365 8.67133 22.8655 9.96031 22.8655ZM17.577 16.0058V9.71784C17.577 6.17368 15.1162 3.3155 11.718 2.5152V1.71491C11.718 0.800292 10.8978 0 9.96031 0C9.02287 0 8.20261 0.800292 8.20261 1.71491V2.5152C4.80439 3.3155 2.3436 6.17368 2.3436 9.71784V16.0058L0 18.2924V19.4357H19.9206V18.2924L17.577 16.0058Z" fill="#8E17AB"/>
                </svg>
                <h2 class="description-title"><?=t($metaDesign['seo_monitoring_title']);?></h2>
                <p class="description-text"><?=t($metaDesign['seo_monitoring_description']);?></p>
                <div class="description-about" role="tablist" id="monitoring-accordion">
                    <div class="panel panel-default active">
                        <div class="panel-heading" role="tab" id="monitoringOne">
                            <div class="panel-title">
                                <h3 data-toggle="collapse" data-parent="#monitoring-accordion" href="#collapseMonitoringOne" aria-expanded="true" aria-controls="collapseMonitoringOne"><?=t($metaDesign['seo_monitoring_1_title']);?></h3>
                            </div>
                        </div>
                        <div id="collapseMonitoringOne" class="panel-collapse collapse show" role="tabpanel" aria-labelledby="monitoringOne">
                            <div class="panel-body"><?=t($metaDesign['seo_monitoring_1_description']);?></div>
                        </div>
                    </div>
                    <?php if(t($metaDesign['seo_monitoring_2_title']) !== ''){ ?>
                        <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="monitoringTwo">
                            <div class="panel-title">
                                <h3 class="collapsed" data-toggle="collapse" data-parent="#monitoring-accordion" href="#collapseMonitoringTwo" aria-expanded="false" aria-controls="collapseMonitoringTwo"><?=t($metaDesign['seo_monitoring_2_title']);?></h3>
                            </div>
                        </div>
                        <div id="collapseMonitoringTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="monitoringTwo">
                            <div class="panel-body"><?=t($metaDesign['seo_monitoring_2_description']);?></div>
                        </div>
                    </div>
                    <?php }?>
                    <?php if(t($metaDesign['seo_monitoring_3_title']) !== ''){ ?>
                        <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="monitoringThree">
                            <div class="panel-title">
                                <h3 class="collapsed" data-toggle="collapse" data-parent="#monitoring-accordion" href="#collapseMonitoringThree" aria-expanded="false" aria-controls="collapseMonitoringThree"><?=t($metaDesign['seo_monitoring_3_title']);?></h3>
                            </div>
                        </div>
                        <div id="collapseMonitoringThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="monitoringThree">
                            <div class="panel-body"><?=t($metaDesign['seo_monitoring_3_description']);?></div>
                        </div>
                    </div>
                    <?php }?>
                    <?php if(t($metaDesign['seo_monitoring_4_title']) !== ''){ ?>
                        <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="monitoringFour">
                            <div class="panel-title">
                                <h3 class="collapsed" data-toggle="collapse" data-parent="#monitoring-accordion" href="#collapseMonitoringFour" aria-expanded="false" aria-controls="collapseMonitoringFour"><?=t($metaDesign['seo_monitoring_4_title']);?></h3>
                            </div>
                        </div>
                        <div id="collapseMonitoringFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="monitoringFour">
                            <div class="panel-body"><?=t($metaDesign['seo_monitoring_4_description']);?></div>
                        </div>
                    </div>
                    <?php }?>
                    <?php if(t($metaDesign['seo_monitoring_5_title']) !== ''){ ?>
                        <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="monitoringFive">
                            <div class="panel-title">
                                <h3 class="collapsed" data-toggle="collapse" data-parent="#monitoring-accordion" href="#collapseMonitoringFive" aria-expanded="false" aria-controls="collapseMonitoringFive"><?=t($metaDesign['seo_monitoring_5_title']);?></h3>
                            </div>
                        </div>
                        <div id="collapseMonitoringFive" class="panel-collapse collapse" role="tabpanel" aria-labelledby="monitoringFive">
                            <div class="panel-body"><?=t($metaDesign['seo_monitoring_5_description']);?></div>
                        </div>
                    </div>
                    <?php }?>
                </div>
                <div class="front__tool-action">
                    <a href="/website-monitoring/" class="btn"><?=t($metaDesign['learn_more_btn']);?></a>
                    <?php
                    if(is_user_logged_in()){
                        echo '<a href="/monitoring/?id=16972" class="btn action">'.t($metaDesign['watch_demo_btn']).'</a>';
                    }else{
                        echo '<button class="btn action front__action-monitoring">'.t($metaDesign['watch_demo_btn']).'</button>';
                    }
                    ?>
                </div>
            </div>
            <div class="illustration illustrationMonitoring">
                <img class="monitoringOne" alt="arrow" title="arrow">
            </div>
        </div>
    </div>
    <?php } ?>
    <?php if(t($metaDesign['sitecheker_benefits_title']) !== '') { ?>
        <div class="front__benefits">
        <div class="wrapper">
            <h2 class="title"><?=t($metaDesign['sitecheker_benefits_title']);?></h2>
            <div class="front__benefits-block">
                <div>
                    <img src="<?= $template_directory_uri; ?>/out/img_design/benefit__comprehensive.svg" alt="<?=t($metaDesign['benefits_1_title']);?>" title="<?=t($metaDesign['benefits_1_title']);?>">
                    <h3><?=t($metaDesign['benefits_1_title']);?></h3>
                    <span><?=t($metaDesign['benefits_1_description']);?></span>
                </div>
                <div>
                    <img src="<?= $template_directory_uri; ?>/out/img_design/benefit__fixes.svg" alt="<?=t($metaDesign['benefits_2_title']);?>" title="<?=t($metaDesign['benefits_2_title']);?>">
                    <h3><?=t($metaDesign['benefits_2_title']);?></h3>
                    <span><?=t($metaDesign['benefits_2_description']);?></span>
                </div>
                <div>
                    <img src="<?= $template_directory_uri; ?>/out/img_design/benefit__lightening.svg" alt="<?=t($metaDesign['benefits_3_title']);?>" title="<?=t($metaDesign['benefits_3_title']);?>">
                    <h3><?=t($metaDesign['benefits_3_title']);?></h3>
                    <span><?=t($metaDesign['benefits_3_description']);?></span>
                </div>
                <div>
                    <img src="<?= $template_directory_uri; ?>/out/img_design/benefit__completely.svg" alt="<?=t($metaDesign['benefits_4_title']);?>" title="<?=t($metaDesign['benefits_4_title']);?>">
                    <h3><?=t($metaDesign['benefits_4_title']);?></h3>
                    <span><?=t($metaDesign['benefits_4_description']);?></span>
                </div>
            </div>
        </div>
    </div>
    <?php }?>
    <?php if(t($metaDesign['testimonials_title']) !== '') { ?>
     <div class="front__testimonials">
        <div class="wrapper">
            <h2 class="title"><?=t($metaDesign['testimonials_title']);?></h2>
            <p class="description"><?=t($metaDesign['testimonials_description']);?></p>
            <div class="front__testimonials-block">
                <div class="testimonials">
                    <div class="author">
                        <div class="author-img">
                            <img data-src="<?php echo get_field('testimonials_1_img', url_to_postid('/front-page-design/'));?>" alt="testimonial" class="testimonial__1">
                        </div>
                        <p class="author-name"><?=t($metaDesign['testimonials_1_name']);?></p>
                    </div>
                    <div class="comment"><?=t($metaDesign['testimonials_1_review']);?></div>
                </div>
                <div class="testimonials">
                    <div class="author">
                        <div class="author-img">
                            <img data-src="<?php echo get_field('testimonials_2_img', url_to_postid('/front-page-design/'));?>" alt="testimonial" class="testimonial__2">
                        </div>
                        <p class="author-name"><?=t($metaDesign['testimonials_2_name']);?></p>
                    </div>
                    <div class="comment"><?=t($metaDesign['testimonials_2_review']);?></div>
                </div>
                <div class="testimonials">
                    <div class="author">
                        <div class="author-img">
                            <img data-src="<?php echo get_field('testimonials_3_img', url_to_postid('/front-page-design/'));?>" alt="testimonial" class="testimonial__3">
                        </div>
                        <p class="author-name"><?=t($metaDesign['testimonials_3_name']);?></p>
                    </div>
                    <div class="comment"><?=t($metaDesign['testimonials_3_review']);?></div>
                </div>
                <a href="https://www.facebook.com/pg/sitecheckerpro/reviews/" class="btn" target="_blank"><?=t($metaDesign['show_more_reviews']);?></a>
            </div>
        </div>
    </div>
    <?php }?>
    <div class="front__check">
        <div class="wrapper">
            <img src="<?= $template_directory_uri; ?>/out/img_design/logo__white.svg" alt="logo__white" title="logo__white" class="front__check-logo">
            <h2 class="title"><?=t($metaDesign['try_sitechecker_for_free']);?></h2>
            <form class="article__seo-search form__Check" name="seo-report">
                <span class="error">Must be a valid URL with http:// or https://</span>
                <input type="text" placeholder="<?=t($metaDesign['enter_url_for_analysis']);?>">
                <button type="submit"><img src="<?= $template_directory_uri; ?>/out/img_design/arrow.svg" alt="arrow" title="arrow" class="arrow"><img src="<?= $template_directory_uri; ?>/out/img_design/loading.svg" alt="arrow" title="arrow" class="loading"></button>
            </form>
            <div class="audit__testCase">
                <form class="article__seo-search audit__form audit__form-blue">
                    <span class="error">Must be a valid URL with http:// or https://</span>
                    <input type="text" placeholder="<?=t($metaCrawlerDesign['health_checker_placeholder']);?>">
                    <button type="submit" class="<?php echo $crawlButton; ?>"><img src="<?= $template_directory_uri; ?>/out/img_design/arrow.svg" alt="arrow" title="arrow" class="arrow"><img src="<?= $template_directory_uri; ?>/out/img_design/loading.svg" alt="arrow" title="arrow" class="loading"></button>
                </form>
            </div>
        </div>
    </div>
    <?php if(t($meta['faq.1.header']) !== ''){ ?>
        <div class="front__faq">
        <div class="wrapper">
            <h2 class="title"><?=t($metaDesign['faq_title']);?></h2>
            <p class="description"><?=t($metaDesign['faq_description']);?></p>
            <div class="front__faq-block">
                <div class="panel-group" id="faq-accordion" role="tablist" aria-multiselectable="true">
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingFaqOne">
                            <div class="panel-title" data-toggle="collapse" data-parent="#faq-accordion" href="#collapseFaqOne" aria-expanded="false" aria-controls="collapseFaqOne">
                                <h3><?=t($meta['faq.1.header']);?></h3>
                                <div class="faq__arrow">
                                    <img src="<?= $template_directory_uri; ?>/out/img_design/arrow__down-blue.svg" alt="arrow" title="arrow">
                                </div>
                            </div>
                        </div>
                        <div id="collapseFaqOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFaqOne">
                            <div class="panel-body"><?=t($meta['faq.1.content']);?></div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingFaqTwo">
                            <div class="panel-title" data-toggle="collapse" data-parent="#faq-accordion" href="#collapseFaqTwo" aria-expanded="false" aria-controls="collapseFaqTwo">
                                <h3><?=t($meta['faq.2.header']);?></h3>
                                <div class="faq__arrow">
                                    <img src="<?= $template_directory_uri; ?>/out/img_design/arrow__down-blue.svg" alt="arrow" title="arrow">
                                </div>
                            </div>
                        </div>
                        <div id="collapseFaqTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFaqTwo">
                            <div class="panel-body"><?=t($meta['faq.2.content']);?></div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingFaqThree">
                            <div class="panel-title" data-toggle="collapse" data-parent="#faq-accordion" href="#collapseFaqThree" aria-expanded="false" aria-controls="collapseFaqThree">
                                <h3><?=t($meta['faq.3.header']);?></h3>
                                <div class="faq__arrow">
                                    <img src="<?= $template_directory_uri; ?>/out/img_design/arrow__down-blue.svg" alt="arrow" title="arrow">
                                </div>
                            </div>
                        </div>
                        <div id="collapseFaqThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFaqThree">
                            <div class="panel-body"><?=t($meta['faq.3.content']);?></div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingFaqFour">
                            <div class="panel-title" data-toggle="collapse" data-parent="#faq-accordion" href="#collapseFaqFour" aria-expanded="false" aria-controls="collapseFaqFour">
                                <h3><?=t($meta['faq.4.header']);?></h3>
                                <div class="faq__arrow">
                                    <img src="<?= $template_directory_uri; ?>/out/img_design/arrow__down-blue.svg" alt="arrow" title="arrow">
                                </div>
                            </div>
                        </div>
                        <div id="collapseFaqFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFaqFour">
                            <div class="panel-body"><?=t($meta['faq.4.content']);?></div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingFaqFive">
                            <div class="panel-title" data-toggle="collapse" data-parent="#faq-accordion" href="#collapseFaqFive" aria-expanded="false" aria-controls="collapseFaqFive">
                                <h3><?=t($meta['faq.5.header']);?></h3>
                                <div class="faq__arrow">
                                    <img src="<?= $template_directory_uri; ?>/out/img_design/arrow__down-blue.svg" alt="arrow" title="arrow">
                                </div>
                            </div>
                        </div>
                        <div id="collapseFaqFive" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFaqFive">
                            <div class="panel-body"><?=t($meta['faq.5.content']);?></div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingFaqSix">
                            <div class="panel-title" data-toggle="collapse" data-parent="#faq-accordion" href="#collapseFaqSix" aria-expanded="false" aria-controls="collapseFaqSix">
                                <h3><?=t($meta['faq.6.header']);?></h3>
                                <div class="faq__arrow">
                                    <img src="<?= $template_directory_uri; ?>/out/img_design/arrow__down-blue.svg" alt="arrow" title="arrow">
                                </div>
                            </div>
                        </div>
                        <div id="collapseFaqSix" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFaqSix">
                            <div class="panel-body"><?=t($meta['faq.6.content']);?></div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingFaqSeven">
                            <div class="panel-title" data-toggle="collapse" data-parent="#faq-accordion" href="#collapseFaqSeven" aria-expanded="false" aria-controls="collapseFaqSeven">
                                <h3><?=t($meta['faq.7.header']);?></h3>
                                <div class="faq__arrow">
                                    <img src="<?= $template_directory_uri; ?>/out/img_design/arrow__down-blue.svg" alt="arrow" title="arrow">
                                </div>
                            </div>
                        </div>
                        <div id="collapseFaqSeven" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFaqSeven">
                            <div class="panel-body"><?=t($meta['faq.7.content']);?></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="stars__block">
                <?php if(function_exists('the_ratings')) { the_ratings(); } ?>
            </div>
        </div>
    </div>
    <?php } ?>
</main>
<?php } ?>