<?php
$meta = get_post_meta_all(get_option('page_on_front'));
$metaDesign = get_post_meta_all(url_to_postid( '/header-footer-design/' ));
$metaCategory = get_post_meta_all(url_to_postid( '/knowledge-base/' ));
$template_directory_uri = get_template_directory_uri();
?>
<main>
    <div class="knbs__first">
        <div class="wrapper">
            <h1><?=t($metaCategory['seo_blog']);?></h1>
            <form class="article__seo-search search knbs__search" method="get" action="<?php echo home_url(); ?>" role="search">
                <input class="search-input" type="search" name="s" placeholder="<?=t($metaCategory['placeholder']);?>">
                <button class="search-submit" type="submit" role="button"><img src="<?= $template_directory_uri; ?>/out/img_design/search.svg" alt="arrow" title="arrow"></button>
            </form>
        </div>
    </div>
    <div class="knbs__popular">
        <div class="wrapper">
            <div class="title"><img src="<?= $template_directory_uri; ?>/out/img_design/fire.svg" alt="most popular"><h2><?=t($metaCategory['popular_articles']);?></h2></div>
            <p class="description"><?=t($metaCategory['popular_articles_description']);?></p>
            <div class="knbs__categories">
                <?php
                $categories = get_categories(array(
                    'orderby' => 'name',
                    'order' => 'ASC',
                    'hide_empty' => 1
                ));
                foreach( $categories as $category ){ ?>
                    <?php
                    $paged = get_query_var( 'paged' ) ? absint( get_query_var( 'paged' ) ) : 1;
                    $args = array(
                        'posts_per_page' => -1,
                        'cat' => $category->term_id,
                        'post_type' => array('page','post'),
                        'paged' => $paged,
                        'hide_empty' => 1
                    );
                    $query = new WP_Query($args);
                    $hasPopular = 0;
                    ?>
                    <?php if ( $query->have_posts() ) :
                        while ( $query->have_posts() ) : $query->the_post();
                        $h1 = get_post_meta( get_the_ID(), 'page.h1', true);
                        $short__title = get_post_meta( get_the_ID(), 'page_short_title', true);
                        $popular = get_field('the_most_popular', get_the_ID());
                        if($popular === 'True' && $short__title !== '' || $popular === 'True' && $h1 !== ''){
                            $hasPopular++;
                            if($hasPopular == 1){ ?>
                                <div class="category">
                                <a href="<?php echo get_category_link($category->term_id); ?>" class="title"><?php echo $category->name; ?></a>
                                <ul>
                            <?php } ?>
                                <li><a href="<?php the_permalink(); ?>"><?php echo $short__title ? $short__title : $h1; ?></a></li>
                        <?php } endwhile;
                        if($hasPopular>0){ ?>
                            </ul></div>
                        <?php } ?>
                    <?php endif; wp_reset_query(); ?>
                <?php }
                ?>
            </div>
        </div>
    </div>
    <div class="knbs__latest">
        <div class="wrapper">
            <div class="title"><img src="<?= $template_directory_uri; ?>/out/img_design/latest.svg" alt="latest articles"><h2><?=t($metaCategory['latest_articles']);?></h2></div>
            <div class="knbs__latest-container">
            <?php
            $args = array(
                'posts_per_page' => -1,
                'post_type' => 'page',
                'orderby' => 'date',
                'order'   => 'DESC'
            );
            $query = new WP_Query( $args );
            $countPages = 0;
            if ($query->have_posts()): while ($query->have_posts()) : $query->the_post();
                $categories = get_the_category();
                $category_name = $categories[0]->cat_name;
                $h1 = get_post_meta(get_the_ID(), 'page.h1', true);
                $short__title = get_post_meta(get_the_ID(), 'page_short_title', true);
                if ($countPages<=5 && $short__title !== '' || $countPages<5 && $h1 !== '') {
                    ?>
                    <div class="knbs__latest-block">
                        <a href="<?php echo get_category_link($categories[0]->term_id); ?>"
                           class="category"><?php echo $category_name ?></a>
                        <a href="<?php echo get_permalink() ?>"
                           class="title"><?php echo $short__title ? $short__title : $h1; ?></a>
                        <p class="description"><?php html5wp_excerpt('html5wp_index'); ?></p>
                        <div class="article__info">
                            <div class="date">
                                <span><?php echo get_the_date('D, M j, Y'); ?></span>
                            </div>
                            <div class="watches">
                                <svg width="22" height="16" viewBox="0 0 22 16" fill="none"
                                     xmlns="http://www.w3.org/2000/svg">
                                    <path fill-rule="evenodd" clip-rule="evenodd"
                                          d="M10.999 4.5C9.339 4.5 8 5.84 8 7.5C8 9.161 9.339 10.501 10.999 10.501C12.66 10.501 14 9.161 14 7.5C14 5.84 12.66 4.5 10.999 4.5ZM10.999 12.5C8.241 12.5 6 10.26 6 7.5C6 4.74 8.241 2.5 10.999 2.5C13.76 2.5 16 4.74 16 7.5C16 10.26 13.76 12.5 10.999 12.5ZM10.999 0C6 0 1.73 3.11 0 7.5C1.73 11.89 6 15 10.999 15C16 15 20.271 11.89 22 7.5C20.271 3.11 16 0 10.999 0Z"
                                          transform="translate(0 0.5)" fill="#E6E6E6"/>
                                </svg>
                                <span><?php echo getPostViews(get_the_ID()); ?></span>
                            </div>
                            <div class="stars">
                                <svg width="19" height="17" viewBox="0 0 19 17" fill="none"
                                     xmlns="http://www.w3.org/2000/svg">
                                    <path d="M9.50018 0L12.4355 5.59599L19 6.49359L14.2499 10.8492L15.3712 17L9.50018 14.0962L3.62879 17L4.75009 10.8492L0 6.49359L6.56448 5.59599L9.50018 0Z"
                                          fill="#E6E6E6"/>
                                </svg>
                                <div class="post__ratings"><?php echo the_ratings_results(get_the_ID()) ?></div>
                                <span class="post__ratings-result"></span>
                            </div>
                        </div>
                    </div>
                <?php $countPages++; }
            endwhile; ?>
            <?php endif; ?>
            </div>
        </div>
    </div>
    <div class="knbs__cat">
        <div class="wrapper">
            <h2 class="title"><?=t($metaCategory['all_categories']);?></h2>
            <div class="knbs__cat-container">
                <?php
                $categories = get_categories(array(
                    'orderby' => 'name',
                    'order' => 'ASC',
                    'hide_empty' => 1
                ));
                foreach( $categories as $category ){
                        $query = new WP_Query($args);
                        $queried_object = get_queried_object();
                        $description = get_field('description',$category);
                        $image = get_field('image',$category);
                        $src = $image ? $image : $template_directory_uri.'/out/img_design/cat_'.$category->term_id.'.svg';
                        if($category->name !== ''){
                        ?>
                        <div class="knbs__cat-block">
                            <div class="image">
                                <img src="<?php echo $src; ?>" alt="category">
                            </div>
                            <a href="<?php echo get_category_link($category->term_id); ?>" class="title"><?php echo $category->name; ?></a>
                            <p class="description"><?php echo $description; ?></p>
                        </div>
                    <?php } }
                ?>
            </div>
        </div>
    </div>
    <div class="subscribe__banner">
        <img src="<?= $template_directory_uri; ?>/out/img_design/email__icon.svg" alt="email__icon"
             title="email__icon">
        <p><?=t($metaDesign['subscribe_title']);?></p>
        <!-- Begin Mailchimp Signup Form --><!-- Begin Mailchimp Signup Form -->
        <div id="mc_embed_signup">
            <form action="https://sitechecker.us13.list-manage.com/subscribe/post?u=b33f62afc5e797a4e431a7266&amp;id=c6ca5d5cd2"
                  method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form"
                  class="validate article__seo-search" target="_blank" novalidate>
                <div id="mc_embed_signup_scroll"><input type="email" value="" name="EMAIL" class="email"
                                                        id="mce-EMAIL" placeholder="<?=t($metaDesign['subscribe_placeholder']);?>" required>
                    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
                    <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text"
                                                                                              name="b_b33f62afc5e797a4e431a7266_c6ca5d5cd2"
                                                                                              tabindex="-1"
                                                                                              value=""></div>
                    <div class="clear"><input type="submit" value="<?=t($metaDesign['subscribe_button']);?>" name="subscribe"
                                              id="mc-embedded-subscribe" class="button btn"></div>
                </div>
            </form>
        </div>
        <!--End mc_embed_signup-->
    </div>
</main>