<?php
    $template_directory_uri = get_template_directory_uri();
    $metaDesign = get_post_meta_all(url_to_postid( '/header-footer-design/' ));
?>
<main>
    <div class="main knowledgeBase">
        <div class="wrapper">
            <h1 class="title">Sitechecker Knowledge Base</h1>
            <form class="search article__seo-search" method="get" action="<?php echo home_url(); ?>" role="search">
                <input class="search-input" type="search" name="s" placeholder="What are you looking for?">
                <button class="search-submit" type="submit" role="button"><img src="<?= $template_directory_uri; ?>/out/img_design/arrow.svg" alt="arrow" title="arrow"></button>
            </form>
            <div class="category__more-container">
                <?php
                $categories = get_categories(array(
                    'orderby' => 'name',
                    'order' => 'ASC',
                    'hide_empty' => 1
                ));
                foreach( $categories as $category ){ ?>
                        <?php
                        $paged = get_query_var( 'paged' ) ? absint( get_query_var( 'paged' ) ) : 1;
                        $args = array(
                            'posts_per_page' => 100,
                            'cat' => $category->term_id,
                            'post_type' => array('page','post'),
                            'paged' => $paged,
                            'hide_empty' => 1
                        );
                        $query = new WP_Query($args);
                        ?>
                        <?php if ( $query->have_posts() ) :
                        ?>
                        <div class="category__block">
                        <a href="<?php echo get_category_link($category->term_id); ?>" class="category__block-title"><?php echo $category->name; ?></a><div class="category__block-list">
                            <?php while ( $query->have_posts() ) : $query->the_post();
                                $h1 = get_post_meta( get_the_ID(), 'page.h1', true);
                                $short__title = get_post_meta( get_the_ID(), 'page_short_title', true);
                                if($short__title !== '' || $h1 !== ''){
                            ?>
                                <a href="<?php the_permalink(); ?>" class="title"><?php echo $short__title ? $short__title : $h1; ?></a>
                            <?php }
                            endwhile; ?>
                            </div>
                        </div>
                        <?php endif; wp_reset_query(); ?>
                <?php }
                ?>
            </div>
        </div>
        <div class="subscribe__banner">
            <img src="<?= $template_directory_uri; ?>/out/img_design/email__icon.svg" alt="email__icon"
                 title="email__icon">
            <p><?=t($metaDesign['subscribe_title']);?></p>
            <!-- Begin Mailchimp Signup Form --><!-- Begin Mailchimp Signup Form -->
            <div id="mc_embed_signup">
                <form action="https://sitechecker.us13.list-manage.com/subscribe/post?u=b33f62afc5e797a4e431a7266&amp;id=c6ca5d5cd2"
                      method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form"
                      class="validate article__seo-search" target="_blank" novalidate>
                    <div id="mc_embed_signup_scroll"><input type="email" value="" name="EMAIL" class="email"
                                                            id="mce-EMAIL" placeholder="<?=t($metaDesign['subscribe_placeholder']);?>" required>
                        <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
                        <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text"
                                                                                                  name="b_b33f62afc5e797a4e431a7266_c6ca5d5cd2"
                                                                                                  tabindex="-1"
                                                                                                  value=""></div>
                        <div class="clear"><input type="submit" value="<?=t($metaDesign['subscribe_button']);?>" name="subscribe"
                                                  id="mc-embedded-subscribe" class="button btn"></div>
                    </div>
                </form>
            </div>
            <!--End mc_embed_signup-->
        </div>
    </div>
</main>