<?php
/*
	Template Name: Logout Page
*/
wp_logout();
$not_empty_redirect = '/tools/seo-platform'.$_REQUEST['redirect_to_seo'];
$empty_redirect = '/tools/seo-platform/';

if(!isset($_REQUEST['redirect_to_seo'])){
    $smart_redirect_to = '/';
}else{
    $smart_redirect_to = !empty( $_REQUEST['redirect_to_seo'] ) ? '/?signup_popup&redirect_to="'.$not_empty_redirect.'"' : '/?signup_popup&redirect_to="'.$empty_redirect.'"';
}
wp_safe_redirect( $smart_redirect_to );
?>