<?php if (have_posts()): while (have_posts()) : the_post();
    $categories = get_the_category();
    $category_name = $categories[0]->cat_name;
    $h1 = get_post_meta(get_the_ID(), 'page.h1', true);
    $short__title = get_post_meta(get_the_ID(), 'page_short_title', true);
        ?>
        <div class="knbs__latest-block">
            <a href="<?php echo get_category_link($categories[0]->term_id); ?>"
               class="category"><?php echo $category_name ?></a>
            <a href="<?php echo get_permalink() ?>"
               class="title"><?php echo $short__title ? $short__title : $h1; ?></a>
            <p class="description"><?php html5wp_excerpt('html5wp_index'); ?></p>
            <div class="article__info">
                <div class="date">
                    <span><?php echo get_the_date('D, M j, Y'); ?></span>
                </div>
                <div class="watches">
                    <svg width="22" height="16" viewBox="0 0 22 16" fill="none"
                         xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd" clip-rule="evenodd"
                              d="M10.999 4.5C9.339 4.5 8 5.84 8 7.5C8 9.161 9.339 10.501 10.999 10.501C12.66 10.501 14 9.161 14 7.5C14 5.84 12.66 4.5 10.999 4.5ZM10.999 12.5C8.241 12.5 6 10.26 6 7.5C6 4.74 8.241 2.5 10.999 2.5C13.76 2.5 16 4.74 16 7.5C16 10.26 13.76 12.5 10.999 12.5ZM10.999 0C6 0 1.73 3.11 0 7.5C1.73 11.89 6 15 10.999 15C16 15 20.271 11.89 22 7.5C20.271 3.11 16 0 10.999 0Z"
                              transform="translate(0 0.5)" fill="#E6E6E6"/>
                    </svg>
                    <span><?php echo getPostViews(get_the_ID()); ?></span>
                </div>
                <div class="stars">
                    <svg width="19" height="17" viewBox="0 0 19 17" fill="none"
                         xmlns="http://www.w3.org/2000/svg">
                        <path d="M9.50018 0L12.4355 5.59599L19 6.49359L14.2499 10.8492L15.3712 17L9.50018 14.0962L3.62879 17L4.75009 10.8492L0 6.49359L6.56448 5.59599L9.50018 0Z"
                              fill="#E6E6E6"/>
                    </svg>
                    <div class="post__ratings"><?php echo the_ratings_results(get_the_ID()) ?></div>
                    <span class="post__ratings-result"></span>
                </div>
            </div>
        </div>
<?php endwhile; endif; ?>