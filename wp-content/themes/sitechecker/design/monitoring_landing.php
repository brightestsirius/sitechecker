<?php
$template_directory_uri = get_template_directory_uri();
$meta = get_post_meta_all($post->ID);
$metaDesign = get_post_meta_all(url_to_postid( '/website-seo-monitoring-design/' ));
if(is_user_logged_in()){
    $userDomains = getDomainsByUserId(get_current_user_id());
    $availableLimit = getAvailableLimitByUser(get_current_user_id());
    $pagesLimits = $availableLimit->getAvailablePages();
    if($pagesLimits <= 0){
        $crawlButton = 'limits__btn';
        $monitoringButton = 'limits__btn';
    }else{
        $monitoringButton = 'sitechecker_monitoring';
    }
} else{
    $monitoringButton = 'sitechecker_reg-monitoring';
}
?>
<main class="monitoring__landing">
    <div class="monitoring__first">
        <div class="wrapper audit__first-container">
            <h1><?=t($meta['main.h1']);?></h1>
            <h2><?=t($meta['main.h2']);?></h2>
            <form class="article__seo-search monitoring__form">
                <span class="error">Must be a valid URL with http:// or https://</span>
                <div class="error__limits">No limits! <a href="/plans/">Upgrade your account</a> to start monitoring the website</div>
                <input type="text" placeholder="<?=t($metaDesign['website_changes_placeholder']);?>">
                <button type="submit" class="<?php echo $monitoringButton; ?>"><img src="<?= $template_directory_uri; ?>/out/img_design/arrow.svg" alt="arrow" title="arrow" class="arrow"><img src="<?= $template_directory_uri; ?>/out/img_design/loading.svg" alt="arrow" title="arrow" class="loading"></button>
            </form>
        </div>
    </div>
    <?php if(t($metaDesign['benefits_title']) !== ''){ ?>
        <div class="monitoring__benefits">
        <div class="wrapper">
            <p class="audit__title"><?=t($metaDesign['benefits_title']);?></p>
            <div class="monitoring__benefits-container">
                <div class="monitoring__benefit">
                    <img src="<?= $template_directory_uri; ?>/out/img_design/monitoring__benefit1.svg" alt="monitoring__benefit" title="monitoring__benefit">
                    <p><?=t($metaDesign['benefits_1_title']);?></p>
                    <span><?=t($metaDesign['benefits_1_description']);?></span>
                </div>
                <div class="monitoring__benefit">
                    <img src="<?= $template_directory_uri; ?>/out/img_design/monitoring__benefit2.svg" alt="monitoring__benefit" title="monitoring__benefit">
                    <p><?=t($metaDesign['benefits_2_title']);?></p>
                    <span><?=t($metaDesign['benefits_2_description']);?></span>
                </div>
                <div class="monitoring__benefit">
                    <img src="<?= $template_directory_uri; ?>/out/img_design/monitoring__benefit3.svg" alt="monitoring__benefit" title="monitoring__benefit">
                    <p><?=t($metaDesign['benefits_3_title']);?></p>
                    <span><?=t($metaDesign['benefits_3_description']);?></span>
                </div>
                <div class="monitoring__benefit">
                    <img src="<?= $template_directory_uri; ?>/out/img_design/monitoring__benefit4.svg" alt="monitoring__benefit" title="monitoring__benefit">
                    <p><?=t($metaDesign['benefits_4_title']);?></p>
                    <span><?=t($metaDesign['benefits_4_description']);?></span>
                </div>
            </div>
        </div>
    </div>
    <?php }?>
    <div class="monitoring__about">
        <div class="wrapper">
            <div class="monitoring__about-text">
                <img src="<?= $template_directory_uri; ?>/out/img_design/monitoring__land-search.svg" alt="monitoring" title="monitoring">
                <p class="title"><?=t($meta['seo.1.header']);?></p>
                <p class="description"><?=t($meta['seo.1.content']);?></p>
            </div>
            <div class="monitoring__about-image monitoring__about_1">
                <img src="<?= $template_directory_uri; ?>/out/img_design/monitoring__triangle1.svg" alt="monitoring" title="monitoring">
                <img src="<?= $template_directory_uri; ?>/out/img_design/monitoringOne.svg" alt="monitoring" title="monitoring">
                <img src="<?= $template_directory_uri; ?>/out/img_design/monitoring__triangle2.svg" alt="monitoring" title="monitoring">
            </div>
        </div>
    </div>
    <div class="monitoring__about">
        <div class="wrapper">
            <div class="monitoring__about-text">
                <img src="<?= $template_directory_uri; ?>/out/img_design/monitoring__land-icon2.svg" alt="monitoring" title="monitoring">
                <p class="title"><?=t($meta['seo.2.header']);?></p>
                <p class="description"><?=t($meta['seo.2.content']);?></p>
            </div>
            <div class="monitoring__about-image monitoring__about_2">
                <img src="<?= $template_directory_uri; ?>/out/img_design/monitoringTwo.svg" alt="monitoring" title="monitoring">
                <img src="<?= $template_directory_uri; ?>/out/img_design/monitoring__triangle3.svg" alt="monitoring" title="monitoring">
            </div>
        </div>
    </div>
    <div class="monitoring__about">
        <div class="wrapper">
            <div class="monitoring__about-text">
                <img src="<?= $template_directory_uri; ?>/out/img_design/monitoring__land-icon3.svg" alt="monitoring" title="monitoring">
                <p class="title"><?=t($meta['seo.3.header']);?></p>
                <p class="description"><?=t($meta['seo.3.content']);?></p>
            </div>
            <div class="monitoring__about-image monitoring__about_3">
                <img src="<?= $template_directory_uri; ?>/out/img_design/monitoringThree.svg" alt="monitoring" title="monitoring">
                <img src="<?= $template_directory_uri; ?>/out/img_design/monitoring__triangle4.svg" alt="monitoring" title="monitoring">
            </div>
        </div>
    </div>
    <div class="monitoring__about">
        <div class="wrapper">
            <div class="monitoring__about-text">
                <img src="<?= $template_directory_uri; ?>/out/img_design/monitoring__land-icon4.svg" alt="monitoring" title="monitoring">
                <p class="title"><?=t($meta['seo.4.header']);?></p>
                <p class="description"><?=t($meta['seo.4.content']);?></p>
            </div>
            <div class="monitoring__about-image monitoring__about_4">
                <img src="<?= $template_directory_uri; ?>/out/img_design/monitoring__triangle5.svg" alt="monitoring" title="monitoring">
                <img src="<?= $template_directory_uri; ?>/out/img_design/monitoringFour.svg" alt="monitoring" title="monitoring">
                <img src="<?= $template_directory_uri; ?>/out/img_design/monitoring__triangle6.svg" alt="monitoring" title="monitoring">
            </div>
        </div>
    </div>
    <div class="monitoring__about">
        <div class="wrapper">
            <div class="monitoring__about-text">
                <img src="<?= $template_directory_uri; ?>/out/img_design/monitoring__land-icon5.svg" alt="monitoring" title="monitoring">
                <p class="title"><?=t($meta['seo.5.header']);?></p>
                <p class="description"><?=t($meta['seo.5.content']);?></p>
            </div>
            <div class="monitoring__about-image monitoring__about_5">
                <img src="<?= $template_directory_uri; ?>/out/img_design/monitoring__triangle7.svg" alt="monitoring" title="monitoring">
                <img src="<?= $template_directory_uri; ?>/out/img_design/monitoringFive.svg" alt="monitoring" title="monitoring">
            </div>
        </div>
    </div>
    <div class="front__check monitoring__land-banner">
        <div class="wrapper">
            <img src="<?= $template_directory_uri; ?>/out/img_design/logo__white.svg" alt="logo__white" title="logo__white" class="front__check-logo">
            <p class="title"><?=t($metaDesign['website_changes_banner']);?></p>
            <form class="article__seo-search monitoring__form">
                <span class="error">Must be a valid URL with http:// or https://</span>
                <div class="error__limits">No limits! <a href="/plans/">Upgrade your account</a> to start monitoring the website</div>
                <input type="text" placeholder="<?=t($metaDesign['website_changes_placeholder']);?>">
                <button type="submit" class="<?php echo $monitoringButton; ?>"><img src="<?= $template_directory_uri; ?>/out/img_design/arrow.svg" alt="arrow" title="arrow" class="arrow"><img src="<?= $template_directory_uri; ?>/out/img_design/loading.svg" alt="arrow" title="arrow" class="loading"></button>
            </form>
        </div>
    </div>
    <div class="stars__block">
        <?php if(function_exists('the_ratings')) { the_ratings(); } ?>
    </div>
</main>