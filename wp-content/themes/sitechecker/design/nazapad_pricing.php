<?php
/*
    Template Name: Nazapad
*/

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $_REQUEST['level'] = $_POST['level_id'];
    require(ABSPATH . "/wp-content/plugins/paid-memberships-pro/preheaders/checkout.php");
}

get_header('tmp_design');
$pmpro_levels = pmpro_getAllLevels();

$payProUrls = [];
foreach ($pmpro_levels as $level){
    $payProUrls[$level->id] = generateUrl(get_current_user_id(), $level->id);
}

$template_directory_uri = get_template_directory_uri();
$meta = get_post_meta_all(get_option('page_on_front'));
$metaDesign = get_post_meta_all(url_to_postid( '/pricing-design/' ));
$pricing = get_post_meta_all(url_to_postid( '/pricing-content/' ));

global $gateway, $pmpro_review, $skip_account_fields, $pmpro_paypal_token, $wpdb, $current_user, $pmpro_msg, $pmpro_msgt, $pmpro_requirebilling, $pmpro_level, $pmpro_levels, $tospage, $pmpro_currency_symbol, $pmpro_show_discount_code;
global $discount_code, $username, $password, $password2, $bfirstname, $blastname, $baddress1, $baddress2, $bcity, $bstate, $bzipcode, $bcountry, $bphone, $bemail, $bconfirmemail, $CardType, $AccountNumber, $ExpirationMonth,$ExpirationYear;

$pmpro_levels = pmpro_getAllLevels();

$customerMembershipLevel = $current_user->membership_level->id;

?>

<div id="pricing__btns" style="display: none!important">
    <?php
    foreach ($pmpro_levels as $level) {
        $_REQUEST['level'] = $level->id;
        require(ABSPATH . "/wp-content/plugins/paid-memberships-pro/preheaders/checkout.php");
        ?>
        <form class="pmpro_form" action="" method="post" name="<?php echo $level->id?>">
            <p class="level__cost" id="level__btn-<?php echo $level->id; ?>"><?php echo intval($level->initial_payment); ?></p>
            <div class="button">
                <input type="hidden" name="level_id" value="<?php echo $level->id?>" />
                <input type="hidden" name="submit-checkout" value="<?php echo $level->id?>" />
                <?php if($gateway == "paypalexpress" && $pmpro_requirebilling) { ?>
                    <input type="submit" class="pmpro_btn-submit-checkout btn <?php if($customerMembershipLevel == $level->id){echo 'your__level';}?>" value="<?php echo $customerMembershipLevel == $level->id ? get_field('your_plan', 6179) : get_field('subscribe', 6179) ?>" name="<?php echo $level->id?>">
                <?php } else { ?>
                    <span id="pmpro_submit_span"><input type="submit" class="pmpro_btn-submit-checkout btn <?php if($customerMembershipLevel == $level->id){echo 'your__level';}?>" value="<?php echo $customerMembershipLevel == $level->id ? get_field('your_plan', 6179) : get_field('subscribe', 6179) ?>" /></span>
                <?php } ?>
            </div>
        </form>
    <?php } ?>
</div>
<div class="checkout__design">
    <div class="wrapper">
        <h1 style="margin-bottom: 80px">Тарифы для участников <br>онлайн конференции Nazapad</h1>
        <div class="checkout__design-pricing">
            <div class="pricing__block">
                <p class="level__name">Junior</p>
                <p class="level__descript"><?=t($pricing['startup_description']);?></p>
                <div class="level__cost"><span>$</span><i class="startup__cost"><?php
                        foreach ($pmpro_levels as $level) {
                            if ($level->name == 'Junior') {
                                $_REQUEST['level'] = $level->id;
                                require(ABSPATH . "/wp-content/plugins/paid-memberships-pro/preheaders/checkout.php");
                                ?><?php echo $level->initial_payment; ?><?php }
                        } ?></i>
                    <div class="pricing__sale">
                        <span><i class="startup__save">28</i>$</span>
                        <img src="<?= $template_directory_uri; ?>/out/img_design/pricing__arrow.svg" alt="checkout__arrow">
                    </div>
                </div>
                <p class="level__month"><?=t($pricing['per_month']);?></p>
                <?php
                foreach ($pmpro_levels as $level) {
                    if ($level->name == 'Junior') {
                        $_REQUEST['level'] = $level->id;
                        require(ABSPATH . "/wp-content/plugins/paid-memberships-pro/preheaders/checkout.php");
                        ?>
                        <form class="pmpro_form" action="" method="post">
                            <div class="button startup__btn">
                                <input type="hidden" name="level_id" value="<?php echo $level->id?>" />
                                <input type="hidden" name="submit-checkout" value="<?php echo $level->id?>" />
                                <?php if($gateway == "paypalexpress" && $pmpro_requirebilling) { ?>
                                    <input type="submit" class="pmpro_btn-submit-checkout btn <?php if($customerMembershipLevel == $level->id){echo 'your__level';}?>" value="<?php echo $customerMembershipLevel == $level->id ? get_field('your_plan', 6179) : get_field('subscribe', 6179) ?>" name="<?php echo $level->id?>">
                                <?php } else { ?>
                                    <span id="pmpro_submit_span"><input type="submit" class="pmpro_btn-submit-checkout btn <?php if($customerMembershipLevel == $level->id){echo 'your__level';}?>" value="<?php echo $customerMembershipLevel == $level->id ? get_field('your_plan', 6179) : get_field('subscribe', 6179) ?>" /></span>
                                <?php } ?>
                            </div>
                        </form>
                    <?php }
                } ?>
                <div class="level__descriptions">
                    <p><b><span class="domains__count">1</span></b> <i class="plural"><?=t($pricing['websites']);?></i><i class="single"><?=t($pricing['website']);?></i>
                    </p>
                    <p><b>100</b> <?=t($pricing['urls_per_website']);?></p>
                    <p><?=t($pricing['hourly_crawling_frequency']);?></p>
                    <p><b>50</b> <?=t($pricing['keywords_per_website']);?></p>
                    <p><?=t($pricing['daily_keyword_tracking']);?></p>
                    <p><b>100</b> <?=t($pricing['backlinks_per_website']);?></p>
                    <p><?=t($pricing['daily_backlink_tracking']);?></p>
                    <span name=".checkout__design_pricingAll" class="scrollto"><?=t($pricing['all_features']);?></span>
                </div>
            </div>
            <div class="pricing__block">
                <div class="most__popular"><?=t($pricing['most_popular']);?></div>
                <p class="level__name">Middle</p>
                <p class="level__descript"><?=t($pricing['growing_description']);?></p>
                <div class="level__cost"><span>$</span><i class="bussines__cost"><?php
                        foreach ($pmpro_levels as $level) {
                            if ($level->name == 'Middle') {
                                $_REQUEST['level'] = $level->id;
                                require(ABSPATH . "/wp-content/plugins/paid-memberships-pro/preheaders/checkout.php");
                                ?><?php echo $level->initial_payment; ?><?php }
                        } ?></i>
                    <div class="pricing__sale">
                        <span><i class="growing__save">114</i>$</span>
                        <img src="<?= $template_directory_uri; ?>/out/img_design/pricing__arrow.svg" alt="checkout__arrow">
                    </div>
                </div>
                <p class="level__month"><?=t($pricing['per_month']);?></p>
                <?php
                foreach ($pmpro_levels as $level) {
                    if ($level->name == 'Middle') {
                        $_REQUEST['level'] = $level->id;
                        require(ABSPATH . "/wp-content/plugins/paid-memberships-pro/preheaders/checkout.php");
                        ?>
                        <form class="pmpro_form" action="" method="post">
                            <div class="button bussines__btn">
                                <input type="hidden" name="level_id" value="<?php echo $level->id?>" />
                                <input type="hidden" name="submit-checkout" value="<?php echo $level->id?>" />
                                <?php if($gateway == "paypalexpress" && $pmpro_requirebilling) { ?>
                                    <input type="submit" class="pmpro_btn-submit-checkout btn <?php if($customerMembershipLevel == $level->id){echo 'your__level';}?>" value="<?php echo $customerMembershipLevel == $level->id ? get_field('your_plan', 6179) : get_field('subscribe', 6179) ?>" name="<?php echo $level->id?>">
                                <?php } else { ?>
                                    <span id="pmpro_submit_span"><input type="submit" class="pmpro_btn-submit-checkout btn <?php if($customerMembershipLevel == $level->id){echo 'your__level';}?>" value="<?php echo $customerMembershipLevel == $level->id ? get_field('your_plan', 6179) : get_field('subscribe', 6179) ?>" /></span>
                                <?php } ?>
                            </div>
                        </form>
                    <?php } } ?>
                <div class="level__descriptions">
                    <p><b><span class="domains__count">1</span></b> <i class="plural"><?=t($pricing['websites']);?></i><i class="single"><?=t($pricing['website']);?></i>
                    </p>
                    <p><b>1000</b> <?=t($pricing['urls_per_website']);?></p>
                    <p><?=t($pricing['hourly_crawling_frequency']);?></p>
                    <p><b>200</b> <?=t($pricing['keywords_per_website']);?></p>
                    <p><?=t($pricing['daily_keyword_tracking']);?></p>
                    <p><b>500</b> <?=t($pricing['backlinks_per_website']);?></p>
                    <p><?=t($pricing['daily_backlink_tracking']);?></p>
                    <span name=".checkout__design_pricingAll" class="scrollto"><?=t($pricing['all_features']);?></span>
                </div>
            </div>
            <div class="pricing__block">
                <p class="level__name">Senior</p>
                <p class="level__descript"><?=t($pricing['business_description']);?></p>
                <div class="level__cost"><span>$</span><i class="enterprise__cost"><?php
                        foreach ($pmpro_levels as $level) {
                            if ($level->name == 'Senior') {
                                $_REQUEST['level'] = $level->id;
                                require(ABSPATH . "/wp-content/plugins/paid-memberships-pro/preheaders/checkout.php");
                                ?><?php echo $level->initial_payment; ?><?php }
                        } ?></i>
                    <div class="pricing__sale">
                        <span><i class="business__save">358</i>$</span>
                        <img src="<?= $template_directory_uri; ?>/out/img_design/pricing__arrow.svg" alt="checkout__arrow">
                    </div>
                </div>
                <p class="level__month"><?=t($pricing['per_month']);?></p>
                <?php
                foreach ($pmpro_levels as $level) {
                    if ($level->name == 'Senior') {
                        $_REQUEST['level'] = $level->id;
                        require(ABSPATH . "/wp-content/plugins/paid-memberships-pro/preheaders/checkout.php");
                        ?>
                        <form class="pmpro_form" action="" method="post">
                            <div class="button enterprise__btn">
                                <input type="hidden" name="level_id" value="<?php echo $level->id?>" />
                                <input type="hidden" name="submit-checkout" value="<?php echo $level->id?>" />
                                <?php if($gateway == "paypalexpress" && $pmpro_requirebilling) { ?>
                                    <input type="submit" class="pmpro_btn-submit-checkout btn <?php if($customerMembershipLevel == $level->id){echo 'your__level';}?>" value="<?php echo $customerMembershipLevel == $level->id ? get_field('your_plan', 6179) : get_field('subscribe', 6179) ?>" name="<?php echo $level->id?>">
                                <?php } else { ?>
                                    <span id="pmpro_submit_span"><input type="submit" class="pmpro_btn-submit-checkout btn <?php if($customerMembershipLevel == $level->id){echo 'your__level';}?>" value="<?php echo $customerMembershipLevel == $level->id ? get_field('your_plan', 6179) : get_field('subscribe', 6179) ?>" /></span>
                                <?php } ?>
                            </div>
                        </form>
                    <?php } } ?>
                <div class="level__descriptions">
                    <p><b><span class="domains__count">1</span></b> <i class="plural"><?=t($pricing['websites']);?></i><i class="single"><?=t($pricing['website']);?></i>
                    </p>
                    <p><b>10000</b> <?=t($pricing['urls_per_website']);?></p>
                    <p><?=t($pricing['hourly_crawling_frequency']);?></p>
                    <p><b>500</b> <?=t($pricing['keywords_per_website']);?></p>
                    <p><?=t($pricing['daily_keyword_tracking']);?></p>
                    <p><b>1000</b> <?=t($pricing['backlinks_per_website']);?></p>
                    <p><?=t($pricing['daily_backlink_tracking']);?></p>
                    <span name=".checkout__design_pricingAll" class="scrollto"><?=t($pricing['all_features']);?></span>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="checkout__design-testimonial">
    <div class="checkout__stars">
        <img src="<?= $template_directory_uri; ?>/out/img_design/checkout__star.svg" alt="star">
        <img src="<?= $template_directory_uri; ?>/out/img_design/checkout__star.svg" alt="star">
        <img src="<?= $template_directory_uri; ?>/out/img_design/checkout__star.svg" alt="star">
        <img src="<?= $template_directory_uri; ?>/out/img_design/checkout__star.svg" alt="star">
        <img src="<?= $template_directory_uri; ?>/out/img_design/checkout__star.svg" alt="star">
    </div>
    <div class="wrapper checkout__testimonial owl-carousel owl-theme"><?=t($pricing['testimonials']);?></div>
</div>
<div class="checkout__design_pricingAll">
    <div class="wrapper">
        <table class="pricingAll_block">
            <tr>
                <td><p class="pricingAll_header"><?=t($pricing['with_your_subscription']);?></p></td>
                <td>
                    <p class="pricing__title">Junior</p>
                    <p class="pricing__descript"><?=t($pricing['startup_description']);?></p>
                    <?php
                    foreach ($pmpro_levels as $level) {
                        if ($level->name == 'Junior') {
                            $_REQUEST['level'] = $level->id;
                            require(ABSPATH . "/wp-content/plugins/paid-memberships-pro/preheaders/checkout.php");
                            ?>
                            <form class="pmpro_form" action="" method="post">
                                <div class="button startup__btn">
                                    <input type="hidden" name="level_id" value="<?php echo $level->id?>" />
                                    <input type="hidden" name="submit-checkout" value="<?php echo $level->id?>" />
                                    <?php if($gateway == "paypalexpress" && $pmpro_requirebilling) { ?>
                                        <input type="submit" class="pmpro_btn-submit-checkout btn <?php if($customerMembershipLevel == $level->id){echo 'your__level';}?>" value="<?php echo $customerMembershipLevel == $level->id ? get_field('your_plan', 6179) : get_field('subscribe', 6179) ?>" name="<?php echo $level->id?>">
                                    <?php } else { ?>
                                        <span id="pmpro_submit_span"><input type="submit" class="pmpro_btn-submit-checkout btn <?php if($customerMembershipLevel == $level->id){echo 'your__level';}?>" value="<?php echo $customerMembershipLevel == $level->id ? get_field('your_plan', 6179) : get_field('subscribe', 6179) ?>" /></span>
                                    <?php } ?>
                                </div>
                            </form>
                        <?php } } ?>
                </td>
                <td>
                    <div class="most__popular"><?=t($pricing['most_popular']);?></div>
                    <p class="pricing__title">Middle</p>
                    <p class="pricing__descript"><?=t($pricing['growing_description']);?></p>
                    <?php
                    foreach ($pmpro_levels as $level) {
                        if ($level->name == 'Middle') {
                            $_REQUEST['level'] = $level->id;
                            require(ABSPATH . "/wp-content/plugins/paid-memberships-pro/preheaders/checkout.php");
                            ?>
                            <form class="pmpro_form" action="" method="post">
                                <div class="button bussines__btn">
                                    <input type="hidden" name="level_id" value="<?php echo $level->id?>" />
                                    <input type="hidden" name="submit-checkout" value="<?php echo $level->id?>" />
                                    <?php if($gateway == "paypalexpress" && $pmpro_requirebilling) { ?>
                                        <input type="submit" class="pmpro_btn-submit-checkout btn <?php if($customerMembershipLevel == $level->id){echo 'your__level';}?>" value="<?php echo $customerMembershipLevel == $level->id ? get_field('your_plan', 6179) : get_field('subscribe', 6179) ?>" name="<?php echo $level->id?>">
                                    <?php } else { ?>
                                        <span id="pmpro_submit_span"><input type="submit" class="pmpro_btn-submit-checkout btn <?php if($customerMembershipLevel == $level->id){echo 'your__level';}?>" value="<?php echo $customerMembershipLevel == $level->id ? get_field('your_plan', 6179) : get_field('subscribe', 6179) ?>" /></span>
                                    <?php } ?>
                                </div>
                            </form>
                        <?php } } ?>
                </td>
                <td>
                    <p class="pricing__title">Senior</p>
                    <p class="pricing__descript"><?=t($pricing['business_description']);?></p>
                    <?php
                    foreach ($pmpro_levels as $level) {
                        if ($level->name == 'Senior') {
                            $_REQUEST['level'] = $level->id;
                            require(ABSPATH . "/wp-content/plugins/paid-memberships-pro/preheaders/checkout.php");
                            ?>
                            <form class="pmpro_form" action="" method="post">
                                <div class="button enterprise__btn">
                                    <input type="hidden" name="level_id" value="<?php echo $level->id?>" />
                                    <input type="hidden" name="submit-checkout" value="<?php echo $level->id?>" />
                                    <?php if($gateway == "paypalexpress" && $pmpro_requirebilling) { ?>
                                        <input type="submit" class="pmpro_btn-submit-checkout btn <?php if($customerMembershipLevel == $level->id){echo 'your__level';}?>" value="<?php echo $customerMembershipLevel == $level->id ? get_field('your_plan', 6179) : get_field('subscribe', 6179) ?>" name="<?php echo $level->id?>">
                                    <?php } else { ?>
                                        <span id="pmpro_submit_span"><input type="submit" class="pmpro_btn-submit-checkout btn <?php if($customerMembershipLevel == $level->id){echo 'your__level';}?>" value="<?php echo $customerMembershipLevel == $level->id ? get_field('your_plan', 6179) : get_field('subscribe', 6179) ?>" /></span>
                                    <?php } ?>
                                </div>
                            </form>
                        <?php } } ?>
                </td>
            </tr>
            <tr class="tableScroll">
                <td></td>
                <td>
                    <p class="pricing__title">Junior</p>
                    <p class="pricing__descript"><?=t($pricing['startup_description']);?></p>
                    <?php
                    foreach ($pmpro_levels as $level) {
                        if ($level->name == 'Junior') {
                            $_REQUEST['level'] = $level->id;
                            require(ABSPATH . "/wp-content/plugins/paid-memberships-pro/preheaders/checkout.php");
                            ?>
                            <form class="pmpro_form" action="" method="post">
                                <div class="button startup__btn">
                                    <input type="hidden" name="level_id" value="<?php echo $level->id?>" />
                                    <input type="hidden" name="submit-checkout" value="<?php echo $level->id?>" />
                                    <?php if($gateway == "paypalexpress" && $pmpro_requirebilling) { ?>
                                        <input type="submit" class="pmpro_btn-submit-checkout btn <?php if($customerMembershipLevel == $level->id){echo 'your__level';}?>" value="<?php echo $customerMembershipLevel == $level->id ? get_field('your_plan', 6179) : get_field('subscribe', 6179) ?>" name="<?php echo $level->id?>">
                                    <?php } else { ?>
                                        <span id="pmpro_submit_span"><input type="submit" class="pmpro_btn-submit-checkout btn <?php if($customerMembershipLevel == $level->id){echo 'your__level';}?>" value="<?php echo $customerMembershipLevel == $level->id ? get_field('your_plan', 6179) : get_field('subscribe', 6179) ?>" /></span>
                                    <?php } ?>
                                </div>
                            </form>
                        <?php } } ?>
                </td>
                <td>
                    <p class="pricing__title">Middle</p>
                    <p class="pricing__descript"><?=t($pricing['growing_description']);?></p>
                    <?php
                    foreach ($pmpro_levels as $level) {
                        if ($level->name == 'Middle') {
                            $_REQUEST['level'] = $level->id;
                            require(ABSPATH . "/wp-content/plugins/paid-memberships-pro/preheaders/checkout.php");
                            ?>
                            <form class="pmpro_form" action="" method="post">
                                <div class="button bussines__btn">
                                    <input type="hidden" name="level_id" value="<?php echo $level->id?>" />
                                    <input type="hidden" name="submit-checkout" value="<?php echo $level->id?>" />
                                    <?php if($gateway == "paypalexpress" && $pmpro_requirebilling) { ?>
                                        <input type="submit" class="pmpro_btn-submit-checkout btn <?php if($customerMembershipLevel == $level->id){echo 'your__level';}?>" value="<?php echo $customerMembershipLevel == $level->id ? get_field('your_plan', 6179) : get_field('subscribe', 6179) ?>" name="<?php echo $level->id?>">
                                    <?php } else { ?>
                                        <span id="pmpro_submit_span"><input type="submit" class="pmpro_btn-submit-checkout btn <?php if($customerMembershipLevel == $level->id){echo 'your__level';}?>" value="<?php echo $customerMembershipLevel == $level->id ? get_field('your_plan', 6179) : get_field('subscribe', 6179) ?>" /></span>
                                    <?php } ?>
                                </div>
                            </form>
                        <?php } } ?>
                </td>
                <td>
                    <p class="pricing__title">Senior</p>
                    <p class="pricing__descript"><?=t($pricing['business_description']);?></p>
                    <?php
                    foreach ($pmpro_levels as $level) {
                        if ($level->name == 'Senior') {
                            $_REQUEST['level'] = $level->id;
                            require(ABSPATH . "/wp-content/plugins/paid-memberships-pro/preheaders/checkout.php");
                            ?>
                            <form class="pmpro_form" action="" method="post">
                                <div class="button enterprise__btn">
                                    <input type="hidden" name="level_id" value="<?php echo $level->id?>" />
                                    <input type="hidden" name="submit-checkout" value="<?php echo $level->id?>" />
                                    <?php if($gateway == "paypalexpress" && $pmpro_requirebilling) { ?>
                                        <input type="submit" class="pmpro_btn-submit-checkout btn <?php if($customerMembershipLevel == $level->id){echo 'your__level';}?>" value="<?php echo $customerMembershipLevel == $level->id ? get_field('your_plan', 6179) : get_field('subscribe', 6179) ?>" name="<?php echo $level->id?>">
                                    <?php } else { ?>
                                        <span id="pmpro_submit_span"><input type="submit" class="pmpro_btn-submit-checkout btn <?php if($customerMembershipLevel == $level->id){echo 'your__level';}?>" value="<?php echo $customerMembershipLevel == $level->id ? get_field('your_plan', 6179) : get_field('subscribe', 6179) ?>" /></span>
                                    <?php } ?>
                                </div>
                            </form>
                        <?php } } ?>
                </td>
            </tr>
            <tr class="pricingAll_block-tr">
                <td><?=t($pricing['pricing']);?></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td><p class="title"><?=t($pricing['platform_price_per_month']);?></p></td>
                <td><div class="price"><span>$</span><i class="startup__cost"><?php
                            foreach ($pmpro_levels as $level) {
                                if ($level->name == 'Junior') {
                                    $_REQUEST['level'] = $level->id;
                                    require(ABSPATH . "/wp-content/plugins/paid-memberships-pro/preheaders/checkout.php");
                                    ?><?php echo $level->initial_payment; ?><?php }
                            } ?></i></div></td>
                <td><div class="price"><span>$</span><i class="bussines__cost"><?php foreach ($pmpro_levels as $level) {
                                if ($level->name == 'Middle') {
                                    $_REQUEST['level'] = $level->id;
                                    require(ABSPATH . "/wp-content/plugins/paid-memberships-pro/preheaders/checkout.php");
                                    ?><?php echo $level->initial_payment; ?><?php }} ?></i></div></td>
                <td><div class="price"><span>$</span><i class="enterprise__cost"><?php
                            foreach ($pmpro_levels as $level) {
                                if ($level->name == 'Senior') {
                                    $_REQUEST['level'] = $level->id;
                                    require(ABSPATH . "/wp-content/plugins/paid-memberships-pro/preheaders/checkout.php");
                                    ?><?php echo $level->initial_payment; ?><?php }
                            } ?></i></div></td>
            </tr>
            <tr class="pricingAll_block-tr">
                <td><p><?=t($pricing['limits']);?></p></td>
                <td></td>
                <td></td>
                <td>
                    <p><?=t($pricing['need_more_limits']);?></p>
                </td>
            </tr>
            <tr>
                <td>
                    <p class="title"><?=t($pricing['websites']);?>
                        <button type="button" class="question">
                            <svg width="22" height="22" viewBox="0 0 22 22" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M11 17.3379C11.5933 17.3379 12.0742 16.8569 12.0742 16.2637C12.0742 15.6704 11.5933 15.1895 11 15.1895C10.4067 15.1895 9.92578 15.6704 9.92578 16.2637C9.92578 16.8569 10.4067 17.3379 11 17.3379Z" fill="#D8D8D8"></path>
                                <path d="M11 0C4.92061 0 0 4.91979 0 11C0 17.0794 4.91979 22 11 22C17.0794 22 22 17.0802 22 11C22 4.92061 17.0802 0 11 0ZM11 20.2812C5.87052 20.2812 1.71875 16.1302 1.71875 11C1.71875 5.87052 5.86983 1.71875 11 1.71875C16.1295 1.71875 20.2812 5.86983 20.2812 11C20.2812 16.1295 16.1302 20.2812 11 20.2812Z" fill="#D8D8D8"></path>
                                <path d="M11 5.52148C9.10456 5.52148 7.5625 7.06355 7.5625 8.95898C7.5625 9.43362 7.94724 9.81836 8.42188 9.81836C8.89651 9.81836 9.28125 9.43362 9.28125 8.95898C9.28125 8.01127 10.0523 7.24023 11 7.24023C11.9477 7.24023 12.7188 8.01127 12.7188 8.95898C12.7188 9.9067 11.9477 10.6777 11 10.6777C10.5254 10.6777 10.1406 11.0625 10.1406 11.5371V13.6855C10.1406 14.1602 10.5254 14.5449 11 14.5449C11.4746 14.5449 11.8594 14.1602 11.8594 13.6855V12.2877C13.3403 11.9052 14.4375 10.5578 14.4375 8.95898C14.4375 7.06355 12.8954 5.52148 11 5.52148Z" fill="#D8D8D8"></path>
                            </svg>
                            <span class="tooltip"><?=t($pricing['websites_tooltip']);?></span>
                        </button>
                    </p>
                </td>
                <td><span class="domains__count">1</span></td>
                <td><span class="domains__count">1</span></td>
                <td><span class="domains__count">1</span></td>
            </tr>
            <tr>
                <td>
                    <p class="title"><?=t($pricing['urls_per_website']);?>
                        <button type="button" class="question">
                            <svg width="22" height="22" viewBox="0 0 22 22" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M11 17.3379C11.5933 17.3379 12.0742 16.8569 12.0742 16.2637C12.0742 15.6704 11.5933 15.1895 11 15.1895C10.4067 15.1895 9.92578 15.6704 9.92578 16.2637C9.92578 16.8569 10.4067 17.3379 11 17.3379Z" fill="#D8D8D8"></path>
                                <path d="M11 0C4.92061 0 0 4.91979 0 11C0 17.0794 4.91979 22 11 22C17.0794 22 22 17.0802 22 11C22 4.92061 17.0802 0 11 0ZM11 20.2812C5.87052 20.2812 1.71875 16.1302 1.71875 11C1.71875 5.87052 5.86983 1.71875 11 1.71875C16.1295 1.71875 20.2812 5.86983 20.2812 11C20.2812 16.1295 16.1302 20.2812 11 20.2812Z" fill="#D8D8D8"></path>
                                <path d="M11 5.52148C9.10456 5.52148 7.5625 7.06355 7.5625 8.95898C7.5625 9.43362 7.94724 9.81836 8.42188 9.81836C8.89651 9.81836 9.28125 9.43362 9.28125 8.95898C9.28125 8.01127 10.0523 7.24023 11 7.24023C11.9477 7.24023 12.7188 8.01127 12.7188 8.95898C12.7188 9.9067 11.9477 10.6777 11 10.6777C10.5254 10.6777 10.1406 11.0625 10.1406 11.5371V13.6855C10.1406 14.1602 10.5254 14.5449 11 14.5449C11.4746 14.5449 11.8594 14.1602 11.8594 13.6855V12.2877C13.3403 11.9052 14.4375 10.5578 14.4375 8.95898C14.4375 7.06355 12.8954 5.52148 11 5.52148Z" fill="#D8D8D8"></path>
                            </svg>
                            <span class="tooltip"><?=t($pricing['urls_per_website_tooltip']);?></span>
                        </button>
                    </p>
                </td>
                <td>100</td>
                <td>1000</td>
                <td>10000</td>
            </tr>
            <tr>
                <td>
                    <p class="title"><?=t($pricing['keywords_per_website']);?>
                        <button type="button" class="question">
                            <svg width="22" height="22" viewBox="0 0 22 22" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M11 17.3379C11.5933 17.3379 12.0742 16.8569 12.0742 16.2637C12.0742 15.6704 11.5933 15.1895 11 15.1895C10.4067 15.1895 9.92578 15.6704 9.92578 16.2637C9.92578 16.8569 10.4067 17.3379 11 17.3379Z" fill="#D8D8D8"></path>
                                <path d="M11 0C4.92061 0 0 4.91979 0 11C0 17.0794 4.91979 22 11 22C17.0794 22 22 17.0802 22 11C22 4.92061 17.0802 0 11 0ZM11 20.2812C5.87052 20.2812 1.71875 16.1302 1.71875 11C1.71875 5.87052 5.86983 1.71875 11 1.71875C16.1295 1.71875 20.2812 5.86983 20.2812 11C20.2812 16.1295 16.1302 20.2812 11 20.2812Z" fill="#D8D8D8"></path>
                                <path d="M11 5.52148C9.10456 5.52148 7.5625 7.06355 7.5625 8.95898C7.5625 9.43362 7.94724 9.81836 8.42188 9.81836C8.89651 9.81836 9.28125 9.43362 9.28125 8.95898C9.28125 8.01127 10.0523 7.24023 11 7.24023C11.9477 7.24023 12.7188 8.01127 12.7188 8.95898C12.7188 9.9067 11.9477 10.6777 11 10.6777C10.5254 10.6777 10.1406 11.0625 10.1406 11.5371V13.6855C10.1406 14.1602 10.5254 14.5449 11 14.5449C11.4746 14.5449 11.8594 14.1602 11.8594 13.6855V12.2877C13.3403 11.9052 14.4375 10.5578 14.4375 8.95898C14.4375 7.06355 12.8954 5.52148 11 5.52148Z" fill="#D8D8D8"></path>
                            </svg>
                            <span class="tooltip"><?=t($pricing['keywords_per_website_tooltip']);?></span>
                        </button>
                    </p>
                </td>
                <td>50</td>
                <td>200</td>
                <td>500</td>
            </tr>
            <tr>
                <td>
                    <p class="title"><?=t($pricing['backlinks_per_website']);?>
                        <button type="button" class="question">
                            <svg width="22" height="22" viewBox="0 0 22 22" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M11 17.3379C11.5933 17.3379 12.0742 16.8569 12.0742 16.2637C12.0742 15.6704 11.5933 15.1895 11 15.1895C10.4067 15.1895 9.92578 15.6704 9.92578 16.2637C9.92578 16.8569 10.4067 17.3379 11 17.3379Z" fill="#D8D8D8"></path>
                                <path d="M11 0C4.92061 0 0 4.91979 0 11C0 17.0794 4.91979 22 11 22C17.0794 22 22 17.0802 22 11C22 4.92061 17.0802 0 11 0ZM11 20.2812C5.87052 20.2812 1.71875 16.1302 1.71875 11C1.71875 5.87052 5.86983 1.71875 11 1.71875C16.1295 1.71875 20.2812 5.86983 20.2812 11C20.2812 16.1295 16.1302 20.2812 11 20.2812Z" fill="#D8D8D8"></path>
                                <path d="M11 5.52148C9.10456 5.52148 7.5625 7.06355 7.5625 8.95898C7.5625 9.43362 7.94724 9.81836 8.42188 9.81836C8.89651 9.81836 9.28125 9.43362 9.28125 8.95898C9.28125 8.01127 10.0523 7.24023 11 7.24023C11.9477 7.24023 12.7188 8.01127 12.7188 8.95898C12.7188 9.9067 11.9477 10.6777 11 10.6777C10.5254 10.6777 10.1406 11.0625 10.1406 11.5371V13.6855C10.1406 14.1602 10.5254 14.5449 11 14.5449C11.4746 14.5449 11.8594 14.1602 11.8594 13.6855V12.2877C13.3403 11.9052 14.4375 10.5578 14.4375 8.95898C14.4375 7.06355 12.8954 5.52148 11 5.52148Z" fill="#D8D8D8"></path>
                            </svg>
                            <span class="tooltip"><?=t($pricing['backlinks_per_website_tooltip']);?></span>
                        </button>
                    </p>
                </td>
                <td>100</td>
                <td>500</td>
                <td>1000</td>
            </tr>
            <tr>
                <td>
                    <p class="title"><?=t($pricing['website_monitoring_frequency']);?>
                        <button type="button" class="question">
                            <svg width="22" height="22" viewBox="0 0 22 22" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M11 17.3379C11.5933 17.3379 12.0742 16.8569 12.0742 16.2637C12.0742 15.6704 11.5933 15.1895 11 15.1895C10.4067 15.1895 9.92578 15.6704 9.92578 16.2637C9.92578 16.8569 10.4067 17.3379 11 17.3379Z" fill="#D8D8D8"></path>
                                <path d="M11 0C4.92061 0 0 4.91979 0 11C0 17.0794 4.91979 22 11 22C17.0794 22 22 17.0802 22 11C22 4.92061 17.0802 0 11 0ZM11 20.2812C5.87052 20.2812 1.71875 16.1302 1.71875 11C1.71875 5.87052 5.86983 1.71875 11 1.71875C16.1295 1.71875 20.2812 5.86983 20.2812 11C20.2812 16.1295 16.1302 20.2812 11 20.2812Z" fill="#D8D8D8"></path>
                                <path d="M11 5.52148C9.10456 5.52148 7.5625 7.06355 7.5625 8.95898C7.5625 9.43362 7.94724 9.81836 8.42188 9.81836C8.89651 9.81836 9.28125 9.43362 9.28125 8.95898C9.28125 8.01127 10.0523 7.24023 11 7.24023C11.9477 7.24023 12.7188 8.01127 12.7188 8.95898C12.7188 9.9067 11.9477 10.6777 11 10.6777C10.5254 10.6777 10.1406 11.0625 10.1406 11.5371V13.6855C10.1406 14.1602 10.5254 14.5449 11 14.5449C11.4746 14.5449 11.8594 14.1602 11.8594 13.6855V12.2877C13.3403 11.9052 14.4375 10.5578 14.4375 8.95898C14.4375 7.06355 12.8954 5.52148 11 5.52148Z" fill="#D8D8D8"></path>
                            </svg>
                            <span class="tooltip"><?=t($pricing['website_monitoring_frequency_tooltip']);?></span>
                        </button>
                    </p>
                </td>
                <td><?=t($pricing['hourly']);?></td>
                <td><?=t($pricing['hourly']);?></td>
                <td><?=t($pricing['hourly']);?></td>
            </tr>
            <tr>
                <td>
                    <p class="title"><?=t($pricing['keywords_tracking_frequency']);?>
                        <button type="button" class="question">
                            <svg width="22" height="22" viewBox="0 0 22 22" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M11 17.3379C11.5933 17.3379 12.0742 16.8569 12.0742 16.2637C12.0742 15.6704 11.5933 15.1895 11 15.1895C10.4067 15.1895 9.92578 15.6704 9.92578 16.2637C9.92578 16.8569 10.4067 17.3379 11 17.3379Z" fill="#D8D8D8"></path>
                                <path d="M11 0C4.92061 0 0 4.91979 0 11C0 17.0794 4.91979 22 11 22C17.0794 22 22 17.0802 22 11C22 4.92061 17.0802 0 11 0ZM11 20.2812C5.87052 20.2812 1.71875 16.1302 1.71875 11C1.71875 5.87052 5.86983 1.71875 11 1.71875C16.1295 1.71875 20.2812 5.86983 20.2812 11C20.2812 16.1295 16.1302 20.2812 11 20.2812Z" fill="#D8D8D8"></path>
                                <path d="M11 5.52148C9.10456 5.52148 7.5625 7.06355 7.5625 8.95898C7.5625 9.43362 7.94724 9.81836 8.42188 9.81836C8.89651 9.81836 9.28125 9.43362 9.28125 8.95898C9.28125 8.01127 10.0523 7.24023 11 7.24023C11.9477 7.24023 12.7188 8.01127 12.7188 8.95898C12.7188 9.9067 11.9477 10.6777 11 10.6777C10.5254 10.6777 10.1406 11.0625 10.1406 11.5371V13.6855C10.1406 14.1602 10.5254 14.5449 11 14.5449C11.4746 14.5449 11.8594 14.1602 11.8594 13.6855V12.2877C13.3403 11.9052 14.4375 10.5578 14.4375 8.95898C14.4375 7.06355 12.8954 5.52148 11 5.52148Z" fill="#D8D8D8"></path>
                            </svg>
                            <span class="tooltip"><?=t($pricing['keywords_tracking_frequency_tooltip']);?></span>
                        </button>
                    </p>
                </td>
                <td><?=t($pricing['daily']);?></td>
                <td><?=t($pricing['daily']);?></td>
                <td><?=t($pricing['daily']);?></td>
            </tr>
            <tr>
                <td>
                    <p class="title"><?=t($pricing['backlinks_tracking_frequency']);?>
                        <button type="button" class="question">
                            <svg width="22" height="22" viewBox="0 0 22 22" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M11 17.3379C11.5933 17.3379 12.0742 16.8569 12.0742 16.2637C12.0742 15.6704 11.5933 15.1895 11 15.1895C10.4067 15.1895 9.92578 15.6704 9.92578 16.2637C9.92578 16.8569 10.4067 17.3379 11 17.3379Z" fill="#D8D8D8"></path>
                                <path d="M11 0C4.92061 0 0 4.91979 0 11C0 17.0794 4.91979 22 11 22C17.0794 22 22 17.0802 22 11C22 4.92061 17.0802 0 11 0ZM11 20.2812C5.87052 20.2812 1.71875 16.1302 1.71875 11C1.71875 5.87052 5.86983 1.71875 11 1.71875C16.1295 1.71875 20.2812 5.86983 20.2812 11C20.2812 16.1295 16.1302 20.2812 11 20.2812Z" fill="#D8D8D8"></path>
                                <path d="M11 5.52148C9.10456 5.52148 7.5625 7.06355 7.5625 8.95898C7.5625 9.43362 7.94724 9.81836 8.42188 9.81836C8.89651 9.81836 9.28125 9.43362 9.28125 8.95898C9.28125 8.01127 10.0523 7.24023 11 7.24023C11.9477 7.24023 12.7188 8.01127 12.7188 8.95898C12.7188 9.9067 11.9477 10.6777 11 10.6777C10.5254 10.6777 10.1406 11.0625 10.1406 11.5371V13.6855C10.1406 14.1602 10.5254 14.5449 11 14.5449C11.4746 14.5449 11.8594 14.1602 11.8594 13.6855V12.2877C13.3403 11.9052 14.4375 10.5578 14.4375 8.95898C14.4375 7.06355 12.8954 5.52148 11 5.52148Z" fill="#D8D8D8"></path>
                            </svg>
                            <span class="tooltip"><?=t($pricing['backlinks_tracking_frequency_tooltip']);?></span>
                        </button>
                    </p>
                </td>
                <td><?=t($pricing['daily']);?></td>
                <td><?=t($pricing['daily']);?></td>
                <td><?=t($pricing['daily']);?></td>
            </tr>
            <tr class="pricingAll_block-tr">
                <td><?=t($pricing['features']);?></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td>
                    <p class="title"><?=t($pricing['on_page_seo_audit']);?>
                        <button type="button" class="question">
                            <svg width="22" height="22" viewBox="0 0 22 22" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M11 17.3379C11.5933 17.3379 12.0742 16.8569 12.0742 16.2637C12.0742 15.6704 11.5933 15.1895 11 15.1895C10.4067 15.1895 9.92578 15.6704 9.92578 16.2637C9.92578 16.8569 10.4067 17.3379 11 17.3379Z" fill="#D8D8D8"></path>
                                <path d="M11 0C4.92061 0 0 4.91979 0 11C0 17.0794 4.91979 22 11 22C17.0794 22 22 17.0802 22 11C22 4.92061 17.0802 0 11 0ZM11 20.2812C5.87052 20.2812 1.71875 16.1302 1.71875 11C1.71875 5.87052 5.86983 1.71875 11 1.71875C16.1295 1.71875 20.2812 5.86983 20.2812 11C20.2812 16.1295 16.1302 20.2812 11 20.2812Z" fill="#D8D8D8"></path>
                                <path d="M11 5.52148C9.10456 5.52148 7.5625 7.06355 7.5625 8.95898C7.5625 9.43362 7.94724 9.81836 8.42188 9.81836C8.89651 9.81836 9.28125 9.43362 9.28125 8.95898C9.28125 8.01127 10.0523 7.24023 11 7.24023C11.9477 7.24023 12.7188 8.01127 12.7188 8.95898C12.7188 9.9067 11.9477 10.6777 11 10.6777C10.5254 10.6777 10.1406 11.0625 10.1406 11.5371V13.6855C10.1406 14.1602 10.5254 14.5449 11 14.5449C11.4746 14.5449 11.8594 14.1602 11.8594 13.6855V12.2877C13.3403 11.9052 14.4375 10.5578 14.4375 8.95898C14.4375 7.06355 12.8954 5.52148 11 5.52148Z" fill="#D8D8D8"></path>
                            </svg>
                            <span class="tooltip"><?=t($pricing['on_page_seo_audit_tooltip']);?></span>
                        </button>
                    </p>
                </td>
                <td><img src="<?= $template_directory_uri; ?>/out/img_design/checkout__yes.svg" alt="yes"></td>
                <td><img src="<?= $template_directory_uri; ?>/out/img_design/checkout__yes.svg" alt="yes"></td>
                <td><img src="<?= $template_directory_uri; ?>/out/img_design/checkout__yes.svg" alt="yes"></td>
            </tr>
            <tr>
                <td>
                    <p class="title"><?=t($pricing['branded_pdf_reports']);?>
                        <button type="button" class="question">
                            <svg width="22" height="22" viewBox="0 0 22 22" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M11 17.3379C11.5933 17.3379 12.0742 16.8569 12.0742 16.2637C12.0742 15.6704 11.5933 15.1895 11 15.1895C10.4067 15.1895 9.92578 15.6704 9.92578 16.2637C9.92578 16.8569 10.4067 17.3379 11 17.3379Z" fill="#D8D8D8"></path>
                                <path d="M11 0C4.92061 0 0 4.91979 0 11C0 17.0794 4.91979 22 11 22C17.0794 22 22 17.0802 22 11C22 4.92061 17.0802 0 11 0ZM11 20.2812C5.87052 20.2812 1.71875 16.1302 1.71875 11C1.71875 5.87052 5.86983 1.71875 11 1.71875C16.1295 1.71875 20.2812 5.86983 20.2812 11C20.2812 16.1295 16.1302 20.2812 11 20.2812Z" fill="#D8D8D8"></path>
                                <path d="M11 5.52148C9.10456 5.52148 7.5625 7.06355 7.5625 8.95898C7.5625 9.43362 7.94724 9.81836 8.42188 9.81836C8.89651 9.81836 9.28125 9.43362 9.28125 8.95898C9.28125 8.01127 10.0523 7.24023 11 7.24023C11.9477 7.24023 12.7188 8.01127 12.7188 8.95898C12.7188 9.9067 11.9477 10.6777 11 10.6777C10.5254 10.6777 10.1406 11.0625 10.1406 11.5371V13.6855C10.1406 14.1602 10.5254 14.5449 11 14.5449C11.4746 14.5449 11.8594 14.1602 11.8594 13.6855V12.2877C13.3403 11.9052 14.4375 10.5578 14.4375 8.95898C14.4375 7.06355 12.8954 5.52148 11 5.52148Z" fill="#D8D8D8"></path>
                            </svg>
                            <span class="tooltip"><?=t($pricing['branded_pdf_reports_tooltip']);?></span>
                        </button>
                    </p>
                </td>
                <td><img src="<?= $template_directory_uri; ?>/out/img_design/checkout__yes.svg" alt="no"></td>
                <td><img src="<?= $template_directory_uri; ?>/out/img_design/checkout__yes.svg" alt="no"></td>
                <td><img src="<?= $template_directory_uri; ?>/out/img_design/checkout__yes.svg" alt="yes"></td>
            </tr>
            <tr>
                <td>
                    <p class="title"><?=t($pricing['sharing']);?>
                        <button type="button" class="question">
                            <svg width="22" height="22" viewBox="0 0 22 22" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M11 17.3379C11.5933 17.3379 12.0742 16.8569 12.0742 16.2637C12.0742 15.6704 11.5933 15.1895 11 15.1895C10.4067 15.1895 9.92578 15.6704 9.92578 16.2637C9.92578 16.8569 10.4067 17.3379 11 17.3379Z" fill="#D8D8D8"></path>
                                <path d="M11 0C4.92061 0 0 4.91979 0 11C0 17.0794 4.91979 22 11 22C17.0794 22 22 17.0802 22 11C22 4.92061 17.0802 0 11 0ZM11 20.2812C5.87052 20.2812 1.71875 16.1302 1.71875 11C1.71875 5.87052 5.86983 1.71875 11 1.71875C16.1295 1.71875 20.2812 5.86983 20.2812 11C20.2812 16.1295 16.1302 20.2812 11 20.2812Z" fill="#D8D8D8"></path>
                                <path d="M11 5.52148C9.10456 5.52148 7.5625 7.06355 7.5625 8.95898C7.5625 9.43362 7.94724 9.81836 8.42188 9.81836C8.89651 9.81836 9.28125 9.43362 9.28125 8.95898C9.28125 8.01127 10.0523 7.24023 11 7.24023C11.9477 7.24023 12.7188 8.01127 12.7188 8.95898C12.7188 9.9067 11.9477 10.6777 11 10.6777C10.5254 10.6777 10.1406 11.0625 10.1406 11.5371V13.6855C10.1406 14.1602 10.5254 14.5449 11 14.5449C11.4746 14.5449 11.8594 14.1602 11.8594 13.6855V12.2877C13.3403 11.9052 14.4375 10.5578 14.4375 8.95898C14.4375 7.06355 12.8954 5.52148 11 5.52148Z" fill="#D8D8D8"></path>
                            </svg>
                            <span class="tooltip"><?=t($pricing['sharing_tooltip']);?></span>
                        </button>
                    </p>
                </td>
                <td><img src="<?= $template_directory_uri; ?>/out/img_design/checkout__yes.svg" alt="yes"></td>
                <td><img src="<?= $template_directory_uri; ?>/out/img_design/checkout__yes.svg" alt="yes"></td>
                <td><img src="<?= $template_directory_uri; ?>/out/img_design/checkout__yes.svg" alt="yes"></td>
            </tr>
            <tr class="pricingAll_block-tr">
                <td><?=t($pricing['integrations']);?></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td>
                    <p class="title"><?=t($pricing['google_analytics']);?></p>
                </td>
                <td><img src="<?= $template_directory_uri; ?>/out/img_design/checkout__yes.svg" alt="yes"></td>
                <td><img src="<?= $template_directory_uri; ?>/out/img_design/checkout__yes.svg" alt="yes"></td>
                <td><img src="<?= $template_directory_uri; ?>/out/img_design/checkout__yes.svg" alt="yes"></td>
            </tr>
            <tr>
                <td>
                    <p class="title"><?=t($pricing['google_search_console']);?></p>
                </td>
                <td><img src="<?= $template_directory_uri; ?>/out/img_design/checkout__yes.svg" alt="yes"></td>
                <td><img src="<?= $template_directory_uri; ?>/out/img_design/checkout__yes.svg" alt="yes"></td>
                <td><img src="<?= $template_directory_uri; ?>/out/img_design/checkout__yes.svg" alt="yes"></td>
            </tr>
        </table>
    </div>
</div>
<div class="checkout__faq">
    <div class="wrapper">
        <div class="checkout__faq-title"><?=t($pricing['frequently_asked_questions']);?></div>
        <div class="checkout__faq-body">
            <?=t($pricing['faq_title_1']);?>
        </div>
    </div>
</div>

<?php get_footer('tmp_design'); ?>