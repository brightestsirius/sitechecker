<?php
    /* Template Name: On Page SEO Checker */
    get_header('tmp_design');
    $template_directory_uri = get_template_directory_uri();
    $meta = get_post_meta_all($post->ID);
?>
<main class="audit__landing checker__landing">
    <div class="audit__first">
        <div class="wrapper audit__first-container">
            <h1><?php echo t($meta['title']);?></h1>
            <h2><?php echo t($meta['description']);?></h2>
            <form class="article__seo-search form__Check checker__form" name="seo-report">
                <span class="error">Must be a valid URL with http:// or https://</span>
                <input type="text" placeholder="<?php echo t($meta['placeholder']);?>">
                <button type="submit"><img src="<?= $template_directory_uri; ?>/out/img_design/arrow.svg" alt="arrow" title="arrow" class="arrow"><img src="<?= $template_directory_uri; ?>/out/img_design/loading.svg" alt="arrow" title="arrow" class="loading"></button>
            </form>
        </div>
    </div>
    <div class="audit__second checker__second">
        <div class="wrapper">
            <p class="audit__title"><?php echo t($meta['page_health_title']);?></p>
            <div class="audit__second-content checker__second-content">
                <div class="audit__second-left checker__second-left">
                    <div class="audit__matter">
                        <img src="<?= $template_directory_uri; ?>/out/img_design/matter_1.svg" alt="audit" title="audit">
                        <div>
                            <p class="audit__matter-title"><?php echo t($meta['page_health_title_1']);?></p>
                            <p class="audit__matter-description"><?php echo t($meta['page_health_description_1']);?></p>
                        </div>
                    </div>
                </div>
                <div class="audit__second-right checker__second-right">
                    <div class="audit__matter">
                        <img src="<?= $template_directory_uri; ?>/out/img_design/matter_2.svg" alt="audit" title="audit">
                        <div>
                            <p class="audit__matter-title"><?php echo t($meta['page_health_title_2']);?></p>
                            <p class="audit__matter-description"><?php echo t($meta['page_health_description_2']);?></p>
                        </div>
                    </div>
                    <div class="audit__matter">
                        <img src="<?= $template_directory_uri; ?>/out/img_design/matter_3.svg" alt="audit" title="audit">
                        <div>
                            <p class="audit__matter-title"><?php echo t($meta['page_health_title_3']);?></p>
                            <p class="audit__matter-description"><?php echo t($meta['page_health_description_3']);?></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="audit__third checker__third">
        <div class="wrapper">
            <p class="audit__title"><?php echo t($meta['cms_title']);?></p>
            <div class="audit__third-content">
                <div class="audit__third-cont">
                    <div class="audit__third-block">
                        <img src="<?= $template_directory_uri; ?>/out/img_design/cms_1.svg" alt="cms" title="cms">
                    </div>
                </div>
                <div class="audit__third-cont">
                    <div class="audit__third-block">
                        <img src="<?= $template_directory_uri; ?>/out/img_design/cms_2.svg" alt="cms" title="cms">
                    </div>
                </div>
                <div class="audit__third-cont">
                    <div class="audit__third-block">
                        <img src="<?= $template_directory_uri; ?>/out/img_design/cms_3.svg" alt="cms" title="cms">
                    </div>
                </div>
                <div class="audit__third-cont">
                    <div class="audit__third-block">
                        <img src="<?= $template_directory_uri; ?>/out/img_design/cms_4.svg" alt="cms" title="cms">
                    </div>
                </div>
                <div class="audit__third-cont">
                    <div class="audit__third-block">
                        <img src="<?= $template_directory_uri; ?>/out/img_design/cms_5.svg" alt="cms" title="cms">
                    </div>
                </div>
                <div class="audit__third-cont">
                    <div class="audit__third-block">
                        <img src="<?= $template_directory_uri; ?>/out/img_design/cms_6.svg" alt="cms" title="cms">
                    </div>
                </div>
                <div class="audit__third-cont">
                    <div class="audit__third-block">
                        <img src="<?= $template_directory_uri; ?>/out/img_design/cms_7.svg" alt="cms" title="cms">
                    </div>
                </div>
                <div class="audit__third-cont">
                    <div class="audit__third-block">
                        <img src="<?= $template_directory_uri; ?>/out/img_design/cms_8.svg" alt="cms" title="cms">
                    </div>
                </div>
                <div class="audit__third-cont">
                    <div class="audit__third-block">
                        <img src="<?= $template_directory_uri; ?>/out/img_design/cms_9.svg" alt="cms" title="cms">
                    </div>
                </div>
                <div class="audit__third-cont">
                    <div class="audit__third-block">
                        <p>Your Сustom CMS</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="audit__fourth checker__fourth">
        <div class="wrapper">
            <p class="audit__title"><?php echo t($meta['helps_title']);?></p>
            <div id="audit__carousel_helps" class="carousel slide carousel-fade audit__carousel checker__carousel" data-ride="carousel">
                <div class="carousel-inner">
                    <div class="carousel-item active">
                        <div class="audit__helps">
                            <div class="audit__helps-text">
                                <p class="audit__helps-title"><?php echo t($meta['helps_title_1']);?></p>
                                <div class="audit__helps-descr"><?php echo t($meta['helps_description_1']);?></div>
                            </div>
                            <div class="audit__helps__img">
                                <img src="<?= $template_directory_uri; ?>/out/img_design/auditOne.svg" alt="audit__helps" title="audit__helps" class="audit__helps-img">
                            </div>
                        </div>
                    </div>
                    <div class="carousel-item">
                        <div class="audit__helps">
                            <div class="audit__helps-text">
                                <p class="audit__helps-title"><?php echo t($meta['helps_title_2']);?></p>
                                <div class="audit__helps-descr"><?php echo t($meta['helps_description_2']);?></div>
                            </div>
                            <div class="audit__helps__img">
                                <img src="<?= $template_directory_uri; ?>/out/img_design/auditTwo.svg" alt="audit__helps" title="audit__helps" class="audit__helps-img">
                            </div>
                        </div>
                    </div>
                    <div class="carousel-item">
                        <div class="audit__helps">
                            <div class="audit__helps-text">
                                <p class="audit__helps-title"><?php echo t($meta['helps_title_3']);?></p>
                                <div class="audit__helps-descr"><?php echo t($meta['helps_description_3']);?></div>
                            </div>
                            <div class="audit__helps__img">
                                <img src="<?= $template_directory_uri; ?>/out/img_design/auditThree.svg" alt="audit__helps" title="audit__helps" class="audit__helps-img">
                            </div>
                        </div>
                    </div>
                    <div class="carousel-item">
                        <div class="audit__helps">
                            <div class="audit__helps-text">
                                <p class="audit__helps-title"><?php echo t($meta['helps_title_4']);?></p>
                                <div class="audit__helps-descr"><?php echo t($meta['helps_description_4']);?></div>
                            </div>
                            <div class="audit__helps__img">
                                <img src="<?= $template_directory_uri; ?>/out/img_design/auditFour.svg" alt="audit__helps" title="audit__helps" class="audit__helps-img">
                            </div>
                        </div>
                    </div>
                    <div class="carousel-item">
                        <div class="audit__helps">
                            <div class="audit__helps-text">
                                <p class="audit__helps-title"><?php echo t($meta['helps_title_5']);?></p>
                                <div class="audit__helps-descr"><?php echo t($meta['helps_description_5']);?></div>
                            </div>
                            <div class="audit__helps__img">
                                <img src="<?= $template_directory_uri; ?>/out/img_design/auditThree.svg" alt="audit__helps" title="audit__helps" class="audit__helps-img">
                            </div>
                        </div>
                    </div>
                </div>
                <ol class="carousel-indicators">
                    <li data-target="#audit__carousel_helps" data-slide-to="0" class="active"></li>
                    <li data-target="#audit__carousel_helps" data-slide-to="1"></li>
                    <li data-target="#audit__carousel_helps" data-slide-to="2"></li>
                    <li data-target="#audit__carousel_helps" data-slide-to="3"></li>
                    <li data-target="#audit__carousel_helps" data-slide-to="4"></li>
                </ol>
                <a class="carousel-control-prev" href="#audit__carousel_helps" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                </a>
                <a class="carousel-control-next" href="#audit__carousel_helps" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                </a>
            </div>
        </div>
    </div>
    <div class="front__check check__six">
        <div class="wrapper">
            <img src="<?= $template_directory_uri; ?>/out/img_design/logo__white.svg" alt="logo__white" title="logo__white" class="front__check-logo">
            <p class="title"><?php echo t($meta['checker_banner']);?></p>
            <form class="article__seo-search form__Check checker__form" name="seo-report">
                <span class="error">Must be a valid URL with http:// or https://</span>
                <input type="text" placeholder="<?php echo t($meta['placeholder']);?>">
                <button type="submit" class="<?php echo $crawlButton; ?>"><img src="<?= $template_directory_uri; ?>/out/img_design/arrow.svg" alt="arrow" title="arrow" class="arrow"><img src="<?= $template_directory_uri; ?>/out/img_design/loading.svg" alt="arrow" title="arrow" class="loading"></button>
            </form>
        </div>
    </div>
    <div class="audit__seven checker__fourth">
        <div class="wrapper">
            <p class="audit__title"><?php echo t($meta['how_it_works']);?></p>
            <div class="audit__seven-container">
                <div class="audit__hiw">
                    <img src="<?= $template_directory_uri; ?>/out/img_design/audit__hiw-icon1.svg" alt="how it works" title="how it works">
                    <p class="title"><?php echo t($meta['how_it_works_title_1']);?></p>
                    <p class="description"><?php echo t($meta['how_it_works_description_1']);?></p>
                </div>
                <div class="audit__hiw">
                    <img src="<?= $template_directory_uri; ?>/out/img_design/audit__hiw-icon2.svg" alt="how it works" title="how it works">
                    <p class="title"><?php echo t($meta['how_it_works_title_2']);?></p>
                    <p class="description"><?php echo t($meta['how_it_works_description_2']);?></p>
                </div>
                <div class="audit__hiw">
                    <img src="<?= $template_directory_uri; ?>/out/img_design/audit__hiw-icon3.svg" alt="how it works" title="how it works">
                    <p class="title"><?php echo t($meta['how_it_works_title_3']);?></p>
                    <p class="description"><?php echo t($meta['how_it_works_description_3']);?></p>
                </div>
            </div>
        </div>
    </div>
    <div class="audit__nine checker__nine">
        <div class="wrapper">
            <p class="audit__title"><?php echo t($meta['stats']);?></p>
            <div class="audit__nine-container">
                <div class="audit__stat">
                    <p class="number" id="audit__stat1">7352</p>
                    <p class="description"><?php echo t($meta['stats_title_1']);?></p>
                </div>
                <div class="audit__stat">
                    <p class="number" id="audit__stat2">2317931</p>
                    <p class="description"><?php echo t($meta['stats_title_2']);?></p>
                </div>
                <div class="audit__stat">
                    <p class="number" id="audit__stat3">3064534</p>
                    <p class="description"><?php echo t($meta['stats_title_3']);?></p>
                </div>
            </div>
        </div>
    </div>
    <div class="audit__eleven checker__eleven">
        <div class="wrapper">
            <div class="audit__eleven-form">
                <div class="left">
                    <img src="<?= $template_directory_uri; ?>/out/img_design/checker__convert__img.svg" alt="audit" title="audit">
                </div>
                <div class="right">
                    <p class="audit__eleven-title"><?php echo t($meta['checker_audit_banner_title']);?></p>
                    <ul class="audit__eleven-list"><?php echo t($meta['checker_audit_banner_list']);?></ul>
                    <form class="article__seo-search form__Check checker__form">
                        <span class="error">Must be a valid URL with http:// or https://</span>
                        <input type="text" placeholder="<?php echo t($meta['checker_audit_banner_placeholder']);?>">
                        <button type="submit" class="<?php echo $monitoringButton; ?>"><img src="<?= $template_directory_uri; ?>/out/img_design/arrow.svg" alt="arrow" title="arrow" class="arrow"><img src="<?= $template_directory_uri; ?>/out/img_design/loading.svg" alt="arrow" title="arrow" class="loading"></button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="stars__block">
        <?php if(function_exists('the_ratings')) { the_ratings(); } ?>
    </div>
</main>
<?php get_footer('tmp_design'); ?>