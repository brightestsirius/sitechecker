<?php
$pages_array = [
    0 => 'robots-tester',
    1 => '404-error',
    2 => 'what-is-favicon',
    3 => 'what-is-url',
    4 => 'h1-tag',
    5 => 'meta-title',
    6 => 'meta-tag-description',
    7 => 'http-vs-https',
    8 => 'www-vs-non-www',
    9 => 'canonical-url',
    10 => 'internal-links',
    11 => 'alt-tags',
    12 => 'http-status-codes',
    13 => 'pagination',
    14 => 'external-links',
    15 => 'page-size',
    16 => 'google-index',
    17 => 'redirect-checker',
    18 => 'google-cache',
    19 => 'backlinks-generator',
    20 => 'rank-checker',
    21 => 'what-is-cms'
];
$path_only = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
$trigger = false;
foreach ($pages_array as $page) {
    if (strpos($path_only, $page)) {
        $trigger = true;
    }
};
$template_directory_uri = get_template_directory_uri();
$meta = get_post_meta_all(get_option('page_on_front'));
$metaDesign = get_post_meta_all(url_to_postid( '/header-footer-design/' ));
$metaPage = get_post_meta_all($post->ID);
$dict = getApiDictionary();
$tt = function ($key) use ($dict) {
    return $dict[$key] ?? $key;
};
?>

    <script>
        const locale = "<?=qtranxf_getLanguage();?>";
        const default_locale = "<?=qtranxf_getLanguageDefault();?>";
        const trans = JSON.parse('<?=json_encode(getApiDictionary(), JSON_HEX_APOS | JSON_HEX_AMP | JSON_HEX_QUOT);?>'.replace(/\\'/g, "\'"));
        const lang_cookie_name = "qtrans_front_language";
    </script>
<?php if (have_posts()): while (have_posts()) : the_post(); ?>
    <main>
        <div class="main article__page">
            <div class="wrapper">
                <div class="navigation">
                    <a href="<?= home_url(); ?>/" class="active">Home</a>
                    <svg width="9" height="5" viewBox="0 0 9 5" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd" clip-rule="evenodd"
                              d="M4.499 0L9 2.5V4.896L4.499 2.38L0 5V2.5L4.499 0Z"
                              transform="translate(9 5) rotate(-180)" fill="#CCCCCC"/>
                    </svg>
                    <a href="/knowledge-base/" class="active"><?= t($meta['user.header.knowledge.base']); ?></a>
                    <svg width="9" height="5" viewBox="0 0 9 5" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd" clip-rule="evenodd"
                              d="M4.499 0L9 2.5V4.896L4.499 2.38L0 5V2.5L4.499 0Z"
                              transform="translate(9 5) rotate(-180)" fill="#CCCCCC"/>
                    </svg>
                    <?php
                    $categories = get_the_category();
                    $category_name = $categories[0]->cat_name;
                    if ($category_name) {
                        ?>
                        <a href="<?php echo get_category_link($categories[0]->cat_ID); ?>"><?php echo $category_name; ?></a>
                        <svg width="9" height="5" viewBox="0 0 9 5" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path fill-rule="evenodd" clip-rule="evenodd"
                                  d="M4.499 0L9 2.5V4.896L4.499 2.38L0 5V2.5L4.499 0Z"
                                  transform="translate(9 5) rotate(-180)" fill="#CCCCCC"/>
                        </svg>
                    <?php } ?>
                    <p><?php echo get_post_meta($post->ID, 'page.h1', true) ?></p>
                </div>
            </div>
                <?php if ($trigger) : ?>
                    <div class="mini__tools-container backlinks mini__backlinks <?php echo $metaPage['minitool_height'] ? 'full__height' : '';?>">
                        <div class="wrapper">
                            <p id="minitool-title" class="backlinks__title"><?=t($metaPage['minitool_title']);?></p>
                            <p class="backlinks__description"><?php echo t($metaPage['minitool_description']);?></p>
                            <form class="home_first-search" id="mini__tool-form">
                                <span class="error">Must be a valid URL with http:// or https://</span>
                                <input id="minitool_input" type="text">
                                <button type="submit" id="minitool_check-landing"><img src="<?= $template_directory_uri; ?>/out/img_design/arrow.svg" alt="arrow" title="arrow" class="arrow"><img src="<?= $template_directory_uri; ?>/out/img_design/loading.svg" alt="arrow" title="arrow" class="loading"></button>
                            </form>
                        </div>
                    </div>
                    <div class="wrapper">
                        <div id="result-container">
                            <div class="container__results">
                                <div class="container__result" id="result-template">
                                        <div class="result">
                                            <?php if($post->ID === url_to_postid( '/backlinks-generator/' )){ ?>
                                                <div class="validation__param_block">
                                                    <table id="result-32" class="results-table">
                                                        <thead>
                                                        <tr>
                                                            <th>#</th>
                                                            <th>URL</th>
                                                            <th>Status</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody></tbody>
                                                    </table>
                                                </div>
                                            <?php } ?>
                                        </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endif; ?>
                <?php if($post->ID === 1063){ ?>
                        <div class="mini__tools-container backlinks mini__backlinks <?php echo $metaPage['minitool_height'] ? 'full__height' : '';?>">
                            <div class="wrapper">
                                <p id="minitool-title" class="backlinks__title"><?=t($metaPage['minitool_title']);?></p>
                                <p class="backlinks__description"><?php echo t($metaPage['minitool_description']);?></p>
                                <form class="home_first-search" id="speed__test-form">
                                    <span class="error">Must be a valid URL with http:// or https://</span>
                                    <input id="speed__test-url" type="text" placeholder="Enter URL to test">
                                    <button type="submit"><img src="<?= $template_directory_uri; ?>/out/img_design/arrow.svg" alt="arrow" title="arrow" class="arrow"><img src="<?= $template_directory_uri; ?>/out/img_design/loading.svg" alt="arrow" title="arrow" class="loading"></button>
                                </form>
                            </div>
                        </div>
                    <div class="speed__testData_container">
                        <div class="wrapper">
                            <div class="speed__testData">
                                <div class="speed__test-info">
                                    <div class="speed__test-average">
                                        <div class="title">Page Speed</div>
                                        <div class="info">
                                            <div class="info__title">Average</div>
                                            <div class="info__description">
                                                <div>First contentful paint: <span>2.6s</span></div>
                                                <div>DOM content loaded: <span>1.1s</span></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="speed__test-distributions">
                                        <div class="title">Page Load Distributions</div>
                                        <div class="info">
                                            <div class="speed__test-graph">
                                                <div class="graph__block">
                                                    <p class="title">FCP</p>
                                                    <div class="graph">
                                                        <div class="graph__section fast"><span>30%</span><div class="tooltip"></div></div>
                                                        <div class="graph__section average"><span>40%</span><div class="tooltip"></div></div>
                                                        <div class="graph__section slow"><span>30%</span><div class="tooltip"></div></div>
                                                    </div>
                                                </div>
                                                <div class="graph__block">
                                                    <p class="title">DCL</p>
                                                    <div class="graph">
                                                        <div class="graph__labels">
                                                            <span>0%</span>
                                                            <span>25%</span>
                                                            <span>50%</span>
                                                            <span>75%</span>
                                                        </div>
                                                        <div class="graph__section fast"><span>30%</span><div class="tooltip"></div></div>
                                                        <div class="graph__section average"><span>40%</span><div class="tooltip"></div></div>
                                                        <div class="graph__section slow"><span>30%</span><div class="tooltip"></div></div>
                                                    </div>
                                                </div>
                                                <div class="graph__description">
                                                    <div class="graph__description-block">
                                                        <span></span>
                                                        <p>Fast</p>
                                                    </div>
                                                    <div class="graph__description-block">
                                                        <span></span>
                                                        <p>Average</p>
                                                    </div>
                                                    <div class="graph__description-block">
                                                        <span></span>
                                                        <p>Slow</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="speed__test-adv">
                                    <p><?=t($metaPage['speed__test-adv_title']);?></p>
                                    <img src="<?= $template_directory_uri; ?>/out/img_design/speed_test-adv.png" alt="page_speed">
                                    <a href="<?=t($metaPage['speed__test-adv_link']);?>" target="_blank" class="btn"><?=t($metaPage['speed__test-adv_btn']);?></a>
                                </div>
                                <div class="speed__test-fix">
                                    <div class="fix__container">
                                        <p class="title">Optimization Suggestions</p>
                                        <div class="fix__containers"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php } ?>
                <?php if($post->ID === 2833){ ?>
                    <div class="mini__tools-container backlinks">
                        <div class="wrapper">
                            <p class="backlinks__title"><?=t($metaPage['banner_title']);?></p>
                            <p class="backlinks__description"><?=t($metaPage['banner_description']);?></p>
                            <form action="" id="backlinks__form" class="home_first-search">
                                <span class="error">Must be a valid URL with http:// or https://</span>
                                <input type="text" placeholder="Enter URL to test">
                                <select class="backlinks__select">
                                    <option value="29"><?=t($metaPage['banner_select_domain']);?></option>
                                    <option value="30"><?=t($metaPage['banner_select_page']);?></option>
                                </select>
                                <button type="submit"><img src="<?= $template_directory_uri; ?>/out/img_design/arrow.svg" alt="arrow" title="arrow" class="arrow"><img src="<?= $template_directory_uri; ?>/out/img_design/loading.svg" alt="arrow" title="arrow" class="loading"></button>
                            </form>
                        </div>
                    </div>
                    <div class="backlinks__data_container">
                        <div class="backlinks__data">
                            <div class="wrapper">
                                <p class="backlinks__info"><b>NOTE!</b> The results are limited. You are able to see only 20 backlinks. Click here to <a href="https://ahrefs.evyy.net/c/1268936/298495/4653" target="_blank">view all the backlinks</a>
                                </p>
                                <div class="backlinks__data-matrix">
                                    <div>
                                        <p class="title">
                                            <span><?=t($metaPage['total_external_backlinks']);?></span>
                                            <button type="button" class="question">
                                                <svg width="22" height="22" viewBox="0 0 22 22" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                    <path d="M11 17.3379C11.5933 17.3379 12.0742 16.8569 12.0742 16.2637C12.0742 15.6704 11.5933 15.1895 11 15.1895C10.4067 15.1895 9.92578 15.6704 9.92578 16.2637C9.92578 16.8569 10.4067 17.3379 11 17.3379Z" fill="#D8D8D8"/>
                                                    <path d="M11 0C4.92061 0 0 4.91979 0 11C0 17.0794 4.91979 22 11 22C17.0794 22 22 17.0802 22 11C22 4.92061 17.0802 0 11 0ZM11 20.2812C5.87052 20.2812 1.71875 16.1302 1.71875 11C1.71875 5.87052 5.86983 1.71875 11 1.71875C16.1295 1.71875 20.2812 5.86983 20.2812 11C20.2812 16.1295 16.1302 20.2812 11 20.2812Z" fill="#D8D8D8"/>
                                                    <path d="M11 5.52148C9.10456 5.52148 7.5625 7.06355 7.5625 8.95898C7.5625 9.43362 7.94724 9.81836 8.42188 9.81836C8.89651 9.81836 9.28125 9.43362 9.28125 8.95898C9.28125 8.01127 10.0523 7.24023 11 7.24023C11.9477 7.24023 12.7188 8.01127 12.7188 8.95898C12.7188 9.9067 11.9477 10.6777 11 10.6777C10.5254 10.6777 10.1406 11.0625 10.1406 11.5371V13.6855C10.1406 14.1602 10.5254 14.5449 11 14.5449C11.4746 14.5449 11.8594 14.1602 11.8594 13.6855V12.2877C13.3403 11.9052 14.4375 10.5578 14.4375 8.95898C14.4375 7.06355 12.8954 5.52148 11 5.52148Z" fill="#D8D8D8"/>
                                                </svg>
                                                <span class="tooltip"><?=t($metaPage['total_external_backlinks_description']);?></span>
                                            </button>
                                        </p>
                                        <p class="value" id="total__backlinks"></p>
                                    </div>
                                    <div>
                                        <p class="title">
                                            <span><?=t($metaPage['total_referring_domains']);?></span>
                                            <button type="button" class="question">
                                                <svg width="22" height="22" viewBox="0 0 22 22" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                    <path d="M11 17.3379C11.5933 17.3379 12.0742 16.8569 12.0742 16.2637C12.0742 15.6704 11.5933 15.1895 11 15.1895C10.4067 15.1895 9.92578 15.6704 9.92578 16.2637C9.92578 16.8569 10.4067 17.3379 11 17.3379Z" fill="#D8D8D8"/>
                                                    <path d="M11 0C4.92061 0 0 4.91979 0 11C0 17.0794 4.91979 22 11 22C17.0794 22 22 17.0802 22 11C22 4.92061 17.0802 0 11 0ZM11 20.2812C5.87052 20.2812 1.71875 16.1302 1.71875 11C1.71875 5.87052 5.86983 1.71875 11 1.71875C16.1295 1.71875 20.2812 5.86983 20.2812 11C20.2812 16.1295 16.1302 20.2812 11 20.2812Z" fill="#D8D8D8"/>
                                                    <path d="M11 5.52148C9.10456 5.52148 7.5625 7.06355 7.5625 8.95898C7.5625 9.43362 7.94724 9.81836 8.42188 9.81836C8.89651 9.81836 9.28125 9.43362 9.28125 8.95898C9.28125 8.01127 10.0523 7.24023 11 7.24023C11.9477 7.24023 12.7188 8.01127 12.7188 8.95898C12.7188 9.9067 11.9477 10.6777 11 10.6777C10.5254 10.6777 10.1406 11.0625 10.1406 11.5371V13.6855C10.1406 14.1602 10.5254 14.5449 11 14.5449C11.4746 14.5449 11.8594 14.1602 11.8594 13.6855V12.2877C13.3403 11.9052 14.4375 10.5578 14.4375 8.95898C14.4375 7.06355 12.8954 5.52148 11 5.52148Z" fill="#D8D8D8"/>
                                                </svg>
                                                <span class="tooltip"><?=t($metaPage['total_referring_domains_description']);?></span>
                                            </button>
                                        </p>
                                        <p class="value" id="refdomains"></p>
                                    </div>
                                    <div>
                                        <p class="title">
                                            <span><?=t($metaPage['dofollow_backlinks']);?></span>
                                            <button type="button" class="question">
                                                <svg width="22" height="22" viewBox="0 0 22 22" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                    <path d="M11 17.3379C11.5933 17.3379 12.0742 16.8569 12.0742 16.2637C12.0742 15.6704 11.5933 15.1895 11 15.1895C10.4067 15.1895 9.92578 15.6704 9.92578 16.2637C9.92578 16.8569 10.4067 17.3379 11 17.3379Z" fill="#D8D8D8"/>
                                                    <path d="M11 0C4.92061 0 0 4.91979 0 11C0 17.0794 4.91979 22 11 22C17.0794 22 22 17.0802 22 11C22 4.92061 17.0802 0 11 0ZM11 20.2812C5.87052 20.2812 1.71875 16.1302 1.71875 11C1.71875 5.87052 5.86983 1.71875 11 1.71875C16.1295 1.71875 20.2812 5.86983 20.2812 11C20.2812 16.1295 16.1302 20.2812 11 20.2812Z" fill="#D8D8D8"/>
                                                    <path d="M11 5.52148C9.10456 5.52148 7.5625 7.06355 7.5625 8.95898C7.5625 9.43362 7.94724 9.81836 8.42188 9.81836C8.89651 9.81836 9.28125 9.43362 9.28125 8.95898C9.28125 8.01127 10.0523 7.24023 11 7.24023C11.9477 7.24023 12.7188 8.01127 12.7188 8.95898C12.7188 9.9067 11.9477 10.6777 11 10.6777C10.5254 10.6777 10.1406 11.0625 10.1406 11.5371V13.6855C10.1406 14.1602 10.5254 14.5449 11 14.5449C11.4746 14.5449 11.8594 14.1602 11.8594 13.6855V12.2877C13.3403 11.9052 14.4375 10.5578 14.4375 8.95898C14.4375 7.06355 12.8954 5.52148 11 5.52148Z" fill="#D8D8D8"/>
                                                </svg>
                                                <span class="tooltip"><?=t($metaPage['dofollow_backlinks_description']);?></span>
                                            </button>
                                        </p>
                                        <p class="value" id="percent"></p>
                                    </div>
                                    <div>
                                        <p class="title">
                                            <span><?=t($metaPage['referring_ips']);?></span>
                                            <button type="button" class="question">
                                                <svg width="22" height="22" viewBox="0 0 22 22" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                    <path d="M11 17.3379C11.5933 17.3379 12.0742 16.8569 12.0742 16.2637C12.0742 15.6704 11.5933 15.1895 11 15.1895C10.4067 15.1895 9.92578 15.6704 9.92578 16.2637C9.92578 16.8569 10.4067 17.3379 11 17.3379Z" fill="#D8D8D8"/>
                                                    <path d="M11 0C4.92061 0 0 4.91979 0 11C0 17.0794 4.91979 22 11 22C17.0794 22 22 17.0802 22 11C22 4.92061 17.0802 0 11 0ZM11 20.2812C5.87052 20.2812 1.71875 16.1302 1.71875 11C1.71875 5.87052 5.86983 1.71875 11 1.71875C16.1295 1.71875 20.2812 5.86983 20.2812 11C20.2812 16.1295 16.1302 20.2812 11 20.2812Z" fill="#D8D8D8"/>
                                                    <path d="M11 5.52148C9.10456 5.52148 7.5625 7.06355 7.5625 8.95898C7.5625 9.43362 7.94724 9.81836 8.42188 9.81836C8.89651 9.81836 9.28125 9.43362 9.28125 8.95898C9.28125 8.01127 10.0523 7.24023 11 7.24023C11.9477 7.24023 12.7188 8.01127 12.7188 8.95898C12.7188 9.9067 11.9477 10.6777 11 10.6777C10.5254 10.6777 10.1406 11.0625 10.1406 11.5371V13.6855C10.1406 14.1602 10.5254 14.5449 11 14.5449C11.4746 14.5449 11.8594 14.1602 11.8594 13.6855V12.2877C13.3403 11.9052 14.4375 10.5578 14.4375 8.95898C14.4375 7.06355 12.8954 5.52148 11 5.52148Z" fill="#D8D8D8"/>
                                                </svg>
                                                <span class="tooltip"><?=t($metaPage['referring_ips_description']);?></span>
                                            </button>
                                        </p>
                                        <p class="value" id="refips"></p>
                                    </div>
                                </div>
                                <div class="backlinks__data-graph">
                                    <div>
                                        <p class="title">
                                            <span><?=t($metaPage['domain_rating_distribution']);?></span>
                                            <button type="button" class="question">
                                                <svg width="22" height="22" viewBox="0 0 22 22" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                    <path d="M11 17.3379C11.5933 17.3379 12.0742 16.8569 12.0742 16.2637C12.0742 15.6704 11.5933 15.1895 11 15.1895C10.4067 15.1895 9.92578 15.6704 9.92578 16.2637C9.92578 16.8569 10.4067 17.3379 11 17.3379Z" fill="#D8D8D8"/>
                                                    <path d="M11 0C4.92061 0 0 4.91979 0 11C0 17.0794 4.91979 22 11 22C17.0794 22 22 17.0802 22 11C22 4.92061 17.0802 0 11 0ZM11 20.2812C5.87052 20.2812 1.71875 16.1302 1.71875 11C1.71875 5.87052 5.86983 1.71875 11 1.71875C16.1295 1.71875 20.2812 5.86983 20.2812 11C20.2812 16.1295 16.1302 20.2812 11 20.2812Z" fill="#D8D8D8"/>
                                                    <path d="M11 5.52148C9.10456 5.52148 7.5625 7.06355 7.5625 8.95898C7.5625 9.43362 7.94724 9.81836 8.42188 9.81836C8.89651 9.81836 9.28125 9.43362 9.28125 8.95898C9.28125 8.01127 10.0523 7.24023 11 7.24023C11.9477 7.24023 12.7188 8.01127 12.7188 8.95898C12.7188 9.9067 11.9477 10.6777 11 10.6777C10.5254 10.6777 10.1406 11.0625 10.1406 11.5371V13.6855C10.1406 14.1602 10.5254 14.5449 11 14.5449C11.4746 14.5449 11.8594 14.1602 11.8594 13.6855V12.2877C13.3403 11.9052 14.4375 10.5578 14.4375 8.95898C14.4375 7.06355 12.8954 5.52148 11 5.52148Z" fill="#D8D8D8"/>
                                                </svg>
                                                <span class="tooltip"><?=t($metaPage['domain_rating_distribution_description']);?></span>
                                            </button>
                                        </p>
                                        <div class="graph" id="domain__graph"></div>
                                    </div>
                                    <div>
                                        <p class="title">
                                            <span><?=t($metaPage['url_rating_distribution']);?></span>
                                            <button type="button" class="question">
                                                <svg width="22" height="22" viewBox="0 0 22 22" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                    <path d="M11 17.3379C11.5933 17.3379 12.0742 16.8569 12.0742 16.2637C12.0742 15.6704 11.5933 15.1895 11 15.1895C10.4067 15.1895 9.92578 15.6704 9.92578 16.2637C9.92578 16.8569 10.4067 17.3379 11 17.3379Z" fill="#D8D8D8"/>
                                                    <path d="M11 0C4.92061 0 0 4.91979 0 11C0 17.0794 4.91979 22 11 22C17.0794 22 22 17.0802 22 11C22 4.92061 17.0802 0 11 0ZM11 20.2812C5.87052 20.2812 1.71875 16.1302 1.71875 11C1.71875 5.87052 5.86983 1.71875 11 1.71875C16.1295 1.71875 20.2812 5.86983 20.2812 11C20.2812 16.1295 16.1302 20.2812 11 20.2812Z" fill="#D8D8D8"/>
                                                    <path d="M11 5.52148C9.10456 5.52148 7.5625 7.06355 7.5625 8.95898C7.5625 9.43362 7.94724 9.81836 8.42188 9.81836C8.89651 9.81836 9.28125 9.43362 9.28125 8.95898C9.28125 8.01127 10.0523 7.24023 11 7.24023C11.9477 7.24023 12.7188 8.01127 12.7188 8.95898C12.7188 9.9067 11.9477 10.6777 11 10.6777C10.5254 10.6777 10.1406 11.0625 10.1406 11.5371V13.6855C10.1406 14.1602 10.5254 14.5449 11 14.5449C11.4746 14.5449 11.8594 14.1602 11.8594 13.6855V12.2877C13.3403 11.9052 14.4375 10.5578 14.4375 8.95898C14.4375 7.06355 12.8954 5.52148 11 5.52148Z" fill="#D8D8D8"/>
                                                </svg>
                                                <span class="tooltip"><?=t($metaPage['url_rating_distribution_description']);?></span>
                                            </button>
                                        </p>
                                        <div class="graph" id="url__graph"></div>
                                    </div>
                                    <div>
                                        <p class="title">
                                            <span><?=t($metaPage['nofollow_dofollow_links']);?></span>
                                            <button type="button" class="question">
                                                <svg width="22" height="22" viewBox="0 0 22 22" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                    <path d="M11 17.3379C11.5933 17.3379 12.0742 16.8569 12.0742 16.2637C12.0742 15.6704 11.5933 15.1895 11 15.1895C10.4067 15.1895 9.92578 15.6704 9.92578 16.2637C9.92578 16.8569 10.4067 17.3379 11 17.3379Z" fill="#D8D8D8"/>
                                                    <path d="M11 0C4.92061 0 0 4.91979 0 11C0 17.0794 4.91979 22 11 22C17.0794 22 22 17.0802 22 11C22 4.92061 17.0802 0 11 0ZM11 20.2812C5.87052 20.2812 1.71875 16.1302 1.71875 11C1.71875 5.87052 5.86983 1.71875 11 1.71875C16.1295 1.71875 20.2812 5.86983 20.2812 11C20.2812 16.1295 16.1302 20.2812 11 20.2812Z" fill="#D8D8D8"/>
                                                    <path d="M11 5.52148C9.10456 5.52148 7.5625 7.06355 7.5625 8.95898C7.5625 9.43362 7.94724 9.81836 8.42188 9.81836C8.89651 9.81836 9.28125 9.43362 9.28125 8.95898C9.28125 8.01127 10.0523 7.24023 11 7.24023C11.9477 7.24023 12.7188 8.01127 12.7188 8.95898C12.7188 9.9067 11.9477 10.6777 11 10.6777C10.5254 10.6777 10.1406 11.0625 10.1406 11.5371V13.6855C10.1406 14.1602 10.5254 14.5449 11 14.5449C11.4746 14.5449 11.8594 14.1602 11.8594 13.6855V12.2877C13.3403 11.9052 14.4375 10.5578 14.4375 8.95898C14.4375 7.06355 12.8954 5.52148 11 5.52148Z" fill="#D8D8D8"/>
                                                </svg>
                                                <span class="tooltip"><?=t($metaPage['nofollow_dofollow_links_description']);?></span>
                                            </button>
                                        </p>
                                        <div class="graph" id="follow__graph"></div>
                                    </div>
                                </div>
                                <div class="backlinks__data-results">
                                    <table class="backlinks__data-table">
                                        <thead>
                                        <th>
                                            <p>
                                                <span><?=t($metaPage['url']);?></span>
                                                <button type="button" class="question">
                                                    <svg width="22" height="22" viewBox="0 0 22 22" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                        <path d="M11 17.3379C11.5933 17.3379 12.0742 16.8569 12.0742 16.2637C12.0742 15.6704 11.5933 15.1895 11 15.1895C10.4067 15.1895 9.92578 15.6704 9.92578 16.2637C9.92578 16.8569 10.4067 17.3379 11 17.3379Z" fill="#D8D8D8"/>
                                                        <path d="M11 0C4.92061 0 0 4.91979 0 11C0 17.0794 4.91979 22 11 22C17.0794 22 22 17.0802 22 11C22 4.92061 17.0802 0 11 0ZM11 20.2812C5.87052 20.2812 1.71875 16.1302 1.71875 11C1.71875 5.87052 5.86983 1.71875 11 1.71875C16.1295 1.71875 20.2812 5.86983 20.2812 11C20.2812 16.1295 16.1302 20.2812 11 20.2812Z" fill="#D8D8D8"/>
                                                        <path d="M11 5.52148C9.10456 5.52148 7.5625 7.06355 7.5625 8.95898C7.5625 9.43362 7.94724 9.81836 8.42188 9.81836C8.89651 9.81836 9.28125 9.43362 9.28125 8.95898C9.28125 8.01127 10.0523 7.24023 11 7.24023C11.9477 7.24023 12.7188 8.01127 12.7188 8.95898C12.7188 9.9067 11.9477 10.6777 11 10.6777C10.5254 10.6777 10.1406 11.0625 10.1406 11.5371V13.6855C10.1406 14.1602 10.5254 14.5449 11 14.5449C11.4746 14.5449 11.8594 14.1602 11.8594 13.6855V12.2877C13.3403 11.9052 14.4375 10.5578 14.4375 8.95898C14.4375 7.06355 12.8954 5.52148 11 5.52148Z" fill="#D8D8D8"/>
                                                    </svg>
                                                    <span class="tooltip"><?=t($metaPage['url_description']);?></span>
                                                </button>
                                            </p>
                                        </th>
                                        <th>
                                            <p>
                                                <span><?=t($metaPage['anchor']);?></span>
                                                <button type="button" class="question">
                                                    <svg width="22" height="22" viewBox="0 0 22 22" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                        <path d="M11 17.3379C11.5933 17.3379 12.0742 16.8569 12.0742 16.2637C12.0742 15.6704 11.5933 15.1895 11 15.1895C10.4067 15.1895 9.92578 15.6704 9.92578 16.2637C9.92578 16.8569 10.4067 17.3379 11 17.3379Z" fill="#D8D8D8"/>
                                                        <path d="M11 0C4.92061 0 0 4.91979 0 11C0 17.0794 4.91979 22 11 22C17.0794 22 22 17.0802 22 11C22 4.92061 17.0802 0 11 0ZM11 20.2812C5.87052 20.2812 1.71875 16.1302 1.71875 11C1.71875 5.87052 5.86983 1.71875 11 1.71875C16.1295 1.71875 20.2812 5.86983 20.2812 11C20.2812 16.1295 16.1302 20.2812 11 20.2812Z" fill="#D8D8D8"/>
                                                        <path d="M11 5.52148C9.10456 5.52148 7.5625 7.06355 7.5625 8.95898C7.5625 9.43362 7.94724 9.81836 8.42188 9.81836C8.89651 9.81836 9.28125 9.43362 9.28125 8.95898C9.28125 8.01127 10.0523 7.24023 11 7.24023C11.9477 7.24023 12.7188 8.01127 12.7188 8.95898C12.7188 9.9067 11.9477 10.6777 11 10.6777C10.5254 10.6777 10.1406 11.0625 10.1406 11.5371V13.6855C10.1406 14.1602 10.5254 14.5449 11 14.5449C11.4746 14.5449 11.8594 14.1602 11.8594 13.6855V12.2877C13.3403 11.9052 14.4375 10.5578 14.4375 8.95898C14.4375 7.06355 12.8954 5.52148 11 5.52148Z" fill="#D8D8D8"/>
                                                    </svg>
                                                    <span class="tooltip"><?=t($metaPage['anchor_description']);?></span>
                                                </button>
                                            </p>
                                        </th>
                                        <th>
                                            <p>
                                                <span><?=t($metaPage['link_type']);?></span>
                                                <button type="button" class="question">
                                                    <svg width="22" height="22" viewBox="0 0 22 22" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                        <path d="M11 17.3379C11.5933 17.3379 12.0742 16.8569 12.0742 16.2637C12.0742 15.6704 11.5933 15.1895 11 15.1895C10.4067 15.1895 9.92578 15.6704 9.92578 16.2637C9.92578 16.8569 10.4067 17.3379 11 17.3379Z" fill="#D8D8D8"/>
                                                        <path d="M11 0C4.92061 0 0 4.91979 0 11C0 17.0794 4.91979 22 11 22C17.0794 22 22 17.0802 22 11C22 4.92061 17.0802 0 11 0ZM11 20.2812C5.87052 20.2812 1.71875 16.1302 1.71875 11C1.71875 5.87052 5.86983 1.71875 11 1.71875C16.1295 1.71875 20.2812 5.86983 20.2812 11C20.2812 16.1295 16.1302 20.2812 11 20.2812Z" fill="#D8D8D8"/>
                                                        <path d="M11 5.52148C9.10456 5.52148 7.5625 7.06355 7.5625 8.95898C7.5625 9.43362 7.94724 9.81836 8.42188 9.81836C8.89651 9.81836 9.28125 9.43362 9.28125 8.95898C9.28125 8.01127 10.0523 7.24023 11 7.24023C11.9477 7.24023 12.7188 8.01127 12.7188 8.95898C12.7188 9.9067 11.9477 10.6777 11 10.6777C10.5254 10.6777 10.1406 11.0625 10.1406 11.5371V13.6855C10.1406 14.1602 10.5254 14.5449 11 14.5449C11.4746 14.5449 11.8594 14.1602 11.8594 13.6855V12.2877C13.3403 11.9052 14.4375 10.5578 14.4375 8.95898C14.4375 7.06355 12.8954 5.52148 11 5.52148Z" fill="#D8D8D8"/>
                                                    </svg>
                                                    <span class="tooltip"><?=t($metaPage['link_type_description']);?></span>
                                                </button>
                                            </p>
                                        </th>
                                        <th>
                                            <p>
                                                <span><?=t($metaPage['dr']);?></span>
                                                <button type="button" class="question">
                                                    <svg width="22" height="22" viewBox="0 0 22 22" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                        <path d="M11 17.3379C11.5933 17.3379 12.0742 16.8569 12.0742 16.2637C12.0742 15.6704 11.5933 15.1895 11 15.1895C10.4067 15.1895 9.92578 15.6704 9.92578 16.2637C9.92578 16.8569 10.4067 17.3379 11 17.3379Z" fill="#D8D8D8"/>
                                                        <path d="M11 0C4.92061 0 0 4.91979 0 11C0 17.0794 4.91979 22 11 22C17.0794 22 22 17.0802 22 11C22 4.92061 17.0802 0 11 0ZM11 20.2812C5.87052 20.2812 1.71875 16.1302 1.71875 11C1.71875 5.87052 5.86983 1.71875 11 1.71875C16.1295 1.71875 20.2812 5.86983 20.2812 11C20.2812 16.1295 16.1302 20.2812 11 20.2812Z" fill="#D8D8D8"/>
                                                        <path d="M11 5.52148C9.10456 5.52148 7.5625 7.06355 7.5625 8.95898C7.5625 9.43362 7.94724 9.81836 8.42188 9.81836C8.89651 9.81836 9.28125 9.43362 9.28125 8.95898C9.28125 8.01127 10.0523 7.24023 11 7.24023C11.9477 7.24023 12.7188 8.01127 12.7188 8.95898C12.7188 9.9067 11.9477 10.6777 11 10.6777C10.5254 10.6777 10.1406 11.0625 10.1406 11.5371V13.6855C10.1406 14.1602 10.5254 14.5449 11 14.5449C11.4746 14.5449 11.8594 14.1602 11.8594 13.6855V12.2877C13.3403 11.9052 14.4375 10.5578 14.4375 8.95898C14.4375 7.06355 12.8954 5.52148 11 5.52148Z" fill="#D8D8D8"/>
                                                    </svg>
                                                    <span class="tooltip"><?=t($metaPage['dr_description']);?></span>
                                                </button>
                                            </p>
                                        </th>
                                        <th>
                                            <p>
                                                <span><?=t($metaPage['ur']);?></span>
                                                <button type="button" class="question">
                                                    <svg width="22" height="22" viewBox="0 0 22 22" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                        <path d="M11 17.3379C11.5933 17.3379 12.0742 16.8569 12.0742 16.2637C12.0742 15.6704 11.5933 15.1895 11 15.1895C10.4067 15.1895 9.92578 15.6704 9.92578 16.2637C9.92578 16.8569 10.4067 17.3379 11 17.3379Z" fill="#D8D8D8"/>
                                                        <path d="M11 0C4.92061 0 0 4.91979 0 11C0 17.0794 4.91979 22 11 22C17.0794 22 22 17.0802 22 11C22 4.92061 17.0802 0 11 0ZM11 20.2812C5.87052 20.2812 1.71875 16.1302 1.71875 11C1.71875 5.87052 5.86983 1.71875 11 1.71875C16.1295 1.71875 20.2812 5.86983 20.2812 11C20.2812 16.1295 16.1302 20.2812 11 20.2812Z" fill="#D8D8D8"/>
                                                        <path d="M11 5.52148C9.10456 5.52148 7.5625 7.06355 7.5625 8.95898C7.5625 9.43362 7.94724 9.81836 8.42188 9.81836C8.89651 9.81836 9.28125 9.43362 9.28125 8.95898C9.28125 8.01127 10.0523 7.24023 11 7.24023C11.9477 7.24023 12.7188 8.01127 12.7188 8.95898C12.7188 9.9067 11.9477 10.6777 11 10.6777C10.5254 10.6777 10.1406 11.0625 10.1406 11.5371V13.6855C10.1406 14.1602 10.5254 14.5449 11 14.5449C11.4746 14.5449 11.8594 14.1602 11.8594 13.6855V12.2877C13.3403 11.9052 14.4375 10.5578 14.4375 8.95898C14.4375 7.06355 12.8954 5.52148 11 5.52148Z" fill="#D8D8D8"/>
                                                    </svg>
                                                    <span class="tooltip"><?=t($metaPage['ur_description']);?></span>
                                                </button>
                                            </p>
                                        </th>
                                        <th>
                                            <p>
                                                <span><?=t($metaPage['total_links']);?></span>
                                                <button type="button" class="question">
                                                    <svg width="22" height="22" viewBox="0 0 22 22" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                        <path d="M11 17.3379C11.5933 17.3379 12.0742 16.8569 12.0742 16.2637C12.0742 15.6704 11.5933 15.1895 11 15.1895C10.4067 15.1895 9.92578 15.6704 9.92578 16.2637C9.92578 16.8569 10.4067 17.3379 11 17.3379Z" fill="#D8D8D8"/>
                                                        <path d="M11 0C4.92061 0 0 4.91979 0 11C0 17.0794 4.91979 22 11 22C17.0794 22 22 17.0802 22 11C22 4.92061 17.0802 0 11 0ZM11 20.2812C5.87052 20.2812 1.71875 16.1302 1.71875 11C1.71875 5.87052 5.86983 1.71875 11 1.71875C16.1295 1.71875 20.2812 5.86983 20.2812 11C20.2812 16.1295 16.1302 20.2812 11 20.2812Z" fill="#D8D8D8"/>
                                                        <path d="M11 5.52148C9.10456 5.52148 7.5625 7.06355 7.5625 8.95898C7.5625 9.43362 7.94724 9.81836 8.42188 9.81836C8.89651 9.81836 9.28125 9.43362 9.28125 8.95898C9.28125 8.01127 10.0523 7.24023 11 7.24023C11.9477 7.24023 12.7188 8.01127 12.7188 8.95898C12.7188 9.9067 11.9477 10.6777 11 10.6777C10.5254 10.6777 10.1406 11.0625 10.1406 11.5371V13.6855C10.1406 14.1602 10.5254 14.5449 11 14.5449C11.4746 14.5449 11.8594 14.1602 11.8594 13.6855V12.2877C13.3403 11.9052 14.4375 10.5578 14.4375 8.95898C14.4375 7.06355 12.8954 5.52148 11 5.52148Z" fill="#D8D8D8"/>
                                                    </svg>
                                                    <span class="tooltip"><?=t($metaPage['total_links_description']);?></span>
                                                </button>
                                            </p>
                                        </th>
                                        <th>
                                            <p>
                                                <span><?=t($metaPage['first_detected']);?></span>
                                                <button type="button" class="question">
                                                    <svg width="22" height="22" viewBox="0 0 22 22" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                        <path d="M11 17.3379C11.5933 17.3379 12.0742 16.8569 12.0742 16.2637C12.0742 15.6704 11.5933 15.1895 11 15.1895C10.4067 15.1895 9.92578 15.6704 9.92578 16.2637C9.92578 16.8569 10.4067 17.3379 11 17.3379Z" fill="#D8D8D8"/>
                                                        <path d="M11 0C4.92061 0 0 4.91979 0 11C0 17.0794 4.91979 22 11 22C17.0794 22 22 17.0802 22 11C22 4.92061 17.0802 0 11 0ZM11 20.2812C5.87052 20.2812 1.71875 16.1302 1.71875 11C1.71875 5.87052 5.86983 1.71875 11 1.71875C16.1295 1.71875 20.2812 5.86983 20.2812 11C20.2812 16.1295 16.1302 20.2812 11 20.2812Z" fill="#D8D8D8"/>
                                                        <path d="M11 5.52148C9.10456 5.52148 7.5625 7.06355 7.5625 8.95898C7.5625 9.43362 7.94724 9.81836 8.42188 9.81836C8.89651 9.81836 9.28125 9.43362 9.28125 8.95898C9.28125 8.01127 10.0523 7.24023 11 7.24023C11.9477 7.24023 12.7188 8.01127 12.7188 8.95898C12.7188 9.9067 11.9477 10.6777 11 10.6777C10.5254 10.6777 10.1406 11.0625 10.1406 11.5371V13.6855C10.1406 14.1602 10.5254 14.5449 11 14.5449C11.4746 14.5449 11.8594 14.1602 11.8594 13.6855V12.2877C13.3403 11.9052 14.4375 10.5578 14.4375 8.95898C14.4375 7.06355 12.8954 5.52148 11 5.52148Z" fill="#D8D8D8"/>
                                                    </svg>
                                                    <span class="tooltip"><?=t($metaPage['first_detected_description']);?></span>
                                                </button>
                                            </p>
                                        </th>
                                        </thead>
                                        <tbody></tbody>
                                    </table>
                                </div>
                                <div class="backlinks__banner">
                                    <img src="<?= $template_directory_uri; ?>/out/img_design/ahrefs.png">
                                    <div>
                                        <p><?=t($metaPage['ahrefs_banner_title']);?></p>
                                        <p><?=t($metaPage['ahrefs_banner_description']);?></p>
                                        <a href="https://ahrefs.evyy.net/c/1268936/298495/4653" target="_blank"><?=t($metaPage['ahrefs_banner_button']);?></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php } ?>
            <div class="wrapper">
                <h1 class="article__title"><?php echo get_post_meta($post->ID, 'page.h1', true) ?></h1>
                <div class="article__info">
                    <div class="date">
                        <img src="<?= $template_directory_uri; ?>/out/img_design/article__clock.svg" alt="time"
                             title="time">
                        <span><?php echo get_the_date('D, M j, Y'); ?></span>
                    </div>
                    <div class="watches">
                        <img src="<?= $template_directory_uri; ?>/out/img_design/article__watch.svg" alt="watches"
                             title="watches">
                        <span><?php setPostViews(get_the_ID());
                            echo getPostViews(get_the_ID()); ?></span>
                    </div>
                    <div class="stars">
                        <img src="<?= $template_directory_uri; ?>/out/img_design/article__star.svg" alt="stars"
                             title="stars">
                        <span>1</span>
                    </div>
                </div>
                <img src="<?php the_post_thumbnail_url(); ?>" alt="<?php echo get_post_meta($post->ID, 'page.h1', true) ?>" title="<?php echo get_post_meta($post->ID, 'page.h1', true) ?>" class="article__img">
                <div class="stars__block">
                    <?php if (function_exists('the_ratings')) {
                        the_ratings();
                    } ?>
                </div>
                <?php
                if(t($metaPage['table_of_contents']) !== ''){
                    ?>
                    <div class="article__content">
                        <div class="article__content-title">
                            <img src="<?= $template_directory_uri; ?>/out/img_design/article__table.svg"
                                 alt="Table of Contents"
                                 title="Table of Contents">
                            <?=t($metaDesign['table_of_contents']);?>
                        </div>
                        <div class="article__content-body"><?=t($metaPage['table_of_contents']);?></div>
                    </div>
                    <?php
                        if($post->ID !== 2833 && $post->ID !== 1063){ ?>
                            <div class="article__menu-small">
                                <button class="article__menu-img">
                                    <svg width="24" height="20" viewBox="0 0 24 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path d="M3 0H1C0.4 0 0 0.4 0 1V3C0 3.6 0.4 4 1 4H3C3.6 4 4 3.6 4 3V1C4 0.4 3.6 0 3 0Z"
                                              fill="#0066FF"/>
                                        <path d="M3 0H1C0.4 0 0 0.4 0 1V3C0 3.6 0.4 4 1 4H3C3.6 4 4 3.6 4 3V1C4 0.4 3.6 0 3 0Z"
                                              transform="translate(0 8)" fill="#0066FF"/>
                                        <path d="M3 0H1C0.4 0 0 0.4 0 1V3C0 3.6 0.4 4 1 4H3C3.6 4 4 3.6 4 3V1C4 0.4 3.6 0 3 0Z"
                                              transform="translate(0 16)" fill="#0066FF"/>
                                        <path d="M15 0H1C0.4 0 0 0.4 0 1V3C0 3.6 0.4 4 1 4H15C15.6 4 16 3.6 16 3V1C16 0.4 15.6 0 15 0Z"
                                              transform="translate(8)" fill="#0066FF"/>
                                        <path d="M15 0H1C0.4 0 0 0.4 0 1V3C0 3.6 0.4 4 1 4H15C15.6 4 16 3.6 16 3V1C16 0.4 15.6 0 15 0Z"
                                              transform="translate(8 8)" fill="#0066FF"/>
                                        <path d="M15 0H1C0.4 0 0 0.4 0 1V3C0 3.6 0.4 4 1 4H15C15.6 4 16 3.6 16 3V1C16 0.4 15.6 0 15 0Z"
                                              transform="translate(8 16)" fill="#0066FF"/>
                                    </svg>
                                </button>
                                <div class="article__menu-block">
                                    <?php echo get_post_meta($post->ID, 'table_of_contents', true) ?>
                                </div>
                            </div>
                        <?php } ?>
                <?php } ?>

                <div class="article__content-block">
                    <?php the_content(); ?>
                </div>
            </div>
        </div>
        <?php if($post->ID === 4179){ ?>
            <span id="5b694ac4-f934-3d32-36b9-c7a328427932"></span>
            <script type="application/javascript">
                var d=document;var s=d.createElement('script');
                s.src='https://t.sitechecker.pro/ZGqxPj?se_referrer=' + encodeURIComponent(document.referrer) + '&default_keyword=' + encodeURIComponent(document.title) + '&'+window.location.search.replace('?', '&')+'&frm=script&_cid=5b694ac4-f934-3d32-36b9-c7a328427932';
                d.getElementById("5b694ac4-f934-3d32-36b9-c7a328427932").appendChild(s);
            </script>
        <?php } ?>
    </main>
    <?php if($post->ID !== 4179){ ?>
        <div class="subscribe__banner">
            <img src="<?= $template_directory_uri; ?>/out/img_design/email__icon.svg" alt="email__icon"
                 title="email__icon">
            <p><?=t($metaDesign['subscribe_title']);?></p>
            <!-- Begin Mailchimp Signup Form --><!-- Begin Mailchimp Signup Form -->
            <div id="mc_embed_signup">
                <form action="https://sitechecker.us13.list-manage.com/subscribe/post?u=b33f62afc5e797a4e431a7266&amp;id=c6ca5d5cd2"
                      method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form"
                      class="validate article__seo-search" target="_blank" novalidate>
                    <div id="mc_embed_signup_scroll"><input type="email" value="" name="EMAIL" class="email" id="mce-EMAIL" placeholder="<?=t($metaDesign['subscribe_placeholder']);?>" required>
                        <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
                        <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_b33f62afc5e797a4e431a7266_c6ca5d5cd2" tabindex="-1" value=""></div>
                        <div class="clear"><input type="submit" value="<?=t($metaDesign['subscribe_button']);?>" name="subscribe" id="mc-embedded-subscribe" class="button btn"></div>
                    </div>
                </form>
            </div>
            <!--End mc_embed_signup-->
        </div>
    <?php } ?>
    <div class="articles__footer">
        <div class="wrapper">
            <div class="article__more">
                <p class="article__more-title"><?=t($metaDesign['you_may_also_like']);?></p>
                <hr>
            </div>
            <div class="article__more-container">
                <?php
                $paged = get_query_var('paged') ? absint(get_query_var('paged')) : 1;
                $categories = get_the_category();
                $category_name = $categories[0]->cat_name;
                $args = array(
                    'post__not_in' => array($post->ID),
                    'posts_per_page' => 6,
                    'cat' => $categories[0]->cat_ID,
                    'post_type' => array('page', 'post'),
                    'paged' => $paged,
                    'orderby' => 'rand',
                    'order'    => 'ASC'
                );
                $query = new WP_Query($args);
                ?>
                <?php if ($query->have_posts()) : ?>
                    <?php while ($query->have_posts()) : $query->the_post(); ?>
                        <div class="article__more-block">
                            <div class="img__block"
                                 style="background-image: url(<?php the_post_thumbnail_url(); ?>)"></div>
                            <a href="<?php the_permalink(); ?>"
                               class="title"><?php echo get_post_meta(get_the_ID(), 'page.h1', true); ?></a>
                            <span class="info__block">
                                <div class="watches">
                                    <img src="<?= $template_directory_uri; ?>/out/img_design/article__watch.svg"
                                         alt="watches"
                                         title="watches">
                                    <span><?php setPostViews(get_the_ID());
                                        echo getPostViews(get_the_ID()); ?></span>
                                </div>
                                <a href="<?php echo get_category_link($categories[0]->cat_ID); ?>"
                                   class="tag"><?php echo $category_name; ?></a>
                            </span>
                        </div>
                    <?php endwhile; ?>
                <?php else : ?>
                    <p><?php _e('Sorry, no posts matched your criteria.'); ?></p>
                <?php endif;
                wp_reset_query(); ?>
            </div>
            <a href="<?php echo get_category_link($categories[0]->cat_ID); ?>" class="btn more__articles"><?=t($metaDesign['show_more_articles']);?></a>
        </div>
    </div>
<?php endwhile; ?>
<?php else: ?>
<?php endif; ?>