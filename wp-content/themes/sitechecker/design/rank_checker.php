<?php
if(is_user_logged_in()){
    $userDomains = getDomainsByUserId(get_current_user_id());
    $availableLimit = getAvailableLimitByUser(get_current_user_id());
    $pagesLimits = $availableLimit->getAvailablePages();
    if($pagesLimits <= 0){
        $crawlButton = 'limits__btn';
        $monitoringButton = 'limits__btn';
    }else{
        $crawlButton = 'sitechecker_crawl';
        $monitoringButton = 'sitechecker_monitoring';
    }
} else{
    $crawlButton = 'sitechecker_reg';
    $monitoringButton = 'sitechecker_reg-monitoring';
}

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $_REQUEST['level'] = $_POST['level_id'];
    require(ABSPATH . "/wp-content/plugins/paid-memberships-pro/preheaders/checkout.php");
}

$pmpro_levels = pmpro_getAllLevels();

$payProUrls = [];
foreach ($pmpro_levels as $level){
    $payProUrls[$level->id] = generateUrl(get_current_user_id(), $level->id);
}

$template_directory_uri = get_template_directory_uri();
$meta = get_post_meta_all(get_option('page_on_front'));
$metaPage = get_post_meta_all($post->ID);
$metaDesign = get_post_meta_all(url_to_postid( '/pricing-design/' ));
$pricing = get_post_meta_all(url_to_postid( '/pricing-content/' ));

global $gateway, $pmpro_review, $skip_account_fields, $pmpro_paypal_token, $wpdb, $current_user, $pmpro_msg, $pmpro_msgt, $pmpro_requirebilling, $pmpro_level, $pmpro_levels, $tospage, $pmpro_currency_symbol, $pmpro_show_discount_code;
global $discount_code, $username, $password, $password2, $bfirstname, $blastname, $baddress1, $baddress2, $bcity, $bstate, $bzipcode, $bcountry, $bphone, $bemail, $bconfirmemail, $CardType, $AccountNumber, $ExpirationMonth,$ExpirationYear;

$pmpro_levels = pmpro_getAllLevels();

$customerMembershipLevel = $current_user->membership_level->id;
?>
<main class="audit__landing">
    <div class="audit__first">
        <div class="wrapper audit__first-container">
            <h1><?=t($metaPage['title']);?></h1>
            <h2><?=t($metaPage['description']);?></h2>
            <?php
            if ( is_user_logged_in() ) { ?>
                <div class="semscale_btns">
                    <a href="/tools/seo-platform/" class="btn seo_platform-btn"><?=t($metaPage['button']);?></a>
                    <a href="https://surveys.hotjar.com/s?siteId=623968&surveyId=130484" class="btn seo_platform-btn semscale_request">Request for a trial</a>
                </div>
            <?php } else {?>
                <div class="semscale_btns">
                    <a class="btn seo_platform-btn semscale_logedin"><?=t($metaPage['button']);?></a>
                    <a class="btn seo_platform-btn semscale_logedin semscale_request">Request for a trial</a>
                </div>
            <?php }?>
        </div>
    </div>
    <div class="audit__second">
        <div class="wrapper">
            <p class="audit__title"><?=t($metaPage['must_header']);?></p>
            <div class="audit__second-content">
                <div class="audit__second-left">
                    <div class="audit__matter">
                        <img src="<?= $template_directory_uri; ?>/out/img_design/audit__matter-first.svg" alt="audit" title="audit">
                        <div>
                            <p class="audit__matter-title"><?=t($metaPage['must_title_1']);?></p>
                            <p class="audit__matter-description"><?=t($metaPage['must_description_1']);?></p>
                        </div>
                    </div>
                </div>
                <div class="audit__second-right">
                    <div class="audit__matter">
                        <img src="<?= $template_directory_uri; ?>/out/img_design/audit__matter-second.svg" alt="audit" title="audit">
                        <div>
                            <p class="audit__matter-title"><?=t($metaPage['must_title_2']);?></p>
                            <p class="audit__matter-description"><?=t($metaPage['must_description_2']);?></p>
                        </div>
                    </div>
                    <div class="audit__matter">
                        <img src="<?= $template_directory_uri; ?>/out/img_design/audit__matter-third.svg" alt="audit" title="audit">
                        <div>
                            <p class="audit__matter-title"><?=t($metaPage['must_title_3']);?></p>
                            <p class="audit__matter-description"><?=t($metaPage['must_description_3']);?></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="audit__fourth seo__platform__fourth">
        <div class="wrapper">
            <p class="audit__title"><?=t($metaPage['helps_header']);?></p>
        </div>
    </div>
    <div class="monitoring__about">
        <div class="wrapper">
            <div class="monitoring__about-text">
                <img src="<?= $template_directory_uri; ?>/out/img_design/monitoring__land-search.svg" alt="monitoring">
                <p class="title"><?=t($metaPage['helps_title_1']);?></p>
                <p class="description"><?=t($metaPage['helps_description_1']);?></p>
            </div>
            <div class="monitoring__about-image monitoring__about_1">
                <img src="<?= $template_directory_uri; ?>/out/img_design/monitoring__triangle1.svg" alt="monitoring">
                <img src="<?= $template_directory_uri; ?>/out/img_design/rank-checker-1.png" alt="monitoring">
                <img src="<?= $template_directory_uri; ?>/out/img_design/monitoring__triangle2.svg" alt="monitoring">
            </div>
        </div>
    </div>
    <div class="monitoring__about">
        <div class="wrapper">
            <div class="monitoring__about-text">
                <img src="<?= $template_directory_uri; ?>/out/img_design/monitoring__land-icon2.svg" alt="monitoring">
                <p class="title"><?=t($metaPage['helps_title_2']);?></p>
                <p class="description"><?=t($metaPage['helps_description_2']);?></p>
            </div>
            <div class="monitoring__about-image monitoring__about_2">
                <img src="<?= $template_directory_uri; ?>/out/img_design/rank-checker-2.png" alt="monitoring">
                <img src="<?= $template_directory_uri; ?>/out/img_design/monitoring__triangle3.svg" alt="monitoring">
            </div>
        </div>
    </div>
    <div class="monitoring__about">
        <div class="wrapper">
            <div class="monitoring__about-text">
                <img src="<?= $template_directory_uri; ?>/out/img_design/monitoring__land-icon3.svg" alt="monitoring">
                <p class="title"><?=t($metaPage['helps_title_3']);?></p>
                <p class="description"><?=t($metaPage['helps_description_3']);?></p>
            </div>
            <div class="monitoring__about-image monitoring__about_3">
                <img src="<?= $template_directory_uri; ?>/out/img_design/rank-checker-3.png" alt="monitoring">
                <img src="<?= $template_directory_uri; ?>/out/img_design/monitoring__triangle4.svg" alt="monitoring">
            </div>
        </div>
    </div>
    <?php
    if ( is_user_logged_in() ) { ?>
        <div class="semscale_btns">
            <a href="/tools/seo-platform/" class="btn seo_platform-btn semscale_logedin_link"><?=t($metaPage['button']);?></a>
            <a href="https://surveys.hotjar.com/s?siteId=623968&surveyId=130484" class="btn seo_platform-btn semscale_logedin_link semscale_request">Request for a trial</a>
        </div>
    <?php } else {?>
        <div class="semscale_btns">
            <a class="btn seo_platform-btn semscale_logedin semscale_logedin_link"><?=t($metaPage['button']);?></a>
            <a class="btn seo_platform-btn semscale_logedin semscale_logedin_link semscale_request">Request for a trial</a>
        </div>
    <?php }?>
    <div class="audit__five seo__platform-five">
        <div class="wrapper">
            <p class="audit__title"><?=t($metaPage['use_header']);?></p>
            <div class="audit__five-container">
                <div class="auditLanding-accordion" role="tablist" id="auditLanding-accordion">
                    <div class="panel panel-default active">
                        <div class="panel-heading" role="tab" id="auditLandingOne">
                            <div class="panel-title">
                                <h4 data-toggle="collapse" data-parent="#auditLanding-accordion" href="#collapseauditLandingOne" aria-expanded="true" aria-controls="collapseauditLandingOne">Step 1.</h4>
                            </div>
                        </div>
                        <div id="collapseauditLandingOne" class="panel-collapse collapse show" role="tabpanel" aria-labelledby="auditLandingOne">
                            <div class="panel-body">
                                <p class="title"><?=t($metaPage['use_title_1']);?></p>
                                <p class="description"><?=t($metaPage['use_description_1']);?></p>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="auditLandingTwo">
                            <div class="panel-title">
                                <h4 data-toggle="collapse" data-parent="#auditLanding-accordion" href="#collapseauditLandingTwo" aria-expanded="false" aria-controls="collapseauditLandingTwo">Step 2.</h4>
                            </div>
                        </div>
                        <div id="collapseauditLandingTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="auditLandingTwo">
                            <div class="panel-body">
                                <p class="title"><?=t($metaPage['use_title_2']);?></p>
                                <p class="description"><?=t($metaPage['use_description_2']);?></p>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="auditLandingThree">
                            <div class="panel-title">
                                <h4 data-toggle="collapse" data-parent="#auditLanding-accordion" href="#collapseauditLandingThree" aria-expanded="false" aria-controls="collapseauditLandingThree">Step 3.</h4>
                            </div>
                        </div>
                        <div id="collapseauditLandingThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="auditLandingThree">
                            <div class="panel-body">
                                <p class="title"><?=t($metaPage['use_title_3']);?></p>
                                <p class="description"><?=t($metaPage['use_description_3']);?></p>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="auditLandingFour">
                            <div class="panel-title">
                                <h4 data-toggle="collapse" data-parent="#auditLanding-accordion" href="#collapseauditLandingFour" aria-expanded="false" aria-controls="collapseauditLandingFour">Step 4.</h4>
                            </div>
                        </div>
                        <div id="collapseauditLandingFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="auditLandingFour">
                            <div class="panel-body">
                                <p class="title"><?=t($metaPage['use_title_4']);?></p>
                                <p class="description"><?=t($metaPage['use_description_4']);?></p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="audit__five-images">
                    <img src="<?= $template_directory_uri; ?>/out/img_design/how-to-rank-checker-1.png" alt="arrow" class="auditLandingOne">
                    <img src="<?= $template_directory_uri; ?>/out/img_design/how-to-rank-checker-2.png" alt="arrow" class="auditLandingTwo">
                    <img src="<?= $template_directory_uri; ?>/out/img_design/how-to-rank-checker-3.png" alt="arrow" class="auditLandingThree">
                    <img src="<?= $template_directory_uri; ?>/out/img_design/how-to-rank-checker-4.png" alt="arrow" class="auditLandingFour">
                </div>
            </div>
        </div>
    </div>

    <div id="pricing__btns" style="display: none!important">
        <span class="show_limits">Contact Us</span>
        <?php
        foreach ($pmpro_levels as $level) {
            $_REQUEST['level'] = $level->id;
            require(ABSPATH . "/wp-content/plugins/paid-memberships-pro/preheaders/checkout.php");
            ?>
            <form class="pmpro_form" action="" method="post" name="<?php echo $level->id?>">
                <p class="level__cost" id="level__btn-<?php echo $level->id; ?>"><?php echo intval($level->initial_payment); ?></p>
                <div class="button">
                    <input type="hidden" name="level_id" value="<?php echo $level->id?>" />
                    <input type="hidden" name="submit-checkout" value="<?php echo $level->id?>" />
                    <?php if($gateway == "paypalexpress" && $pmpro_requirebilling) { ?>
                        <input type="submit" class="pmpro_btn-submit-checkout btn <?php if($customerMembershipLevel == $level->id){echo 'your__level';}?>" value="<?php echo $customerMembershipLevel == $level->id ? 'Your level' : 'Subscribe' ?>" name="<?php echo $level->id?>">
                    <?php } else { ?>
                        <span id="pmpro_submit_span"><input type="submit" class="pmpro_btn-submit-checkout btn <?php if($customerMembershipLevel == $level->id){echo 'your__level';}?>" value="<?php echo $customerMembershipLevel == $level->id ? 'Your level' : 'Subscribe' ?>" /></span>
                    <?php } ?>
                </div>
            </form>
        <?php } ?>
    </div>
    <div class="checkout__design rank_page">
        <div class="wrapper">
            <h2 class="title"><?=t($pricing['pricing_title']);?></h2>
            <div class="checkout__design-slider">
                <p><?=t($pricing['select_count']);?></p>
                <div class="pricing__slider">
                    <input id="checkout__thread" type="text"
                           data-provide="slider"
                           data-slider-ticks="[0, 5, 10, 15, 20, 25, 30, 35, 40]"
                           data-slider-min="0"
                           data-slider-max="35"
                           data-slider-step="5"
                           data-slider-value="5"
                           data-slider-tooltip="hide" />
                    <i class="plural_counter"><?=t($pricing['websites']);?></i>
                    <i class="single_counter"><?=t($pricing['website']);?></i>
                </div>
                <div class="checkout__design-descript">
                    <span><?=t($pricing['save_money']);?></span>
                    <img src="<?= $template_directory_uri; ?>/out/img_design/checkout__arrow.svg" alt="checkout__arrow">
                </div>
            </div>
            <div class="checkout__design-pricing">
                <div class="pricing__block">
                    <p class="level__name"><?=t($pricing['startup']);?></p>
                    <p class="level__descript"><?=t($pricing['startup_description']);?></p>
                    <div class="level__cost"><span>$</span><i class="startup__cost">
                            <?php
                            foreach ($pmpro_levels as $level) {
                                if ($level->name == 'Startup-1') {
                                    $_REQUEST['level'] = $level->id;
                                    require(ABSPATH . "/wp-content/plugins/paid-memberships-pro/preheaders/checkout.php");
                                    ?>
                                    <?php echo intval($level->initial_payment); ?>
                                <?php }
                            } ?>
                        </i>
                        <div class="pricing__sale">
                            <span><i class="startup__save">28</i>$</span>
                            <img src="<?= $template_directory_uri; ?>/out/img_design/pricing__arrow.svg" alt="checkout__arrow">
                        </div>
                    </div>
                    <p class="level__month"><?=t($pricing['per_month']);?></p>
                    <?php
                    foreach ($pmpro_levels as $level) {
                        if ($level->name == 'Startup-1') {
                            $_REQUEST['level'] = $level->id;
                            require(ABSPATH . "/wp-content/plugins/paid-memberships-pro/preheaders/checkout.php");
                            ?>
                            <form class="pmpro_form" action="" method="post">
                                <div class="button startup__btn">
                                    <input type="hidden" name="level_id" value="<?php echo $level->id?>" />
                                    <input type="hidden" name="submit-checkout" value="<?php echo $level->id?>" />
                                    <?php if($gateway == "paypalexpress" && $pmpro_requirebilling) { ?>
                                        <input type="submit" class="pmpro_btn-submit-checkout btn <?php if($customerMembershipLevel == $level->id){echo 'your__level';}?>" value="<?php echo $customerMembershipLevel == $level->id ? get_field('your_plan', 6179) : get_field('subscribe', 6179) ?>" name="<?php echo $level->id?>">
                                    <?php } else { ?>
                                        <span id="pmpro_submit_span"><input type="submit" class="pmpro_btn-submit-checkout btn <?php if($customerMembershipLevel == $level->id){echo 'your__level';}?>" value="<?php echo $customerMembershipLevel == $level->id ? get_field('your_plan', 6179) : get_field('subscribe', 6179) ?>" /></span>
                                    <?php } ?>
                                </div>
                            </form>
                        <?php }
                    } ?>
                    <div class="level__descriptions">
                        <p><b><span class="domains__count">1</span></b> <i class="plural"><?=t($pricing['websites']);?></i><i class="single"><?=t($pricing['website']);?></i>
                        </p>
                        <p><b>100</b> <?=t($pricing['urls_per_website']);?></p>
                        <p><?=t($pricing['hourly_crawling_frequency']);?></p>
                        <p><b>50</b> <?=t($pricing['keywords_per_website']);?></p>
                        <p><?=t($pricing['daily_keyword_tracking']);?></p>
                        <p><b>100</b> <?=t($pricing['backlinks_per_website']);?></p>
                        <p><?=t($pricing['daily_backlink_tracking']);?></p>
                    </div>
                </div>
                <div class="pricing__block">
                    <div class="most__popular"><?=t($pricing['most_popular']);?></div>
                    <p class="level__name"><?=t($pricing['growing']);?></p>
                    <p class="level__descript"><?=t($pricing['growing_description']);?></p>
                    <div class="level__cost"><span>$</span><i class="bussines__cost">
                            <?php
                            foreach ($pmpro_levels as $level) {
                                if ($level->name == 'Growth-1') {
                                    $_REQUEST['level'] = $level->id;
                                    require(ABSPATH . "/wp-content/plugins/paid-memberships-pro/preheaders/checkout.php");
                                    ?>
                                    <?php echo intval($level->initial_payment); ?>
                                <?php }
                            } ?>
                        </i>
                        <div class="pricing__sale">
                            <span><i class="growing__save">114</i>$</span>
                            <img src="<?= $template_directory_uri; ?>/out/img_design/pricing__arrow.svg" alt="checkout__arrow">
                        </div>
                    </div>
                    <p class="level__month"><?=t($pricing['per_month']);?></p>
                    <?php
                    foreach ($pmpro_levels as $level) {
                        if ($level->name == 'Growth-1') {
                            $_REQUEST['level'] = $level->id;
                            require(ABSPATH . "/wp-content/plugins/paid-memberships-pro/preheaders/checkout.php");
                            ?>
                            <form class="pmpro_form" action="" method="post">
                                <div class="button bussines__btn">
                                    <input type="hidden" name="level_id" value="<?php echo $level->id?>" />
                                    <input type="hidden" name="submit-checkout" value="<?php echo $level->id?>" />
                                    <?php if($gateway == "paypalexpress" && $pmpro_requirebilling) { ?>
                                        <input type="submit" class="pmpro_btn-submit-checkout btn <?php if($customerMembershipLevel == $level->id){echo 'your__level';}?>" value="<?php echo $customerMembershipLevel == $level->id ? get_field('your_plan', 6179) : get_field('subscribe', 6179) ?>" name="<?php echo $level->id?>">
                                    <?php } else { ?>
                                        <span id="pmpro_submit_span"><input type="submit" class="pmpro_btn-submit-checkout btn <?php if($customerMembershipLevel == $level->id){echo 'your__level';}?>" value="<?php echo $customerMembershipLevel == $level->id ? get_field('your_plan', 6179) : get_field('subscribe', 6179) ?>" /></span>
                                    <?php } ?>
                                </div>
                            </form>
                        <?php } } ?>
                    <div class="level__descriptions">
                        <p><b><span class="domains__count">1</span></b> <i class="plural"><?=t($pricing['websites']);?></i><i class="single"><?=t($pricing['website']);?></i>
                        </p>
                        <p><b>1000</b> <?=t($pricing['urls_per_website']);?></p>
                        <p><?=t($pricing['hourly_crawling_frequency']);?></p>
                        <p><b>200</b> <?=t($pricing['keywords_per_website']);?></p>
                        <p><?=t($pricing['daily_keyword_tracking']);?></p>
                        <p><b>500</b> <?=t($pricing['backlinks_per_website']);?></p>
                        <p><?=t($pricing['daily_backlink_tracking']);?></p>
                    </div>
                </div>
                <div class="pricing__block">
                    <p class="level__name"><?=t($pricing['business']);?></p>
                    <p class="level__descript"><?=t($pricing['business_description']);?></p>
                    <div class="level__cost"><span>$</span><i class="enterprise__cost">
                            <?php
                            foreach ($pmpro_levels as $level) {
                                if ($level->name == 'Business-1') {
                                    $_REQUEST['level'] = $level->id;
                                    require(ABSPATH . "/wp-content/plugins/paid-memberships-pro/preheaders/checkout.php");
                                    ?>
                                    <?php echo intval($level->initial_payment); ?>
                                <?php }
                            } ?>
                        </i>
                        <div class="pricing__sale">
                            <span><i class="business__save">358</i>$</span>
                            <img src="<?= $template_directory_uri; ?>/out/img_design/pricing__arrow.svg" alt="checkout__arrow">
                        </div>
                    </div>
                    <p class="level__month"><?=t($pricing['per_month']);?></p>
                    <?php
                    foreach ($pmpro_levels as $level) {
                        if ($level->name == 'Business-1') {
                            $_REQUEST['level'] = $level->id;
                            require(ABSPATH . "/wp-content/plugins/paid-memberships-pro/preheaders/checkout.php");
                            ?>
                            <form class="pmpro_form" action="" method="post">
                                <div class="button enterprise__btn">
                                    <input type="hidden" name="level_id" value="<?php echo $level->id?>" />
                                    <input type="hidden" name="submit-checkout" value="<?php echo $level->id?>" />
                                    <?php if($gateway == "paypalexpress" && $pmpro_requirebilling) { ?>
                                        <input type="submit" class="pmpro_btn-submit-checkout btn <?php if($customerMembershipLevel == $level->id){echo 'your__level';}?>" value="<?php echo $customerMembershipLevel == $level->id ? get_field('your_plan', 6179) : get_field('subscribe', 6179) ?>" name="<?php echo $level->id?>">
                                    <?php } else { ?>
                                        <span id="pmpro_submit_span"><input type="submit" class="pmpro_btn-submit-checkout btn <?php if($customerMembershipLevel == $level->id){echo 'your__level';}?>" value="<?php echo $customerMembershipLevel == $level->id ? get_field('your_plan', 6179) : get_field('subscribe', 6179) ?>" /></span>
                                    <?php } ?>
                                </div>
                            </form>
                        <?php } } ?>
                    <div class="level__descriptions">
                        <p><b><span class="domains__count">1</span></b> <i class="plural"><?=t($pricing['websites']);?></i><i class="single"><?=t($pricing['website']);?></i>
                        </p>
                        <p><b>10000</b> <?=t($pricing['urls_per_website']);?></p>
                        <p><?=t($pricing['hourly_crawling_frequency']);?></p>
                        <p><b>500</b> <?=t($pricing['keywords_per_website']);?></p>
                        <p><?=t($pricing['daily_keyword_tracking']);?></p>
                        <p><b>1000</b> <?=t($pricing['backlinks_per_website']);?></p>
                        <p><?=t($pricing['daily_backlink_tracking']);?></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="stars__block">
        <?php if(function_exists('the_ratings')) { the_ratings(); } ?>
    </div>
</main>