<?php
$meta = get_post_meta_all(get_option('page_on_front'));
$metaDesign = get_post_meta_all(url_to_postid( '/header-footer-design/' ));
$metaCategory = get_post_meta_all(url_to_postid( '/knowledge-base/' ));
$template_directory_uri = get_template_directory_uri();
?>
<main>
    <div class="knbs__first search__results">
        <div class="wrapper">
            <h1>Sitechecker SEO Blog</h1>
            <form class="article__seo-search search knbs__search" method="get" action="<?php echo home_url(); ?>" role="search">
                <input class="search-input" type="search" name="s" placeholder="<?=t($metaCategory['placeholder']);?>">
                <button class="search-submit" type="submit" role="button"><img src="<?= $template_directory_uri; ?>/out/img_design/search.svg" alt="arrow" title="arrow"></button>
            </form>
        </div>
    </div>
    <div class="pages_wrapper-results">
        <div class="wrapper">
            <?php
            if($wp_query->found_posts == 0){
                echo '<p>Unfortunately, we didn\'t find your request.</p><p>Suggestions:</p><ul><li>Use chat at the right if you have a specific question</li><li>Use <a href="https://www.google.com/" target="_blank">google search</a> to collect other guides</li></ul>';
            } else{
                echo '<p class="search_results"><span>'.$wp_query->found_posts.'</span> Search Results for <span>'.get_search_query().'</span></p>';
            }
            ?>
        </div>
    </div>
    <div class="wrapper">
        <div class="knbs__latest-container">
            <?php get_template_part('design/loop'); ?>
        </div>
        <?php get_template_part('pagination'); ?>
    </div>
</main>