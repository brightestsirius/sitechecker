<?php
$template_directory_uri = get_template_directory_uri();
$metaDesign = get_post_meta_all(url_to_postid('/header-footer-design/'));
$meta = get_post_meta_all(get_option('page_on_front'));
$pricing = get_post_meta_all(url_to_postid('/pricing-content/'));
?>
<footer>
    <div class="footer">
        <div class="wrapper">
            <div class="footer__logo">
                <?php if (is_front_page()) { ?>
                    <img src="<?= $template_directory_uri; ?>/out/img_design/logo.svg" alt="logo" title="logo">
                <?php } else { ?>
                    <a href="<?= home_url(); ?>/">
                        <img src="<?= $template_directory_uri; ?>/out/img_design/logo.svg" alt="logo" title="logo">
                    </a>
                <?php } ?>
                <p>© 2019 Sitechecker. All rights reserved.</p>
            </div>
            <div class="footer__menu">
                <p><?= t($metaDesign['footer_products_title']); ?></p>
                <?php footer_products_design(); ?>
            </div>
            <div class="footer__menu">
                <p><?= t($metaDesign['footer_company_title']); ?></p>
                <ul>
                    <?php footer_company_design(); ?>
                </ul>
            </div>
            <div class="footer__menu">
                <p><?= t($metaDesign['footer_resources_title']); ?></p>
                <ul>
                    <?php footer_resources_design(); ?>
                </ul>
            </div>
            <div>
                <ul class="language_menu <?php echo get_locale(); ?>">
                    <li>
                        <a title="<?php echo get_locale(); ?>" href="#" class="current_lang">
                            <svg class="lang__arrow" width="9" height="5" viewBox="0 0 9 5" fill="none"
                                 xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd" clip-rule="evenodd"
                                      d="M4.499 0L9 2.5V4.896L4.499 2.38L0 5V2.5L4.499 0Z"
                                      transform="translate(9 5) rotate(-180)" fill="#495c6a"/>
                            </svg>
                        </a>
                        <div class="sub-menu_container">
                            <ul class="sub-menu">
                                <?php
                                global $wp_query;
                                $current_locale = get_locale();
                                $locales = ['en', 'de', 'ru', 'fr', 'it', 'nl', 'es', 'no', 'pt', 'sv', 'tr', 'pl', 'ja'];
                                $postId = $wp_query->post->ID;
                                $title = $wp_query->post->post_title_ml;
                                if ($wp_query->is_category()) {
                                    $term = $wp_query->queried_object;
                                    $title = $term->cat_name;
                                }

                                $base_url = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on' ? 'https' : 'http') . '://' . $_SERVER['HTTP_HOST'];
                                $url = $base_url . $_SERVER["REQUEST_URI"];
                                $navClass = '';
                                foreach ($locales as $value) {
                                    //echo '<div style="background: pink;font-size: 10px">' . $value . ' : ' . qtrans_use($value, $title, false) . '</div>';
                                    if ('' == qtrans_use($value, $title, false)) {
                                        $navClass .= $value . " ";
                                    }

                                    if ($value == 'en') {
                                        $link = $url;
                                        $link = str_replace('/' . $current_locale . '/', '/en/', $link);
                                    } else {
                                        $link = qtrans_convertURL($url, $value);
                                    }
                                    if (qtranxf_getLanguage() !== $value) {
                                        if (qtrans_use($value, $title, false) !== '') {
                                            echo '<li><a title="' . $value . '" href="' . $link . '"></a></li>';
                                        }
                                    }
                                }
                                ?>
                            </ul>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</footer>
<div class="footer__cookie">
    <div class="wrapper">
        <img src="<?= $template_directory_uri; ?>/out/img/cookies.svg" alt="cookies">
        <p><?= t($meta['cookies']); ?></p>
        <img src="<?= $template_directory_uri; ?>/out/img/close_btn.svg" alt="close" class="footer__cookie-close">
    </div>
</div>
<div class="login__popup">
    <div class="login__popup-block">
        <img src="<?= $template_directory_uri; ?>/out/img_design/close.svg" alt="close" class="login__popup-close">
        <div class="limit__popup">
            <img src="<?= $template_directory_uri; ?>/out/img_design/white_sitechecker.svg" alt="limit" class="logo">
            <div class="text">
                <p><?= t($metaDesign['limit_popup_title']); ?></p>
            </div>
            <ul class="products">
                <li><a href="">
                        <svg width="14" height="22" viewBox="0 0 14 22" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M13.4194 11.0403L8.47127 8.03611L10.9658 1.76489C11.0428 1.60111 11.086 1.41656 11.086 1.22222C11.086 0.547556 10.5667 0 9.9227 0C9.63218 0.00488889 9.3895 0.101444 9.19699 0.261556C9.15848 0.293333 9.13515 0.312889 9.11531 0.334889L0.364705 9.01267C0.0975201 9.27789 -0.0343224 9.65922 0.00768055 10.0418C0.0496835 10.4243 0.263198 10.7641 0.580554 10.9584L5.5299 13.9651L3.00622 20.3084C2.79271 20.8413 2.96655 21.4573 3.41925 21.7849C3.61993 21.9291 3.85328 22 4.08196 22C4.37132 22 4.66067 21.8863 4.88469 21.6651L13.6353 12.9849C13.9025 12.7197 14.0343 12.3383 13.9923 11.9558C13.9503 11.5732 13.7368 11.2334 13.4194 11.0403Z"
                                  fill="#CCCCCC"/>
                        </svg>
                        <div class="products__description">
                            <p><?= t($metaDesign['audit_title']); ?></p>
                            <span><?= t($metaDesign['audit_description']); ?></span>
                        </div>
                    </a>
                </li>
                <li><a href="">
                        <svg width="24" height="23" viewBox="0 0 24 23" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M10.2347 8.37864L8.70534 6.88655C9.1759 6.19789 9.41117 5.39446 9.41117 4.59103C9.41117 2.06596 7.29366 0 4.70559 0C2.11751 0 0 2.06596 0 4.59103C0 7.1161 2.11751 9.18207 4.70559 9.18207C5.52906 9.18207 6.35254 8.95251 7.05838 8.49341L8.5877 9.9855C8.82298 10.215 9.52881 10.6742 10.2347 9.9855C10.7052 9.52639 10.7052 8.83774 10.2347 8.37864ZM4.70559 6.88655C3.41155 6.88655 2.35279 5.85357 2.35279 4.59103C2.35279 3.3285 3.41155 2.29552 4.70559 2.29552C5.99962 2.29552 7.05838 3.3285 7.05838 4.59103C7.05838 5.85357 5.99962 6.88655 4.70559 6.88655Z"
                                  transform="translate(12.9403 12.6254)" fill="#CCCCCC"/>
                            <path d="M17.646 0H1.1764C0.470559 0 0 0.459103 0 1.14776V21.8074C0 22.4961 0.470559 22.9552 1.1764 22.9552H13.7638C11.8816 21.6926 10.5876 19.6267 10.5876 17.2164C10.5876 13.4288 13.7638 10.3298 17.646 10.3298C17.9989 10.3298 18.4694 10.3298 18.8223 10.4446V1.14776C18.8223 0.459103 18.3518 0 17.646 0ZM9.41117 9.18207H3.52919C2.82335 9.18207 2.35279 8.60819 2.35279 8.03431C2.35279 7.34565 2.82335 6.88655 3.52919 6.88655H9.41117C10.117 6.88655 10.5876 7.34565 10.5876 8.03431C10.5876 8.60819 10.117 9.18207 9.41117 9.18207ZM15.2932 4.59103H3.52919C2.82335 4.59103 2.35279 4.01715 2.35279 3.44327C2.35279 2.75462 2.82335 2.29552 3.52919 2.29552H15.2932C15.999 2.29552 16.4696 2.75462 16.4696 3.44327C16.4696 4.01715 15.999 4.59103 15.2932 4.59103Z"
                                  fill="#CCCCCC"/>
                        </svg>
                        <div class="products__description">
                            <p><?= t($metaDesign['health_checker_title']); ?></p>
                            <span><?= t($metaDesign['health_checker_description']); ?></span>
                        </div>
                    </a>
                </li>
                <li><a href="">
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1"
                             id="Capa_1" x="0px" y="0px" width="512px" height="512px" viewBox="0 0 457.03 457.03"
                             style="enable-background:new 0 0 457.03 457.03;" xml:space="preserve">
<g>
    <path d="M421.512,207.074l-85.795,85.767c-47.352,47.38-124.169,47.38-171.529,0c-7.46-7.439-13.296-15.821-18.421-24.465   l39.864-39.861c1.895-1.911,4.235-3.006,6.471-4.296c2.756,9.416,7.567,18.33,14.972,25.736c23.648,23.667,62.128,23.634,85.762,0   l85.768-85.765c23.666-23.664,23.666-62.135,0-85.781c-23.635-23.646-62.105-23.646-85.768,0l-30.499,30.532   c-24.75-9.637-51.415-12.228-77.373-8.424l64.991-64.989c47.38-47.371,124.177-47.371,171.557,0   C468.869,82.897,468.869,159.706,421.512,207.074z M194.708,348.104l-30.521,30.532c-23.646,23.634-62.128,23.634-85.778,0   c-23.648-23.667-23.648-62.138,0-85.795l85.778-85.767c23.665-23.662,62.121-23.662,85.767,0   c7.388,7.39,12.204,16.302,14.986,25.706c2.249-1.307,4.56-2.369,6.454-4.266l39.861-39.845   c-5.092-8.678-10.958-17.03-18.421-24.477c-47.348-47.371-124.172-47.371-171.543,0L35.526,249.96   c-47.366,47.385-47.366,124.172,0,171.553c47.371,47.356,124.177,47.356,171.547,0l65.008-65.003   C246.109,360.336,219.437,357.723,194.708,348.104z"
          fill="#4c5b8b"/>
</g>
</svg>
                        <div class="products__description">
                            <p><?= t($metaDesign['backlink_title']); ?></p>
                            <span><?= t($metaDesign['backlink_description']); ?></span>
                        </div>
                    </a>
                </li>
                <li><a href="">
                        <svg width="20" height="23" viewBox="0 0 20 23" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M9.96031 22.8655C11.2493 22.8655 12.3039 21.8365 12.3039 20.5789H7.61671C7.61671 21.8365 8.67133 22.8655 9.96031 22.8655ZM17.577 16.0058V9.71784C17.577 6.17368 15.1162 3.3155 11.718 2.5152V1.71491C11.718 0.800292 10.8978 0 9.96031 0C9.02287 0 8.20261 0.800292 8.20261 1.71491V2.5152C4.80439 3.3155 2.3436 6.17368 2.3436 9.71784V16.0058L0 18.2924V19.4357H19.9206V18.2924L17.577 16.0058Z"
                                  fill="#CCCCCC"/>
                        </svg>
                        <div class="products__description">
                            <p><?= t($metaDesign['monitor_title']); ?></p>
                            <span><?= t($metaDesign['monitor_description']); ?></span>
                        </div>
                    </a>
                </li>
                <li><a href="">
                        <svg xmlns="http://www.w3.org/2000/svg" height="512px" viewBox="0 0 512 512" width="512px"
                             class="">
                            <g>
                                <path d="m76 240c12.101562 0 23.054688-4.855469 31.148438-12.652344l44.402343 22.199219c-.222656 1.808594-.550781 3.585937-.550781 5.453125 0 24.8125 20.1875 45 45 45s45-20.1875 45-45c0-6.925781-1.703125-13.410156-4.511719-19.277344l60.234375-60.234375c5.867188 2.808594 12.351563 4.511719 19.277344 4.511719 24.8125 0 45-20.1875 45-45 0-4.671875-.917969-9.089844-2.246094-13.328125l52.335938-39.242187c7.140625 4.769531 15.699218 7.570312 24.910156 7.570312 24.8125 0 45-20.1875 45-45s-20.1875-45-45-45-45 20.1875-45 45c0 4.671875.917969 9.089844 2.246094 13.328125l-52.335938 39.242187c-7.140625-4.769531-15.699218-7.570312-24.910156-7.570312-24.8125 0-45 20.1875-45 45 0 6.925781 1.703125 13.410156 4.511719 19.277344l-60.234375 60.234375c-5.867188-2.808594-12.351563-4.511719-19.277344-4.511719-12.101562 0-23.054688 4.855469-31.148438 12.652344l-44.402343-22.199219c.222656-1.808594.550781-3.585937.550781-5.453125 0-24.8125-20.1875-45-45-45s-45 20.1875-45 45 20.1875 45 45 45zm0 0"
                                      data-original="#000000" class="active-path" data-old_color="#007bff"
                                      fill="#007bff"/>
                                <path d="m497 482h-16v-317c0-8.289062-6.710938-15-15-15h-60c-8.289062 0-15 6.710938-15 15v317h-30v-227c0-8.289062-6.710938-15-15-15h-60c-8.289062 0-15 6.710938-15 15v227h-30v-107c0-8.289062-6.710938-15-15-15h-60c-8.289062 0-15 6.710938-15 15v107h-30v-167c0-8.289062-6.710938-15-15-15h-60c-8.289062 0-15 6.710938-15 15v167h-16c-8.289062 0-15 6.710938-15 15s6.710938 15 15 15h482c8.289062 0 15-6.710938 15-15s-6.710938-15-15-15zm0 0"
                                      data-original="#000000" class="active-path" data-old_color="#007bff"
                                      fill="#007bff"/>
                            </g>
                        </svg>
                        <div class="products__description">
                            <p><?= t($metaDesign['website_rank_tracker']); ?></p>
                            <span><?= t($metaDesign['website_rank_tracker_description']); ?></span>
                        </div>
                    </a>
                </li>
                <li><a href="">
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1"
                             id="Capa_1" x="0px" y="0px" viewBox="0 0 512.039 512.039"
                             style="enable-background:new 0 0 512.039 512.039;" xml:space="preserve" width="512px"
                             height="512px" class=""><g>
                                <g>
                                    <path d="M255.976,421.19c-42.568,0-84.668-16.553-116.631-48.516c-64.336-64.336-64.336-169.014,0-233.35s169.014-64.336,233.35,0   c40.911,40.911,56.501,98.461,44.507,152.285c22.752,5.313,38.488,15.561,53.386,30.469c0.819-2.651,1.904-5.215,2.624-7.896   l30.517-14.766c5.083-2.549,8.291-7.734,8.291-13.418v-60c0-5.684-3.208-10.869-8.291-13.418l-30.517-14.751   c-5.068-18.94-12.612-37.119-22.5-54.214l10.459-31.362c1.787-5.391,0.381-11.338-3.633-15.352l-42.422-42.422   c-4.014-4.014-9.976-5.391-15.352-3.633l-31.362,10.459c-17.095-9.888-35.273-17.432-54.214-22.5L299.438,8.291   C296.889,3.208,291.703,0,286.02,0h-60c-5.684,0-10.869,3.208-13.418,8.291l-14.751,30.517c-18.94,5.068-37.119,12.612-54.214,22.5   l-31.362-10.459c-5.391-1.758-11.338-0.381-15.352,3.633L54.501,96.903c-4.014,4.014-5.42,9.961-3.633,15.352l10.459,31.362   c-9.888,17.095-17.432,35.273-22.5,54.214L8.311,212.582C3.228,215.131,0.02,220.316,0.02,226v60   c0,5.684,3.208,10.869,8.291,13.418l30.517,14.751c5.068,18.94,12.612,37.119,22.5,54.214l-10.459,31.362   c-1.787,5.391-0.381,11.338,3.633,15.352l42.422,42.422c4.014,4.014,9.961,5.435,15.352,3.633l31.362-10.459   c17.095,9.888,35.273,17.432,54.214,22.5l14.751,30.517c2.549,5.083,7.734,8.291,13.418,8.291h60   c5.684,0,10.869-3.208,13.418-8.291l14.766-30.517c2.681-0.719,5.244-1.805,7.896-2.624c-15.14-15.13-25.448-30.937-30.645-53.641   C279.719,419.533,267.852,421.19,255.976,421.19z"
                                          data-original="#000000" class="active-path" data-old_color="#007bff"
                                          fill="#007bff"/>
                                    <path d="M490.035,384l-43.41-43.464c-27.669-27.671-60.409-27.868-85.901-13.767l-21.089-21.085   c13.215-26.822,10.964-61.179-14.079-86.203l-42.422-42.437c-29.238-29.238-76.831-29.238-106.069,0   c-29.224,29.238-29.224,76.831,0,106.069l42.437,42.422c14.224,14.224,33.486,21.958,53.159,21.958   c12.303,0,23.495-2.797,33.263-7.648l21.227,21.238c-13.695,25.889-13.331,58.784,13.406,85.521l43.425,43.449   c29.282,29.313,76.772,29.313,106.055,0C519.348,460.773,519.348,413.282,490.035,384z M208.881,251.283   c-11.689-11.689-11.689-30.718,0.015-42.422c11.631-11.662,30.727-11.694,42.422,0l42.407,42.437   c6.249,6.224,8.909,13.291,8.97,20.159c-2.805-1.124-5.831-1.716-8.955-1.716c-6.431,0-12.451,2.49-16.963,7.017   c-4.526,4.526-7.017,10.547-7.017,16.963c0,3.226,0.631,6.351,1.832,9.238c-6.794-0.094-13.865-2.829-20.274-9.238L208.881,251.283   z M458.233,458.224c-11.792,11.792-30.678,11.773-42.422,0.029l-43.439-43.479c-5.394-5.373-8.413-12.586-8.756-20.057   c8.061,3.162,17.837,1.951,25.749-5.373c6.917-6.928,8.7-17.069,5.372-25.669c7.508,0.245,14.784,3.411,20.072,8.677l43.395,43.464   C469.972,427.555,469.987,446.471,458.233,458.224z"
                                          data-original="#000000" class="active-path" data-old_color="#007bff"
                                          fill="#007bff"/>
                                </g>
                            </g> </svg>
                        <div class="products__description">
                            <p><?= t($metaDesign['website_backlink_tracker']); ?></p>
                            <span><?= t($metaDesign['website_backlink_tracker_description']); ?></span>
                        </div>
                    </a>
                </li>
            </ul>
        </div>
        <div class="limit__popup-sign">
            <div class="signin__block">
                <?= do_shortcode('[theme-my-login instance=1 default_action=login show_title=0 show_reg_link=0]'); ?>
                <div class="login__popup-actions">
                    <div class="popup-actions__wrapper">
                        <button class="popup__signup" type="button"><?= t($metaDesign['sign_up']); ?></button>
                        <button class="popup__pass" type="button"><?= t($metaDesign['forgot_password']); ?></button>
                    </div>
                </div>
            </div>
            <div class="signup__block">
                <?= do_shortcode('[theme-my-login instance=2 default_action=register show_title=0 show_pass_link=0 show_log_link=0]'); ?>
                <div class="login__terms"><?= t($metaDesign['terms_and_conditions']); ?></div>
                <div class="login__popup-actions">
                    <div class="popup-actions__wrapper">
                        <p><?= t($metaDesign['already_have_an_account']); ?></p>
                        <button class="popup__signin"><?= t($metaDesign['sign_in']); ?></button>
                    </div>
                </div>
            </div>
            <div class="resetpass__block">
                <?= do_shortcode('[theme-my-login instance=3 default_action=lostpassword show_title=0 show_reg_link=0 show_log_link=0]'); ?>
                <div class="login__popup-actions">
                    <div class="popup-actions__wrapper">
                        <p><?= t($metaDesign['not_a_member_yet']); ?></p>
                        <button class="popup__signup"><?= t($metaDesign['sign_up']); ?></button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="resetpass__popup">
    <div class="resetpass__popup-block">
        <img src="<?= $template_directory_uri; ?>/out/img_design/close.svg" alt="close" title="close"
             class="login__popup-close">
        <div class="circle-loader">
            <div class="checkmark draw"></div>
        </div>
        <p class="title"><?= t($metaDesign['check_your_email']); ?></p>
        <div class="description"><?= t($metaDesign['sent_email_with_new_password']); ?> <img
                    src="<?= $template_directory_uri; ?>/out/img_design/envelope.svg" alt="envelope" title="envelope">
        </div>
    </div>
</div>
<div class="newpass__popup">
    <div class="resetpass__popup-block">
        <img src="<?= $template_directory_uri; ?>/out/img_design/close.svg" alt="close" title="close"
             class="login__popup-close">
        <div class="circle-loader">
            <div class="checkmark draw"></div>
        </div>
        <p class="title"><?= t($metaDesign['password_reset_successfully']); ?></p>
    </div>
</div>
<div class="checkout__popup_container">
    <div class="checkout__popup">
        <img src="<?= $template_directory_uri; ?>/out/img_design/close.svg" alt="close" class="checkout__popup-close">
        <div class="checkout__popup-header">
            <img src="<?= $template_directory_uri; ?>/out/img_design/conversation.svg" alt="conversation">
            <div>
                <p><?= t($pricing['contact_us']); ?></p>
                <p><?= t($pricing['contact_us_description']); ?></p>
            </div>
        </div>
        <form action="" class="checkout__popup-body">
            <div>
                <div class="validation validation_name">
                    <input type="text" placeholder="First Name" class="checkout__name" required>
                    <p>ERROR: The Name field is empty.</p>
                </div>
                <div class="validation validation_lastname">
                    <input type="text" placeholder="Last Name" class="checkout__lastname" required>
                    <p>ERROR: The Last Name field is empty.</p>
                </div>
            </div>
            <div class="validation validation_email">
                <input type="email" placeholder="Work Email" class="checkout__email" required>
                <p>ERROR: The Work Email field is empty.</p>
            </div>
            <div>
                <select class="select2-active" id="contact_tel">
                    <option value=""></option>
                    <option class="af" value="AF+93">
                        Afghanistan +93
                    </option>
                    <option class="al" value="AL+355">
                        Albania +355
                    </option>
                    <option class="dz" value="DZ+213">
                        Algeria +213
                    </option>
                    <option class="ad" value="AD+376">
                        Andorra +376
                    </option>
                    <option class="ao" value="AO+244">
                        Angola +244
                    </option>
                    <option class="ai" value="AI+1264">
                        Anguilla +1264
                    </option>
                    <option class="ag" value="AG+1268">
                        Antigua and Barbuda +1268
                    </option>
                    <option class="ar" value="AR+54">
                        Argentina +54
                    </option>
                    <option class="am" value="AM+374">
                        Armenia +374
                    </option>
                    <option class="aw" value="AW+297">
                        Aruba +297
                    </option>
                    <option class="au" value="AU+61">
                        Australia +61
                    </option>
                    <option class="at" value="AT+43">
                        Austria +43
                    </option>
                    <option class="az" value="AZ+994">
                        Azerbaijan +994
                    </option>
                    <option class="bs" value="BS+1242">
                        Bahamas +1242
                    </option>
                    <option class="bh" value="BH+973">
                        Bahrain +973
                    </option>
                    <option class="bd" value="BD+880">
                        Bangladesh +880
                    </option>
                    <option class="bb" value="BB+1246">
                        Barbados +1246
                    </option>
                    <option class="by" value="BY+375">
                        Belarus +375
                    </option>
                    <option class="be" value="BE+32">
                        Belgium +32
                    </option>
                    <option class="bz" value="BZ+501">
                        Belize +501
                    </option>
                    <option class="bj" value="BJ+229">
                        Benin +229
                    </option>
                    <option class="bm" value="BM+1441">
                        Bermuda +1441
                    </option>
                    <option class="bt" value="BT+975">
                        Bhutan +975
                    </option>
                    <option class="bo" value="BO+591">
                        Bolivia +591
                    </option>
                    <option class="ba" value="BA+387">
                        Bosnia and Herzegovina +387
                    </option>
                    <option class="bw" value="BW+267">
                        Botswana +267
                    </option>
                    <option class="br" value="BR+55">
                        Brazil +55
                    </option>
                    <option class="bn" value="BN+673">
                        Brunei Darussalam +673
                    </option>
                    <option class="bg" value="BG+359">
                        Bulgaria +359
                    </option>
                    <option class="bf" value="BF+226">
                        Burkina Faso +226
                    </option>
                    <option class="bi" value="BI+257">
                        Burundi +257
                    </option>
                    <option class="cv" value="CV+238">
                        Cabo Verde +238
                    </option>
                    <option class="kh" value="KH+855">
                        Cambodia +855
                    </option>
                    <option class="cm" value="CM+237">
                        Cameroon +237
                    </option>
                    <option class="ca" value="CA+1">
                        Canada +1
                    </option>
                    <option class="ky" value="KY+1345">
                        Cayman Islands +1345
                    </option>
                    <option class="cf" value="CF+236">
                        Central African Republic +236
                    </option>
                    <option class="td" value="TD+235">
                        Chad +235
                    </option>
                    <option class="cl" value="CL+56">
                        Chile +56
                    </option>
                    <option class="cn" value="CN+86">
                        China +86
                    </option>
                    <option class="co" value="CO+57">
                        Colombia +57
                    </option>
                    <option class="cg" value="CG+242">
                        Congo +242
                    </option>
                    <option class="cd" value="CD+243">
                        Congo, the Democratic Republic of the +243
                    </option>
                    <option class="ck" value="CK+682">
                        Cook Islands +682
                    </option>
                    <option class="cr" value="CR+506">
                        Costa Rica +506
                    </option>
                    <option class="ci" value="CI+225">
                        Côte d'Ivoire +225
                    </option>
                    <option class="hr" value="HR+385">
                        Croatia +385
                    </option>
                    <option class="cu" value="CU+53">
                        Cuba +53
                    </option>
                    <option class="cy" value="CY+357">
                        Cyprus +357
                    </option>
                    <option class="cz" value="CZ+420">
                        Czech Republic +420
                    </option>
                    <option class="dk" value="DK+45">
                        Denmark +45
                    </option>
                    <option class="dj" value="DJ+253">
                        Djibouti +253
                    </option>
                    <option class="dm" value="DM+1767">
                        Dominica +1767
                    </option>
                    <option class="do" value="DO+1809">
                        Dominican Republic +1809
                    </option>
                    <option class="do" value="DO+1829">
                        Dominican Republic +1829
                    </option>
                    <option class="do" value="DO+1849">
                        Dominican Republic +1849
                    </option>
                    <option class="ec" value="EC+593">
                        Ecuador +593
                    </option>
                    <option class="eg" value="EG+20">
                        Egypt +20
                    </option>
                    <option class="sv" value="SV+503">
                        El Salvador +503
                    </option>
                    <option class="ee" value="EE+372">
                        Estonia +372
                    </option>
                    <option class="et" value="ET+251">
                        Ethiopia +251
                    </option>
                    <option class="fk" value="FK+500">
                        Falkland Islands (Malvinas) +500
                    </option>
                    <option class="fo" value="FO+298">
                        Faroe Islands +298
                    </option>
                    <option class="fj" value="FJ+679">
                        Fiji +679
                    </option>
                    <option class="fi" value="FI+358">
                        Finland +358
                    </option>
                    <option class="fr" value="FR+33">
                        France +33
                    </option>
                    <option class="gf" value="GF+594">
                        French Guiana +594
                    </option>
                    <option class="pf" value="PF+689">
                        French Polynesia +689
                    </option>
                    <option class="ga" value="GA+241">
                        Gabon +241
                    </option>
                    <option class="gm" value="GM+220">
                        Gambia +220
                    </option>
                    <option class="ge" value="GE+995">
                        Georgia +995
                    </option>
                    <option class="de" value="DE+49">
                        Germany +49
                    </option>
                    <option class="gh" value="GH+233">
                        Ghana +233
                    </option>
                    <option class="gi" value="GI+350">
                        Gibraltar +350
                    </option>
                    <option class="gr" value="GR+30">
                        Greece +30
                    </option>
                    <option class="gl" value="GL+299">
                        Greenland +299
                    </option>
                    <option class="gd" value="GD+1473">
                        Grenada +1473
                    </option>
                    <option class="gp" value="GP+590">
                        Guadeloupe +590
                    </option>
                    <option class="gu" value="GU+1671">
                        Guam +1671
                    </option>
                    <option class="gt" value="GT+502">
                        Guatemala +502
                    </option>
                    <option class="gg" value="GG+44">
                        Guernsey +44
                    </option>
                    <option class="gn" value="GN+224">
                        Guinea +224
                    </option>
                    <option class="gw" value="GW+245">
                        Guinea-Bissau +245
                    </option>
                    <option class="gy" value="GY+592">
                        Guyana +592
                    </option>
                    <option class="ht" value="HT+509">
                        Haiti +509
                    </option>
                    <option class="hn" value="HN+504">
                        Honduras +504
                    </option>
                    <option class="hk" value="HK+852">
                        Hong Kong +852
                    </option>
                    <option class="hu" value="HU+36">
                        Hungary +36
                    </option>
                    <option class="is" value="IS+354">
                        Iceland +354
                    </option>
                    <option class="in" value="IN+91">
                        India +91
                    </option>
                    <option class="id" value="ID+62">
                        Indonesia +62
                    </option>
                    <option class="ir" value="IR+98">
                        Iran +98
                    </option>
                    <option class="iq" value="IQ+964">
                        Iraq +964
                    </option>
                    <option class="ie" value="IE+353">
                        Ireland +353
                    </option>
                    <option class="im" value="IM+44">
                        Isle of Man +44
                    </option>
                    <option class="il" value="IL+972">
                        Israel +972
                    </option>
                    <option class="it" value="IT+39">
                        Italy +39
                    </option>
                    <option class="jm" value="JM+1876">
                        Jamaica +1876
                    </option>
                    <option class="jp" value="JP+81">
                        Japan +81
                    </option>
                    <option class="je" value="JE+44">
                        Jersey +44
                    </option>
                    <option class="jo" value="JO+962">
                        Jordan +962
                    </option>
                    <option class="kz" value="KZ+7">
                        Kazakhstan +7
                    </option>
                    <option class="ke" value="KE+254">
                        Kenya +254
                    </option>
                    <option class="kp" value="KP+850">
                        Korea, Democratic People's Republic of +850
                    </option>
                    <option class="kr" value="KR+82">
                        Korea, Republic of +82
                    </option>
                    <option class="kw" value="KW+965">
                        Kuwait +965
                    </option>
                    <option class="kg" value="KG+996">
                        Kyrgyzstan +996
                    </option>
                    <option class="la" value="LA+856">
                        Lao People's Democratic Republic +856
                    </option>
                    <option class="lv" value="LV+371">
                        Latvia +371
                    </option>
                    <option class="lb" value="LB+961">
                        Lebanon +961
                    </option>
                    <option class="ls" value="LS+266">
                        Lesotho +266
                    </option>
                    <option class="lr" value="LR+231">
                        Liberia +231
                    </option>
                    <option class="ly" value="LY+218">
                        Libya +218
                    </option>
                    <option class="li" value="LI+423">
                        Liechtenstein +423
                    </option>
                    <option class="lt" value="LT+370">
                        Lithuania +370
                    </option>
                    <option class="lu" value="LU+352">
                        Luxembourg +352
                    </option>
                    <option class="mo" value="MO+853">
                        Macao +853
                    </option>
                    <option class="mk" value="MK+389">
                        Macedonia (FYROM) +389
                    </option>
                    <option class="mg" value="MG+261">
                        Madagascar +261
                    </option>
                    <option class="mw" value="MW+265">
                        Malawi +265
                    </option>
                    <option class="my" value="MY+60">
                        Malaysia +60
                    </option>
                    <option class="mv" value="MV+960">
                        Maldives +960
                    </option>
                    <option class="ml" value="ML+223">
                        Mali +223
                    </option>
                    <option class="mt" value="MT+356">
                        Malta +356
                    </option>
                    <option class="mq" value="MQ+596">
                        Martinique +596
                    </option>
                    <option class="mr" value="MR+222">
                        Mauritania +222
                    </option>
                    <option class="mu" value="MU+230">
                        Mauritius +230
                    </option>
                    <option class="yt" value="YT+262">
                        Mayotte +262
                    </option>
                    <option class="mx" value="MX+52">
                        Mexico +52
                    </option>
                    <option class="md" value="MD+373">
                        Moldova +373
                    </option>
                    <option class="mc" value="MC+377">
                        Monaco +377
                    </option>
                    <option class="mn" value="MN+976">
                        Mongolia +976
                    </option>
                    <option class="me" value="ME+382">
                        Montenegro +382
                    </option>
                    <option class="ms" value="MS+1664">
                        Montserrat +1664
                    </option>
                    <option class="ma" value="MA+212">
                        Morocco +212
                    </option>
                    <option class="mz" value="MZ+258">
                        Mozambique +258
                    </option>
                    <option class="mm" value="MM+95">
                        Myanmar +95
                    </option>
                    <option class="na" value="NA+264">
                        Namibia +264
                    </option>
                    <option class="nr" value="NR+674">
                        Nauru +674
                    </option>
                    <option class="np" value="NP+977">
                        Nepal +977
                    </option>
                    <option class="nl" value="NL+31">
                        Netherlands +31
                    </option>
                    <option class="an" value="AN+599">
                        Netherlands Antilles +599
                    </option>
                    <option class="nc" value="NC+687">
                        New Caledonia +687
                    </option>
                    <option class="nz" value="NZ+64">
                        New Zealand +64
                    </option>
                    <option class="ni" value="NI+505">
                        Nicaragua +505
                    </option>
                    <option class="ne" value="NE+227">
                        Niger +227
                    </option>
                    <option class="ng" value="NG+234">
                        Nigeria +234
                    </option>
                    <option class="no" value="NO+47">
                        Norway +47
                    </option>
                    <option class="om" value="OM+968">
                        Oman +968
                    </option>
                    <option class="pk" value="PK+92">
                        Pakistan +92
                    </option>
                    <option class="ps" value="PS+970">
                        Palestine +970
                    </option>
                    <option class="pa" value="PA+507">
                        Panama +507
                    </option>
                    <option class="pg" value="PG+675">
                        Papua New Guinea +675
                    </option>
                    <option class="py" value="PY+595">
                        Paraguay +595
                    </option>
                    <option class="pe" value="PE+51">
                        Peru +51
                    </option>
                    <option class="ph" value="PH+63">
                        Philippines +63
                    </option>
                    <option class="pl" value="PL+48">
                        Poland +48
                    </option>
                    <option class="pt" value="PT+351">
                        Portugal +351
                    </option>
                    <option class="pr" value="PR+1787">
                        Puerto Rico +1787
                    </option>
                    <option class="pr" value="PR+1939">
                        Puerto Rico +1939
                    </option>
                    <option class="qa" value="QA+974">
                        Qatar +974
                    </option>
                    <option class="re" value="RE+262">
                        Réunion +262
                    </option>
                    <option class="ro" value="RO+40">
                        Romania +40
                    </option>
                    <option class="ru" value="RU+7">
                        Russia +7
                    </option>
                    <option class="rw" value="RW+250">
                        Rwanda +250
                    </option>
                    <option class="kn" value="KN+1869">
                        Saint Kitts and Nevis +1869
                    </option>
                    <option class="lc" value="LC+1758">
                        Saint Lucia +1758
                    </option>
                    <option class="vc" value="VC+1784">
                        Saint Vincent and the Grenadines +1784
                    </option>
                    <option class="ws" value="WS+685">
                        Samoa +685
                    </option>
                    <option class="sm" value="SM+378">
                        San Marino +378
                    </option>
                    <option class="st" value="ST+239">
                        Sao Tome and Principe +239
                    </option>
                    <option class="sa" value="SA+966">
                        Saudi Arabia +966
                    </option>
                    <option class="sn" value="SN+221">
                        Senegal +221
                    </option>
                    <option class="rs" value="RS+381">
                        Serbia +381
                    </option>
                    <option class="sc" value="SC+248">
                        Seychelles +248
                    </option>
                    <option class="sl" value="SL+232">
                        Sierra Leone +232
                    </option>
                    <option class="sg" value="SG+65">
                        Singapore +65
                    </option>
                    <option class="sk" value="SK+421">
                        Slovakia +421
                    </option>
                    <option class="si" value="SI+386">
                        Slovenia +386
                    </option>
                    <option class="sb" value="SB+677">
                        Solomon Islands +677
                    </option>
                    <option class="so" value="SO+252">
                        Somalia +252
                    </option>
                    <option class="za" value="ZA+27">
                        South Africa +27
                    </option>
                    <option class="es" value="ES+34">
                        Spain +34
                    </option>
                    <option class="lk" value="LK+94">
                        Sri Lanka +94
                    </option>
                    <option class="sr" value="SR+597">
                        Suriname +597
                    </option>
                    <option class="sz" value="SZ+268">
                        Swaziland +268
                    </option>
                    <option class="se" value="SE+46">
                        Sweden +46
                    </option>
                    <option class="ch" value="CH+41">
                        Switzerland +41
                    </option>
                    <option class="sy" value="SY+963">
                        Syrian Arab Republic +963
                    </option>
                    <option class="tw" value="TW+886">
                        Taiwan +886
                    </option>
                    <option class="tj" value="TJ+992">
                        Tajikistan +992
                    </option>
                    <option class="tz" value="TZ+255">
                        Tanzania, United Republic of +255
                    </option>
                    <option class="th" value="TH+66">
                        Thailand +66
                    </option>
                    <option class="tl" value="TL+670">
                        Timor-Leste +670
                    </option>
                    <option class="tg" value="TG+228">
                        Togo +228
                    </option>
                    <option class="to" value="TO+676">
                        Tonga +676
                    </option>
                    <option class="tt" value="TT+1868">
                        Trinidad and Tobago +1868
                    </option>
                    <option class="tn" value="TN+216">
                        Tunisia +216
                    </option>
                    <option class="tr" value="TR+90">
                        Turkey +90
                    </option>
                    <option class="tm" value="TM+993">
                        Turkmenistan +993
                    </option>
                    <option class="tc" value="TC+1649">
                        Turks and Caicos Islands +1649
                    </option>
                    <option class="ug" value="UG+256">
                        Uganda +256
                    </option>
                    <option class="ua" value="UA+380">
                        Ukraine +380
                    </option>
                    <option class="ae" value="AE+971">
                        United Arab Emirates +971
                    </option>
                    <option class="gb" value="GB+44">
                        United Kingdom +44
                    </option>
                    <option class="us" value="US+1">
                        United States +1
                    </option>
                    <option class="uy" value="UY+598">
                        Uruguay +598
                    </option>
                    <option class="uz" value="UZ+998">
                        Uzbekistan +998
                    </option>
                    <option class="vu" value="VU+678">
                        Vanuatu +678
                    </option>
                    <option class="ve" value="VE+58">
                        Venezuela +58
                    </option>
                    <option class="vn" value="VN+84">
                        Vietnam +84
                    </option>
                    <option class="vg" value="VG+1284">
                        Virgin Islands, British +1284
                    </option>
                    <option class="ye" value="YE+967">
                        Yemen +967
                    </option>
                    <option class="zm" value="ZM+260">
                        Zambia +260
                    </option>
                    <option class="zw" value="ZW+263">
                        Zimbabwe +263
                    </option>
                </select>
                <input type="tel" placeholder="55-5555555" class="checkout__tel">
            </div>
            <div>
                <input type="text" placeholder="Company Name" class="checkout__companyName">
                <input type="text" placeholder="Company Website" class="checkout__companySite">
            </div>
            <div>
                <select class="select2-active" id="contact_employees">
                    <option value=""></option>
                    <option>1-10</option>
                    <option>11-50</option>
                    <option>51-200</option>
                    <option>201-500</option>
                    <option>501-1000</option>
                    <option>1001-5000</option>
                    <option>5001+</option>
                </select>
                <select class="select2-active" id="contact_industry">
                    <option value=""></option>
                    <option>Automotive &amp; Transportation</option>
                    <option>AdTech and AdNetwork</option>
                    <option>Agency</option>
                    <option>B2B Software</option>
                    <option>B2C Internet Services</option>
                    <option>Banking &amp; Insurance</option>
                    <option>Consulting &amp; Market Research</option>
                    <option>CPG, Food &amp; Beverages</option>
                    <option>Education</option>
                    <option>Financial Services</option>
                    <option>Gambling &amp; Gaming</option>
                    <option>Government, Public Sector &amp; Non Profit</option>
                    <option>Hedge Funds and Asset Management</option>
                    <option>Investment Banking</option>
                    <option>Pharmaceutical</option>
                    <option>Publishers &amp; Media</option>
                    <option>Real Estate</option>
                    <option>Recruitment &amp; Jobs</option>
                    <option>Retail &amp; eCommerce</option>
                    <option>Telecom &amp; Hardware</option>
                    <option>Travel &amp; Hospitality</option>
                    <option>Venture Capital &amp; Private Equity</option>
                    <option>Other</option>
                </select>
            </div>
            <div>
                <select class="select2-active" id="contact_department">
                    <option value=""></option>
                    <option>BI / Analytics</option>
                    <option>Corporate Development / Mergers and Acquisitions</option>
                    <option>eCommerce</option>
                    <option>Marketing: Affiliate / Media Buying</option>
                    <option>Marketing: Management / General</option>
                    <option>Marketing: SEO / Content</option>
                    <option>Marketing: PPC / Display</option>
                    <option>Marketing: PR / Comms / Social Media</option>
                    <option>Product Management</option>
                    <option>Sales / Business Development</option>
                    <option>Account Management / Customer Success</option>
                    <option>Sales Operations</option>
                    <option>Strategy / Market Research</option>
                    <option>Investment</option>
                    <option>Management (C-Level)</option>
                    <option>Other</option>
                </select>
                <select class="select2-active" id="contact_job">
                    <option value=""></option>
                    <option>Analyst</option>
                    <option>Consultant</option>
                    <option>Manager</option>
                    <option>Director</option>
                    <option>Vice President (VP)</option>
                    <option>C-Level</option>
                    <option>CEO/President</option>
                    <option>Professor/Student</option>
                    <option>Other</option>
                </select>
            </div>
            <div class="validation validation_message">
                <textarea name="message" class="checkout__message" placeholder="What would you like to discuss?"
                          maxlength="1000" required></textarea>
                <p>ERROR: The Message field is empty.</p>
            </div>
            <button type="submit" class="btn"><span>Send</span><img
                        src="<?= $template_directory_uri; ?>/out/img_design/loading.svg" alt="loading" class="loading">
            </button>
        </form>
    </div>
</div>
<div class="paypal_redirect">
    <p class="title">Thank you for registration!</p>
    <p>We are redirecting you to PayPal</p>
    <div class="spinWrap">
        <p class="spinnerImage"></p>
        <p class="loader"></p>
    </div>
</div>
<?php wp_footer();
if (!is_page(2792)) {
    echo '<script
  src="https://code.jquery.com/jquery-2.2.4.min.js"
  integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44="
  crossorigin="anonymous"></script>';
}
?>
<style>
    ul.heateor_ss_follow_ul li, ul.the_champ_sharing_ul li {
        margin: 5px 0 !important
    }

    .theChampPushIn {
        display: none !important
    }
</style>
<script src="https://rawgit.com/kimmobrunfeldt/progressbar.js/1.0.0/dist/progressbar.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
        integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
        crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/bootstrap-select.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.4.1/jquery.cookie.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
<script src="<?= $template_directory_uri; ?>/out/js/prism.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-slider/10.3.1/bootstrap-slider.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script>
<script src="<?= $template_directory_uri; ?>/out/js/scripts-design-min.js?<?php echo mt_rand(100000, 999999); ?>"></script>
<script src="<?= $template_directory_uri; ?>/javascripts/index.js?<?php echo mt_rand(100000, 999999); ?>"></script>

<?php
if (!is_user_logged_in()) {
    ?>
    <script>
        $('.pricing__level').submit(function (e) {
            e.preventDefault();
            let level__id = $(this).find('.pmpro_btn-submit-checkout').attr('name');
            $('.signin__btn').click();
            $('.tml-rememberme-submit-wrap input[name="redirect_to"]').val("/plans/?plan=" + level__id);
            $('.registr-tml-submit-wrap input[name="redirect_to"]').val("/plans/?registration=true&plan=" + level__id);
        })
    </script>
<?php }
?>
</body>
<!-- End Google Tag Manager (noscript) -->
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-N2SCB2J"
            height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->
</html>