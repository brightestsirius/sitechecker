<?php
$template_directory_uri = get_template_directory_uri();
$currentLang = qtrans_getLanguage();
$meta = get_post_meta_all(get_option('page_on_front'));
$footerPosts = get_post_meta_by_urls(['about-us', 'contact-us', 'privacy-policy', 'terms-and-conditions', 'technical-seo-audit'], 'footer_title');
?>
<footer>
    <div class="footer">
        <div class="wrapper">
            <div class="footer_menu">
                <div class="footer_menu-links">
                    <div class="footer_link">
                        <p><?=t($meta['footer.company']);?></p>
                        <ul>
                            <?php
                            foreach(['about-us', 'contact-us'] as $slug) {
                                $seo = url_to_postid("/". $slug ."/");
                                wp_cache_delete($seo, 'posts');
                                $mypost = WP_Post::get_instance($seo);
                                $lang = qtrans_use($currentLang, $mypost->post_title, false);
                                if($lang == ''){
                                    $enLang = '/';
                                }
                                else if($currentLang == 'en'){
                                    $enLang = '/';
                                } else{
                                    $enLang = "/".$currentLang."/";
                                }

                                if($lang == ''){
                                    $en_class = 'en_class';
                                } else{
                                    $en_class = 'class';
                                }

                                echo '<li><a class="'.$en_class.'" href="'.$enLang.''.$slug.'/">'. get_post_meta($seo, 'footer_title', true ) .'</a></li>';
                            }
                            ?>
                            <li>
                                <a href="https://www.facebook.com/sitecheckerpro/" target="_blank" rel="nofollow"><?php echo t($meta['footer.facebook']);?></a>
                            </li>
                            <li><a href="/affiliate-program/"><?=t($meta['affiliate_program']);?></a></li>
                            <?php
                            foreach(['privacy-policy', 'terms-and-conditions'] as $slug) {
                                $seo = url_to_postid("/". $slug ."/");
                                wp_cache_delete($seo, 'posts');
                                $mypost = WP_Post::get_instance($seo);
                                $lang = qtrans_use($currentLang, $mypost->post_title, false);
                                if($lang == ''){
                                    $enLang = '/';
                                }
                                else if($currentLang == 'en'){
                                    $enLang = '/';
                                } else{
                                    $enLang = "/".$currentLang."/";
                                }

                                if($lang == ''){
                                    $en_class = 'en_class';
                                } else{
                                    $en_class = 'class';
                                }

                                echo '<li><a class="'.$en_class.'" href="'.$enLang.''.$slug.'/">'. get_post_meta($seo, 'footer_title', true ) .'</a></li>';
                            }
                            ?>
                        </ul>
                    </div>
                    <div class="footer_link">
                        <p><?=t($meta['footer.services']);?></p>
                        <ul>
                            <?php
                            if ( is_user_logged_in() ) {
                                $crawl_link = 'crawl-report';
                            } else{
                                $crawl_link = 'website-crawler';
                            }
                            foreach([$crawl_link] as $slug) {
                                $seo = url_to_postid("/". $slug ."/");
                                wp_cache_delete($seo, 'posts');
                                $mypost = WP_Post::get_instance($seo);
                                $lang = qtrans_use($currentLang, $mypost->post_title, false);
                                if($lang == ''){
                                    $enLang = '/';
                                }
                                else if($currentLang == 'en'){
                                    $enLang = '/';
                                } else{
                                    $enLang = "/".$currentLang."/";
                                }

                                if($lang == ''){
                                    $en_class = 'en_class';
                                } else{
                                    $en_class = 'class';
                                }
                                echo '<li><a class="'.$en_class.'" href="'.$enLang.''.$slug.'/">'. get_post_meta($seo, 'footer_title', true ) .'</a></li>';
                            }
                            ?>
                            <li><a href="/website-monitoring/"><?=t($meta['website_monitoring']);?></a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="footer_about">
                <p class="footer_about-title"><?=t($meta['footer.products']);?></p>
                <div class="footer_about-products">
                    <div class="footer_product">
                        <a href="https://kparser.com/" target="_blank">Kparser<span>.com</span></a>
                        <p><?=t($meta['footer.kparser']);?></p>
                    </div>
                    <div class="footer_product">
                        <a href="https://copywritely.com/" target="_blank">Copywritely<span>.com</span></a>
                        <p><?=t($meta['footer.linkstrategy']);?></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer footer_bottom">
        <div class="wrapper">
            <div class="footer_menu">
                <div class="footer_menu-dev">
                    <p><?=t($meta['footer.developedby']);?>: <a href="http://boosta.biz/" target="_blank"><img src="<?=$template_directory_uri;?>/out/img/boosta.svg" alt="boosta" title="boosta"></a></p>
                </div>
            </div>
            <div class="footer_about">
                <div class="footer_about-copyright">
                    <p>© <?=date('Y');?> Sitechecker. All rights reserved.</p>
                </div>
            </div>
        </div>
    </div>
</footer>

<div class="footer__cookie">
    <div class="wrapper">
        <img src="<?=$template_directory_uri;?>/out/img/cookies.svg" alt="cookies" title="cookies"><p><?=t($meta['cookies']);?></p>
        <img src="<?=$template_directory_uri;?>/out/img/close_btn.svg" alt="close" title="close" class="footer__cookie-close">
    </div>
</div>


<div class="header__popup">
    <div class="login__popup-block">
        <div class="login__popup-close">
            <img src="<?=$template_directory_uri;?>/out/img/popup__close.svg" alt="close" title="close" class="img-close">
        </div>
        <div class="login__popup-title"></div>
        <div class="login__popup-description"></div>
        <div class="login__popup-error"></div>
        <div class="login__popup-form">
            <div class="login__popup-content login__popup-signin">
                <?=do_shortcode('[theme-my-login instance=1 default_action=login show_title=0 show_reg_link=0]');?>
            </div>
            <div class="login__popup-content login__popup-signup">
                <?=do_shortcode('[theme-my-login instance=2 default_action=register show_title=0 show_pass_link=0 show_log_link=0]');?>
            </div>
            <div class="login__popup-content login__popup-lostpass">
                <?=do_shortcode('[theme-my-login instance=3 default_action=lostpassword show_title=0 show_reg_link=0 show_log_link=0]');?>
            </div>
        </div>
    </div>
</div>

<div class="checkemail">
    <div class="checkemail-block">
        <div class="login_popup-close">
            <img src="<?=$template_directory_uri;?>/out/img/img-close-white.svg" alt="close" title="close" class="img-close">
        </div>
        <img src="<?=$template_directory_uri;?>/out/img/letter.svg" alt="sent-mail" title="sent-mail">
        <p class="checkemail-sent"><?=t($meta['change_pass_sent']);?></p>
        <p class="checkemail-complete"><?=t($meta['change_pass_complete']);?></p>
    </div>
</div>

<?php $pageID = get_the_ID(); if($pageID == 709){ ?>
    <div class="limit_popup">
        <div class="limit_popup-block">
            <div class="limit__description">
                <div>
                    <?=t($meta['limit__description']);?></p>
                </div>
            </div>
            <div class="limit__login">
                <div class="login__popup-block">
                    <div class="limit__about">
                        <p class="limitik login__popup-title"><?=t($meta['limit-popup-title']);?></p>
                    </div>
                    <div class="login__popup-error"></div>
                    <div class="login__popup-form">
                        <div class="login__popup-content login__popup-signin">
                            <?=do_shortcode('[theme-my-login instance=1 default_action=login show_title=0 show_reg_link=0]');?>
                        </div>
                        <div class="login__popup-content login__popup-signup">
                            <?=do_shortcode('[theme-my-login instance=2 default_action=register show_title=0 show_pass_link=0 show_log_link=0]');?>
                        </div>
                        <div class="login__popup-content login__popup-lostpass">
                            <?=do_shortcode('[theme-my-login instance=3 default_action=lostpassword show_title=0 show_reg_link=0 show_log_link=0]');?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php }?>

<?php wp_footer(); ?>

<script src="<?=$template_directory_uri;?>/out/js/jquery-3.2.1.min.js"></script>
<script src="<?=$template_directory_uri;?>/out/js/owl.carousel.min-min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/bootstrap-select.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.4.1/jquery.cookie.js"></script>
<script src="<?=$template_directory_uri;?>/out/js/scripts-min.js?<?php echo mt_rand(100000,999999); ?>"></script>

<?php
if(get_the_ID() == 709){
    $signUpTitle = t($meta['limit.popup.signup.title']);
    $signInTitle = t($meta['limit.popup.signin.title']);
} else{
    $signUpTitle = t($meta['signup__title']);
    $signInTitle = t($meta['signin__title']);
}
if(is_user_logged_in()){
    $USER_STATUS = 'true';
} else{
    $USER_STATUS = 'false';
}
?>
<script>
    $('#toNewDesign').click(function () {
        document.cookie="design_new=1;path=/";
        location.reload();
    });
    let socialCrawler = localStorage.getItem('socialCrawler'),
        socialCrawlerRedirect = localStorage.getItem('socialCrawlerRedirect');
    if(socialCrawler == 'socialCrawler'){
        socialCrawler = localStorage.setItem('socialCrawler', 'false');
        $('.header, .footer, main').css('display','none');
        $('.paypal_redirect').css('display','flex');
        window.location.href = socialCrawlerRedirect;
    }
    let socialPaypalLogin = localStorage.getItem('socialPaypalLogin'),
        socialPaypalRegistration = localStorage.getItem('socialPaypalRegistration'),
        socialPlan = localStorage.getItem('socialPlan');
    const user__status = '<?php echo $USER_STATUS; ?>';
    if(user__status === 'false'){
        $('.pmpro_btn-submit-checkout').click(function () {
            let selectedPlan = $(this).attr('name');
            $('.signup').click();
            $('.theChampLoginSvg').click(function () {
                socialPlan = localStorage.setItem('socialPlan', selectedPlan);
                if($(this).parent().attr('id') == 'signInGoogle' || $(this).parent().attr('id') == 'signInFacebook'){
                    if(socialPaypalLogin !== 'loginPaypal'){
                        socialPaypalLogin = localStorage.setItem('socialPaypalLogin','loginPaypal');
                    }
                } else if($(this).parent().attr('id') == 'signUpGoogle' || $(this).parent().attr('id') == 'signUpFacebook'){
                    if(socialPaypalRegistration !== 'signupPaypal'){
                        socialPaypalRegistration = localStorage.setItem('socialPaypalRegistration','signupPaypal');
                    }
                }
            });
            $('.tml-submit-wrap').find('input.login_redirection[name="redirect_to"]').val('/plans/?plan='+selectedPlan);
            $('.tml-submit-wrap').find('input.registr_redirection[name="redirect_to"]').val('/plans/?registration=true&plan='+selectedPlan);
            return false;
        })
    }

    //signup btn in header popup
    $( ".popup__register-btn" ).click(function(){

        $('.login_popup-error').css('display', 'none');

        let user_email = $('.user_email-registr').val(),
            user_pass = $('.pass__register-btn').val();

        if(user_email == ''){
            $('.login__popup-error').fadeIn().html(email__empty);
            return false;
        } else if (user_email.indexOf(' ') >= 0){
            $('.login__popup-error').fadeIn().html(email__spaces);
            return false;
        } else if(user_email.indexOf('@') < 0 || user_email.indexOf('.') < 0 || user_email.substring(user_email.length-1) == "@" || user_email.substring(user_email.length-1) == "." || user_email.charAt(0) == "@" || user_email.charAt(0) == "."){
            $('.login__popup-error').fadeIn().html(email__incorrect);
            return false;
        } else if(user_pass == ''){
            $('.login__popup-error').fadeIn().html(pass__empty);
            return false;
        } else if (user_pass.indexOf(' ') >= 0){
            $('.login__popup-error').fadeIn().html(pass__spaces);
            return false;
        } else if(user_pass !== ''){
            let user_exist = false;
            $.ajax({
                url : '/wp-admin/admin-ajax.php',
                async: false,
                type: "POST",
                data: {'action': 'check_username', email: user_email, pass: user_pass},
                dataType: "json",
                success: function(response) {
                    console.log(response);
                    if(response == 1 || response == 2){
                        $('.login__popup-error').fadeIn().html(email__exist);
                        user_exist = false;
                    } else{
                        $('.popup__register-btn').css('display','none');
                        $('.tml-submit-wrap').prepend('<img class="popup_loader" src="/wp-content/plugins/super-socializer/images/social_loader.svg">');
                        user_exist = true;
                    }
                }
            });
            localStorage.setItem('RegistrationPopup', 'false');
            return user_exist;
        }

    });

    //login btn in header popup
    $('.popup__login-btn').click(function(){

        $('.login_popup-error').css('display', 'none');

        let user_email = $('.user_login').val(),
            user_pass = $('.user_pass-login').val();

        if(user_email == ''){
            $('.login__popup-error').fadeIn().html(email__empty);
            return false;
        } else if (user_email.indexOf(' ') >= 0){
            $('.login__popup-error').fadeIn().html(email__spaces);
            return false;
        } else if(user_email.indexOf('@') < 0 || user_email.indexOf('.') < 0){
            $('.login__popup-error').fadeIn().html(email__incorrect);
            return false;
        } else if(user_pass == ''){
            $('.login__popup-error').fadeIn().html(pass__empty);
            return false;
        } else if (user_pass.indexOf(' ') >= 0){
            $('.login__popup-error').fadeIn().html(pass__spaces);
            return false;
        } else if(user_pass !== '' && user_email !== ''){
            let user_exist = true;
            localStorage.setItem('visitorExistingCustomer', 'Yes');
            $.ajax({
                url : '/wp-admin/admin-ajax.php',
                async: false,
                type: "POST",
                data: {'action': 'check_username', email: user_email, pass: user_pass},
                dataType: "json",
                success: function(response) {
                    if(response == 1){
                        $('.login__popup-error').fadeIn().html(pass__notexist);
                        user_exist = false;
                    } else if(response == 0){
                        $('.login__popup-error').fadeIn().html(email__notexist);
                        user_exist = false;
                    } else{
                        $('.popup__login-btn').css('display','none');
                        $('.tml-submit-wrap').prepend('<img class="popup_loader" src="https://sitechecker.pro/wp-content/plugins/super-socializer/images/social_loader.svg">');
                    }
                }
            });
            return user_exist;
        }

    });

    //lostpass btn in header popup
    $('.popup__lostpass-btn').on('click', function(e) {
        $('.login_popup-error').css('display', 'none');

        if($('.user_login-lostpass').val() == '' || $('.user_login-lostpass').val().indexOf(' ') >= 0){
            $('.login__popup-error').fadeIn().html(email__empty);
            return false;
        } else if($('.user_login-lostpass').val() !== ''){
            let user_exist = true;
            if(!$(this).hasClass('lostAccount')){
                $.ajax({
                    url : '/wp-admin/admin-ajax.php',
                    async: false,
                    type: "POST",
                    data: {'action': 'check_username', email: $('.user_login-lostpass').val(), pass: $('.user_login-lostpass').val()},
                    dataType: "json",
                    success: function(response) {
                        if(response == 0){
                            $('.login__popup-error').fadeIn().html(email__notexist);
                            user_exist = false;
                        }
                    }
                });
            }
            localStorage.setItem('CheckemailPopup', 'false');
            return user_exist;
        }
    });

    const signin__title = '<?php echo $signInTitle; ?>';
    const signup__title = '<?php echo $signUpTitle; ?>';
    const lostpass__title = '<?=t($meta['lostpass__title']);?>';
    const lostpass__description = '<?=t($meta['lostpass_popup_description']);?>';

    //header popup errors
    const email__empty = '<?=t($meta['error_emailempty']);?>';
    const email__spaces = '<?=t($meta['error_emailspaces']);?>';
    const email__incorrect = '<?=t($meta['error_emailincorrect']);?>';
    const email__notexist = '<?=t($meta['error_emailnotexist']);?>';
    const email__exist = '<?=t($meta['error_emailexist']);?>';

    const pass__notexist = '<?=t($meta['error_passnotexist']);?>';
    const pass__empty = '<?=t($meta['error_passempty']);?>';
    const pass__spaces = '<?=t($meta['error_passspaces']);?>';
    const pass_length = '<?=t($meta['error_passlength']);?>';

    //header popup
    const login__title = {
        signin: signin__title,
        signup: signup__title,
        lostpass: lostpass__title,
        lostpass_seoreport: lostpass__title
    };
    $.each(login__title, function( key, value ) {
        $('.'+key).click(function () {
            $('body').addClass('popup__open');
            $('.login__popup-content, .login__popup-error, .login__popup-description').css('display','none');
            disableScroll();
            $('.login__popup-title').text(value);
            switch(key) {
                case 'signin':
                    $('.login__popup-signin').css('display','block').find('.theChampFacebookLoginSvg').parent().attr('id','signInFacebook');
                    $('.login__popup-signin').find('.theChampGoogleLoginSvg').parent().attr('id','signInGoogle');
                    break;
                case 'signup':
                    $('.login__popup-signup').css('display','block').find('.theChampFacebookLoginSvg').parent().attr('id','signUpFacebook');
                    $('.login__popup-signup').find('.theChampGoogleLoginSvg').parent().attr('id','signUpGoogle');
                    break;
                case 'lostpass':
                    $('.login__popup-lostpass').css('display','block');
                    $('.login__popup-description').css('display','block').text(lostpass__description);
                    break;
                case 'lostpass_seoreport':
                    $('.login__popup-lostpass').css('display','block');
                    $('.login__popup-description').css('display','block').text(lostpass__description);
                    break;
            }
            $('#signInGoogle .theChampGoogleLoginSvg').text('Sign in with Google');
            $('#signInFacebook .theChampFacebookLoginSvg').text('Sign in with Facebook');
            $('#signUpGoogle .theChampGoogleLoginSvg').text('Sign up with Google');
            $('#signUpFacebook .theChampFacebookLoginSvg').text('Sign up with Facebook');
            $('.header__popup').css('display', 'flex');
            $('.login__popup-block').fadeIn().css('display', 'flex');

            $('.header__popup .header__registr-checkout').click(function () {
                $('.signup').click();

            });
            $('.header__popup .header__login-checkout').click(function () {
                $('.signin').click();
            });

            $('.header__container-wrap input').focusin(function(){
                $(this).css('padding-top','7px');
                $(this).parent('.header__container-wrap').find('label').css({
                    'transform': 'translate3d(0, -14px, 0)',
                    'top': '5px',
                    'color': '#495c6a',
                    'z-index': '10',
                })
            }).focusout(function(){
                $(this).css('padding-top','0');
                $(this).parent('.header__container-wrap').find('label').css({
                    'transform': 'translate3d(0, 0, 0)',
                    'top': '0',
                    'color': 'transparent',
                    'z-index': '-1',
                })
            });
        });
    });

    $('.header__popup .header__registr-checkout').click(function () {
        $('.signup').click();
    });
    $('.header__popup .header__login-checkout').click(function () {
        $('.signin').click();
    });

    $('#sitechecker_reg').click(function () {
        let apiPath = "/wp-admin/admin-ajax.php",
            socialCrawler = localStorage.getItem('socialCrawler'),
            socialCrawlerRedirect = localStorage.getItem('socialCrawlerRedirect'),
            registr = $(this);
        $('.home_first-search').removeClass('no_valid');
        let url = $('#sitechecker_input').val().replace(/\s/g, "");
        registr.find('span').text('Validating');
        registr.find('.fa').css('display', 'inline-block');
        $.ajax({
            url: apiPath,
            type: "POST",
            dataType: "JSON",
            async: true,
            data: {
                action: 'crawler_validate_domain_request',
                url: url
            },
            success: function (data) {
                if(data.status === false){
                    $('.home_first-search').addClass('no_valid');
                    registr.find('span').text('Start');
                    registr.find('.fa').css('display', 'none');
                } else{
                    let crawlUrl = '/crawl-report?url='+data.valid+'&create';
                    if(registr.attr('name') === 'monitoringEnable'){
                        crawlUrl = '/crawl-report?url='+data.valid+'&create&monitoring_enabled=1';
                    }
                    $('.theChampLoginSvg').click(function () {
                        socialCrawler = localStorage.setItem('socialCrawler', 'socialCrawler');
                        socialCrawlerRedirect = localStorage.setItem('socialCrawlerRedirect', crawlUrl);
                    });
                    localStorage.setItem('create', 'Shown');
                    $('#sitechecker_input').val(data.valid);
                    registr.find('span').text('Validate');
                    registr.find('.fa').css('display', 'none');
                    $('.signup').click();
                    $('.tml-submit-wrap input[name="redirect_to"]').val(crawlUrl);
                }
            }
        });
    });
</script>
</body>
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-TLCC7S4"
                  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-N2SCB2J"
                  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
</html>