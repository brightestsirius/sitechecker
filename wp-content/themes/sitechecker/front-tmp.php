<?php
if($post->ID === 3375 || $post->ID === 3386){
    $meta = get_post_meta_all(get_option('page_on_front'));
} else{
    $meta = get_post_meta_all($post->ID);
}
$template_directory_uri = get_template_directory_uri();
$front_second = url_to_postid( '/website-crawler/' );
$front_third = url_to_postid( '/website-monitoring/' );
$front_fourth = url_to_postid( '/affiliate-program/' );
?>
<main class="front">
    <div class="home_first">
        <div class="wrapper">
            <h1><?=t($meta['main.h1']);?></h1>
            <p><?=t($meta['main.h2']);?></p>
            <?php
            $monitoring = 'crawl';
            if(is_page($front_second)){
                if(is_user_logged_in()){
                    $page = 'sitechecker_crawl';
                } else{
                    $page = 'sitechecker_reg';
                }
            } else if(is_page($front_third)){
                if(is_user_logged_in()){
                    $page = 'sitechecker_crawl';
                } else{
                    $page = 'sitechecker_reg';
                }
                $monitoring = 'monitoringEnable';
            }
            else{
                $page = 'sitechecker_check-landing';
            } ?>
            <?php if(is_page($front_fourth)){ ?>
                <div id="mc_embed_signup">
                    <form action="https://sitechecker.us13.list-manage.com/subscribe/post?u=b33f62afc5e797a4e431a7266&amp;id=bc2bf182e6" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
                        <div id="mc_embed_signup_scroll">
                            <input type="email" value="" name="EMAIL" class="email" id="mce-EMAIL" placeholder="<?=t($meta['main.input']);?>" required>
                            <input type="text" name="b_b33f62afc5e797a4e431a7266_bc2bf182e6" tabindex="-1" value="" style="display: none;">
                            <input type="submit" value="<?=t($meta['main.button']);?>" name="subscribe" id="mc-embedded-subscribe" class="button">
                        </div>
                    </form>
                </div>
            <?php } else { ?>
                <div class="home_first-search">
                    <input id="sitechecker_input" type="text" placeholder="<?=t($meta['main.input']);?>" class="<?php echo $page; ?>">
                    <button id="<?php echo $page; ?>" name="<?php echo $monitoring ?>"><span><?=t($meta['main.button']);?></span><i class="fa fa-spinner fa-spin"></i></button>
                </div>
            <?php } ?>
        </div>
        <?php if(!is_page($front_fourth)){ ?>
            <div class="wrapper home_first-down">
                <a class="scrollto" id=".home_about"><?=t($meta['main.arrow-down']);?> <svg xmlns="http://www.w3.org/2000/svg" width="10" height="7" viewBox="0 0 10 7">
                        <g fill="#495C6A" fill-rule="evenodd">
                            <path d="M5.104 7l1.088-1.142L1.296.718.21 1.86z"/>
                            <path d="M10 1.857L8.912.715l-4.895 5.14 1.088 1.143z"/>
                        </g>
                    </svg>
                </a>
            </div>
        <?php } ?>
    </div>
    <?php if(is_page($front_third) || is_page($front_fourth)){ ?>
    <?php }else{ ?>
        <div class="home_about">
            <div class="wrapper owl-carousel">
                <div class="home_about-block item">
                    <img src="<?=$template_directory_uri;?>/out/img/about_1.svg" alt="<?=t($meta['features.1.header']);?>" title="<?=t($meta['features.1.header']);?>">
                    <h3><?=t($meta['features.1.header']);?></h3>
                    <span><?=t($meta['features.1.content']);?></span>
                </div>
                <div class="home_about-block item">
                    <img src="<?=$template_directory_uri;?>/out/img/about_2.svg" alt="<?=t($meta['features.2.header']);?>" title="<?=t($meta['features.2.header']);?>">
                    <h3><?=t($meta['features.2.header']);?></h3>
                    <span><?=t($meta['features.2.content']);?></span>
                </div>
                <div class="home_about-block item">
                    <img src="<?=$template_directory_uri;?>/out/img/about_3.svg" alt="<?=t($meta['features.3.header']);?>" title="<?=t($meta['features.3.header']);?>">
                    <h3><?=t($meta['features.3.header']);?></h3>
                    <span><?=t($meta['features.3.content']);?></span>
                </div>
                <div class="home_about-block item">
                    <img src="<?=$template_directory_uri;?>/out/img/about_4.svg" alt="<?=t($meta['features.4.header']);?>" title="<?=t($meta['features.4.header']);?>">
                    <h3><?=t($meta['features.4.header']);?></h3>
                    <span><?=t($meta['features.4.content']);?></span>
                </div>
            </div>
        </div>
    <?php } ?>
    <?php if(!is_page($front_fourth)){ ?>
        <div class="home_about" style="padding: 0"></div>
        <div class="home_description">
            <div class="wrapper">
                <div class="home_description-text">
                    <h2><?=t($meta['seo.1.header']);?></h2>
                    <p><?=t($meta['seo.1.content']);?></p>
                </div>
                <div class="home_description-img">
                    <img src="<?=$template_directory_uri;?>/out/img/img-border.svg" alt="img-border" title="img-border" class="img-border">
                    <div>
                        <?php if (is_page($front_second)){$image = $template_directory_uri.'/out/img/website_crawler-1.png';} else if (is_page($front_third)){$image = 'https://sitechecker.pro/wp-content/uploads/2018/07/monitoring_1.png';} else {$image = $template_directory_uri.'/out/img/seo_first.png';} ?>
                        <img src="<?php echo $image; ?>" alt="<?=t($meta['seo.1.alt']);?>" title="<?=t($meta['seo.1.title']);?>" class="description-img">
                        <img src="<?=$template_directory_uri;?>/out/img/img-close.svg" alt="img-close" title="img-close" class="img-close">
                    </div>
                </div>
            </div>
        </div>
        <div class="home_description">
            <div class="wrapper">
                <div class="home_description-img">
                    <img src="<?=$template_directory_uri;?>/out/img/img-border.svg" alt="img-border" title="img-border" class="img-border">
                    <div>
                        <?php if (is_page($front_second)){$image = $template_directory_uri.'/out/img/website_crawler-2.png';}  else if (is_page($front_third)){$image = 'https://sitechecker.pro/wp-content/uploads/2018/07/monitoring-2.png';} else{$image = $template_directory_uri.'/out/img/seo_second.png';} ?>
                        <img src="<?php echo $image; ?>" alt="<?=t($meta['seo.2.alt']);?>" title="<?=t($meta['seo.2.title']);?>" class="description-img">
                        <img src="<?=$template_directory_uri;?>/out/img/img-close.svg" alt="img-close" title="img-close" class="img-close">
                    </div>
                </div>
                <div class="home_description-text">
                    <h2><?=t($meta['seo.2.header']);?></h2>
                    <p><?=t($meta['seo.2.content']);?></p>
                </div>
            </div>
        </div>
        <div class="home_description">
            <div class="wrapper">
                <div class="home_description-text">
                    <h2><?=t($meta['seo.3.header']);?></h2>
                    <p><?=t($meta['seo.3.content']);?></p>
                </div>
                <div class="home_description-img">
                    <img src="<?=$template_directory_uri;?>/out/img/img-border.svg" alt="img-border" title="img-border" class="img-border">
                    <div>
                        <?php if (is_page($front_second)){$image = $template_directory_uri.'/out/img/website_crawler-3.png';}  else if (is_page($front_third)){$image = 'https://sitechecker.pro/wp-content/uploads/2018/07/monitoring-3.png';} else{$image = $template_directory_uri.'/out/img/seo_third.png';} ?>
                        <img src="<?php echo $image; ?>" alt="<?=t($meta['seo.3.alt']);?>" title="<?=t($meta['seo.3.title']);?>" class="description-img">
                        <img src="<?=$template_directory_uri;?>/out/img/img-close.svg" alt="img-close" title="img-close" class="img-close">
                    </div>
                </div>
            </div>
        </div>
        <div class="home_description">
            <div class="wrapper">
                <div class="home_description-img">
                    <img src="<?=$template_directory_uri;?>/out/img/img-border.svg" alt="img-border" title="img-border" class="img-border">
                    <div>
                        <?php if (is_page($front_second)){$image = $template_directory_uri.'/out/img/website_crawler-4.png';}  else if (is_page($front_third)){$image = 'https://sitechecker.pro/wp-content/uploads/2018/07/monitoring-4.png';} else{$image = $template_directory_uri.'/out/img/seo_fourth.png';} ?>
                        <img src="<?php echo $image; ?>" alt="<?=t($meta['seo.4.alt']);?>" title="<?=t($meta['seo.4.title']);?>" class="description-img">
                        <img src="<?=$template_directory_uri;?>/out/img/img-close.svg" alt="img-close" title="img-close" class="img-close">
                    </div>
                </div>
                <div class="home_description-text">
                    <h2><?=t($meta['seo.4.header']);?></h2>
                    <p><?=t($meta['seo.4.content']);?></p>
                </div>
            </div>
        </div>
        <div class="home_description">
            <div class="wrapper">
                <div class="home_description-text">
                    <h2><?=t($meta['seo.5.header']);?></h2>
                    <p><?=t($meta['seo.5.content']);?></p>
                </div>
                <div class="home_description-img">
                    <img src="<?=$template_directory_uri;?>/out/img/img-border.svg" alt="img-border" title="img-border" class="img-border">
                    <div>
                        <?php if (is_page($front_second)){$image = $template_directory_uri.'/out/img/website_crawler-5.png';}  else if (is_page($front_third)){$image = 'https://sitechecker.pro/wp-content/uploads/2018/07/monitoring-5.png';} else{$image = $template_directory_uri.'/out/img/seo_five.png';} ?>
                        <img src="<?php echo $image; ?>" alt="<?=t($meta['seo.5.alt']);?>" title="<?=t($meta['seo.5.title']);?>" class="description-img">
                        <img src="<?=$template_directory_uri;?>/out/img/img-close.svg" alt="img-close" title="img-close" class="img-close">
                    </div>
                </div>
            </div>
        </div>
    <?php } ?>
    <?php if(!is_page($front_third) && !is_page($front_fourth)){ ?>
        <div class="home_faq">
            <div class="wrapper">
                <p class="home_block-title">FAQ</p>
                <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingOne">
                            <div class="panel-title">
                                <h4 data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                                    <?=t($meta['faq.1.header']);?>
                                </h4>
                            </div>
                        </div>
                        <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                            <div class="panel-body">
                                <?=t($meta['faq.1.content']);?>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingTwo">
                            <div class="panel-title">
                                <h4 class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                    <?=t($meta['faq.2.header']);?>
                                </h4>
                            </div>
                        </div>
                        <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                            <div class="panel-body">
                                <?=t($meta['faq.2.content']);?>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingThree">
                            <div class="panel-title">
                                <h4 class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                    <?=t($meta['faq.3.header']);?>
                                </h4>
                            </div>
                        </div>
                        <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                            <div class="panel-body">
                                <?=t($meta['faq.3.content']);?>
                            </div>
                        </div>
                    </div>
                    <?php
                    if(t($meta['faq.4.header']) !== ''){
                        ?>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingFour">
                                <div class="panel-title">
                                    <h4 class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                        <?=t($meta['faq.4.header']);?>
                                    </h4>
                                </div>
                            </div>
                            <div id="collapseFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFour">
                                <div class="panel-body">
                                    <?=t($meta['faq.4.content']);?>
                                </div>
                            </div>
                        </div>
                        <?php
                    }
                    ?>
                    <?php
                    if(t($meta['faq.5.header']) !== ''){
                        ?>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingFive">
                                <div class="panel-title">
                                    <h4 class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                                        <?=t($meta['faq.5.header']);?>
                                    </h4>
                                </div>
                            </div>
                            <div id="collapseFive" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFive">
                                <div class="panel-body">
                                    <?=t($meta['faq.5.content']);?>
                                </div>
                            </div>
                        </div>
                        <?php
                    }
                    ?>
                    <?php
                    if(t($meta['faq.6.header']) !== ''){
                        ?>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingSix">
                                <div class="panel-title">
                                    <h4 class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
                                        <?=t($meta['faq.6.header']);?>
                                    </h4>
                                </div>
                            </div>
                            <div id="collapseSix" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingSix">
                                <div class="panel-body">
                                    <?=t($meta['faq.6.content']);?>
                                </div>
                            </div>
                        </div>
                        <?php
                    }
                    ?>
                    <?php
                    if(t($meta['faq.7.header']) !== ''){
                        ?>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingSeven">
                                <div class="panel-title">
                                    <h4 class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseSeven" aria-expanded="false" aria-controls="collapseSeven">
                                        <?=t($meta['faq.7.header']);?>
                                    </h4>
                                </div>
                            </div>
                            <div id="collapseSeven" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingSeven">
                                <div class="panel-body">
                                    <?=t($meta['faq.7.content']);?>
                                </div>
                            </div>
                        </div>
                        <?php
                    }
                    ?>
                </div>
            </div>
        </div>
    <?php } ?>
    <?php if(!is_page($front_fourth)){ ?>
        <div class="home_faq stars_wrap">
            <div class="wrapper"><?php if(function_exists('the_ratings')) { the_ratings(); } ?></div>
        </div>
    <?php } ?>
</main>
<div class="paypal_redirect">
    <p class="title">You are logged in now</p>
    <p>We are redirecting you to Websites Dashboard</p>
    <div class="spinWrap">
        <p class="spinnerImage"></p>
        <p class="loader"></p>
    </div>
</div>
<?php if(is_page($front_third) || is_page($front_fourth)){ ?>
    <style>
        #mc_embed_signup_scroll {
            display: flex;
            align-items: center;
            justify-content: space-between;
            height: 60px;
            max-width: 750px;
            margin: 40px auto 10px auto;
            border-radius: 2px;
            box-shadow: 0 1px 3px 0 rgba(24,24,24,.2), 0 2px 2px 0 rgba(24,24,24,.12), 0 0 2px 0 rgba(24,24,24,.14);
        }
        #mc_embed_signup_scroll input, .home_first-search input {
            width: 80%;
            height: 100%;
            background: #fff;
            color: #898c8e;
            border: none;
            padding: 0 20px;
            font-size: 15px;
            font-weight: 100;
            letter-spacing: .2px;
            border-top-left-radius: 2px;
            border-bottom-left-radius: 2px;
        }
        #mc_embed_signup_scroll input[name=b_b33f62afc5e797a4e431a7266_c6ca5d5cd2], .home_first-search input[name=b_b33f62afc5e797a4e431a7266_c6ca5d5cd2] {
            display: none;
        }
        #mc_embed_signup_scroll button, #mc_embed_signup_scroll input[name=subscribe], .home_first-search button, .home_first-search input[name=subscribe] {
            position: relative;
            appearance: none;
            background: #1687bd;
            color: #fff;
            outline: 0;
            overflow: hidden;
            cursor: pointer;
            width: 15%;
            min-width: 180px;
            border: none;
            border-top-right-radius: 2px;
            border-bottom-right-radius: 2px;
            border-top-left-radius: 0;
            border-bottom-left-radius: 0;
            font-size: 16px;
            font-weight: 700;
            letter-spacing: .3px;
            height: 100%;
        }
        #mc_embed_signup_scroll input:focus::-webkit-input-placeholder{color:transparent}#mc_embed_signup_scroll input:focus:-moz-placeholder{color:transparent} #mc_embed_signup_scroll input:focus::-moz-placeholder{color:transparent}#mc_embed_signup_scroll input:focus:-ms-input-placeholder{color:transparent}
        .home_description:nth-child(2n) .home_description-img {
            text-align: right;
        }
        .home_description-img {
            text-align: left;
        }
        .home_faq.stars_wrap{margin: 40px auto;}
        .home_description:nth-child(6){border-bottom:0;}
        .post-ratings{justify-content: center;}
    </style>
<?php } ?>