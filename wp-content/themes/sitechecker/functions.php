<?php

if (!function_exists('write_log')) {

    function write_log($log) {
        //f (true === WP_DEBUG) {
        if (is_array($log) || is_object($log)) {
            error_log(print_r($log, true));
        } else {
            error_log($log);
        }
        //}
    }

}


//i can log data like objects
//write_log($whatever_you_want_to_log);

/*
 *  Author: Todd Motto | @toddmotto
 *  URL: html5blank.com | @html5blank
 *  Custom functions, support, custom post types and more.
 */

/*------------------------------------*\
	External Modules/Files
\*------------------------------------*/

// Load any external files you have here

/*------------------------------------*\
	Theme Support
\*------------------------------------*/
//ini_set('display_errors', 1);
//ini_set('display_startup_errors', 1);
//error_reporting(E_ALL);
require_once ('crawler/functions/crawler_functions.php');

if (!isset($content_width)) {
    $content_width = 900;
}

if (function_exists('add_theme_support')) {
    // Add Menu Support
    add_theme_support('menus');

    // Add Thumbnail Theme Support
    add_theme_support('post-thumbnails');
    add_image_size('large', 700, '', true); // Large Thumbnail
    add_image_size('medium', 250, '', true); // Medium Thumbnail
    add_image_size('small', 120, '', true); // Small Thumbnail
    add_image_size('custom-size', 700, 200, true); // Custom Thumbnail Size call using the_post_thumbnail('custom-size');

    // Add Support for Cаustom Backgrounds - Uncomment below if you're going to use
    /*add_theme_support('custom-background', array(
	'default-color' => 'FFF',
	'default-image' => get_template_directory_uri() . '/img/bg.jpg'
    ));*/

    // Add Support for Custom Header - Uncomment below if you're going to use
    /*add_theme_support('custom-header', array(
	'default-image'			=> get_template_directory_uri() . '/img/headers/default.jpg',
	'header-text'			=> false,
	'default-text-color'		=> '000',
	'width'				=> 1000,
	'height'			=> 198,
	'random-default'		=> false,
	'wp-head-callback'		=> $wphead_cb,
	'admin-head-callback'		=> $adminhead_cb,
	'admin-preview-callback'	=> $adminpreview_cb
    ));*/

    // Enables post and comment RSS feed links to head
    add_theme_support('automatic-feed-links');

    // Localisation Support
    load_theme_textdomain('html5blank', get_template_directory() . '/languages');
}

/*------------------------------------*\
	Functions
\*------------------------------------*/

// HTML5 Blank navigation
function footer_products_design()
{
    wp_nav_menu(
        array(
            'theme_location' => 'footer-menu',
            'menu' => 'Footer Products Design',
            'container' => 'div',
            'container_class' => 'menu-{menu slug}-container',
            'container_id' => '',
            'menu_class' => 'menu',
            'menu_id' => '',
            'echo' => true,
            'fallback_cb' => 'wp_page_menu',
            'before' => '',
            'after' => '',
            'link_before' => '',
            'link_after' => '',
            'items_wrap' => '<ul>%3$s</ul>',
            'depth' => 0,
            'walker' => ''
        )
    );
}
function footer_company_design()
{
    wp_nav_menu(
        array(
            'theme_location' => 'footer-menu',
            'menu' => 'Footer Company Design',
            'container' => 'div',
            'container_class' => 'menu-{menu slug}-container',
            'container_id' => '',
            'menu_class' => 'menu',
            'menu_id' => '',
            'echo' => true,
            'fallback_cb' => 'wp_page_menu',
            'before' => '',
            'after' => '',
            'link_before' => '',
            'link_after' => '',
            'items_wrap' => '<ul>%3$s</ul>',
            'depth' => 0,
            'walker' => ''
        )
    );
}
function footer_resources_design()
{
    wp_nav_menu(
        array(
            'theme_location' => 'footer-menu',
            'menu' => 'Footer Resources Design',
            'container' => 'div',
            'container_class' => 'menu-{menu slug}-container',
            'container_id' => '',
            'menu_class' => 'menu',
            'menu_id' => '',
            'echo' => true,
            'fallback_cb' => 'wp_page_menu',
            'before' => '',
            'after' => '',
            'link_before' => '',
            'link_after' => '',
            'items_wrap' => '<ul>%3$s</ul>',
            'depth' => 0,
            'walker' => ''
        )
    );
}

function footer_company_nav()
{
    wp_nav_menu(
        array(
            'theme_location' => 'footer-menu',
            'menu' => 'Footer Company',
            'container' => 'div',
            'container_class' => 'menu-{menu slug}-container',
            'container_id' => '',
            'menu_class' => 'menu',
            'menu_id' => '',
            'echo' => true,
            'fallback_cb' => 'wp_page_menu',
            'before' => '',
            'after' => '',
            'link_before' => '',
            'link_after' => '',
            'items_wrap' => '<ul>%3$s</ul>',
            'depth' => 0,
            'walker' => ''
        )
    );
}

// HTML5 Blank navigation
function footer_services_nav()
{
    wp_nav_menu(
        array(
            'theme_location' => 'footer-menu',
            'menu' => 'Footer Services',
            'container' => 'div',
            'container_class' => 'menu-{menu slug}-container',
            'container_id' => '',
            'menu_class' => 'menu',
            'menu_id' => '',
            'echo' => true,
            'fallback_cb' => 'wp_page_menu',
            'before' => '',
            'after' => '',
            'link_before' => '',
            'link_after' => '',
            'items_wrap' => '<ul>%3$s</ul>',
            'depth' => 0,
            'walker' => ''
        )
    );
}

// HTML5 Blank navigation
function header_language_nav()
{
    wp_nav_menu(
        array(
            'theme_location' => 'header-menu',
            'menu' => 'Language Menu',
            'container' => 'div',
            'container_class' => 'menu-{menu slug}-container',
            'container_id' => '',
            'menu_class' => 'menu',
            'menu_id' => '',
            'echo' => true,
            'fallback_cb' => 'wp_page_menu',
            'before' => '',
            'after' => '',
            'link_before' => '',
            'link_after' => '',
            'items_wrap' => '<ul class="language_menu">%3$s</ul>',
            'depth' => 0,
            'walker' => ''
        )
    );
}

// Register HTML5 Blank Navigation
function register_html5_menu()
{
    register_nav_menus(array( // Using array to specify more menus if needed
        'header-menu' => __('Header Menu', 'html5blank'), // Main Navigation
        'sidebar-menu' => __('Sidebar Menu', 'html5blank'), // Sidebar Navigation
        'extra-menu' => __('Extra Menu', 'html5blank') // Extra Navigation if needed (duplicate as many as you need!)
    ));
}

// Remove the <div> surrounding the dynamic navigation to cleanup markup
function my_wp_nav_menu_args($args = '')
{
    $args['container'] = false;
    return $args;
}

// Remove Injected classes, ID's and Page ID's from Navigation <li> items
function my_css_attributes_filter($var)
{
    return is_array($var) ? array() : '';
}

// Remove invalid rel attribute values in the categorylist
function remove_category_rel_from_category_list($thelist)
{
    return str_replace('rel="category tag"', 'rel="tag"', $thelist);
}

// Add page slug to body class, love this - Credit: Starkers Wordpress Theme
function add_slug_to_body_class($classes)
{
    global $post;
    if (is_home()) {
        $key = array_search('blog', $classes);
        if ($key > -1) {
            unset($classes[$key]);
        }
    } elseif (is_page()) {
        $classes[] = sanitize_html_class($post->post_name);
    } elseif (is_singular()) {
        $classes[] = sanitize_html_class($post->post_name);
    }

    return $classes;
}

// If Dynamic Sidebar Exists
if (function_exists('register_sidebar')) {
    // Define Sidebar Widget Area 1
    register_sidebar(array(
        'name' => __('Widget Area 1', 'html5blank'),
        'description' => __('Description for this widget-area...', 'html5blank'),
        'id' => 'widget-area-1',
        'before_widget' => '<div id="%1$s" class="%2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h3>',
        'after_title' => '</h3>'
    ));

    // Define Sidebar Widget Area 2
    register_sidebar(array(
        'name' => __('Widget Area 2', 'html5blank'),
        'description' => __('Description for this widget-area...', 'html5blank'),
        'id' => 'widget-area-2',
        'before_widget' => '<div id="%1$s" class="%2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h3>',
        'after_title' => '</h3>'
    ));
}

// Remove wp_head() injected Recent Comment styles
function my_remove_recent_comments_style()
{
    global $wp_widget_factory;
    remove_action('wp_head', array(
        $wp_widget_factory->widgets['WP_Widget_Recent_Comments'],
        'recent_comments_style'
    ));
}

// Pagination for paged posts, Page 1, Page 2, Page 3, with Next and Previous Links, No plugin
function html5wp_pagination()
{
    global $wp_query;
    $big = 999999999;
    echo paginate_links( array(
        'base' => str_replace($big, '%#%', get_pagenum_link($big)),
        'total' => $wp_query->max_num_pages,
        'current' => max(1, get_query_var('paged')),
        'format'       => '?paged=%#%',
        'show_all'     => false,
        'type'         => 'plain',
        'end_size'     => 2,
        'mid_size'     => 1,
        'prev_next'    => true,
        'prev_text'    => sprintf( '<i class="fa fa-chevron-left"></i> %1$s', __('') ),
        'next_text'    => sprintf( '%1$s <i class="fa fa-chevron-right"></i>', __('') ),
        'add_args'     => false,
        'add_fragment' => '',
    ) );
}

// Custom Excerpts
function html5wp_index($length) // Create 20 Word Callback for Index page Excerpts, call using html5wp_excerpt('html5wp_index');
{
    return 30;
}

// Create 40 Word Callback for Custom Post Excerpts, call using html5wp_excerpt('html5wp_custom_post');
function html5wp_custom_post($length)
{
    return 40;
}

// Create the Custom Excerpts callback
function html5wp_excerpt($length_callback = '', $more_callback = '')
{
    global $post;
    if (function_exists($length_callback)) {
        add_filter('excerpt_length', $length_callback);
    }
    if (function_exists($more_callback)) {
        add_filter('excerpt_more', $more_callback);
    }
    $output = get_the_excerpt();
    $output = apply_filters('wptexturize', $output);
    $output = apply_filters('convert_chars', $output);
    echo $output;
}

// Custom View Article link to Post
function html5_blank_view_article($more)
{
    global $post;
    return '...';
}

// Remove Admin bar
function remove_admin_bar()
{
    return false;
}

// Remove 'text/css' from our enqueued stylesheet
function html5_style_remove($tag)
{
    return preg_replace('~\s+type=["\'][^"\']++["\']~', '', $tag);
}

// Remove thumbnail width and height dimensions that prevent fluid images in the_thumbnail
function remove_thumbnail_dimensions($html)
{
    $html = preg_replace('/(width|height)=\"\d*\"\s/', "", $html);
    return $html;
}

// Custom Gravatar in Settings > Discussion
function html5blankgravatar($avatar_defaults)
{
    $myavatar = get_template_directory_uri() . '/img/gravatar.jpg';
    $avatar_defaults[$myavatar] = "Custom Gravatar";
    return $avatar_defaults;
}

// Threaded Comments
function enable_threaded_comments()
{
    if (!is_admin()) {
        if (is_singular() AND comments_open() AND (get_option('thread_comments') == 1)) {
            wp_enqueue_script('comment-reply');
        }
    }
}

// Custom Comments Callback
function html5blankcomments($comment, $args, $depth)
{
    $GLOBALS['comment'] = $comment;
    extract($args, EXTR_SKIP);

    if ('div' == $args['style']) {
        $tag = 'div';
        $add_below = 'comment';
    } else {
        $tag = 'li';
        $add_below = 'div-comment';
    }
    ?>
    <!-- heads up: starting < for the html tag (li or div) in the next line: -->
    <<?php echo $tag ?><?php comment_class(empty($args['has_children']) ? '' : 'parent') ?> id="comment-<?php comment_ID() ?>">
    <?php if ('div' != $args['style']) : ?>
    <div id="div-comment-<?php comment_ID() ?>" class="comment-body">
<?php endif; ?>
    <div class="comment-author vcard">
        <?php if ($args['avatar_size'] != 0) echo get_avatar($comment, $args['180']); ?>
        <?php printf(__('<cite class="fn">%s</cite> <span class="says">says:</span>'), get_comment_author_link()) ?>
    </div>
    <?php if ($comment->comment_approved == '0') : ?>
    <em class="comment-awaiting-moderation"><?php _e('Your comment is awaiting moderation.') ?></em>
    <br/>
<?php endif; ?>

    <div class="comment-meta commentmetadata"><a
                href="<?php echo htmlspecialchars(get_comment_link($comment->comment_ID)) ?>">
            <?php
            printf(__('%1$s at %2$s'), get_comment_date(), get_comment_time()) ?></a><?php edit_comment_link(__('(Edit)'), '  ', '');
        ?>
    </div>

    <?php comment_text() ?>

    <div class="reply">
        <?php comment_reply_link(array_merge($args, array('add_below' => $add_below, 'depth' => $depth, 'max_depth' => $args['max_depth']))) ?>
    </div>
    <?php if ('div' != $args['style']) : ?>
    </div>
<?php endif; ?>
<?php }

/*------------------------------------*\
	Actions + Filters + ShortCodes
\*------------------------------------*/

// Add Actions
add_action('init', 'html5blank_header_scripts'); // Add Custom Scripts to wp_head
add_action('wp_print_scripts', 'html5blank_conditional_scripts'); // Add Conditional Page Scripts
add_action('get_header', 'enable_threaded_comments'); // Enable Threaded Comments
add_action('wp_enqueue_scripts', 'html5blank_styles'); // Add Theme Stylesheet
add_action('init', 'register_html5_menu'); // Add HTML5 Blank Menu
// add_action('init', 'create_post_type_html5'); // Add our HTML5 Blank Custom Post Type
add_action('widgets_init', 'my_remove_recent_comments_style'); // Remove inline Recent Comment Styles from wp_head()
add_action('init', 'html5wp_pagination'); // Add our HTML5 Pagination

// Remove Actions
remove_action('wp_head', 'feed_links_extra', 3); // Display the links to the extra feeds such as category feeds
remove_action('wp_head', 'feed_links', 2); // Display the links to the general feeds: Post and Comment Feed
remove_action('wp_head', 'rsd_link'); // Display the link to the Really Simple Discovery service endpoint, EditURI link
remove_action('wp_head', 'wlwmanifest_link'); // Display the link to the Windows Live Writer manifest file.
remove_action('wp_head', 'index_rel_link'); // Index link
remove_action('wp_head', 'parent_post_rel_link', 10, 0); // Prev link
remove_action('wp_head', 'start_post_rel_link', 10, 0); // Start link
remove_action('wp_head', 'adjacent_posts_rel_link', 10, 0); // Display relational links for the posts adjacent to the current post.
remove_action('wp_head', 'wp_generator'); // Display the XHTML generator that is generated on the wp_head hook, WP version
remove_action('wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);
remove_action('wp_head', 'rel_canonical');
remove_action('wp_head', 'wp_shortlink_wp_head', 10, 0);

// Add Filters
add_filter('avatar_defaults', 'html5blankgravatar'); // Custom Gravatar in Settings > Discussion
add_filter('body_class', 'add_slug_to_body_class'); // Add slug to body class (Starkers build)
add_filter('widget_text', 'do_shortcode'); // Allow shortcodes in Dynamic Sidebar
add_filter('widget_text', 'shortcode_unautop'); // Remove <p> tags in Dynamic Sidebars (better!)
add_filter('wp_nav_menu_args', 'my_wp_nav_menu_args'); // Remove surrounding <div> from WP Navigation
// add_filter('nav_menu_css_class', 'my_css_attributes_filter', 100, 1); // Remove Navigation <li> injected classes (Commented out by default)
// add_filter('nav_menu_item_id', 'my_css_attributes_filter', 100, 1); // Remove Navigation <li> injected ID (Commented out by default)
// add_filter('page_css_class', 'my_css_attributes_filter', 100, 1); // Remove Navigation <li> Page ID's (Commented out by default)
add_filter('the_category', 'remove_category_rel_from_category_list'); // Remove invalid rel attribute
add_filter('the_excerpt', 'shortcode_unautop'); // Remove auto <p> tags in Excerpt (Manual Excerpts only)
add_filter('the_excerpt', 'do_shortcode'); // Allows Shortcodes to be executed in Excerpt (Manual Excerpts only)
add_filter('excerpt_more', 'html5_blank_view_article'); // Add 'View Article' button instead of [...] for Excerpts
add_filter('show_admin_bar', 'remove_admin_bar'); // Remove Admin bar
add_filter('style_loader_tag', 'html5_style_remove'); // Remove 'text/css' from enqueued stylesheet
add_filter('post_thumbnail_html', 'remove_thumbnail_dimensions', 10); // Remove width and height dynamic attributes to thumbnails
add_filter('image_send_to_editor', 'remove_thumbnail_dimensions', 10); // Remove width and height dynamic attributes to post images

// Remove Filters
remove_filter('the_excerpt', 'wpautop'); // Remove <p> tags from Excerpt altogether

// Shortcodes
add_shortcode('html5_shortcode_demo', 'html5_shortcode_demo'); // You can place [html5_shortcode_demo] in Pages, Posts now.
add_shortcode('html5_shortcode_demo_2', 'html5_shortcode_demo_2'); // Place [html5_shortcode_demo_2] in Pages, Posts now.

/*------------------------------------*\
	ShortCode Functions
\*------------------------------------*/

// Shortcode Demo with Nested Capability
function html5_shortcode_demo($atts, $content = null)
{
    return '<div class="shortcode-demo">' . do_shortcode($content) . '</div>'; // do_shortcode allows for nested Shortcodes
}

// Shortcode Demo with simple <h2> tag
function html5_shortcode_demo_2($atts, $content = null) // Demo Heading H2 shortcode, allows for nesting within above element. Fully expandable.
{
    return '<h2>' . $content . '</h2>';
}

add_shortcode('blog_tags', 'blog_tags_func');
function blog_tags_func($atts)
{

    extract(shortcode_atts(array(
        'tags' => null
    ), $atts));

    $tag_names = explode(",", strval($tags));
    $content = '';

    foreach ($tag_names as $tag_name) {

        $tag = get_term_by('name', $tag_name, 'post_tag');

        $tag_link = get_tag_link($tag->term_id);

        $tag_img = $tag->description;

        $content .= '<a href="' . $tag_link . '" class="blog_tags-block" style="background-image: url(' . $tag_img . ')"><span>' . $tag->name . '</span></a>';

    }

    return $content;
}

function image_func($atts)
{

    $content .= '<div class="home_description-img"><img src="' . get_template_directory_uri() . '/out/img/img-border.svg" alt="img-border" title="img-border" class="img-border"><div><img src="' . $atts['url'] . '" alt="' . $atts['alt'] . '" title="' . $atts['title'] . '" class="description-img"><img src="' . get_template_directory_uri() . '/out/img/img-close.svg" alt="img-close" title="img-close" class="img-close"></div></div>';

    return $content;
}

add_shortcode('image', 'image_func');

function code_func($atts)
{

    $content .= '<div class="code"><span class="copy_code">Copy code</span><xmp>' . $atts['text'] . '</xmp></div>';

    return $content;
}

add_shortcode('code', 'code_func');

// Load HTML5 Blank scripts (header.php)
function html5blank_header_scripts()
{
}

function html5blank_styles()
{
}

function html5blank_conditional_scripts()
{
}

function my_wpcf7_dropdown_form($html)
{
    $text = 'Select your budget';
    $html = str_replace('---', '' . $text . '', $html);
    return $html;
}

add_filter('wpcf7_form_elements', 'my_wpcf7_dropdown_form');

function remove_parent_filters()
{ //Have to do it after theme setup, because child theme functions are loaded first
    remove_filter('show_admin_bar', 'remove_admin_bar');
}

add_action('after_setup_theme', 'remove_parent_filters');

/**
 * Alias for translate function
 *
 * @param $message
 * @return array|mixed|void
 */
function t($message)
{
    return qtrans_useCurrentLanguageIfNotFoundUseDefaultLanguage($message);
}

function get_post_meta_all($post_id)
{
    global $wpdb;
    $data = array();
    $wpdb->query("
        SELECT `meta_key`, `meta_value`
        FROM $wpdb->postmeta
        WHERE `post_id` = $post_id
    ");
    foreach ($wpdb->last_result as $k => $v) {
        $data[$v->meta_key] = $v->meta_value;
    };
    return $data;
}

/** Memcached connect */
function initMemcached()
{
    $mc = new Memcached();
    $mc->addServer(MEMCACHED['host'], MEMCACHED['port']);

    return $mc;
}

function getPageBySlug($slug, $type = OBJECT)
{
    return get_page_by_path($slug, $type);
}

/**
 * Seo-report page rewrite rule
 */
add_action('parse_request', 'seo_report_pages');

function seo_report_pages(&$wp)
{
    // seo-report
    if (preg_match('@^/([a-z]{2}/)?seo-report/(.*)$@', $_SERVER['REQUEST_URI'], $matches)) {
        if (isset($matches[2]) && !empty($matches[2])) {
            global $wp_query;
            $wp_query->post = getPageBySlug('seo-report');
            $wp->query_vars['seo-report'] = $matches[2];
            include('seo-report/main.php');
        } else {
            status_header(404);
            include(get_query_template('404'));
        }
        exit;

        // mtf
    } elseif (preg_match('@^/mtf/(.*)$@', $_SERVER['REQUEST_URI'], $matches)) {
        if (isset($matches[1]) && !empty($matches[1])) {
            global $wp_query;
            $wp_query->post = getPageBySlug('seo-report');
            $wp->query_vars['seo-report'] = $matches[1];
            include('seo-report/mtf.php');
        } else {
            status_header(404);
            include(get_query_template('404'));
        }
        exit;
    }
}
/**
 * Seo-report demo page rewrite rule
 */
add_action('parse_request', 'seo_report_pages_demo');

function seo_report_pages_demo(&$wp)
{
    // seo-report-demo
    if (preg_match('@^/([a-z]{2}/)?seo-report-demo/(.*)$@', $_SERVER['REQUEST_URI'], $matches)) {
        if (isset($matches[2]) && !empty($matches[2])) {
            global $wp_query;
            $wp_query->post = getPageBySlug('seo-report-demo');
            $wp->query_vars['seo-report-demo'] = $matches[2];
            include('seo-report/main__demo.php');
        } else {
            status_header(404);
            include(get_query_template('404'));
        }
        exit;
    }
}
function getTransResultByLocale($lang){
    $config = API_DB;
    $pdo = new PDO("pgsql:dbname=${config['name']};host=${config['host']}", $config['user'], $config['password']);

    $query = "SELECT m.sign AS sign, t.sign AS message FROM messages m
          LEFT JOIN messages_translations t ON (m.id = t.translatable_id AND t.locale = '" . $lang . "')";

    $result = [];
    $stmt = $pdo->query($query);
    while ($row = $stmt->fetch()) {
        $result[$row['sign']] = addslashes($row['message']);
    }
    return $result;
}
/** Get Symfony API translations & cache them */
function getApiDictionary()
{
    $lang = qtrans_getLanguage();
    $key = 'api_dictionary_' . $lang;
    $memcached = initMemcached();

    if (!$dict = $memcached->get($key)) {

        $result = getTransResultByLocale($lang);
        $engResult = getTransResultByLocale('en');

        unset($pdo);

        $t = function ($msg) use ($result, $engResult) {
            if($result[$msg]){
                return $result[$msg];
            }
            return $engResult[$msg] ?? $msg;
        };

        $dict = [
            'Quick navigation' => $t('Quick navigation'),
            'Results' => $t('Results'),
            'Page score' => $t('Page score'),
            'Waiting' => $t('Waiting'),
            'Indexable' => $t('Indexable'),
            'Not indexable' => $t('Not indexable'),
            'How to fix' => $t('How to fix'),
            'Error' => $t('Error'),
            'empty' => $t('empty'),
            'Hide' => $t('Hide'),
            'Warnings' => $t('Warnings'),
            'Notice' => $t('Notice'),
            'Show more' => $t('Show more'),
            'Critical Errors' => $t('Critical Errors'),
            'validations' => [
                'names' => [
                    1 => $t('InformationValidation.Group'),
                    2 => $t('ImagesValidation.Group'),
                    3 => $t('IsOpenForIndexation.Group'),
                    4 => $t('ExternalLinks.Group'),
                    5 => $t('PageSpeedDesktopValidation.Group'),
                ],
                '0' => $t('InformationValidation.Title'),
                '1' => $t('TitleValidation.Title'),
                '2' => $t('DescriptionValidation.Title'),
                '3' => $t('HTagsValidation.Title'),
                '4' => $t('ContentValidation.Title'),
                '5' => $t('CanonicalLinkValidation.Title'),
                '6' => $t('AlternateLinkValidation.Title'),
                '7' => $t('PaginationValidation.Title'),
                '8' => $t('IsOpenForIndexation.Title'),
                '9' => $t('VulnerabilityValidation.Title'),
                '10' => $t('BotCheckValidation.Title'),
                '11' => $t('ExternalLinks.Title'),
                '12' => $t('SubdomainLinks.Title'),
                '13' => $t('InternalLinks.Title'),
                '14' => $t('PageSpeedMobileValidation.Title'),
                '15' => $t('PageSpeedDesktopValidation.Title'),
                '16' => $t('FaviconsValidation.Title'),
                '17' => $t('ImagesValidation.Title'),
            ],
            'MobilePreview' => $t('MobilePreview.Title'),
            'GoogleSnippet' => [
                'Title' => $t('GoogleSnippet.Title'),
                'Fix' => $t('GoogleSnippet.Fix'),
                'EmptyDescription' => $t('GoogleSnippet.EmptyDescription'),
            ],
            'Tables' => [
                'Preview' => $t('ImagesValidation.Preview'),
                'PreviewTooltip' => $t('ImagesValidation.PreviewTooltip'),
                'AltAttribute' => $t('ImagesValidation.AltAttribute'),
                'AltAttributeTooltip' => $t('ImagesValidation.AltAttributeTooltip'),
                'TitleAttribute' => $t('ImagesValidation.TitleAttribute'),
                'TitleAttributeTooltip' => $t('ImagesValidation.TitleAttributeTooltip'),
                'Size' => $t('ImagesValidation.Size'),
                'SizeTooltip' => $t('ImagesValidation.SizeTooltip'),
                'Link' => $t('Tables.Link'),
                'LinkTooltip' => $t('Tables.LinkTooltip'),
                'Lang' => $t('Tables.Lang'),
                'LangTooltip' => $t('Tables.LangTooltip'),
                'MH' => $t('Tables.MH'),
                'MHTooltip' => $t('Tables.MHTooltip'),
                'B' => $t('Tables.B'),
                'BTooltip' => $t('Tables.BTooltip'),
                'Code' => $t('Tables.Code'),
                'CodeTooltip' => $t('Tables.CodeTooltip'),
                'Qty' => $t('Tables.Qty'),
                'QtyTooltip' => $t('Tables.QtyTooltip'),
                'Anchor' => $t('Tables.Anchor'),
                'AnchorTooltip' => $t('Tables.AnchorTooltip'),
                'DisplayTags' => $t('HTagsValidation.DisplayTagsDescription'),
                'CountAllTags' => $t('HTagsValidation.CountAllTagsDescription'),
            ],
            'Newsletter' => [
                'Thank You for signing up' => $t('Thank You for signing up'),
                'Enter correct email' => $t('Enter correct email'),
                'Enter your email' => $t('Error'),
            ],
            'Form.Page is' => $t('Form.Page is'),
            'Form.Page size' => $t('Form.Page size'),
            'Form.Page status codes' => $t('Form.Page status codes'),
            'Minitools.Robots.Placeholder' => $t('Minitools.Robots.Placeholder'),
            'Minitools.Button' => $t('Minitools.Button'),
            'Minitools.Robots.Title' => $t('Minitools.Robots.Title'),
            'Minitools.404.Title' => $t('Minitools.404.Title'),
            'Minitools.Error.Placeholder' => $t('Minitools.Error.Placeholder'),
            'Minitools.Favicons.Title' => $t('Minitools.Favicons.Title'),
            'Minitools.URLLength.Title' => $t('Minitools.URLLength.Title'),
            'Minitools.H-1.Title' => $t('Minitools.H-1.Title'),
            'Minitools.Title.Title' => $t('Minitools.Title.Title'),
            'Minitools.Description.Title' => $t('Minitools.Description.Title'),
            'Minitools.Redirection.Title' => $t('Minitools.Redirection.Title'),
            'Minitools.WwwNonWww.Title' => $t('Minitools.WwwNonWww.Title'),
            'Minitools.Canonical.Title' => $t('Minitools.Canonical.Title'),
            'Minitools.InternalLinks.Title' => $t('Minitools.InternalLinks.Title'),
            'Minitools.Images.Title' => $t('Minitools.Images.Title'),
            'Minitools.StatusCode.Title' => $t('Minitools.StatusCode.Title'),
            'Minitools.Pagination.Title' => $t('Minitools.Pagination.Title'),
            'Minitools.ExternalLinks.Title' => $t('Minitools.ExternalLinks.Title'),
            'Minitools.PageSize.Title' => $t('Minitools.PageSize.Title'),
            'Minitools.Indexing.Title' => $t('Minitools.Indexing.Title'),
            'Minitools.Speed.Title' => $t('Minitools.Speed.Title')
        ];

        $memcached->set($key, $dict, 5 * 60);
    }

    return $dict;
}

function get_post_meta_by_urls(array $urls, $meta_key)
{
    if (!$urls)
        return;

    global $wpdb;
    $sql = "SELECT p.post_name AS name, m.meta_value AS val 
            FROM " . $wpdb->prefix . "posts p 
            LEFT JOIN wp_postmeta m ON (m.post_id = p.ID AND m.meta_key = '" . $meta_key . "') 
            WHERE p.post_name IN ('" . implode("','", $urls) . "')";

    if ($data = $wpdb->get_results($sql, ARRAY_A)) {
        $result = [];
        foreach ($data as $d) {
            $result[$d['name']] = $d['val'];
        }

        return $result;
    }

    return false;
}

add_filter('robots_txt', 'wpse_77969_robots');
function wpse_77969_robots()
{
    status_header(404);
    return '';
}

add_filter("pmpro_login_redirect", "__return_false");
function my_pmpro_default_registration_level($user_id)
{
    //Give all members who register membership level 1
    pmpro_changeMembershipLevel(1, $user_id);
}

add_action('register_new_user', 'my_pmpro_default_registration_level');

function auto_login_new_user($user_id)
{
    if ($_REQUEST['action'] !== 'process_crm_message') {
        if(isset($_REQUEST['redirect_to'])){
            wp_set_current_user($user_id);
            wp_set_auth_cookie($user_id);
            set_semscale_cookie_after_register();
            $haystack = $_REQUEST['redirect_to'];
            $needle = 'crawl-report';
            $needleurl = 'create';

            if(strstr($haystack, $needle) && strstr($haystack, $needleurl)){
                wp_redirect($_REQUEST['redirect_to']);
            } else{
                wp_redirect($_REQUEST['redirect_to']);
            }
            exit;
        }
        wp_set_current_user($user_id);
        wp_set_auth_cookie($user_id);
        set_semscale_cookie_after_register();
        wp_redirect('/');
        exit;
    }

}

add_action('register_new_user', 'auto_login_new_user');

function register_in_semscale($userId)
{
    try{

        write_log('User registration in semscale with id: '.$userId);

        $user_info = get_userdata($userId);
        $userEmail = $user_info->user_email;
        $userLogin = $user_info->user_login ? $user_info->user_login : $userEmail;

        $data["dataActions"][] = [
            "title" => "user register",
            "action" => "REGISTER",
            "arguments" => [
                "username" => $userLogin,
                "email" => $userEmail,
                "password" => $userEmail,
                "source" => "sitechecker"
            ],
        ];

        write_log('User registration in semscale with request: '.json_encode($data));

        $response = Requests::post(SEMASCALE_URL, array(), json_encode($data));
        $response = json_decode($response->body, true);
        $api_key = $response['dataActions'][0]['response']['apiKey'];

        try{
            $message = "request: ".json_encode($data)."\n response".json_encode($response);
//            sendMessageToLogger($message);
            write_log($message);
        }catch (Throwable $e){

        }

        global $wpdb;
        $wpdb->query("
        UPDATE wp_users 
        SET semscale_api_key = '$api_key'
        WHERE ID = $userId
    ");
    }catch (Throwable $exception){
        write_log('Registration Exception: '.$exception->getMessage());
        write_log($exception);
        sendExceptionToLogger($exception);
    }
}

add_action('user_register', 'register_in_semscale');

function change_semscale_quotas($level_id, $user_id, $cancel_level){
    global $wpdb;

    $quotas = $wpdb->get_row("
    SELECT 
      semscale_domains_per_user,
      semscale_keywords_per_domain,
      semscale_links_per_domain
    FROM crawler_memberships_levels_info WHERE membership_id = $level_id");

    $quotasFinal = [
        "domains_per_user" => $quotas->semscale_domains_per_user,
        "keywords_per_domain" => $quotas->semscale_keywords_per_domain,
        "links_per_domain" => $quotas->semscale_links_per_domain
    ];

    $apiKey = $wpdb->get_var("SELECT u.semscale_api_key FROM wp_users u WHERE ID = $user_id");

    $data["dataActions"][] = [
        "title" => "set user quota",
        "action" => "SET_USER_QUOTA",
        "arguments" => [
            "quota" => $quotasFinal,
            "apiKey" => $apiKey
        ],
    ];
    Requests::post(SEMASCALE_URL, array(), json_encode($data));
}

add_action('pmpro_after_change_membership_level', 'change_semscale_quotas', 10, 3);


function set_semscale_cookie($login)
{
    $user = get_user_by('login', $login);
    $userId = $user->ID;

    semascaleLogin($userId);
}
add_action( 'wp_login', 'set_semscale_cookie', 99);

function semascaleLogin($userId = false){

    write_log('Semscale login action with userId: '.$userId);


    if(!$userId){
        $userId = get_current_user_id();
        write_log('Semscale login action with userId by get_current_user_id function: '.$userId);
    }
    try{
        delete_semscale_cookie();
        global $wpdb;

        $apiKey = $wpdb->get_var("SELECT u.semscale_api_key FROM wp_users u WHERE ID = $userId");
        $data["dataActions"][] = [
            "title" => "user login",
            "action" => "LOGIN",
            "arguments" => [
                "apiKey" => $apiKey
            ],
        ];

        write_log('Semscale login api request: '.json_encode($data));

        $response = Requests::post(SEMASCALE_URL, [], json_encode($data));

        write_log('Semscale login api response: '.json_encode($response));

        $cookies = $response->cookies;
        $sessionCookie = $cookies->offsetGet(SEMSCALE_LOGIN_COOKIE)->value;
        setcookie(SEMSCALE_LOGIN_COOKIE, $sessionCookie, time() + 30 * DAY_IN_SECONDS);
    }catch (Throwable $exception){
        write_log('Login Exception: '.$exception->getMessage());
        write_log($exception);
        sendExceptionToLogger($exception);

    }
}
function set_semscale_cookie_after_register()
{
    semascaleLogin();
}
function logout_from_semscale(){

    write_log('Semscale logout action');

    $cookie_string = '';
    if(isset($_COOKIE[SEMSCALE_LOGIN_COOKIE])){
        $cookie_string = SEMSCALE_LOGIN_COOKIE . '=' . urlencode($_COOKIE[SEMSCALE_LOGIN_COOKIE]);
    }
    $headers = array(
        'Cookie' => $cookie_string
    );

    $data["dataActions"][] = [
        "title" => "user logout",
        "action" => "LOGOUT",
    ];

    write_log('Semscale logout request: '.json_encode($data));
    write_log('Semscale logout request headers: '.json_encode($headers));
    Requests::post(SEMASCALE_URL, $headers, json_encode($data));
    delete_semscale_cookie();
}

add_action('wp_logout', 'logout_from_semscale');

function delete_semscale_cookie(){
    if(isset($_COOKIE[SEMSCALE_LOGIN_COOKIE])){
        unset($_COOKIE[SEMSCALE_LOGIN_COOKIE]);
    }

    setcookie(SEMSCALE_LOGIN_COOKIE, '', time() - 3600, '/', parse_url(WP_SITEURL)['host']);
    setcookie(".".SEMSCALE_LOGIN_COOKIE, '', time() - 3600, '/', parse_url(WP_SITEURL)['host']);
}

add_action('wp_ajax_check_username', 'ajax_email_exist');
add_action('wp_ajax_nopriv_check_username', 'ajax_email_exist');

add_action('wp_ajax_log_check_request', 'log_check_request');
add_action('wp_ajax_nopriv_log_check_request', 'log_check_request');

function log_check_request(){
    global $wpdb;

    $userId = get_current_user_id();

    $url = $_POST['url'];

    if (substr($url, 0, 4) !== 'http') {
        $url = "http://".$url;
    }

    try{
        $urlParts = parse_url($url);
        $urlHost = $urlParts['scheme'].'://'.$urlParts['host'];
    }catch (Throwable $e){
        $urlHost = $url;
    }
    $wpdb->insert("user_check_log", [
        "url" => $url,
        "status_code" => $_POST['status'],
        "user_id" => $userId,
        "domain" => $urlHost,
        "date_check" => time()
    ]);

    echo json_encode(["status" => true]);
    wp_die();
}

function ajax_email_exist()
{
    $status = USER_NOT_EXISTS;
    if (isset($_POST['email']) && isset($_POST['pass'])) {
        if ($user = get_user_by('email', $_POST['email'])) {
            $status = USER_EMAIL_EXISTS;

            $user_authenticate = wp_authenticate($_POST['email'], $_POST['pass']);
            if (!is_wp_error($user_authenticate)) {
                $status = USER_EXISTS;
            }
        }
    }

    echo $status;
    exit;
}

/*
	Shortcode to show a member's expiration date.

	Add this code to your active theme's functions.php or a custom plugin.

	Then add the shortcode [pmpro_expiration_date] where you want the current user's
	expiration date to appear.

	If the user is logged out or doesn't have an expiration date, then --- is shown.
*/
function pmpro_expiration_date_shortcode( $atts ) {
    //make sure PMPro is active
    if(!function_exists('pmpro_getMembershipLevelForUser'))
        return;

    //get attributes
    $a = shortcode_atts( array(
        'user' => '',
    ), $atts );

    //find user
    if(!empty($a['user']) && is_numeric($a['user'])) {
        $user_id = $a['user'];
    } elseif(!empty($a['user']) && strpos($a['user'], '@') !== false) {
        $user = get_user_by('email', $a['user']);
        $user_id = $user->ID;
    } elseif(!empty($a['user'])) {
        $user = get_user_by('login', $a['user']);
        $user_id = $user->ID;
    } else {
        $user_id = false;
    }

    //no user ID? bail
    if(!isset($user_id))
        return;

    //get the user's level
    $level = pmpro_getMembershipLevelForUser($user_id);

    if(!empty($level) && !empty($level->enddate))
        $content = '<div class="account-table_email"><p>Expiration</p><p class="user-email">'.date(get_option('date_format'), $level->enddate).'</p></div>';

    return $content;
}
add_shortcode('pmpro_expiration_date', 'pmpro_expiration_date_shortcode');

add_action('init','possibly_redirect');

function possibly_redirect(){
    global $pagenow;
    if( 'wp-login.php' == $pagenow ) {
        wp_redirect('/');
        exit();
    }
}

function acme_login_redirect( $redirect_to, $request, $user  ) {
    if ($_REQUEST['action'] !== 'process_crm_message') {

        $user_id = $user->ID;

        if (strpos($_REQUEST['redirect_to'], "create_user_domain") !== false) {
            $newUrl = str_replace("create_user_domain", 'create_user_domain_by_user_id', $_REQUEST['redirect_to']);
            $newUrl = site_url($newUrl."&userId=$user_id");
            header("Location: $newUrl");
            exit;
        }

        if(isset($_REQUEST['redirect_to'])){
            wp_redirect($_REQUEST['redirect_to']);
            exit;
        }
        wp_redirect('/');
        exit;
    }
}
add_filter( 'login_redirect', 'acme_login_redirect', 10, 3 );

function _wpse206466_can_view()
{
    // or any other admin level capability
    return current_user_can('manage_options');
}


add_action('load-index.php', 'wpse206466_load_index');
function wpse206466_load_index()
{
    if (!_wpse206466_can_view()) {
        $qs = empty($_GET) ? '' : '?'.http_build_query($_GET);
        wp_safe_redirect(site_url());
        exit;
    }
}

/*
 * Adding the column
 */
function rd_user_id_column( $columns ) {
    $columns['user_id'] = 'ID';
    return $columns;
}
add_filter('manage_users_columns', 'rd_user_id_column');

/*
 * Column content
 */
function rd_user_id_column_content($value, $column_name, $user_id) {
    if ( 'user_id' == $column_name )
        return $user_id;
    return $value;
}
add_action('manage_users_custom_column',  'rd_user_id_column_content', 10, 3);

/*
 * Column style (you can skip this if you want)
 */
function rd_user_id_column_style(){
    echo '<style>.column-user_id{width: 5%}</style>';
}
add_action('admin_head-users.php',  'rd_user_id_column_style');

function getPostViews($postID){
    $count_key = 'post_views_count';
    $count = get_post_meta($postID, $count_key, true);
    if($count==''){
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '256');
        return "256";
    }
    return $count.'';
}
function setPostViews($postID) {
    $count_key = 'post_views_count';
    $count = get_post_meta($postID, $count_key, true);
    if($count==''){
        $count = 256;
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '256');
    }else{
        $count++;
        update_post_meta($postID, $count_key, $count);
    }
}
// Remove issues with prefetching adding extra views
remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);

//Exclude pages from WordPress Search
if (!is_admin()) {
    function wpb_search_filter($query) {
        if ($query->is_search) {
            $query->set('meta_query', array(
                array(
                    'key' => 'page.about'
                )
            ));
            $query->set('post__not_in', array(17,15,13));
        }
        return $query;
    }
    add_filter('pre_get_posts','wpb_search_filter');
}

function my_pmpro_after_change_membership_level_default_level($level_id, $user_id)
{
    //set this to the id of the level you want to give members when they cancel
    $cancel_level_id = 1;

    //if we see this global set, then another gist is planning to give the user their level back
    global $pmpro_next_payment_timestamp;
    if(!empty($pmpro_next_payment_timestamp))
        return;

    //are they cancelling?
    if($level_id == 0)
    {
        //check if they are cancelling from level $cancel_level_id
        global $wpdb;
        $last_level_id = $wpdb->get_var("SELECT membership_id FROM $wpdb->pmpro_memberships_users WHERE user_id = '" . $user_id . "' ORDER BY id DESC");
        if($last_level_id == $cancel_level_id)
            return;	//let them cancel

        //otherwise give them level $cancel_level_id instead
        pmpro_changeMembershipLevel($cancel_level_id, $user_id);
    }
}
add_action("pmpro_after_change_membership_level", "my_pmpro_after_change_membership_level_default_level", 10, 2);

add_action('wp_footer', 'mycustom_wp_footer' );

function mycustom_wp_footer() {
    ?>
    <script>
        document.addEventListener( 'wpcf7mailsent', function( event ) {
            $('.popup_order').css('display','none');
            let closing_popup = function(){
                $('.popup_sent').fadeOut();
                $('.popup_sent-block').css('display','none');
                enableScroll();
            };
            $('.popup_sent').css('display', 'flex');
            $('.popup_sent-block').fadeIn().css('display', 'flex');
            disableScroll();
            setTimeout(closing_popup, 5000);
            $('.popup_sent-close').click(function(){
                $('.popup_sent').fadeOut();
                $('.popup_sent-block').css('display','none');
                enableScroll();
            })
        }, false );
    </script>
    <?php
}

function remove_jquery_migrate( $scripts ) {
    if ( ! is_admin() && isset( $scripts->registered['jquery'] ) ) {
        $script = $scripts->registered['jquery'];

        if ( $script->deps ) { // Check whether the script has any dependencies
            $script->deps = array_diff( $script->deps, array( 'jquery-migrate' ) );
        }
    }
}

add_action( 'wp_default_scripts', 'remove_jquery_migrate' );

function replace_core_jquery_version() {
    if(is_page(2792)){
        wp_deregister_script( 'jquery' );
    }
}
add_action( 'wp_enqueue_scripts', 'replace_core_jquery_version' );

function prefix_wcount(){
    ob_start();
    the_content();
    $content = ob_get_clean();
    return sizeof(explode(" ", $content));
}
if( wp_verify_nonce( $_POST['fileup_nonce'], 'my_file_upload' ) ){
    if ( ! function_exists( 'wp_handle_upload' ) )
        require_once( ABSPATH . 'wp-admin/includes/file.php' );
    $file = & $_FILES['my_file_upload'];
    $overrides = [ 'test_form' => false ];
    $movefile = wp_handle_upload( $file, $overrides );
    update_user_meta( get_current_user_id(), 'user_pdf_branding', $movefile[url]);
}


/**
 * Send default 'X-Frame-Options' for new Iframe widget
 */
remove_action('login_init', 'send_frame_options_header');
// OR
//function send_frame_options_header() {
//    @header( 'X-Frame-Options: ALLOWALL' );
//}
//add_action( 'send_headers', 'send_frame_options_header' );

?>