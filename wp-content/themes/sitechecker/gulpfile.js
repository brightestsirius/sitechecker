var gulp = require('gulp');
var concatCss = require('gulp-concat-css');
var cleanCSS = require('gulp-clean-css');
var sass = require('gulp-sass');
var es6transpiler = require('gulp-es6-transpiler');
var minify = require('gulp-minify');
var pump = require('pump');
var browserSync = require('browser-sync');

gulp.task ('css', function() {
  return gulp.src('css/main.sass')
    .pipe(sass())
    .pipe(concatCss("styles/main.min.css"))
    .pipe(cleanCSS({compatibility: 'ie8'}))
    .pipe(gulp.dest('out'))
    .pipe(browserSync.stream());
});

gulp.task ('design', function() {
    return gulp.src('css/design.sass')
        .pipe(sass())
        .pipe(concatCss("styles/design.min.css"))
        .pipe(cleanCSS({compatibility: 'ie8'}))
        .pipe(gulp.dest('out'))
        .pipe(browserSync.stream());
});

gulp.task ('js', function() {
  return gulp.src('js/*')
      .pipe(minify())
      .pipe(gulp.dest('out/js'))
      .pipe(browserSync.stream());
});

gulp.task ('img', function() {
  return gulp.src('img/*')
    .pipe(gulp.dest('out/img'))
    .pipe(browserSync.stream());
});

gulp.task ('img_design', function() {
    return gulp.src('img_design/*')
        .pipe(gulp.dest('out/img_design'))
        .pipe(browserSync.stream());
});

gulp.task('server', function() {
    browserSync({
        server: {
            baseDir: ''
        },
        port: 8088
    });

    gulp.watch(["img/*"], ['img']);
    gulp.watch(["img_design/*"], ['img_design']);
    gulp.watch(["css/*"], ['css','design']);
    gulp.watch(["js/*"], ['js']);
    gulp.watch("*.html").on('change', browserSync.reload);

});

gulp.task('default', ['server']);