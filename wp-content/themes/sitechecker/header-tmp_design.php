<!doctype html>
<html <?php language_attributes(); ?> class="no-js">
<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <?php
    $template_directory_uri = get_template_directory_uri();
    $front_second = url_to_postid( '/website-crawler/' );
    $front_third = url_to_postid( '/website-monitoring/' );
    $front_fourth = url_to_postid( '/on-page-seo-checker/' );
    $front_five = url_to_postid( '/seo-platform/' );
    $rank_checker = url_to_postid( '/rank-tracker/' );
    $backlinks_tracker = url_to_postid( '/backlinks-tracker/' );
    $white_label = url_to_postid( '/white-label/' );
    $metaDesign = get_post_meta_all(url_to_postid( '/header-footer-design/' ));
    global $wp;
    global $wp_query;
    $post = $wp_query->post;

    $today = getdate();
    $date = new DateTime();
    $dayOfYear = $today[yday];
    $monthOfYear = $today[mon];
    $weekOfYear = $date->format("W");

    if(is_page('/logout/')){
        var_dump('here');
        exit();
    }

    if ( is_user_logged_in() ) {
        $visitorCohortDay = get_user_meta(get_current_user_id(), 'visitorCohortDay', true);
        $visitorCohortWeek = get_user_meta(get_current_user_id(), 'visitorCohortWeek', true);
        $visitorCohortMonth = get_user_meta(get_current_user_id(), 'visitorCohortMonth', true);
        if(!$visitorCohortDay){
            update_user_meta( get_current_user_id(), 'visitorCohortDay', $dayOfYear);
        }
        if(!$visitorCohortWeek){
            update_user_meta( get_current_user_id(), 'visitorCohortWeek', $weekOfYear);
        }
        if(!$visitorCohortMonth){
            update_user_meta( get_current_user_id(), 'visitorCohortMonth', $monthOfYear);
        }
    }
    ?>

    <?php
    if ( is_user_logged_in() ) {
        $current_user = wp_get_current_user();
        $customerLoggedIn = 'Yes';
        $customerId = $current_user->ID;
        $customerEmail = $current_user->user_email;
        $customerMembershipStatus = $current_user->membership_level->name;
        $customerLifetimeOrders = $current_user->ExpirationMonth;
        $invoices = $wpdb->get_results("SELECT o.*, UNIX_TIMESTAMP(o.timestamp) as timestamp, l.name as membership_level_name FROM $wpdb->pmpro_membership_orders o LEFT JOIN $wpdb->pmpro_membership_levels l ON o.membership_id = l.id WHERE o.user_id = '$current_user->ID' ORDER BY timestamp DESC");
        $payment_month = 0;
        $payment_price = 0;
        ?>
        <?php
        foreach($invoices as $invoice)
        {
            $payment_month = $payment_month + 1;
            $payment_price = $payment_price +$invoice->total;
        }
        ?>
        <?php
    } else{
        $customerLoggedIn = 'No';
    }
    ?>

    <script>
        let visitorExistingUser = localStorage.getItem('visitorExistingCustomer'),
            visitorCohortDay = localStorage.getItem('visitorCohortDay'),
            visitorCohortWeek = localStorage.getItem('visitorCohortWeek'),
            visitorCohortMonth = localStorage.getItem('visitorCohortMonth');

        if(visitorExistingUser == null || visitorExistingUser !== 'Yes'){
            localStorage.setItem('visitorExistingCustomer', 'No');
        }

        if('<?=$customerLoggedIn?>' === 'Yes'){
            visitorCohortDay = '<?=$visitorCohortDay?>';
            visitorCohortWeek = '<?=$visitorCohortWeek?>';
            visitorCohortMonth = '<?=$visitorCohortMonth?>';
        }else{
            if(visitorCohortDay == null){
                localStorage.setItem('visitorCohortDay', '<?=$dayOfYear?>');
                visitorCohortDay = localStorage.getItem('visitorCohortDay');
            }
            if(visitorCohortWeek == null){
                localStorage.setItem('visitorCohortWeek', '<?=$weekOfYear?>');
                visitorCohortWeek = localStorage.getItem('visitorCohortWeek');
            }
            if(visitorCohortMonth == null){
                localStorage.setItem('visitorCohortMonth', '<?=$monthOfYear?>');
                visitorCohortMonth = localStorage.getItem('visitorCohortMonth');
            }
        }

        window.dataLayer = window.dataLayer || [];

        const USERID = '<?=$customerId?>';
        const USEREMAIL = '<?=$customerEmail?>';
        const USERMEMBERSHIPSTATUS = '<?=$customerMembershipStatus?>';
        const USERLIFETIMEORDERS = '<?=$payment_month?>';
        const USERLIFETIMEVALUE = '<?=$payment_price?>';

        window.dataLayer.push({
            'visitorExistingCustomer': localStorage.getItem('visitorExistingCustomer'),
            'customerLoggedIn': '<?=$customerLoggedIn?>',
            'customerId': '<?=$customerId?>',
            'customerMembershipStatus': '<?=$customerMembershipStatus?>',
            'customerLifetimeOrders': '<?=$payment_month?>',
            'customerLifetimeValue': '<?=$payment_price?>',
            'visitorCohortDay': visitorCohortDay,
            'visitorCohortWeek': visitorCohortWeek,
            'visitorCohortMonth': visitorCohortMonth,
        });

    </script>


    <title><?php
        if(is_category()){
            echo single_tag_title('', false);
        }else{
            the_title();
        }
        ?></title>
    <link rel="apple-touch-icon" sizes="180x180" href="<?= $template_directory_uri; ?>/out/img_design/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?= $template_directory_uri; ?>/out/img_design/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?= $template_directory_uri; ?>/out/img_design/favicon-16x16.png">
    <link rel="manifest" href="<?= $template_directory_uri; ?>/out/img_design/site.webmanifest">
    <link rel="mask-icon" href="<?= $template_directory_uri; ?>/out/img_design/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#00aba9">
    <meta name="theme-color" content="#ffffff">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <?php if(!is_category()){ ?>
        <meta name="robots" content="<?=get_field('noindex', $post->ID);?>"/>
    <?php } ?>

    <meta name="google-site-verification" content="eQF_wkzvhKYn757NRtDGzu-1PMBkEEsflHZs3QMHu_s" />
    <meta name="msvalidate.01" content="0FD73A58AA195EF7A0EC234993CBDA1E" />

    <meta name="yandex-verification" content="e0624a52482ea478" />

    <style>.async-hide { opacity: 0 !important} </style>
    <script>(function(a,s,y,n,c,h,i,d,e){s.className+=' '+y;h.start=1*new Date;            h.end=i=function(){s.className=s.className.replace(RegExp(' ?'+y),'')};            (a[n]=a[n]||[]).hide=h;setTimeout(function(){i();h.end=null},c);h.timeout=c;        })(window,document.documentElement,'async-hide','dataLayer',4000,            {'GTM-N2SCB2J':true});</script>
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push(
            {'gtm.start': new Date().getTime(),event:'gtm.js'}
        );var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window, document, 'script', 'dataLayer', 'GTM-N2SCB2J');</script>
    <!-- End Google Tag Manager -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700,800" rel="stylesheet">
    <link href="<?= $template_directory_uri; ?>/css/select2.min.css" rel="stylesheet">

    <?php if(is_page(3383)){ ?>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <?php }else{ ?>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
    <?php } ?>
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-slider/10.3.1/css/bootstrap-slider.min.css">

    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.4/css/select2.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.theme.default.min.css">
    <link rel="stylesheet"
          href="<?= $template_directory_uri; ?>/out/styles/prism.css">
    <link rel="stylesheet"
          href="<?= $template_directory_uri; ?>/out/styles/design.min.css?<?php echo mt_rand(100000, 999999); ?>">
  <link rel="stylesheet"
        href="<?= $template_directory_uri; ?>/stylesheets/style.css?<?php echo mt_rand(100000, 999999); ?>">
    <?php
    $current_locale = get_locale();

    $locales = ['en', 'de', 'ru', 'fr', 'it', 'nl', 'es', 'no', 'pt', 'sv', 'pl'];
    $postId = get_the_ID();
    wp_cache_delete($postId, 'posts');
    $mypost = WP_Post::get_instance($postId);

    $navClass = '';

    $default_link = get_permalink();
    $default_link = str_replace('/'.$current_locale.'/', '/en/', $default_link);

    foreach ($locales as $value) {
        if ('' == qtrans_use($value, $mypost->post_title, false)) {
            $navClass .= $value . " ";
        }

        if($value == 'en'){
            $link = get_permalink();
            $link = str_replace('/'.$current_locale.'/', '/', $link);
        } else{
            $link = qtrans_convertURL(get_permalink(), $value);
        }
        if (qtranxf_getLanguage() !== $value){
            if(qtrans_use($value, $mypost->post_title, false) !== ''){
                echo '<link hreflang="'.$value.'" href="'.$link.'" rel="alternate" />';
            }
        }
    }
    echo '<link hreflang="'.$current_locale.'" href="'.get_permalink().'" rel="alternate" /><link hreflang="x-default" href="'.$default_link.'" rel="alternate" />';

    ?>
    <?php if(is_front_page()){
        echo '<meta name="p:domain_verify" content="8e11d2ee0e8e18a9c305d89630004694"/>';
    }?>
<?php wp_head(); ?>
</head>
<?php if(is_front_page() || is_page($front_second) || is_page($front_third) || is_page($front_fourth) || is_page($front_five) || is_page($rank_checker) || is_page($backlinks_tracker) || is_page($white_label)) { ?>
    <body class="front__page">
<?php } else { ?>
    <body>
<?php }?>
<header>
    <?php if(is_front_page() || is_page($front_second) || is_page($front_third) || is_page($front_fourth) || is_page($front_five) || is_page($rank_checker) || is_page($backlinks_tracker) || is_page($white_label)) { ?>
    <div class="header header__fixed">
    <?php } else { ?>
        <div class="header">
    <?php } ?>
        <div class="wrapper">
            <?php if(is_front_page()) { ?>
                <div class="header__logo">
                    <img src="<?= $template_directory_uri; ?>/out/img_design/white_sitechecker.svg" alt="logo" title="logo">
                </div>
            <?php } else if(is_page($front_second) || is_page($front_third) || is_page($front_fourth) || is_page($front_five) || is_page($rank_checker) || is_page($backlinks_tracker) || is_page($white_label)) {?>
                <a href="<?=home_url();?>/" class="header__logo">
                    <img src="<?= $template_directory_uri; ?>/out/img_design/white_sitechecker.svg" alt="logo" title="logo">
                </a>
            <?php } else { ?>
                <a href="<?=home_url();?>/" class="header__logo">
                    <img src="<?= $template_directory_uri; ?>/out/img_design/logo.svg" alt="logo" title="logo">
                </a>
            <?php } ?>
            <div class="header__menu-mobile">
                <div class="mobile__close">
                    <span></span>
                    <span></span>
                </div>
                <div class="header__menu">
                    <ul>
                        <li class="menu-item-has-children">
                            <span><?=t($metaDesign['products']);?> <span class="arrow"><svg width="9" height="5" viewBox="0 0 9 5" fill="none" xmlns="http://www.w3.org/2000/svg">
<path fill-rule="evenodd" clip-rule="evenodd" d="M4.499 0L9 2.5V4.896L4.499 2.38L0 5V2.5L4.499 0Z" transform="translate(9 5) rotate(-180)" fill="#495c6a"/>
</svg></span></span>
                            <ul class="sub-menu products">
                                <li><a href="<?php echo get_permalink(5434); ?>">
                                        <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                             viewBox="0 0 486.742 486.742" style="enable-background:new 0 0 486.742 486.742;" xml:space="preserve">
<g>
    <g>
        <path d="M33,362.371v78.9c0,4.8,3.9,8.8,8.8,8.8h61c4.8,0,8.8-3.9,8.8-8.8v-138.8l-44.3,44.3
			C57.9,356.071,45.9,361.471,33,362.371z"/>
        <path d="M142,301.471v139.8c0,4.8,3.9,8.8,8.8,8.8h61c4.8,0,8.8-3.9,8.8-8.8v-82.3c-13.9-0.3-26.9-5.8-36.7-15.6L142,301.471z"/>
        <path d="M251,350.271v91c0,4.8,3.9,8.8,8.8,8.8h61c4.8,0,8.8-3.9,8.8-8.8v-167.9l-69.9,69.9
			C257,345.971,254.1,348.271,251,350.271z"/>
        <path d="M432.7,170.171l-72.7,72.7v198.4c0,4.8,3.9,8.8,8.8,8.8h61c4.8,0,8.8-3.9,8.8-8.8v-265.6c-2-1.7-3.5-3.2-4.6-4.2
			L432.7,170.171z"/>
        <path d="M482.6,41.371c-2.9-3.1-7.3-4.7-12.9-4.7c-0.5,0-1.1,0-1.6,0c-28.4,1.3-56.7,2.7-85.1,4c-3.8,0.2-9,0.4-13.1,4.5
			c-1.3,1.3-2.3,2.8-3.1,4.6c-4.2,9.1,1.7,15,4.5,17.8l7.1,7.2c4.9,5,9.9,10,14.9,14.9l-171.6,171.7l-77.1-77.1
			c-4.6-4.6-10.8-7.2-17.4-7.2c-6.6,0-12.7,2.6-17.3,7.2L7.2,286.871c-9.6,9.6-9.6,25.1,0,34.7l4.6,4.6c4.6,4.6,10.8,7.2,17.4,7.2
			s12.7-2.6,17.3-7.2l80.7-80.7l77.1,77.1c4.6,4.6,10.8,7.2,17.4,7.2c6.6,0,12.7-2.6,17.4-7.2l193.6-193.6l21.9,21.8
			c2.6,2.6,6.2,6.2,11.7,6.2c2.3,0,4.6-0.6,7-1.9c1.6-0.9,3-1.9,4.2-3.1c4.3-4.3,5.1-9.8,5.3-14.1c0.8-18.4,1.7-36.8,2.6-55.3
			l1.3-27.7C487,49.071,485.7,44.571,482.6,41.371z"/>
    </g>
</svg>
                                        <div class="products__description">
                                            <p><?=t($metaDesign['all_seo_platform']);?> <span class="new__platform">new</span></p>
                                            <span><?=t($metaDesign['all_seo_platform_description']);?></span>
                                        </div>
                                    </a>
                                </li>
                                <li><a href="<?php echo get_permalink(1389); ?>">
                                        <img src="<?= $template_directory_uri; ?>/out/img_design/audit__icon-active.svg"
                                             alt="products" title="products">
                                        <div class="products__description">
                                            <p><?=t($metaDesign['health_checker_title']);?></p>
                                            <span><?=t($metaDesign['health_checker_description']);?></span>
                                        </div>
                                    </a>
                                </li>
                                <li><a href="<?php echo get_permalink(3678); ?>">
                                        <img src="<?= $template_directory_uri; ?>/out/img_design/monitoring__icon-active.svg"
                                             alt="products" title="products">
                                        <div class="products__description">
                                            <p><?=t($metaDesign['monitor_title']);?></p>
                                            <span><?=t($metaDesign['monitor_description']);?></span>
                                        </div>
                                    </a>
                                </li>
                                <li><a href="<?php echo get_permalink(5221); ?>">
                                        <img src="<?= $template_directory_uri; ?>/out/img_design/check__icon-active.svg"
                                             alt="products" title="products">
                                        <div class="products__description">
                                            <p><?=t($metaDesign['audit_title']);?></p>
                                            <span><?=t($metaDesign['audit_description']);?></span>
                                        </div>
                                    </a>
                                </li>
                                <li><a href="<?php echo get_permalink(2833); ?>">
                                        <img src="<?= $template_directory_uri; ?>/out/img_design/link-symbol.svg"
                                             alt="products" title="products">
                                        <div class="products__description">
                                            <p><?=t($metaDesign['backlink_title']);?></p>
                                            <span><?=t($metaDesign['backlink_description']);?></span>
                                        </div>
                                    </a>
                                </li>
                                <li><a href="<?php echo get_permalink(5871); ?>">
                                        <img src="<?= $template_directory_uri; ?>/out/img_design/ranking.svg"
                                             alt="products" title="products">
                                        <div class="products__description">
                                            <p><?=t($metaDesign['website_rank_tracker']);?></p>
                                            <span><?=t($metaDesign['website_rank_tracker_description']);?></span>
                                        </div>
                                    </a>
                                </li>
                                <li><a href="<?php echo get_permalink(5869); ?>">
                                        <img src="<?= $template_directory_uri; ?>/out/img_design/backlink.svg"
                                             alt="products" title="products">
                                        <div class="products__description">
                                            <p><?=t($metaDesign['website_backlink_tracker']);?></p>
                                            <span><?=t($metaDesign['website_backlink_tracker_description']);?></span>
                                        </div>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li><a href="/knowledge-base/"><?=t($metaDesign['knowledge_base']);?></a></li>
                        <li><a href="/account/plans/"><?=t($metaDesign['pricing']);?></a></li>
                        <?php
                        if ( is_user_logged_in() ) {

                            if(!function_exists('pmpro_hasMembershipLevel') || !pmpro_hasMembershipLevel())
                            {
                                global $current_user;
                                pmpro_changeMembershipLevel(1, $current_user->ID);
                            }

                            $userViewPosts = get_user_meta( get_current_user_id(), 'user_view_posts', true );

                            if($userViewPosts == ''){
                                update_user_meta( get_current_user_id(), 'user_view_posts', array('0'));
                            }

                            if(is_single()){
                                if (!in_array($pageID, $userViewPosts)){
                                    array_push($userViewPosts, $pageID);
                                    update_user_meta( get_current_user_id(), 'user_view_posts', $userViewPosts);
                                    $userViewPosts = get_user_meta( get_current_user_id(), 'user_view_posts', true );
                                }
                            }

                            $args = array(
                                'numberposts' => 100,
                                'sort_order'   => 'DESC',
                                'sort_column' => 'date',
                                'post_type'    => 'post',
                                'post_status'  => 'publish'
                            );
                            $pages = get_posts( $args );
                            if(sizeof($pages)>0){
                                $showNews = 'flex';
                            }else{
                                $showNews = 'none';
                            }
                            $content = '';
                            $newsCount = get_user_meta( get_current_user_id(), 'news_count', true );
                            if($newsCount == ''){
                                update_user_meta( get_current_user_id(), 'news_count', sizeof($pages));
                                $bellPosts = sizeof($pages);
                            }else{
                                if($newsCount<sizeof($pages)){
                                    $updateClass = 'request';
                                    $updateNews = sizeof($pages);
                                    $bellPosts = sizeof($pages) - $newsCount;
                                }else if($newsCount === sizeof($pages)){
                                    $bellPosts = sizeof($pages);
                                }
                                else{
                                    $updateClass = 'non_request';
                                }
                            }
                            foreach( $pages as $key=>$post ){
                                $background = '';
                                if($bellPosts>0){
                                    for ($x = 0; $x < $bellPosts; $x++) {
                                        if($key == $x){
                                            $background = 'new';
                                        }
                                    }
                                }
                                if (in_array($post->ID, $userViewPosts)){
                                    $post_seen = 'seen';
                                }else{
                                    $post_seen = 'notSeen';
                                }
                                $content .= '<div class="user-bell_news '.$background .' '.$post_seen.'"><a href="'.get_the_permalink().'">'.get_the_title().'</a>';
                                if(has_excerpt()) {
                                    $content .= '<p>'.get_the_excerpt().'</p>';
                                }
                                $content .= '</div>';
                            }
                            wp_reset_postdata();
                            ?>
                            <li style="display:<?php echo $showNews ?>" class="user-bell <?php echo $updateClass?>" name="<?php echo $updateNews ?>">
                                <span class="user-bell-news">News <span class="arrow"><svg width="9" height="5" viewBox="0 0 9 5" fill="none" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" clip-rule="evenodd" d="M4.499 0L9 2.5V4.896L4.499 2.38L0 5V2.5L4.499 0Z" transform="translate(9 5) rotate(-180)" fill="#495c6a"/>
</svg></span></span><span class="bellPosts"><?php echo $bellPosts ?></span><div class="user-bell_list"><?php echo $content ?></div></li>
                        <?php } ?>
                    </ul>
                </div>
                <?php
                if ( is_user_logged_in() ) { ?>
                    <div class="header__user-block">
                        <a href="/tools/seo-platform/" class="header_dashboard seo__platform-header">
                            <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                 viewBox="0 0 486.742 486.742" style="enable-background:new 0 0 486.742 486.742;" xml:space="preserve">
<g>
    <g>
        <path d="M33,362.371v78.9c0,4.8,3.9,8.8,8.8,8.8h61c4.8,0,8.8-3.9,8.8-8.8v-138.8l-44.3,44.3
			C57.9,356.071,45.9,361.471,33,362.371z"/>
        <path d="M142,301.471v139.8c0,4.8,3.9,8.8,8.8,8.8h61c4.8,0,8.8-3.9,8.8-8.8v-82.3c-13.9-0.3-26.9-5.8-36.7-15.6L142,301.471z"/>
        <path d="M251,350.271v91c0,4.8,3.9,8.8,8.8,8.8h61c4.8,0,8.8-3.9,8.8-8.8v-167.9l-69.9,69.9
			C257,345.971,254.1,348.271,251,350.271z"/>
        <path d="M432.7,170.171l-72.7,72.7v198.4c0,4.8,3.9,8.8,8.8,8.8h61c4.8,0,8.8-3.9,8.8-8.8v-265.6c-2-1.7-3.5-3.2-4.6-4.2
			L432.7,170.171z"/>
        <path d="M482.6,41.371c-2.9-3.1-7.3-4.7-12.9-4.7c-0.5,0-1.1,0-1.6,0c-28.4,1.3-56.7,2.7-85.1,4c-3.8,0.2-9,0.4-13.1,4.5
			c-1.3,1.3-2.3,2.8-3.1,4.6c-4.2,9.1,1.7,15,4.5,17.8l7.1,7.2c4.9,5,9.9,10,14.9,14.9l-171.6,171.7l-77.1-77.1
			c-4.6-4.6-10.8-7.2-17.4-7.2c-6.6,0-12.7,2.6-17.3,7.2L7.2,286.871c-9.6,9.6-9.6,25.1,0,34.7l4.6,4.6c4.6,4.6,10.8,7.2,17.4,7.2
			s12.7-2.6,17.3-7.2l80.7-80.7l77.1,77.1c4.6,4.6,10.8,7.2,17.4,7.2c6.6,0,12.7-2.6,17.4-7.2l193.6-193.6l21.9,21.8
			c2.6,2.6,6.2,6.2,11.7,6.2c2.3,0,4.6-0.6,7-1.9c1.6-0.9,3-1.9,4.2-3.1c4.3-4.3,5.1-9.8,5.3-14.1c0.8-18.4,1.7-36.8,2.6-55.3
			l1.3-27.7C487,49.071,485.7,44.571,482.6,41.371z"/>
    </g>
</svg> <?=t($metaDesign['seo_platform']);?><span class="new__platform">new</span></a>
                        <div class="header_dashboard_container">
                            <a href="/tool/crawl-report/" class="header_dashboard">
                                <svg width="24" height="23" viewBox="0 0 24 23" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M10.2347 8.37864L8.70534 6.88655C9.1759 6.19789 9.41117 5.39446 9.41117 4.59103C9.41117 2.06596 7.29366 0 4.70559 0C2.11751 0 0 2.06596 0 4.59103C0 7.1161 2.11751 9.18207 4.70559 9.18207C5.52906 9.18207 6.35254 8.95251 7.05838 8.49341L8.5877 9.9855C8.82298 10.215 9.52881 10.6742 10.2347 9.9855C10.7052 9.52639 10.7052 8.83774 10.2347 8.37864ZM4.70559 6.88655C3.41155 6.88655 2.35279 5.85357 2.35279 4.59103C2.35279 3.3285 3.41155 2.29552 4.70559 2.29552C5.99962 2.29552 7.05838 3.3285 7.05838 4.59103C7.05838 5.85357 5.99962 6.88655 4.70559 6.88655Z" transform="translate(12.9403 12.6254)" fill="#CCCCCC"/>
                                    <path d="M17.646 0H1.1764C0.470559 0 0 0.459103 0 1.14776V21.8074C0 22.4961 0.470559 22.9552 1.1764 22.9552H13.7638C11.8816 21.6926 10.5876 19.6267 10.5876 17.2164C10.5876 13.4288 13.7638 10.3298 17.646 10.3298C17.9989 10.3298 18.4694 10.3298 18.8223 10.4446V1.14776C18.8223 0.459103 18.3518 0 17.646 0ZM9.41117 9.18207H3.52919C2.82335 9.18207 2.35279 8.60819 2.35279 8.03431C2.35279 7.34565 2.82335 6.88655 3.52919 6.88655H9.41117C10.117 6.88655 10.5876 7.34565 10.5876 8.03431C10.5876 8.60819 10.117 9.18207 9.41117 9.18207ZM15.2932 4.59103H3.52919C2.82335 4.59103 2.35279 4.01715 2.35279 3.44327C2.35279 2.75462 2.82335 2.29552 3.52919 2.29552H15.2932C15.999 2.29552 16.4696 2.75462 16.4696 3.44327C16.4696 4.01715 15.999 4.59103 15.2932 4.59103Z" fill="#CCCCCC"/>
                                </svg><?=t($metaDesign['dashboard']);?></a>
                            <div class="header_dashboard__list wp_header_dashboard__list"></div>
                        </div>
                        <div class="header__user">
                            <div class="header__user-info">
                                <div class="header__user-img">
                                    <img src="<?php echo get_avatar_url( $current_user->user_email, 32 )?>" alt="">
                                </div>
                                <span class="header__user-arrow">
                                    <svg width="9" height="5" viewBox="0 0 9 5" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path fill-rule="evenodd" clip-rule="evenodd" d="M4.499 0L9 2.5V4.896L4.499 2.38L0 5V2.5L4.499 0Z" transform="translate(9 5) rotate(-180)" fill="#495c6a"/>
                                </svg>
                                </span>
                            </div>
                            <div class="header__user-menu">
                                <?php
                                    $logout_url = get_permalink();
                                    $pageID = get_the_ID();
                                    if($pageID == 709){
                                        $logout_url = '/';
                                    } else if($pageID == 1101 || $pageID == 1099 || $pageID == 2792){
                                        $logout_url = '/website-crawler/';
                                    }
                                ?>
                                <ul>
                                    <li><p id="user__email" class="header__user-name"><?=$current_user->user_email?></p></li>
                                    <li><a href="/account/" class="header__user-account"><?=t($metaDesign['personal_area']);?></a></li>
                                    <li><a href="/plans/"><?=t($metaDesign['upgrade_plan']);?></a></li>
                                    <li><a href="<?php echo wp_logout_url( $logout_url ) ?>" id="logout"><?=t($metaDesign['log_out']);?></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                <?php } else{ ?>
                    <div class="header__user not__login <?php $pageID = get_the_ID(); if($pageID == 709){echo 'seo_report_page';} ?>">
                        <button class="signin__btn"><?=t($metaDesign['sign_in']);?></button>
                        <button class="signup__btn <?php if(is_page($front_second) || is_page($front_five)){echo 'audit__login';} elseif (is_page($front_third)){echo 'monitoring__login';} elseif (is_page($front_fourth) || is_page($rank_checker) || is_page($backlinks_tracker) || is_page($white_label)){echo 'checker__login';} ?>"><?=t($metaDesign['sign_up']);?></button>
                    </div>
                <?php } ?>
            </div>
            <?php if ( !is_user_logged_in() ) { ?>
                <button class="ipad signin__btn <?php if(is_page($front_second) || is_page($front_five)){echo 'audit__login';} elseif (is_page($front_third)){echo 'monitoring__login';} elseif (is_page($front_fourth) || is_page($rank_checker) || is_page(is_page($backlinks_tracker) || is_page($white_label))) echo 'checker__login'; ?>"><?=t($metaDesign['sign_in']);?></button>
            <?php } ?>
            <div class="header__menu-mob">
                <span></span>
                <span></span>
                <span></span>
            </div>
        </div>
    </div>
</header>