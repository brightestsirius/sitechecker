<!doctype html>
<html <?php language_attributes(); ?> class="no-js">
<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <?php
    $template_directory_uri = get_template_directory_uri();
    global $wp;
    global $wp_query;

    $post = $wp_query->post;

    ?>

    <?php
    if ( is_user_logged_in() ) {
        $current_user = wp_get_current_user();
        $customerLoggedIn = 'Yes';
        $customerId = $current_user->ID;
        $customerEmail = $current_user->user_email;
        $customerMembershipStatus = $current_user->membership_level->name;
        $customerLifetimeOrders = $current_user->ExpirationMonth;
        $invoices = $wpdb->get_results("SELECT o.*, UNIX_TIMESTAMP(o.timestamp) as timestamp, l.name as membership_level_name FROM $wpdb->pmpro_membership_orders o LEFT JOIN $wpdb->pmpro_membership_levels l ON o.membership_id = l.id WHERE o.user_id = '$current_user->ID' ORDER BY timestamp DESC");
        $payment_month = 0;
        $payment_price = 0;
        ?>
        <?php
        foreach($invoices as $invoice)
        {
            $payment_month = $payment_month + 1;
            $payment_price = $payment_price +$invoice->total;
        }
        ?>
        <?php
    } else{
        $customerLoggedIn = 'No';
    }
    ?>

    <script>
        let visitorExistingUser = localStorage.getItem('visitorExistingCustomer');

        if(visitorExistingUser == null || visitorExistingUser !== 'Yes'){
            localStorage.setItem('visitorExistingCustomer', 'No');
        }

        window.dataLayer = window.dataLayer || [];

        const USERID = '<?=$customerId?>';
        const USEREMAIL = '<?=$customerEmail?>';
        const USERMEMBERSHIPSTATUS = '<?=$customerMembershipStatus?>';
        const USERLIFETIMEORDERS = '<?=$payment_month?>';
        const USERLIFETIMEVALUE = '<?=$payment_price?>';

        window.dataLayer.push({
            'visitorExistingCustomer': localStorage.getItem('visitorExistingCustomer'),
            'customerLoggedIn': '<?=$customerLoggedIn?>',
            'customerId': '<?=$customerId?>',
            'customerMembershipStatus': '<?=$customerMembershipStatus?>',
            'customerLifetimeOrders': '<?=$payment_month?>',
            'customerLifetimeValue': '<?=$payment_price?>'
        });

    </script>


    <title><?php
        if(is_category()){
            single_cat_title();
        }else{
            the_title();
        }
        ?></title>
    <link href="<?=$template_directory_uri;?>/out/img/favicon.ico" rel="icon" type="image/x-icon">
    <link href="<?=$template_directory_uri;?>/out/img/favicon-32x32.png" rel="icon" type="image/png" sizes="32x32">
    <link href="<?=$template_directory_uri;?>/out/img/favicon-16x16.png" rel="icon" type="image/png" sizes="16x16">
    <link href="<?=$template_directory_uri;?>/out/img/manifest.json" rel="manifest">
    <link href="<?=$template_directory_uri;?>/out/img/safari-pinned-tab.svg" rel="mask-icon" color="#5bbad5">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" rel="stylesheet">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css">

    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.4/css/select2.min.css" rel="stylesheet">

    <link rel="stylesheet" href="<?=$template_directory_uri;?>/out/styles/main.min.css?<?php echo mt_rand(100000,999999); ?>">

    <meta name="robots" content="<?=get_field('noindex', $post->ID);?>"/>

    <meta name="google-site-verification" content="eQF_wkzvhKYn757NRtDGzu-1PMBkEEsflHZs3QMHu_s" />
    <meta name="msvalidate.01" content="0FD73A58AA195EF7A0EC234993CBDA1E" />

    <meta name="yandex-verification" content="e0624a52482ea478" />

    <!--Start of Zendesk Chat Script-->
    <script>
        window.$zopim||(function(d,s){var z=$zopim=function(c){
            z._.push(c)},$=z.s=
            d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
        _.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute('charset','utf-8');
            $.src='https://v2.zopim.com/?2aNK8BsvxtNwghN0d6joK2L7A9XVux9U';z.t=+new Date;$.
                type='text/javascript';e.parentNode.insertBefore($,e)})(document,'script');
    </script>
    <!--End of Zendesk Chat Script-->
    <script>
        $zopim(function() {$zopim.livechat.setName($zopim.livechat.getName() + ' (SITECHECKER.PRO)');});
    </script>
    <style>.async-hide { opacity: 0 !important} </style>
    <script>(function(a,s,y,n,c,h,i,d,e){s.className+=' '+y;h.start=1*new Date;
            h.end=i=function(){s.className=s.className.replace(RegExp(' ?'+y),'')};
            (a[n]=a[n]||[]).hide=h;setTimeout(function(){i();h.end=null},c);h.timeout=c;
        })(window,document.documentElement,'async-hide','dataLayer',4000,
            {'GTM-PCZVGX4':true});</script>
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push(
            {'gtm.start': new Date().getTime(),event:'gtm.js'}
        );var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window, document, 'script', 'dataLayer', 'GTM-TLCC7S4');</script>

    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push(
            {'gtm.start': new Date().getTime(),event:'gtm.js'}
        );var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window, document, 'script', 'dataLayer', 'GTM-N2SCB2J');</script>
    <!-- End Google Tag Manager -->
    <?php
    $current_locale = get_locale();

    $locales = ['en', 'de', 'ru', 'fr', 'it', 'nl', 'es', 'no', 'pt', 'sv'];
    $postId = get_the_ID();
    wp_cache_delete($postId, 'posts');
    $mypost = WP_Post::get_instance($postId);

    $navClass = '';

    $default_link = get_permalink();
    $default_link = str_replace('/'.$current_locale.'/', '/en/', $default_link);

    foreach ($locales as $value) {
        if ('' == qtrans_use($value, $mypost->post_title, false)) {
            $navClass .= $value . " ";
        }

        if($value == 'en'){
            $link = get_permalink();
            $link = str_replace('/'.$current_locale.'/', '/', $link);
        } else{
            $link = qtrans_convertURL(get_permalink(), $value);
        }
        if (qtranxf_getLanguage() !== $value){
            if(qtrans_use($value, $mypost->post_title, false) !== ''){
                echo '<link hreflang="'.$value.'" href="'.$link.'" rel="alternate" />';
            }
        }
    }
    echo '<link hreflang="'.$current_locale.'" href="'.get_permalink().'" rel="alternate" /><link hreflang="x-default" href="'.$default_link.'" rel="alternate" />';

    ?>

    <?php if(is_front_page()){
        echo '<meta name="p:domain_verify" content="8e11d2ee0e8e18a9c305d89630004694"/>';
    }?>

<style>
.home_description .wrapper {
    flex-direction: row-reverse;
}
</style>

    <?php wp_head(); ?>
</head>
<body>
<header>
    <div class="header <?=((is_front_page()) || is_page('technical-seo-audit-2') || is_page('technical-seo-audit') ? 'home' : '');?>">
        <div class="wrapper">
            <div class="header_logo">
                <a href="<?=home_url();?>/">
                    <p>sitechecker<span>.pro</span></p><span class="header_logo-beta">beta</span>
                </a>
            </div>
            <ul id="locale_seo">
                <?php
                $locales = ['en', 'de', 'ru', 'fr', 'it', 'nl', 'es', 'no', 'pt', 'sv'];
                $postId = get_the_ID();
                wp_cache_delete($postId, 'posts');
                $mypost = WP_Post::get_instance($postId);

                $navClass = '';
                foreach ($locales as $value) {
                    if ('' == qtrans_use($value, $mypost->post_title, false)) {
                        $navClass .= $value. " ";
                    }

                    if($value == 'en'){
                        $link = get_permalink();
                        $link = str_replace('/'.$current_locale.'/', '/en/', $link);
                    } else{
                        $link = qtrans_convertURL(get_permalink(), $value);
                    }

                    if (qtranxf_getLanguage() !== $value){
                        if(qtrans_use($value, $mypost->post_title, false) !== ''){
                            echo '<li><a data-lang="'.$value.'" href="'.$link.'" itemprop="sameAs"><span>'.$link.'</span></a></li>';
                        }
                    }
                }
                ?></ul>
            <?php
            $meta = get_post_meta_all(get_option('page_on_front'));
            ?>
            <div class="header_btn">
                <div class="header_links">
                    <div class="header_links_btn">
                        <ul>
                            <li class="menu-item-has-children">
                                <span><?php echo t($meta['title_1']);?></span>
                                <ul class="sub-menu products">
                                    <li><a href="<?=home_url();?>/">
                                            <img src="<?= $template_directory_uri; ?>/out/img/check__icon-active.svg"
                                                 alt="products" title="products">
                                            <div class="products__description">
                                                <p><?php echo t($meta['menu1.1.title']);?></p>
                                                <span><?php echo t($meta['menu1.1.description']);?></span>
                                            </div>
                                        </a></li>
                                    <li><a href="/website-crawler/">
                                            <img src="<?= $template_directory_uri; ?>/out/img/audit__icon-active.svg"
                                                 alt="products" title="products">
                                            <div class="products__description">
                                                <p><?php echo t($meta['menu1.2.title']);?></p>
                                                <span><?php echo t($meta['menu1.2.description']);?></span>
                                            </div>
                                        </a></li>
                                    <li><a href="/website-monitoring/">
                                            <img src="<?= $template_directory_uri; ?>/out/img/monitoring__icon-active.svg"
                                                 alt="products" title="products">
                                            <div class="products__description">
                                                <p><?php echo t($meta['menu1.3.title']);?></p>
                                                <span><?php echo t($meta['menu1.3.description']);?></span>
                                            </div>
                                        </a></li>
                                </ul>
                            </li>
                            <li class="menu-item-has-children">
                                <span><?php echo t($meta['title_2']);?></span>
                                <ul class="sub-menu products resources">
                                    <li><a href="/knowledge-base/"><?php echo t($meta['menu2.1.title']);?></a></li>
                                    <li><a href="https://sitechecker.zendesk.com/hc/en-us" target="_blank"><?php echo t($meta['menu2.2.title']);?></a></li>
                                </ul>
                            </li>
                            <li><a href="/plans/"><?php echo t($meta['title_3']);?></a></li>
                        </ul>
                    </div>
                    <?php
                    if ( is_user_logged_in() ) {

                        $current_user = wp_get_current_user();
                        $logout_url = get_permalink();
                        $pageID = get_the_ID();
                        if($pageID == 709){
                            $logout_url = '/';
                        } else if($pageID == 1101 || $pageID == 1099 || $pageID == 2792){
                            $logout_url = '/website-crawler/';
                        }

                        $userViewPosts = get_user_meta( get_current_user_id(), 'user_view_posts', true );

                        if($userViewPosts == ''){
                            update_user_meta( get_current_user_id(), 'user_view_posts', array('0'));
                        }

                        if(is_single()){
                            if (!in_array($pageID, $userViewPosts)){
                                array_push($userViewPosts, $pageID);
                                update_user_meta( get_current_user_id(), 'user_view_posts', $userViewPosts);
                                $userViewPosts = get_user_meta( get_current_user_id(), 'user_view_posts', true );
                            }
                        }

                        $args = array(
                            'numberposts' => 100,
                            'sort_order'   => 'DESC',
                            'sort_column' => 'date',
                            'post_type'    => 'post',
                            'post_status'  => 'publish'
                        );
                        $pages = get_posts( $args );
                        if(sizeof($pages)>0){
                            $showNews = 'flex';
                        }else{
                            $showNews = 'none';
                        }
                        $content = '';
                        $newsCount = get_user_meta( get_current_user_id(), 'news_count', true );
                        if($newsCount == ''){
                            update_user_meta( get_current_user_id(), 'news_count', sizeof($pages));
                            $bellPosts = sizeof($pages);
                        }else{
                            if($newsCount<sizeof($pages)){
                                $updateClass = 'request';
                                $updateNews = sizeof($pages);
                                $bellPosts = sizeof($pages) - $newsCount;
                            }else if($newsCount === sizeof($pages)){
                                $bellPosts = sizeof($pages);
                            }
                            else{
                                $updateClass = 'non_request';
                            }
                        }
                        foreach( $pages as $key=>$post ){
                            $background = '';
                            if($bellPosts>0){
                                for ($x = 0; $x < $bellPosts; $x++) {
                                    if($key == $x){
                                        $background = 'new';
                                    }
                                }
                            }
                            if (in_array($post->ID, $userViewPosts)){
                                $post_seen = 'seen';
                            }else{
                                $post_seen = 'notSeen';
                            }
                            $content .= '<div class="user-bell_news '.$background .' '.$post_seen.'"><a href="'.get_the_permalink().'">'.get_the_title().'</a>';
                            if(has_excerpt()) {
                                $content .= '<p>'.get_the_excerpt().'</p>';
                            }
                            $content .= '</div>';
                        }
                        wp_reset_postdata();

                        echo '<div style="display:'.$showNews.';" class="user-bell '.$updateClass.'" name="'.$updateNews.'">News<span class="bellPosts">'.$bellPosts.'</span><div class="user-bell_list">'.$content.'</div></div><a href="/crawl-report/"><span>'.t($meta["user.header.crawlreport"]).'</span></a><div class="header_user-block"><div class="header_user"><span>'.t($meta['user.header.profile']).'</span></div><div class="header_user-info"><span class="user_email-header">'.$current_user->user_email.'</span><a href="/account/"><span>'.t($meta['user.header.area']).'</span></a><a href="/affiliate-report/"><span>Affiliate Report</span></a><a href="/plans/"><span>'.t($meta['user.header.plan']).'</span></a><a href="'.wp_logout_url( $logout_url ).'" class="logout_url" id="logout"><span>'.t($meta['log_out']).'</span></a></div></div>';

                    } else{
                        echo '<a class="signin header_login">'.t($meta['log_in']).'</a>';
                    }
                    ?>
                    <?php
                    if ( !is_user_logged_in() ) {
                        if(is_front_page()){
                            echo '<a class="signup" style="display: none">'.t($meta['sign_up']).'</a>';
                        } else{
                            echo '<a class="signup">'.t($meta['sign_up']).'</a>';
                        }
                    }
                    ?>
                </div>

                <ul class="language_menu">
                    <li id="menu-item-81">
                        <a title="<?php echo $current_locale ?>" href="#"></a>
                        <ul class="sub-menu">
                            <?php
                            $locales = ['en', 'de', 'ru', 'fr', 'it', 'nl', 'es', 'no', 'pt', 'sv'];
                            $postId = get_the_ID();
                            wp_cache_delete($postId, 'posts');
                            $mypost = WP_Post::get_instance($postId);

                            $base_url = ( isset($_SERVER['HTTPS']) && $_SERVER['HTTPS']=='on' ? 'https' : 'http' ) . '://' .  $_SERVER['HTTP_HOST'];
                            $url = $base_url . $_SERVER["REQUEST_URI"];

                            $navClass = '';
                            foreach ($locales as $value) {
                                if ('' == qtrans_use($value, $mypost->post_title, false)) {
                                    $navClass .= $value. " ";
                                }

                                if($value == 'en'){
                                    $link = $url;
                                    $link = str_replace('/'.$current_locale.'/', '/', $link);
                                } else{
                                    $link = qtrans_convertURL($url, $value);
                                }
                                if (qtranxf_getLanguage() !== $value){
                                    if(qtrans_use($value, $mypost->post_title, false) !== ''){
                                        echo '<li><a title="'.$value.'" href="'.$link.'"></a></li>';
                                    }
                                }
                            }
                            ?>
                        </ul>
                    </li>
                </ul>
                <div id="burger">
                    <span class="top-bar"></span>
                    <span class="middle-bar"></span>
                    <span class="bottom-bar"></span>
                </div>
            </div>
        </div>
    </div>
</header>