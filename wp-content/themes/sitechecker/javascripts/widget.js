(function () {
	/**
	 * Dynamically connect widget stylesheets
	 *
	 * @param {string} host - base url of wp-theme
	 */
	function connectWidgetStyles(host) {
		const scwStylesId = 'scw-styles';
		const fcss = document.getElementsByTagName('link')[0];
		if (document.getElementById(scwStylesId)) return;
		let css = document.createElement('link');
		css.id = scwStylesId;
		css.href = host + '/stylesheets/widget-styles.css?v=' + Math.random().toString();
		css.rel = 'stylesheet';
		css.type = 'text/css';
		fcss.parentNode.insertBefore(css, fcss);
	}

	/**
	 *
	 * @param {string} link
	 */
	function openLinkInNewTab(link) {
		const a = document.createElement('a');
		a.href = link;
		a.target = '_blank';
		a.style.position = 'absolute';
		a.style.left = '-9999px';
		document.querySelector('body').appendChild(a);
		a.click();
		a.remove();
	}

	/**
	 * Returns widget wrapper element
	 *
	 * @param {string} color - widget color
	 * @param {string} host - base url of wp-theme
	 * @param {boolean} isOpenInIframe
	 *
	 * @returns {HTMLElement}
	 */
	function createWrapper(color, host, isOpenInIframe) {
		const _logoSVG = document.createElement('img');
		_logoSVG.src = host + '/images/widget-logo.svg';
		const _logo = document.createElement('p');
		_logo.classList.add('scw__logo');
		_logo.appendChild(_logoSVG);
		const _el = document.createElement('div');
		_el.classList.add('scw');
		if (isOpenInIframe) _el.classList.add('iframe');
		_el.style.backgroundColor = color;
		_el.appendChild(_logo);

		return _el;
	}

	/**
	 * Returns widget form element
	 *
	 * @param {boolean} isOpenInIframe
	 * @param {string} host - base url of wp-theme
	 * @param {function} submitFormCb
	 *
	 */
	function createForm(isOpenInIframe, host, submitFormCb) {
		const _wrapperEl = document.createElement('div');
		_wrapperEl.classList.add('scw__input-wrapper');

		const _input = document.createElement('input');
		_input.classList.add('scw__input');
		_input.placeholder = 'Enter URL for analysis';
		_input.size = 30;
		_input.pattern = '(https?://)?([a-zA-Z0-9-_]+\\.)*[a-zA-Z0-9][a-zA-Z0-9-_]+\\.([a-zA-Z]){2,11}?(\\/)?.*';
		_input.required = true;

		const _form = document.createElement('form');
		_form.classList.add('scw__form');
		_form.addEventListener('submit', function (e) {
			e.preventDefault();

			submitFormCb(_input.value);
		});

		const _btnText = document.createElement('SPAN');
		_btnText.textContent = 'Check';
		const _arrowRight = document.createElement('img');
		_arrowRight.src = host + '/images/arrow-right.svg';
		const _btn = document.createElement('button');
		_btn.type = 'submit';
		_btn.classList.add('scw__submit-btn');
		_btn.appendChild(_btnText);
		_btn.appendChild(_arrowRight);

		_form.appendChild(_input);
		_form.appendChild(_btn);
		_wrapperEl.appendChild(_form);

		return _wrapperEl;
	}

	/**
	 *
	 * @returns {HTMLElement} - preloader element
	 */
	function createPreloader() {
		const _wrap = document.createElement('div');
		_wrap.classList.add('scw__preloader');
		_wrap.style.display = 'none';

		const _el = document.createElement('div');
		_el.classList.add('scw__preloader-ring');

		new Array(3)
			.fill(document.createElement('div'))
			.forEach(el => _el.appendChild(el));
		_wrap.appendChild(_el);

		return _wrap;
	}

	/**
	 * @param {string} url - page check Url
	 * @param {string} lang - report language
	 * @param {string} host - sitechecker server theme url
	 *
	 * @returns {HTMLElement} - iframe element
	 */
	function createCheckPageIframe(url, lang, host) {
		const _iframe = document.createElement('iframe');
		const baseUrl = /^https?:\/\/test/.test(host) ? 'https://test.sitechecker.pro/' : 'https://sitechecker.pro/';
		const iFrameSource = baseUrl + lang + '/seo-report/' + url ;

		Object.assign(_iframe, {
			width: '100%',
			height: '100%',
			frameborder: '0',
			allowfullscreen: true,
			name: 'iframe_sitechecker',
			src: iFrameSource,
		});
		Object.assign(_iframe.style, {
			border: 'none',
			overflow: 'hidden',
			height: '100%',
			width: '100%',
			maxHeight: '80vh',
		});

		return _iframe;
	}

	function generateWidget(rootElement, isOpenInIframe) {
		if (!rootElement) {
			setTimeout(function () {
				throw new Error('Can not find sitechecker widget root element');
			});
			return;
		}
		rootElement.classList.add('cf');
		rootElement.style.position = 'relative';

		const host = rootElement.dataset.host;

		connectWidgetStyles(host);

		const widgetColor = rootElement.dataset.color;


		const preloader = createPreloader();
		const formWrapper = createWrapper(widgetColor, host, isOpenInIframe);
		const form = createForm(isOpenInIframe, host, onFormSubmit);


		formWrapper.appendChild(form);
		rootElement.appendChild(formWrapper);
		rootElement.appendChild(preloader);

		function onFormSubmit(url) {
			if (isOpenInIframe) {
				/** Show Preloader */
				preloader.style.display = 'block';

				const lang = rootElement.dataset.language;
				const iframe = createCheckPageIframe(url, lang, host);

				/** Hide Preloader after iframe loading*/
				iframe.addEventListener('load', function () {
					preloader.style.display = 'none';
					rootElement.classList.add('iframe-widget-wrapper');
				});

				/** add iframe to DOM */
				rootElement.appendChild(iframe);

				/** Hide Preloader */
				formWrapper.remove();
			} else {
				const linkToOpen = 'https://sitechecker.pro/seo-report/' + url;
				openLinkInNewTab(linkToOpen);
			}
		}
	}

	/**
	 * Main script for generation widget
	 */
	const initSCW = function () {
		const rootElement = document.getElementById('sitechecker-widget-root');
		if (rootElement) generateWidget(rootElement, false);

		const iFrameWidgetRootElement = document.getElementById('sitechecker-iframe-widget-root');
		if (iFrameWidgetRootElement) generateWidget(iFrameWidgetRootElement, true);
	};

	const idDOMReady = ['complete', 'loaded', 'interactive'].indexOf(document.readyState) !== -1;

	if (idDOMReady) {
		initSCW();
	} else {
		document.addEventListener('DOMContentLoaded', initSCW);
	}
}());