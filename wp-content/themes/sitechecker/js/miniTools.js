let currentValidationGroup,
    currentValidationIndex,
    currentCheckIndex,
    fullPath = `${document.location.pathname}`.split('/'),
    landingPath = fullPath[2] !== '' && fullPath[2] !== '#' ? fullPath[2] : fullPath[1];


$(document).ready(function () {
    $('#minitool_input').attr('placeholder', trans['Minitools.Robots.Placeholder']);
    $('#minitool-span').html(trans['Minitools.Button']);

    switch (landingPath) {
        case 'robots-tester':
            currentValidationGroup = 3;
            currentValidationIndex = 24;
            break;
        case '404-error':
            currentValidationGroup = 3;
            currentValidationIndex = 21;
            break;
        case 'what-is-favicon':
            currentValidationGroup = 2;
            currentValidationIndex = 16;
            break;
        case 'what-is-url':
            currentValidationGroup = 3;
            currentValidationIndex = 26;
            break;
        case 'h1-tag':
            currentValidationGroup = 1;
            currentValidationIndex = 20;
            break;
        case 'meta-title':
            currentValidationGroup = 1;
            currentValidationIndex = 1;
            break;
        case 'meta-tag-description':
            currentValidationGroup = 1;
            currentValidationIndex = 2;
            break;
        case 'http-vs-https':
            currentValidationGroup = 3;
            currentValidationIndex = 23;
            break;
        case 'www-vs-non-www':
            currentValidationGroup = 3;
            currentValidationIndex = 27;
            break;
        case 'canonical-url':
            currentValidationGroup = 3;
            currentValidationIndex = 5;
            break;
        case 'internal-links':
            currentValidationGroup = 4;
            currentValidationIndex = 13;
            break;
        case 'alt-tags':
            currentValidationGroup = 2;
            currentValidationIndex = 17;
            break;
        case 'http-status-codes':
            currentValidationGroup = 1;
            currentValidationIndex = 25;
            break;
        case 'redirect-checker':
            currentValidationGroup = 1;
            currentValidationIndex = 25;
            break;
        case 'pagination':
            currentValidationGroup = 3;
            currentValidationIndex = 7;
            break;
        case 'external-links':
            currentValidationGroup = 3;
            currentValidationIndex = 11;
            break;
        case 'page-size':
            currentValidationGroup = 1;
            currentValidationIndex = 22;
            break;
        case 'google-index':
            currentValidationGroup = 3;
            currentValidationIndex = 8;
            break;
        case 'speed-test':
            currentValidationGroup = 5;
            currentValidationIndex = [18, 19];
            $('#minitool-title').html(trans['Minitools.Speed.Title']);
            break;
        case 'google-cache':
            currentValidationGroup = 1;
            currentValidationIndex = 31;
            break;
        case 'backlinks-generator':
            currentValidationGroup = 1;
            currentValidationIndex = 32;
            break;
        case 'rank-checker':
            currentValidationGroup = 1;
            currentValidationIndex = 33;
            break;
        case 'what-is-cms':
            currentValidationGroup = 1;
            currentValidationIndex = 35;
            break;
        case 'traffic-checker':
            currentValidationGroup = 1;
            currentValidationIndex = 34;
            break;
    }

    function getMinitoolDomain(validationIndex){
        let result;
        switch (validationIndex) {
            case 0:
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
            case 8:
                result = 'api1.' + host;
                break;

            case 9:
            case 10:
            case 11:
            case 12:
            case 13:
            case 14:
            case 15:
            case 16:
            case 17:
                result = 'api2.' + host;
                break;
            case 18:
            case 19:
            case 20:
            case 21:
            case 22:
            case 23:
            case 24:
            case 25:
            case 26:
            case 27:
                result = 'api3.' + host;
                break;
            default:
                result = 'api3.' + host;
        }

        return document.location.protocol + '//' + result;
    }


    function executeCurrentCheckQuery(query, validURL) {
        let isLastIteration = false;

            if (Array.isArray(currentValidationIndex)) {
            currentValidationIndex.forEach(function (index){
                isLastIteration = currentValidationIndex[currentValidationIndex.length - 1] === index;
                validateCurrentCheck(index, query, isLastIteration, validURL);
            })
        } else {
            isLastIteration = true;
            validateCurrentCheck(currentValidationIndex, query, isLastIteration, validURL);
        }
    }

    function validateCurrentCheck(currentValidationIndex, query, isLastIteration, validURL){
        renderValidationTemplate(currentValidationIndex);

        let protocol = `${document.location.protocol}`,
            hostname = `${document.location.hostname}`,
            url = $('#minitool_input').val(),
            fullCheckHref = protocol + `//` + hostname + `/tool/create_user_domain/?url=` + validURL,
            domain = Array.isArray(currentValidationIndex) ? getMinitoolDomain(currentValidationIndex[1]) : getMinitoolDomain(currentValidationIndex),
            results = $('#result-template .result');

        $.ajax({
            url: domain + `/api/v1/full/${currentValidationIndex}/${query}`,
            method: 'GET',
            dataType: 'json',
            xhrFields: {
                withCredentials: true
            },
            crossDomain: true,
            success: function(response){
                let params = {
                    0: currentValidationGroup,
                    1: currentValidationIndex,
                    2: query,
                    3: response,
                };
                $('#minitool_check-landing .loading').css('display','none');
                $('#minitool_check-landing span').fadeIn();
                if(currentValidationIndex === 33){
                    $('#result-container').parent().css('width','90%');
                    $('.results-table').empty();
                    let tableRow = '';
                    $.each(response.data['checks'], function( index, value ) {
                        tableRow = tableRow+'<tr>' +
                            '<td>'+value['title']['key']+'</td>' +
                            '<td>'+value['title']['position']+'</td>' +
                            '<td><a href="'+value['title']['url']+'" target="_blank">'+value['title']['url']+'</a></td>' +
                            '<td>'+value['title']['results_count']+'</td>' +
                            '<td>'+value['title']['seatch_volume']+'</td>' +
                            '<td>'+value['title']['competition']+'</td>' +
                            '<td>'+value['title']['cpc']+'</td>' +
                            '<td>'+value['title']['country_code']+'</td>' +
                            '<td>'+value['title']['language']+'</td>' +
                            '</tr>';
                    });
                    results.append('<div class="validation__param_block">' +
                        '<table class="results-table" id="result-33">' +
                        '<tbody>' +
                        '<tr>' +
                        '<th>Key <div class="tooltip">Keyword in SERP</div></th>' +
                        '<th>Position <div class="tooltip">Position in SERP</div></th>' +
                        '<th>URL <div class="tooltip">Relevant full URL in the SERP</div></th>' +
                        '<th>Results count <div class="tooltip">Total number of results in the SERP</div></th>' +
                        '<th>Search volume <div class="tooltip">Average number of searches per month</div></th>' +
                        '<th>Competition <div class="tooltip">Google AdWords competition value</div></th>' +
                        '<th>CPC <div class="tooltip">Cost per click from Google AdWords</div></th>' +
                        '<th>Country code <div class="tooltip">ISO country code</div></th>' +
                        '<th>Language</th>' +
                        '</tr>' + tableRow +
                        '</tbody></table></div>')
                }else if(currentValidationIndex === 35){
                    $('#result-template .result').empty();
                    results.append('<div id="result-35">' +
                        '<div class="validation__param-body">' +
                        '<div class="validation__param_block">' +
                        '<div class="title"><p class="name">Website CMS checker</p>' +
                        '<div class="value">'+ response.data['checks'][0]['title'] +
                        '</div>' +
                        '</div>' +
                        '</div>' +
                        '</div>' +
                        '</div>' +
                        '</div>');
                }else{
                    renderResult(params, true, true);
                }
                if(isLastIteration){
                    $('.container__results').append('<div class="full-check-href">' +
                        '<a href="' + fullCheckHref + '" name="'+url+'">' + trans['Show more'] + '<a></div>');
                    $('.loader').hide();
                    let btn = $('#mini__tool-form button');
                    btn.find('.arrow').fadeIn();
                    btn.find('.loading').css('display','none');

                    let resultContainer = $('#result-container'),
                        destination = (resultContainer.offset().top)-50;

                    jQuery("html:not(:animated),body:not(:animated)").animate({
                        scrollTop: destination
                    }, 800);
                }
            },
            error: function (jqXHR, textStatus) {
                renderFail(currentValidationIndex, textStatus, 0);
            }
        });
    }

    $('body').on('click', '.full-check-href a', function (e) {
        let url = $(this).attr('name');
        if($('.header__user').hasClass('not__login')){
            e.preventDefault();
            $('.signin__btn').click();
            $('.tml-submit-wrap input[name="redirect_to"]').val('/tool/create_user_domain/?url='+url);
        }
    });

    $('#mini__tool-form').submit(function (e) {
        e.preventDefault();

        $('#result-container').show();

        $(this).removeClass('invalid');
        let form = $(this),
            btn = $(this).find('button'),
            apiPath = "/wp-admin/admin-ajax.php",
            url = $('#minitool_input').val();
        resetResults();
        form.removeClass('no_valid');
        $('.full-check-href').remove();
        $('#minitool_input').blur();
        if(url === ''){
            form.addClass('no_valid');
        } else {
            btn.find('.arrow').css('display', 'none');
            btn.find('.loading').fadeIn();
            $.ajax({
                url: apiPath,
                type: "POST",
                dataType: "JSON",
                data: {
                    action: 'crawler_validate_domain_request',
                    url: url
                },
                success: function success(data) {
                    if (data.status === false) {
                        $.each( data.requestLog, function( key, value ) {
                            if(value.statusCode === 301){
                                executeCurrentCheckQuery(url, value.url);
                                return false;
                            }else{
                                $('#minitool_input').val("").attr('placeholder', trans['Minitools.Error.Placeholder']);
                                $('#mini__tool-form').addClass('invalid');
                                $('#result-container').hide();
                                btn.find('.arrow').fadeIn();
                                btn.find('.loading').css('display', 'none');
                            }
                        });
                    }else {
                        executeCurrentCheckQuery(url, data.valid);
                    }
                }
            })
        }
    });
});



