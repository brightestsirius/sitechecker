let errorStorage      = 0,
    warningStorage    = 0,
    noticeStorage     = 0,
    passedStorage     = 18,
    evaluationStorage = 100,
    requestsStorage   = [],
    validationsArray = [],
    validationsName = [
        'parameterInformation',
        'parameterTitle',
        'parameterDescription',
        'parameterH1H6',
        'parameterContent',
        'parameterCanonicallink',
        'parameterAlternatelink',
        'parameterPagination',
        'parameterIndexation',
        'parameterVulnerability',
        'parameterBot',
        'parameterExternallinks',
        'parameterSubdomainlinks',
        'parameterInternallinks',
        'parameterMobileSpeed',
        'parameterDesktopSpeed',
        'parameterFavicon',
        'parameterImages'
    ],
    siteUrl;

const debugMode = false;
const isMtfIframe = window.location.pathname.slice(0,4) === '/mtf';
const host = document.location.host || document.location.hostname;

const loader         = $('.cprogress__loader'),
    circle           = $('.cprogress__circle'),
    percents         = $('.cprogress__percents').children('span'),
    bar              = $('.cprogress__bar'),
    indexBlock       = $('.cprogress-index').find('span'),
    sizeBlock        = $('.cprogress-size').find('span'),
    statusBlock      = $('.cprogress-status').find('span'),
    pageSpeedBlock   = $('.cprogress-page-speed').find('span'),
    websiteSpeedBlock = $('.cprogress-website-speed').find('span');

const selectedValidations = [1, 2, 3, 4, 5];

const validations = {
    1: {
        name: trans['validations']['names'][1],
        group: {
            'n-0': trans['validations'][0],
            'n-1': trans['validations'][1],
            'n-2': trans['validations'][2],
            'n-3': trans['validations'][3],
            'n-4': trans['validations'][4]
        }
    },
    2: {
        name: trans['validations']['names'][2],
        group: {
            'n-16': trans['validations'][16],
            'n-17': trans['validations'][17]
        }
    },
    3: {
        name: trans['validations']['names'][3],
        group: {
            'n-5': trans['validations'][5],
            'n-6': trans['validations'][6],
            'n-7': trans['validations'][7],
            'n-8': trans['validations'][8],
            'n-9': trans['validations'][9],
            'n-10': trans['validations'][10]
        }
    },
    4: {
        name: trans['validations']['names'][4],
        group: {
            'n-11': trans['validations'][11],
            'n-12': trans['validations'][12],
            'n-13': trans['validations'][13]
        }
    },
    5: {
        name: trans['validations']['names'][5],
        group: {
            'n-14': trans['validations'][14],
            'n-15': trans['validations'][15]
        }
    }
};

function closeAll(){
    $('.report__sidenav-title').each(function () {
        $(this).addClass('closed');
        $(this).next().height(0).addClass('closed');
    });
    $('.report__sidenav-title span.showing').fadeIn();
}

function getHostName(url) {
    let match = url.match(/:\/\/(www[0-9]?\.)?(.[^/:]+)/i);
    if (match != null && match.length > 2 && typeof match[2] === 'string' && match[2].length > 0) {
        return match[2];
    }
    else {
        return null;
    }
}

function countAnimation(element, count) {
    $({
        countNum: $('#'+element).text()
    }).animate({
            countNum: count
        },
        {
            duration: 2000,
            easing: 'swing',
            step: function () {
                $('#'+element).text(Math.floor(this.countNum));
            },
            complete: function () {
                $('#'+element).text(this.countNum);
            }
        }
    );
}

let allValidations = 0,
    passedChecks   = 0;

let validationResult = new Map();

$(document).ready(function() {

    allValidations = countValidations();

    $(function(){
        if (getAndCheckQuery()) {
            executeQuery();
        }
    });
});

/**
 * Check is page opened in Iframe or not
 *
 * @returns {boolean}
 */
function isInIframe () {
	try {
		return window.self !== window.top;
	} catch (e) {
		return true;
	}
}

function checkValidationsCount() {
    if (typeof check_cookie_name !== "undefined" && !isInIframe()) {
        let value = getCookie(check_cookie_name);
        if (!value) {
            value = 1;
        } else {
            value++;
        }
        setCookie(check_cookie_name, value);
        if (value > 1) {
            showValidation();
            console.log('limit!');
        }
    }
    return true;
}

function getCookie(name) {
    var matches = document.cookie.match(new RegExp(
        "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
    ));
    return matches ? decodeURIComponent(matches[1]) : false;
}

function setCookie(name,value,days) {
    var expires = "";
    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days*24*60*60*1000));
        expires = "; expires=" + date.toUTCString();
    }
    document.cookie = name + "=" + value + expires + "; path=/";
}

function showValidation() {
    let header_links = [],
        product_links = [];

    $('.header .products li').each(function () {
        let element = $(this).find('a');
        header_links.push(element.attr('href'));
    });

    product_links[0] = header_links[3];
    product_links[1] = header_links[1];
    product_links[2] = header_links[4];
    product_links[3] = header_links[2];
    product_links[4] = header_links[5];
    product_links[5] = header_links[6];

    $('.limit__popup .products li').each(function (index) {
        let element = $(this).find('a');
        element.attr('href',product_links[index]);
    });
    $('.tml-submit-wrap.registr-tml-submit-wrap input[name="redirect_to').val(window.location.href);
    $('.login__popup').addClass('limitPopup').css('display','flex');
    $('.signup__block').css({
        'right':'0',
        'top': '80px'
    }).addClass('animated');
    if (window.matchMedia('(max-width: 460px)').matches) {
        $('.login__popup-block').css({
            'height':'525px'
        });
    } else {
        $('.login__popup-block').css({
            'height':'550px'
        });
    }
    $('.signin__block, .resetpass__block').css({
        'left':'-650px'
    }).removeClass('bounceInLeft').removeClass('animated');

    $('.popup__signin').click(function () {
        $('.signup__block').css({
            'right':'-650px'
        }).removeClass('bounceInRight').removeClass('animated');
        if (window.matchMedia('(max-width: 460px)').matches) {
            $('.login__popup-block').css({
                'height':'525px'
            });
        } else {
            $('.login__popup-block').css({
                'height':'550px'
            });
        }
        $('.signin__block').css({
            'left':'0'
        }).addClass('bounceInLeft').addClass('animated');
    });
    $('.popup__signup').click(function () {
        $('.signup__block').css({
            'right':'0',
            'top': '80px'
        }).addClass('bounceInRight').addClass('animated');
        if (window.matchMedia('(max-width: 460px)').matches) {
            $('.login__popup-block').css({
                'height':'525px'
            });
        } else {
            $('.login__popup-block').css({
                'height':'550px'
            });
        }
        $('.signin__block, .resetpass__block').css({
            'left':'-650px'
        }).removeClass('bounceInLeft').removeClass('animated');
    });
    $('.popup__pass').click(function () {
        $('.signin__block').css({
            'left':'-650px'
        });
        if (window.matchMedia('(max-width: 460px)').matches) {
            $('.login__popup-block').css({
                'height':'525px'
            });
        } else {
            $('.login__popup-block').css({
                'height':'550px'
            });
        }
        $('.resetpass__block').css({
            'left':'0',
            'top': '160px'
        }).addClass('bounceInLeft').addClass('animated');
    });

    disableScroll();
    dataLayer.push({ 'event':'autoEvent','eventCategory':'limit_popup', 'eventAction': 'shown'});
}

function executeQuery() {
    let query = getAndCheckQuery();
    if (!query || selectedValidations.length === 0) {
        return;
    }

    $('.container__results .cloudflare').remove();

    if (!isMtfIframe) {
        changeState('seo-report/' + query);
        resetResults();
    }

    if (checkValidationsCount()) {
        const statusUrl = `${document.location.protocol}//api1.${host}/api/v1/status/${query}`;
        let statusRequest = $.ajax({
            url: statusUrl,
            method: 'GET',
            dataType: 'json',
            xhrFields: {
                withCredentials: true
            }
        });

        statusRequest.done(function(response) {
            if (response.url) {
                var SEOURL = encodeURIComponent(getHostName(response.url)),
                    d = document,
                    userLanguage = $('html').attr('lang'),
                    userId = checkData(USERID),
                    userEmail = checkData(USEREMAIL),
                    userLifetimeOrders = checkData(USERLIFETIMEORDERS),
                    userLifetimeValue = checkData(USERLIFETIMEVALUE),
                    userCrawlerDomains = checkData(USERCRAWLERDOMAINS),
                    userMembeshipStatus = checkData(USERMEMBERSHIPSTATUS),
                    userAvaliableURL = checkData(USERAVALIABLEURL),
                    userAvaliableDomains = checkData(USERAVALIABLEDOMAINS);
            }

            function checkData(data){
                if (data === ''){
                    return 'false';
                } else{
                    return data;
                }
            }

            function seoBanner(id, code){
                let script = d.createElement('script');
                script.src='https://t.sitechecker.pro/'+code+'?se_referrer=' + encodeURIComponent(document.referrer) + '&default_keyword=' + encodeURIComponent(document.title) + '&'+window.location.search.replace('?', '&')+'&frm=script&_cid='+id+'&extra_param_1='+SEOURL+'&extra_param_2='+userMembeshipStatus+'&extra_param_3='+userId+'&extra_param_4='+userEmail+'&extra_param_5='+userLifetimeOrders+'&extra_param_6='+userLifetimeValue+'&extra_param_7='+userCrawlerDomains+'&extra_param_8='+userLanguage+'&extra_param_9='+userAvaliableURL+'&extra_param_10='+userAvaliableDomains;
                d.getElementById(id).appendChild(script);
            }
            if (response !== undefined) {
                if ("htmlStatus" in response && response.htmlStatus) {
                    statusBlock.html(response.htmlStatus);
                }

                if ("url" in response && response.url && query !== response.url) {
                    query = response.url;
                    changeState('seo-report/' + query);
                }
            }

            if (response === undefined || response.status === undefined || response.status !== 200 || !response.url) {
                renderAbsolutelyFail("htmlStatus" in response && response.htmlStatus);
                if ("error" in response) {
                    $('.report__container').addClass('result__error');
                    countAnimation('score__all', 0);
                    renderResponsesNavRow('error', 0, 0, response.error, trans['Error'], false);
                }
            } else {
                executeAllValidations(query);
                $('.validation__block#h-1').after('<span id="b02d1043-9111-f95d-a6f3-1d636bbdeaa4"></span>');
                seoBanner("b02d1043-9111-f95d-a6f3-1d636bbdeaa4", "kZdZsm");
            }

            addValidationResult('status', response);
            logStatus(response)
        });

        statusRequest.fail(function(jqXHR, textStatus) {
            renderAbsolutelyFail();
        });

    } else {
        resetResults();
    }
}

function addValidationResult(num, response) {
    validationResult.set(num, response);
}

function logStatus(statusRequest) {
    $.ajax({
    url: '/tool/crawler_api/log_on_page_check/',
    type: 'POST',
    dataType: 'JSON',
    data: statusRequest,
        success: function success(data) {
        console.log(data);
    }
});
}

function executeAllValidations(query) {

    $.each(selectedValidations, function (key, validationsGroupIndex) {

        $.each(validations[validationsGroupIndex]['group'], function (validationIndex, validationName) {
            validationIndex = Number(validationIndex.split('-')[1]);
            renderGroupTitles(validationsGroupIndex, validationIndex, validations[validationsGroupIndex]['name']);
            renderValidationTemplate(validationIndex,validationsGroupIndex);

            requestsStorage[validationIndex] = $.ajax({
                url: getDomain(validationIndex) + `/api/v1/full/${validationIndex}/${query}`,
                method: 'GET',
                dataType: 'json',
                xhrFields: {
                    withCredentials: true
                },
                crossDomain: true,
                success: function(response){
                    let params = {
                        0: validationsGroupIndex,
                        1: validationIndex,
                        2: query,
                        3: response
                    };

                    ++passedChecks;

                    if (debugMode) {
                        console.log(`check ${passedChecks} of ${allValidations}`);
                    }

                    addValidationResult(validationIndex, response);

                    if (allValidations === passedChecks) {
                        hideLoader();
                        renderResult(params, true);
                    } else {
                        renderResult(params, true);
                    }
                },
                error: function (jqXHR, textStatus) {
                    renderFail(validationIndex, textStatus, allValidations);
                }
            });

        });
    });
}

function getFixBlock(message) {
    return $('<div/>',{
        class: 'expand expand-fix'
    }).append(
        $('<div/>', {
            class: 'message hidden',
            html: message
        }),
        $('<a/>', {
            class: 'code--ok',
            html: `${trans['How to fix']} <img src="/wp-content/themes/sitechecker/out/img_design/arrow__down.svg">`
        })
    );
}

function renderResult(params, lastIteration = false, miniTool = false) {
    if(miniTool) {
        $('.result').append('<div id="result-' + params[1] + '"><div class="validation__param-body"></div></div>');
    }

    let validationsGroupIndex = params[0],
        validationIndex       = params[1],
        response              = params[3],
        array                 = response.data.checks,
        evaluation            = response.data.evaluation,
        mainBlock = $(`#result-${validationIndex}`),
        resultBlock = mainBlock.find('.validation__param-body');

    resultBlock.empty();
    updateEvaluation(evaluation);
    if(validationIndex === 32) {
        $('#result-32 tbody').empty();
    }

    $.each(array, function(key, obj) {
        let importance = obj.importance,
            subgroup = obj.description ? obj.description : obj.subgroup,
            title = obj.title,
            query = params[2],
            description = obj.label,
            fix = obj.fix;

        if(validationIndex === 32){
            $('#result-32 tbody').append('<tr><td>'+(key+1)+'</td><td><a href="'+title[0]+'">'+title[0]+'</a></td><td><span class="code--'+importance+'">'+importance+'</span></td></tr>');
        }else{
            if (title instanceof Array) {
                switch (validationIndex) {
                    case 17:
                        return;
                    case 2:
                        if (!miniTool) {
                            renderGoogleSnippet(title[0], query);
                        }
                        return;
                    default:
                        title = renderTable(title, validationIndex, subgroup);
                        if (obj.fix) {
                            if (validationIndex == 9 && key == 4 || validationIndex == 7) {
                                title = $('<div/>').append(title, getFixBlock(obj.fix));
                            } else {
                                title = $('<div/>').append(getFixBlock(obj.fix), title);
                            }
                            obj.fix = null;
                        }
                }

            } else if (validationIndex == 17 && array.length == 2) {
                title = `<span class="code--ok">${title}</span>`;
            }

            $('img[src="/assets/images/graph.svg"]').attr('src','/wp-content/themes/sitechecker/out/img_design/external.svg');
            $('.icon--arrow-down').attr('src','/wp-content/themes/sitechecker/out/img_design/arrow__down.svg');
            let parentTransfer = $('span.glyphicon-transfer').parent(),
                parentLink = $('span.glyphicon-link').parent(),
                parentUnlink = $('img[src="/assets/images/unlink.svg"]').parent(),
                parentUnlinkError = $('img[src="/assets/images/nofollow.svg"]').parent();

            parentTransfer.empty().html('<i class="fas fa-exchange-alt" style="font-size: 10px"></i>');
            parentLink.empty().html('<i class="fas fa-link" style="font-size: 10px"></i>');
            parentUnlink.empty().html('<i class="fas fa-unlink" style="font-size: 10px"></i>');
            parentUnlinkError.empty().html('<i class="fas fa-unlink" style="color: #ff4b55;font-size: 10px"></i>');

            if(!miniTool) {
                if (validationIndex === 0) {
                    switch (key) {
                        case 2:
                            sizeBlock.html(`<span class="code--${importance}">${title}</span>`);
                            break;
                    }
                }
                if (validationIndex === 8) {
                    renderIndex(evaluation == 100);
                }
                if (validationIndex === 14) {
                    switch (key) {
                        case 0:
                            let speed = title,
                                val = pageSpeedBlock.html(speed).find('b').text();
                            pageSpeedBlock.html(`<span class="code--${importance}">${val}%</span>`);
                            break;
                        case 1:
                            let user = title,
                                value = websiteSpeedBlock.html(user).find('b').text();
                            websiteSpeedBlock.html(`<span class="code--${importance}">${value}%</span>`);
                            break;
                    }
                }
                renderPageScore(importance, lastIteration);
            }

            if (key === 0) {
                if(!miniTool) {
                    renderSidebarTitle(validationsGroupIndex, validationIndex);
                    renderTitle(mainBlock, validations[validationsGroupIndex]['group'][`n-${validationIndex}`], validationIndex);
                } else if(validationIndex === 18 || validationIndex === 19){
                    let validationIndexFull = validationIndex === 18 ? 15 : 14;
                    renderTitle(mainBlock, validations[validationsGroupIndex]['group'][`n-${validationIndexFull}`], validationIndexFull);
                }
            }

            if (validationIndex === 14 && obj.description == 'screenshot' && obj.title) {
                return renderMobilePreview(obj.title);
            }

            if (validationIndex === 0 && key === 0) {
                return;
            }

            if (validationIndex === 0 && key === 4) {
                return;
            }

            if (validationIndex === 0 && key === 3) {
                return;
            }

            if (validationIndex === 16 && key !== 2) {
                return;
            }

            switch (importance) {
                case 'error':
                case 'warning':
                case 'notice':
                    renderResponsesNavRow(importance, key, validationIndex, title, obj.subgroup, obj.description);
                    renderErrorInSidebar(importance, validationIndex,validationsGroupIndex);
            }

            $('<div/>', {
                class: function() {
                    switch (validationIndex) {
                        case 3:
                            if (key === 5) {
                                return 'validation__param_block display__tags '+importance;
                            }else{
                                return 'validation__param_block '+importance;
                            }
                        case 6:
                            if (key === 0) {
                                if(typeof(obj.title) !== 'string'){
                                    return 'validation__param_block alternate__links '+importance;
                                }else{
                                    return 'validation__param_block '+importance;
                                }
                            }else{
                                return 'validation__param_block '+importance;
                            }
                        case 16:
                            return 'validation__param_block favicon__block '+array[1]['importance'];
                            break;
                        case 17:
                            return 'validation__param_block image__block';
                            break;
                        default:
                            return 'validation__param_block '+importance;
                    }
                }
            }).append(
                $('<div/>', {
                    class: 'title'
                }).append(
                    $('<p/>', {
                        class: 'name',
                        html: subgroup
                    }),
                    $('<div/>', {
                        class: 'value',
                        html: function() {
                            if (validationIndex === 16) {
                                let img = '',
                                    img_description = array[1]['title'];
                                if (array[0]['title']){
                                    img = array[0]['title'];
                                }else{
                                    img = '/assets/images/image_error.png';
                                }
                                return '<img src="'+img+'"><p>'+img_description+'</p>';
                            }else if(validationIndex === 11 && key === 0 || validationIndex === 12 && key === 0 || validationIndex === 13 && key === 0){
                                if(typeof(array[0]['title']) !== 'string'){
                                    return 'Found <b>'+(array[0]['title']).length+'</b> '+array[0]['subgroup']+':';
                                }else{
                                    return title;
                                }
                            }else if(validationIndex !== 31){
                                return title;
                            }
                        }
                    }),
                    $('<div/>', {
                        class: 'arrow',
                        html: function() {
                            if(validationIndex === 16){
                                return '<span>Show all favicon checks</span><svg width="9" height="5" viewBox="0 0 9 5" fill="none" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" clip-rule="evenodd" d="M4.499 0L9 2.5V4.896L4.499 2.38L0 5V2.5L4.499 0Z" transform="translate(9 5) rotate(-180)" fill="#495c6a"></path></svg>';
                            }
                            else if(fix === undefined && description === undefined){
                                return '';
                            }else{
                                return '<span>How to fix</span><svg width="9" height="5" viewBox="0 0 9 5" fill="none" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" clip-rule="evenodd" d="M4.499 0L9 2.5V4.896L4.499 2.38L0 5V2.5L4.499 0Z" transform="translate(9 5) rotate(-180)" fill="#495c6a"></path></svg>';
                            }
                        }
                    }),
                ),
                $('<div/>', {
                    class: 'description',
                    style: function() {
                        if (validationIndex === 3) {
                            if (subgroup === trans['Tables']['DisplayTags']) {
                                return 'display: none';
                            }
                        }
                        if(validationIndex === 17 || validationIndex === 6 && key === 1 || validationIndex === 13 && key === 1){
                            return 'display: none';
                        }
                        else if(fix === undefined && description === undefined){
                            return 'display: none';
                        }
                    },
                    html: function() {
                        let descriptionBlock = description,
                            fixBlock = fix;

                        if(descriptionBlock !== undefined){
                            descriptionBlock = '<div>'+description+'</div>';
                        }else{
                            descriptionBlock = '';
                        }

                        if(fixBlock !== undefined){
                            fixBlock = '<div class="content__fix">'+fix+'</div>';
                        }else{
                            fixBlock = '';
                        }

                        if (validationIndex === 16) {
                            let favicon__table = '';
                            $.each(array[2]['title'], function(key, val) {
                                $.each(val, function(key, browser) {
                                    favicon__table = favicon__table + '<div class="'+browser[0]['importance']+'"><b>'+key + '</b>: ' +browser[0]['message']+'</div>';
                                });
                            });
                            return '<div class="content">' +
                                '<div class="favicons">'+favicon__table+'</div>'+descriptionBlock+'</div>';
                        }
                        else if(validationIndex === 6 && key === 1){
                            return '<div class="content">' +fixBlock+'</div>';
                        }
                        else{
                            return '<div class="content">'+fixBlock+descriptionBlock+'</div>';
                        }
                    }
                }),
                (((validationIndex === 11 && key === 0 && typeof(obj.title) !== 'string') || (validationIndex === 12 && key === 0 && typeof(obj.title) !== 'string') || (validationIndex === 13 && key === 0 && typeof(obj.title) !== 'string') || (validationIndex === 6 && key === 0 && typeof(obj.title) !== 'string')) || (validationIndex === 31 && key === 0 && typeof(obj.title) !== 'string') ? title : ''),
                (((validationIndex === 17 && key === 0 && array.length > 1)) ? renderImageTable(array[1].title, array[0]) : '')
            ).appendTo(resultBlock);
        }
        $('#result-32').fadeIn();
    });

    if (validationIndex === 17) {
        renderImages();
    }
}

function renderValidationTemplate(validationIndex,validationsGroupIndex) {
    let idArray = String(validationIndex).split('-'); // google snippet
    let template = $('<div id="result-'+validationIndex+'" class="validation__param"><div class="validation__param-body"><img src="/wp-content/themes/sitechecker/out/img_design/loading-blue.svg" alt="arrow" title="arrow" class="loading"></div></div>');

    if (idArray.length !== 2) {
        template.appendTo('.validation__block[data-target="h-'+validationsGroupIndex+'"] .validation__block-body');
    } else {
        template.insertAfter(`#result-${idArray[0]}`); // google snippet
    }
}

function renderGoogleSnippet(snippet, query) {

    renderValidationTemplate('2-1');

    let template = $('#result-2-1'),
        result   = template.children('.validation__param-body').empty(),
        text     = trans['GoogleSnippet']['Fix'];

    let validationIndex = 2.1;

    siteUrl = siteUrl ? siteUrl : query;
    if (siteUrl.slice(0,4) != 'http') {
        siteUrl = 'http://' + siteUrl;
    }

    $('<p class="validation__param-title" data-target="'+validationIndex+'">' + trans['GoogleSnippet']['Title'] +'</p>').appendTo(result);

    $('<div/>', {class: 'validation__param-body'}).empty().append(
        $('<div/>', {class: 'validation__param_block google__prev'}).append(
            $('<div/>', {class: 'title'}).append(
                $('<div/>', {
                    class: 'name',
                    html: '<img src="/wp-content/themes/sitechecker/out/img_design/google__logo.svg">'
                }),
                $('<div/>', {class: 'value'}).append(
                    $('<a/>', {
                        class: 'snippet__title',
                        target: '_blank',
                        text: function() {
                            if (snippet.title.length > 70) {
                                return cutStringByWords(snippet.title, 70) + ' ...';
                            } else {
                                return snippet.title;
                            }
                        },
                        href: siteUrl
                    }),
                    $('<div/>', {
                        class: 'snippet__url',
                        text: siteUrl,
                    }),
                    $('<div/>', {
                        class: function() {
                            if (snippet.description) {
                                return 'snippet__description';
                            } else {
                                return 'snippet__description snippet__description--error';
                            }
                        },
                        text: function() {
                            if (snippet.description) {
                                return cutStringByWords(snippet.description, 154) + ' ...';
                            } else {
                                return trans['GoogleSnippet']['EmptyDescription'];
                            }
                        }
                    })
                ),
                $('<div/>', {
                    class: 'arrow',
                    html: '<span>How to fix</span><svg width="9" height="5" viewBox="0 0 9 5" fill="none" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" clip-rule="evenodd" d="M4.499 0L9 2.5V4.896L4.499 2.38L0 5V2.5L4.499 0Z" transform="translate(9 5) rotate(-180)" fill="#495c6a"></path></svg>'
                }),
            ),
            $('<div/>', {class: 'description'}).append(
                $('<div/>', {class: 'content'}).append(
                    $('<div/>', {
                        html: text
                    })
                )
            )
        )
    ).appendTo(result);

    function cutStringByWords(snippetString, maxLength) {
        let lastSpaceIndex;

        while (snippetString > maxLength) {
            lastSpaceIndex = snippetString.lastIndexOf(' ');
            snippetString.slice(0, lastSpaceIndex);
        }

        return snippetString;
    }

    let sidebarBlock = $('<div/>', {class: 'report__sidenav-link', 'data-index': validationIndex}).append(
        $('<div/>').append($('<p/>', {
        text: function() {
            if (debugMode) {
                return validationIndex + ': ' + trans['GoogleSnippet']['Title'];
            } else {
                return trans['GoogleSnippet']['Title'];
            }
        }
    }))).appendTo('#report__sidenav-links-1');

    template.show();

    sidebarBlock
        .slideDown(300);
}

function renderMobilePreview(img) {

    // check for duplicate
    if ($('.mobile-preview-wrapper').length > 0) {
        return;
    }

    let validationIndex = 13.1;

    $('<div/>', {
        class: 'validation__param mobile-preview-wrapper',
        id: 'result-13-1'
    }).append(
        $('<div/>', {
            class: 'validation__param-title',
            text: trans['MobilePreview']
        })
    ).insertBefore('#result-14');

    $('<div/>', {class: 'validation__param-body mobile-preview'}).empty().append('<div class="mobile-preview-iphone"><img src="'+img+'" alt="mobile-preview" title="mobile-preview"><img src="/wp-content/themes/sitechecker/out/img_design/iphone.svg" alt="iphone" title="iphone"></div>').appendTo('.mobile-preview-wrapper');

    let sidebarBlock = $('<div/>', {class: 'report__sidenav-link', 'data-index': validationIndex}).append(
        $('<div/>').append($('<p/>'), {
        text: function() {
            if (debugMode) {
                return validationIndex + ': ' + trans['MobilePreview'];
            } else {
                return trans['MobilePreview'];
            }
        }
    })).prependTo('#report__sidenav-links-5');

    sidebarBlock
        .hide()
        .slideDown(300);
}

function renderTable(titleArray, validationIndex, subgroup) {
    let table = $('<table/>', {
        class: function() {
            if (validationIndex === 3 && subgroup === trans['Tables']['DisplayTags']) {
                return 'results-table headers';
            } else if(validationIndex === 5) {
                return 'results-table canonical';
            }else if(validationIndex === 11) {
                return 'results-table external hidden-rows';
            } else if(validationIndex === 13 || validationIndex === 12) {
                return 'results-table internal hidden-rows';
            } else if(validationIndex === 31) {
                return 'results-table google-cache';
            } else {
                return 'results-table';
            }
        }
    });

    renderTableHeaderRow();
    renderTableRows();
    renderShowMoreRow();

    function renderTableHeaderRow() {

        const unlinkIcon = '<img src="/assets/images/unlink.svg" class="icon--unlink">';

        let headers, cell, headerRow = $('<tr/>');

        switch (validationIndex) {
            case 6:
                headers = [trans['Tables']['Link'], trans['Tables']['Lang'], trans['Tables']['MH'], trans['Tables']['B'], trans['Tables']['Code']];
                break;
            case 11:
                headers = ['#', trans['Tables']['Link'], unlinkIcon, trans['Tables']['Anchor'], trans['Tables']['Code']];
                break;
            case 12:
            case 13:
                headers = ['#', trans['Tables']['Link'], unlinkIcon, trans['Tables']['Anchor'], trans['Tables']['Code']];
                break;
            case 31:
                headers = ['URL', 'Status', 'Last Modified'];
                break;
            default:
                return;
        }
        $.each(headers, function(key, head) {

            cell = $('<th/>', {
                html: head
            });

            if (validationIndex === 6) {

                if (key !== 0) {
                    $('<div/>', {
                        class: 'tooltip',
                        html: function() {
                            if (key === 1) {
                                return trans['Tables']['LinkTooltip'];
                            } else if (key === 2) {
                                return trans['Tables']['LangTooltip'];
                            } else if (key === 3) {
                                return trans['Tables']['MHTooltip'];
                            } else if (key === 4) {
                                return trans['Tables']['BTooltip'];
                            } else if (key === 5) {
                                return trans['Tables']['CodeTooltip'];
                            }
                        }
                    }).appendTo(cell);
                }

            } else if(validationIndex === 11) {

                if (key !== 0 && key !== 3) {
                    $('<div/>', {
                        class: 'tooltip',
                        html: function() {
                            if (key === 1) {
                                return trans['Tables']['QtyTooltip'];
                            } else if (key === 2) {
                                return trans['Tables']['LinkTooltip'];
                            } else if (key === 4) {
                                return trans['Tables']['AnchorTooltip'];
                            } else if (key === 5) {
                                return trans['Tables']['CodeTooltip'];
                            }
                        }
                    }).appendTo(cell);
                }

            } else if(validationIndex === 13) {

                if (key !== 0 && key !== 2 && key !== 4) {
                    $('<div/>', {
                        class: 'tooltip',
                        html: function() {
                            if (key === 1) {
                                return trans['Tables']['QtyTooltip'];
                            } else if (key === 3) {
                                return trans['Tables']['LinkTooltip'];
                            } else if (key === 5) {
                                return trans['Tables']['AnchorTooltip'];
                            } else if (key === 6) {
                                return trans['Tables']['CodeTooltip'];
                            }
                        }
                    }).appendTo(cell);
                }

            }

            cell.appendTo(headerRow);

        });

        headerRow.appendTo(table);

    }

    function renderTableRows() {
        if(validationIndex === 31){
            let row = $('<tr/>'),
                headerSize,
                style;

            $.each(titleArray, function (key, value) {
                let val = value;
                switch (key) {
                    case 0 :
                        val = '<a href="'+value+'" target="_blank">'+value+'</a>';
                        break;
                    case 1 :
                        val = (val === 'ok' ? '<span class="code--ok">'+value+'</span>' : value);
                        break;
                }
                $('<td/>', {
                    html: val,
                    style: style
                }).appendTo(row);
            });

            row.appendTo(table);
        } else{
            $.each(titleArray, function (key, array) {

                let row = $('<tr/>'),
                    headerSize,
                    style;

                if (validationIndex === 3 && subgroup === trans['Tables']['DisplayTags'] /*H1-H6*/) {
                    row = $('<tr/>', {class: 'row--hidden'});
                }

                $.each(array, function (key, value) {

                    switch (validationIndex) {
                        case 3:
                            switch (subgroup) {
                                case trans['Tables']['CountAllTags']:
                                    style = 'padding: 0;';
                                    break;
                                case trans['Tables']['DisplayTags']:
                                    switch (key) {
                                        case 0:
                                            headerSize = +value.substr(-1, 1);
                                            style = '';
                                            break;
                                        case 1:
                                            headerSize = (headerSize === 1) ? 0 : headerSize;
                                            style = `padding-left: ${15 * headerSize}px;`;
                                    }
                                    break;
                                default:
                                    style = '';
                            }
                            break;
                    }

                    $('<td/>', {
                        html: value,
                        style: style
                    }).appendTo(row);

                });

                row.appendTo(table);
            });
        }
    }

    function renderShowMoreRow() {
        if (validationIndex === 3 && subgroup === trans['Tables']['DisplayTags'] && titleArray.length >=5) {
            $('<tr/>', {
                class: 'expand-headers'
            }).append(
                $('<td/>', {
                    class: 'code--clear',
                    colspan: 2
                }).append(
                    $('<span/>', {text: trans['Show more']}),
                    $('<img/>', {src: '/wp-content/themes/sitechecker/out/img_design/arrow__down.svg'})
                )
            ).appendTo(table);
        } else if(validationIndex === 11 && titleArray.length > 10) {
            $('<tr/>', {
                class: 'expand expand-external'
            }).append(
                $('<td/>', {
                    class: 'code--clear',
                    colspan: 2
                }).append(
                    $('<span/>', {text: trans['Show more']}),
                    $('<img/>', {src: '/wp-content/themes/sitechecker/out/img_design/arrow__down.svg'})
                )
            ).appendTo(table);
        } else if(validationIndex === 13 && titleArray.length > 10) {
            $('<tr/>', {
                class: 'expand expand-internal'
            }).append(
                $('<td/>', {
                    class: 'code--clear',
                    colspan: 4
                }).append(
                    $('<span/>', {text: trans['Show more']}),
                    $('<img/>', {src: '/wp-content/themes/sitechecker/out/img_design/arrow__down.svg'})
                )
            ).appendTo(table);
        }
    }

    return table;
}

function renderImageTable(titleArray, messages) {

    let table = $('<table/>', {class: 'results-table images hidden-rows'}),
        row   = $('<tr/>');

    const tableHeads = [
            trans['Tables']['Preview'],
            trans['Tables']['AltAttribute'],
            trans['Tables']['TitleAttribute'],
            trans['Tables']['Size']
        ],
        tableLabels  = [
            trans['Tables']['PreviewTooltip'],
            trans['Tables']['AltAttributeTooltip'],
            trans['Tables']['TitleAttributeTooltip'],
            trans['Tables']['SizeTooltip']
        ],

        errorBlock = $('<span/>', {
            class: 'code--error',
            text: `[ ${trans['empty']} ]`
        });

    $.each(tableHeads, function (key, tableHead) {
        $('<th/>', {text: tableHead}).append(
            $('<div/>', {
                class: 'tooltip',
                text: tableLabels[key]
            })
        ).appendTo(row);
    });

    row.appendTo(table);

    $.each(titleArray, function (key, array) {

        if (array.error) {

            $('<tr/>', {
                class: 'row--images',
                'data-sort': 0
            }).append(
                $('<td/>', {
                    class: 'code--clear'
                }).append(
                    $('<img/>', {src: '/assets/images/image_error.png'})
                ),

                $('<td/>', {
                    class: 'code--clear'
                }).append(
                    array.alt
                        ? array.alt
                        : errorBlock.prop('outerHTML')
                ).append($('<br>')
                ).append($('<span/>', {
                    html: array.error,
                    style: 'color: #ff4b55'
                })),

                $('<td/>', {
                    class: 'code--clear'
                }).append(
                    array.title
                        ? array.title
                        : errorBlock.prop('outerHTML')
                ),

                $('<td/>', {
                    class: 'code--clear'
                }).append(
                    array.size
                        ? (array.size < 100
                        ? array.size + ' KB'
                        : $('<span/>', {
                            class: 'code--error',
                            text: array.size + ' KB',
                            title: array.label,
                            'data-toggle': function() {
                                return (array.label) ? 'tooltip' : '';
                            }
                        }))
                        : errorBlock.prop('outerHTML')
                )
            ).appendTo(table);

        } else {

            $('<tr/>', {
                class: 'row--images',
                'data-sort': array.size ? array.size : 0
            }).append(

                ((array.originUrl) ?
                        $('<td/>', {
                            'data-alt': array.alt ? array.alt : errorBlock.prop('outerHTML'),
                            'data-title': array.title ? array.title : errorBlock.prop('outerHTML'),
                            'data-size': array.size ? (array.size < 100 ? array.size + ' KB' : $('<span/>', {
                                class: 'code--error',
                                text: array.size + ' KB'
                            })) : errorBlock.prop('outerHTML')
                        }).append(
                            $('<img/>', {
                                src: array.originUrl,
                                title: 'Image from Base64',
                                style: 'max-width: 100px; max-height: 50px; padding-right: 10px'
                            })
                        )
                        :
                        $('<td/>', {
                            'data-image-src': array.originUrl,
                            'data-alt': array.alt ? array.alt : errorBlock.prop('outerHTML'),
                            'data-title': array.title ? array.title : errorBlock.prop('outerHTML'),
                            'data-size': array.size ? (array.size < 100 ? array.size + ' KB' : $('<span/>', {
                                class: 'code--error',
                                text: array.size + ' KB',
                                title: array.label,
                                'data-toggle': function() {
                                    return (array.label) ? 'tooltip' : '';
                                }
                            })) : errorBlock.prop('outerHTML')
                        }).append(
                            $('<div/>', {
                                class: 'cprogress__loader cprogress__loader--image'
                            })
                        )
                ),

                $('<td/>').append(
                    array.alt
                        ? array.alt
                        : errorBlock.prop('outerHTML')
                ),

                $('<td/>').append(
                    array.title
                        ? array.title
                        : errorBlock.prop('outerHTML')
                ),

                $('<td/>').append(
                    array.size
                        ? (array.size < 100
                        ? array.size + ' KB'
                        : $('<span/>', {
                            class: 'code--error',
                            text: array.size + ' KB',
                            title: array.label,
                            'data-toggle': function() {
                                return (array.label) ? 'tooltip' : '';
                            }
                        }))
                        : errorBlock.prop('outerHTML')
                )
            ).appendTo(table);
        }
    });

    if (Object.keys(titleArray).length > 5) {
        $('<tr/>', {
            class: 'expand expand-images',
            'data-sort': -1
        }).append(
            $('<td/>', {
                class: 'code--clear',
                colspan: 4
            }).append(
                $('<span/>', {text: trans['Show more']}),
                $('<img/>', {src: '/wp-content/themes/sitechecker/out/img_design/arrow__down.svg'})
            )
        ).appendTo(table);
    }

    let content = '<div class="result__content result__content--break">';

    content += table.prop('outerHTML') + '</div>';

    return content;
}

function renderImages(){
    $('td[data-image-src]').each(function(index, cell) {

        let $cell   = $(cell),
            src     = $cell.data('image-src'),
            request = $.ajax({
                url: getImagesDomain() + `/api/v1/image/${src}`,
                method: 'GET',
                dataType: 'json'
            });

        request.done(function(response) {

            if (response.data) {
                $cell
                    .empty()
                    .append(
                        $('<a/>', {
                            href: src,
                            rel: 'group',
                            class: 'fancybox'
                        }).append(
                            $('<img/>', {
                                src: response.data,
                                style: 'max-width: 100px; max-height: 50px; padding-right: 10px'
                            })
                        )
                    );
            }

            if (response.error) {
                $cell.parent().attr('data-sort', 0);

                $cell.html(
                    $('<img/>', {
                        src: '/assets/images/image_error.png'
                    })
                );

                $cell.next()
                    .append($('<br>'))
                    .append($('<span/>', {
                            class: 'code--error',
                            text: response.error
                        })
                    );
            }

            $('table.images tr[data-sort]').sortElements(function(a, b){
                return parseInt($(a).attr('data-sort')) < parseInt($(b).attr('data-sort')) ? 1 : -1;
            });

            initFancy();
        });
    });
}

function updateEvaluation(evaluation) {
    evaluationStorage -= (100 - evaluation);
    if (evaluationStorage < 0) {
        evaluationStorage = 0;
    }
}

function renderPageScore(importance, lastIteration = false) {
    switch (importance) {
        case 'error':
            ++errorStorage;
            break;
        case 'warning':
            ++warningStorage;
            break;
        case 'notice':
            ++noticeStorage;
    }

    countAnimation('score__errors', errorStorage);
    countAnimation('score__warnings', warningStorage);

    if (lastIteration) {
        let score = evaluationStorage;

        if (evaluationStorage < 0 || evaluationStorage === 0) {
            evaluationStorage = 0;
        }
        countAnimation('score__all', score);
    }
}

function renderGroupTitles(validationsGroupIndex, validationIndex, groupName) {
    switch (validationIndex) {
        case 0:
        case 5:
        case 11:
        case 14:
        case 16:
            // main headers
            $('<div class="validation__block" data-target="h-'+validationsGroupIndex+'" id="h-'+validationsGroupIndex+'">' +
                '<div class="validation__block-title">' +
                '<p>'+groupName+'</p>' +
                '<div class="errors"><span class="error">0</span><span class="warning">0</span></div>' +
                '<svg width="9" height="5" viewBox="0 0 9 5" fill="none" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" clip-rule="evenodd" d="M4.499 0L9 2.5V4.896L4.499 2.38L0 5V2.5L4.499 0Z" transform="translate(9 5) rotate(-180)" fill="#495c6a"/></svg></div><div class="validation__block-body"></div></div>').appendTo('.report__validations');

            // sidenav
            $('<div class="report__sidenav-block" data-index="h-'+validationsGroupIndex+'">' +
                '<p class="report__sidenav-title" id="report__sidenav-title-'+validationsGroupIndex+'"><span class="groupName">'+groupName+'</span><svg width="9" height="5" viewBox="0 0 9 5" fill="none" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" clip-rule="evenodd" d="M4.499 0L9 2.5V4.896L4.499 2.38L0 5V2.5L4.499 0Z" transform="translate(9 5) rotate(-180)" fill="#495c6a"></path></svg></p>' +
                '<div class="report__sidenav-links" id="report__sidenav-links-'+validationsGroupIndex+'"></div></div>').appendTo('.report__sidenav');
    }
}

function renderTitle(resultBlock, name, validationIndex) {
    $('<p class="validation__param-title" data-target="'+validationIndex+'">'+name+'</p>').prependTo(resultBlock);
}

function renderSidebarTitle(validationsGroupIndex, validationIndex) {
    let wrapper = $(`#report__sidenav-links-${validationsGroupIndex}`);
    wrapper.append('<div class="report__sidenav-link" data-index="'+validationIndex+'"><div><p>'+validations[validationsGroupIndex]['group'][`n-${validationIndex}`]+'</p><span class="error">0</span><span class="warning">0</span></div></div>');
    sortSidebarTitles();
}

function sortSidebarTitles() {
    let previd, currid;

    $('.report__sidenav-links').each(function() {

        previd = false;

        $(this).children().each(function() {
            currid = $(this).data('index');

            if (!previd) {
                previd = currid;
            }

            if (currid < previd) {
                $(this).insertBefore(`.report__sidenav-link[data-index="${previd}"]`);
            }

            previd = currid;
        });
    });
}

function renderErrorInSidebar(importance, validationIndex, validationsGroupIndex) {
    let counter  = $(`.report__sidenav-link[data-index="${validationIndex}"]`),
        title = $(`.validation__block[data-target="h-${validationsGroupIndex}"] .validation__block-title .errors span.${importance}`),
        titleQuantity = title.text();
        title.text(++titleQuantity).fadeIn();
    let parentCounter = $(`.report__sidenav-link[data-index="${validationIndex}"]`).parent().parent().find(`.report__sidenav-title`);
    if(importance === 'error'){
        let counters = parentCounter.find('span.errors'),
            parentCounterValue = counters.text();
        counters.text(++parentCounterValue);
        counters.addClass('showing');

        let quantity = counter.find('span.error').text();
        counter.find('span.error').text(++quantity).fadeIn();

    }else if(importance === 'warning'){
        let counters = parentCounter.find('span.warnings'),
            parentCounterValue = counters.text();
        counters.text(++parentCounterValue);
        counters.addClass('showing');

        let quantity = counter.find('span.warning').text();
        counter.find('span.warning').text(++quantity).fadeIn();
    }
}

function renderResponsesNavRow(importance, key, validationIndex, title, subgroup, description) {
    let responsesContainer = $(`.responses--${importance}`),
        responsesBlock = $(`.responses--${importance} .responses--content`),
        message;
    $('.report__response .loading').css('display','none');
    responsesContainer.fadeIn();

    if (title instanceof Array || title instanceof Object) {
        message = subgroup + ': ' + description;
    } else {
        message = subgroup + ': ' + title;
    }

    let renderedBlock = $('<div/>', {
        class: 'responses__row',
        'data-index': validationIndex,
        html: message
    }).appendTo(responsesBlock);

    renderedBlock.slideDown(300);
}

function countValidations() {
    let count = 0;

    $.each(selectedValidations, function(key, validationsGroupIndex) {
        $.each(validations[validationsGroupIndex]['group'], function() {
            ++count;
        });
    });

    return count;
}

function hideLoader(status) {
    let endColor = '#d7d7d7',
        score = evaluationStorage;

    $('.audit__banner').fadeIn();

    if (evaluationStorage < 0 || evaluationStorage === 0) {
        evaluationStorage = 0;
    }
    if(evaluationStorage<33){
        endColor = 'error';
    }else if(evaluationStorage>=33 && evaluationStorage<=65){
        endColor = 'warning';
    }else if(evaluationStorage>65){
        endColor = 'ok';
    }
    $('.score__all-animation').fadeOut();
    function finalScore(){
        if(status === 'fail') {
            score = 0;
        }
        console.log('finalScore: '+score);
        countAnimation('score__all', score);
        $('#score__all').addClass(endColor);
    }
    setTimeout(finalScore,300);
    $('#save__pdf').removeClass('inprogress');

    let validationString = '';
    let clientID = ga.getAll()[0].get('clientId');

    console.log(validationsArray);
    $.each(validationsName, function( i, val ) {
        $.each(validationsArray, function( index, value ) {
            window.dataLayer[validationsName[value]] = 'false';
            validationString = validationString+"v=1&tid=UA-82230026-1&cid="+clientID+"&t=event&ec="+validationsName[value]+"&ea=false&el="+location.href+"\r\n";
        });
        window.dataLayer[validationsName[i]] = 'true';
        validationString = validationString+"v=1&tid=UA-82230026-1&cid="+clientID+"&t=event&ec="+validationsName[i]+"&ea=true&el="+location.href+"\r\n";
    });
    let statusCode = $('#result-0 .statusHtml').text();

    window.dataLayer.push({
        'statusCode': statusCode
    });

    validationString = validationString+"v=1&tid=UA-82230026-1&cid="+clientID+"&t=event&ec=statusCode&ea="+statusCode+"\r\n";
    $.ajax({
        async: true,
        crossDomain: true,
        url: "https://www.google-analytics.com/batch",
        type: "POST",
        data: validationString,
        success: function (data) {}
    });

    function openBlock(index){
        closeAll();
        let block = $('#report__sidenav-title-'+index),
            autoHeight = block.next().css('height', 'auto').height();
        block.next().height(0).removeClass('closed').animate({height: autoHeight}, 500);
    }

    // $(window).scroll(function() {
    //     if($(this).scrollTop() >= 950){
    //         closeAll();
    //     }
    // });
}

function showLoader() {
    loader.show();
    circle.addClass('cprogress__circle--hidden');
    percents.parent().addClass('cprogress__percents--hidden');
    bar.addClass('cprogress__bar--hidden');
}

function renderFail(validationIndex, textStatus, allValidations) {
    console.log(`${validationIndex} validation failed: ${textStatus}`);

    validationsArray.push(validationIndex);

    ++passedChecks;

    if (debugMode) {
        console.log(`${passedChecks}/${allValidations}`);
    }

    if (allValidations === passedChecks) {
        hideLoader();
    }
}

function renderIndex(indexable) {
    if (indexable) {
        indexBlock
            .addClass('indexable--ok')
            .text(trans['Indexable']);
    } else {
        indexBlock
            .addClass('indexable--error')
            .text(trans['Not indexable']);
    }
}

function abortRequest() {
    $.each(requestsStorage, function (index, request) {
        request.abort();
    });
}

function resetResults() {
    let resultBlock   = $('.container__result'),
        headerBlock   = $('.container__group-header'),
        sidebarBlock  = $('.sidenav'),
        responseBlock = $('.responses__row');

    abortRequest();

    resultBlock
        .not('#result-template')
        .remove();

    headerBlock.remove();
    sidebarBlock.empty();
    responseBlock.remove();

    showLoader();

    errorStorage      = 0;
    warningStorage    = 0;
    noticeStorage     = 0;
    passedStorage     = 18;
    evaluationStorage = 100;

    passedChecks = 0;

    siteUrl = '';

    indexBlock.html(trans['Waiting']).attr('class', 'indexable');
    sizeBlock.html(trans['Waiting']);
    statusBlock.html(trans['Waiting']);
    pageSpeedBlock.html(trans['Waiting']);
    websiteSpeedBlock.html(trans['Waiting']);
}

function getAndCheckQuery() {
    let query = $('#sitechecker_input input').val();

    if (!query || query.length === 0) {
        return;
    }
    return query;
}

function renderAbsolutelyFail(withoutStatus = false) {
    hideLoader('fail');
    indexBlock.html(`<span class="code--error">${trans['Error']}</span>`);
    sizeBlock.html(`<span class="code--error">${trans['Error']}</span>`);
    if (!withoutStatus) {
        statusBlock.html(`<span class="code--error">${trans['Error']}</span>`);
    }
}

function changeState(newState) {
    if (newState.charAt(0) !== '/') {
        newState = `/${newState}`;
    }

    if (locale && locale !== default_locale) {
        newState = `/${locale}${newState}`;
    }

    window.history.replaceState(window.history.state, '', newState);
}

jQuery.fn.rotate = function(degrees) {
    $(this).css({'transform': `rotate(${degrees}deg)`});
    return $(this);
};

function getDomain(num) {
    let result;
    switch (num) {
        case 0:
        case 1:
        case 2:
        case 3:
        case 4:
        case 5:
            result = 'api1.' + host;
            break;
        case 6:
        case 7:
        case 8:
        case 9:
        case 10:
        case 11:
            result = 'api2.' + host;
            break;
        case 12:
        case 13:
        case 14:
        case 15:
        case 16:
        case 17:
            result = 'api3.' + host;
            break;
        default:
            result = host;
    }

    return document.location.protocol + '//' + result;
}

function getImagesDomain() {
    return document.location.protocol + '//images.' + host;
}

$('button#save__pdf').click(function () {
    let pdfurl = 'https://pdf.sitechecker.pro/api/v1/export/pdf',
        resultText = $('.report__results').html();
    $('.downloadPDF').css('display','none');
    $('#save__pdf .loading').fadeIn();
    function hidePDFLoader() {
        console.log('downloading');
        $('.downloadPDF').fadeIn();
        $('#save__pdf .loading').css('display','none');
    }
    let data = new FormData(),
        logo__url = 'https://sitechecker.pro/wp-content/themes/sitechecker/out/img_design/logo.svg';
    if($('#user_branding_url').length > 0 && $('#user_branding_url').text() !== ''){
        logo__url = $('#user_branding_url').text();
    }
    data.append('html_for_pdf', resultText);
    data.append('branding', logo__url);
    fetch(pdfurl, { method: 'post', body: data }).then(function (r) {
        return r.blob();
    }).then(downloadFile).catch(hidePDFLoader);

    function downloadFile(blob) {
        // It is necessary to create a new blob object with mime-type explicitly set
        // otherwise only Chrome works like it should
        let newBlob = new Blob([blob], { type: "application/pdf" });

        // IE doesn't allow using a blob object directly as link href
        // instead it is necessary to use msSaveOrOpenBlob
        if (window.navigator && window.navigator.msSaveOrOpenBlob) {
            window.navigator.msSaveOrOpenBlob(newBlob);
            return;
        }

        // For other browsers:
        // Create a link pointing to the ObjectURL containing the blob.

        let link = document.createElement('a');
        link.style = "display: none";
        let revote = window.URL.createObjectURL(newBlob);
        link.href = revote;
        link.download = "Sitechecker_Report.pdf";
        document.body.appendChild(link);
        link.click();
        setTimeout(function () {
            document.body.removeChild(link);
            window.URL.revokeObjectURL(revote);
        }, 100);
        hidePDFLoader();
    }
});