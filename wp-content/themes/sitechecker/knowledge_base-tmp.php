<?php
/*
	Template Name: Knowledge Base
*/
get_header('tmp_design');
?>

<?php if (have_posts()): while (have_posts()) : the_post(); ?>
    <?php
        get_template_part('design/knbs');
    ?>
<?php endwhile; ?>
<?php else: ?>
<?php endif; ?>
<?php
    get_footer('tmp_design');
?>