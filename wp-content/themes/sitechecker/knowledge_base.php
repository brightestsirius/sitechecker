<?php
    $template_directory_uri = get_template_directory_uri();
    $meta = get_post_meta_all(get_option('page_on_front'));
?>
<main class="home knowledge">
    <div class="wrapper">
        <h1><?php the_title() ?></h1>
        <!-- search -->
        <form class="search" method="get" action="<?php echo home_url(); ?>" role="search">
            <img src="<?=$template_directory_uri;?>/out/img/search.svg" alt="search" title="search">
            <input class="search-input" type="search" name="s" placeholder="What are you looking for?">
            <button class="search-submit btn_gradient" type="submit" role="button"><span>Search</span></button>
        </form>
        <!-- /search -->
        <?php
        $paged = get_query_var( 'paged' ) ? absint( get_query_var( 'paged' ) ) : 1;
        $args = array(
            'posts_per_page' => 12,
            'post__not_in' => array(17,15),
            'post_type' => array('page','post'),
            'hide_empty' => 1,
            'paged' => $paged,
            'meta_query' => array(
                array(
                    'key' => 'page.about'
                )
            )
        );
        $query = new WP_Query($args);
        ?>
        <?php if ( $query->have_posts() ) : ?>
            <div class="pages_more-wrap">
                <!-- begin loop -->
                <?php while ( $query->have_posts() ) : $query->the_post(); ?>
                    <div class="block">
                        <div class="block_img" style="background: url(<?php the_post_thumbnail_url(); ?>);background-size: cover;background-position: center center;"></div>
                        <a href="<?php the_permalink(); ?>"><?php echo get_post_meta( get_the_ID(), 'page.h1', true); ?></a>
                        <?php
                        $string = get_post_meta( get_the_ID(), 'page.about', true);
                        $maxLength = 200;
                        if (strlen($string) > $maxLength) {
                            $stringCut = substr($string, 0, $maxLength);
                            $string = substr($stringCut, 0, strrpos($stringCut, ' '));
                        }
                        ?>
                        <p><?php echo $string ?></p>
                    </div>
                <?php endwhile; ?>
                <!-- end loop -->
            </div>
            <div class="pagination">
                <?php
                echo paginate_links( array(
                    'base'         => str_replace( 999999999, '%#%', esc_url( get_pagenum_link( 999999999 ) ) ),
                    'total'        => $query->max_num_pages,
                    'current'      => max( 1, get_query_var( 'paged' ) ),
                    'format'       => '?paged=%#%',
                    'show_all'     => false,
                    'type'         => 'plain',
                    'end_size'     => 2,
                    'mid_size'     => 1,
                    'prev_next'    => true,
                    'prev_text'    => sprintf( '<i class="fa fa-chevron-left"></i> %1$s', __('') ),
                    'next_text'    => sprintf( '%1$s <i class="fa fa-chevron-right"></i>', __('') ),
                    'add_args'     => false,
                    'add_fragment' => '',
                ) );
                ?>
            </div>
        <?php else : ?>
            <p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
        <?php endif; wp_reset_query(); ?>
    </div>
</main>