<?php
/*
	Template Name: Landing Page
*/
?>
<?php get_header(); ?>

<main class="home">
    <div class="landing_first">
        <div class="wrapper">
            <div class="landing_first-text">
                <h1><?php echo get_post_meta($post->ID, 'landing.h1', true) ?></h1>
                <p><?php echo get_post_meta($post->ID, 'landing.h2', true) ?></p>
                <a id="order-form_btn" class="btn"><?php echo get_post_meta($post->ID, 'landing.button', true) ?></a>
            </div>
            <div class="landing_first-img">
                <img src="<?php echo get_template_directory_uri(); ?>/out/img/landing_first.svg" alt="landing_first">
            </div>
        </div>
    </div>
    <div class="landing_about">
        <div class="wrapper">
            <h2><?php echo get_post_meta($post->ID, 'landing.about.title', true) ?></h2>
            <div class="landing_about-block">
                <div class="landing_block-img">
                    <img src="<?php echo get_template_directory_uri(); ?>/out/img/landing_about_1.svg" alt="landing_about">
                </div>
                <div class="landing_block-text">
                    <h3><?php echo get_post_meta($post->ID, 'landing.about.header.1', true) ?></h3>
                    <p><?php echo get_post_meta($post->ID, 'landing.about.content.1', true) ?></p>
                </div>
            </div>
            <div class="landing_about-block">
                <div class="landing_block-text">
                    <h3><?php echo get_post_meta($post->ID, 'landing.about.header.2', true) ?></h3>
                    <p><?php echo get_post_meta($post->ID, 'landing.about.content.2', true) ?></p>
                </div>
                <div class="landing_block-img">
                    <img src="<?php echo get_template_directory_uri(); ?>/out/img/landing_about_2.svg" alt="landing_about">
                </div>
            </div>
        </div>
    </div>
    <div class="landing_form">
        <div class="landing_form-text">
            <div class="wrapper">
                <p>
                    <?php echo get_post_meta($post->ID, 'landing.form.content', true) ?>
                </p>
                <div class="landing_form-form">
                    <?php if (have_posts()): while (have_posts()) : the_post(); ?>
                        <?php the_content(); ?>
                    <?php endwhile; ?>
                    <?php else: ?>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</main>
<div class="popup_order">
    <div class="landing_form-form">
        <div class="form_header">
            <p><?php echo get_post_meta($post->ID, 'order.form.title', true) ?></p>
            <img src="<?php echo get_template_directory_uri(); ?>/out/img/close.svg" alt="close" class="popup_order-close">
        </div>
        <?php if (have_posts()): while (have_posts()) : the_post(); ?>
            <?php the_content(); ?>
        <?php endwhile; ?>
        <?php else: ?>
        <?php endif; ?>
    </div>
</div>

<div class="popup_sent">
    <div class="popup_sent-block">
        <img src="<?php echo get_template_directory_uri(); ?>/out/img/img-close.svg" alt="close" title="close" class="popup_sent-close">
        <img src="<?php echo get_template_directory_uri(); ?>/out/img/letter.svg" alt="sent-mail" title="sent-mail" class="popup_sent-img">
        <p><?php echo get_post_meta($post->ID, 'sent.form', true) ?></p>
    </div>
</div>

<?php get_footer(); ?>