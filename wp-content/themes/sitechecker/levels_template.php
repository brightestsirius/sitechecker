<?php
/*
	Template Name: Levels Pages
*/
?>
<?php get_header(); ?>
    <main class="levels_main">
        <div class="wrapper">
            <?php if (have_posts()): while (have_posts()) : the_post(); ?>
                <?php the_content(); ?>
            <?php endwhile; ?>
            <?php else: ?>
            <?php endif; ?>
        </div>
    </main>
<?php get_footer(); ?>