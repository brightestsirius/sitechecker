<?php if (have_posts()): while (have_posts()) : the_post(); ?>
    <div class="block <?php the_ID(); ?>">
        <div class="block_img" style="background: url(<?php the_post_thumbnail_url(); ?>);background-size: cover;background-position: center center;"></div>
        <a href="<?php the_permalink(); ?>"><?php echo get_post_meta( get_the_ID(), 'page.h1', true); ?></a>
        <?php
        $string = get_post_meta( get_the_ID(), 'page.about', true);
        $maxLength = 200;
        if (strlen($string) > $maxLength) {
            $stringCut = substr($string, 0, $maxLength);
            $string = substr($stringCut, 0, strrpos($stringCut, ' '));
        }
        ?>
        <p><?php echo $string ?></p>
    </div>
<?php endwhile; ?>
<?php endif; ?>