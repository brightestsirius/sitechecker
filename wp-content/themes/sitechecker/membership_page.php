<?php
/*
	Template Name: Membership Pages
*/
?>
<?php get_header('tmp_design'); ?>
    <main>
        <div class="wrapper">
            <div class="blog_container">
                <div class="blog_post-contant single_post">
            <?php if (have_posts()): while (have_posts()) : the_post(); ?>
                <?php if ( has_post_thumbnail()) : // Check if Thumbnail exists ?>
                    <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
                        <?php the_post_thumbnail(); // Fullsize image for the single post ?>
                    </a>
                <?php endif; ?>
                <?php the_content(); ?>
            <?php endwhile; ?>
            <?php else: ?>
            <?php endif; ?>
                </div>
            </div>
        </div>
    </main>
<?php get_footer('tmp_design'); ?>