function initFancy() {
    $('.fancybox').fancybox({
        type: 'image',
        padding: 0,
        openEffect	: 'none',
        closeEffect	: 'none',
        live: true,
        helpers: {
            overlay : { locked: false },
            title	: { type : 'float' },
            thumbs	: {
                width	: 50,
                height	: 50
            },
            buttons	: {}
        },
        afterLoad: function(){
            this.title = '<b>Alt</b>: ' + $(this.element).parent().attr('data-alt');
            this.title += '<br><b>Title</b>: ' + $(this.element).parent().attr('data-title');
            this.title += '<br><b>Size</b>: ' + $(this.element).parent().attr('data-size');

            $('.fancybox-title').css({'margin-bottom': (-10 - $('.fancybox-title').height()) + 'px'});
            $('.fancybox-title .child').css({'text-align': 'left', 'font-weight': 'normal', 'border-radius': '5px'});
        },
        onUpdate: function() {
            $('.fancybox-title').css({'margin-bottom': (-10 - $('.fancybox-title').height()) + 'px'});
            $('.fancybox-title .child').css({'text-align': 'left', 'font-weight': 'normal', 'border-radius': '5px'});
        }
    });
}

$(window)
	.scroll(stickySidebar)
	.scroll(highlightSidenavBtn);

$('.sidenav')
	.on('click', '.sidenav__title', function() {

		let childrens = $(this).nextUntil('.sidenav__title');

		$(this).toggleClass('sidenav__btn--opened');

		childrens
			.toggleClass('sidenav__btn--opened')
			.toggle();
	})
	.on('click', '.sidenav__btn--inner', scrollPage);

$('.responses-wrapper')
	.on('click', '.responses__row', scrollPage)
	.on('click', 'a', function(event) {
		event.preventDefault();
	});

$('.cprogress-index').on('click', '.indexable', scrollPage);

$('.container__results')
	.on('click', '.expand-favicons',    function() {expandTable('favicons', $(this))})
	.on('click', '.expand-images',      function() {expandTable('images', $(this))})
	.on('click', '.expand-internal',    function() {expandTable('internal', $(this))})
	.on('click', '.expand-external',    function() {expandTable('external', $(this))})
	.on('click', '.expand-rows',        function() {expandRows('rows', $(this))})
	.on('click', '.expand-headers',     function() {expandRows('headers', $(this))})
	.on('click', '.expand-fix a',       function() {expandFix($(this))})
	.on('click', '.expand-fix span',    function() {expandShow($(this))});

function expandFix(that) {
    let parent = that.parent(), img = parent.find('img'), message = parent.find('.message');

    if (message.hasClass('hidden')) {
        message.removeClass('hidden');
        img.attr('src', '/assets/images/keyboard-arrow-up.svg');
    } else {
        message.addClass('hidden');
        img.attr('src', '/assets/images/keyboard-arrow-down.svg');
    }
}

function expandShow(that) {
    let message = that.parent().find('.message');

    if (message.hasClass('hidden')) {
        message.removeClass('hidden');
        that.html(trans['Hide'] + '<img src="/assets/images/keyboard-arrow-up.svg">');
    } else {
        message.addClass('hidden');
        that.html(trans['Show more'] + '<img src="/assets/images/keyboard-arrow-down.svg">');
    }
}

function expandTable(dest, that) {
    let text = that.find('span').text();

    that
        .toggleClass(`expand-${dest}--opened`)
        .find('span').text(text === trans['Show more'] ? trans['Hide'] : trans['Show more']);
    that.find('img').attr('src', (text === trans['Show more'] ? '/assets/images/keyboard-arrow-up.svg' : '/assets/images/keyboard-arrow-down.svg'));

    $(`table.results-table.${dest}`).toggleClass('hidden-rows');
}

function expandRows(dest, that) {
	let text = that.find('span').text();

	that
		.toggleClass(`expand-${dest}--opened`)
        .find('span').text(text === trans['Show more'] ? trans['Hide'] : trans['Show more']);

	if (dest === 'rows') {
		that
			.prevUntil('tr:not(.row--hidden)')
			.toggle();
	} else {
		$(`.${dest}`)
			.find('.row--hidden')
			.toggle();
	}
}

function scrollPage(event, index = false) {
	index = index ? index : $(this).data('index');

	let target = $(`[data-target="${index}"]`),
		stop   = target.offset().top,
		delay  = 500;

	if ($(this).is('.responses__row')) {
		stop -= 40;
		highlightRow(target, delay);
	}

	$(`.${sideBtnActiveClass}`).removeClass(sideBtnActiveClass);
	$(`.sidenav__btn--inner[data-index="${index}"]`).addClass(sideBtnActiveClass);

	$('html, body').animate({scrollTop: stop + 'px'}, delay);
}

function highlightRow(target, delay) {
	setTimeout(function() {
		target.addClass('highlight-row');
	}, delay);

	setTimeout(function() {
		target.removeClass('highlight-row');
	}, delay + 2000);
}

const sidenav     = $('.container__left-col'),
	stickyClass   = 'container__left-col--sticky',
	bottomClass   = 'container__left-col--bottom-sticked';

var footerHeight;

if(sidenav.hasClass(('beta'))){
    footerHeight = 500;
} else{
    footerHeight  = 700;
}

let sidenavHeight, scrollPosition, formHeight;

function stickySidebar() {
	sidenavHeight  = sidenav.height();
	scrollPosition = $(document).scrollTop();
	formHeight     = $('.home_first-search').outerHeight(true) + 85;

	if (sidenavHeight + scrollPosition + footerHeight < $(document).height()) {
		if (scrollPosition > formHeight) {
			sidenav
				.addClass(stickyClass)
				.removeClass(bottomClass);
		} else {
			sidenav
				.removeClass(stickyClass)
				.removeClass(bottomClass);
		}
	} else {
		sidenav
			.removeClass(stickyClass)
			.addClass(bottomClass);
	}
}

const sideBtnActiveClass = 'sidenav__btn--active';

let sidebarBtnIndex, mainBlockTarget, currentElOffset, lastElOffset, lastElHeight, clusterHeight;

function highlightSidenavBtn() {
	$('.sidenav__btn').each(function() {

		sidebarBtnIndex = $(this).data('index');
		mainBlockTarget = $(`[data-target="${sidebarBtnIndex}"]`);

		currentElOffset = mainBlockTarget.offset().top;
		lastElOffset    = mainBlockTarget.siblings(':last').offset().top;
		lastElHeight    = mainBlockTarget.siblings(':last').outerHeight();

		clusterHeight = lastElOffset + lastElHeight - currentElOffset;

		if (currentElOffset <= scrollPosition && (currentElOffset + clusterHeight) > scrollPosition) {
			$(`.${sideBtnActiveClass}`).removeClass(sideBtnActiveClass);
			$(this).addClass(sideBtnActiveClass);
		} else {
			$(this).removeClass(sideBtnActiveClass);
		}
	});
}

function initLabels() {
    $('[data-toggle="tooltip"]').tooltip({
        animation: false,
        html: true,
        container: 'body'
    }).attr('data-toggle', 'inited');
}

function getQuestionIcon(text) {
    return `<img src="/assets/images/question.svg" class="question" data-toggle="tooltip" title="${escapeHtml(text)}">`;
}

function escapeHtml(text) {
    if (text) {
        let map = {
            '&': '&amp;',
            '<': '&lt;',
            '>': '&gt;',
            '"': '&quot;',
            "'": '&#039;'
        };

        return text.replace(/[&<>"']/g, function(m) { return map[m]; });
    }

    return text;
}

$.fn.sortElements = (function(){
    let sort = [].sort;

    return function(comparator, getSortable) {
        getSortable = getSortable || function(){return this;};
        let placements = this.map(function(){

            let sortElement = getSortable.call(this),
                parentNode = sortElement.parentNode,
                nextSibling = parentNode.insertBefore(
                    document.createTextNode(''),
                    sortElement.nextSibling
                );
            return function() {
                if (parentNode === this) {
                    throw new Error(
                        "You can't sort elements if any one is a descendant of another."
                    );
                }
                // Insert before flag:
                parentNode.insertBefore(this, nextSibling);
                // Remove flag:
                parentNode.removeChild(nextSibling);
            };
        });

        return sort.call(this, comparator).each(function(i){
            placements[i].call(getSortable.call(this));
        });
    };
})();

function getCookie(name) {
    var matches = document.cookie.match(new RegExp(
        "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
    ));
    return matches ? decodeURIComponent(matches[1]) : false;
}

function setCookie(name,value,days) {
    var expires = "";
    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days*24*60*60*1000));
        expires = "; expires=" + date.toUTCString();
    }
    document.cookie = name + "=" + value + expires + "; path=/";
}

function eraseCookie(name) {
    setCookie(name,"",-1);
}

function showValidationMock() {
    $('.limit_popup').fadeIn().css('display','flex');
    $('.signup').click();
    $('.header__popup').empty().css('display', 'none');
    $('.signin, .signup').click(function () {
        $('.header__popup').css('display', 'none');
        $('.limit_popup').fadeIn().css('display','flex');
    });
    $('.limit_popup-block .lostpass_seoreport').click(function () {
        $('.header__popup').css('display', 'none');
    });
    $('.limit_popup-block .header__login-checkout').click(function () {
        $('.signin').click();
        $('.header__popup').css('display', 'none');
    });
    $('.limit_popup-block .header__registr-checkout').click(function () {
        $('.signup').click();
        $('.header__popup').css('display', 'none');
    });
    dataLayer.push({ 'event':'autoEvent','eventCategory':'limit_popup', 'eventAction': 'shown'});
    disableScroll();
}

$(window).scroll(function () {
    if ($(this).scrollTop() > 0) {
        $('.scroll_up').fadeIn();
    } else {
        $('.scroll_up').fadeOut();
    }
});

// scroll body to 0px on click
$('.scroll_up').click(function () {
    $('body,html').animate({
        scrollTop: 0
    }, 800);
    return false;
});
