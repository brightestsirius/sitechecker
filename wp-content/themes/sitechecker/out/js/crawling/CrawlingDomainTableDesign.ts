/// <reference path="CrawlingBaseTable.ts"/>
/// <reference path="../../../../../../typings/globals/jquery/index.d.ts" />

namespace Crawling {

    export class CrawlingDomainTableDesign extends BaseTable {

        private baseUrl: string;
        private dictionary: any;
        private availablePagesLimit: number;
        private basicTemplateDirectoryUri: string;

        constructor(apiPath: string, baseUrl: string, availablePagesLimit: number, basicTemplateDirectoryUri: string, dictionary: any) {
            super(apiPath);
            this.baseUrl = baseUrl;
            this.availablePagesLimit = availablePagesLimit;
            this.basicTemplateDirectoryUri = basicTemplateDirectoryUri;
            this.dictionary = dictionary;
        }

        getPageData() {
            return this.doRequest(
                this.getRequestData()
            )
        }

        protected processResult(response: any) {
            const data = response.data;
            this.getTBody().empty();
            if (data.length > 0) {
                this.drawRows(data)
            }
        }

        protected getSortParam(): string {
            return 'weight';
        }

        protected getPagination() {
            return $('#pageTablePagination');
        }

        protected getTable() {
            return $('#domainsTable');
        }

        protected getTBody() {
            return this.getTable().find("tbody");
        }

        protected getCrawlingDate() {
            return $('#crawling_date');
        }

        protected getRequestData() {
            const requestData = {
                action: "crawler_domain_list",
                sortParam: this.sortParam,
                sortBy: this.sortBy,
                pageSize: this.pageSize,
                page: this.page,
            };

            return requestData;
        }

        public sort(column) {
            if (column === this.sortParam) {
                this.sortBy = this.sortBy === 'asc' ? 'desc' : 'asc';
            } else {
                this.sortBy = 'asc';
                this.sortParam = column;
            }

            this.display();
        };

        getDiffSpan(currentCount,  previousCount): string{
            let diffSpan = "";

            if(previousCount !== null){
                const pagesDiff = parseInt(currentCount) - parseInt(previousCount);

                if(pagesDiff > 0){
                    diffSpan= `+${pagesDiff}`;
                }else if(pagesDiff === 0 ) {
                    diffSpan = ``;
                }else{
                    diffSpan = `${pagesDiff}`;
                }
            }

            return diffSpan;
        }

        isDomainInProgress(domainStatus){
            if(parseInt(domainStatus) === 0){
                return 'waiting';
            }else if(parseInt(domainStatus) === 1 || parseInt(domainStatus) === 4){
                return 'inprogress';
            }else{
                return 'finished';
            }
        }

        needsDomainUpdateProgress(domainStatus){
            return parseInt(domainStatus) === 0 || parseInt(domainStatus) === 1 || parseInt(domainStatus) === 4;
        }

        drawRows(rows) {
            rows.forEach((value) => {
                const diffSpanCount = this.getDiffSpan(value.domain_count, value.domain_previous_count);
                const domainStatus = value.domain_status;

                const queueCount = parseInt(value.in_queue_count);
                const favicon_url = value.favicon_url;

                let domain_last_crawling = value.domain_last_crawling;

                if(domain_last_crawling == null){
                    domain_last_crawling = new Date();
                }else{
                    domain_last_crawling = new Date(domain_last_crawling * 1000);
                }

                let dateFormat = domain_last_crawling,
                    options = {year: "numeric", month: "short", day: "numeric", hour: '2-digit', minute: '2-digit'},
                    date = dateFormat.toLocaleDateString("en-us",options);

                this.addTableRow(`
                    <tr data-id="${value.domain_id}" name="${this.isDomainInProgress(domainStatus)}" class="${value.monitoring_enabled === 1 ? '' : 'monitoring__disable'} ${(this.needsDomainUpdateProgress(domainStatus)) ? 'update_progress' : ''}">
                        <td>
                            <div class="dashboard__url">
                                <div class="dashboard__favicon" style="background-image: url(${favicon_url !== null ? favicon_url : ''})"></div>
                                <div class="external-link"><a href="${this.baseUrl}/crawl-report-domain/?id=${value.domain_id}" class="external-link__val">${value.domain_url}</a><a href="${value.domain_url}" target="_blank"><img src="/wp-content/themes/sitechecker/out/img_design/external.svg" alt="external" title="external"></a></div>
                                <p class="dashboard__url-testCase">${this.isDomainInProgress(domainStatus) === 'inprogress' ? 'In Progress' : 'Waiting Slot'}</p>
                            </div>
                        </td>
                        <td>
                            <div class="dashboard__values">
                            <p class="${(this.needsDomainUpdateProgress(domainStatus)) ? 'update_progress_count' : ''}">
                            ${this.needsDomainUpdateProgress(domainStatus) ?
                                this.isValidProgressNum(value.in_progress_count) ? value.in_progress_count : 0
                                :
                                this.isValidProgressNum(value.domain_count) ? value.domain_count : '-'
                            }
                            </p>
                            <span class="${(this.needsDomainUpdateProgress(domainStatus)) ? 'update_progress_count_previous' : ''}">${!this.needsDomainUpdateProgress(domainStatus) ? diffSpanCount : ''}</span>
                            </div>
                        </td>
                        <td>
                            <div class="dashboard__score">
                                ${this.isValidPageScore(domainStatus, value.main_page_score,value.domain_id,value.domain_url)}
                            </div>
                        </td>
                        <td>
                            <div class="dashboard__diagram">
                                ${this.activityChanges(value.last_week_changes_activity)}
                            </div>
                        </td>
                        <td>
                            ${date}
                        </td>
                        <td>
                            <div class="dashboard__links">
                                <a href="${this.baseUrl}/crawl-report-domain/?id=${value.domain_id}" class="btn audit__btn">
                                    <svg width="24" height="23" viewBox="0 0 24 23" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path d="M10.2347 8.37864L8.70534 6.88655C9.1759 6.19789 9.41117 5.39446 9.41117 4.59103C9.41117 2.06596 7.29366 0 4.70559 0C2.11751 0 0 2.06596 0 4.59103C0 7.1161 2.11751 9.18207 4.70559 9.18207C5.52906 9.18207 6.35254 8.95251 7.05838 8.49341L8.5877 9.9855C8.82298 10.215 9.52881 10.6742 10.2347 9.9855C10.7052 9.52639 10.7052 8.83774 10.2347 8.37864ZM4.70559 6.88655C3.41155 6.88655 2.35279 5.85357 2.35279 4.59103C2.35279 3.3285 3.41155 2.29552 4.70559 2.29552C5.99962 2.29552 7.05838 3.3285 7.05838 4.59103C7.05838 5.85357 5.99962 6.88655 4.70559 6.88655Z" transform="translate(12.9403 12.6254)" fill="#ffffff"/>
                                        <path d="M17.646 0H1.1764C0.470559 0 0 0.459103 0 1.14776V21.8074C0 22.4961 0.470559 22.9552 1.1764 22.9552H13.7638C11.8816 21.6926 10.5876 19.6267 10.5876 17.2164C10.5876 13.4288 13.7638 10.3298 17.646 10.3298C17.9989 10.3298 18.4694 10.3298 18.8223 10.4446V1.14776C18.8223 0.459103 18.3518 0 17.646 0ZM9.41117 9.18207H3.52919C2.82335 9.18207 2.35279 8.60819 2.35279 8.03431C2.35279 7.34565 2.82335 6.88655 3.52919 6.88655H9.41117C10.117 6.88655 10.5876 7.34565 10.5876 8.03431C10.5876 8.60819 10.117 9.18207 9.41117 9.18207ZM15.2932 4.59103H3.52919C2.82335 4.59103 2.35279 4.01715 2.35279 3.44327C2.35279 2.75462 2.82335 2.29552 3.52919 2.29552H15.2932C15.999 2.29552 16.4696 2.75462 16.4696 3.44327C16.4696 4.01715 15.999 4.59103 15.2932 4.59103Z" fill="#ffffff"/>
                                    </svg>
                                    Audit
                                </a>
                                <a href="${this.baseUrl}/monitoring/?id=${value.domain_id}" class="btn monitoring__btn" data-id="${value.domain_id}">
                                    <svg width="20" height="23" viewBox="0 0 20 23" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path d="M9.96031 22.8655C11.2493 22.8655 12.3039 21.8365 12.3039 20.5789H7.61671C7.61671 21.8365 8.67133 22.8655 9.96031 22.8655ZM17.577 16.0058V9.71784C17.577 6.17368 15.1162 3.3155 11.718 2.5152V1.71491C11.718 0.800292 10.8978 0 9.96031 0C9.02287 0 8.20261 0.800292 8.20261 1.71491V2.5152C4.80439 3.3155 2.3436 6.17368 2.3436 9.71784V16.0058L0 18.2924V19.4357H19.9206V18.2924L17.577 16.0058Z" fill="#ffffff"/>
                                    </svg>
                                    Monitoring
                                </a>
                            </div>
                        </td>
                        <td>
                        <div class="dashboard__monitoring">
                            <div class="dashboard__monitoring-switch" data-id="${value.domain_id}" class="dashboard__monitoring-switch" name="${value.monitoring_enabled === 1 ? 'checked' : 'unchecked'}">
                            <svg width="20" height="23" viewBox="0 0 20 23" fill="none" xmlns="http://www.w3.org/2000/svg" style="display: ${value.monitoring_enabled === 1 ? 'block' : 'none'}" class="checked">
                            <path d="M10.75 20C11.8824 20 12.8088 19.1 12.8088 18H8.69118C8.69118 19.1 9.61765 20 10.75 20ZM17.4412 14V8.5C17.4412 5.4 15.2794 2.9 12.2941 2.2V1.5C12.2941 0.7 11.5735 0 10.75 0C9.92647 0 9.20588 0.7 9.20588 1.5V2.2C6.22059 2.9 4.05882 5.4 4.05882 8.5V14L2 16V17H19.5V16L17.4412 14Z" fill="black"/><path fill-rule="evenodd" clip-rule="evenodd" d="M5.6 1.4L4.2 0C1.8 1.8 0.2 4.6 0 7.8H2C2.2 5.1 3.5 2.8 5.6 1.4V1.4Z" fill="black"/><path fill-rule="evenodd" clip-rule="evenodd" d="M19 7.8H21C20.8 4.6 19.3 1.8 16.9 0L15.5 1.4C17.5 2.8 18.8 5.1 19 7.8V7.8Z" fill="black"/>
                            </svg>
                            <svg style="display: ${value.monitoring_enabled === 0 ? 'block' : 'none'}" width="20" height="23" viewBox="0 0 20 23" fill="none" xmlns="http://www.w3.org/2000/svg" class="unchecked">
                            <path d="M9.96031 22.8655C11.2493 22.8655 12.3039 21.8365 12.3039 20.5789H7.61671C7.61671 21.8365 8.67133 22.8655 9.96031 22.8655ZM17.577 16.0058V9.71784C17.577 6.17368 15.1162 3.3155 11.718 2.5152V1.71491C11.718 0.800292 10.8978 0 9.96031 0C9.02287 0 8.20261 0.800292 8.20261 1.71491V2.5152C4.80439 3.3155 2.3436 6.17368 2.3436 9.71784V16.0058L0 18.2924V19.4357H19.9206V18.2924L17.577 16.0058Z" fill="#CCCCCC"/>
                            </svg>
                            <span class="tooltip"><span>${value.monitoring_enabled === 1 ? 'Stop' : 'Start'}</span> tracking changes critical for SEO</span>
                            </div>
                            </div>
                        </td>
                        <td>
                            <div class="dashboard__btns">
                                <div class="dashboard__btns-btn">
                                    <span></span>
                                    <span></span>
                                    <span></span>
                                </div>
                                <div class="dashboard__btns-list">
                                    <div class="share__website" name="${value.domain_url}" data-id="${value.domain_id}" data-target="${parseInt(value.crawling_sharing_enabled)}">
                                        <svg data-target="${this.baseUrl}" name="${value.token}" width="15" height="15" viewBox="0 0 15 15" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M12.2417 9.53342C11.3658 9.53342 10.5933 9.94715 10.0886 10.5807L5.40714 8.20691C5.46842 7.98026 5.51248 7.74609 5.51248 7.49963C5.51248 7.23133 5.46016 6.97873 5.38716 6.73294L10.0479 4.37007C10.5499 5.03232 11.3424 5.46517 12.2423 5.46517C13.7668 5.46517 15 4.24243 15 2.73225C15 1.22343 13.7668 0 12.2424 0C10.7214 0 9.48681 1.22343 9.48681 2.73222C9.48681 2.97937 9.53089 3.21422 9.59284 3.44156L4.91209 5.81537C4.40669 5.18114 3.63275 4.76605 2.75555 4.76605C1.23247 4.76605 0 5.99014 0 7.49963C0 9.00911 1.23251 10.2325 2.75555 10.2325C3.65685 10.2325 4.4487 9.79834 4.95269 9.13543L9.61139 11.4983C9.53839 11.7434 9.4854 11.998 9.4854 12.267C9.4854 13.7765 10.72 15 12.241 15C13.7654 15 14.9986 13.7765 14.9986 12.267C14.9993 10.7562 13.7661 9.53342 12.2417 9.53342Z" fill="#BBBBBB"/>
                                        </svg>
                                        Share website</div>
                                    <a class="recrawl__website" href="${this.baseUrl}/crawl-report/?re_crawl=1&id=${value.domain_id}">
                                        <svg width="16" height="15" viewBox="0 0 16 15" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M2.48186 8.4375H0.587178H0.529053V13.125L1.91749 11.8491C3.27779 13.7559 5.5081 15 8.02903 15C11.854 15 15.0096 12.1369 15.4709 8.4375H13.5762C13.1299 11.0981 10.8162 13.125 8.02903 13.125C6.05466 13.125 4.31654 12.1069 3.31342 10.5666L5.62997 8.4375H2.48186Z" fill="#BBBBBB"/>
                                            <path d="M8.02901 0C4.20402 0 1.04841 2.86312 0.587158 6.56248H2.48184C2.92809 3.90186 5.24183 1.87499 8.02901 1.87499C10.0624 1.87499 11.8446 2.95405 12.8327 4.57124L10.8415 6.56248H12.7165H13.5762H15.4709H15.529V1.87499L14.1865 3.21749C12.8318 1.27218 10.579 0 8.02901 0Z" fill="#BBBBBB"/>
                                        </svg>
                                        Run new audit</a>
                                    <div class="delete__website" name="${value.domain_id}">
                                        <svg width="13" height="15" viewBox="0 0 13 15" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path fill-rule="evenodd" clip-rule="evenodd" d="M1.36239 13.3335C1.36239 14.2501 2.11238 15.0001 3.02905 15.0001H9.69569C10.6124 15.0001 11.3624 14.2501 11.3624 13.3335V3.3335H1.36239V13.3335ZM12.1957 0.833331H9.27902L8.44569 0H4.27904L3.44571 0.833331H0.529053V2.49999H12.1957V0.833331Z" fill="#BBBBBB"/>
                                        </svg>
                                        Delete website</div>
                                </div>
                            </div>
                        </td>
                    </tr>
                `);
            });

            $('.dashboard__monitoring-switch').click(function () {
                const apiPath = "/wp-admin/admin-ajax.php";
                let action = 'monitoring_enable',
                    btn = $(this),
                    status = btn.attr('name'),
                    domainID = btn.attr('data-id');
                if(status === 'checked'){
                    action = 'monitoring_disable';
                }
                $(btn).css('pointer-events','none');
                $.ajax({
                    url: apiPath,
                    type: "POST",
                    dataType: "JSON",
                    data: {
                        id: domainID,
                        action: action,
                    },
                    success: function (data) {
                        $.ajax({
                            url: apiPath,
                            type: "POST",
                            dataType: "JSON",
                            data: {
                                action: "crawler_get_limits",
                            },
                            success: function (limits) {
                                let monitoring = limits.data.availbleMonitoring;
                                $('#available_monitorings').text(monitoring);
                                if (monitoring > 0) {
                                    $('.monitoring__limit-banner').fadeOut();
                                    $('.dashboard__monitoring-switch[name="exceeded"]').attr('name','unchecked').find('.tooltip').html('<span>Start</span> tracking changes critical for SEO');
                                }

                                if(data.success === true){
                                    if(action === 'monitoring_enable'){
                                        btn.attr('name','checked');
                                        $('tr[data-id="'+domainID+'"]').removeClass('monitoring__disable ');
                                        btn.find('.checked').css('display','block');
                                        btn.find('.unchecked').css('display','none');
                                        btn.find('.tooltip span').text('Stop');
                                    }else if(action === 'monitoring_disable'){
                                        $('tr[data-id="'+domainID+'"]').addClass('monitoring__disable ');
                                        btn.attr('name','unchecked');
                                        btn.find('.unchecked').css('display','block');
                                        btn.find('.checked').css('display','none');
                                        btn.find('.tooltip span').text('Start');
                                    }
                                }else if(data.success === false && data.status === 'no exceeded'){
                                    btn.attr('name','exceeded');
                                    btn.parent().find('.tooltip').text('Upgrade Plan');

                                    $('.monitoring__limit-banner').fadeIn().css('display','flex').append('<script>dataLayer.push({ \'event\': \'autoEvent\', \'eventCategory\': \'Monitoring limited popup shown\', \'eventAction\': \'shown\' });</script>');
                                }
                                $(btn).css('pointer-events','auto');
                            }
                        });
                    }
                });
            });

            $('#enable_sharing').change(function () {
                const apiPath = "/wp-admin/admin-ajax.php";
                $.ajax({
                    url: apiPath,
                    type: "POST",
                    dataType: "JSON",
                    data: {
                        id:  $('.share__popup').attr('data-id'),
                        action: 'enable_disable_crawling_sharing',
                    },
                    success: function (data) {
                        console.log(data);
                        let popupId = $('.share__popup').attr('data-id'),
                            img_attr = $('.share__website[data-id='+popupId+']');
                        if(data.enabled === 1){
                            img_attr.attr('data-target', '1');
                            $('.share__popup-body button').removeClass('btn-disabled');
                        } else{
                            img_attr.attr('data-target', '0');
                            $('.share__popup-body button').addClass('btn-disabled');
                        }
                    }
                });
                return false;
            });

            $('.share__website').click(function () {
                let enabled_sharing = $(this).attr('data-target');
                if(parseInt(enabled_sharing) === 1){
                    $("#enable_sharing").prop("checked", true);
                    $('.share__popup-body button').removeClass('btn-disabled');
                } else{
                    $('.share__popup-body button').addClass('btn-disabled');
                    $("#enable_sharing").prop("checked", false);
                }
                let domainId =  $(this).attr('data-id'),
                    baseUrl = $(this).find('svg').attr('data-target'),
                    token = $(this).find('svg').attr('name'),
                    domainUrl = $(this).attr('name');
                $('#share_url').val(baseUrl+'/crawl-report-domain/?token='+token);
                $('.share__popup-header span').text(domainUrl);
                $('.share__popup').attr('data-id', domainId).fadeIn().css('display','flex');

                function share__invit(){
                    let btn = $('#share_invite'),
                        sharing_email = $('#share_email');

                    if(sharing_email.val() === ''){
                        $(sharing_email).attr('placeholder','Please, type email.')
                    } else{
                        btn.addClass('generating').text('Inviting');
                        $.ajax({
                            url: "/wp-admin/admin-ajax.php",
                            type: "POST",
                            dataType: "JSON",
                            data: {
                                email: sharing_email.val(),
                                link: $('#share_url').val(),
                                action: 'send_share_invite'
                            },
                            success: function (data) {
                                function btnRestart(){
                                    btn.text('Invite');
                                    $('.share__popup-body').append('<p class="invited_urls">'+$('#share_email').val()+' was invited.</p>');
                                    $('#share_email').val('');
                                    return false;
                                }
                                if(data.success === true){
                                    btn.removeClass('generating').text('Invited!');
                                    setTimeout(btnRestart,1000);
                                }
                            }
                        });
                    }
                }

                $('#share_invite').click(function () {
                    share__invit();
                    return false;
                });

                $('#share_email').keypress(function(e) {
                    if(e.which == 13) {
                        share__invit();
                        e.preventDefault();
                        return false;
                    }
                });


                $('.share__popup-header img').click(function () {
                    $('.share__popup').fadeOut();
                    $('#share_url, #share_email').val('');
                    $('.invited_urls').remove();
                })
            });

            this.startProgressUpdater();
        }

        activityChanges(changes){
            changes = JSON.parse(changes);
            if (changes !== null && changes.length>0) {
                let changesBlock = '',
                    changesLength = changes.length,
                    maxCount = 0;
                $.each(changes, function () {
                    let value = parseInt(this.pages_changed_count);
                    if (value>maxCount){
                        maxCount = value
                    }
                });
                $.each(changes, function () {
                    let value = parseInt(this.pages_changed_count),
                        percentage = 0;
                    percentage = ((value*100)/maxCount);
                    changesBlock = changesBlock+'<div class="dashboard__diagram-block"><span class="dashboard__diagram-val" style="height: '+percentage+'%"></span></div>';
                });
                if(changesLength < 7){
                    let index = 7-changesLength,
                        i = 0;
                    while (i < index) {
                        changesBlock = changesBlock+'<div class="dashboard__diagram-block"><span class="dashboard__diagram-val"></span></div>';
                        i++;
                    }
                }
                return changesBlock;
            }else{
                let changesBlock = '',
                    i = 1;
                while (i < 8) {
                    changesBlock = changesBlock+'<div class="dashboard__diagram-block"><span class="dashboard__diagram-val"></span></div>';
                    i++;
                }
                return changesBlock;
            }
        }

        isValidPageScore(status, num, id, url){
            let endColor = 'ok';
            function scoreColor(data) {
                if(data<33){
                    endColor = 'error';
                }else if(data>=33 && data<=65){
                    endColor = 'warning';
                }else if(data>65){
                    endColor = 'ok';
                }
            }
            if(num === null || isNaN(num) || parseInt(status) === 0 || parseInt(status) === 1){
                const host = document.location.host || document.location.hostname,
                    statusUrl = `${document.location.protocol}//api1.${host}/api/v1/full/all/${url}`;
                let statusRequest = $.ajax({
                    url: statusUrl,
                    method: 'GET',
                    dataType: 'json',
                    xhrFields: {
                        withCredentials: true
                    }
                });
                statusRequest.done(function(response) {
                    $.ajax({
                        url: "/wp-admin/admin-ajax.php",
                        type: "POST",
                        async: false,
                        dataType: "JSON",
                        data: {
                            id: id,
                            score: response.data.evaluation,
                            action: 'update_user_domain_main_page_score',
                        },
                        success: function () {
                            scoreColor(response.data.evaluation);
                            return '<span class="'+endColor+'">'+response.data.evaluation+'</span><span>/100</span>';
                        }
                    });
                });
                return '<img class="dashboard__score-loading" src="/wp-content/themes/sitechecker/out/img_design/loading-blue.svg">';
            }else{
                scoreColor(num);
                return '<span class="'+endColor+'">'+num+'</span><span>/100</span>';
            }
        }

        isValidProgressNum(num){
            return num !== null && !isNaN(num)
        }

        logFailed(log): void {
            console.log("error" + log);
        }

        display() {
            this.getPageData().then(
                data => this.processResult(JSON.parse(data)),
                error => this.logFailed(error)
            )
        };

        startProgressUpdater(){
            const rowsToUpdate = this.getTBody().find('.update_progress');
            const table = this;

            rowsToUpdate.each(function () {
                const currentRow = $(this);
                const spanToUpdateCount = currentRow.find('.update_progress_count');
                const statusToUpdate = currentRow.attr('name');

                function countUpdater(blockName, value) {
                    let $countToBlock = blockName,
                        countTo = value;
                    if(countTo === null || isNaN(countTo)){
                        countTo = 0;
                    }
                    $({
                        countNum: blockName.text()
                    }).animate({
                            countNum: countTo
                        },
                        {
                            duration: 2000,
                            easing: 'swing',
                            step: function () {
                                $countToBlock.text(Math.floor(this.countNum));
                            },
                            complete: function () {
                                $countToBlock.text(this.countNum);
                            }
                        });
                }

                const id = currentRow.data('id');

                const updater = setInterval(function () {
                    $.ajax({
                        url: "/wp-admin/admin-ajax.php",
                        type: "POST",
                        async:  false,
                        dataType: "JSON",
                        data: {
                            domainId: id,
                            action: 'crawler_in_progress_statistic',
                        },
                        success: function (data) {
                            if(parseInt(data.status) === 0){
                                countUpdater(spanToUpdateCount, parseInt(data.in_progress_count));
                                currentRow.attr('name','waiting');
                                currentRow.find('.dashboard__url-testCase').text('Waiting slot');
                            }
                            else if(parseInt(data.status) === 1){
                                countUpdater(spanToUpdateCount, parseInt(data.in_progress_count));
                                currentRow.attr('name','inprogress');
                                currentRow.find('.dashboard__url-testCase').text('In progress');
                            }else if(parseInt(data.status) == 2){
                                currentRow.find('.dashboard__url-testCase').text('');
                                clearInterval(updater);
                                $('.dashboard__testCase').hide();
                                table.init();
                                table.updateLimits();
                            }else{
                                currentRow.find('.dashboard__url-testCase').text('');
                            }
                        }
                    });
                }, 5000);
            });
        }

        crawlConfirmClose(){
            $('.addCrawl-block').css('display', 'none');
            $('.addCrawl_popup').fadeOut();
        }

        updateLimits(){
            this.doRequest({
                action: "crawler_get_limits"
            }).then(
                resultJSON => {
                    const result = JSON.parse(resultJSON);

                    if(result.success){
                        const availableDomains = parseInt(result.data.availableDomains) < 0 ? 0 : parseInt(result.data.availableDomains),
                            availableMonitoringDomains = parseInt(result.data.availbleMonitoring) < 0 ? 0 : parseInt(result.data.availbleMonitoring);
                        this.availablePagesLimit = parseInt(result.data.availablePages) < 0 ? 0 : parseInt(result.data.availablePages)  ;

                        $("#available_pages").text(this.availablePagesLimit);
                        $("#available_domains").text(availableDomains);
                        $("#available_monitorings").text(availableMonitoringDomains);

                        let monitoring__close = $('.monitoring__limit-banner').hasClass('monitoring__close'),
                            audit__close = $('.audit__limit-banner').hasClass('audit__close');

                        if(availableDomains > 0){
                            $('.audit__limit-banner').fadeOut();
                        }

                        if(availableDomains <= 0 && !audit__close){
                            $('.audit__limit-banner').fadeIn().css('display','flex');
                            $('.audit__limits-container').append('<script>dataLayer.push({ \'event\': \'autoEvent\', \'eventCategory\': \'Crawling limited popup shown\', \'eventAction\': \'shown\' });</script>');
                        } else if(availableMonitoringDomains <=0 && !monitoring__close && !audit__close){
                            $('.monitoring__limit-banner').fadeIn().css('display','flex');
                            $('.audit__limits-container').append('<script>dataLayer.push({ \'event\': \'autoEvent\', \'eventCategory\': \'Monitoring limited popup shown\', \'eventAction\': \'shown\' });</script>');
                        }else if(availableMonitoringDomains > 0){
                            $('.monitoring__limit-banner').fadeOut();
                        }

                        let pagesLimit = this.availablePagesLimit,
                            slider = $('#settings__pages');
                        $('#settings__pages-value').val(pagesLimit);
                        setTimeout(function () {
                            slider.data('slider').options.max = pagesLimit;
                            slider.slider('setValue', pagesLimit);
                        },1000);

                        if(window.location.href.indexOf('create') > 0){
                            let create = localStorage.getItem('create'),
                                createLogin = localStorage.getItem('createLogin');

                            if(createLogin == 'Shown' || $('.dashboard__testCase').is(":visible")){
                                if(create == 'Shown'){
                                    localStorage.setItem('create', 'false');
                                    localStorage.setItem('createLogin', 'false');
                                    $('.addCrawl_popup .login_popup-close').click(function(){
                                        $('.addCrawl-block').css('display', 'none');
                                        $('.addCrawl_popup').fadeOut();
                                    });
                                    setTimeout(this.crawlConfirmClose, 3000);
                                }
                            } else if(availableDomains !== 0){
                                if(create == 'Shown'){
                                    localStorage.setItem('create', 'false');
                                    localStorage.setItem('createLogin', 'false');
                                    $('.addCrawl_popup .login_popup-close').click(function(){
                                        $('.addCrawl-block').css('display', 'none');
                                        $('.addCrawl_popup').fadeOut();
                                    });
                                    setTimeout(this.crawlConfirmClose, 3000);
                                }
                            }
                        }

                        $('#loader-list').css('opacity', '0');
                        $('.crawlerData').fadeIn();
                    }
                },
                error => {
                    console.log(error);
                }
            )
        }
    }
}