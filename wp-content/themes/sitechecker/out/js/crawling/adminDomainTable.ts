/// <reference path="CrawlingBaseTable.ts"/>
/// <reference path="../../../../../../typings/globals/jquery/index.d.ts" />

namespace Crawling {

    export class AdminDomainTable extends BaseTable {
        getPageData() {
            return this.doRequest(
                this.getRequestData()
            )
        }

        protected processResult(response: any) {
            this.totalCount = response.totalCount;
            this.filteredCount = response.filteredCount;
            const data = response.data;

            if (data.length > 0) {
                this.drawRows(data)
            } else {
                this.addTableRow(`
                    <tr>
                        <td colspan="5">
                            <p class="no__data">No data</p>
                        </td>
                    </tr>
                `);
            }
        }

        private sort_weight: JQuery;
        private sort_url: JQuery;

        constructor(apiPath: string) {
            super(apiPath);

            this.apiPath = apiPath;
            this.sort_url = $('#sort_url');
            this.sort_weight = $('#sort_weight');
        }

        protected getSortParam(): string {
            return 'weight';
        }

        protected getPagination() {
            return $('#pageTablePagination');
        }

        protected getTable() {
            return $('#domainsTable');
        }

        protected getTBody() {
            return this.getTable().find("tbody");
        }

        protected getCrawlingDate() {
            return $('#crawling_date');
        }

        protected setPagination() {
            const html = `
                <div>
                    <div class="dataTables_info" id="pageTable_info" role="status" aria-live="polite">
                        Showing ${((this.page * this.pageSize) - this.pageSize)} to ${this.page * this.pageSize > this.totalCount ? this.totalCount : this.page * this.pageSize}  of ${this.totalCount} entries
                    </div>
                </div>
                <div>
                    <div class="dataTables_paginate paging_simple_numbers" id="pageTable_paginate">
                        <ul class="pagination">
                            <li class="page-item disabled">
                                <span>Page ${this.page} from ${this.totalPages}</span>
                            </li>
                            <li class="paginate_button page-item previous ${ this.page > 1 ? '' : 'disabled'}">
                                <a href="javascript:void(0);" aria-controls="pageTable" class="page-link" ${this.page > 1 ? `onclick="crawlingTable.previousPage();"` : ''}  >Previous</a>
                            </li>
                            <li class="paginate_button page-item ${this.page < this.totalPages ? '' : 'disabled'}">
                                <a href="javascript:void(0);" aria-controls="pageTable"  class="page-link" ${this.page < this.totalPages ? `onclick="crawlingTable.nextPage()"` : ''} >Next</a>
                            </li>
                        </ul>
                    </div>
                </div>
            `;

            this.getPagination().html(html);
        };

        protected getRequestData() {
            const requestData = {
                action: "crawler_admin_domain_request",
                sortParam: this.sortParam,
                sortBy: this.sortBy,
                pageSize: this.pageSize,
                page: this.page,
            };

            return requestData;
        }

        public sort(column) {
            if (column === this.sortParam) {
                this.sortBy = this.sortBy === 'asc' ? 'desc' : 'asc';
            } else {
                this.sortBy = 'asc';
                this.sortParam = column;
            }

            this.sort_url.attr('class', 'sorting');
            this.sort_weight.attr('class', 'sorting');

            if (column === 'weight') {
                this.sort_weight.attr('class', 'sorting_' + this.sortBy);
            } else {
                this.sort_url.attr('class', 'sorting_' + this.sortBy);
            }

            this.display();
        };

        drawRows(rows) {
            rows.forEach((value, index) => {
                const dateCrawlingFinished = value.domain_last_date_crawling !== null ? new Date(parseInt(value.domain_last_date_crawling) * 1000).toLocaleString() : '-';

                this.addTableRow(`
                    <tr class="domain_statistic">
                        <td class="domain_statistic-href">
                            <div class="domain_links">
                                <a href="/crawl-report-domain/?id=${value.domain_id}">${value.domain_url}</a>
                                <a class="external-link" href="${value.domain_url}" target="_blank">
                                    <img src="/wp-content/themes/sitechecker/out/img/external-link.svg" alt="external-link" title="external-link">
                                </a>
                            </div>
                        </td>
                        <td><span class="crawler-domain-status ${parseInt(value.domain_status) === 2 ? 'completed' : 'inprogress'}">${parseInt(value.domain_status) === 2 ? 'Completed' : 'In Progress'}</span></td>
                        <td><span class="crawler-domain-status completed">${value.domain_all_pages_count}</span></td>
                        <td class="crawler-domain-data">${dateCrawlingFinished}</td>
                    </tr>
                `);
            });
        }

        changeFilter() {
            this.page = 1;
            this.display();
        }

        logFailed(log): void {
            console.log("error" + log)
        }

        display() {
            this.getPageData().then(
                data => this.processResult(JSON.parse(data)),
                error => this.logFailed(error)
            )
        };
    }
}