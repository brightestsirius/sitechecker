/// <reference path="CrawlingBaseTable.ts"/>
/// <reference path="../../../../../../typings/globals/jquery/index.d.ts" />

namespace Crawling {

    export class AdminLogTable extends BaseTable {
        getPageData() {
            return this.doRequest(
                this.getRequestData()
            )
        }

        protected processResult(response: any) {
            this.totalCount = response.totalCount;
            this.filteredCount = response.filteredCount;
            const data = response.data;

            if (data.length > 0) {
                this.drawRows(data)
            } else {
                this.addTableRow(`
                    <tr>
                        <td colspan="5">
                            <p class="no__data">No data</p>
                        </td>
                    </tr>
                `);
            }
        }

        private sort_weight: JQuery;
        private sort_url: JQuery;

        constructor(apiPath: string) {
            super(apiPath);

            this.apiPath = apiPath;
            this.sort_url = $('#sort_url');
            this.sort_weight = $('#sort_weight');
        }

        protected getSortParam(): string {
            return 'weight';
        }

        protected getPagination() {
            return $('#pageTablePagination');
        }

        protected getTable() {
            return $('#domainsTable');
        }

        protected getTBody() {
            return this.getTable().find("tbody");
        }

        protected getCrawlingDate() {
            return $('#crawling_date');
        }

        protected setPagination() {
            const html = `
                <div>
                    <div class="dataTables_info" id="pageTable_info" role="status" aria-live="polite">
                        Showing ${((this.page * this.pageSize) - this.pageSize)} to ${this.page * this.pageSize > this.totalCount ? this.totalCount : this.page * this.pageSize}  of ${this.totalCount} entries
                    </div>
                </div>
                <div>
                    <div class="dataTables_paginate paging_simple_numbers" id="pageTable_paginate">
                        <ul class="pagination">
                            <li class="page-item disabled">
                                <span>Page ${this.page} from ${this.totalPages}</span>
                            </li>
                            <li class="paginate_button page-item previous ${ this.page > 1 ? '' : 'disabled'}">
                                <a href="javascript:void(0);" aria-controls="pageTable" class="page-link" ${this.page > 1 ? `onclick="crawlingTable.previousPage();"` : ''}  >Previous</a>
                            </li>
                            <li class="paginate_button page-item ${this.page < this.totalPages ? '' : 'disabled'}">
                                <a href="javascript:void(0);" aria-controls="pageTable"  class="page-link" ${this.page < this.totalPages ? `onclick="crawlingTable.nextPage()"` : ''} >Next</a>
                            </li>
                        </ul>
                    </div>
                </div>
            `;

            this.getPagination().html(html);
        };

        protected getRequestData() {
            const requestData = this.addFilters({
                action: "crawler_admin_log_request",
                sortParam: this.sortParam,
                sortBy: this.sortBy,
                pageSize: this.pageSize,
                page: this.page,
                searchParamUrl: this.getSearchParamUrl()
            });

            return requestData;
        }

        public sort(column) {
            if (column === this.sortParam) {
                this.sortBy = this.sortBy === 'asc' ? 'desc' : 'asc';
            } else {
                this.sortBy = 'asc';
                this.sortParam = column;
            }

            this.sort_url.attr('class', 'sorting');
            this.sort_weight.attr('class', 'sorting');

            if (column === 'weight') {
                this.sort_weight.attr('class', 'sorting_' + this.sortBy);
            } else {
                this.sort_url.attr('class', 'sorting_' + this.sortBy);
            }

            this.display();
        };

        drawRows(rows) {
            rows.forEach((value, index) => {
                const dateCreate = new Date(value.user_domain_crawling_info_date_create*1000).toLocaleString();
                const dateCrawlingFinished = value.user_domain_crawling_info_date_crawling_finished !== null ? new Date(value.user_domain_crawling_info_date_crawling_finished*1000).toLocaleString() : '-';

                this.addTableRow(`
                    <tr class="domain_statistic">
                        <td class="domain_statistic-href">
                            <div class="domain_links">
                                <a href="${value.domain_url}">${value.domain_url}</a>
                                <a class="external-link" href="${value.domain_url}" target="_blank">
                                    <img src="/wp-content/themes/sitechecker/out/img/external-link.svg" alt="external-link" title="external-link">
                                </a>
                            </div>
                        </td>
                        <td><span class="crawler-domain-status completed">${value.domain_all_pages_count}</span></td>
                        <td class="crawler-domain-data">${dateCreate}</td>
                        <td class="crawler-domain-data">${dateCrawlingFinished}</td>
                        <td class="crawler-domain-urls">${value.user_username}</td>
                        <td class="crawler-domain-urls">${value.user_level === null ? "Free" : value.user_level}</td>
                    </tr>
                `);
            });

            $('[data-toggle="tooltip"]').tooltip({
                'template': `<div class="tooltip" role="tooltip"><div class="tooltip-arrow"></div><div class="tooltip-inner" style="max-width: 500px;"></div></div>`
            })
        }

        addFilters(request) {
            if ($('#option_search').val() !== "") {
                request.searchParamUrl = $('#option_search').val();
            }
            if ($('#option_filter').val() !== "" && $('#option_filter').val() !== "0") {
                request.customFilter = $('#custom_filter').val();
            }

            return request;
        }

        changeFilter() {
            this.page = 1;
            this.display();
        }

        private getSearchParamUrl() {
            return $('#option_search').val();
        }

        getStatusCodeWithColor(statusCode: number) {
            let color = '#f24c27';
            if (statusCode >= 200 && statusCode < 300) {
                color = "#56c504";
            } else if (statusCode > 300 && statusCode < 400) {
                color = "#fd9425"
            }
            return color;
        }

        getStatusCodeIcon(statusCode: number): string {
            if (statusCode >= 200 && statusCode < 300) {
                return "#56c504"
            } else if (statusCode > 300 && statusCode < 400) {
                return "#fd9425";
            } else {
                return "#f24c27";
            }
        }

        logFailed(log): void {
            console.log("error" + log)
        }

        display() {
            this.getPageData().then(
                data => this.processResult(JSON.parse(data)),
                error => this.logFailed(error)
            )
        };
    }
}