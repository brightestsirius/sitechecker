var Crawling;
(function (Crawling) {
    var BaseTable = /** @class */ (function () {
        function BaseTable(requestUrl, crawlerDomainId) {
            this.pageSize = 50;
            this.sortBy = 'desc';
            this.page = 1;
            this.totalPages = 0;
            this.totalCount = 0;
            this.filteredCount = 0;
            this.crawlerDomainId = crawlerDomainId;
            this.requestUrl = requestUrl;
            this.sortParam = this.getSortParam();
        }
        BaseTable.prototype.init = function () {
            this.display();
        };
        BaseTable.prototype.display = function () {
            var _this = this;
            this.getLoader().show();
            this.getTableElements().hide();
            this.getPageData().then(function (data) { return _this.processResult(data); }, function (error) { return _this.logFailed(error); });
        };
        ;
        BaseTable.prototype.logFailed = function (log) {
            this.getCrawlingDate().css("color", "red");
            this.getCrawlingDate().html("Failed to get data");
            console.log(log);
        };
        BaseTable.prototype.nextPage = function () {
            if (this.page * this.pageSize < this.totalCount) {
                this.page++;
                this.display();
            }
        };
        BaseTable.prototype.previousPage = function () {
            if (this.page > 1) {
                this.page--;
                this.display();
            }
        };
        BaseTable.prototype.changePageSize = function (pageSize) {
            this.pageSize = pageSize;
            this.page = 1;
            this.display();
        };
        BaseTable.prototype.getPageData = function () {
            var request = {
                'data': [
                    this.getRequestData()
                ]
            };
            console.log(request);
            return this.doRequest({
                type: "POST",
                url: this.requestUrl,
                dataType: "json",
                contentType: "application/json",
                data: JSON.stringify(request),
            });
        };
        BaseTable.prototype.doRequest = function (ajaxObj) {
            return $.ajax(ajaxObj);
        };
        BaseTable.prototype.addTableRow = function (tr) {
            this.getTBody().append(tr);
        };
        return BaseTable;
    }());
    Crawling.BaseTable = BaseTable;
})(Crawling || (Crawling = {}));
