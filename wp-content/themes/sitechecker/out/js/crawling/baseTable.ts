namespace Crawling{

    export abstract class BaseTable {

        public pageSize: number = 50;
        public sortBy: string = 'desc';
        public page: number = 1;
        public sortParam: string;

        protected totalPages: number = 0;
        protected totalCount: number = 0;
        protected filteredCount: number = 0;
        protected apiPath: string;

        constructor(apiPath: string) {
            this.apiPath = apiPath;
            this.sortParam = this.getSortParam();
        }

        public init(){
            this.display();
        }

        protected abstract getSortParam():string;

        protected abstract getPagination(): JQuery;

        protected abstract getCrawlingDate(): JQuery;

        protected abstract getTable(): JQuery;

        protected abstract getTBody(): JQuery;

        protected abstract getRequestData();

        protected abstract drawRows(rows);

        protected abstract processResult(data: any);

        protected abstract logFailed(error): void;

        protected abstract display();

        abstract sort(column: string);

        abstract getPageData();

        nextPage() {
            if (this.page * this.pageSize < this.totalCount) {
                this.page++;
                this.display();
            }
        }

        previousPage() {
            if (this.page > 1) {
                this.page--;
                this.display();
            }
        }

        changePageSize(pageSize) {
            this.pageSize = pageSize;
            this.page = 1;
            this.display();
        }

        doRequest(data = {}) {
            return $.post(this.apiPath, data)
        }

        addTableRow(tr: string) {
            this.getTBody().append(tr);
        }
    }
}
