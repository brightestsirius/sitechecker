var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var Crawling;
(function (Crawling) {
    var BaseTable = /** @class */ (function () {
        function BaseTable(apiPath) {
            this.pageSize = 50;
            this.sortBy = 'desc';
            this.page = 1;
            this.totalPages = 0;
            this.totalCount = 0;
            this.filteredCount = 0;
            this.apiPath = apiPath;
            this.sortParam = this.getSortParam();
        }
        BaseTable.prototype.init = function () {
            this.display();
        };
        BaseTable.prototype.nextPage = function () {
            if (this.page * this.pageSize < this.totalCount) {
                this.page++;
                this.display();
            }
        };
        BaseTable.prototype.previousPage = function () {
            if (this.page > 1) {
                this.page--;
                this.display();
            }
        };
        BaseTable.prototype.changePageSize = function (pageSize) {
            this.pageSize = pageSize;
            this.page = 1;
            this.display();
        };
        BaseTable.prototype.doRequest = function (data) {
            if (data === void 0) { data = {}; }
            return $.post(this.apiPath, data);
        };
        BaseTable.prototype.addTableRow = function (tr) {
            this.getTBody().append(tr);
        };
        return BaseTable;
    }());
    Crawling.BaseTable = BaseTable;
})(Crawling || (Crawling = {}));
/// <reference path="baseTable.ts"/>
var Crawling;
(function (Crawling) {
    var CrawlingBaseTable = /** @class */ (function (_super) {
        __extends(CrawlingBaseTable, _super);
        function CrawlingBaseTable(apiPath, requestPath, token) {
            var _this = _super.call(this, apiPath) || this;
            _this.token = token;
            _this.requestPath = requestPath;
            return _this;
        }
        CrawlingBaseTable.prototype.getLoader = function () {
            return $('.loader');
        };
        CrawlingBaseTable.prototype.processResult = function (data) {
            var result = data[0];
            var message = result.message, summary = result.summary;
            if (message !== 'success') {
                this.getCrawlingDate().css("color", "red");
                this.getCrawlingDate().html("Crawling in progress");
            }
            else {
                this.totalCount = result.totalCount;
                this.filteredCount = result.filteredCount;
                this.totalPages = result.pages;
                this.setPagination();
                this.getTBody().empty();
                if (summary.length > 0) {
                    this.drawRows(summary);
                }
                else {
                    $('#popup_table').css('display', 'table');
                    this.addTableRow("\n                        <tr>\n                            <td colspan=\"5\">\n                                <p class=\"no__data\">No data</p>\n                            </td>\n                        </tr>\n                    ");
                }
            }
            this.getTableElements().show();
            this.hideLoader();
        };
        ;
        CrawlingBaseTable.prototype.hideLoader = function () {
            this.getLoader().css('opacity', '0');
        };
        CrawlingBaseTable.prototype.getStatusCodeWithColor = function (statusCode) {
            var color = 'red';
            if (statusCode >= 200 && statusCode < 300) {
                color = "#6fc66f";
            }
            else if (statusCode > 300 && statusCode < 400) {
                color = "#f7a72c";
            }
            return color;
        };
        CrawlingBaseTable.prototype.getStatusCodeIcon = function (statusCode) {
            if (statusCode >= 200 && statusCode < 300) {
                return "#56c504";
            }
            else if (statusCode > 300 && statusCode < 400) {
                return "#fd9425";
            }
            else {
                return "#f24c27";
            }
        };
        CrawlingBaseTable.prototype.logFailed = function (log) {
            this.getCrawlingDate().css("color", "red");
            this.getCrawlingDate().html("Failed to get data");
            console.log(log);
        };
        CrawlingBaseTable.prototype.display = function () {
            var _this = this;
            this.getLoader().css('opacity', '1');
            this.getPageData().then(function (data) { return _this.processResult(JSON.parse(data)); }, function (error) { return _this.logFailed(error); });
        };
        ;
        CrawlingBaseTable.prototype.getPageData = function () {
            var request = {
                'data': [
                    this.getRequestData()
                ]
            };
            var data = JSON.stringify(request);
            return this.crawlerRequest(data);
        };
        CrawlingBaseTable.prototype.crawlerRequest = function (data) {
            return this.doRequest({
                path: this.requestPath,
                action: 'crawler_request',
                data: data,
                method: 'POST'
            });
        };
        return CrawlingBaseTable;
    }(Crawling.BaseTable));
    Crawling.CrawlingBaseTable = CrawlingBaseTable;
})(Crawling || (Crawling = {}));
/// <reference path="CrawlingBaseTable.ts"/>
/// <reference path="../../../../../../typings/globals/jquery/index.d.ts" />
var Crawling;
(function (Crawling) {
    var AdminDomainTable = /** @class */ (function (_super) {
        __extends(AdminDomainTable, _super);
        function AdminDomainTable(apiPath) {
            var _this = _super.call(this, apiPath) || this;
            _this.apiPath = apiPath;
            _this.sort_url = $('#sort_url');
            _this.sort_weight = $('#sort_weight');
            return _this;
        }
        AdminDomainTable.prototype.getPageData = function () {
            return this.doRequest(this.getRequestData());
        };
        AdminDomainTable.prototype.processResult = function (response) {
            this.totalCount = response.totalCount;
            this.filteredCount = response.filteredCount;
            var data = response.data;
            if (data.length > 0) {
                this.drawRows(data);
            }
            else {
                this.addTableRow("\n                    <tr>\n                        <td colspan=\"5\">\n                            <p class=\"no__data\">No data</p>\n                        </td>\n                    </tr>\n                ");
            }
        };
        AdminDomainTable.prototype.getSortParam = function () {
            return 'weight';
        };
        AdminDomainTable.prototype.getPagination = function () {
            return $('#pageTablePagination');
        };
        AdminDomainTable.prototype.getTable = function () {
            return $('#domainsTable');
        };
        AdminDomainTable.prototype.getTBody = function () {
            return this.getTable().find("tbody");
        };
        AdminDomainTable.prototype.getCrawlingDate = function () {
            return $('#crawling_date');
        };
        AdminDomainTable.prototype.setPagination = function () {
            var html = "\n                <div>\n                    <div class=\"dataTables_info\" id=\"pageTable_info\" role=\"status\" aria-live=\"polite\">\n                        Showing " + ((this.page * this.pageSize) - this.pageSize) + " to " + (this.page * this.pageSize > this.totalCount ? this.totalCount : this.page * this.pageSize) + "  of " + this.totalCount + " entries\n                    </div>\n                </div>\n                <div>\n                    <div class=\"dataTables_paginate paging_simple_numbers\" id=\"pageTable_paginate\">\n                        <ul class=\"pagination\">\n                            <li class=\"page-item disabled\">\n                                <span>Page " + this.page + " from " + this.totalPages + "</span>\n                            </li>\n                            <li class=\"paginate_button page-item previous " + (this.page > 1 ? '' : 'disabled') + "\">\n                                <a href=\"javascript:void(0);\" aria-controls=\"pageTable\" class=\"page-link\" " + (this.page > 1 ? "onclick=\"crawlingTable.previousPage();\"" : '') + "  >Previous</a>\n                            </li>\n                            <li class=\"paginate_button page-item " + (this.page < this.totalPages ? '' : 'disabled') + "\">\n                                <a href=\"javascript:void(0);\" aria-controls=\"pageTable\"  class=\"page-link\" " + (this.page < this.totalPages ? "onclick=\"crawlingTable.nextPage()\"" : '') + " >Next</a>\n                            </li>\n                        </ul>\n                    </div>\n                </div>\n            ";
            this.getPagination().html(html);
        };
        ;
        AdminDomainTable.prototype.getRequestData = function () {
            var requestData = {
                action: "crawler_admin_domain_request",
                sortParam: this.sortParam,
                sortBy: this.sortBy,
                pageSize: this.pageSize,
                page: this.page
            };
            return requestData;
        };
        AdminDomainTable.prototype.sort = function (column) {
            if (column === this.sortParam) {
                this.sortBy = this.sortBy === 'asc' ? 'desc' : 'asc';
            }
            else {
                this.sortBy = 'asc';
                this.sortParam = column;
            }
            this.sort_url.attr('class', 'sorting');
            this.sort_weight.attr('class', 'sorting');
            if (column === 'weight') {
                this.sort_weight.attr('class', 'sorting_' + this.sortBy);
            }
            else {
                this.sort_url.attr('class', 'sorting_' + this.sortBy);
            }
            this.display();
        };
        ;
        AdminDomainTable.prototype.drawRows = function (rows) {
            var _this = this;
            rows.forEach(function (value, index) {
                var dateCrawlingFinished = value.domain_last_date_crawling !== null ? new Date(parseInt(value.domain_last_date_crawling) * 1000).toLocaleString() : '-';
                _this.addTableRow("\n                    <tr class=\"domain_statistic\">\n                        <td class=\"domain_statistic-href\">\n                            <div class=\"domain_links\">\n                                <a href=\"/crawl-report-domain/?id=" + value.domain_id + "\">" + value.domain_url + "</a>\n                                <a class=\"external-link\" href=\"" + value.domain_url + "\" target=\"_blank\">\n                                    <img src=\"/wp-content/themes/sitechecker/out/img/external-link.svg\" alt=\"external-link\" title=\"external-link\">\n                                </a>\n                            </div>\n                        </td>\n                        <td><span class=\"crawler-domain-status " + (parseInt(value.domain_status) === 2 ? 'completed' : 'inprogress') + "\">" + (parseInt(value.domain_status) === 2 ? 'Completed' : 'In Progress') + "</span></td>\n                        <td><span class=\"crawler-domain-status completed\">" + value.domain_all_pages_count + "</span></td>\n                        <td class=\"crawler-domain-data\">" + dateCrawlingFinished + "</td>\n                    </tr>\n                ");
            });
        };
        AdminDomainTable.prototype.changeFilter = function () {
            this.page = 1;
            this.display();
        };
        AdminDomainTable.prototype.logFailed = function (log) {
            console.log("error" + log);
        };
        AdminDomainTable.prototype.display = function () {
            var _this = this;
            this.getPageData().then(function (data) { return _this.processResult(JSON.parse(data)); }, function (error) { return _this.logFailed(error); });
        };
        ;
        return AdminDomainTable;
    }(Crawling.BaseTable));
    Crawling.AdminDomainTable = AdminDomainTable;
})(Crawling || (Crawling = {}));
/// <reference path="CrawlingBaseTable.ts"/>
/// <reference path="../../../../../../typings/globals/jquery/index.d.ts" />
var Crawling;
(function (Crawling) {
    var AdminLogTable = /** @class */ (function (_super) {
        __extends(AdminLogTable, _super);
        function AdminLogTable(apiPath) {
            var _this = _super.call(this, apiPath) || this;
            _this.apiPath = apiPath;
            _this.sort_url = $('#sort_url');
            _this.sort_weight = $('#sort_weight');
            return _this;
        }
        AdminLogTable.prototype.getPageData = function () {
            return this.doRequest(this.getRequestData());
        };
        AdminLogTable.prototype.processResult = function (response) {
            this.totalCount = response.totalCount;
            this.filteredCount = response.filteredCount;
            var data = response.data;
            if (data.length > 0) {
                this.drawRows(data);
            }
            else {
                this.addTableRow("\n                    <tr>\n                        <td colspan=\"5\">\n                            <p class=\"no__data\">No data</p>\n                        </td>\n                    </tr>\n                ");
            }
        };
        AdminLogTable.prototype.getSortParam = function () {
            return 'weight';
        };
        AdminLogTable.prototype.getPagination = function () {
            return $('#pageTablePagination');
        };
        AdminLogTable.prototype.getTable = function () {
            return $('#domainsTable');
        };
        AdminLogTable.prototype.getTBody = function () {
            return this.getTable().find("tbody");
        };
        AdminLogTable.prototype.getCrawlingDate = function () {
            return $('#crawling_date');
        };
        AdminLogTable.prototype.setPagination = function () {
            var html = "\n                <div>\n                    <div class=\"dataTables_info\" id=\"pageTable_info\" role=\"status\" aria-live=\"polite\">\n                        Showing " + ((this.page * this.pageSize) - this.pageSize) + " to " + (this.page * this.pageSize > this.totalCount ? this.totalCount : this.page * this.pageSize) + "  of " + this.totalCount + " entries\n                    </div>\n                </div>\n                <div>\n                    <div class=\"dataTables_paginate paging_simple_numbers\" id=\"pageTable_paginate\">\n                        <ul class=\"pagination\">\n                            <li class=\"page-item disabled\">\n                                <span>Page " + this.page + " from " + this.totalPages + "</span>\n                            </li>\n                            <li class=\"paginate_button page-item previous " + (this.page > 1 ? '' : 'disabled') + "\">\n                                <a href=\"javascript:void(0);\" aria-controls=\"pageTable\" class=\"page-link\" " + (this.page > 1 ? "onclick=\"crawlingTable.previousPage();\"" : '') + "  >Previous</a>\n                            </li>\n                            <li class=\"paginate_button page-item " + (this.page < this.totalPages ? '' : 'disabled') + "\">\n                                <a href=\"javascript:void(0);\" aria-controls=\"pageTable\"  class=\"page-link\" " + (this.page < this.totalPages ? "onclick=\"crawlingTable.nextPage()\"" : '') + " >Next</a>\n                            </li>\n                        </ul>\n                    </div>\n                </div>\n            ";
            this.getPagination().html(html);
        };
        ;
        AdminLogTable.prototype.getRequestData = function () {
            var requestData = this.addFilters({
                action: "crawler_admin_log_request",
                sortParam: this.sortParam,
                sortBy: this.sortBy,
                pageSize: this.pageSize,
                page: this.page,
                searchParamUrl: this.getSearchParamUrl()
            });
            return requestData;
        };
        AdminLogTable.prototype.sort = function (column) {
            if (column === this.sortParam) {
                this.sortBy = this.sortBy === 'asc' ? 'desc' : 'asc';
            }
            else {
                this.sortBy = 'asc';
                this.sortParam = column;
            }
            this.sort_url.attr('class', 'sorting');
            this.sort_weight.attr('class', 'sorting');
            if (column === 'weight') {
                this.sort_weight.attr('class', 'sorting_' + this.sortBy);
            }
            else {
                this.sort_url.attr('class', 'sorting_' + this.sortBy);
            }
            this.display();
        };
        ;
        AdminLogTable.prototype.drawRows = function (rows) {
            var _this = this;
            rows.forEach(function (value, index) {
                var dateCreate = new Date(value.user_domain_crawling_info_date_create * 1000).toLocaleString();
                var dateCrawlingFinished = value.user_domain_crawling_info_date_crawling_finished !== null ? new Date(value.user_domain_crawling_info_date_crawling_finished * 1000).toLocaleString() : '-';
                _this.addTableRow("\n                    <tr class=\"domain_statistic\">\n                        <td class=\"domain_statistic-href\">\n                            <div class=\"domain_links\">\n                                <a href=\"" + value.domain_url + "\">" + value.domain_url + "</a>\n                                <a class=\"external-link\" href=\"" + value.domain_url + "\" target=\"_blank\">\n                                    <img src=\"/wp-content/themes/sitechecker/out/img/external-link.svg\" alt=\"external-link\" title=\"external-link\">\n                                </a>\n                            </div>\n                        </td>\n                        <td><span class=\"crawler-domain-status completed\">" + value.domain_all_pages_count + "</span></td>\n                        <td class=\"crawler-domain-data\">" + dateCreate + "</td>\n                        <td class=\"crawler-domain-data\">" + dateCrawlingFinished + "</td>\n                        <td class=\"crawler-domain-urls\">" + value.user_username + "</td>\n                        <td class=\"crawler-domain-urls\">" + (value.user_level === null ? "Free" : value.user_level) + "</td>\n                    </tr>\n                ");
            });
            $('[data-toggle="tooltip"]').tooltip({
                'template': "<div class=\"tooltip\" role=\"tooltip\"><div class=\"tooltip-arrow\"></div><div class=\"tooltip-inner\" style=\"max-width: 500px;\"></div></div>"
            });
        };
        AdminLogTable.prototype.addFilters = function (request) {
            if ($('#option_search').val() !== "") {
                request.searchParamUrl = $('#option_search').val();
            }
            if ($('#option_filter').val() !== "" && $('#option_filter').val() !== "0") {
                request.customFilter = $('#custom_filter').val();
            }
            return request;
        };
        AdminLogTable.prototype.changeFilter = function () {
            this.page = 1;
            this.display();
        };
        AdminLogTable.prototype.getSearchParamUrl = function () {
            return $('#option_search').val();
        };
        AdminLogTable.prototype.getStatusCodeWithColor = function (statusCode) {
            var color = '#f24c27';
            if (statusCode >= 200 && statusCode < 300) {
                color = "#56c504";
            }
            else if (statusCode > 300 && statusCode < 400) {
                color = "#fd9425";
            }
            return color;
        };
        AdminLogTable.prototype.getStatusCodeIcon = function (statusCode) {
            if (statusCode >= 200 && statusCode < 300) {
                return "#56c504";
            }
            else if (statusCode > 300 && statusCode < 400) {
                return "#fd9425";
            }
            else {
                return "#f24c27";
            }
        };
        AdminLogTable.prototype.logFailed = function (log) {
            console.log("error" + log);
        };
        AdminLogTable.prototype.display = function () {
            var _this = this;
            this.getPageData().then(function (data) { return _this.processResult(JSON.parse(data)); }, function (error) { return _this.logFailed(error); });
        };
        ;
        return AdminLogTable;
    }(Crawling.BaseTable));
    Crawling.AdminLogTable = AdminLogTable;
})(Crawling || (Crawling = {}));
/// <reference path="CrawlingBaseTable.ts"/>
/// <reference path="../../../../../../typings/globals/jquery/index.d.ts" />
var Crawling;
(function (Crawling) {
    var CrawlingDomainTable = /** @class */ (function (_super) {
        __extends(CrawlingDomainTable, _super);
        function CrawlingDomainTable(apiPath, baseUrl, availablePagesLimit, basicTemplateDirectoryUri, dictionary) {
            var _this = _super.call(this, apiPath) || this;
            _this.baseUrl = baseUrl;
            _this.availablePagesLimit = availablePagesLimit;
            _this.basicTemplateDirectoryUri = basicTemplateDirectoryUri;
            _this.dictionary = dictionary;
            return _this;
        }
        CrawlingDomainTable.prototype.getPageData = function () {
            return this.doRequest(this.getRequestData());
        };
        CrawlingDomainTable.prototype.processResult = function (response) {
            var data = response.data;
            this.getTBody().empty();
            if (data.length > 0) {
                this.drawRows(data);
            }
        };
        CrawlingDomainTable.prototype.getSortParam = function () {
            return 'weight';
        };
        CrawlingDomainTable.prototype.getPagination = function () {
            return $('#pageTablePagination');
        };
        CrawlingDomainTable.prototype.getTable = function () {
            return $('#domainsTable');
        };
        CrawlingDomainTable.prototype.getTBody = function () {
            return this.getTable().find("tbody");
        };
        CrawlingDomainTable.prototype.getCrawlingDate = function () {
            return $('#crawling_date');
        };
        CrawlingDomainTable.prototype.getRequestData = function () {
            var requestData = {
                action: "crawler_domain_list",
                sortParam: this.sortParam,
                sortBy: this.sortBy,
                pageSize: this.pageSize,
                page: this.page
            };
            return requestData;
        };
        CrawlingDomainTable.prototype.sort = function (column) {
            if (column === this.sortParam) {
                this.sortBy = this.sortBy === 'asc' ? 'desc' : 'asc';
            }
            else {
                this.sortBy = 'asc';
                this.sortParam = column;
            }
            this.display();
        };
        ;
        CrawlingDomainTable.prototype.getDiffSpan = function (currentCount, previousCount) {
            var diffSpan = "";
            if (previousCount !== null) {
                var pagesDiff = parseInt(currentCount) - parseInt(previousCount);
                if (pagesDiff > 0) {
                    diffSpan = "<span>+" + pagesDiff + "</span>";
                }
                else if (pagesDiff === 0) {
                    diffSpan = "<span></span>";
                }
                else {
                    diffSpan = "<span>" + pagesDiff + "</span>";
                }
            }
            return diffSpan;
        };
        CrawlingDomainTable.prototype.isDomainInProgress = function (domainStatus) {
            return parseInt(domainStatus) === 0 || parseInt(domainStatus) === 1 || parseInt(domainStatus) === 4;
        };
        CrawlingDomainTable.prototype.needsDomainUpdateProgress = function (domainStatus) {
            return parseInt(domainStatus) === 0 || parseInt(domainStatus) === 1 || parseInt(domainStatus) === 4;
        };
        CrawlingDomainTable.prototype.drawRows = function (rows) {
            var _this = this;
            rows.forEach(function (value) {
                var diffSpanCount = _this.getDiffSpan(value.domain_count, value.domain_previous_count);
                var diffSpanCritical = _this.getDiffSpan(value.count_critical, value.previous_count_critical);
                var domainStatus = value.domain_status;
                var queueCount = parseInt(value.in_queue_count);
                var monitoring_enabled, monitoringClass;
                if (value.monitoring_enabled == true) {
                    monitoring_enabled = 'checked';
                    monitoringClass = 'monitoringEnabled';
                }
                else {
                    monitoring_enabled = 'unchecked';
                    monitoringClass = 'monitoringDisabled';
                }
                _this.addTableRow("\n                    <tr class=\"domain_statistic " + ((_this.needsDomainUpdateProgress(domainStatus)) ? 'update_progress' : '') + "\" data-id=\"" + value.domain_id + "\">\n                        <td href=\"" + _this.baseUrl + "/crawl-report-domain/?id=" + value.domain_id + "\" class=\"crawler-domain-url\">\n                            <div class=\"domain_statistic-block\">\n                                <div class=\"domain_links\">\n                                    <a class=\"statistic_link\" href=\"" + _this.baseUrl + "/crawl-report-domain/?id=" + value.domain_id + "\">\n                                        " + value.domain_url + "\n                                    </a>\n                                    <a class=\"external-link\" href=\"" + value.domain_url + "\" target=\"_blank\">\n                                        <img src=\"" + _this.basicTemplateDirectoryUri + "/out/img/external-link.svg\" alt=\"external-link\" title=\"external-link\">\n                                    </a>\n                                </div>\n                            </div>\n                        </td>\n                        <td class=\"crawler-domain-urls\">\n                            <div class=\"domain_status\">\n                                <button type=\"button\" class=\"btn\">\n                                " + ((value.domain_status === 2 && _this.availablePagesLimit > 0) ? '<span class="crawler-domain-status exceeded">Limit Exceeded</span>' : '') + "\n                                " + ((_this.isDomainInProgress(domainStatus)) ? '<span class="crawler-domain-status inprogress">In progress</span>' : '<span class="crawler-domain-status completed">Finished</span>') + "</button>\n                            </div>\n                        </td>\n                        <td class=\"crawler-domain-urls\">\n                            <a href=\"" + _this.baseUrl + "/crawl-report-domain/?id=" + value.domain_id + "\">\n                                <span>\n                                    <span class=\"" + ((_this.needsDomainUpdateProgress(domainStatus)) ? 'update_progress_count' : '') + "\" data-id=\"" + value.domain_id + "\" data-url=\"" + value.domain_url + "\">\n                                       " + (_this.needsDomainUpdateProgress(domainStatus) ?
                    _this.isValidProgressNum(value.in_progress_count) ? value.in_progress_count : 0
                    :
                        _this.isValidProgressNum(value.domain_count) ? value.domain_count : '-') + " \n                                    </span>\n                                    <sub class=\"" + ((_this.needsDomainUpdateProgress(domainStatus)) ? 'update_progress_count_previous' : '') + "\">\n                                        " + (!_this.needsDomainUpdateProgress(domainStatus) ? diffSpanCount : '') + "\n                                    </sub>\n                                </span>\n                            </a>\n                            <div class=\"free_limits\" style=\"display: " + (queueCount > 0 && !$('.crawler_data-info').hasClass('boostaLevel') ? 'inline-block' : 'none') + "\">\n                            <div class=\"pulse1\"></div>\n                            <div class=\"pulse2\"></div>\n                            <div class=\"icon\"></div>\n                                <div><img src=\"/wp-content/themes/sitechecker/out/img/urls_warning.svg\" alt=\"urls_warning\" title=\"urls_warning\">\n                                    <div class=\"tooltip\"><a href=\"/plans/\">Upgrade your Plan now</a> to check all pages on the website.</div>\n                                </div>\n                            </div>\n                        </td>\n                        <td class=\"crawler-domain-urls\">\n                            <a href=\"" + _this.baseUrl + "/crawl-report-domain/?id=" + value.domain_id + "\">\n                                <span>\n                                    <span class=\"" + ((_this.needsDomainUpdateProgress(domainStatus)) ? 'update_progress_count_critical' : '') + "\" data-id=\"" + value.domain_id + "\" data-url=\"" + value.domain_url + "\">\n                                        " + (_this.needsDomainUpdateProgress(domainStatus) ?
                    _this.isValidProgressNum(value.in_progress_count_critical) ? value.in_progress_count_critical : 0
                    :
                        _this.isValidProgressNum(value.count_critical) ? value.count_critical : '-') + "\n                                    </span>\n                                    <sub class=\"" + ((_this.needsDomainUpdateProgress(domainStatus)) ? 'update_progress_count_critical_previous' : '') + "\">\n                                        " + (!_this.needsDomainUpdateProgress(domainStatus) ? diffSpanCritical : '') + "\n                                    </sub>\n                                </span>\n                            </a>\n                        </td>\n                        <td class=\"crawler-domain_urls\">\n                            <label class=\"switch\">\n                                <input type=\"checkbox\" class=\"boosta_monitoring-input monitoring " + monitoringClass + "\" data-id=\"" + value.domain_id + "\" " + monitoring_enabled + ">\n                                <span class=\"slider round\"></span>\n                            </label>\n                        </td>\n                        <td class=\"crawler-domain-recrawl\">\n                            <div class=\"crawler-domain_block\">\n                                <a class=\"audit_btn\" href=\"" + _this.baseUrl + "/crawl-report-domain/?id=" + value.domain_id + "\">\n                                    <img src=\"/wp-content/themes/sitechecker/out/img/audit__icon-active.svg\" alt=\"chart\" title=\"chart\">\n                                    <span>View audit</span>\n                                </a>\n                                <a class=\"changes_btn " + monitoring_enabled + "\" href=\"" + _this.baseUrl + "/monitoring/?id=" + value.domain_id + "\">\n                                    <img src=\"/wp-content/themes/sitechecker/out/img/monitoring__icon" + (monitoring_enabled === 'checked' ? '-active' : '') + ".svg\" alt=\"chart\" title=\"chart\">\n                                    <span>View changes</span>\n                                </a>\n                                <span class=\"share_btn\" name=\"" + value.domain_url + "\" data-id=\"" + value.domain_id + "\">\n                                <input type=\"hidden\" class=\"share_token\" name=\"" + value.token + "\">\n                                    <img src=\"/wp-content/themes/sitechecker/out/img/" + (parseInt(value.crawling_sharing_enabled) === 1 ? 'share__active' : 'share') + ".svg\" alt=\"share\" title=\"share\" name=\"" + _this.baseUrl + "\" data-id=\"" + parseInt(value.crawling_sharing_enabled) + "\">\n                                    <div class=\"tooltip\">Share website</div>\n                                </span>\n                                <a class=\"recrawl_btn " + (parseInt(domainStatus) === 1 || parseInt(domainStatus) === 0 ? 'inprogress' : '') + "\" href=\"" + _this.baseUrl + "/crawl-report/?re_crawl=1&id=" + value.domain_id + "\">\n                                    <img src=\"/wp-content/themes/sitechecker/out/img/" + (parseInt(domainStatus) === 1 || parseInt(domainStatus) === 0 ? 'reload__unactive' : 'reload') + ".svg\" alt=\"recrawl\" title=\"recrawl\">\n                                    <div class=\"tooltip\">Run new audit</div>\n                                </a>\n                                <a class=\"deletecrawl_btn\" name=\"" + value.domain_id + "\">\n                                    <img src=\"/wp-content/themes/sitechecker/out/img/trash.svg\" alt=\"trash\" title=\"trash\">\n                                    <div class=\"tooltip\">Delete website</div>\n                                </a>\n                            </div>\n                        </td>\n                    </tr>\n                ");
            });
            $('.monitoring').change(function () {
                $('.monitoring').removeClass('exceeded');
                var apiPath = "/wp-admin/admin-ajax.php";
                var action, domainID = $(this).attr('data-id'), btn = $('tr[data-id=' + domainID + ']').find('.changes_btn'), switcher = $(this);
                if (switcher.is(':checked')) {
                    switcher.removeClass('monitoringDisabled').addClass('monitoringEnabled');
                    action = 'monitoring_enable';
                    btn.removeClass('unchecked exceeded').addClass('checked').find('img').attr('src', '/wp-content/themes/sitechecker/out/img/monitoring__icon-active.svg');
                }
                else {
                    switcher.removeClass('monitoringEnabled').addClass('monitoringDisabled');
                    action = 'monitoring_disable';
                    btn.removeClass('checked exceeded').addClass('unchecked').find('img').attr('src', '/wp-content/themes/sitechecker/out/img/monitoring__icon.svg');
                }
                $.ajax({
                    url: apiPath,
                    type: "POST",
                    dataType: "JSON",
                    data: {
                        id: domainID,
                        action: action
                    },
                    success: function (data) {
                        console.log(data);
                        if (data.status == 'no exceeded') {
                            switcher.prop('checked', false).removeClass('monitoringEnabled monitoringDisabled').addClass('exceeded');
                            btn.removeClass('checked').addClass('unchecked').find('img').attr('src', '/wp-content/themes/sitechecker/out/img/monitoring__icon.svg');
                        }
                    }
                });
                return false;
            });
            $('#enable_sharing').change(function () {
                var apiPath = "/wp-admin/admin-ajax.php";
                $.ajax({
                    url: apiPath,
                    type: "POST",
                    dataType: "JSON",
                    data: {
                        id: $('.share__popup').attr('data-id'),
                        action: 'enable_disable_crawling_sharing'
                    },
                    success: function (data) {
                        console.log(data);
                        var img_attr = $('.share_btn[data-id=' + $('.share__popup').attr('data-id') + ']').find('img');
                        if (data.enabled === 1) {
                            img_attr.attr('src', '/wp-content/themes/sitechecker/out/img/share__active.svg').attr('data-id', '1');
                            $('.share__popup-body button').removeClass('btn-disabled');
                        }
                        else {
                            img_attr.attr('src', '/wp-content/themes/sitechecker/out/img/share.svg').attr('data-id', '0');
                            $('.share__popup-body button').addClass('btn-disabled');
                        }
                    }
                });
                return false;
            });
            $('.share_btn').click(function () {
                var enabled_sharing = $(this).find('img').attr('data-id');
                if (parseInt(enabled_sharing) === 1) {
                    $("#enable_sharing").prop("checked", true);
                    $('.share__popup-body button').removeClass('btn-disabled');
                }
                else {
                    $('.share__popup-body button').addClass('btn-disabled');
                    $("#enable_sharing").prop("checked", false);
                }
                var domainId = $(this).attr('data-id'), baseUrl = $(this).find('img').attr('name'), token = $(this).find('.share_token').attr('name');
                $('#share_url').val(baseUrl + '/crawl-report-domain/?token=' + token);
                $('.share__popup-header span').text($(this).attr('name'));
                $('.share__popup').attr('data-id', domainId).fadeIn().css('display', 'flex');
                $('body').css({
                    'height': '100%',
                    'overflow': 'hidden'
                });
                function share__invit() {
                    var btn = $('#share_invite'), sharing_email = $('#share_email');
                    if (sharing_email.val() === '') {
                        $(sharing_email).attr('placeholder', 'Please, type email.');
                    }
                    else {
                        btn.addClass('generating').text('Inviting');
                        $.ajax({
                            url: "/wp-admin/admin-ajax.php",
                            type: "POST",
                            dataType: "JSON",
                            data: {
                                email: sharing_email.val(),
                                link: $('#share_url').val(),
                                action: 'send_share_invite'
                            },
                            success: function (data) {
                                function btnRestart() {
                                    btn.text('Invite');
                                    $('.share__popup-body').append('<p class="invited_urls">' + $('#share_email').val() + ' was invited.</p>');
                                    $('#share_email').val('');
                                    return false;
                                }
                                if (data.success === true) {
                                    btn.removeClass('generating').text('Invited!');
                                    setTimeout(btnRestart, 1000);
                                }
                            }
                        });
                    }
                }
                $('#share_invite').click(function () {
                    share__invit();
                });
                $('#share_email').keypress(function (e) {
                    if (e.which == 13) {
                        share__invit();
                        e.preventDefault();
                        return false;
                    }
                });
                $('.share__popup-header img').click(function () {
                    $('.share__popup').fadeOut();
                    $('body').css({
                        'height': 'auto',
                        'overflow': 'auto'
                    });
                    $('#share_url, #share_email').val('');
                    $('.invited_urls').remove();
                });
            });
            $('[data-toggle="tooltip"]').tooltip();
            $(".crawler-domain-urls, .crawler-domain-url").on('click', function (e) {
                var domainLink = $(this).parent('tr');
                document.location.href = domainLink.find('.crawler-domain-url').attr('href');
            });
            $(".external-link").click(function (e) {
                e.stopPropagation();
            });
            $('#loader-list').css('opacity', '0');
            this.startProgressUpdater();
        };
        CrawlingDomainTable.prototype.isValidProgressNum = function (num) {
            return num !== null && !isNaN(num);
        };
        CrawlingDomainTable.prototype.logFailed = function (log) {
            console.log("error" + log);
        };
        CrawlingDomainTable.prototype.display = function () {
            var _this = this;
            this.getPageData().then(function (data) { return _this.processResult(JSON.parse(data)); }, function (error) { return _this.logFailed(error); });
        };
        ;
        CrawlingDomainTable.prototype.startProgressUpdater = function () {
            var rowsToUpdate = this.getTBody().find('.update_progress');
            var table = this;
            rowsToUpdate.each(function () {
                var currentRow = $(this);
                var spanToUpdateCount = currentRow.find('.update_progress_count');
                var spanToUpdateNon200 = currentRow.find('.update_progress_count_non_200');
                var spanToUpdateBlocked = currentRow.find('.update_progress_count_blocked');
                var spanToUpdateCritical = currentRow.find('.update_progress_count_critical');
                function countUpdater(blockName, value) {
                    var $countToBlock = blockName, countTo = value;
                    $({
                        countNum: blockName.text()
                    }).animate({
                        countNum: countTo
                    }, {
                        duration: 2000,
                        easing: 'swing',
                        step: function () {
                            $countToBlock.text(Math.floor(this.countNum));
                        },
                        complete: function () {
                            $countToBlock.text(this.countNum);
                        }
                    });
                }
                var id = currentRow.data('id');
                var updater = setInterval(function () {
                    $.ajax({
                        url: "/wp-admin/admin-ajax.php",
                        type: "POST",
                        async: false,
                        dataType: "JSON",
                        data: {
                            domainId: id,
                            action: 'crawler_in_progress_statistic'
                        },
                        success: function (data) {
                            if (parseInt(data.status) === 1) {
                                countUpdater(spanToUpdateCount, parseInt(data.in_progress_count));
                                countUpdater(spanToUpdateNon200, parseInt(data.in_progress_count_non_200));
                                countUpdater(spanToUpdateBlocked, parseInt(data.in_progress_count_blocked));
                                countUpdater(spanToUpdateCritical, parseInt(data.in_progress_count_critical));
                            }
                            else if (parseInt(data.status) == 2) {
                                clearInterval(updater);
                                $('#testDomain').hide();
                                table.init();
                                table.updateLimits();
                            }
                        }
                    });
                }, 5000);
            });
        };
        CrawlingDomainTable.prototype.crawlConfirmClose = function () {
            $('.addCrawl-block').css('display', 'none');
            $('.addCrawl_popup').fadeOut();
        };
        CrawlingDomainTable.prototype.updateLimits = function () {
            var _this = this;
            this.doRequest({
                action: "crawler_get_limits"
            }).then(function (resultJSON) {
                var result = JSON.parse(resultJSON);
                if (result.success) {
                    var availableDomains = parseInt(result.data.availableDomains) < 0 ? 0 : parseInt(result.data.availableDomains);
                    _this.availablePagesLimit = parseInt(result.data.availablePages) < 0 ? 0 : parseInt(result.data.availablePages);
                    $("#available_pages").text(_this.availablePagesLimit);
                    $("#available_domains").text(availableDomains);
                    if (window.location.href.indexOf('create') > 0) {
                        var create = localStorage.getItem('create'), createLogin = localStorage.getItem('createLogin');
                        if (createLogin == 'Shown' || $('#testDomain').is(":visible")) {
                            if (create == 'Shown') {
                                localStorage.setItem('create', 'false');
                                localStorage.setItem('createLogin', 'false');
                                $('.addCrawl_popup .login_popup-close').click(function () {
                                    $('.addCrawl-block').css('display', 'none');
                                    $('.addCrawl_popup').fadeOut();
                                });
                                setTimeout(_this.crawlConfirmClose, 3000);
                            }
                        }
                        else if (availableDomains !== 0) {
                            if (create == 'Shown') {
                                localStorage.setItem('create', 'false');
                                localStorage.setItem('createLogin', 'false');
                                $('.addCrawl_popup .login_popup-close').click(function () {
                                    $('.addCrawl-block').css('display', 'none');
                                    $('.addCrawl_popup').fadeOut();
                                });
                                setTimeout(_this.crawlConfirmClose, 3000);
                            }
                        }
                    }
                    $('#loader-list').css('opacity', '0');
                    $('.crawlerData').fadeIn();
                }
            }, function (error) {
                console.log(error);
            });
        };
        return CrawlingDomainTable;
    }(Crawling.BaseTable));
    Crawling.CrawlingDomainTable = CrawlingDomainTable;
})(Crawling || (Crawling = {}));
/// <reference path="CrawlingBaseTable.ts"/>
/// <reference path="../../../../../../typings/globals/jquery/index.d.ts" />
var Crawling;
(function (Crawling) {
    var CrawlingDomainTableDesign = /** @class */ (function (_super) {
        __extends(CrawlingDomainTableDesign, _super);
        function CrawlingDomainTableDesign(apiPath, baseUrl, availablePagesLimit, basicTemplateDirectoryUri, dictionary) {
            var _this = _super.call(this, apiPath) || this;
            _this.baseUrl = baseUrl;
            _this.availablePagesLimit = availablePagesLimit;
            _this.basicTemplateDirectoryUri = basicTemplateDirectoryUri;
            _this.dictionary = dictionary;
            return _this;
        }
        CrawlingDomainTableDesign.prototype.getPageData = function () {
            return this.doRequest(this.getRequestData());
        };
        CrawlingDomainTableDesign.prototype.processResult = function (response) {
            var data = response.data;
            this.getTBody().empty();
            if (data.length > 0) {
                this.drawRows(data);
            }
        };
        CrawlingDomainTableDesign.prototype.getSortParam = function () {
            return 'weight';
        };
        CrawlingDomainTableDesign.prototype.getPagination = function () {
            return $('#pageTablePagination');
        };
        CrawlingDomainTableDesign.prototype.getTable = function () {
            return $('#domainsTable');
        };
        CrawlingDomainTableDesign.prototype.getTBody = function () {
            return this.getTable().find("tbody");
        };
        CrawlingDomainTableDesign.prototype.getCrawlingDate = function () {
            return $('#crawling_date');
        };
        CrawlingDomainTableDesign.prototype.getRequestData = function () {
            var requestData = {
                action: "crawler_domain_list",
                sortParam: this.sortParam,
                sortBy: this.sortBy,
                pageSize: this.pageSize,
                page: this.page
            };
            return requestData;
        };
        CrawlingDomainTableDesign.prototype.sort = function (column) {
            if (column === this.sortParam) {
                this.sortBy = this.sortBy === 'asc' ? 'desc' : 'asc';
            }
            else {
                this.sortBy = 'asc';
                this.sortParam = column;
            }
            this.display();
        };
        ;
        CrawlingDomainTableDesign.prototype.getDiffSpan = function (currentCount, previousCount) {
            var diffSpan = "";
            if (previousCount !== null) {
                var pagesDiff = parseInt(currentCount) - parseInt(previousCount);
                if (pagesDiff > 0) {
                    diffSpan = "+" + pagesDiff;
                }
                else if (pagesDiff === 0) {
                    diffSpan = "";
                }
                else {
                    diffSpan = "" + pagesDiff;
                }
            }
            return diffSpan;
        };
        CrawlingDomainTableDesign.prototype.isDomainInProgress = function (domainStatus) {
            if (parseInt(domainStatus) === 0) {
                return 'waiting';
            }
            else if (parseInt(domainStatus) === 1 || parseInt(domainStatus) === 4) {
                return 'inprogress';
            }
            else {
                return 'finished';
            }
        };
        CrawlingDomainTableDesign.prototype.needsDomainUpdateProgress = function (domainStatus) {
            return parseInt(domainStatus) === 0 || parseInt(domainStatus) === 1 || parseInt(domainStatus) === 4;
        };
        CrawlingDomainTableDesign.prototype.drawRows = function (rows) {
            var _this = this;
            rows.forEach(function (value) {
                var diffSpanCount = _this.getDiffSpan(value.domain_count, value.domain_previous_count);
                var domainStatus = value.domain_status;
                var queueCount = parseInt(value.in_queue_count);
                var favicon_url = value.favicon_url;
                var domain_last_crawling = value.domain_last_crawling;
                if (domain_last_crawling == null) {
                    domain_last_crawling = new Date();
                }
                else {
                    domain_last_crawling = new Date(domain_last_crawling * 1000);
                }
                var dateFormat = domain_last_crawling, options = { year: "numeric", month: "short", day: "numeric", hour: '2-digit', minute: '2-digit' }, date = dateFormat.toLocaleDateString("en-us", options);
                _this.addTableRow("\n                    <tr data-id=\"" + value.domain_id + "\" name=\"" + _this.isDomainInProgress(domainStatus) + "\" class=\"" + (value.monitoring_enabled === 1 ? '' : 'monitoring__disable') + " " + ((_this.needsDomainUpdateProgress(domainStatus)) ? 'update_progress' : '') + "\">\n                        <td>\n                            <div class=\"dashboard__url\">\n                                <div class=\"dashboard__favicon\" style=\"background-image: url(" + (favicon_url !== null ? favicon_url : '') + ")\"></div>\n                                <div class=\"external-link\"><a href=\"" + _this.baseUrl + "/crawl-report-domain/?id=" + value.domain_id + "\" class=\"external-link__val\">" + value.domain_url + "</a><a href=\"" + value.domain_url + "\" target=\"_blank\"><img src=\"/wp-content/themes/sitechecker/out/img_design/external.svg\" alt=\"external\" title=\"external\"></a></div>\n                                <p class=\"dashboard__url-testCase\">" + (_this.isDomainInProgress(domainStatus) === 'inprogress' ? 'In Progress' : 'Waiting Slot') + "</p>\n                            </div>\n                        </td>\n                        <td>\n                            <div class=\"dashboard__values\">\n                            <p class=\"" + ((_this.needsDomainUpdateProgress(domainStatus)) ? 'update_progress_count' : '') + "\">\n                            " + (_this.needsDomainUpdateProgress(domainStatus) ?
                    _this.isValidProgressNum(value.in_progress_count) ? value.in_progress_count : 0
                    :
                        _this.isValidProgressNum(value.domain_count) ? value.domain_count : '-') + "\n                            </p>\n                            <span class=\"" + ((_this.needsDomainUpdateProgress(domainStatus)) ? 'update_progress_count_previous' : '') + "\">" + (!_this.needsDomainUpdateProgress(domainStatus) ? diffSpanCount : '') + "</span>\n                            </div>\n                        </td>\n                        <td>\n                            <div class=\"dashboard__score\">\n                                " + _this.isValidPageScore(domainStatus, value.main_page_score, value.domain_id, value.domain_url) + "\n                            </div>\n                        </td>\n                        <td>\n                            <div class=\"dashboard__diagram\">\n                                " + _this.activityChanges(value.last_week_changes_activity) + "\n                            </div>\n                        </td>\n                        <td>\n                            " + date + "\n                        </td>\n                        <td>\n                            <div class=\"dashboard__links\">\n                                <a href=\"" + _this.baseUrl + "/crawl-report-domain/?id=" + value.domain_id + "\" class=\"btn audit__btn\">\n                                    <svg width=\"24\" height=\"23\" viewBox=\"0 0 24 23\" fill=\"none\" xmlns=\"http://www.w3.org/2000/svg\">\n                                        <path d=\"M10.2347 8.37864L8.70534 6.88655C9.1759 6.19789 9.41117 5.39446 9.41117 4.59103C9.41117 2.06596 7.29366 0 4.70559 0C2.11751 0 0 2.06596 0 4.59103C0 7.1161 2.11751 9.18207 4.70559 9.18207C5.52906 9.18207 6.35254 8.95251 7.05838 8.49341L8.5877 9.9855C8.82298 10.215 9.52881 10.6742 10.2347 9.9855C10.7052 9.52639 10.7052 8.83774 10.2347 8.37864ZM4.70559 6.88655C3.41155 6.88655 2.35279 5.85357 2.35279 4.59103C2.35279 3.3285 3.41155 2.29552 4.70559 2.29552C5.99962 2.29552 7.05838 3.3285 7.05838 4.59103C7.05838 5.85357 5.99962 6.88655 4.70559 6.88655Z\" transform=\"translate(12.9403 12.6254)\" fill=\"#ffffff\"/>\n                                        <path d=\"M17.646 0H1.1764C0.470559 0 0 0.459103 0 1.14776V21.8074C0 22.4961 0.470559 22.9552 1.1764 22.9552H13.7638C11.8816 21.6926 10.5876 19.6267 10.5876 17.2164C10.5876 13.4288 13.7638 10.3298 17.646 10.3298C17.9989 10.3298 18.4694 10.3298 18.8223 10.4446V1.14776C18.8223 0.459103 18.3518 0 17.646 0ZM9.41117 9.18207H3.52919C2.82335 9.18207 2.35279 8.60819 2.35279 8.03431C2.35279 7.34565 2.82335 6.88655 3.52919 6.88655H9.41117C10.117 6.88655 10.5876 7.34565 10.5876 8.03431C10.5876 8.60819 10.117 9.18207 9.41117 9.18207ZM15.2932 4.59103H3.52919C2.82335 4.59103 2.35279 4.01715 2.35279 3.44327C2.35279 2.75462 2.82335 2.29552 3.52919 2.29552H15.2932C15.999 2.29552 16.4696 2.75462 16.4696 3.44327C16.4696 4.01715 15.999 4.59103 15.2932 4.59103Z\" fill=\"#ffffff\"/>\n                                    </svg>\n                                    Audit\n                                </a>\n                                <a href=\"" + _this.baseUrl + "/monitoring/?id=" + value.domain_id + "\" class=\"btn monitoring__btn\" data-id=\"" + value.domain_id + "\">\n                                    <svg width=\"20\" height=\"23\" viewBox=\"0 0 20 23\" fill=\"none\" xmlns=\"http://www.w3.org/2000/svg\">\n                                        <path d=\"M9.96031 22.8655C11.2493 22.8655 12.3039 21.8365 12.3039 20.5789H7.61671C7.61671 21.8365 8.67133 22.8655 9.96031 22.8655ZM17.577 16.0058V9.71784C17.577 6.17368 15.1162 3.3155 11.718 2.5152V1.71491C11.718 0.800292 10.8978 0 9.96031 0C9.02287 0 8.20261 0.800292 8.20261 1.71491V2.5152C4.80439 3.3155 2.3436 6.17368 2.3436 9.71784V16.0058L0 18.2924V19.4357H19.9206V18.2924L17.577 16.0058Z\" fill=\"#ffffff\"/>\n                                    </svg>\n                                    Monitoring\n                                </a>\n                            </div>\n                        </td>\n                        <td>\n                        <div class=\"dashboard__monitoring\">\n                            <div class=\"dashboard__monitoring-switch\" data-id=\"" + value.domain_id + "\" class=\"dashboard__monitoring-switch\" name=\"" + (value.monitoring_enabled === 1 ? 'checked' : 'unchecked') + "\">\n                            <svg width=\"20\" height=\"23\" viewBox=\"0 0 20 23\" fill=\"none\" xmlns=\"http://www.w3.org/2000/svg\" style=\"display: " + (value.monitoring_enabled === 1 ? 'block' : 'none') + "\" class=\"checked\">\n                            <path d=\"M10.75 20C11.8824 20 12.8088 19.1 12.8088 18H8.69118C8.69118 19.1 9.61765 20 10.75 20ZM17.4412 14V8.5C17.4412 5.4 15.2794 2.9 12.2941 2.2V1.5C12.2941 0.7 11.5735 0 10.75 0C9.92647 0 9.20588 0.7 9.20588 1.5V2.2C6.22059 2.9 4.05882 5.4 4.05882 8.5V14L2 16V17H19.5V16L17.4412 14Z\" fill=\"black\"/><path fill-rule=\"evenodd\" clip-rule=\"evenodd\" d=\"M5.6 1.4L4.2 0C1.8 1.8 0.2 4.6 0 7.8H2C2.2 5.1 3.5 2.8 5.6 1.4V1.4Z\" fill=\"black\"/><path fill-rule=\"evenodd\" clip-rule=\"evenodd\" d=\"M19 7.8H21C20.8 4.6 19.3 1.8 16.9 0L15.5 1.4C17.5 2.8 18.8 5.1 19 7.8V7.8Z\" fill=\"black\"/>\n                            </svg>\n                            <svg style=\"display: " + (value.monitoring_enabled === 0 ? 'block' : 'none') + "\" width=\"20\" height=\"23\" viewBox=\"0 0 20 23\" fill=\"none\" xmlns=\"http://www.w3.org/2000/svg\" class=\"unchecked\">\n                            <path d=\"M9.96031 22.8655C11.2493 22.8655 12.3039 21.8365 12.3039 20.5789H7.61671C7.61671 21.8365 8.67133 22.8655 9.96031 22.8655ZM17.577 16.0058V9.71784C17.577 6.17368 15.1162 3.3155 11.718 2.5152V1.71491C11.718 0.800292 10.8978 0 9.96031 0C9.02287 0 8.20261 0.800292 8.20261 1.71491V2.5152C4.80439 3.3155 2.3436 6.17368 2.3436 9.71784V16.0058L0 18.2924V19.4357H19.9206V18.2924L17.577 16.0058Z\" fill=\"#CCCCCC\"/>\n                            </svg>\n                            <span class=\"tooltip\"><span>" + (value.monitoring_enabled === 1 ? 'Stop' : 'Start') + "</span> tracking changes critical for SEO</span>\n                            </div>\n                            </div>\n                        </td>\n                        <td>\n                            <div class=\"dashboard__btns\">\n                                <div class=\"dashboard__btns-btn\">\n                                    <span></span>\n                                    <span></span>\n                                    <span></span>\n                                </div>\n                                <div class=\"dashboard__btns-list\">\n                                    <div class=\"share__website\" name=\"" + value.domain_url + "\" data-id=\"" + value.domain_id + "\" data-target=\"" + parseInt(value.crawling_sharing_enabled) + "\">\n                                        <svg data-target=\"" + _this.baseUrl + "\" name=\"" + value.token + "\" width=\"15\" height=\"15\" viewBox=\"0 0 15 15\" fill=\"none\" xmlns=\"http://www.w3.org/2000/svg\">\n                                            <path d=\"M12.2417 9.53342C11.3658 9.53342 10.5933 9.94715 10.0886 10.5807L5.40714 8.20691C5.46842 7.98026 5.51248 7.74609 5.51248 7.49963C5.51248 7.23133 5.46016 6.97873 5.38716 6.73294L10.0479 4.37007C10.5499 5.03232 11.3424 5.46517 12.2423 5.46517C13.7668 5.46517 15 4.24243 15 2.73225C15 1.22343 13.7668 0 12.2424 0C10.7214 0 9.48681 1.22343 9.48681 2.73222C9.48681 2.97937 9.53089 3.21422 9.59284 3.44156L4.91209 5.81537C4.40669 5.18114 3.63275 4.76605 2.75555 4.76605C1.23247 4.76605 0 5.99014 0 7.49963C0 9.00911 1.23251 10.2325 2.75555 10.2325C3.65685 10.2325 4.4487 9.79834 4.95269 9.13543L9.61139 11.4983C9.53839 11.7434 9.4854 11.998 9.4854 12.267C9.4854 13.7765 10.72 15 12.241 15C13.7654 15 14.9986 13.7765 14.9986 12.267C14.9993 10.7562 13.7661 9.53342 12.2417 9.53342Z\" fill=\"#BBBBBB\"/>\n                                        </svg>\n                                        Share website</div>\n                                    <a class=\"recrawl__website\" href=\"" + _this.baseUrl + "/crawl-report/?re_crawl=1&id=" + value.domain_id + "\">\n                                        <svg width=\"16\" height=\"15\" viewBox=\"0 0 16 15\" fill=\"none\" xmlns=\"http://www.w3.org/2000/svg\">\n                                            <path d=\"M2.48186 8.4375H0.587178H0.529053V13.125L1.91749 11.8491C3.27779 13.7559 5.5081 15 8.02903 15C11.854 15 15.0096 12.1369 15.4709 8.4375H13.5762C13.1299 11.0981 10.8162 13.125 8.02903 13.125C6.05466 13.125 4.31654 12.1069 3.31342 10.5666L5.62997 8.4375H2.48186Z\" fill=\"#BBBBBB\"/>\n                                            <path d=\"M8.02901 0C4.20402 0 1.04841 2.86312 0.587158 6.56248H2.48184C2.92809 3.90186 5.24183 1.87499 8.02901 1.87499C10.0624 1.87499 11.8446 2.95405 12.8327 4.57124L10.8415 6.56248H12.7165H13.5762H15.4709H15.529V1.87499L14.1865 3.21749C12.8318 1.27218 10.579 0 8.02901 0Z\" fill=\"#BBBBBB\"/>\n                                        </svg>\n                                        Run new audit</a>\n                                    <div class=\"delete__website\" name=\"" + value.domain_id + "\">\n                                        <svg width=\"13\" height=\"15\" viewBox=\"0 0 13 15\" fill=\"none\" xmlns=\"http://www.w3.org/2000/svg\">\n                                            <path fill-rule=\"evenodd\" clip-rule=\"evenodd\" d=\"M1.36239 13.3335C1.36239 14.2501 2.11238 15.0001 3.02905 15.0001H9.69569C10.6124 15.0001 11.3624 14.2501 11.3624 13.3335V3.3335H1.36239V13.3335ZM12.1957 0.833331H9.27902L8.44569 0H4.27904L3.44571 0.833331H0.529053V2.49999H12.1957V0.833331Z\" fill=\"#BBBBBB\"/>\n                                        </svg>\n                                        Delete website</div>\n                                </div>\n                            </div>\n                        </td>\n                    </tr>\n                ");
            });
            $('.dashboard__monitoring-switch').click(function () {
                var apiPath = "/wp-admin/admin-ajax.php";
                var action = 'monitoring_enable', btn = $(this), status = btn.attr('name'), domainID = btn.attr('data-id');
                if (status === 'checked') {
                    action = 'monitoring_disable';
                }
                $(btn).css('pointer-events', 'none');
                $.ajax({
                    url: apiPath,
                    type: "POST",
                    dataType: "JSON",
                    data: {
                        id: domainID,
                        action: action
                    },
                    success: function (data) {
                        $.ajax({
                            url: apiPath,
                            type: "POST",
                            dataType: "JSON",
                            data: {
                                action: "crawler_get_limits"
                            },
                            success: function (limits) {
                                var monitoring = limits.data.availbleMonitoring;
                                $('#available_monitorings').text(monitoring);
                                if (monitoring > 0) {
                                    $('.monitoring__limit-banner').fadeOut();
                                    $('.dashboard__monitoring-switch[name="exceeded"]').attr('name', 'unchecked').find('.tooltip').html('<span>Start</span> tracking changes critical for SEO');
                                }
                                if (data.success === true) {
                                    if (action === 'monitoring_enable') {
                                        btn.attr('name', 'checked');
                                        $('tr[data-id="' + domainID + '"]').removeClass('monitoring__disable ');
                                        btn.find('.checked').css('display', 'block');
                                        btn.find('.unchecked').css('display', 'none');
                                        btn.find('.tooltip span').text('Stop');
                                    }
                                    else if (action === 'monitoring_disable') {
                                        $('tr[data-id="' + domainID + '"]').addClass('monitoring__disable ');
                                        btn.attr('name', 'unchecked');
                                        btn.find('.unchecked').css('display', 'block');
                                        btn.find('.checked').css('display', 'none');
                                        btn.find('.tooltip span').text('Start');
                                    }
                                }
                                else if (data.success === false && data.status === 'no exceeded') {
                                    btn.attr('name', 'exceeded');
                                    btn.parent().find('.tooltip').text('Upgrade Plan');
                                    $('.monitoring__limit-banner').fadeIn().css('display', 'flex').append('<script>dataLayer.push({ \'event\': \'autoEvent\', \'eventCategory\': \'Monitoring limited popup shown\', \'eventAction\': \'shown\' });</script>');
                                }
                                $(btn).css('pointer-events', 'auto');
                            }
                        });
                    }
                });
            });
            $('#enable_sharing').change(function () {
                var apiPath = "/wp-admin/admin-ajax.php";
                $.ajax({
                    url: apiPath,
                    type: "POST",
                    dataType: "JSON",
                    data: {
                        id: $('.share__popup').attr('data-id'),
                        action: 'enable_disable_crawling_sharing'
                    },
                    success: function (data) {
                        console.log(data);
                        var popupId = $('.share__popup').attr('data-id'), img_attr = $('.share__website[data-id=' + popupId + ']');
                        if (data.enabled === 1) {
                            img_attr.attr('data-target', '1');
                            $('.share__popup-body button').removeClass('btn-disabled');
                        }
                        else {
                            img_attr.attr('data-target', '0');
                            $('.share__popup-body button').addClass('btn-disabled');
                        }
                    }
                });
                return false;
            });
            $('.share__website').click(function () {
                var enabled_sharing = $(this).attr('data-target');
                if (parseInt(enabled_sharing) === 1) {
                    $("#enable_sharing").prop("checked", true);
                    $('.share__popup-body button').removeClass('btn-disabled');
                }
                else {
                    $('.share__popup-body button').addClass('btn-disabled');
                    $("#enable_sharing").prop("checked", false);
                }
                var domainId = $(this).attr('data-id'), baseUrl = $(this).find('svg').attr('data-target'), token = $(this).find('svg').attr('name'), domainUrl = $(this).attr('name');
                $('#share_url').val(baseUrl + '/crawl-report-domain/?token=' + token);
                $('.share__popup-header span').text(domainUrl);
                $('.share__popup').attr('data-id', domainId).fadeIn().css('display', 'flex');
                function share__invit() {
                    var btn = $('#share_invite'), sharing_email = $('#share_email');
                    if (sharing_email.val() === '') {
                        $(sharing_email).attr('placeholder', 'Please, type email.');
                    }
                    else {
                        btn.addClass('generating').text('Inviting');
                        $.ajax({
                            url: "/wp-admin/admin-ajax.php",
                            type: "POST",
                            dataType: "JSON",
                            data: {
                                email: sharing_email.val(),
                                link: $('#share_url').val(),
                                action: 'send_share_invite'
                            },
                            success: function (data) {
                                function btnRestart() {
                                    btn.text('Invite');
                                    $('.share__popup-body').append('<p class="invited_urls">' + $('#share_email').val() + ' was invited.</p>');
                                    $('#share_email').val('');
                                    return false;
                                }
                                if (data.success === true) {
                                    btn.removeClass('generating').text('Invited!');
                                    setTimeout(btnRestart, 1000);
                                }
                            }
                        });
                    }
                }
                $('#share_invite').click(function () {
                    share__invit();
                    return false;
                });
                $('#share_email').keypress(function (e) {
                    if (e.which == 13) {
                        share__invit();
                        e.preventDefault();
                        return false;
                    }
                });
                $('.share__popup-header img').click(function () {
                    $('.share__popup').fadeOut();
                    $('#share_url, #share_email').val('');
                    $('.invited_urls').remove();
                });
            });
            this.startProgressUpdater();
        };
        CrawlingDomainTableDesign.prototype.activityChanges = function (changes) {
            changes = JSON.parse(changes);
            if (changes !== null && changes.length > 0) {
                var changesBlock_1 = '', changesLength = changes.length, maxCount_1 = 0;
                $.each(changes, function () {
                    var value = parseInt(this.pages_changed_count);
                    if (value > maxCount_1) {
                        maxCount_1 = value;
                    }
                });
                $.each(changes, function () {
                    var value = parseInt(this.pages_changed_count), percentage = 0;
                    percentage = ((value * 100) / maxCount_1);
                    changesBlock_1 = changesBlock_1 + '<div class="dashboard__diagram-block"><span class="dashboard__diagram-val" style="height: ' + percentage + '%"></span></div>';
                });
                if (changesLength < 7) {
                    var index = 7 - changesLength, i = 0;
                    while (i < index) {
                        changesBlock_1 = changesBlock_1 + '<div class="dashboard__diagram-block"><span class="dashboard__diagram-val"></span></div>';
                        i++;
                    }
                }
                return changesBlock_1;
            }
            else {
                var changesBlock = '', i = 1;
                while (i < 8) {
                    changesBlock = changesBlock + '<div class="dashboard__diagram-block"><span class="dashboard__diagram-val"></span></div>';
                    i++;
                }
                return changesBlock;
            }
        };
        CrawlingDomainTableDesign.prototype.isValidPageScore = function (status, num, id, url) {
            var endColor = 'ok';
            function scoreColor(data) {
                if (data < 33) {
                    endColor = 'error';
                }
                else if (data >= 33 && data <= 65) {
                    endColor = 'warning';
                }
                else if (data > 65) {
                    endColor = 'ok';
                }
            }
            if (num === null || isNaN(num) || parseInt(status) === 0 || parseInt(status) === 1) {
                var host = document.location.host || document.location.hostname, statusUrl = document.location.protocol + "//api1." + host + "/api/v1/full/all/" + url;
                var statusRequest = $.ajax({
                    url: statusUrl,
                    method: 'GET',
                    dataType: 'json',
                    xhrFields: {
                        withCredentials: true
                    }
                });
                statusRequest.done(function (response) {
                    $.ajax({
                        url: "/wp-admin/admin-ajax.php",
                        type: "POST",
                        async: false,
                        dataType: "JSON",
                        data: {
                            id: id,
                            score: response.data.evaluation,
                            action: 'update_user_domain_main_page_score'
                        },
                        success: function () {
                            scoreColor(response.data.evaluation);
                            return '<span class="' + endColor + '">' + response.data.evaluation + '</span><span>/100</span>';
                        }
                    });
                });
                return '<img class="dashboard__score-loading" src="/wp-content/themes/sitechecker/out/img_design/loading-blue.svg">';
            }
            else {
                scoreColor(num);
                return '<span class="' + endColor + '">' + num + '</span><span>/100</span>';
            }
        };
        CrawlingDomainTableDesign.prototype.isValidProgressNum = function (num) {
            return num !== null && !isNaN(num);
        };
        CrawlingDomainTableDesign.prototype.logFailed = function (log) {
            console.log("error" + log);
        };
        CrawlingDomainTableDesign.prototype.display = function () {
            var _this = this;
            this.getPageData().then(function (data) { return _this.processResult(JSON.parse(data)); }, function (error) { return _this.logFailed(error); });
        };
        ;
        CrawlingDomainTableDesign.prototype.startProgressUpdater = function () {
            var rowsToUpdate = this.getTBody().find('.update_progress');
            var table = this;
            rowsToUpdate.each(function () {
                var currentRow = $(this);
                var spanToUpdateCount = currentRow.find('.update_progress_count');
                var statusToUpdate = currentRow.attr('name');
                function countUpdater(blockName, value) {
                    var $countToBlock = blockName, countTo = value;
                    if (countTo === null || isNaN(countTo)) {
                        countTo = 0;
                    }
                    $({
                        countNum: blockName.text()
                    }).animate({
                        countNum: countTo
                    }, {
                        duration: 2000,
                        easing: 'swing',
                        step: function () {
                            $countToBlock.text(Math.floor(this.countNum));
                        },
                        complete: function () {
                            $countToBlock.text(this.countNum);
                        }
                    });
                }
                var id = currentRow.data('id');
                var updater = setInterval(function () {
                    $.ajax({
                        url: "/wp-admin/admin-ajax.php",
                        type: "POST",
                        async: false,
                        dataType: "JSON",
                        data: {
                            domainId: id,
                            action: 'crawler_in_progress_statistic'
                        },
                        success: function (data) {
                            if (parseInt(data.status) === 0) {
                                countUpdater(spanToUpdateCount, parseInt(data.in_progress_count));
                                currentRow.attr('name', 'waiting');
                                currentRow.find('.dashboard__url-testCase').text('Waiting slot');
                            }
                            else if (parseInt(data.status) === 1) {
                                countUpdater(spanToUpdateCount, parseInt(data.in_progress_count));
                                currentRow.attr('name', 'inprogress');
                                currentRow.find('.dashboard__url-testCase').text('In progress');
                            }
                            else if (parseInt(data.status) == 2) {
                                currentRow.find('.dashboard__url-testCase').text('');
                                clearInterval(updater);
                                $('.dashboard__testCase').hide();
                                table.init();
                                table.updateLimits();
                            }
                            else {
                                currentRow.find('.dashboard__url-testCase').text('');
                            }
                        }
                    });
                }, 5000);
            });
        };
        CrawlingDomainTableDesign.prototype.crawlConfirmClose = function () {
            $('.addCrawl-block').css('display', 'none');
            $('.addCrawl_popup').fadeOut();
        };
        CrawlingDomainTableDesign.prototype.updateLimits = function () {
            var _this = this;
            this.doRequest({
                action: "crawler_get_limits"
            }).then(function (resultJSON) {
                var result = JSON.parse(resultJSON);
                if (result.success) {
                    var availableDomains = parseInt(result.data.availableDomains) < 0 ? 0 : parseInt(result.data.availableDomains), availableMonitoringDomains = parseInt(result.data.availbleMonitoring) < 0 ? 0 : parseInt(result.data.availbleMonitoring);
                    _this.availablePagesLimit = parseInt(result.data.availablePages) < 0 ? 0 : parseInt(result.data.availablePages);
                    $("#available_pages").text(_this.availablePagesLimit);
                    $("#available_domains").text(availableDomains);
                    $("#available_monitorings").text(availableMonitoringDomains);
                    var monitoring__close = $('.monitoring__limit-banner').hasClass('monitoring__close'), audit__close = $('.audit__limit-banner').hasClass('audit__close');
                    if (availableDomains > 0) {
                        $('.audit__limit-banner').fadeOut();
                    }
                    if (availableDomains <= 0 && !audit__close) {
                        $('.audit__limit-banner').fadeIn().css('display', 'flex');
                        $('.audit__limits-container').append('<script>dataLayer.push({ \'event\': \'autoEvent\', \'eventCategory\': \'Crawling limited popup shown\', \'eventAction\': \'shown\' });</script>');
                    }
                    else if (availableMonitoringDomains <= 0 && !monitoring__close && !audit__close) {
                        $('.monitoring__limit-banner').fadeIn().css('display', 'flex');
                        $('.audit__limits-container').append('<script>dataLayer.push({ \'event\': \'autoEvent\', \'eventCategory\': \'Monitoring limited popup shown\', \'eventAction\': \'shown\' });</script>');
                    }
                    else if (availableMonitoringDomains > 0) {
                        $('.monitoring__limit-banner').fadeOut();
                    }
                    var pagesLimit_1 = _this.availablePagesLimit, slider_1 = $('#settings__pages');
                    $('#settings__pages-value').val(pagesLimit_1);
                    setTimeout(function () {
                        slider_1.data('slider').options.max = pagesLimit_1;
                        slider_1.slider('setValue', pagesLimit_1);
                    }, 1000);
                    if (window.location.href.indexOf('create') > 0) {
                        var create = localStorage.getItem('create'), createLogin = localStorage.getItem('createLogin');
                        if (createLogin == 'Shown' || $('.dashboard__testCase').is(":visible")) {
                            if (create == 'Shown') {
                                localStorage.setItem('create', 'false');
                                localStorage.setItem('createLogin', 'false');
                                $('.addCrawl_popup .login_popup-close').click(function () {
                                    $('.addCrawl-block').css('display', 'none');
                                    $('.addCrawl_popup').fadeOut();
                                });
                                setTimeout(_this.crawlConfirmClose, 3000);
                            }
                        }
                        else if (availableDomains !== 0) {
                            if (create == 'Shown') {
                                localStorage.setItem('create', 'false');
                                localStorage.setItem('createLogin', 'false');
                                $('.addCrawl_popup .login_popup-close').click(function () {
                                    $('.addCrawl-block').css('display', 'none');
                                    $('.addCrawl_popup').fadeOut();
                                });
                                setTimeout(_this.crawlConfirmClose, 3000);
                            }
                        }
                    }
                    $('#loader-list').css('opacity', '0');
                    $('.crawlerData').fadeIn();
                }
            }, function (error) {
                console.log(error);
            });
        };
        return CrawlingDomainTableDesign;
    }(Crawling.BaseTable));
    Crawling.CrawlingDomainTableDesign = CrawlingDomainTableDesign;
})(Crawling || (Crawling = {}));
/// <reference path="CrawlingBaseTable.ts"/>
/// <reference path="../../../../../../typings/globals/jquery/index.d.ts" />
var Crawling;
(function (Crawling) {
    var CrawlingTable = /** @class */ (function (_super) {
        __extends(CrawlingTable, _super);
        function CrawlingTable(apiPath, domainUrl, token) {
            var _this = _super.call(this, apiPath, '/getWeightByDomains', token) || this;
            _this.domainUrl = domainUrl;
            _this.apiPath = apiPath;
            _this.sort_url = $('#sort_url');
            _this.sort_weight = $('#sort_weight');
            _this.sort_status = $('#sort_status_code');
            _this.sort_sitemap = $('#sort_sitemap');
            return _this;
        }
        CrawlingTable.prototype.getSortParam = function () {
            return 'weight';
        };
        CrawlingTable.prototype.getPagination = function () {
            return $('#pageTablePagination');
        };
        CrawlingTable.prototype.getTableElements = function () {
            return $('.table_elements');
        };
        CrawlingTable.prototype.getTable = function () {
            return $('#pageTable');
        };
        CrawlingTable.prototype.getTBody = function () {
            return this.getTable().find("tbody");
        };
        CrawlingTable.prototype.getCrawlingDate = function () {
            return $('#crawling_date');
        };
        CrawlingTable.prototype.setPagination = function () {
            var html = "\n                <div>\n                    <div class=\"dataTables_info\" id=\"pageTable_info\" role=\"status\" aria-live=\"polite\">\n                        Showing " + ((this.page * this.pageSize) - this.pageSize) + " to " + (this.page * this.pageSize > this.totalCount ? this.totalCount : this.page * this.pageSize) + "  of " + this.totalCount + " entries\n                    </div>\n                </div>\n                <div>\n                    <div class=\"dataTables_paginate paging_simple_numbers\" id=\"pageTable_paginate\">\n                        <ul class=\"pagination\">\n                            <li class=\"page-item disabled\">\n                                <span>Page " + this.page + " from " + this.totalPages + "</span>\n                            </li>\n                            <li class=\"paginate_button page-item previous " + (this.page > 1 ? '' : 'disabled') + "\">\n                                <a href=\"javascript:void(0);\" aria-controls=\"pageTable\" class=\"page-link\" " + (this.page > 1 ? "onclick=\"crawlingTable.previousPage();\"" : '') + "  >Previous</a>\n                            </li>\n                            <li class=\"paginate_button page-item " + (this.page < this.totalPages ? '' : 'disabled') + "\">\n                                <a href=\"javascript:void(0);\" aria-controls=\"pageTable\"  class=\"page-link\" " + (this.page < this.totalPages ? "onclick=\"crawlingTable.nextPage()\"" : '') + " >Next</a>\n                            </li>\n                        </ul>\n                    </div>\n                </div>\n            ";
            this.getPagination().html(html);
        };
        ;
        CrawlingTable.prototype.getRequestData = function () {
            return this.addFilters({
                url: this.domainUrl,
                sortParam: this.sortParam,
                sortBy: this.sortBy,
                pageSize: this.pageSize,
                page: this.page,
                token: this.token
            });
        };
        CrawlingTable.prototype.sort = function (column) {
            if (column === this.sortParam) {
                this.sortBy = this.sortBy === 'asc' ? 'desc' : 'asc';
            }
            else {
                this.sortBy = 'asc';
                this.sortParam = column;
            }
            this.sort_url.attr('class', 'sorting');
            this.sort_weight.attr('class', 'sorting');
            this.sort_status.attr('class', 'sorting');
            this.sort_sitemap.attr('class', 'sorting');
            if (column === 'weight') {
                this.sort_weight.attr('class', 'sorting_' + this.sortBy);
            }
            else if (column === 'http_status_code') {
                this.sort_status.attr('class', 'sorting_' + this.sortBy);
            }
            else {
                this.sort_url.attr('class', 'sorting_' + this.sortBy);
            }
            this.display();
        };
        ;
        CrawlingTable.prototype.toggleRowData = function (index) {
            var row = $('#row_' + index);
            var rowIcon = row.find('.icon');
            var rowData = $('#row_data_' + index);
            if (row.hasClass("dataShow")) {
                row.removeClass('dataShow');
                rowIcon.text('+').css('font-size', '16px');
                rowData.hide();
            }
            else {
                row.addClass("dataShow");
                rowIcon.text('-').css('font-size', '25px');
                rowData.show();
            }
        };
        CrawlingTable.prototype.drawRows = function (rows) {
            var _this = this;
            $('#loader').css('opacity', '0');
            $('#pageTable_wrapper').fadeIn();
            rows.forEach(function (page, index) {
                _this.addTableRow(_this.renderRow(page, index));
            });
            $('[data-toggle="tooltip"]').tooltip({
                'template': "<div class=\"tooltip\" role=\"tooltip\"><div class=\"tooltip-arrow\"></div><div class=\"tooltip-inner\" style=\"max-width: 500px;\"></div></div>"
            });
        };
        CrawlingTable.prototype.renderRow = function (page, index) {
            var _this = this;
            var statuses = this.getPageStatuses(page);
            return "\n                <tr id=\"row_" + index + "\">\n                    <td>\n                    <div class=\"domain_links\">\n                        <a href=\"/page-information/?url=" + page.crc32Url + "&token=" + this.token + "\">" + page.url + "</a>\n                        <a class=\"external-link\" href=\"" + page.url + "\" target=\"_blank\"><img src=\"/wp-content/themes/sitechecker/out/img/external-link.svg\" alt=\"external-link\" title=\"external-link\"></a>\n                    </div>\n                        <p>" + page.title + "</p>\n                    </td>\n                    <td>\n                        " + this.numberFormat(page.normalizedWeight, 1) + "\n                    </td>\n                    <td>\n                        " + this.getStatusCodeColumn(page) + "\n                    </td>\n                    <td>\n                        <button type=\"button text-center\" class=\"btn btn-md\" " + (page.externalCountTotal > 0 || 'disabled="disabled"') + "  onclick=\"popupTable = new Crawling.Popup.ExternalPopupTable('" + this.apiPath + "', '" + page.id + "', '" + this.token + "')\">External links (" + page.externalCountTotal + ")</button>\n                        <button type=\"button text-center\" class=\"btn btn-md\"  " + (page.internalCountTotal > 0 || 'disabled="disabled"') + " onclick=\"popupTable = new Crawling.Popup.InternalPopupTable('" + this.apiPath + "', '" + page.id + "', '" + this.token + "')\">Internal links (" + page.internalCountTotal + ")</button>\n                        <button type=\"button text-center\" class=\"btn btn-md\" " + (page.incomingCountTotal > 0 || 'disabled="disabled"') + " onclick=\"popupTable = new Crawling.Popup.IncomingPopupTable('" + this.apiPath + "', '" + page.id + "', '" + this.token + "')\">Anchors (" + page.incomingCountTotal + ")</button>\n                    </td>\n                    <td>\n                        <div class=\"statistic_description\">\n                            " + statuses
                .map(function (status) { return "<a href=\"/page-information/?url=" + page.crc32Url + "&token=" + _this.token + "\"><span class=\"" + status["class"] + "\">" + status.title + "</span></a>"; })
                .reduce(function (prev, curr) { return prev + curr; }, '') + "\n                        </div>\n                    </td>\n                </tr>\n            ";
        };
        CrawlingTable.prototype.numberFormat = function (val, decimalPlaces) {
            return parseFloat(parseFloat(val).toFixed(decimalPlaces));
        };
        CrawlingTable.prototype.getPageStatuses = function (page) {
            var statuses = [];
            var httpStatusCode = page.httpStatusCode;
            if (httpStatusCode == 200 && page.title !== '' && page.contentType === 'text/html') {
                if (page.title === '') {
                    statuses.push({ "class": "red", 'title': "No Title" });
                }
                if (page.h1 === '') {
                    statuses.push({ "class": "red", 'title': "No H1" });
                }
                if (page.description === '') {
                    statuses.push({ "class": "red", 'title': "No Description" });
                }
                if (page.canonical === '') {
                    statuses.push({ "class": "red", 'title': "No Canonical" });
                }
                if (page.h1 === page.title) {
                    statuses.push({ "class": "yellow", 'title': "H1 = Title" });
                }
                if (page.title.length < 30) {
                    statuses.push({ "class": "yellow", 'title': "Short Title" });
                }
                if (page.title.length > 70) {
                    statuses.push({ "class": "yellow", 'title': "Max Title Length" });
                }
                if (!page.robotsTxtAll) {
                    statuses.push({ "class": "yellow", 'title': "Blocked URL" });
                }
            }
            if (httpStatusCode >= 300 && httpStatusCode < 400) {
                statuses.push({ "class": "yellow", 'title': "Redirect" });
            }
            return statuses;
        };
        CrawlingTable.prototype.getStatusCodeColumn = function (page) {
            var domain = window.location.protocol + "//" + window.location.host;
            return "\n                " + (page.httpStatusCode < 300 || page.httpStatusCode >= 400 ?
                "\n                        <span style=\"color: " + this.getStatusCodeIcon(parseInt(page.httpStatusCode)) + "\">" + page.httpStatusCode + "</span>\n                    " :
                "\n                        <span style=\"color: " + this.getStatusCodeWithColor(parseInt(page.httpStatusCode)) + "\">" + page.httpStatusCode + "</span> <img class=\"crawler_statistics-arrow\" src=\"" + domain + "/images/arrow_down.svg\" alt=\"arrow\" title=\"arrow\">\n                        " + (page.redirectToStatusCode === 0 ?
                    "\n                                <span data-toggle=\"tooltip\" data-placement=\"top\" title=\"" + page.location + "\">???</span>\n                            " :
                    "\n                                <span data-toggle=\"tooltip\" data-placement=\"top\" title=\"" + page.location + "\" style=\"color: " + this.getStatusCodeWithColor(parseInt(page.redirectToStatusCode)) + "\" >" + page.redirectToStatusCode + "</span>\n                            ") + " \n                    ") + "\n            ";
        };
        CrawlingTable.prototype.addFilters = function (request) {
            request.searchParamUrl = $('#crawling_search').val();
            if (this.filterId !== null) {
                request.customFilter = this.filterId;
            }
            return request;
        };
        CrawlingTable.prototype.changeFilter = function (filterId) {
            this.filterId = filterId;
            this.page = 1;
            this.display();
        };
        CrawlingTable.prototype.getStatusCodeWithColor = function (statusCode) {
            var color = '#f24c27';
            if (statusCode >= 200 && statusCode < 300) {
                color = "#56c504";
            }
            else if (statusCode > 300 && statusCode < 400) {
                color = "#fd9425";
            }
            return color;
        };
        CrawlingTable.prototype.getStatusCodeIcon = function (statusCode) {
            if (statusCode >= 200 && statusCode < 300) {
                return "#56c504";
            }
            else if (statusCode > 300 && statusCode < 400) {
                return "#fd9425";
            }
            else {
                return "#f24c27";
            }
        };
        return CrawlingTable;
    }(Crawling.CrawlingBaseTable));
    Crawling.CrawlingTable = CrawlingTable;
})(Crawling || (Crawling = {}));
/// <reference path="CrawlingBaseTable.ts"/>
/// <reference path="../../../../../../typings/globals/jquery/index.d.ts" />
var Crawling;
(function (Crawling) {
    var CrawlingTableDesign = /** @class */ (function (_super) {
        __extends(CrawlingTableDesign, _super);
        function CrawlingTableDesign(apiPath, domainUrl, token) {
            var _this = _super.call(this, apiPath, '/getWeightByDomains', token) || this;
            _this.domainUrl = domainUrl;
            _this.apiPath = apiPath;
            _this.sort_url = $('#sort_url');
            _this.sort_weight = $('#sort_weight');
            _this.sort_status = $('#sort_status_code');
            _this.sort_sitemap = $('#sort_sitemap');
            return _this;
        }
        CrawlingTableDesign.prototype.getSortParam = function () {
            return 'weight';
        };
        CrawlingTableDesign.prototype.getPagination = function () {
            return $('#pageTablePagination');
        };
        CrawlingTableDesign.prototype.getTableElements = function () {
            return $('.table_elements');
        };
        CrawlingTableDesign.prototype.getTable = function () {
            return $('#pageTable');
        };
        CrawlingTableDesign.prototype.getTBody = function () {
            return this.getTable().find("tbody");
        };
        CrawlingTableDesign.prototype.getCrawlingDate = function () {
            return $('#crawling_date');
        };
        CrawlingTableDesign.prototype.setPagination = function () {
            var html = "\n                <div>\n                    <div class=\"dataTables_info\" id=\"pageTable_info\" role=\"status\" aria-live=\"polite\">\n                        Showing " + ((this.page * this.pageSize) - this.pageSize) + " to " + (this.page * this.pageSize > this.totalCount ? this.totalCount : this.page * this.pageSize) + "  of " + this.totalCount + " entries\n                    </div>\n                </div>\n                <div>\n                    <div class=\"dataTables_paginate paging_simple_numbers\" id=\"pageTable_paginate\">\n                        <ul class=\"pagination\">\n                            <li class=\"page-item disabled\">\n                                <span>Page " + this.page + " from " + this.totalPages + "</span>\n                            </li>\n                            <li class=\"paginate_button page-item previous " + (this.page > 1 ? '' : 'disabled') + "\">\n                                <a href=\"javascript:void(0);\" aria-controls=\"pageTable\" class=\"page-link\" " + (this.page > 1 ? "onclick=\"crawlingTable.previousPage();\"" : '') + "  >Previous</a>\n                            </li>\n                            <li class=\"paginate_button page-item " + (this.page < this.totalPages ? '' : 'disabled') + "\">\n                                <a href=\"javascript:void(0);\" aria-controls=\"pageTable\"  class=\"page-link\" " + (this.page < this.totalPages ? "onclick=\"crawlingTable.nextPage()\"" : '') + " >Next</a>\n                            </li>\n                        </ul>\n                    </div>\n                </div>\n            ";
            this.getPagination().html(html);
        };
        ;
        CrawlingTableDesign.prototype.getRequestData = function () {
            return this.addFilters({
                url: this.domainUrl,
                sortParam: this.sortParam,
                sortBy: this.sortBy,
                pageSize: this.pageSize,
                page: this.page,
                token: this.token
            });
        };
        CrawlingTableDesign.prototype.sort = function (column) {
            if (column === this.sortParam) {
                this.sortBy = this.sortBy === 'asc' ? 'desc' : 'asc';
            }
            else {
                this.sortBy = 'asc';
                this.sortParam = column;
            }
            this.sort_url.attr('class', 'sorting');
            this.sort_weight.attr('class', 'sorting');
            this.sort_status.attr('class', 'sorting');
            this.sort_sitemap.attr('class', 'sorting');
            if (column === 'weight') {
                this.sort_weight.attr('class', 'sorting_' + this.sortBy);
            }
            else if (column === 'http_status_code') {
                this.sort_status.attr('class', 'sorting_' + this.sortBy);
            }
            else {
                this.sort_url.attr('class', 'sorting_' + this.sortBy);
            }
            this.display();
        };
        ;
        CrawlingTableDesign.prototype.toggleRowData = function (index) {
            var row = $('#row_' + index);
            var rowIcon = row.find('.icon');
            var rowData = $('#row_data_' + index);
            if (row.hasClass("dataShow")) {
                row.removeClass('dataShow');
                rowIcon.text('+').css('font-size', '16px');
                rowData.hide();
            }
            else {
                row.addClass("dataShow");
                rowIcon.text('-').css('font-size', '25px');
                rowData.show();
            }
        };
        CrawlingTableDesign.prototype.drawRows = function (rows) {
            var _this = this;
            $('#loader').css('opacity', '0');
            $('#pageTable_wrapper').fadeIn();
            rows.forEach(function (page, index) {
                _this.addTableRow(_this.renderRow(page, index));
            });
            $('[data-toggle="tooltip"]').tooltip({
                'template': "<div class=\"tooltip\" role=\"tooltip\"><div class=\"tooltip-arrow\"></div><div class=\"tooltip-inner\" style=\"max-width: 500px;\"></div></div>"
            });
        };
        CrawlingTableDesign.prototype.renderRow = function (page, index) {
            var _this = this;
            var statuses = this.getPageStatuses(page);
            return "\n                <tr id=\"row_" + index + "\">\n                    <td class=\"overview__weight-td\">\n                        " + this.numberFormat(page.normalizedWeight, 1) + "\n                    </td>\n                    <td class=\"overview__link-td\">\n                        <div class=\"external-link\"><a href=\"/page-information/?url=" + page.crc32Url + "&token=" + this.token + "\">" + page.url + "</a><a href=\"" + page.url + "\" target=\"_blank\"><img src=\"/wp-content/themes/sitechecker/out/img_design/external.svg\" alt=\"external\" title=\"external\"></a></div>\n                        <p class=\"title\">" + this.titleExist(page.title) + "</p>\n                    </td>\n                    <td class=\"overview__status-td\"> \n                        " + this.getStatusCodeColumn(page) + "\n                    </td>\n                    <td class=\"overview__links-td\">\n                        <p class=\"crawled__links-btn " + (page.externalCountTotal === 0 ? 'empty' : '') + "\" onclick=\"popupTable = new Crawling.Popup.ExternalPopupTable('" + this.apiPath + "', '" + page.id + "', '" + this.token + "')\">External " + (page.externalCountTotal !== 0 ? '(' + page.externalCountTotal + ')' : '') + "</p>\n                        <p class=\"crawled__links-btn " + (page.internalCountTotal === 0 ? 'empty' : '') + "\" onclick=\"popupTable = new Crawling.Popup.InternalPopupTable('" + this.apiPath + "', '" + page.id + "', '" + this.token + "')\">Internal " + (page.internalCountTotal !== 0 ? '(' + page.internalCountTotal + ')' : '') + "</p>\n                        <p class=\"crawled__links-btn " + (page.incomingCountTotal === 0 ? 'empty' : '') + "\" onclick=\"popupTable = new Crawling.Popup.IncomingPopupTable('" + this.apiPath + "', '" + page.id + "', '" + this.token + "')\">Anchors " + (page.incomingCountTotal !== 0 ? '(' + page.incomingCountTotal + ')' : '') + "</p>\n                        <p class=\"crawled__links-btn duplicated__links-title\" onclick=\"popupTable = new Crawling.Popup.DuplicatedLinks('" + this.apiPath + "', '" + page.id + "', '" + this.token + "', 'title')\">Show Duplicates</p>\n                        <p class=\"crawled__links-btn duplicated__links-h1\" onclick=\"popupTable = new Crawling.Popup.DuplicatedLinks('" + this.apiPath + "', '" + page.id + "', '" + this.token + "', 'h1')\">Show Duplicates</p>\n                        <p class=\"crawled__links-btn duplicated__links-descript\" onclick=\"popupTable = new Crawling.Popup.DuplicatedLinks('" + this.apiPath + "', '" + page.id + "', '" + this.token + "', 'description')\">Show Duplicates</p>\n                    </td>\n                    <td class=\"overview__errors-td\">\n                        <div class=\"messages\">\n                            " + statuses
                .map(function (status) { return "<a href=\"/page-information/?url=" + page.crc32Url + "&token=" + _this.token + "\" class=\"" + status["class"] + "\">" + status.title + "</a>"; })
                .reduce(function (prev, curr) { return prev + curr; }, '') + "\n                        </div>\n                    </td>\n                </tr>\n            ";
        };
        CrawlingTableDesign.prototype.titleExist = function (val) {
            if (val !== undefined) {
                return val;
            }
            else {
                return '';
            }
        };
        CrawlingTableDesign.prototype.numberFormat = function (val, decimalPlaces) {
            if (isNaN(parseFloat(val))) {
                return '-';
            }
            else if (val !== null) {
                return parseFloat(parseFloat(val).toFixed(decimalPlaces));
            }
        };
        CrawlingTableDesign.prototype.getPageStatuses = function (page) {
            var statuses = [];
            var httpStatusCode = page.httpStatusCode;
            if (httpStatusCode == 200 && page.title !== '' && page.contentType === 'text/html') {
                if (page.title === '') {
                    statuses.push({ "class": "red", 'title': "No Title" });
                }
                if (page.h1 === '') {
                    statuses.push({ "class": "red", 'title': "No H1" });
                }
                if (page.description === '') {
                    statuses.push({ "class": "red", 'title': "No Description" });
                }
                if (page.canonical === '') {
                    statuses.push({ "class": "red", 'title': "No Canonical" });
                }
                if (page.h1 === page.title) {
                    statuses.push({ "class": "yellow", 'title': "H1 = Title" });
                }
                if (page.title.length < 30) {
                    statuses.push({ "class": "yellow", 'title': "Short Title" });
                }
                if (page.title.length > 70) {
                    statuses.push({ "class": "yellow", 'title': "Max Title Length" });
                }
                if (!page.robotsTxtAll) {
                    statuses.push({ "class": "yellow", 'title': "Blocked URL" });
                }
            }
            if (httpStatusCode >= 300 && httpStatusCode < 400) {
                statuses.push({ "class": "yellow", 'title': "Redirect" });
            }
            return statuses;
        };
        CrawlingTableDesign.prototype.getStatusCodeColumn = function (page) {
            var domain = window.location.protocol + "//" + window.location.host;
            return "\n                " + (page.httpStatusCode < 300 || page.httpStatusCode >= 400 ?
                "\n                        <span style=\"color: " + this.getStatusCodeIcon(parseInt(page.httpStatusCode)) + "\">" + page.httpStatusCode + "</span>\n                    " :
                "\n                        <span style=\"color: " + this.getStatusCodeWithColor(parseInt(page.httpStatusCode)) + "\">" + page.httpStatusCode + "</span> <img class=\"crawler_statistics-arrow\" src=\"/wp-content/themes/sitechecker/out/img_design/arrow__down.svg\" alt=\"arrow\" title=\"arrow\">\n                        " + (page.redirectToStatusCode === 0 ?
                    "\n                                <span data-toggle=\"tooltip\" data-placement=\"top\" title=\"" + page.location + "\">???</span>\n                            " :
                    "\n                                <span data-toggle=\"tooltip\" data-placement=\"top\" title=\"" + page.location + "\" style=\"color: " + this.getStatusCodeWithColor(parseInt(page.redirectToStatusCode)) + "\" >" + page.redirectToStatusCode + "</span>\n                            ") + " \n                    ") + "\n            ";
        };
        CrawlingTableDesign.prototype.addFilters = function (request) {
            request.searchParamUrl = $('#crawling_search').val();
            if (this.filterId !== null) {
                request.customFilter = this.filterId;
            }
            return request;
        };
        CrawlingTableDesign.prototype.changeFilter = function (filterId, filterTitle, filterDescription, whatItIsImportant) {
            $('.overview__table').removeClass('overview__count').attr('name', filterId);
            $('#crawling_search').val('');
            $('#pageTablePagination').css('opacity', '1');
            var param = $('.overview__tables .title');
            param.find('span').text(filterTitle).attr('name', filterId);
            if (filterDescription !== 'undefined') {
                param.find('.tooltip').text(filterDescription);
                param.find('.description').fadeIn();
            }
            else {
                param.find('.description').css('display', 'none');
            }
            if (whatItIsImportant !== undefined) {
                param.find('.important__overview-name').fadeIn();
                $('.important__overview-content p').text(whatItIsImportant);
            }
            else {
                param.find('.important__overview-name').css('display', 'none');
            }
            $('#pageTable tbody').empty().append('<tr class="loading"><td colspan="4"><div class="loading"><img src="/wp-content/themes/sitechecker/out/img_design/loading-blue.svg" alt="loading" title="loading"></div></td></tr>');
            if (filterId !== 0) {
                $('.overview__block, .overview__graph').css('display', 'none');
            }
            else {
                $('.overview__block, .overview__graph').fadeIn().css('display', 'flex');
                this.filterId = 1;
            }
            this.filterId = filterId;
            this.page = 1;
            this.display();
        };
        CrawlingTableDesign.prototype.getStatusCodeWithColor = function (statusCode) {
            var color = '#FF4B55';
            if (statusCode >= 200 && statusCode < 300) {
                color = "#50D166";
            }
            else if (statusCode > 300 && statusCode < 400) {
                color = "#FFB300";
            }
            return color;
        };
        CrawlingTableDesign.prototype.getStatusCodeIcon = function (statusCode) {
            if (statusCode >= 200 && statusCode < 300) {
                return "#50D166";
            }
            else if (statusCode > 300 && statusCode < 400) {
                return "#FFB300";
            }
            else {
                return "#FF4B55";
            }
        };
        return CrawlingTableDesign;
    }(Crawling.CrawlingBaseTable));
    Crawling.CrawlingTableDesign = CrawlingTableDesign;
})(Crawling || (Crawling = {}));
/// <reference path="CrawlingBaseTable.ts"/>
/// <reference path="../../../../../../typings/globals/jquery/index.d.ts" />
var Crawling;
(function (Crawling) {
    var DomainMonitoringTable = /** @class */ (function (_super) {
        __extends(DomainMonitoringTable, _super);
        function DomainMonitoringTable(apiPath, baseUrl, availablePagesLimit, basicTemplateDirectoryUri, dictionary) {
            var _this = _super.call(this, apiPath) || this;
            _this.baseUrl = baseUrl;
            _this.availablePagesLimit = availablePagesLimit;
            _this.basicTemplateDirectoryUri = basicTemplateDirectoryUri;
            _this.dictionary = dictionary;
            return _this;
        }
        DomainMonitoringTable.prototype.getPageData = function () {
            return this.doRequest(this.getRequestData());
        };
        DomainMonitoringTable.prototype.processResult = function (response) {
            var data = response.data;
            this.getTBody().empty();
            if (data.length > 0) {
                var monitoringEnable_1 = 0;
                $.each(data, function (key, val) {
                    if (val.monitoring_enabled === 1) {
                        monitoringEnable_1++;
                    }
                });
                if (monitoringEnable_1 > 0) {
                    this.drawRows(data);
                }
                else {
                    $('#monitoringTable tbody').append('<tr class="monitoringOff"><td colspan="6">You should enable monitoring on <a href="/crawl-report/">Audit page</a></td></tr>');
                }
            }
            else {
                this.addTableRow("\n                    <tr>\n                        <td colspan=\"6\" class=\"row text-center\">\n                            <p style=\"padding: 10px 0\">No data</p>\n                        </td>\n                    </tr>\n                ");
            }
        };
        DomainMonitoringTable.prototype.getSortParam = function () {
            return 'weight';
        };
        DomainMonitoringTable.prototype.getPagination = function () {
            return $('#pageTablePagination');
        };
        DomainMonitoringTable.prototype.getTable = function () {
            return $('#monitoringTable');
        };
        DomainMonitoringTable.prototype.getTBody = function () {
            return this.getTable().find("tbody");
        };
        DomainMonitoringTable.prototype.getCrawlingDate = function () {
            return $('#crawling_date');
        };
        DomainMonitoringTable.prototype.getRequestData = function () {
            var requestData = {
                action: "crawler_domain_list",
                sortParam: this.sortParam,
                sortBy: this.sortBy,
                pageSize: this.pageSize,
                page: this.page
            };
            return requestData;
        };
        DomainMonitoringTable.prototype.sort = function (column) {
            if (column === this.sortParam) {
                this.sortBy = this.sortBy === 'asc' ? 'desc' : 'asc';
            }
            else {
                this.sortBy = 'asc';
                this.sortParam = column;
            }
            this.display();
        };
        ;
        DomainMonitoringTable.prototype.getDiffSpan = function (currentCount, previousCount) {
            var diffSpan = "";
            if (previousCount !== null) {
                var pagesDiff = parseInt(currentCount) - parseInt(previousCount);
                if (pagesDiff > 0) {
                    diffSpan = "<span>+" + pagesDiff + "</span>";
                }
                else if (pagesDiff === 0) {
                    diffSpan = "<span></span>";
                }
                else {
                    diffSpan = "<span>" + pagesDiff + "</span>";
                }
            }
            return diffSpan;
        };
        DomainMonitoringTable.prototype.isDomainInProgress = function (domainStatus) {
            return parseInt(domainStatus) === 0 || parseInt(domainStatus) === 1 || parseInt(domainStatus) === 4;
        };
        DomainMonitoringTable.prototype.needsDomainUpdateProgress = function (domainStatus) {
            return parseInt(domainStatus) === 0 || parseInt(domainStatus) === 1 || parseInt(domainStatus) === 4;
        };
        DomainMonitoringTable.prototype.drawRows = function (rows) {
            var _this = this;
            rows.forEach(function (value) {
                var domainStatus = value.domain_status;
                var domainID = value.domain_id;
                var monitoring_enabled, apiPath = "/wp-admin/admin-ajax.php", token = value.token, changedPages = 0, addedPages = 0, deletedPages = 0, allPages = 0;
                if (value.monitoring_enabled == true) {
                    $.ajax({
                        url: apiPath,
                        async: false,
                        type: "POST",
                        dataType: "JSON",
                        data: {
                            path: '/getMonitoringStatisticForGraph',
                            method: 'POST',
                            action: 'crawler_request',
                            data: JSON.stringify({
                                token: token,
                                interval: 'month'
                            })
                        },
                        success: function (statistic) {
                            changedPages = statistic['summary'][0]['pagesChangesCount'];
                            addedPages = statistic['summary'][0]['pagesAddedCount'];
                            deletedPages = statistic['summary'][0]['pagesDeletedCount'];
                        }
                    });
                    $.ajax({
                        url: apiPath,
                        async: false,
                        type: "POST",
                        dataType: "JSON",
                        data: {
                            path: '/getMonitoringStatisticAllPagesCountForGraph',
                            method: 'POST',
                            action: 'crawler_request',
                            data: JSON.stringify({
                                token: token,
                                interval: 'month'
                            })
                        },
                        success: function (pages) {
                            allPages = pages['summary'][0]['pagesAllCount'];
                        }
                    });
                    monitoring_enabled = 'checked';
                    _this.addTableRow("\n                    <tr class=\"domain_statistic " + ((_this.needsDomainUpdateProgress(domainStatus)) ? 'update_progress' : '') + "\" data-id=\"" + value.domain_id + "\">\n                        <td href=\"" + _this.baseUrl + "/monitoring/?id=" + domainID + "\" class=\"crawler-domain-url\">\n                            <div class=\"domain_statistic-block\">\n                                <div class=\"domain_links\">\n                                    <a class=\"statistic_link " + monitoring_enabled + "\" href=\"" + _this.baseUrl + "/monitoring/?id=" + value.domain_id + "\">" + value.domain_url + "</a>\n                                    <a class=\"external-link\" href=\"" + value.domain_url + "\" target=\"_blank\">\n                                        <img src=\"" + _this.basicTemplateDirectoryUri + "/out/img/external-link.svg\" alt=\"external-link\" title=\"external-link\">\n                                    </a>\n                                </div>\n                            </div>\n                        </td>\n                        <td class=\"crawler-domain_urls\">" + allPages + "</td>\n                        <td class=\"crawler-domain_urls\">" + addedPages + "</td>\n                        <td class=\"crawler-domain_urls\">" + changedPages + "</td>\n                        <td class=\"crawler-domain_urls\">" + deletedPages + "</td>\n                        <td class=\"crawler-domain-recrawl\">\n                            <div class=\"crawler-domain_block\">\n                                <a class=\"changes_btn " + monitoring_enabled + "\" href=\"" + _this.baseUrl + "/monitoring/?id=" + domainID + "\">\n                                    <img src=\"/wp-content/themes/sitechecker/out/img/monitoring__icon-active.svg\" alt=\"chart\" title=\"chart\">\n                                    <span>View changes</span>\n                                </a>\n                            </div>\n                        </td>\n                    </tr>\n                ");
                }
            });
            $(".crawler-domain_urls, .crawler-domain-url").on('click', function () {
                var domainLink = $(this).parent('tr');
                document.location.href = domainLink.find('.crawler-domain-url').attr('href');
            });
            $(".external-link").click(function (e) {
                e.stopPropagation();
            });
            $('#loader-list').css('opacity', '0');
            this.startProgressUpdater();
        };
        DomainMonitoringTable.prototype.isValidProgressNum = function (num) {
            return num !== null && !isNaN(num);
        };
        DomainMonitoringTable.prototype.logFailed = function (log) {
            console.log("error" + log);
        };
        DomainMonitoringTable.prototype.display = function () {
            var _this = this;
            this.getPageData().then(function (data) { return _this.processResult(JSON.parse(data)); }, function (error) { return _this.logFailed(error); });
        };
        ;
        DomainMonitoringTable.prototype.startProgressUpdater = function () {
            var rowsToUpdate = this.getTBody().find('.update_progress');
            var table = this;
            rowsToUpdate.each(function () {
                var currentRow = $(this);
                var id = currentRow.data('id');
                var updater = setInterval(function () {
                    $.ajax({
                        url: "/wp-admin/admin-ajax.php",
                        type: "POST",
                        async: false,
                        dataType: "JSON",
                        data: {
                            domainId: id,
                            action: 'crawler_in_progress_statistic'
                        },
                        success: function (data) {
                            if (parseInt(data.status) == 2) {
                                clearInterval(updater);
                                $('#testMonitoringDomain').hide();
                                table.init();
                                table.updateLimits();
                            }
                        }
                    });
                }, 5000);
            });
        };
        DomainMonitoringTable.prototype.crawlConfirmClose = function () {
            $('.addCrawl-block').css('display', 'none');
            $('.addCrawl_popup').fadeOut();
        };
        DomainMonitoringTable.prototype.updateLimits = function () {
            this.doRequest({
                action: "crawler_get_limits"
            }).then(function (resultJSON) {
                var result = JSON.parse(resultJSON);
                if (result.success) {
                    var availableDomains = parseInt(result.data.availbleMonitoring) < 0 ? 0 : parseInt(result.data.availbleMonitoring);
                    $("#available_domains").text(availableDomains);
                    $('#loader-list').css('opacity', '0');
                    $('.crawlerData').fadeIn();
                }
            }, function (error) {
                console.log(error);
            });
        };
        return DomainMonitoringTable;
    }(Crawling.BaseTable));
    Crawling.DomainMonitoringTable = DomainMonitoringTable;
})(Crawling || (Crawling = {}));
/// <reference path="CrawlingBaseTable.ts"/>
/// <reference path="../../../../../../typings/globals/jquery/index.d.ts" />
var Crawling;
(function (Crawling) {
    var InProgressCrawlingTable = /** @class */ (function (_super) {
        __extends(InProgressCrawlingTable, _super);
        function InProgressCrawlingTable(apiPath, domainUrl, token) {
            var _this = _super.call(this, apiPath, '/inProgress/getWeightByDomains', token) || this;
            _this.lastDateFrom = 0;
            _this.domainUrl = domainUrl;
            _this.apiPath = apiPath;
            _this.sort_url = $('#sort_url');
            _this.sort_weight = $('#sort_weight');
            _this.sort_status = $('#sort_status_code');
            _this.sort_sitemap = $('#sort_sitemap');
            _this.pageSize = 50;
            return _this;
        }
        InProgressCrawlingTable.prototype.getSortParam = function () {
            return 'dateCreate';
        };
        InProgressCrawlingTable.prototype.getPagination = function () {
            return $('#pageTablePagination');
        };
        InProgressCrawlingTable.prototype.getTableElements = function () {
            return $('.table_elements');
        };
        InProgressCrawlingTable.prototype.getTable = function () {
            return $('#pageTable');
        };
        InProgressCrawlingTable.prototype.getTBody = function () {
            return this.getTable().find("tbody");
        };
        InProgressCrawlingTable.prototype.getCrawlingDate = function () {
            return $('#crawling_date');
        };
        InProgressCrawlingTable.prototype.setPagination = function () {
            var html = "\n                <div>\n                    <div class=\"dataTables_info\" id=\"pageTable_info\" role=\"status\" aria-live=\"polite\">\n                        Showing " + ((this.page * this.pageSize) - this.pageSize) + " to " + (this.page * this.pageSize > this.totalCount ? this.totalCount : this.page * this.pageSize) + "  of " + this.totalCount + " entries\n                    </div>\n                </div>\n                <div>\n                    <div class=\"dataTables_paginate paging_simple_numbers\" id=\"pageTable_paginate\">\n                        <ul class=\"pagination\">\n                            <li class=\"page-item disabled\">\n                                <span>Page " + this.page + " from " + this.totalPages + "</span>\n                            </li>\n                            <li class=\"paginate_button page-item previous " + (this.page > 1 ? '' : 'disabled') + "\">\n                                <a href=\"javascript:void(0);\" aria-controls=\"pageTable\" class=\"page-link\" " + (this.page > 1 ? "onclick=\"crawlingTable.previousPage();\"" : '') + "  >Previous</a>\n                            </li>\n                            <li class=\"paginate_button page-item " + (this.page < this.totalPages ? '' : 'disabled') + "\">\n                                <a href=\"javascript:void(0);\" aria-controls=\"pageTable\"  class=\"page-link\" " + (this.page < this.totalPages ? "onclick=\"crawlingTable.nextPage()\"" : '') + " >Next</a>\n                            </li>\n                        </ul>\n                    </div>\n                </div>\n            ";
            // this.getPagination().html(html);
        };
        ;
        InProgressCrawlingTable.prototype.checkNewData = function () {
            var _this = this;
            var requestBody = this.getRequestData();
            requestBody.lastDateFrom = this.lastDateFrom;
            var request = {
                'data': [
                    requestBody
                ]
            };
            var data = JSON.stringify(request);
            this.crawlerRequest(data).then(function (data) { return _this.processNewRows(JSON.parse(data)); }, function (error) { return _this.logFailed(error); });
        };
        InProgressCrawlingTable.prototype.processNewRows = function (data) {
            var result = data[0];
            var summary = result.summary;
            this.drawNewRows(summary);
        };
        InProgressCrawlingTable.prototype.drawNewRows = function (rows) {
            var _this = this;
            $('#pageTable_wrapper').fadeIn();
            $('.table-loading').parent().remove();
            rows.forEach(function (page, index) {
                var countOfRows = _this.getTBody().find('tr').length;
                //console.log([countOfRows, this.pageSize]);
                if (_this.lastDateFrom < parseInt(page.dateCreate)) {
                    _this.lastDateFrom = parseInt(page.dateCreate);
                }
                var update = true;
                _this.getTBody().prepend(_this.renderRow(page, index, update));
                if (countOfRows + 1 >= _this.pageSize) {
                    _this.getTable().find('tbody tr:last-of-type').remove();
                }
            });
        };
        InProgressCrawlingTable.prototype.getRequestData = function () {
            return this.addFilters({
                url: this.domainUrl,
                sortParam: this.sortParam,
                sortBy: this.sortBy,
                pageSize: this.pageSize,
                page: this.page,
                token: this.token
            });
        };
        InProgressCrawlingTable.prototype.sort = function (column) {
            if (column === this.sortParam) {
                this.sortBy = this.sortBy === 'asc' ? 'desc' : 'asc';
            }
            else {
                this.sortBy = 'asc';
                this.sortParam = column;
            }
            this.sort_url.attr('class', 'sorting');
            this.sort_weight.attr('class', 'sorting');
            this.sort_status.attr('class', 'sorting');
            this.sort_sitemap.attr('class', 'sorting');
            if (column === 'weight') {
                this.sort_weight.attr('class', 'sorting_' + this.sortBy);
            }
            else if (column === 'http_status_code') {
                this.sort_status.attr('class', 'sorting_' + this.sortBy);
            }
            else {
                this.sort_url.attr('class', 'sorting_' + this.sortBy);
            }
            this.display();
        };
        ;
        InProgressCrawlingTable.prototype.toggleRowData = function (index) {
            var row = $('#row_' + index);
            var rowIcon = row.find('.icon');
            var rowData = $('#row_data_' + index);
            if (row.hasClass("dataShow")) {
                row.removeClass('dataShow');
                rowIcon.text('+').css('font-size', '16px');
                rowData.hide();
            }
            else {
                row.addClass("dataShow");
                rowIcon.text('-').css('font-size', '25px');
                rowData.show();
            }
        };
        InProgressCrawlingTable.prototype.drawRows = function (rows) {
            var _this = this;
            rows.forEach(function (page, index) {
                if (_this.lastDateFrom < parseInt(page.dateDrom)) {
                    _this.lastDateFrom = parseInt(page.dateDrom);
                }
                var update = false;
                _this.addTableRow(_this.renderRow(page, index, update));
            });
            $('[data-toggle="tooltip"]').tooltip({
                'template': "<div class=\"tooltip\" role=\"tooltip\"><div class=\"tooltip-arrow\"></div><div class=\"tooltip-inner\" style=\"max-width: 500px;\"></div></div>"
            });
        };
        InProgressCrawlingTable.prototype.renderRow = function (page, index, update) {
            $('tr.gradient').css('display', 'none').fadeIn();
            setInterval(function () {
                $('#row_' + index).css('display', 'table-row');
            }, 1000);
            var statuses = this.getPageStatuses(page);
            var contentLength, contentType, updateClass, statusColor;
            switch (page.httpStatusCode.toString()) {
                case '200':
                    statusColor = 'success';
                    break;
                case '301':
                    statusColor = 'warning';
                    break;
                case '404':
                    statusColor = 'error';
                    break;
                default:
                    statusColor = 'warning';
                    break;
            }
            if (update == true) {
                updateClass = 'gradient';
                contentLength = '<i class="fa fa-spinner fa-spin"></i>';
                contentType = '<i class="fa fa-spinner fa-spin"></i>';
            }
            else {
                updateClass = 'normal';
                contentLength = '';
                contentType = '';
            }
            return "\n                <tr id=\"row_" + index + "\" class=\"" + updateClass + " " + statusColor + "\">\n                    <td>\n                        <a href=\"" + page.url + "\" target=\"_blank\">" + page.url + "</a>\n                        <p>" + page.title + "</p>\n                    </td>\n                    <td>" + contentLength + "</td>\n                    <td>\n                        " + this.getStatusCodeColumn(page) + "\n                    </td>\n                    <td>" + contentType + "</td>\n                    <td>\n                        <div class=\"statistic_description\">\n                            " + statuses
                .map(function (status) { return "<span class=\"" + status["class"] + "\">" + status.title + "</span>"; })
                .reduce(function (prev, curr) { return prev + curr; }, '') + "\n                        </div>\n                    </td>\n                </tr>\n                \n            ";
        };
        InProgressCrawlingTable.prototype.numberFormat = function (val, decimalPlaces) {
            return parseFloat(parseFloat(val).toFixed(decimalPlaces));
        };
        InProgressCrawlingTable.prototype.getPageStatuses = function (page) {
            var statuses = [];
            var httpStatusCode = page.httpStatusCode;
            if (httpStatusCode == 200 && page.title !== '' && page.contentType === 'text/html') {
                if (page.title === '') {
                    statuses.push({ "class": "red", 'title': "No Title" });
                }
                if (page.h1 === '') {
                    statuses.push({ "class": "red", 'title': "No H1" });
                }
                if (page.description === '') {
                    statuses.push({ "class": "red", 'title': "No Description" });
                }
                if (page.canonical === '') {
                    statuses.push({ "class": "red", 'title': "Invalid Canonical" });
                }
                if (page.h1 === page.title) {
                    statuses.push({ "class": "yellow", 'title': "H1 = Title" });
                }
                if (page.title.length < 30) {
                    statuses.push({ "class": "yellow", 'title': "Short Title" });
                }
                if (page.title.length > 70) {
                    statuses.push({ "class": "yellow", 'title': "Max Title Length" });
                }
                if (!page.robotsTxtAll) {
                    statuses.push({ "class": "yellow", 'title': "Blocked URL" });
                }
            }
            if (httpStatusCode >= 300 && httpStatusCode < 400) {
                statuses.push({ "class": "yellow", 'title': "Redirect" });
            }
            return statuses;
        };
        InProgressCrawlingTable.prototype.getStatusCodeColumn = function (page) {
            var domain = window.location.protocol + "//" + window.location.host;
            return "\n                " + (page.httpStatusCode < 300 || page.httpStatusCode >= 400 ?
                "\n                        <span style=\"color: " + this.getStatusCodeIcon(parseInt(page.httpStatusCode)) + "\">" + page.httpStatusCode + "</span>\n                    " :
                "\n                        <span style=\"color: " + this.getStatusCodeWithColor(parseInt(page.httpStatusCode)) + "\">" + page.httpStatusCode + "</span> <img class=\"crawler_statistics-arrow\" src=\"" + domain + "/images/arrow_down.svg\" alt=\"arrow\" title=\"arrow\">\n                        " + (page.redirectToStatusCode === 0 ?
                    "\n                                <span data-toggle=\"tooltip\" data-placement=\"top\" title=\"" + page.location + "\">???</span>\n                            " :
                    "\n                                <span data-toggle=\"tooltip\" data-placement=\"top\" title=\"" + page.location + "\" style=\"color: " + this.getStatusCodeWithColor(parseInt(page.redirectToStatusCode)) + "\" >" + page.redirectToStatusCode + "</span>\n                            ") + " \n                    ") + "\n            ";
        };
        InProgressCrawlingTable.prototype.addFilters = function (request) {
            request.searchParamUrl = $('#crawling_search').val();
            if (this.filterId !== null) {
                request.customFilter = this.filterId;
            }
            return request;
        };
        InProgressCrawlingTable.prototype.changeFilter = function (filterId) {
            this.filterId = filterId;
            this.page = 1;
            this.display();
        };
        InProgressCrawlingTable.prototype.getStatusCodeWithColor = function (statusCode) {
            var color = '#f24c27';
            if (statusCode >= 200 && statusCode < 300) {
                color = "#56c504";
            }
            else if (statusCode > 300 && statusCode < 400) {
                color = "#fd9425";
            }
            return color;
        };
        InProgressCrawlingTable.prototype.getStatusCodeIcon = function (statusCode) {
            if (statusCode >= 200 && statusCode < 300) {
                return "#56c504";
            }
            else if (statusCode > 300 && statusCode < 400) {
                return "#fd9425";
            }
            else {
                return "#f24c27";
            }
        };
        return InProgressCrawlingTable;
    }(Crawling.CrawlingBaseTable));
    Crawling.InProgressCrawlingTable = InProgressCrawlingTable;
})(Crawling || (Crawling = {}));
/// <reference path="CrawlingBaseTable.ts"/>
/// <reference path="../../../../../../typings/globals/jquery/index.d.ts" />
var Crawling;
(function (Crawling) {
    var InProgressCrawlingTableDesign = /** @class */ (function (_super) {
        __extends(InProgressCrawlingTableDesign, _super);
        function InProgressCrawlingTableDesign(apiPath, domainUrl, token) {
            var _this = _super.call(this, apiPath, '/inProgress/getWeightByDomains', token) || this;
            _this.lastDateFrom = 0;
            _this.domainUrl = domainUrl;
            _this.apiPath = apiPath;
            _this.sort_url = $('#sort_url');
            _this.sort_weight = $('#sort_weight');
            _this.sort_status = $('#sort_status_code');
            _this.sort_sitemap = $('#sort_sitemap');
            _this.pageSize = 10;
            return _this;
        }
        InProgressCrawlingTableDesign.prototype.getSortParam = function () {
            return 'dateCreate';
        };
        InProgressCrawlingTableDesign.prototype.getPagination = function () {
            return $('#pageTablePagination');
        };
        InProgressCrawlingTableDesign.prototype.getTableElements = function () {
            return $('.table_elements');
        };
        InProgressCrawlingTableDesign.prototype.getTable = function () {
            return $('#pageTable');
        };
        InProgressCrawlingTableDesign.prototype.getTBody = function () {
            return this.getTable().find("tbody");
        };
        InProgressCrawlingTableDesign.prototype.getCrawlingDate = function () {
            return $('#crawling_date');
        };
        InProgressCrawlingTableDesign.prototype.setPagination = function () {
            var html = "\n                <div>\n                    <div class=\"dataTables_info\" id=\"pageTable_info\" role=\"status\" aria-live=\"polite\">\n                        Showing " + ((this.page * this.pageSize) - this.pageSize) + " to " + (this.page * this.pageSize > this.totalCount ? this.totalCount : this.page * this.pageSize) + "  of " + this.totalCount + " entries\n                    </div>\n                </div>\n                <div>\n                    <div class=\"dataTables_paginate paging_simple_numbers\" id=\"pageTable_paginate\">\n                        <ul class=\"pagination\">\n                            <li class=\"page-item disabled\">\n                                <span>Page " + this.page + " from " + this.totalPages + "</span>\n                            </li>\n                            <li class=\"paginate_button page-item previous " + (this.page > 1 ? '' : 'disabled') + "\">\n                                <a href=\"javascript:void(0);\" aria-controls=\"pageTable\" class=\"page-link\" " + (this.page > 1 ? "onclick=\"crawlingTable.previousPage();\"" : '') + "  >Previous</a>\n                            </li>\n                            <li class=\"paginate_button page-item " + (this.page < this.totalPages ? '' : 'disabled') + "\">\n                                <a href=\"javascript:void(0);\" aria-controls=\"pageTable\"  class=\"page-link\" " + (this.page < this.totalPages ? "onclick=\"crawlingTable.nextPage()\"" : '') + " >Next</a>\n                            </li>\n                        </ul>\n                    </div>\n                </div>\n            ";
            // this.getPagination().html(html);
        };
        ;
        InProgressCrawlingTableDesign.prototype.checkNewData = function () {
            var _this = this;
            var requestBody = this.getRequestData();
            requestBody.lastDateFrom = this.lastDateFrom;
            var request = {
                'data': [
                    requestBody
                ]
            };
            var data = JSON.stringify(request);
            this.crawlerRequest(data).then(function (data) { return _this.processNewRows(JSON.parse(data)); }, function (error) { return _this.logFailed(error); });
        };
        InProgressCrawlingTableDesign.prototype.processNewRows = function (data) {
            var result = data[0];
            var summary = result.summary;
            this.drawNewRows(summary);
        };
        InProgressCrawlingTableDesign.prototype.drawNewRows = function (rows) {
            var _this = this;
            $('#pageTable tbody tr.loading').css('display', 'none');
            rows.forEach(function (page, index) {
                var countOfRows = _this.getTBody().find('tr').length;
                if (_this.lastDateFrom < parseInt(page.dateCreate)) {
                    _this.lastDateFrom = parseInt(page.dateCreate);
                }
                var update = true;
                _this.getTBody().prepend(_this.renderRow(page, index, update));
                if (countOfRows + 1 >= _this.pageSize) {
                    _this.getTable().find('tbody tr:last-of-type').remove();
                }
            });
        };
        InProgressCrawlingTableDesign.prototype.getRequestData = function () {
            return this.addFilters({
                url: this.domainUrl,
                sortParam: this.sortParam,
                sortBy: this.sortBy,
                pageSize: this.pageSize,
                page: this.page,
                token: this.token
            });
        };
        InProgressCrawlingTableDesign.prototype.sort = function (column) {
            if (column === this.sortParam) {
                this.sortBy = this.sortBy === 'asc' ? 'desc' : 'asc';
            }
            else {
                this.sortBy = 'asc';
                this.sortParam = column;
            }
            this.sort_url.attr('class', 'sorting');
            this.sort_weight.attr('class', 'sorting');
            this.sort_status.attr('class', 'sorting');
            this.sort_sitemap.attr('class', 'sorting');
            if (column === 'weight') {
                this.sort_weight.attr('class', 'sorting_' + this.sortBy);
            }
            else if (column === 'http_status_code') {
                this.sort_status.attr('class', 'sorting_' + this.sortBy);
            }
            else {
                this.sort_url.attr('class', 'sorting_' + this.sortBy);
            }
            this.display();
        };
        ;
        InProgressCrawlingTableDesign.prototype.toggleRowData = function (index) {
            var row = $('#row_' + index);
            var rowIcon = row.find('.icon');
            var rowData = $('#row_data_' + index);
            if (row.hasClass("dataShow")) {
                row.removeClass('dataShow');
                rowIcon.text('+').css('font-size', '16px');
                rowData.hide();
            }
            else {
                row.addClass("dataShow");
                rowIcon.text('-').css('font-size', '25px');
                rowData.show();
            }
        };
        InProgressCrawlingTableDesign.prototype.drawRows = function (rows) {
            var _this = this;
            rows.forEach(function (page, index) {
                if (_this.lastDateFrom < parseInt(page.dateDrom)) {
                    _this.lastDateFrom = parseInt(page.dateDrom);
                }
                var update = false;
                _this.addTableRow(_this.renderRow(page, index, update));
            });
            $('[data-toggle="tooltip"]').tooltip({
                'template': "<div class=\"tooltip\" role=\"tooltip\"><div class=\"tooltip-arrow\"></div><div class=\"tooltip-inner\" style=\"max-width: 500px;\"></div></div>"
            });
        };
        InProgressCrawlingTableDesign.prototype.renderRow = function (page, index, update) {
            var _this = this;
            $('tr.gradient').css('display', 'none').fadeIn().css('display', 'table-row');
            var statuses = this.getPageStatuses(page);
            var contentLength, updateClass, statusColor;
            switch (page.httpStatusCode.toString()) {
                case '200':
                    statusColor = 'success';
                    break;
                case '301':
                    statusColor = 'warning';
                    break;
                case '404':
                    statusColor = 'error';
                    break;
                default:
                    statusColor = 'warning';
                    break;
            }
            if (update == true) {
                updateClass = 'gradient';
                contentLength = '<svg class="lds-message" width="100%" height="100%" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 100 100" preserveAspectRatio="xMidYMid" style="background: none;"><g transform="translate(25 50)"><circle cx="0" cy="0" r="6" fill="#267DFF" transform="scale(0.966961 0.966961)"><animateTransform attributeName="transform" type="scale" begin="-0.3333333333333333s" calcMode="spline" keySplines="0.3 0 0.7 1;0.3 0 0.7 1" values="0;1;0" keyTimes="0;0.5;1" dur="1s" repeatCount="indefinite"></animateTransform></circle></g><g transform="translate(50 50)"><circle cx="0" cy="0" r="6" fill="#267DFF" transform="scale(0.597389 0.597389)"><animateTransform attributeName="transform" type="scale" begin="-0.16666666666666666s" calcMode="spline" keySplines="0.3 0 0.7 1;0.3 0 0.7 1" values="0;1;0" keyTimes="0;0.5;1" dur="1s" repeatCount="indefinite"></animateTransform></circle></g><g transform="translate(75 50)"><circle cx="0" cy="0" r="6" fill="#267DFF" transform="scale(0.146741 0.146741)"><animateTransform attributeName="transform" type="scale" begin="0s" calcMode="spline" keySplines="0.3 0 0.7 1;0.3 0 0.7 1" values="0;1;0" keyTimes="0;0.5;1" dur="1s" repeatCount="indefinite"></animateTransform>\n                                </circle></g></svg>';
            }
            else {
                updateClass = 'normal';
                contentLength = '';
            }
            return "\n                <tr id=\"row_" + index + "\" class=\"" + updateClass + " " + statusColor + "\">\n                    <td>\n                        <div class=\"loading\">" + contentLength + "</div>\n                    </td>\n                    <td>\n                        <div class=\"external-link\"><a href=\"/page-information/?url=" + page.crc32Url + "&token=" + this.token + "\">" + page.url + "</a><a href=\"" + page.url + "\" target=\"_blank\"><img src=\"/wp-content/themes/sitechecker/out/img_design/external.svg\" alt=\"external\" title=\"external\"></a></div>\n                        <p class=\"title\">" + page.title + "</p>\n                    </td>\n                    <td class=\"overview__status-td\">\n                        " + this.getStatusCodeColumn(page) + "\n                    </td>\n                    <td><div class=\"loading\">\n                    <svg class=\"lds-message\" width=\"100%\" height=\"100%\" xmlns=\"http://www.w3.org/2000/svg\"\n                         xmlns:xlink=\"http://www.w3.org/1999/xlink\" viewBox=\"0 0 100 100\" preserveAspectRatio=\"xMidYMid\"\n                         style=\"background: none;\">\n                        <g transform=\"translate(25 50)\">\n                            <circle cx=\"0\" cy=\"0\" r=\"6\" fill=\"#267DFF\" transform=\"scale(0.966961 0.966961)\">\n                                <animateTransform attributeName=\"transform\" type=\"scale\" begin=\"-0.3333333333333333s\"\n                                                  calcMode=\"spline\" keySplines=\"0.3 0 0.7 1;0.3 0 0.7 1\" values=\"0;1;0\"\n                                                  keyTimes=\"0;0.5;1\" dur=\"1s\"\n                                                  repeatCount=\"indefinite\"></animateTransform>\n                            </circle>\n                        </g>\n                        <g transform=\"translate(50 50)\">\n                            <circle cx=\"0\" cy=\"0\" r=\"6\" fill=\"#267DFF\" transform=\"scale(0.597389 0.597389)\">\n                                <animateTransform attributeName=\"transform\" type=\"scale\" begin=\"-0.16666666666666666s\"\n                                                  calcMode=\"spline\" keySplines=\"0.3 0 0.7 1;0.3 0 0.7 1\" values=\"0;1;0\"\n                                                  keyTimes=\"0;0.5;1\" dur=\"1s\"\n                                                  repeatCount=\"indefinite\"></animateTransform>\n                            </circle>\n                        </g>\n                        <g transform=\"translate(75 50)\">\n                            <circle cx=\"0\" cy=\"0\" r=\"6\" fill=\"#267DFF\" transform=\"scale(0.146741 0.146741)\">\n                                <animateTransform attributeName=\"transform\" type=\"scale\" begin=\"0s\" calcMode=\"spline\"\n                                                  keySplines=\"0.3 0 0.7 1;0.3 0 0.7 1\" values=\"0;1;0\" keyTimes=\"0;0.5;1\"\n                                                  dur=\"1s\" repeatCount=\"indefinite\"></animateTransform>\n                            </circle>\n                        </g>\n                    </svg></div></td>\n                    <td>\n                        <div class=\"messages\">\n                            " + statuses
                .map(function (status) { return "<a href=\"/page-information/?url=" + page.crc32Url + "&token=" + _this.token + "\" class=\"" + status["class"] + "\">" + status.title + "</a>"; })
                .reduce(function (prev, curr) { return prev + curr; }, '') + "\n                        </div>\n                    </td>\n                </tr>\n            ";
        };
        InProgressCrawlingTableDesign.prototype.numberFormat = function (val, decimalPlaces) {
            return parseFloat(parseFloat(val).toFixed(decimalPlaces));
        };
        InProgressCrawlingTableDesign.prototype.getPageStatuses = function (page) {
            var statuses = [];
            var httpStatusCode = page.httpStatusCode;
            if (httpStatusCode == 200 && page.title !== '' && page.contentType === 'text/html') {
                if (page.title === '') {
                    statuses.push({ "class": "red", 'title': "No Title" });
                }
                if (page.h1 === '') {
                    statuses.push({ "class": "red", 'title': "No H1" });
                }
                if (page.description === '') {
                    statuses.push({ "class": "red", 'title': "No Description" });
                }
                if (page.canonical === '') {
                    statuses.push({ "class": "red", 'title': "Invalid Canonical" });
                }
                if (page.h1 === page.title) {
                    statuses.push({ "class": "yellow", 'title': "H1 = Title" });
                }
                if (page.title.length < 30) {
                    statuses.push({ "class": "yellow", 'title': "Short Title" });
                }
                if (page.title.length > 70) {
                    statuses.push({ "class": "yellow", 'title': "Max Title Length" });
                }
                if (!page.robotsTxtAll) {
                    statuses.push({ "class": "yellow", 'title': "Blocked URL" });
                }
            }
            if (httpStatusCode >= 300 && httpStatusCode < 400) {
                statuses.push({ "class": "yellow", 'title': "Redirect" });
            }
            return statuses;
        };
        InProgressCrawlingTableDesign.prototype.getStatusCodeColumn = function (page) {
            var domain = window.location.protocol + "//" + window.location.host;
            return "\n                " + (page.httpStatusCode < 300 || page.httpStatusCode >= 400 ?
                "\n                        <span style=\"color: " + this.getStatusCodeIcon(parseInt(page.httpStatusCode)) + "\">" + page.httpStatusCode + "</span>\n                    " :
                "\n                        <span style=\"color: " + this.getStatusCodeWithColor(parseInt(page.httpStatusCode)) + "\">" + page.httpStatusCode + "</span> <img class=\"crawler_statistics-arrow\" src=\"" + domain + "/images/arrow_down.svg\" alt=\"arrow\" title=\"arrow\">\n                        " + (page.redirectToStatusCode === 0 ?
                    "\n                                <span data-toggle=\"tooltip\" data-placement=\"top\" title=\"" + page.location + "\">???</span>\n                            " :
                    "\n                                <span data-toggle=\"tooltip\" data-placement=\"top\" title=\"" + page.location + "\" style=\"color: " + this.getStatusCodeWithColor(parseInt(page.redirectToStatusCode)) + "\" >" + page.redirectToStatusCode + "</span>\n                            ") + " \n                    ") + "\n            ";
        };
        InProgressCrawlingTableDesign.prototype.addFilters = function (request) {
            request.searchParamUrl = $('#crawling_search').val();
            if (this.filterId !== null) {
                request.customFilter = this.filterId;
            }
            return request;
        };
        InProgressCrawlingTableDesign.prototype.changeFilter = function (filterId) {
            this.filterId = filterId;
            this.page = 1;
            this.pageSize = 10;
            this.display();
        };
        InProgressCrawlingTableDesign.prototype.getStatusCodeWithColor = function (statusCode) {
            var color = '#f24c27';
            if (statusCode >= 200 && statusCode < 300) {
                color = "#56c504";
            }
            else if (statusCode > 300 && statusCode < 400) {
                color = "#fd9425";
            }
            return color;
        };
        InProgressCrawlingTableDesign.prototype.getStatusCodeIcon = function (statusCode) {
            if (statusCode >= 200 && statusCode < 300) {
                return "#56c504";
            }
            else if (statusCode > 300 && statusCode < 400) {
                return "#fd9425";
            }
            else {
                return "#f24c27";
            }
        };
        return InProgressCrawlingTableDesign;
    }(Crawling.CrawlingBaseTable));
    Crawling.InProgressCrawlingTableDesign = InProgressCrawlingTableDesign;
})(Crawling || (Crawling = {}));
/// <reference path="CrawlingBaseTable.ts"/>
/// <reference path="../../../../../../typings/globals/jquery/index.d.ts" />
var Crawling;
(function (Crawling) {
    var MonitoringEvent = /** @class */ (function (_super) {
        __extends(MonitoringEvent, _super);
        function MonitoringEvent(apiPath, requestPath, token, interval, filterDate) {
            var _this = _super.call(this, apiPath) || this;
            _this.requestPath = requestPath;
            _this.pageSize = 10;
            _this.sortBy = 'desc';
            _this.sortParam = 'date_create';
            _this.page = 1;
            _this.token = token;
            _this.interval = interval;
            _this.filterDate = filterDate;
            return _this;
        }
        MonitoringEvent.prototype.processResult = function (response) {
            //console.log(response);
            this.totalCount = response.totalCount;
            this.filteredCount = response.filteredCount;
            this.totalPages = response.pages;
            var events = response.summary;
            if (events.length > 0) {
                $('.monitoring_graph').css('display', 'flex');
                this.drawRows(events);
                if (response.pages === 1) {
                    $('.nextEvent button').css('display', 'none');
                }
            }
            else if (events.length === 0 && response.pages === 0) {
                $('.empty_array').fadeIn();
            }
            else if (events.length === 0 && response.pages > 0) {
                $('.nextEvent').css('display', 'none');
            }
        };
        MonitoringEvent.prototype.nextPage = function () {
            if (this.page * this.pageSize < this.totalCount) {
                this.page++;
                this.display();
            }
            else {
                $('.nextEvent button, .nextEvent .loader').css('display', 'none');
            }
        };
        MonitoringEvent.prototype.getSortParam = function () {
            return 'weight';
        };
        MonitoringEvent.prototype.getPagination = function () {
            return $('#pageTablePagination');
        };
        MonitoringEvent.prototype.getTable = function () {
            return $('#graph .history__blocks');
        };
        MonitoringEvent.prototype.getTBody = function () {
            return this.getTable();
        };
        MonitoringEvent.prototype.getCrawlingDate = function () {
            return $('#crawling_date');
        };
        MonitoringEvent.prototype.sort = function (column) {
            if (column === this.sortParam) {
                this.sortBy = this.sortBy === 'asc' ? 'desc' : 'asc';
            }
            else {
                this.sortBy = 'asc';
                this.sortParam = column;
            }
            this.display();
        };
        ;
        MonitoringEvent.prototype.drawRows = function (events) {
            var _this = this;
            events.forEach(function (event) {
                var row = _this.renderEvent(event);
                _this.addTableRow(row);
            });
            $('#nextEvent').fadeIn();
            $('.nextEvent .loader').css('display', 'none');
        };
        MonitoringEvent.prototype.logFailed = function (log) {
            console.log("error" + log);
        };
        MonitoringEvent.prototype.display = function () {
            var _this = this;
            this.getPageData().then(function (data) { return _this.processResult(JSON.parse(data)); }, function (error) { return _this.logFailed(error); });
        };
        ;
        MonitoringEvent.prototype.getRequestData = function () {
            return {
                sortBy: this.sortBy,
                pageSize: this.pageSize,
                page: this.page,
                token: this.token,
                sortParam: this.sortParam,
                interval: this.interval,
                filterDate: this.filterDate
            };
        };
        MonitoringEvent.prototype.getPageData = function () {
            var request = this.getRequestData();
            //console.log(request);
            var data = JSON.stringify(request);
            return this.monitoringRequest(data);
        };
        MonitoringEvent.prototype.monitoringRequest = function (data) {
            return this.doRequest({
                path: this.requestPath,
                action: 'crawler_request',
                data: data,
                method: 'POST'
            });
        };
        MonitoringEvent.prototype.renderEvent = function (event) {
            var dateFormat = new Date(event.monitoringStatistic.dateCreate * 1000), options = { weekday: "long", year: "numeric", month: "long", day: "numeric", hour: '2-digit', minute: '2-digit' }, date = dateFormat.toLocaleDateString("en-us", options).replace(",", "");
            var id = event.monitoringStatistic.id;
            var pagesAdded = JSON.parse(event.pagesAdded);
            var pageAddedArray = pagesAdded.map(function (page) {
                var pageUrl = page.url;
                return "<li><div class=\"links\"><a href=\"" + pageUrl + "\" class=\"internal_link\">" + pageUrl + "</a></div></li>";
            }).join('\n');
            var timeLineNewPages = "\n                <div class=\"history__block-data added\">\n                    <div class=\"top\">\n                        <img src=\"/wp-content/themes/sitechecker/out/img_design/monitoring__plus.svg\"\n                             alt=\"new pages\" title=\"new pages\" class=\"status\">\n                        <p>" + pagesAdded.length + " new pages</p>\n                        <div class=\"arrow\">\n                            <img src=\"/wp-content/themes/sitechecker/out/img_design/monitoring__arrow-down.svg\"\n                                 alt=\"arrow\" title=\"arrow\">\n                        </div>\n                    </div>\n                    <div class=\"body\">\n                        <ul>\n                            " + pageAddedArray + "\n                        </ul>\n                    </div>\n                </div>\n            ";
            var pagesDeleted = JSON.parse(event.pagesDeleted);
            var pageDeletedArray = pagesDeleted.map(function (page) {
                var pageUrl = page.url;
                return "<li><div><a href=\"" + pageUrl + "\">" + pageUrl + "</a></div></li>";
            }).join('\n');
            var timeLineDeletedPages = "\n                <div class=\"history__block-data deleted\">\n                    <div class=\"top\">\n                        <img src=\"/wp-content/themes/sitechecker/out/img_design/monitoring__delete.svg\"\n                             alt=\"Deleted Pages\" title=\"Deleted Pages\" class=\"status\">\n                        <p>" + pagesDeleted.length + " deleted pages</p>\n                        <div class=\"arrow\">\n                            <img src=\"/wp-content/themes/sitechecker/out/img_design/monitoring__arrow-down.svg\"\n                                 alt=\"arrow\" title=\"arrow\">\n                        </div>\n                    </div>\n                    <div class=\"body\">\n                        <ul>\n                            " + pageDeletedArray + "\n                        </ul>\n                    </div>\n                </div>\n            ";
            var pagesChanged = JSON.parse(event.pagesChanged);
            function getPagesByChangeTitle(pagesChanged, changeTitle) {
                return pagesChanged.filter(function (page) {
                    var pageChangeDataItem = page.change_data.find(function (pageChange) { return pageChange.param === changeTitle; });
                    return pageChangeDataItem !== undefined;
                }).map(function (page) {
                    var pageChangeDataItem = page.change_data.find(function (pageChange) { return pageChange.param === changeTitle; });
                    return {
                        url: page.url,
                        crc32url: page.crc32url,
                        previous: pageChangeDataItem.previous,
                        changed: pageChangeDataItem.changed,
                        data: pageChangeDataItem.data
                    };
                });
            }
            var resultBlocks = "";
            if (pagesChanged.length > 0) {
                var changeParams = [
                    {
                        paramTitle: 'title',
                        image: 'event_html',
                        title: 'Title has changed',
                        needsCheckForTitle: false,
                        changedElements: getPagesByChangeTitle(pagesChanged, 'title')
                    },
                    {
                        paramTitle: 'description',
                        image: 'event_html',
                        title: 'Description has changed',
                        needsCheckForTitle: false,
                        changedElements: getPagesByChangeTitle(pagesChanged, 'description')
                    },
                    {
                        paramTitle: 'h1',
                        image: 'event_html',
                        title: 'H1 has changed',
                        needsCheckForTitle: false,
                        changedElements: getPagesByChangeTitle(pagesChanged, 'h1')
                    },
                    {
                        paramTitle: 'http_status_code',
                        image: 'event_server',
                        title: 'HTTP status code has changed',
                        needsCheckForTitle: false,
                        changedElements: getPagesByChangeTitle(pagesChanged, 'http_status_code')
                    },
                    {
                        paramTitle: 'robots_txt_all',
                        image: 'event_server',
                        title: 'Robots.txt has changed',
                        needsCheckForTitle: true,
                        falseTitle: 'URL disallowed',
                        trueTitle: 'URL allowed',
                        changedElements: getPagesByChangeTitle(pagesChanged, 'robots_txt_all')
                    },
                    {
                        paramTitle: 'robots_txt_content',
                        image: 'event_server',
                        title: 'Robots.txt has changed',
                        needsCheckForTitle: true,
                        falseTitle: 'URL disallowed',
                        trueTitle: 'URL allowed',
                        changedElements: getPagesByChangeTitle(pagesChanged, 'robots_txt_content')
                    }, {
                        paramTitle: 'noindex',
                        image: 'event_server',
                        title: 'Index status has changed',
                        needsCheckForTitle: true,
                        falseTitle: 'URL indexation opened by index tag',
                        trueTitle: 'URL indexation closed by noindex tag',
                        changedElements: getPagesByChangeTitle(pagesChanged, 'noindex')
                    }
                ].filter(function (changeParam) {
                    return changeParam.changedElements.length > 0;
                });
                resultBlocks = changeParams.map(function (changeParam) {
                    var changedElements = changeParam.changedElements;
                    var changedArray = changedElements.map(function (page) {
                        var previous = page.changed, changed = page.previous, data = page.data, stuff = [], output = '', array = '', elementSring;
                        $(data).each(function (i, item) {
                            array = item.text.split("\r\n");
                            $(array).each(function (i, element) {
                                elementSring = { string: element, view: item.operation };
                                stuff.push(elementSring);
                            });
                        });
                        $(stuff).each(function (i, item) {
                            var elementString = item.string, elementView = item.view, prevString, prevView, nextString, nextView, secondNextString;
                            if (i !== 0) {
                                prevString = stuff[i - 1].string,
                                    prevView = stuff[i - 1].view;
                            }
                            if (i !== stuff.length - 1) {
                                nextString = stuff[i + 1].string,
                                    nextView = stuff[i + 1].view;
                            }
                            if (i !== stuff.length - 2 && i !== stuff.length - 1) {
                                secondNextString = stuff[i + 2].string;
                            }
                            if (elementView === 'EQUAL' && nextView === 'DELETE' || elementView === 'DELETE' || elementView === 'INSERT' || prevView === 'INSERT') {
                            }
                            else {
                                output = output + '<li class="robots-current">' + elementString + '</li>';
                            }
                            if (item.view === "DELETE") {
                                output = output + '<li class="robots-delete"><span class="prev">' + prevString + '</span><span class="change">' + elementString + '</span><span class="next">' + secondNextString + '</span></li><li class="robots-added"><span class="prev">' + prevString + '</span><span class="change">' + nextString + '</span><span class="next">' + secondNextString + '</span></li>';
                            }
                        });
                        if (data) {
                            return "<ul class=\"robots__changes\">" + output + "</ul>";
                        }
                        else {
                            if (changeParam.needsCheckForTitle === true) {
                                changed = page.changed === false ? changeParam.falseTitle : changeParam.trueTitle;
                                previous = page.previous === false ? changeParam.falseTitle : changeParam.trueTitle;
                            }
                            return "\n                                <div class=\"history__block-data change__child\">\n                                    <div class=\"top\">\n                                        <img src=\"/wp-content/themes/sitechecker/out/img_design/monitoring__edit.svg\"\n                                             alt=\"Deleted Pages\" title=\"Deleted Pages\" class=\"status\">\n                                        <div class=\"links\">\n                                            <a href=\"" + page.url + "\" class=\"internal_link\">" + page.url + "</a><a\n                                                    href=\"" + page.url + "\"\n                                                    target=\"_blank\" class=\"external_link\"><img\n                                                        src=\"/wp-content/themes/sitechecker/out/img_design/external.svg\"\n                                                        alt=\"external\" title=\"external\"></a>\n                                        </div>\n                                        <div class=\"arrow\">\n                                            <img src=\"/wp-content/themes/sitechecker/out/img_design/monitoring__arrow-down.svg\"\n                                                 alt=\"arrow\" title=\"arrow\">\n                                        </div>\n                                    </div>\n                                    <div class=\"body\">\n                                        <div class=\"event__changes\">\n                                            <div class=\"changes\">\n                                                <p>New data:</p>\n                                                <span>" + changed + "</span>\n                                                <img src=\"/wp-content/themes/sitechecker/out/img_design/changed__status.svg\"\n                                                     alt=\"Changed status\" title=\"Changed status\" class=\"status\">\n                                                <p>Previous data:</p>\n                                                <span>" + previous + "</span>\n                                            </div>\n                                        </div>\n                                    </div>\n                                </div>\n                        ";
                        }
                    });
                    var changeArrayHTML = changedArray.join('\n');
                    return "\n                        <div class=\"history__block-data changed\">\n                            <div class=\"top\">\n                                <img src=\"/wp-content/themes/sitechecker/out/img_design/monitoring__edit.svg\"\n                                     alt=\"Changed Pages\" title=\"Changed Pages\" class=\"status\">\n                                <div class=\"links\"><p>" + changeParam.title + "</p></div>\n                                <div class=\"changes-count\" style=\"display: " + (changeParam.title === 'Robots.txt has changed' ? 'none' : 'block') + "\">" + changedArray.length + " changes</div>\n                                <div class=\"arrow\">\n                                    <img src=\"/wp-content/themes/sitechecker/out/img_design/monitoring__arrow-down.svg\"\n                                         alt=\"arrow\" title=\"arrow\">\n                                </div>\n                            </div>\n                            <div class=\"body\">\n                                " + changeArrayHTML + "\n                            </div>\n                        </div>\n                ";
                }).join('\n');
            }
            var timeLineHTML, newPages, deletedPages;
            if (pageAddedArray.length !== 0) {
                newPages = timeLineNewPages;
            }
            else {
                newPages = "";
            }
            if (pageDeletedArray.length !== 0) {
                deletedPages = timeLineDeletedPages;
            }
            else {
                deletedPages = "";
            }
            if (pageAddedArray.length == 0 && pageDeletedArray.length == 0 && pagesChanged.length == 0) {
                timeLineHTML = '';
            }
            else {
                timeLineHTML = '<div class="history__block" id="' + id + '">' +
                    '<p class="date">' + date + '</p>' + newPages + deletedPages + resultBlocks + '</div>';
            }
            return timeLineHTML;
        };
        return MonitoringEvent;
    }(Crawling.BaseTable));
    Crawling.MonitoringEvent = MonitoringEvent;
})(Crawling || (Crawling = {}));
/// <reference path="CrawlingBaseTable.ts"/>
/// <reference path="../../../../../../typings/globals/jquery/index.d.ts" />
var Crawling;
(function (Crawling) {
    var MonitoringTable = /** @class */ (function (_super) {
        __extends(MonitoringTable, _super);
        function MonitoringTable(apiPath, requestPath, token, url) {
            var _this = _super.call(this, apiPath) || this;
            _this.token = token;
            _this.requestPath = requestPath;
            _this.sortParam = 'date_create';
            _this.pageSize = 10;
            _this.url = url;
            return _this;
        }
        MonitoringTable.prototype.processResult = function (response) {
            this.totalCount = response.totalCount;
            this.filteredCount = response.filteredCount;
            this.totalPages = response.pages;
            var events = response.summary;
            if (events.length > 0) {
                this.drawRows(events);
            }
            else if (events.length === 0 && response.pages === 0) {
                $('#next').css('display', 'none');
                $('.empty_array').fadeIn();
            }
            else if (events.length === 0 && response.pages > 0) {
                $('#next').css('display', 'none');
                var firstRow = "<div class=\"event-box first__entry\">\n                    <div class=\"event-box__header started\"><div class=\"event-box__icon\">\n                    <img src=\"/wp-content/themes/sitechecker/out/img/event_start.svg\" alt=\"deleted_pages\" title=\"deleted_pages\"></div>\n                    <div class=\"event-box__label\"><span>Sitechecker started monitoring this website</span></div></div></div>";
                $('.monitoring_tab-first .timeline-entry:last-of-type').append(firstRow);
                $('.first__entry').fadeIn();
            }
        };
        MonitoringTable.prototype.nextPage = function () {
            if (this.page * this.pageSize < this.totalCount) {
                this.page++;
                this.display();
            }
            else {
                $('#next').css('display', 'none');
                var firstRow = "<div class=\"event-box first__entry\">\n                    <div class=\"event-box__header started\"><div class=\"event-box__icon\">\n                    <img src=\"/wp-content/themes/sitechecker/out/img/event_start.svg\" alt=\"deleted_pages\" title=\"deleted_pages\"></div>\n                    <div class=\"event-box__label\"><span>Sitechecker started monitoring this website</span></div></div></div>";
                $('.monitoring_tab-first .timeline-entry:last-of-type').append(firstRow);
                $('.first__entry').fadeIn();
            }
        };
        MonitoringTable.prototype.getSortParam = function () {
            return 'weight';
        };
        MonitoringTable.prototype.getPagination = function () {
            return $('#pageTablePagination');
        };
        MonitoringTable.prototype.getTable = function () {
            return $('.monitoring_tab-first');
        };
        MonitoringTable.prototype.getTBody = function () {
            return this.getTable();
        };
        MonitoringTable.prototype.getCrawlingDate = function () {
            return $('#crawling_date');
        };
        MonitoringTable.prototype.sort = function (column) {
            if (column === this.sortParam) {
                this.sortBy = this.sortBy === 'asc' ? 'desc' : 'asc';
            }
            else {
                this.sortBy = 'asc';
                this.sortParam = column;
            }
            this.display();
        };
        ;
        MonitoringTable.prototype.drawRows = function (events) {
            var _this = this;
            events.forEach(function (event) {
                var row = _this.renderEvent(event);
                _this.addTableRow(row);
            });
            $('#next .fa').css('display', 'none');
        };
        MonitoringTable.prototype.logFailed = function (log) {
            console.log("error" + log);
        };
        MonitoringTable.prototype.display = function () {
            var _this = this;
            this.getPageData().then(function (data) { return _this.processResult(JSON.parse(data)); }, function (error) { return _this.logFailed(error); });
        };
        ;
        MonitoringTable.prototype.getRequestData = function () {
            return {
                sortParam: this.sortParam,
                sortBy: this.sortBy,
                pageSize: this.pageSize,
                page: this.page,
                token: this.token,
                url: this.url
            };
        };
        MonitoringTable.prototype.getPageData = function () {
            var request = this.getRequestData();
            //console.log(request);
            var data = JSON.stringify(request);
            return this.monitoringRequest(data);
        };
        MonitoringTable.prototype.monitoringRequest = function (data) {
            return this.doRequest({
                path: this.requestPath,
                action: 'crawler_request',
                data: data,
                method: 'POST'
            });
        };
        MonitoringTable.prototype.renderEvent = function (event) {
            var _this = this;
            var dateFormat = new Date(event.monitoringStatistic.dateCreate * 1000), options = { weekday: "long", year: "numeric", month: "long", day: "numeric", hour: '2-digit', minute: '2-digit' }, date = dateFormat.toLocaleDateString("en-us", options).replace(",", "");
            var id = event.monitoringStatistic.id;
            var pagesAdded = JSON.parse(event.pagesAdded);
            var pageAddedArray = pagesAdded.map(function (page) {
                var pageUrl = page.url;
                return "<a href=\"" + pageUrl + "\" target=\"_blank\">" + pageUrl + "</a> ";
            }).join('\n');
            var timeLineNewPages = "\n                <div class=\"event-box\">\n                    <div class=\"event-box__header\">\n                        <div class=\"event-box__icon\">\n                            <img src=\"/wp-content/themes/sitechecker/out/img/event_add.svg\" alt=\"added_pages\" title=\"added_pages\">\n                        </div>\n                        <div class=\"event-box__label\">\n                            <span>New pages</span>\n                        </div>\n                        <div class=\"event-box__secondary-section\">\n                            <div class=\"event-box__additional-info\">\n                                <span>" + pagesAdded.length + " pages</span>\n                            </div>\n                        </div>\n                        <div class=\"event-box__dropdown-icon\">\n                            <i class=\"dropdown-icon--greyscale\"></i>\n                        </div>\n                    </div>\n                    <div class=\"event-box__hidden\">\n                        <div class=\"event-box__content\">\n                            " + pageAddedArray + "\n                        </div>\n                    </div>\n                </div>\n            ";
            var pagesDeleted = JSON.parse(event.pagesDeleted);
            var pageDeletedArray = pagesDeleted.map(function (page) {
                var pageUrl = page.url;
                return "<a href=\"" + pageUrl + "\" target=\"_blank\">" + pageUrl + "</a> ";
            }).join('\n');
            var timeLineDeletedPages = "\n                <div class=\"event-box\">\n                    <div class=\"event-box__header\">\n                        <div class=\"event-box__icon\">\n                            <img src=\"/wp-content/themes/sitechecker/out/img/event_delete.svg\" alt=\"deleted_pages\" title=\"deleted_pages\">\n                        </div>\n                        <div class=\"event-box__label\">\n                            <span>Deleted pages</span>\n                        </div>\n                        <div class=\"event-box__secondary-section\">\n                            <div class=\"event-box__additional-info\">\n                                <span>" + pagesDeleted.length + " pages</span>\n                            </div>\n                        </div>\n                        <div class=\"event-box__dropdown-icon\">\n                            <i class=\"dropdown-icon--greyscale\"></i>\n                        </div>\n                    </div>\n                    <div class=\"event-box__hidden\">\n                        <div class=\"event-box__content\">\n                            " + pageDeletedArray + "\n                        </div>\n                    </div>\n                </div>\n            ";
            var pagesChanged = JSON.parse(event.pagesChanged);
            var pagesChangedBlock = pagesChanged.map(function (page) {
                var output = '';
                var pageChangesData = page.change_data.filter(function (pageChange) { return Object.keys(pageChange).length > 0; });
                var pageChangesBlock = pageChangesData.map(function (pageChange) {
                    var image, title, robots, previous = pageChange.changed, changed = pageChange.previous;
                    switch (pageChange.param) {
                        case 'title':
                            image = 'event_html';
                            title = 'Title has changed';
                            break;
                        case 'description':
                            image = 'event_html';
                            title = 'Description has changed';
                            break;
                        case 'h1':
                            image = 'event_html';
                            title = 'H1 has changed';
                            break;
                        case 'http_status_code':
                            image = 'event_server';
                            title = 'HTTP status code has changed';
                            break;
                        case 'robots_txt_all':
                            image = 'event_server';
                            title = 'Robots.txt has changed';
                            if (previous === false) {
                                previous = 'URL disallowed';
                            }
                            else {
                                previous = 'URL allowed';
                            }
                            if (changed === false) {
                                changed = 'URL disallowed';
                            }
                            else {
                                changed = 'URL allowed';
                            }
                            break;
                        case 'robots_txt_content':
                            image = 'event_server';
                            title = 'robots_txt_content';
                            robots = pageChange.data;
                            var stuff_1 = [], array_1 = '', elementSring_1;
                            $(robots).each(function (i, item) {
                                array_1 = item.text.split("\r\n");
                                $(array_1).each(function (i, element) {
                                    elementSring_1 = { string: element, view: item.operation };
                                    stuff_1.push(elementSring_1);
                                });
                            });
                            $(stuff_1).each(function (i, item) {
                                var elementString = item.string, elementView = item.view;
                                if (elementView === "DELETE" && elementString !== '') {
                                    output = output + '<li class="robots-delete"><span class="change">' + elementString + '</span></li>';
                                }
                                else if (elementView === 'INSERT' && elementString !== '') {
                                    output = output + '<li class="robots-added"><span class="change">' + elementString + '</span></li>';
                                }
                                else if (elementString !== '') {
                                    output = output + '<li class="robots-current">' + elementString + '</li>';
                                }
                            });
                            break;
                        case 'noindex':
                            image = 'event_server';
                            title = 'Index status has changed';
                            if (previous === true) {
                                previous = 'URL indexation closed by noindex tag';
                            }
                            else {
                                previous = 'URL indexation opened by index tag';
                            }
                            if (changed === true) {
                                changed = 'URL indexation closed by noindex tag';
                            }
                            else {
                                changed = 'URL indexation opened by index tag';
                            }
                            break;
                        default:
                            image = 'event_edit';
                            break;
                    }
                    return "\n                            <div class=\"event-box\">\n                                <div class=\"event-box__header\">\n                                    <div class=\"event-box__icon\">\n                                        <img src=\"/wp-content/themes/sitechecker/out/img/" + image + ".svg\" alt=\"edited_pages\" title=\"edited_pages\">\n                                    </div>\n                                    <div class=\"event-box__label\">\n                                        <span>" + title + "</span>\n                                    </div>\n                                    <div class=\"event-box__dropdown-icon\">\n                                        <i class=\"dropdown-icon--greyscale\"></i>\n                                    </div>\n                                </div>\n                                <div class=\"event-box__hidden hidden_child\">\n                                    <div class=\"event-box__content\">\n                                        <div class=\"event-box__data\">\n                                            <div class=\"separated-blocks\">\n                                                <div class=\"separated-blocks__block\">\n                                                    <p class=\"labeled-value__title\">New data</p>\n                                                    <div class=\"labeled-value__row\">\n                                                        <p>" + changed + "</p>\n                                                    </div>\n                                                </div>\n                                                <div class=\"separated-blocks__block\">\n                                                    <p class=\"labeled-value__title\">Previous data</p>\n                                                    <div class=\"labeled-value__row\">\n                                                        <p>" + previous + "</p>\n                                                    </div>\n                                                </div>\n                                            </div>\n                                        </div>\n                                    </div>\n                                </div>\n                            </div>\n                        ";
                }).join('\n');
                if (output.length > 0) {
                    return "\n                     <div class=\"event-box\">\n                        <div class=\"event-box__header\">\n                            <div class=\"event-box__icon\">\n                                    <img src=\"/wp-content/themes/sitechecker/out/img/event_edit.svg\" alt=\"edited_pages\" title=\"edited_pages\">\n                                </div>\n                            <div class=\"event-box__label\">\n                                <div><a href=\"" + page.url + "\" target=\"_blank\">" + page.url + "</a></div>\n                            </div>\n                            <div class=\"event-box__secondary-section\"></div>\n                            <div class=\"event-box__dropdown-icon\">\n                                <i class=\"dropdown-icon--greyscale\"></i>\n                            </div>\n                            </div>\n                        <div class=\"event-box__hidden hidden_parent\">\n                            <div class=\"event-box__content\">\n                                <ul class=\"robots__changes\">" + output + "</ul>\n                            </div>\n                        </div>\n                    </div>\n                ";
                }
                else {
                    return "\n                     <div class=\"event-box\">\n                        <div class=\"event-box__header\">\n                            <div class=\"event-box__icon\">\n                                    <img src=\"/wp-content/themes/sitechecker/out/img/event_edit.svg\" alt=\"edited_pages\" title=\"edited_pages\">\n                                </div>\n                            <div class=\"event-box__label\">\n                                <div><a href=\"/page-information/?url=" + page.crc32url + "&token=" + _this.token + "\">" + page.url + "</a> <a href=\"" + page.url + "\" target=\"_blank\"><img src=\"/wp-content/themes/sitechecker/out/img/external-link.svg\" alt=\"link\" title=\"link\"></a></div>\n                            </div>\n                            <div class=\"event-box__secondary-section\">\n                                <div class=\"event-box__additional-info\">\n                                    <span>" + pageChangesData.length + " changes</span>\n                                </div>\n                            </div>\n                            <div class=\"event-box__dropdown-icon\">\n                                <i class=\"dropdown-icon--greyscale\"></i>\n                            </div>\n                            </div>\n                        <div class=\"event-box__hidden hidden_parent\">\n                            <div class=\"event-box__content\">\n                            " + pageChangesBlock + "\n                            </div>\n                        </div>\n                    </div>\n                ";
                }
            }).join('\n');
            var timeLineHTML, newPages, deletedPages;
            if (pageAddedArray.length !== 0) {
                newPages = "" + timeLineNewPages;
            }
            else {
                newPages = "";
            }
            if (pageDeletedArray.length !== 0) {
                deletedPages = "" + timeLineDeletedPages;
            }
            else {
                deletedPages = "";
            }
            if (pageAddedArray.length == 0 && pageDeletedArray.length == 0 && pagesChanged.length == 0) {
                timeLineHTML = '';
            }
            else {
                timeLineHTML = '<div class="timeline-entry" id="' + id + '">' +
                    '<div class="timeline-entry__title"><span>' + date + '</span></div>' +
                    '<div class="timeline-entry__data">' +
                    '<div>' + newPages + '</div>' +
                    '<div>' + deletedPages + '</div>' +
                    '<div>' + pagesChangedBlock + '</div>' +
                    '</div></div>';
            }
            return timeLineHTML;
        };
        return MonitoringTable;
    }(Crawling.BaseTable));
    Crawling.MonitoringTable = MonitoringTable;
})(Crawling || (Crawling = {}));
/// <reference path="CrawlingBaseTable.ts"/>
/// <reference path="../../../../../../typings/globals/jquery/index.d.ts" />
var Crawling;
(function (Crawling) {
    var MonitoringTableDesign = /** @class */ (function (_super) {
        __extends(MonitoringTableDesign, _super);
        function MonitoringTableDesign(apiPath, requestPath, token, url) {
            var _this = _super.call(this, apiPath) || this;
            _this.token = token;
            _this.requestPath = requestPath;
            _this.sortParam = 'date_create';
            _this.pageSize = 10;
            _this.url = url;
            return _this;
        }
        MonitoringTableDesign.prototype.processResult = function (response) {
            this.totalCount = response.totalCount;
            this.filteredCount = response.filteredCount;
            this.totalPages = response.pages;
            var events = response.summary;
            if (events.length > 0) {
                $('.monitoring_graph').css('display', 'flex');
                this.drawRows(events);
            }
            else if (events.length === 0 && response.pages === 0) {
                $('.empty_array').fadeIn();
            }
            else if (events.length === 0 && response.pages > 0) {
                $('.nextData').css('display', 'none');
                var firstRow = "<div class=\"history__block-data first__entry\">\n                        <div class=\"top\">\n                            <img src=\"/wp-content/themes/sitechecker/out/img_design/monitoring__plus.svg\" alt=\"new pages\" title=\"new pages\" class=\"status\">\n                            <p>Sitechecker started monitoring this website</p>\n                        </div>\n                    </div>";
                $('#calendar .history__blocks .history__block:last-of-type').append(firstRow);
                $('#calendar .first__entry').fadeIn();
            }
        };
        MonitoringTableDesign.prototype.nextPage = function () {
            if (this.page * this.pageSize < this.totalCount) {
                this.page++;
                this.display();
            }
        };
        MonitoringTableDesign.prototype.getSortParam = function () {
            return 'weight';
        };
        MonitoringTableDesign.prototype.getPagination = function () {
            return $('#pageTablePagination');
        };
        MonitoringTableDesign.prototype.getTable = function () {
            return $('#calendar .history__blocks');
        };
        MonitoringTableDesign.prototype.getTBody = function () {
            return this.getTable();
        };
        MonitoringTableDesign.prototype.getCrawlingDate = function () {
            return $('#crawling_date');
        };
        MonitoringTableDesign.prototype.sort = function (column) {
            if (column === this.sortParam) {
                this.sortBy = this.sortBy === 'asc' ? 'desc' : 'asc';
            }
            else {
                this.sortBy = 'asc';
                this.sortParam = column;
            }
            this.display();
        };
        ;
        MonitoringTableDesign.prototype.drawRows = function (events) {
            var _this = this;
            events.forEach(function (event) {
                var row = _this.renderEvent(event);
                _this.addTableRow(row);
            });
            $('#nextData').fadeIn();
            $('.nextData .loader').css('display', 'none');
        };
        MonitoringTableDesign.prototype.logFailed = function (log) {
            console.log("error" + log);
        };
        MonitoringTableDesign.prototype.display = function () {
            var _this = this;
            this.getPageData().then(function (data) { return _this.processResult(JSON.parse(data)); }, function (error) { return _this.logFailed(error); });
        };
        ;
        MonitoringTableDesign.prototype.getRequestData = function () {
            return {
                sortParam: this.sortParam,
                sortBy: this.sortBy,
                pageSize: this.pageSize,
                page: this.page,
                token: this.token,
                url: this.url
            };
        };
        MonitoringTableDesign.prototype.getPageData = function () {
            var request = this.getRequestData();
            //console.log(request);
            var data = JSON.stringify(request);
            return this.monitoringRequest(data);
        };
        MonitoringTableDesign.prototype.monitoringRequest = function (data) {
            return this.doRequest({
                path: this.requestPath,
                action: 'crawler_request',
                data: data,
                method: 'POST'
            });
        };
        MonitoringTableDesign.prototype.renderEvent = function (event) {
            var _this = this;
            var dateFormat = new Date(event.monitoringStatistic.dateCreate * 1000), options = { weekday: "long", year: "numeric", month: "long", day: "numeric", hour: '2-digit', minute: '2-digit' }, date = dateFormat.toLocaleDateString("en-us", options).replace(",", "");
            var id = event.monitoringStatistic.id;
            var pagesAdded = JSON.parse(event.pagesAdded);
            var pageAddedArray = pagesAdded.map(function (page) {
                var pageUrl = page.url;
                return "<li><div class=\"links\"><a href=\"" + pageUrl + "\" class=\"internal_link\">" + pageUrl + "</a></div></li>";
            }).join('\n');
            var timeLineNewPages = "\n                <div class=\"history__block-data added\">\n                    <div class=\"top\">\n                        <img src=\"/wp-content/themes/sitechecker/out/img_design/monitoring__plus.svg\"\n                             alt=\"new pages\" title=\"new pages\" class=\"status\">\n                        <p>" + pagesAdded.length + " new pages</p>\n                        <div class=\"arrow\">\n                            <img src=\"/wp-content/themes/sitechecker/out/img_design/monitoring__arrow-down.svg\"\n                                 alt=\"arrow\" title=\"arrow\">\n                        </div>\n                    </div>\n                    <div class=\"body\">\n                        <ul>\n                            " + pageAddedArray + "\n                        </ul>\n                    </div>\n                </div>\n            ";
            var pagesDeleted = JSON.parse(event.pagesDeleted);
            var pageDeletedArray = pagesDeleted.map(function (page) {
                var pageUrl = page.url;
                return "<li><div><a href=\"" + pageUrl + "\">" + pageUrl + "</a></div></li>";
            }).join('\n');
            var timeLineDeletedPages = "\n                <div class=\"history__block-data deleted\">\n                    <div class=\"top\">\n                        <img src=\"/wp-content/themes/sitechecker/out/img_design/monitoring__delete.svg\"\n                             alt=\"Deleted Pages\" title=\"Deleted Pages\" class=\"status\">\n                        <p>" + pagesDeleted.length + " deleted pages</p>\n                        <div class=\"arrow\">\n                            <img src=\"/wp-content/themes/sitechecker/out/img_design/monitoring__arrow-down.svg\"\n                                 alt=\"arrow\" title=\"arrow\">\n                        </div>\n                    </div>\n                    <div class=\"body\">\n                        <ul>\n                            " + pageDeletedArray + "\n                        </ul>\n                    </div>\n                </div>\n            ";
            var pagesChanged = JSON.parse(event.pagesChanged);
            var pagesChangedBlock = pagesChanged.map(function (page) {
                var output = '';
                var pageChangesData = page.change_data.filter(function (pageChange) { return Object.keys(pageChange).length > 0; });
                var pageChangesBlock = pageChangesData.map(function (pageChange, index) {
                    var image, title, robots, previous = pageChange.changed, changed = pageChange.previous;
                    switch (pageChange.param) {
                        case 'title':
                            image = 'event_html';
                            title = 'Title has changed';
                            break;
                        case 'description':
                            image = 'event_html';
                            title = 'Description has changed';
                            break;
                        case 'h1':
                            image = 'event_html';
                            title = 'H1 has changed';
                            break;
                        case 'http_status_code':
                            image = 'event_server';
                            title = 'HTTP status code has changed';
                            break;
                        case 'robots_txt_all':
                            image = 'event_server';
                            title = 'Robots.txt has changed';
                            if (previous === false) {
                                previous = 'URL disallowed';
                            }
                            else {
                                previous = 'URL allowed';
                            }
                            if (changed === false) {
                                changed = 'URL disallowed';
                            }
                            else {
                                changed = 'URL allowed';
                            }
                            break;
                        case 'robots_txt_content':
                            image = 'event_server';
                            title = 'robots_txt_content';
                            robots = pageChange.data;
                            var stuff_2 = [], array_2 = '', elementSring_2;
                            $(robots).each(function (i, item) {
                                array_2 = item.text.split("\r\n");
                                $(array_2).each(function (i, element) {
                                    elementSring_2 = { string: element, view: item.operation };
                                    stuff_2.push(elementSring_2);
                                });
                            });
                            $(stuff_2).each(function (i, item) {
                                var elementString = item.string, elementView = item.view, prevString, prevView, nextString, nextView, secondNextString;
                                if (i !== 0) {
                                    prevString = stuff_2[i - 1].string,
                                        prevView = stuff_2[i - 1].view;
                                }
                                if (i !== stuff_2.length - 1) {
                                    nextString = stuff_2[i + 1].string,
                                        nextView = stuff_2[i + 1].view;
                                }
                                if (i !== stuff_2.length - 2 && i !== stuff_2.length - 1) {
                                    secondNextString = stuff_2[i + 2].string;
                                }
                                if (elementView === 'EQUAL' && nextView === 'DELETE' || elementView === 'DELETE' || elementView === 'INSERT' || prevView === 'INSERT') {
                                }
                                else {
                                    output = output + '<li class="robots-current">' + elementString + '</li>';
                                }
                                if (item.view === "DELETE") {
                                    output = output + '<li class="robots-delete"><span class="prev">' + prevString + '</span><span class="change">' + elementString + '</span><span class="next">' + secondNextString + '</span></li><li class="robots-added"><span class="prev">' + prevString + '</span><span class="change">' + nextString + '</span><span class="next">' + secondNextString + '</span></li>';
                                }
                            });
                            break;
                        case 'noindex':
                            image = 'event_server';
                            title = 'Index status has changed';
                            if (previous === true) {
                                previous = 'URL indexation closed by noindex tag';
                            }
                            else {
                                previous = 'URL indexation opened by index tag';
                            }
                            if (changed === true) {
                                changed = 'URL indexation closed by noindex tag';
                            }
                            else {
                                changed = 'URL indexation opened by index tag';
                            }
                            break;
                        default:
                            image = 'event_edit';
                            break;
                    }
                    return "\n                        <div class=\"history__block-data change__child\">\n                            <div class=\"top\">\n                                <img src=\"/wp-content/themes/sitechecker/out/img_design/monitoring__edit.svg\"\n                                     alt=\"Changed Pages\" title=\"Changed Pages\" class=\"status\">\n                                <div class=\"links\"><p>" + title + "</p></div>\n                                <div class=\"arrow\">\n                                    <img src=\"/wp-content/themes/sitechecker/out/img_design/monitoring__arrow-down.svg\"\n                                         alt=\"arrow\" title=\"arrow\">\n                                </div>\n                            </div>\n                            <div class=\"body\">\n                                <div class=\"event__changes\">\n                                    <div class=\"changes\">\n                                        <p>New data:</p>\n                                        <span>" + changed + "</span>\n                                        <img src=\"/wp-content/themes/sitechecker/out/img_design/changed__status.svg\"\n                                             alt=\"Changed status\" title=\"Changed status\" class=\"status\">\n                                        <p>Previous data:</p>\n                                        <span>" + previous + "</span>\n                                    </div>\n                                </div>\n                            </div>\n                        </div>\n                        ";
                }).join('\n');
                if (output.length > 0) {
                    return "\n                    <div class=\"history__block-data changed\">\n                        <div class=\"top\">\n                            <img src=\"/wp-content/themes/sitechecker/out/img_design/monitoring__edit.svg\"\n                                 alt=\"Changed Pages\" title=\"Changed Pages\" class=\"status\">\n                            <div class=\"links\">\n                                <a href=\"" + page.url + "\" class=\"internal_link\">" + page.url + "</a><a\n                                        href=\"" + page.url + "\"\n                                        target=\"_blank\" class=\"external_link\"><img\n                                            src=\"/wp-content/themes/sitechecker/out/img_design/external.svg\"\n                                            alt=\"external\" title=\"external\"></a>\n                            </div>\n                            <div class=\"arrow\">\n                                <img src=\"/wp-content/themes/sitechecker/out/img_design/monitoring__arrow-down.svg\"\n                                     alt=\"arrow\" title=\"arrow\">\n                            </div>\n                        </div>\n                        <div class=\"body\">\n                            <ul class=\"robots__changes\">" + output + "</ul>\n                        </div>\n                    </div>\n                ";
                }
                else {
                    return "\n                    <div class=\"history__block-data changed\">\n                        <div class=\"top\">\n                            <img src=\"/wp-content/themes/sitechecker/out/img_design/monitoring__edit.svg\"\n                                 alt=\"Changed Pages\" title=\"Changed Pages\" class=\"status\">\n                            <div class=\"links\">\n                                <a href=\"/page-information/?url=" + page.crc32url + "&token=" + _this.token + "\" class=\"internal_link\">" + page.url + "</a><a\n                                        href=\"" + page.url + "\"\n                                        target=\"_blank\" class=\"external_link\"><img\n                                            src=\"/wp-content/themes/sitechecker/out/img_design/external.svg\"\n                                            alt=\"external\" title=\"external\"></a>\n                            </div>\n                            <div class=\"changes-count\">" + pageChangesData.length + " changes</div>\n                            <div class=\"arrow\">\n                                <img src=\"/wp-content/themes/sitechecker/out/img_design/monitoring__arrow-down.svg\"\n                                     alt=\"arrow\" title=\"arrow\">\n                            </div>\n                        </div>\n                        <div class=\"body\">\n                            " + pageChangesBlock + "\n                        </div>\n                    </div>\n                ";
                }
            }).join('\n');
            var timeLineHTML, newPages, deletedPages, changesPages;
            if (pageAddedArray.length !== 0) {
                newPages = timeLineNewPages;
            }
            else {
                newPages = "";
            }
            if (pageDeletedArray.length !== 0) {
                deletedPages = timeLineDeletedPages;
            }
            else {
                deletedPages = "";
            }
            if (pagesChanged.length !== 0) {
                changesPages = pagesChangedBlock;
            }
            else {
                changesPages = "";
            }
            if (pageAddedArray.length == 0 && pageDeletedArray.length == 0 && pagesChanged.length == 0) {
                timeLineHTML = '';
            }
            else {
                timeLineHTML = '<div class="history__block" id="' + id + '">' +
                    '<p class="date">' + date + '</p>' + newPages + deletedPages + changesPages + '</div>';
            }
            return timeLineHTML;
        };
        return MonitoringTableDesign;
    }(Crawling.BaseTable));
    Crawling.MonitoringTableDesign = MonitoringTableDesign;
})(Crawling || (Crawling = {}));
/// <reference path="CrawlingBaseTable.ts"/>
/// <reference path="../../../../../../typings/globals/jquery/index.d.ts" />
var Crawling;
(function (Crawling) {
    var MonitoringTableForPage = /** @class */ (function (_super) {
        __extends(MonitoringTableForPage, _super);
        function MonitoringTableForPage(apiPath, requestPath, token, url) {
            var _this = _super.call(this, apiPath) || this;
            _this.token = token;
            _this.url = url;
            _this.requestPath = requestPath;
            _this.pageSize = 10;
            return _this;
        }
        MonitoringTableForPage.prototype.processResult = function (response) {
            console.log(response);
            this.totalCount = response.totalCount;
            this.filteredCount = response.filteredCount;
            this.totalPages = response.pages;
            var events = response.summary;
            this.drawRows(events);
        };
        MonitoringTableForPage.prototype.nextPage = function () {
            if (this.page * this.pageSize < this.totalCount) {
                this.page++;
                this.display();
            }
            else {
                $('#next-second').css('display', 'none');
                $('.empty_array').fadeIn();
            }
        };
        MonitoringTableForPage.prototype.getSortParam = function () {
            return 'date_create';
        };
        MonitoringTableForPage.prototype.getPagination = function () {
            return $('#pageTablePagination');
        };
        MonitoringTableForPage.prototype.getTable = function () {
            return $('.monitoring_tab-second');
        };
        MonitoringTableForPage.prototype.getTBody = function () {
            return this.getTable();
        };
        MonitoringTableForPage.prototype.getCrawlingDate = function () {
            return $('#crawling_date');
        };
        MonitoringTableForPage.prototype.sort = function (column) {
            if (column === this.sortParam) {
                this.sortBy = 'desc';
            }
            else {
                this.sortBy = 'desc';
                this.sortParam = column;
            }
            this.display();
        };
        ;
        MonitoringTableForPage.prototype.drawRows = function (events) {
            var _this = this;
            if (events.length == 0) {
                $('#next-second').css('display', 'none');
                $('.empty_array').fadeIn();
            }
            else {
                $('#next-second').css('display', 'flex');
            }
            events.forEach(function (event) {
                var row = _this.renderEvent(event);
                _this.addTableRow(row);
            });
            $('#next-second .fa').css('display', 'none');
        };
        MonitoringTableForPage.prototype.logFailed = function (log) {
            console.log("error" + log);
        };
        MonitoringTableForPage.prototype.display = function () {
            var _this = this;
            this.getPageData().then(function (data) { return _this.processResult(JSON.parse(data)); }, function (error) { return _this.logFailed(error); });
        };
        ;
        MonitoringTableForPage.prototype.getRequestData = function () {
            return {
                sortParam: this.sortParam,
                sortBy: this.sortBy,
                pageSize: this.pageSize,
                page: this.page,
                token: this.token,
                url: this.url
            };
        };
        MonitoringTableForPage.prototype.getPageData = function () {
            var request = this.getRequestData(), data = JSON.stringify(request);
            return this.monitoringRequest(data);
        };
        MonitoringTableForPage.prototype.monitoringRequest = function (data) {
            return this.doRequest({
                path: this.requestPath,
                action: 'crawler_request',
                data: data,
                method: 'POST'
            });
        };
        MonitoringTableForPage.prototype.renderEvent = function (event) {
            var dateFormat = new Date(event.monitoringStatistic.dateCreate * 1000), date = dateFormat.toGMTString().split(' ', 5).join(' ');
            var pagesChanged = JSON.parse(event.changeData);
            var resultBlocks = pagesChanged.filter(function (page) {
                var pageChangeDataItem = page.param;
                return pageChangeDataItem !== undefined;
            }).map(function (page) {
                var pageParam = page.param;
                var image, title, pageChanged = page.changed, pagePrevious = page.previous;
                switch (pageParam) {
                    case 'title':
                        image = 'event_html';
                        title = 'Title has changed';
                        break;
                    case 'description':
                        image = 'event_html';
                        title = 'Description has changed';
                        break;
                    case 'h1':
                        image = 'event_html';
                        title = 'H1 has changed';
                        break;
                    case 'http_status_code':
                        image = 'event_server';
                        title = 'HTTP status code has changed';
                        break;
                    case 'robots_txt_all':
                        image = 'event_server';
                        title = 'Robots.txt has changed';
                        if (pageChanged === false) {
                            pageChanged = 'URL disallowed';
                        }
                        else {
                            pageChanged = 'URL allowed';
                        }
                        if (pagePrevious === false) {
                            pagePrevious = 'URL disallowed';
                        }
                        else {
                            pagePrevious = 'URL allowed';
                        }
                        break;
                    case 'noindex':
                        image = 'event_server';
                        title = 'Index status has changed';
                        if (pagePrevious === true) {
                            pagePrevious = 'URL indexation closed by noindex tag';
                        }
                        else {
                            pagePrevious = 'URL indexation opened by index tag';
                        }
                        if (pageChanged === true) {
                            pageChanged = 'URL indexation closed by noindex tag';
                        }
                        else {
                            pageChanged = 'URL indexation opened by index tag';
                        }
                        break;
                    default:
                        image = 'event_edit';
                        break;
                }
                return "<div class=\"event-box domain_monitoring\">\n                            <div class=\"event-box__head\">\n                                <div class=\"event-box__icon\">\n                                    <img src=\"/wp-content/themes/sitechecker/out/img/" + image + ".svg\" alt=\"edited_pages\" title=\"edited_pages\">\n                                </div>\n                                <div class=\"event-box__label\">\n                                    <span>" + title + "</span>\n                                </div>\n                                <div class=\"event-box__dropdown-icon\">\n                                    <i class=\"dropdown-icon--greyscale\"></i>\n                                </div>\n                            </div>\n                            <div class=\"event-box__hidden hidden_child\">\n                                <div class=\"event-box__content\">\n                                    <div class=\"event-box__data\">\n                                        <div class=\"separated-blocks\">\n                                            <div class=\"separated-blocks__block\">\n                                                <p class=\"labeled-value__title\">New data</p>\n                                                <div class=\"labeled-value__row\">\n                                                    <p>" + pagePrevious + "</p>\n                                                </div>\n                                            </div>\n                                            <div class=\"separated-blocks__block\">\n                                                <p class=\"labeled-value__title\">Previous data</p>\n                                                <div class=\"labeled-value__row\">\n                                                    <p>" + pageChanged + "</p>\n                                                </div>\n                                            </div>\n                                        </div>\n                                    </div>\n                                </div>\n                            </div>\n                        </div>";
            }).join('\n');
            var timeLineHTML = '<div class="timeline-entry">' +
                '<div class="timeline-entry__title"><span>' + date + '</span></div>' +
                '<div class="robots">' + resultBlocks + '</div>' +
                '</div>';
            return timeLineHTML;
        };
        return MonitoringTableForPage;
    }(Crawling.BaseTable));
    Crawling.MonitoringTableForPage = MonitoringTableForPage;
})(Crawling || (Crawling = {}));
/// <reference path="CrawlingBaseTable.ts"/>
/// <reference path="../../../../../../typings/globals/jquery/index.d.ts" />
var Crawling;
(function (Crawling) {
    var MonitoringTableForPageDesign = /** @class */ (function (_super) {
        __extends(MonitoringTableForPageDesign, _super);
        function MonitoringTableForPageDesign(apiPath, requestPath, token, url) {
            var _this = _super.call(this, apiPath) || this;
            _this.token = token;
            _this.url = url;
            _this.requestPath = requestPath;
            _this.pageSize = 10;
            return _this;
        }
        MonitoringTableForPageDesign.prototype.processResult = function (response) {
            console.log(response);
            this.totalCount = response.totalCount;
            this.filteredCount = response.filteredCount;
            this.totalPages = response.pages;
            var events = response.summary;
            this.drawRows(events);
        };
        MonitoringTableForPageDesign.prototype.nextPage = function () {
            if (this.page * this.pageSize < this.totalCount) {
                this.page++;
                this.display();
            }
            else {
                $('.load__more').css('display', 'none');
                $('.monitoring__history-point.started').fadeIn();
            }
        };
        MonitoringTableForPageDesign.prototype.getSortParam = function () {
            return 'date_create';
        };
        MonitoringTableForPageDesign.prototype.getPagination = function () {
            return $('#pageTablePagination');
        };
        MonitoringTableForPageDesign.prototype.getTable = function () {
            return $('.history__block');
        };
        MonitoringTableForPageDesign.prototype.getTBody = function () {
            return this.getTable();
        };
        MonitoringTableForPageDesign.prototype.getCrawlingDate = function () {
            return $('#crawling_date');
        };
        MonitoringTableForPageDesign.prototype.sort = function (column) {
            if (column === this.sortParam) {
                this.sortBy = 'desc';
            }
            else {
                this.sortBy = 'desc';
                this.sortParam = column;
            }
            this.display();
        };
        ;
        MonitoringTableForPageDesign.prototype.drawRows = function (events) {
            var _this = this;
            if (events.length == 0) {
                $('.load__more').css('display', 'none');
                $('.monitoring__history-point.started').fadeIn();
            }
            else {
                $('.load__more').css('display', 'flex');
            }
            events.forEach(function (event) {
                var row = _this.renderEvent(event);
                _this.addTableRow(row);
            });
        };
        MonitoringTableForPageDesign.prototype.logFailed = function (log) {
            console.log("error" + log);
        };
        MonitoringTableForPageDesign.prototype.display = function () {
            var _this = this;
            this.getPageData().then(function (data) { return _this.processResult(JSON.parse(data)); }, function (error) { return _this.logFailed(error); });
        };
        ;
        MonitoringTableForPageDesign.prototype.getRequestData = function () {
            return {
                sortParam: this.sortParam,
                sortBy: this.sortBy,
                pageSize: this.pageSize,
                page: this.page,
                token: this.token,
                url: this.url
            };
        };
        MonitoringTableForPageDesign.prototype.getPageData = function () {
            var request = this.getRequestData(), data = JSON.stringify(request);
            return this.monitoringRequest(data);
        };
        MonitoringTableForPageDesign.prototype.monitoringRequest = function (data) {
            return this.doRequest({
                path: this.requestPath,
                action: 'crawler_request',
                data: data,
                method: 'POST'
            });
        };
        MonitoringTableForPageDesign.prototype.renderEvent = function (event) {
            var weekday = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"], monthNames = ["January", "February", "March", "April", "May", "June",
                "July", "August", "September", "October", "November", "December"
            ];
            function formatAMPM(date) {
                var hours = date.getHours(), minutes = date.getMinutes(), ampm = hours >= 12 ? 'pm' : 'am';
                hours = hours % 12;
                hours = hours ? hours : 12;
                minutes = minutes < 10 ? '0' + minutes : minutes;
                return hours + ':' + minutes + ' ' + ampm;
            }
            var dateFormat = new Date(event.dateCreate * 1000), date = formatAMPM(dateFormat), dateDay = weekday[dateFormat.getDay()] + ' ' + monthNames[dateFormat.getMonth()] + ' ' + dateFormat.getDate() + ', ' + dateFormat.getFullYear();
            var pagesChanged = JSON.parse(event.changeData);
            var resultBlocks = pagesChanged.filter(function (page) {
                var pageChangeDataItem = page.param;
                return pageChangeDataItem !== undefined;
            }).map(function (page) {
                var pageParam = page.param;
                var image, title, pageChanged = page.changed, pagePrevious = page.previous;
                if (pageChanged === '') {
                    pageChanged = '<p class="message__error">No ' + pageParam + '</p>';
                }
                if (pagePrevious === '') {
                    pagePrevious = '<p class="message__error">No ' + pageParam + '</p>';
                }
                switch (pageParam) {
                    case 'title':
                        image = 'event_html';
                        title = 'Title has changed';
                        break;
                    case 'description':
                        image = 'event_html';
                        title = 'Description has changed';
                        break;
                    case 'h1':
                        image = 'event_html';
                        title = 'H1 has changed';
                        break;
                    case 'http_status_code':
                        image = 'event_server';
                        title = 'HTTP status code has changed';
                        break;
                    case 'robots_txt_all':
                        image = 'event_server';
                        title = 'Robots.txt has changed';
                        if (pageChanged === false) {
                            pageChanged = 'URL disallowed';
                        }
                        else {
                            pageChanged = 'URL allowed';
                        }
                        if (pagePrevious === false) {
                            pagePrevious = 'URL disallowed';
                        }
                        else {
                            pagePrevious = 'URL allowed';
                        }
                        break;
                    case 'noindex':
                        image = 'event_server';
                        title = 'Index status has changed';
                        if (pagePrevious === true) {
                            pagePrevious = 'URL indexation closed by noindex tag';
                        }
                        else {
                            pagePrevious = 'URL indexation opened by index tag';
                        }
                        if (pageChanged === true) {
                            pageChanged = 'URL indexation closed by noindex tag';
                        }
                        else {
                            pageChanged = 'URL indexation opened by index tag';
                        }
                        break;
                    default:
                        image = 'event_edit';
                        break;
                }
                return "<div class=\"event-box domain_monitoring\">\n                            <div class=\"header\"><img src=\"/wp-content/themes/sitechecker/out/img_design/monitoring_status.svg\" alt=\"status\" title=\"" + title + "\">" + title + "<p>" + date + "</p>\n                            <svg width=\"9\" height=\"5\" viewBox=\"0 0 9 5\" fill=\"none\" xmlns=\"http://www.w3.org/2000/svg\"><path fill-rule=\"evenodd\" clip-rule=\"evenodd\" d=\"M4.499 0L9 2.5V4.896L4.499 2.38L0 5V2.5L4.499 0Z\" transform=\"translate(9 5) rotate(-180)\" fill=\"#cccccc\"></path></svg></div>\n                            <div class=\"body\">\n                                <div class=\"container\">\n                                    <div class=\"changes\">\n                                        <p class=\"" + image + "\">" + pagePrevious + "</p>\n                                        <img src=\"/wp-content/themes/sitechecker/out/img_design/arrow_monitoring.svg\" alt=\"arrow\" title=\"arrow\">\n                                        <p class=\"" + image + "\">" + pageChanged + "</p>\n                                    </div>\n                                </div>\n                            </div>\n                        </div>";
            }).join('\n');
            var timeLineHTML = '', historyPoint = $('.monitoring__history-point[name="' + dateDay + '"]');
            if (historyPoint.length > 0) {
                historyPoint.find('.monitoring__history-event').append(resultBlocks);
            }
            else {
                timeLineHTML = '<div class="monitoring__history-point" name="' + dateDay + '">' +
                    '<p class="history-point-date">' + dateDay + '</p>' +
                    '<div class="monitoring__history-event">' + resultBlocks + '</div></div>';
            }
            $('.monitoring__history-content').fadeIn();
            return timeLineHTML;
        };
        return MonitoringTableForPageDesign;
    }(Crawling.BaseTable));
    Crawling.MonitoringTableForPageDesign = MonitoringTableForPageDesign;
})(Crawling || (Crawling = {}));
/// <reference path="CrawlingBaseTable.ts"/>
/// <reference path="../../../../../../typings/globals/jquery/index.d.ts" />
var Crawling;
(function (Crawling) {
    var MonitoringTabTable = /** @class */ (function (_super) {
        __extends(MonitoringTabTable, _super);
        function MonitoringTabTable(apiPath, requestPath, token, interval, filterDate, searchParamUrl, isIndex) {
            var _this = _super.call(this, apiPath) || this;
            _this.requestPath = requestPath;
            _this.sortParam = 'date_create';
            _this.pageSize = 10;
            _this.token = token;
            _this.interval = interval;
            _this.filterDate = filterDate;
            _this.searchParamUrl = searchParamUrl;
            _this.isIndex = isIndex;
            return _this;
        }
        MonitoringTabTable.prototype.processResult = function (response) {
            this.totalCount = response.totalCount;
            this.filteredCount = response.filteredCount;
            this.totalPages = response.pages;
            var events = response.summary;
            var searchParam = false;
            if (this.searchParamUrl !== undefined && this.searchParamUrl !== '') {
                searchParam = true;
            }
            if (events.length > 0) {
                $('.empty_array').css('display', 'none');
                this.drawRows(events);
                $('.monitoring__graph-block').css({
                    'height': 'auto',
                    'overflow': 'auto',
                    'padding': '15px 0 8px 15px'
                });
                $('.monitoring__filter').css('display', 'flex');
                $('#next-second').css('display', 'block');
            }
            else if (events.length === 0 && response.pages === 0) {
                $('#next-second, .monitoring__graph-block').css('display', 'none');
                $('.empty_array').fadeIn();
            }
            else if (events.length === 0 && response.pages > 0) {
                $('#next-second').css('display', 'none');
                if (searchParam) {
                    $('.empty_array').css('display', 'none');
                    $('.monitoring_tab-second').empty().append('<p style="padding-left: 20px">URL not found in this domain</p>');
                }
                else {
                    $('.empty_array').fadeIn();
                    $('#next-second').css('display', 'none');
                }
            }
        };
        MonitoringTabTable.prototype.nextPage = function () {
            if (this.page * this.pageSize < this.totalCount) {
                this.page++;
                this.display();
            }
            else {
                $('.empty_array').fadeIn();
                $('#next-second').css('display', 'none');
            }
        };
        MonitoringTabTable.prototype.getSortParam = function () {
            return 'weight';
        };
        MonitoringTabTable.prototype.getPagination = function () {
            return $('#pageTablePagination');
        };
        MonitoringTabTable.prototype.getTable = function () {
            return $('.monitoring_tab-second');
        };
        MonitoringTabTable.prototype.getTBody = function () {
            return this.getTable();
        };
        MonitoringTabTable.prototype.getCrawlingDate = function () {
            return $('#crawling_date');
        };
        MonitoringTabTable.prototype.sort = function (column) {
            if (column === this.sortParam) {
                this.sortBy = this.sortBy === 'asc' ? 'desc' : 'asc';
            }
            else {
                this.sortBy = 'asc';
                this.sortParam = column;
            }
            this.display();
        };
        ;
        MonitoringTabTable.prototype.drawRows = function (events) {
            var _this = this;
            events.forEach(function (event) {
                var row = _this.renderEvent(event);
                _this.addTableRow(row);
            });
            $('#next-second .fa').css('display', 'none');
        };
        MonitoringTabTable.prototype.logFailed = function (log) {
            console.log("error" + log);
        };
        MonitoringTabTable.prototype.display = function () {
            var _this = this;
            this.getPageData().then(function (data) { return _this.processResult(JSON.parse(data)); }, function (error) { return _this.logFailed(error); });
        };
        ;
        MonitoringTabTable.prototype.getRequestData = function () {
            return {
                sortParam: this.sortParam,
                sortBy: this.sortBy,
                pageSize: this.pageSize,
                page: this.page,
                token: this.token,
                interval: this.interval,
                filterDate: this.filterDate,
                searchParamUrl: this.searchParamUrl,
                isIndex: this.isIndex
            };
        };
        MonitoringTabTable.prototype.getPageData = function () {
            var request = this.getRequestData(), data = JSON.stringify(request);
            return this.monitoringRequest(data);
        };
        MonitoringTabTable.prototype.monitoringRequest = function (data) {
            return this.doRequest({
                path: this.requestPath,
                action: 'crawler_request',
                data: data,
                method: 'POST'
            });
        };
        MonitoringTabTable.prototype.renderEvent = function (event) {
            var _this = this;
            var dateFormat = new Date(event.monitoringStatistic.dateCreate * 1000), options = { weekday: "long", year: "numeric", month: "long", day: "numeric", hour: '2-digit', minute: '2-digit' }, date = dateFormat.toLocaleDateString("en-us", options).replace(",", "");
            var id = event.monitoringStatistic.id;
            var pagesAdded = JSON.parse(event.pagesAdded);
            var pageAddedArray = pagesAdded.map(function (page) {
                var pageUrl = page.url;
                return "<div class=\"domain_links\">\n                                            <a href=\"/page-information/?url=" + page.crc32url + "&token=" + _this.token + "\">" + pageUrl + "</a>\n                                            <a class=\"external-link\" href=\"" + pageUrl + "\" target=\"_blank\"><img src=\"/wp-content/themes/sitechecker/out/img/external-link.svg\" alt=\"external-link\" title=\"external-link\"></a>\n                                        </div>";
            }).join('\n');
            var timeLineNewPages = "\n                <div class=\"event-box\">\n                    <div class=\"event-box__header\">\n                        <div class=\"event-box__icon\">\n                            <img src=\"/wp-content/themes/sitechecker/out/img/event_add.svg\" alt=\"added_pages\" title=\"added_pages\">\n                        </div>\n                        <div class=\"event-box__label\">\n                            <span>New pages</span>\n                        </div>\n                        <div class=\"event-box__secondary-section\">\n                            <div class=\"event-box__additional-info\">\n                                <span>" + pagesAdded.length + " pages</span>\n                            </div>\n                        </div>\n                        <div class=\"event-box__dropdown-icon\">\n                            <i class=\"dropdown-icon--greyscale\"></i>\n                        </div>\n                    </div>\n                    <div class=\"event-box__hidden\">\n                        <div class=\"event-box__content\">\n                            " + pageAddedArray + "\n                        </div>\n                    </div>\n                </div>\n            ";
            var pagesDeleted = JSON.parse(event.pagesDeleted);
            var pageDeletedArray = pagesDeleted.map(function (page) {
                var pageUrl = page.url;
                return "<a href=\"" + pageUrl + "\" target=\"_blank\">" + pageUrl + "</a> ";
            }).join('\n');
            var timeLineDeletedPages = "\n                <div class=\"event-box\">\n                    <div class=\"event-box__header\">\n                        <div class=\"event-box__icon\">\n                            <img src=\"/wp-content/themes/sitechecker/out/img/event_delete.svg\" alt=\"deleted_pages\" title=\"deleted_pages\">\n                        </div>\n                        <div class=\"event-box__label\">\n                            <span>Deleted pages</span>\n                        </div>\n                        <div class=\"event-box__secondary-section\">\n                            <div class=\"event-box__additional-info\">\n                                <span>" + pagesDeleted.length + " pages</span>\n                            </div>\n                        </div>\n                        <div class=\"event-box__dropdown-icon\">\n                            <i class=\"dropdown-icon--greyscale\"></i>\n                        </div>\n                    </div>\n                    <div class=\"event-box__hidden\">\n                        <div class=\"event-box__content\">\n                            " + pageDeletedArray + "\n                        </div>\n                    </div>\n                </div>\n            ";
            var pagesChanged = JSON.parse(event.pagesChanged);
            function getPagesByChangeTitle(pagesChanged, changeTitle) {
                return pagesChanged.filter(function (page) {
                    var pageChangeDataItem = page.change_data.find(function (pageChange) { return pageChange.param === changeTitle; });
                    return pageChangeDataItem !== undefined;
                }).map(function (page) {
                    var pageChangeDataItem = page.change_data.find(function (pageChange) { return pageChange.param === changeTitle; });
                    return {
                        url: page.url,
                        crc32url: page.crc32url,
                        previous: pageChangeDataItem.previous,
                        changed: pageChangeDataItem.changed,
                        data: pageChangeDataItem.data
                    };
                });
            }
            var resultBlocks = "";
            if (pagesChanged.length > 0) {
                var changeParams = [
                    {
                        paramTitle: 'title',
                        image: 'event_html',
                        title: 'Title has changed',
                        needsCheckForTitle: false,
                        changedElements: getPagesByChangeTitle(pagesChanged, 'title')
                    },
                    {
                        paramTitle: 'description',
                        image: 'event_html',
                        title: 'Description has changed',
                        needsCheckForTitle: false,
                        changedElements: getPagesByChangeTitle(pagesChanged, 'description')
                    },
                    {
                        paramTitle: 'h1',
                        image: 'event_html',
                        title: 'H1 has changed',
                        needsCheckForTitle: false,
                        changedElements: getPagesByChangeTitle(pagesChanged, 'h1')
                    },
                    {
                        paramTitle: 'http_status_code',
                        image: 'event_server',
                        title: 'HTTP status code has changed',
                        needsCheckForTitle: false,
                        changedElements: getPagesByChangeTitle(pagesChanged, 'http_status_code')
                    },
                    {
                        paramTitle: 'robots_txt_all',
                        image: 'event_server',
                        title: 'Index status has changed',
                        needsCheckForTitle: true,
                        falseTitle: 'URL allowed by robots.txt',
                        trueTitle: 'URL disallowed by robots.txt',
                        changedElements: getPagesByChangeTitle(pagesChanged, 'robots_txt_all')
                    },
                    {
                        paramTitle: 'robots_txt_content',
                        image: 'event_server',
                        title: 'Robots.txt has changed',
                        needsCheckForTitle: true,
                        falseTitle: 'URL disallowed',
                        trueTitle: 'URL allowed',
                        changedElements: getPagesByChangeTitle(pagesChanged, 'robots_txt_content')
                    }, {
                        paramTitle: 'noindex',
                        image: 'event_server',
                        title: 'Index status has changed',
                        needsCheckForTitle: true,
                        falseTitle: 'URL indexation closed by noindex tag',
                        trueTitle: 'URL indexation opened by index tag',
                        changedElements: getPagesByChangeTitle(pagesChanged, 'noindex')
                    }
                ].filter(function (changeParam) {
                    return changeParam.changedElements.length > 0;
                });
                resultBlocks = changeParams.map(function (changeParam) {
                    var changedElements = changeParam.changedElements;
                    var changedArray = changedElements.map(function (page) {
                        var previous = page.changed, changed = page.previous, data = page.data, stuff = [], output = '', array = '', elementSring;
                        $(data).each(function (i, item) {
                            array = item.text.split("\r\n");
                            $(array).each(function (i, element) {
                                elementSring = { string: element, view: item.operation };
                                stuff.push(elementSring);
                            });
                        });
                        $(stuff).each(function (i, item) {
                            var elementString = item.string, elementView = item.view;
                            if (elementView === "DELETE" && elementString !== '') {
                                output = output + '<li class="robots-delete"><span class="change">' + elementString + '</span></li>';
                            }
                            else if (elementView === 'INSERT' && elementString !== '') {
                                output = output + '<li class="robots-added"><span class="change">' + elementString + '</span></li>';
                            }
                            else if (elementString !== '') {
                                output = output + '<li class="robots-current">' + elementString + '</li>';
                            }
                        });
                        if (data) {
                            return "<div class=\"robots__changes\">" + output + "</div>";
                        }
                        else {
                            if (changeParam.needsCheckForTitle === true) {
                                changed = page.changed === false ? changeParam.falseTitle : changeParam.trueTitle;
                                previous = page.previous === false ? changeParam.falseTitle : changeParam.trueTitle;
                            }
                            return "<div class=\"event-box\">\n                                <div class=\"event-box__header\">\n                                    <div class=\"event-box__icon\">\n                                            <img src=\"/wp-content/themes/sitechecker/out/img/event_edit.svg\" alt=\"edited_pages\" title=\"edited_pages\">\n                                        </div>\n                                    <div class=\"event-box__label\">\n                                        <div class=\"domain_links\">\n                                            <a href=\"/page-information/?url=" + page.crc32url + "&token=" + _this.token + "\">" + page.url + "</a>\n                                            <a class=\"external-link\" href=\"" + page.url + "\" target=\"_blank\"><img src=\"/wp-content/themes/sitechecker/out/img/external-link.svg\" alt=\"external-link\" title=\"external-link\"></a>\n                                        </div>\n                                    </div>\n                                </div>\n                                <div class=\"separated-blocks\">\n                                    <div class=\"separated-blocks__block\">\n                                        <p class=\"labeled-value__title\">New data</p>\n                                        <div class=\"labeled-value__row\">\n                                            <p>" + changed + "</p>\n                                        </div>\n                                    </div>\n                                    <div class=\"separated-blocks__block\">\n                                        <p class=\"labeled-value__title\">Previous data</p>\n                                        <div class=\"labeled-value__row\">\n                                            <p>" + previous + "</p>\n                                        </div>\n                                    </div>\n                                </div>\n                            </div>\n                        ";
                        }
                    });
                    var changeArrayHTML = changedArray.join('\n');
                    return "\n                    <div class=\"event-box\">\n                        <div class=\"event-box__header\">\n                            <div class=\"event-box__icon\">\n                                <img src=\"/wp-content/themes/sitechecker/out/img/" + changeParam.image + ".svg\" alt=\"deleted_pages\" title=\"deleted_pages\">\n                            </div>\n                            <div class=\"event-box__label\">\n                                <span>" + changeParam.title + "</span>\n                            </div>\n                            <div class=\"event-box__secondary-section\">\n                                <div class=\"event-box__additional-info\">\n                                    <span>" + changedArray.length + " pages</span>\n                                </div>\n                            </div>\n                            <div class=\"event-box__dropdown-icon\">\n                                <i class=\"dropdown-icon--greyscale\"></i>\n                            </div>\n                        </div>\n                        <div class=\"event-box__hidden\">\n                            <div class=\"event-box__content\">\n                                " + changeArrayHTML + "\n                            </div>\n                        </div>\n                    </div>\n                ";
                }).join('\n');
            }
            var timeLineHTML, newPages, deletedPages;
            if (pageAddedArray.length !== 0) {
                newPages = "" + timeLineNewPages;
            }
            else {
                newPages = "";
            }
            if (pageDeletedArray.length !== 0) {
                deletedPages = "" + timeLineDeletedPages;
            }
            else {
                deletedPages = "";
            }
            if (pageAddedArray.length == 0 && pageDeletedArray.length == 0 && pagesChanged.length == 0) {
                timeLineHTML = '';
            }
            else {
                timeLineHTML = '<div class="timeline-entry" id="' + id + '">' +
                    '<div class="timeline-entry__title"><span>' + date + '</span></div>' +
                    '<div class="timeline-entry__data">' +
                    '<div class="newPages">' + newPages + '</div>' +
                    '<div class="deletedPages">' + deletedPages + '</div>' +
                    '<div class="robots">' + resultBlocks + '</div>' +
                    '</div></div>';
            }
            return timeLineHTML;
        };
        return MonitoringTabTable;
    }(Crawling.BaseTable));
    Crawling.MonitoringTabTable = MonitoringTabTable;
})(Crawling || (Crawling = {}));
/// <reference path="CrawlingBaseTable.ts"/>
/// <reference path="../../../../../../typings/globals/jquery/index.d.ts" />
var Crawling;
(function (Crawling) {
    var MonitoringTabTableDesign = /** @class */ (function (_super) {
        __extends(MonitoringTabTableDesign, _super);
        function MonitoringTabTableDesign(apiPath, requestPath, token, interval, filterDate, searchParamUrl, isIndex, dateFrom, dateTill) {
            var _this = _super.call(this, apiPath) || this;
            _this.requestPath = requestPath;
            _this.sortParam = 'date_create';
            _this.pageSize = 10;
            _this.token = token;
            _this.interval = interval;
            _this.filterDate = filterDate;
            _this.searchParamUrl = searchParamUrl;
            _this.isIndex = isIndex;
            _this.dateFrom = dateFrom;
            _this.dateTill = dateTill;
            return _this;
        }
        MonitoringTabTableDesign.prototype.processResult = function (response) {
            this.totalCount = response.totalCount;
            this.filteredCount = response.filteredCount;
            this.totalPages = response.pages;
            var events = response.summary;
            var searchParam = false;
            if (this.searchParamUrl !== undefined && this.searchParamUrl !== '' || this.isIndex === true) {
                searchParam = true;
            }
            if (events.length > 0) {
                $('.history__block').removeClass('empty');
                $('.monitoring__graph').css('display', 'block');
                if (this.filterDate !== undefined && this.filterDate !== '') {
                    $('.load__more-filter').removeClass('active').fadeIn();
                }
                else {
                    $('.load__more').removeClass('active').fadeIn();
                }
                this.drawRows(events);
            }
            else if (events.length === 0 && response.pages === 0) {
                $('.load__more, .load__more-filter, .monitoring-loader').css('display', 'none');
                $('.monitoring__history-point.started').fadeIn();
            }
            else if (events.length === 0 && response.pages > 0) {
                if (searchParam) {
                    $('.monitoring__history-point.started, .load__more, .monitoring-loader').css('display', 'none');
                    $('.history__block').empty().addClass('empty').append('<p class="not__found">URL not found in this domain</p>');
                }
                else {
                    $('.monitoring__history-point.started').fadeIn();
                    $('.load__more, .load__more-filter, .monitoring-loader').css('display', 'none');
                }
            }
        };
        MonitoringTabTableDesign.prototype.nextPage = function () {
            if (this.page * this.pageSize < this.totalCount) {
                this.page++;
                this.display();
            }
            else {
                $('.monitoring__history-point.started').fadeIn();
                $('.load__more, .load__more-filter').css('display', 'none');
            }
        };
        MonitoringTabTableDesign.prototype.getSortParam = function () {
            return 'weight';
        };
        MonitoringTabTableDesign.prototype.getPagination = function () {
            return $('#pageTablePagination');
        };
        MonitoringTabTableDesign.prototype.getTable = function () {
            return $('.history__block');
        };
        MonitoringTabTableDesign.prototype.getTBody = function () {
            return this.getTable();
        };
        MonitoringTabTableDesign.prototype.getCrawlingDate = function () {
            return $('#crawling_date');
        };
        MonitoringTabTableDesign.prototype.sort = function (column) {
            if (column === this.sortParam) {
                this.sortBy = this.sortBy === 'asc' ? 'desc' : 'asc';
            }
            else {
                this.sortBy = 'asc';
                this.sortParam = column;
            }
            this.display();
        };
        ;
        MonitoringTabTableDesign.prototype.drawRows = function (events) {
            var _this = this;
            events.forEach(function (event) {
                var row = _this.renderEvent(event);
                _this.addTableRow(row);
            });
            $('#next-second .fa').css('display', 'none');
        };
        MonitoringTabTableDesign.prototype.logFailed = function (log) {
            console.log("error" + log);
        };
        MonitoringTabTableDesign.prototype.display = function () {
            var _this = this;
            this.getPageData().then(function (data) { return _this.processResult(JSON.parse(data)); }, function (error) { return _this.logFailed(error); });
        };
        ;
        MonitoringTabTableDesign.prototype.getRequestData = function () {
            return {
                sortParam: this.sortParam,
                sortBy: this.sortBy,
                pageSize: this.pageSize,
                page: this.page,
                token: this.token,
                interval: this.interval,
                filterDate: this.filterDate,
                searchParamUrl: this.searchParamUrl,
                isIndex: this.isIndex,
                dateFrom: this.dateFrom,
                dateTill: this.dateTill
            };
        };
        MonitoringTabTableDesign.prototype.getPageData = function () {
            var request = this.getRequestData(), data = JSON.stringify(request);
            return this.monitoringRequest(data);
        };
        MonitoringTabTableDesign.prototype.monitoringRequest = function (data) {
            return this.doRequest({
                path: this.requestPath,
                action: 'crawler_request',
                data: data,
                method: 'POST'
            });
        };
        MonitoringTabTableDesign.prototype.renderEvent = function (event) {
            var _this = this;
            var weekday = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"], monthNames = ["January", "February", "March", "April", "May", "June",
                "July", "August", "September", "October", "November", "December"
            ];
            function formatAMPM(date) {
                var hours = date.getHours(), minutes = date.getMinutes(), ampm = hours >= 12 ? 'pm' : 'am';
                hours = hours % 12;
                hours = hours ? hours : 12;
                minutes = minutes < 10 ? '0' + minutes : minutes;
                return hours + ':' + minutes + ' ' + ampm;
            }
            var dateFormat = new Date(event.monitoringStatistic.dateCreate * 1000), date = formatAMPM(dateFormat), dateDay = weekday[dateFormat.getDay()] + ' ' + monthNames[dateFormat.getMonth()] + ' ' + dateFormat.getDate() + ', ' + dateFormat.getFullYear();
            var id = event.monitoringStatistic.id;
            var pagesAdded = JSON.parse(event.pagesAdded);
            var pageAddedArray = pagesAdded.map(function (page) {
                var pageUrl = page.url;
                return "<li>\n                            <div class=\"external-link\"><a href=\"/page-information/?url=" + page.crc32url + "&token=" + _this.token + "\">" + page.url + "</a><a href=\"" + page.url + "\" target=\"_blank\"><img src=\"/wp-content/themes/sitechecker/out/img_design/external.svg\" alt=\"external\" title=\"external\"></a></div>\n                        </li>";
            }).join('\n');
            var timeLineNewPages = "\n                <div class=\"header\">\n                    <img src=\"/wp-content/themes/sitechecker/out/img_design/monitoring_add.svg\" alt=\"\">" + pagesAdded.length + " new pages\n                    <p>" + date + "</p>\n                    <svg width=\"9\" height=\"5\" viewBox=\"0 0 9 5\" fill=\"none\" xmlns=\"http://www.w3.org/2000/svg\">\n<path fill-rule=\"evenodd\" clip-rule=\"evenodd\" d=\"M4.499 0L9 2.5V4.896L4.499 2.38L0 5V2.5L4.499 0Z\" transform=\"translate(9 5) rotate(-180)\" fill=\"#cccccc\"/></svg></div>\n                <div class=\"body\">\n                    <div class=\"container\">\n                        <ul class=\"added\">\n                            " + pageAddedArray + "\n                        </ul>\n                    </div>\n                </div>\n            ";
            var pagesDeleted = JSON.parse(event.pagesDeleted);
            var pageDeletedArray = pagesDeleted.map(function (page) {
                var pageUrl = page.url;
                return "<li><span>" + pageUrl + "</span></li> ";
            }).join('\n');
            var timeLineDeletedPages = "\n                <div class=\"header\">\n                    <img src=\"/wp-content/themes/sitechecker/out/img_design/monitoring_delete.svg\" alt=\"\">" + pagesDeleted.length + " deleted pages\n                    <p>" + date + "</p>\n                    <svg width=\"9\" height=\"5\" viewBox=\"0 0 9 5\" fill=\"none\" xmlns=\"http://www.w3.org/2000/svg\">\n<path fill-rule=\"evenodd\" clip-rule=\"evenodd\" d=\"M4.499 0L9 2.5V4.896L4.499 2.38L0 5V2.5L4.499 0Z\" transform=\"translate(9 5) rotate(-180)\" fill=\"#cccccc\"/></svg></div>\n                <div class=\"body\">\n                    <div class=\"container\">\n                        <ul class=\"deleted\">\n                            " + pageDeletedArray + "\n                        </ul>\n                    </div>\n                </div>\n            ";
            var pagesChanged = JSON.parse(event.pagesChanged);
            function getPagesByChangeTitle(pagesChanged, changeTitle) {
                return pagesChanged.filter(function (page) {
                    var pageChangeDataItem = page.change_data.find(function (pageChange) { return pageChange.param === changeTitle; });
                    return pageChangeDataItem !== undefined;
                }).map(function (page) {
                    var pageChangeDataItem = page.change_data.find(function (pageChange) { return pageChange.param === changeTitle; });
                    return {
                        url: page.url,
                        crc32url: page.crc32url,
                        previous: pageChangeDataItem.previous,
                        changed: pageChangeDataItem.changed,
                        data: pageChangeDataItem.data
                    };
                });
            }
            var resultBlocks = "";
            if (pagesChanged.length > 0) {
                var changeParams = [
                    {
                        paramTitle: 'title',
                        image: 'title',
                        title: 'Title has changed',
                        needsCheckForTitle: false,
                        changedElements: getPagesByChangeTitle(pagesChanged, 'title')
                    },
                    {
                        paramTitle: 'description',
                        image: 'title',
                        title: 'Description has changed',
                        needsCheckForTitle: false,
                        changedElements: getPagesByChangeTitle(pagesChanged, 'description')
                    },
                    {
                        paramTitle: 'h1',
                        image: 'title',
                        title: 'H1 has changed',
                        needsCheckForTitle: false,
                        changedElements: getPagesByChangeTitle(pagesChanged, 'h1')
                    },
                    {
                        paramTitle: 'http_status_code',
                        image: 'status',
                        title: 'HTTP status code has changed',
                        needsCheckForTitle: false,
                        changedElements: getPagesByChangeTitle(pagesChanged, 'http_status_code')
                    },
                    {
                        paramTitle: 'robots_txt_all',
                        image: 'status',
                        title: 'Index status has changed',
                        needsCheckForTitle: true,
                        falseTitle: '<span class="allow">URL allowed by robots.txt</span>',
                        trueTitle: '<span class="disallow">URL disallowed by robots.txt</span>',
                        changedElements: getPagesByChangeTitle(pagesChanged, 'robots_txt_all')
                    },
                    {
                        paramTitle: 'robots_txt_content',
                        image: 'robots',
                        title: 'Robots.txt has changed',
                        needsCheckForTitle: true,
                        falseTitle: '<span class="disallow">URL disallowed</span>',
                        trueTitle: '<span class="allow">URL allowed</span>',
                        changedElements: getPagesByChangeTitle(pagesChanged, 'robots_txt_content')
                    }, {
                        paramTitle: 'noindex',
                        image: 'index',
                        title: 'Index status has changed',
                        needsCheckForTitle: true,
                        falseTitle: '<span class="disallow">URL indexation closed by noindex tag</span>',
                        trueTitle: '<span class="allow">URL indexation opened by index tag</span>',
                        changedElements: getPagesByChangeTitle(pagesChanged, 'noindex')
                    }
                ].filter(function (changeParam) {
                    return changeParam.changedElements.length > 0;
                });
                resultBlocks = changeParams.map(function (changeParam) {
                    var changedElements = changeParam.changedElements;
                    var changedArray = changedElements.map(function (page) {
                        var previous = page.changed, changed = page.previous, data = page.data, stuff = [], output = '', array = '', elementSring;
                        $(data).each(function (i, item) {
                            array = item.text.split("\r\n");
                            $(array).each(function (i, element) {
                                elementSring = { string: element, view: item.operation };
                                stuff.push(elementSring);
                            });
                        });
                        $(stuff).each(function (i, item) {
                            var elementString = item.string, elementView = item.view;
                            if (elementView === "DELETE" && elementString !== '') {
                                output = output + '<li class="robots-delete"><span class="change">' + elementString + '</span></li>';
                            }
                            else if (elementView === 'INSERT' && elementString !== '') {
                                output = output + '<li class="robots-added"><span class="change">' + elementString + '</span></li>';
                            }
                            else if (elementString !== '') {
                                output = output + '<li class="robots-current">' + elementString + '</li>';
                            }
                        });
                        if (data) {
                            return "<div class=\"body\">\n                                <div class=\"container\"><div class=\"robots__changes\">" + output + "</div></div></div>";
                        }
                        else {
                            if (changeParam.needsCheckForTitle === true) {
                                changed = page.changed === false ? changeParam.falseTitle : changeParam.trueTitle;
                                previous = page.previous === false ? changeParam.falseTitle : changeParam.trueTitle;
                            }
                            if (changed === '') {
                                changed = '<p class="message__error">No ' + changeParam.paramTitle + '</p>';
                            }
                            if (previous === '') {
                                previous = '<p class="message__error">No ' + changeParam.paramTitle + '</p>';
                            }
                            return "\n                            <div class=\"body\">\n                                <div class=\"container\">\n                                    <div class=\"url\">URL: <div class=\"external-link\"><a href=\"/page-information/?url=" + page.crc32url + "&token=" + _this.token + "\">" + page.url + "</a><a href=\"" + page.url + "\" target=\"_blank\"><img src=\"/wp-content/themes/sitechecker/out/img_design/external.svg\" alt=\"external\" title=\"external\"></a></div></div>\n                                    <div class=\"changes\">\n                                        <p class=\"" + changeParam.paramTitle + "\">" + previous + "</p>\n                                        <img src=\"/wp-content/themes/sitechecker/out/img_design/arrow_monitoring.svg\" alt=\"arrow\" title=\"arrow\">\n                                        <p class=\"" + changeParam.paramTitle + "\">" + changed + "</p>\n                                    </div>\n                                </div>\n                            </div>\n                        ";
                        }
                    });
                    var changeArrayHTML = changedArray.join('\n');
                    return "\n                    <div class=\"monitoring__history-event\">\n                        <div class=\"header\"><img src=\"/wp-content/themes/sitechecker/out/img_design/monitoring_" + changeParam.image + ".svg\" alt=\"" + changeParam.image + "\" title=\"" + changeParam.title + "\">" + changeParam.title + "\n                        <p>" + date + "</p>\n                        <svg width=\"9\" height=\"5\" viewBox=\"0 0 9 5\" fill=\"none\" xmlns=\"http://www.w3.org/2000/svg\"><path fill-rule=\"evenodd\" clip-rule=\"evenodd\" d=\"M4.499 0L9 2.5V4.896L4.499 2.38L0 5V2.5L4.499 0Z\" transform=\"translate(9 5) rotate(-180)\" fill=\"#cccccc\"/></svg></div>\n                        " + changeArrayHTML + "\n                    </div>\n                ";
                }).join('\n');
            }
            var timeLineHTML, newPages, deletedPages;
            if (pageAddedArray.length !== 0) {
                newPages = "<div class=\"monitoring__history-event added\">" + timeLineNewPages + "</div>";
            }
            else {
                newPages = "";
            }
            if (pageDeletedArray.length !== 0) {
                deletedPages = "<div class=\"monitoring__history-event deleted\">" + timeLineDeletedPages + "</div>";
            }
            else {
                deletedPages = "";
            }
            if (pageAddedArray.length == 0 && pageDeletedArray.length == 0 && pagesChanged.length == 0) {
                timeLineHTML = '';
            }
            else {
                var historyPoint = $('.monitoring__history-point[name="' + dateDay + '"]');
                if (historyPoint.length > 0) {
                    historyPoint.find('.content').append(deletedPages + newPages + resultBlocks);
                }
                else {
                    timeLineHTML = '<div class="monitoring__history-point" name="' + dateDay + '">' +
                        '<p class="history-point-date">' + dateDay + '</p>' +
                        '<div class="content">' + deletedPages + newPages + resultBlocks + '</div></div>';
                }
            }
            $('.monitoring-loader').css('display', 'none');
            $('.monitoring__history-content').fadeIn();
            return timeLineHTML;
        };
        return MonitoringTabTableDesign;
    }(Crawling.BaseTable));
    Crawling.MonitoringTabTableDesign = MonitoringTabTableDesign;
})(Crawling || (Crawling = {}));
/// <reference path="../CrawlingBaseTable.ts"/>
var Crawling;
(function (Crawling) {
    var Popup;
    (function (Popup) {
        var PopupTable = /** @class */ (function (_super) {
            __extends(PopupTable, _super);
            function PopupTable(apiPath, requestPath, id, token) {
                var _this = _super.call(this, apiPath, requestPath, token) || this;
                _this.id = id;
                $('#popup_search').val('');
                _this.getPopup().show();
                _this.init();
                return _this;
            }
            PopupTable.prototype.getPopup = function () {
                if (!$('main').hasClass('domainPage')) {
                    $('body').css('overflow', 'hidden');
                }
                return $('#popup');
            };
            PopupTable.prototype.sort = function (column) { };
            PopupTable.prototype.getPagination = function () {
                return $('#popup_pagination');
            };
            PopupTable.prototype.getTableElements = function () {
                return $('.popup_table_elements');
            };
            PopupTable.prototype.getTable = function () {
                return $('#popup_table');
            };
            PopupTable.prototype.getTBody = function () {
                return this.getTable().find("tbody");
            };
            PopupTable.prototype.getCrawlingDate = function () {
                return $('#popupCrawlingDate');
            };
            PopupTable.prototype.setPagination = function () {
                var html = "\n                <div class=\"dataTables_info\" id=\"pageTable_info\" role=\"status\" aria-live=\"polite\">\n                    Showing " + ((this.page * this.pageSize) - this.pageSize) + " to " + (this.page * this.pageSize > this.totalCount ? this.totalCount : this.page * this.pageSize) + "  of " + this.totalCount + " entries\n                </div>\n                <div class=\"dataTables_paginate paging_simple_numbers\" id=\"pageTable_paginate\">\n                    <ul class=\"pagination\">\n                        <li class=\"page-item disabled\">\n                            <span>Page " + this.page + " from " + this.totalPages + "</span>\n                        </li>\n                        <li class=\"paginate_button page-item previous " + (this.page > 1 ? '' : 'disabled') + "\">\n                            <a href=\"javascript:void(0);\" aria-controls=\"pageTable\" class=\"page-link\" " + (this.page > 1 ? "onclick=\"popupTable.previousPage();\"" : '') + "  >Previous</a>\n                        </li>\n                        <li class=\"paginate_button page-item " + (this.page < this.totalPages ? '' : 'disabled') + "\">\n                            <a href=\"javascript:void(0);\" aria-controls=\"pageTable\"  class=\"page-link\" " + (this.page < this.totalPages ? "onclick=\"popupTable.nextPage()\"" : '') + " >Next</a>\n                        </li>\n                    </ul>\n                </div>\n            ";
                this.getPagination().html(html);
            };
            ;
            PopupTable.prototype.getPopupRowDataHTML = function (data, index) {
                var content = Object.keys(data).map(function (key) {
                    var value = data[key];
                    return "\n                <div class=\"col-xs-12 col-sm-6 col-lg-4\">\n                    <div class=\"detail_title\">" + key + ": " + value + "</div>\n                </div>\n            ";
                }).reduce(function (previousValue, currentValue) {
                    return previousValue + currentValue;
                });
                return "\n                <tr id=\"popup_row_data_" + index + "\" style=\"display: none\">\n                    <td colspan=\"6\">\n                       <div class=\"popup_row_data\">\n                            " + content + "\n                        </div>\n                    </td>\n                </tr>\n            ";
            };
            PopupTable.prototype.getRequestData = function () {
                return this.addFilters({
                    id: this.id,
                    sortParam: this.sortParam,
                    sortBy: this.sortBy,
                    pageSize: this.pageSize,
                    page: this.page,
                    token: this.token
                });
            };
            PopupTable.prototype.close = function () {
                this.getPopup().hide();
            };
            PopupTable.prototype.toggleRowData = function (index) {
                var row = $('#popup_row_' + index);
                var rowIcon = row.find('.icon');
                var rowData = $('#popup_row_data_' + index);
                if (row.hasClass("dataShow")) {
                    row.removeClass('dataShow');
                    rowIcon.removeClass("fa-minus");
                    rowIcon.addClass("fa-plus");
                    rowData.hide();
                }
                else {
                    row.addClass("dataShow");
                    rowIcon.removeClass("fa-plus");
                    rowIcon.addClass("fa-minus");
                    rowData.show();
                }
            };
            PopupTable.prototype.parseCrawledDate = function (_a) {
                var timestamp = _a.checked;
                return new Date(timestamp * 1000);
            };
            PopupTable.prototype.addFilters = function (request) {
                request.searchParamUrl = $('#popup_search').val();
                console.log(request);
                return request;
            };
            return PopupTable;
        }(Crawling.CrawlingBaseTable));
        Popup.PopupTable = PopupTable;
    })(Popup = Crawling.Popup || (Crawling.Popup = {}));
})(Crawling || (Crawling = {}));
/// <reference path="PopupTable.ts"/>
var Crawling;
(function (Crawling) {
    var Popup;
    (function (Popup) {
        var ExternalPopupTable = /** @class */ (function (_super) {
            __extends(ExternalPopupTable, _super);
            function ExternalPopupTable(apiPath, url, token) {
                var _this = _super.call(this, apiPath, '/getExternalLinks', url, token) || this;
                $('.popup__export').click(function () {
                    exportLinks('external', url, token);
                });
                $('#popup_table tbody').empty();
                return _this;
            }
            ExternalPopupTable.prototype.getSortParam = function () {
                return 'url';
            };
            ExternalPopupTable.prototype.drawRows = function (rows) {
                var _this = this;
                $('.crawler_popup-title').text('External links');
                $('#overview__popup').removeClass('duplicate__popup').fadeIn().css('display', 'flex');
                rows.forEach(function (row, index) {
                    var rowData = _this.getPopupRowDataHTML(row, index);
                    var tr = "\n                    <tr id=\"popup_row_" + index + "\" >\n                        <td>\n                            <div class=\"external-link\">\n                            <a href=\"/page-information/?url=" + row.url + "&token=" + _this.token + "\">" + row.url + "</a>\n                            <a href=\"" + row.url + "\" target=\"_blank\"><img src=\"/wp-content/themes/sitechecker/out/img_design/external.svg\" alt=\"external\" title=\"external\"></a>\n                            </div>\n                        </td>\n                        <td>" + (row.link_anchor || '-') + "</td>\n                    </tr>\n                    " + rowData + "\n                ";
                    _this.addTableRow(tr);
                });
                $('.additional_param').hide(); //если ссылки внутренние дополнительные колонки не нужны
            };
            return ExternalPopupTable;
        }(Popup.PopupTable));
        Popup.ExternalPopupTable = ExternalPopupTable;
    })(Popup = Crawling.Popup || (Crawling.Popup = {}));
})(Crawling || (Crawling = {}));
/// <reference path="../popup/ExternalPopupTable.ts"/>
var Crawling;
(function (Crawling) {
    var Popup;
    (function (Popup) {
        var ExternalPopupTable = Crawling.Popup.ExternalPopupTable;
        var ExternalTable = /** @class */ (function (_super) {
            __extends(ExternalTable, _super);
            function ExternalTable(apiPath, url, token) {
                return _super.call(this, apiPath, url, token) || this;
            }
            ExternalTable.prototype.setPagination = function () {
                var html = "\n                <div>\n                    <div class=\"dataTables_info\" id=\"pageTable_info\" role=\"status\" aria-live=\"polite\">\n                        Showing " + ((this.page * this.pageSize) - this.pageSize) + " to " + (this.page * this.pageSize > this.totalCount ? this.totalCount : this.page * this.pageSize) + "  of " + this.totalCount + " entries\n                    </div>\n                </div>\n                <div>\n                    <div class=\"dataTables_paginate paging_simple_numbers\" id=\"pageTable_paginate\">\n                        <ul class=\"pagination\">\n                            <li class=\"page-item disabled\">\n                                <span>Page " + this.page + " from " + this.totalPages + "</span>\n                            </li>\n                            <li class=\"paginate_button page-item previous " + (this.page > 1 ? '' : 'disabled') + "\">\n                                <a href=\"javascript:void(0);\" aria-controls=\"pageTable\" class=\"page-link\" " + (this.page > 1 ? "onclick=\"popoupTable.previousPage();\"" : '') + "  >Previous</a>\n                            </li>\n                            <li class=\"paginate_button page-item " + (this.page < this.totalPages ? '' : 'disabled') + "\">\n                                <a href=\"javascript:void(0);\" aria-controls=\"pageTable\"  class=\"page-link\" " + (this.page < this.totalPages ? "onclick=\"popoupTable.nextPage()\"" : '') + " >Next</a>\n                            </li>\n                        </ul>\n                    </div>\n                </div>\n            ";
                this.getPagination().html(html);
            };
            return ExternalTable;
        }(ExternalPopupTable));
        Popup.ExternalTable = ExternalTable;
    })(Popup = Crawling.Popup || (Crawling.Popup = {}));
})(Crawling || (Crawling = {}));
/// <reference path="PopupTable.ts"/>
var Crawling;
(function (Crawling) {
    var Popup;
    (function (Popup) {
        var IncomingPopupTable = /** @class */ (function (_super) {
            __extends(IncomingPopupTable, _super);
            function IncomingPopupTable(apiPath, url, token) {
                var _this = _super.call(this, apiPath, '/getIncomingLinks', url, token) || this;
                $('.popup__export').click(function () {
                    exportLinks('incoming', url, token);
                });
                $('#popup_table tbody').empty();
                return _this;
            }
            IncomingPopupTable.prototype.getSortParam = function () {
                return 'weight';
            };
            IncomingPopupTable.prototype.drawRows = function (rows) {
                var _this = this;
                $('.crawler_popup-title').text('Anchors');
                $('#overview__popup').removeClass('duplicate__popup').fadeIn().css('display', 'flex');
                rows.forEach(function (row, index) {
                    var rowData = _this.getPopupRowDataHTML(row, index);
                    var tr = "\n                    <tr id=\"popup_row_" + index + "\" >\n                        <td>\n                            <div class=\"external-link\">\n                            <a href=\"/page-information/?url=" + row.url + "&token=" + _this.token + "\">" + row.url + "</a>\n                            <a href=\"" + row.url + "\" target=\"_blank\"><img src=\"/wp-content/themes/sitechecker/out/img_design/external.svg\" alt=\"external\" title=\"external\"></a>\n                            </div>\n                        </td>\n                        <td>" + (row.link_anchor || '-') + "</td>\n                        <td>" + parseInt(row.normalized_weight) + "</td>\n                        <td>\n                            <span style=\"color: " + _this.getStatusCodeIcon(parseInt(row.http_status_code)) + "\">" + row.http_status_code + "</span>\n                        </td>\n                    </tr>\n                    " + rowData + "\n                ";
                    $('.additional_param').show();
                    _this.addTableRow(tr);
                });
            };
            return IncomingPopupTable;
        }(Popup.PopupTable));
        Popup.IncomingPopupTable = IncomingPopupTable;
    })(Popup = Crawling.Popup || (Crawling.Popup = {}));
})(Crawling || (Crawling = {}));
/// <reference path="../popup/IncomingPopupTable.ts"/>
var Crawling;
(function (Crawling) {
    var Popup;
    (function (Popup) {
        var IncomingPopupTable = Crawling.Popup.IncomingPopupTable;
        var IncomingTable = /** @class */ (function (_super) {
            __extends(IncomingTable, _super);
            function IncomingTable(apiPath, url, token) {
                return _super.call(this, apiPath, url, token) || this;
            }
            IncomingTable.prototype.setPagination = function () {
                var html = "\n                <div>\n                    <div class=\"dataTables_info\" id=\"pageTable_info\" role=\"status\" aria-live=\"polite\">\n                        Showing " + ((this.page * this.pageSize) - this.pageSize) + " to " + (this.page * this.pageSize > this.totalCount ? this.totalCount : this.page * this.pageSize) + "  of " + this.totalCount + " entries\n                    </div>\n                </div>\n                <div>\n                    <div class=\"dataTables_paginate paging_simple_numbers\" id=\"pageTable_paginate\">\n                        <ul class=\"pagination\">\n                            <li class=\"page-item disabled\">\n                                <span>Page " + this.page + " from " + this.totalPages + "</span>\n                            </li>\n                            <li class=\"paginate_button page-item previous " + (this.page > 1 ? '' : 'disabled') + "\">\n                                <a href=\"javascript:void(0);\" aria-controls=\"pageTable\" class=\"page-link\" " + (this.page > 1 ? "onclick=\"popoupTable.previousPage();\"" : '') + "  >Previous</a>\n                            </li>\n                            <li class=\"paginate_button page-item " + (this.page < this.totalPages ? '' : 'disabled') + "\">\n                                <a href=\"javascript:void(0);\" aria-controls=\"pageTable\"  class=\"page-link\" " + (this.page < this.totalPages ? "onclick=\"popoupTable.nextPage()\"" : '') + " >Next</a>\n                            </li>\n                        </ul>\n                    </div>\n                </div>\n            ";
                this.getPagination().html(html);
            };
            return IncomingTable;
        }(IncomingPopupTable));
        Popup.IncomingTable = IncomingTable;
    })(Popup = Crawling.Popup || (Crawling.Popup = {}));
})(Crawling || (Crawling = {}));
/// <reference path="PopupTable.ts"/>
var Crawling;
(function (Crawling) {
    var Popup;
    (function (Popup) {
        var InternalPopupTable = /** @class */ (function (_super) {
            __extends(InternalPopupTable, _super);
            function InternalPopupTable(apiPath, url, token) {
                var _this = _super.call(this, apiPath, '/getInternalLinks', url, token) || this;
                $('.popup__export').click(function () {
                    exportLinks('internal', url, token);
                });
                $('#popup_table tbody').empty();
                return _this;
            }
            InternalPopupTable.prototype.getSortParam = function () {
                return 'weight';
            };
            InternalPopupTable.prototype.getPath = function () {
                return '/getInternalLinks';
            };
            InternalPopupTable.prototype.drawRows = function (rows) {
                var _this = this;
                $('.crawler_popup-title').text('Internal links');
                $('#overview__popup').removeClass('duplicate__popup').fadeIn().css('display', 'flex');
                rows.forEach(function (row, index) {
                    var rowData = _this.getPopupRowDataHTML(row, index);
                    var tr = "\n                    <tr id=\"popup_row_" + index + "\" >\n                        <td>\n                            <div class=\"external-link\">\n                            <a href=\"/page-information/?url=" + row.url + "&token=" + _this.token + "\">" + row.url + "</a>\n                            <a href=\"" + row.url + "\" target=\"_blank\"><img src=\"/wp-content/themes/sitechecker/out/img_design/external.svg\" alt=\"external\" title=\"external\"></a>\n                            </div>\n                        </td>\n                        <td>" + (row.link_anchor || '-') + "</td>\n                        <td>" + parseInt(row.normalized_weight) + "</td>\n                        <td>\n                            <span style=\"color: " + _this.getStatusCodeIcon(parseInt(row.http_status_code)) + "\">" + row.http_status_code + "</span>\n                        </td>\n                    </tr>\n                    " + rowData + "\n                ";
                    $('.additional_param').show();
                    _this.addTableRow(tr);
                });
            };
            return InternalPopupTable;
        }(Popup.PopupTable));
        Popup.InternalPopupTable = InternalPopupTable;
    })(Popup = Crawling.Popup || (Crawling.Popup = {}));
})(Crawling || (Crawling = {}));
/// <reference path="../popup/InternalPopupTable.ts"/>
var Crawling;
(function (Crawling) {
    var Popup;
    (function (Popup) {
        var InternalPopupTable = Crawling.Popup.InternalPopupTable;
        var InternalTable = /** @class */ (function (_super) {
            __extends(InternalTable, _super);
            function InternalTable(apiPath, url, token) {
                return _super.call(this, apiPath, url, token) || this;
            }
            InternalTable.prototype.setPagination = function () {
                var html = "\n                <div>\n                    <div class=\"dataTables_info\" id=\"pageTable_info\" role=\"status\" aria-live=\"polite\">\n                        Showing " + ((this.page * this.pageSize) - this.pageSize) + " to " + (this.page * this.pageSize > this.totalCount ? this.totalCount : this.page * this.pageSize) + "  of " + this.totalCount + " entries\n                    </div>\n                </div>\n                <div>\n                    <div class=\"dataTables_paginate paging_simple_numbers\" id=\"pageTable_paginate\">\n                        <ul class=\"pagination\">\n                            <li class=\"page-item disabled\">\n                                <span>Page " + this.page + " from " + this.totalPages + "</span>\n                            </li>\n                            <li class=\"paginate_button page-item previous " + (this.page > 1 ? '' : 'disabled') + "\">\n                                <a href=\"javascript:void(0);\" aria-controls=\"pageTable\" class=\"page-link\" " + (this.page > 1 ? "onclick=\"popoupTable.previousPage();\"" : '') + "  >Previous</a>\n                            </li>\n                            <li class=\"paginate_button page-item " + (this.page < this.totalPages ? '' : 'disabled') + "\">\n                                <a href=\"javascript:void(0);\" aria-controls=\"pageTable\"  class=\"page-link\" " + (this.page < this.totalPages ? "onclick=\"popoupTable.nextPage()\"" : '') + " >Next</a>\n                            </li>\n                        </ul>\n                    </div>\n                </div>\n            ";
                this.getPagination().html(html);
            };
            return InternalTable;
        }(InternalPopupTable));
        Popup.InternalTable = InternalTable;
    })(Popup = Crawling.Popup || (Crawling.Popup = {}));
})(Crawling || (Crawling = {}));
/// <reference path="PopupTable.ts"/>
var Crawling;
(function (Crawling) {
    var Popup;
    (function (Popup) {
        var DuplicatedLinks = /** @class */ (function (_super) {
            __extends(DuplicatedLinks, _super);
            function DuplicatedLinks(apiPath, url, token, paramForSearch) {
                var _this = _super.call(this, apiPath, '/getDuplicatedLinks', url, token) || this;
                $('#popup_table tbody').empty();
                _this.paramForSearch = paramForSearch;
                _this.display();
                return _this;
            }
            DuplicatedLinks.prototype.init = function () { };
            DuplicatedLinks.prototype.getSortParam = function () {
                return 'url';
            };
            DuplicatedLinks.prototype.getRequestData = function () {
                var requestData = _super.prototype.getRequestData.call(this);
                requestData.param = this.paramForSearch;
                return requestData;
            };
            DuplicatedLinks.prototype.drawRows = function (rows) {
                var _this = this;
                $('#overview__popup').addClass('duplicate__popup').fadeIn().css('display', 'flex');
                var param = 'Title', val = 'title';
                if (this.paramForSearch === 'h1') {
                    param = 'H1';
                    val = 'h1';
                }
                else if (this.paramForSearch === 'description') {
                    param = 'Description';
                    val = 'description';
                }
                $('.crawler_popup-title').html(param + ': <span>' + rows[0][val] + '</span>');
                rows.forEach(function (row, index) {
                    var rowData = _this.getPopupRowDataHTML(row, index);
                    var tr = "\n                    <tr id=\"popup_row_" + index + "\" >\n                        <td>\n                            <div class=\"external-link\">\n                            <a href=\"/page-information/?url=" + row.url + "&token=" + _this.token + "\">" + row.url + "</a>\n                            <a href=\"" + row.url + "\" target=\"_blank\"><img src=\"/wp-content/themes/sitechecker/out/img_design/external.svg\" alt=\"external\" title=\"external\"></a>\n                            </div>\n                        </td>\n                    </tr>\n                    " + rowData + "\n                ";
                    _this.addTableRow(tr);
                });
                $('.additional_param').hide(); //если ссылки внутренние дополнительные колонки не нужны
            };
            return DuplicatedLinks;
        }(Popup.PopupTable));
        Popup.DuplicatedLinks = DuplicatedLinks;
    })(Popup = Crawling.Popup || (Crawling.Popup = {}));
})(Crawling || (Crawling = {}));
//# sourceMappingURL=crawling.js.map