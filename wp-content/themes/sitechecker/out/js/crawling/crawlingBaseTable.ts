/// <reference path="baseTable.ts"/>

namespace Crawling{

    export abstract class CrawlingBaseTable extends BaseTable {
        protected requestPath: string;

        protected token: string;

        constructor(apiPath: string, requestPath: string, token: string) {
            super(apiPath);
            this.token = token;

            this.requestPath = requestPath;
        }

        protected abstract setPagination();
        protected abstract getTableElements(): JQuery;

        protected getLoader(){
            return $('.loader');
        }

        processResult(data: any) {
            const result = data[0];

            const {message, summary} = result;

            if (message !== 'success') {
                this.getCrawlingDate().css("color", "red");
                this.getCrawlingDate().html("Crawling in progress");
            }else{
                this.totalCount = result.totalCount;
                this.filteredCount = result.filteredCount;
                this.totalPages = result.pages;

                this.setPagination();

                this.getTBody().empty();

                if (summary.length > 0) {
                    this.drawRows(summary)
                } else {
                    $('#popup_table').css('display','table');
                    this.addTableRow(`
                        <tr>
                            <td colspan="5">
                                <p class="no__data">No data</p>
                            </td>
                        </tr>
                    `);
                }
            }

            this.getTableElements().show();

            this.hideLoader();
        };

        hideLoader(){
            this.getLoader().css('opacity', '0');
        }

        getStatusCodeWithColor(statusCode: number){
            let color = 'red';
            if(statusCode >= 200 && statusCode < 300){
                color = "#6fc66f";
            }else if(statusCode > 300 && statusCode < 400){
                color = "#f7a72c"
            }
            return color;
        }

        getStatusCodeIcon(statusCode: number): string{
            if(statusCode >= 200 && statusCode < 300){
                return "#56c504"
            }else if(statusCode > 300 && statusCode < 400){
                return "#fd9425";
            }else{
                return "#f24c27";
            }
        }

        logFailed(log): void{
            this.getCrawlingDate().css("color", "red");
            this.getCrawlingDate().html("Failed to get data");
            console.log(log)
        }

        display() {
            this.getLoader().css('opacity','1');

            this.getPageData().then(
                data => this.processResult(JSON.parse(data)),
                error => this.logFailed(error)
            )
        };

        getPageData() {
            const request = {
                'data': [
                    this.getRequestData()
                ]
            };

            const data = JSON.stringify(request);

            return this.crawlerRequest(data);
        }

        crawlerRequest(data){
            return this.doRequest({
                path:this.requestPath,
                action: 'crawler_request',
                data: data,
                method: 'POST'
            })
        }
    }
}