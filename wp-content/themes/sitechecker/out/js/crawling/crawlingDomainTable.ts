/// <reference path="CrawlingBaseTable.ts"/>
/// <reference path="../../../../../../typings/globals/jquery/index.d.ts" />

namespace Crawling {

    export class CrawlingDomainTable extends BaseTable {

        private baseUrl: string;
        private dictionary: any;
        private availablePagesLimit: number;
        private basicTemplateDirectoryUri: string;

        constructor(apiPath: string, baseUrl: string, availablePagesLimit: number, basicTemplateDirectoryUri: string, dictionary: any) {
            super(apiPath);
            this.baseUrl = baseUrl;
            this.availablePagesLimit = availablePagesLimit;
            this.basicTemplateDirectoryUri = basicTemplateDirectoryUri;
            this.dictionary = dictionary;
        }

        getPageData() {
            return this.doRequest(
                this.getRequestData()
            )
        }

        protected processResult(response: any) {
            const data = response.data;

            this.getTBody().empty();
            if (data.length > 0) {
                this.drawRows(data)
            }
        }

        protected getSortParam(): string {
            return 'weight';
        }

        protected getPagination() {
            return $('#pageTablePagination');
        }

        protected getTable() {
            return $('#domainsTable');
        }

        protected getTBody() {
            return this.getTable().find("tbody");
        }

        protected getCrawlingDate() {
            return $('#crawling_date');
        }

        protected getRequestData() {
            const requestData = {
                action: "crawler_domain_list",
                sortParam: this.sortParam,
                sortBy: this.sortBy,
                pageSize: this.pageSize,
                page: this.page,
            };

            return requestData;
        }

        public sort(column) {
            if (column === this.sortParam) {
                this.sortBy = this.sortBy === 'asc' ? 'desc' : 'asc';
            } else {
                this.sortBy = 'asc';
                this.sortParam = column;
            }

            this.display();
        };

        getDiffSpan(currentCount,  previousCount): string{
            let diffSpan = "";

            if(previousCount !== null){
                const pagesDiff = parseInt(currentCount) - parseInt(previousCount);

                if(pagesDiff > 0){
                    diffSpan=   `<span>+${pagesDiff}</span>`;
                }else if(pagesDiff === 0 ) {
                    diffSpan = `<span></span>`;
                }else{
                    diffSpan = `<span>${pagesDiff}</span>`;
                }
            }

            return diffSpan;
        }

        isDomainInProgress(domainStatus){
            return parseInt(domainStatus) === 0 || parseInt(domainStatus) === 1 || parseInt(domainStatus) === 4;
        }

        needsDomainUpdateProgress(domainStatus){
            return parseInt(domainStatus) === 0 || parseInt(domainStatus) === 1 || parseInt(domainStatus) === 4;
        }

        drawRows(rows) {
            rows.forEach((value) => {
                const diffSpanCount = this.getDiffSpan(value.domain_count, value.domain_previous_count);
                const diffSpanCritical = this.getDiffSpan(value.count_critical, value.previous_count_critical);
                const domainStatus = value.domain_status;

                const queueCount = parseInt(value.in_queue_count);

                let monitoring_enabled,
                    monitoringClass;
                if(value.monitoring_enabled == true){
                    monitoring_enabled = 'checked';
                    monitoringClass = 'monitoringEnabled';
                } else{
                    monitoring_enabled = 'unchecked';
                    monitoringClass = 'monitoringDisabled';
                }

                this.addTableRow(`
                    <tr class="domain_statistic ${(this.needsDomainUpdateProgress(domainStatus)) ? 'update_progress' : ''}" data-id="${value.domain_id}">
                        <td href="${this.baseUrl}/crawl-report-domain/?id=${value.domain_id}" class="crawler-domain-url">
                            <div class="domain_statistic-block">
                                <div class="domain_links">
                                    <a class="statistic_link" href="${this.baseUrl}/crawl-report-domain/?id=${value.domain_id}">
                                        ${value.domain_url}
                                    </a>
                                    <a class="external-link" href="${value.domain_url}" target="_blank">
                                        <img src="${this.basicTemplateDirectoryUri}/out/img/external-link.svg" alt="external-link" title="external-link">
                                    </a>
                                </div>
                            </div>
                        </td>
                        <td class="crawler-domain-urls">
                            <div class="domain_status">
                                <button type="button" class="btn">
                                ${(value.domain_status === 2 && this.availablePagesLimit > 0)? '<span class="crawler-domain-status exceeded">Limit Exceeded</span>' : ''}
                                ${(this.isDomainInProgress(domainStatus)) ? '<span class="crawler-domain-status inprogress">In progress</span>' : '<span class="crawler-domain-status completed">Finished</span>'}</button>
                            </div>
                        </td>
                        <td class="crawler-domain-urls">
                            <a href="${this.baseUrl}/crawl-report-domain/?id=${value.domain_id}">
                                <span>
                                    <span class="${(this.needsDomainUpdateProgress(domainStatus)) ? 'update_progress_count' : ''}" data-id="${value.domain_id }" data-url="${value.domain_url }">
                                       ${
                                            this.needsDomainUpdateProgress(domainStatus) ?
                                                this.isValidProgressNum(value.in_progress_count) ? value.in_progress_count : 0
                                                    :
                                                this.isValidProgressNum(value.domain_count) ? value.domain_count : '-'
                                        } 
                                    </span>
                                    <sub class="${(this.needsDomainUpdateProgress(domainStatus)) ? 'update_progress_count_previous' : ''}">
                                        ${!this.needsDomainUpdateProgress(domainStatus) ? diffSpanCount : ''}
                                    </sub>
                                </span>
                            </a>
                            <div class="free_limits" style="display: ${queueCount>0 && !$('.crawler_data-info').hasClass('boostaLevel') ? 'inline-block':'none'}">
                            <div class="pulse1"></div>
                            <div class="pulse2"></div>
                            <div class="icon"></div>
                                <div><img src="/wp-content/themes/sitechecker/out/img/urls_warning.svg" alt="urls_warning" title="urls_warning">
                                    <div class="tooltip"><a href="/plans/">Upgrade your Plan now</a> to check all pages on the website.</div>
                                </div>
                            </div>
                        </td>
                        <td class="crawler-domain-urls">
                            <a href="${this.baseUrl}/crawl-report-domain/?id=${value.domain_id}">
                                <span>
                                    <span class="${(this.needsDomainUpdateProgress(domainStatus)) ? 'update_progress_count_critical' : ''}" data-id="${value.domain_id }" data-url="${value.domain_url }">
                                        ${
                                            this.needsDomainUpdateProgress(domainStatus) ?
                                                this.isValidProgressNum(value.in_progress_count_critical)? value.in_progress_count_critical :0
                                                    :
                                                this.isValidProgressNum(value.count_critical) ? value.count_critical : '-'
                                        }
                                    </span>
                                    <sub class="${(this.needsDomainUpdateProgress(domainStatus)) ? 'update_progress_count_critical_previous' : ''}">
                                        ${!this.needsDomainUpdateProgress(domainStatus) ? diffSpanCritical : ''}
                                    </sub>
                                </span>
                            </a>
                        </td>
                        <td class="crawler-domain_urls">
                            <label class="switch">
                                <input type="checkbox" class="boosta_monitoring-input monitoring ${monitoringClass}" data-id="${value.domain_id}" ${monitoring_enabled}>
                                <span class="slider round"></span>
                            </label>
                        </td>
                        <td class="crawler-domain-recrawl">
                            <div class="crawler-domain_block">
                                <a class="audit_btn" href="${this.baseUrl}/crawl-report-domain/?id=${value.domain_id}">
                                    <img src="/wp-content/themes/sitechecker/out/img/audit__icon-active.svg" alt="chart" title="chart">
                                    <span>View audit</span>
                                </a>
                                <a class="changes_btn ${monitoring_enabled}" href="${this.baseUrl}/monitoring/?id=${value.domain_id}">
                                    <img src="/wp-content/themes/sitechecker/out/img/monitoring__icon${monitoring_enabled === 'checked' ? '-active' : ''}.svg" alt="chart" title="chart">
                                    <span>View changes</span>
                                </a>
                                <span class="share_btn" name="${value.domain_url}" data-id="${value.domain_id}">
                                <input type="hidden" class="share_token" name="${value.token}">
                                    <img src="/wp-content/themes/sitechecker/out/img/${parseInt(value.crawling_sharing_enabled)===1 ? 'share__active' : 'share'}.svg" alt="share" title="share" name="${this.baseUrl}" data-id="${parseInt(value.crawling_sharing_enabled)}">
                                    <div class="tooltip">Share website</div>
                                </span>
                                <a class="recrawl_btn ${parseInt(domainStatus) === 1 || parseInt(domainStatus) === 0 ? 'inprogress' : ''}" href="${this.baseUrl}/crawl-report/?re_crawl=1&id=${value.domain_id}">
                                    <img src="/wp-content/themes/sitechecker/out/img/${parseInt(domainStatus) === 1 || parseInt(domainStatus) === 0 ? 'reload__unactive' : 'reload'}.svg" alt="recrawl" title="recrawl">
                                    <div class="tooltip">Run new audit</div>
                                </a>
                                <a class="deletecrawl_btn" name="${value.domain_id}">
                                    <img src="/wp-content/themes/sitechecker/out/img/trash.svg" alt="trash" title="trash">
                                    <div class="tooltip">Delete website</div>
                                </a>
                            </div>
                        </td>
                    </tr>
                `);
            });

            $('.monitoring').change(function () {
                $('.monitoring').removeClass('exceeded');
                const apiPath = "/wp-admin/admin-ajax.php";
                let action,
                    domainID = $(this).attr('data-id'),
                    btn = $('tr[data-id='+domainID+']').find('.changes_btn'),
                    switcher = $(this);
                if(switcher.is(':checked')){
                    switcher.removeClass('monitoringDisabled').addClass('monitoringEnabled');
                    action = 'monitoring_enable';
                    btn.removeClass('unchecked exceeded').addClass('checked').find('img').attr('src','/wp-content/themes/sitechecker/out/img/monitoring__icon-active.svg');
                } else {
                    switcher.removeClass('monitoringEnabled').addClass('monitoringDisabled');
                    action = 'monitoring_disable';
                    btn.removeClass('checked exceeded').addClass('unchecked').find('img').attr('src','/wp-content/themes/sitechecker/out/img/monitoring__icon.svg');
                }
                $.ajax({
                    url: apiPath,
                    type: "POST",
                    dataType: "JSON",
                    data: {
                        id: domainID,
                        action: action,
                    },
                    success: function (data) {
                        console.log(data);
                        if(data.status == 'no exceeded'){
                            switcher.prop('checked', false).removeClass('monitoringEnabled monitoringDisabled').addClass('exceeded');
                            btn.removeClass('checked').addClass('unchecked').find('img').attr('src','/wp-content/themes/sitechecker/out/img/monitoring__icon.svg');
                        }
                    }
                });
                return false;
            });

            $('#enable_sharing').change(function () {
                const apiPath = "/wp-admin/admin-ajax.php";
                $.ajax({
                    url: apiPath,
                    type: "POST",
                    dataType: "JSON",
                    data: {
                        id:  $('.share__popup').attr('data-id'),
                        action: 'enable_disable_crawling_sharing',
                    },
                    success: function (data) {
                        console.log(data);
                        let img_attr = $('.share_btn[data-id='+$('.share__popup').attr('data-id')+']').find('img');
                        if(data.enabled === 1){
                            img_attr.attr('src', '/wp-content/themes/sitechecker/out/img/share__active.svg').attr('data-id', '1');
                            $('.share__popup-body button').removeClass('btn-disabled');
                        } else{
                            img_attr.attr('src', '/wp-content/themes/sitechecker/out/img/share.svg').attr('data-id', '0');
                            $('.share__popup-body button').addClass('btn-disabled');
                        }
                    }
                });
                return false;
            });

            $('.share_btn').click(function () {
                let enabled_sharing = $(this).find('img').attr('data-id');
                if(parseInt(enabled_sharing) === 1){
                    $("#enable_sharing").prop("checked", true);
                    $('.share__popup-body button').removeClass('btn-disabled');
                } else{
                    $('.share__popup-body button').addClass('btn-disabled');
                    $("#enable_sharing").prop("checked", false);
                }
                let domainId =  $(this).attr('data-id'),
                    baseUrl = $(this).find('img').attr('name'),
                    token = $(this).find('.share_token').attr('name');
                $('#share_url').val(baseUrl+'/crawl-report-domain/?token='+token);
                $('.share__popup-header span').text($(this).attr('name'));
                $('.share__popup').attr('data-id', domainId).fadeIn().css('display','flex');
                $('body').css({
                    'height':'100%',
                    'overflow':'hidden'
                });

                function share__invit(){
                    let btn = $('#share_invite'),
                        sharing_email = $('#share_email');

                    if(sharing_email.val() === ''){
                        $(sharing_email).attr('placeholder','Please, type email.')
                    } else{
                        btn.addClass('generating').text('Inviting');
                        $.ajax({
                            url: "/wp-admin/admin-ajax.php",
                            type: "POST",
                            dataType: "JSON",
                            data: {
                                email: sharing_email.val(),
                                link: $('#share_url').val(),
                                action: 'send_share_invite'
                            },
                            success: function (data) {
                                function btnRestart(){
                                    btn.text('Invite');
                                    $('.share__popup-body').append('<p class="invited_urls">'+$('#share_email').val()+' was invited.</p>');
                                    $('#share_email').val('');
                                    return false;
                                }
                                if(data.success === true){
                                    btn.removeClass('generating').text('Invited!');
                                    setTimeout(btnRestart,1000);
                                }
                            }
                        });
                    }
                }

                $('#share_invite').click(function () {
                    share__invit();
                });

                $('#share_email').keypress(function(e) {
                    if(e.which == 13) {
                        share__invit();
                        e.preventDefault();
                        return false;
                    }
                });


                $('.share__popup-header img').click(function () {
                    $('.share__popup').fadeOut();
                    $('body').css({
                        'height':'auto',
                        'overflow':'auto'
                    });
                    $('#share_url, #share_email').val('');
                    $('.invited_urls').remove();
                })
            });

            $('[data-toggle="tooltip"]').tooltip();
            $(".crawler-domain-urls, .crawler-domain-url").on('click', function (e) {
                var domainLink = $(this).parent('tr');
                document.location.href = domainLink.find('.crawler-domain-url').attr('href');
            });
            $(".external-link").click(function (e) {
                e.stopPropagation();
            });

            $('#loader-list').css('opacity', '0');

            this.startProgressUpdater();
        }

        isValidProgressNum(num){
            return num !== null && !isNaN(num)
        }

        logFailed(log): void {
            console.log("error" + log);
        }

        display() {
            this.getPageData().then(
                data => this.processResult(JSON.parse(data)),
                error => this.logFailed(error)
            )
        };

        startProgressUpdater(){
            const rowsToUpdate = this.getTBody().find('.update_progress');
            const table = this;

            rowsToUpdate.each(function () {
                const currentRow = $(this);

                const spanToUpdateCount = currentRow.find('.update_progress_count');
                const spanToUpdateNon200 = currentRow.find('.update_progress_count_non_200');
                const spanToUpdateBlocked = currentRow.find('.update_progress_count_blocked');
                const spanToUpdateCritical = currentRow.find('.update_progress_count_critical');

                function countUpdater(blockName, value) {
                    let $countToBlock = blockName,
                        countTo = value;
                    $({
                        countNum: blockName.text()
                    }).animate({
                            countNum: countTo
                        },
                        {
                            duration: 2000,
                            easing: 'swing',
                            step: function () {
                                $countToBlock.text(Math.floor(this.countNum));
                            },
                            complete: function () {
                                $countToBlock.text(this.countNum);
                            }
                        });
                }

                const id = currentRow.data('id');

                const updater = setInterval(function () {
                    $.ajax({
                        url: "/wp-admin/admin-ajax.php",
                        type: "POST",
                        async:  false,
                        dataType: "JSON",
                        data: {
                            domainId: id,
                            action: 'crawler_in_progress_statistic',
                        },
                        success: function (data) {
                            if(parseInt(data.status) === 1){
                                countUpdater(spanToUpdateCount, parseInt(data.in_progress_count));
                                countUpdater(spanToUpdateNon200, parseInt(data.in_progress_count_non_200));
                                countUpdater(spanToUpdateBlocked, parseInt(data.in_progress_count_blocked));
                                countUpdater(spanToUpdateCritical, parseInt(data.in_progress_count_critical));
                            }else if(parseInt(data.status) == 2){
                                clearInterval(updater);

                                $('#testDomain').hide();
                                table.init();
                                table.updateLimits();
                            }
                        }
                    });
                }, 5000);
            });
        }

        crawlConfirmClose(){
            $('.addCrawl-block').css('display', 'none');
            $('.addCrawl_popup').fadeOut();
        }

        updateLimits(){
            this.doRequest({
                action: "crawler_get_limits"
            }).then(
                resultJSON => {
                    const result = JSON.parse(resultJSON);

                    if(result.success){
                        const availableDomains = parseInt(result.data.availableDomains) < 0 ? 0 : parseInt(result.data.availableDomains)  ;
                        this.availablePagesLimit = parseInt(result.data.availablePages) < 0 ? 0 : parseInt(result.data.availablePages)  ;

                        $("#available_pages").text(this.availablePagesLimit);
                        $("#available_domains").text(availableDomains);

                        if(window.location.href.indexOf('create') > 0){
                            let create = localStorage.getItem('create'),
                                createLogin = localStorage.getItem('createLogin');

                            if(createLogin == 'Shown' || $('#testDomain').is(":visible")){
                                if(create == 'Shown'){
                                    localStorage.setItem('create', 'false');
                                    localStorage.setItem('createLogin', 'false');
                                    $('.addCrawl_popup .login_popup-close').click(function(){
                                        $('.addCrawl-block').css('display', 'none');
                                        $('.addCrawl_popup').fadeOut();
                                    });
                                    setTimeout(this.crawlConfirmClose, 3000);
                                }
                            } else if(availableDomains !== 0){
                                if(create == 'Shown'){
                                    localStorage.setItem('create', 'false');
                                    localStorage.setItem('createLogin', 'false');
                                    $('.addCrawl_popup .login_popup-close').click(function(){
                                        $('.addCrawl-block').css('display', 'none');
                                        $('.addCrawl_popup').fadeOut();
                                    });
                                    setTimeout(this.crawlConfirmClose, 3000);
                                }
                            }
                        }

                        $('#loader-list').css('opacity', '0');
                        $('.crawlerData').fadeIn();
                    }
                },
                error => {
                    console.log(error);
                }
            )
        }
    }
}