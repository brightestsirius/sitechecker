var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/// <reference path="CrawlingBaseTable.ts"/>
var Crawling;
(function (Crawling) {
    var CrawlingTable = /** @class */ (function (_super) {
        __extends(CrawlingTable, _super);
        function CrawlingTable(apiPath, domainUrl, crawlerDomainId) {
            var _this = _super.call(this, apiPath + '/getWeightByDomains', crawlerDomainId) || this;
            _this.domainUrl = domainUrl;
            _this.apiPath = apiPath;
            _this.sort_url = $('#sort_url');
            _this.sort_weight = $('#sort_weight');
            return _this;
        }
        CrawlingTable.prototype.getSortParam = function () {
            return 'weight';
        };
        CrawlingTable.prototype.getLoader = function () {
            return $('#loader');
        };
        CrawlingTable.prototype.getPagination = function () {
            return $('#pageTablePagination');
        };
        CrawlingTable.prototype.getTableElements = function () {
            return $('.table_elements');
        };
        CrawlingTable.prototype.getTable = function () {
            return $('#pageTable');
        };
        CrawlingTable.prototype.getTBody = function () {
            return this.getTable().find("tbody");
        };
        CrawlingTable.prototype.getCrawlingDate = function () {
            return $('#crawlingDate');
        };
        CrawlingTable.prototype.setPagination = function () {
            var html = "\n                <div class=\"col-sm-5\">\n                    <div class=\"dataTables_info\" id=\"pageTable_info\" role=\"status\" aria-live=\"polite\">\n                        Showing " + ((this.page * this.pageSize) - this.pageSize) + " to " + (this.page * this.pageSize > this.totalCount ? this.totalCount : this.page * this.pageSize) + "  of " + this.totalCount + " entries\n                    </div>\n                </div>\n                <div class=\"col-sm-7\">\n                    <div class=\"dataTables_paginate paging_simple_numbers\" id=\"pageTable_paginate\">\n                        <ul class=\"pagination\">\n                            <li class=\"page-item disabled\">\n                                <span>Page " + this.page + " from " + this.totalPages + "</span>\n                            </li>\n                            <li class=\"paginate_button page-item previous " + (this.page > 1 ? '' : 'disabled') + "\">\n                                <a href=\"javascript:void(0);\" aria-controls=\"pageTable\" class=\"page-link\" " + (this.page > 1 ? "onclick=\"crawlingTable.previousPage();\"" : '') + "  >Previous</a>\n                            </li>\n                            <li class=\"paginate_button page-item " + (this.page < this.totalPages ? '' : 'disabled') + "\">\n                                <a href=\"javascript:void(0);\" aria-controls=\"pageTable\"  class=\"page-link\" " + (this.page < this.totalPages ? "onclick=\"crawlingTable.nextPage()\"" : '') + " >Next</a>\n                            </li>\n                        </ul>\n                    </div>\n                </div>\n            ";
            this.getPagination().html(html);
        };
        ;
        CrawlingTable.prototype.getRequestData = function () {
            var requestData = this.addFilters({
                url: this.domainUrl,
                domainId: this.crawlerDomainId,
                sortParam: this.sortParam,
                sortBy: this.sortBy,
                pageSize: this.pageSize,
                page: this.page,
                searchParamUrl: this.getSearchParamUrl()
            });
            var queryString = $.param(requestData);
            // if (history.pushState) {
            //     const newUrl = window.location.protocol + "//" + window.location.host + window.location.pathname + "?"+ queryString;
            //     window.history.pushState({path:newUrl},'',newUrl);
            // }
            return requestData;
        };
        CrawlingTable.prototype.sort = function (column) {
            if (column === this.sortParam) {
                this.sortBy = this.sortBy === 'asc' ? 'desc' : 'asc';
            }
            else {
                this.sortBy = 'asc';
                this.sortParam = column;
            }
            this.sort_url.attr('class', 'sorting');
            this.sort_weight.attr('class', 'sorting');
            if (column === 'weight') {
                this.sort_weight.attr('class', 'sorting_' + this.sortBy);
            }
            else {
                this.sort_url.attr('class', 'sorting_' + this.sortBy);
            }
            this.display();
        };
        ;
        CrawlingTable.prototype.parseCrawledDate = function (_a) {
            var timestamp = _a.settings.dateUpdateTask;
            return new Date(timestamp * 1000);
        };
        CrawlingTable.prototype.setCrawlingDate = function (result) {
            var date = this.parseCrawledDate(result);
            try {
                this.getCrawlingDate().css("color", 'black');
                this.getCrawlingDate().html(date.toISOString().replace(/z|t/gi, ' ').trim().substr(0, 10));
            }
            catch (err) {
                console.log(date);
            }
        };
        CrawlingTable.prototype.toggleRowData = function (index) {
            var row = $('#row_' + index);
            var rowIcon = row.find('.icon');
            var rowData = $('#row_data_' + index);
            if (row.hasClass("dataShow")) {
                row.removeClass('dataShow');
                rowIcon.removeClass("fa-minus-circle");
                rowIcon.addClass("fa-plus-circle");
                rowIcon.css("color", "green");
                rowData.hide();
            }
            else {
                row.addClass("dataShow");
                rowIcon.removeClass("fa-plus-circle");
                rowIcon.addClass("fa-minus-circle");
                rowIcon.css("color", "red");
                rowData.show();
            }
        };
        CrawlingTable.prototype.getRowDataHTML = function (data, index) {
            return "\n                <tr id=\"row_data_" + index + "\" style=\"display: none;\">\n                    <td colspan=\"6\" class=\"container-fluid\">\n                        <div class=\"row\">\n                            <div class=\"col-md-4\" style=\"min-height: 20px\">\n                                <div class=\"row\">\n                                    <div class=\"text-center\">\n                                        General\n                                    </div>\n                                </div>\n                                <div class=\"row\">\n                                     <div class=\"col-md-8 col-lg-offset-2 line\" style=\"border-bottom: 3px solid black;\"></div>\n                                </div>\n                                <div class=\"col-md-10 col-md-offset-1\" style=\"padding-top: 5px;\">\n                                    <div class=\"row\">Status code: " + (data.httpStatusCode || '-') + " <img src=\"" + this.getStatusCodeIcon(parseInt(data.httpStatusCode)) + "\" style=\"width: 15; height: 15px\"></div>\n                                    <div class=\"row\">Content type: " + (data.contentType || '-') + "</div>\n                                    <div class=\"row\">Charset: " + (data.charset || '-') + "</div>\n                                    <div class=\"row\">Location: \n                                        " + (data.location !== '' ?
                "\n                                                <a href=\"" + data.location + "\">" + data.location + " </a>\n                                                <a href=\"" + data.location + "\" target=\"_blank\"><i class=\"fa fa-external-link\" aria-hidden=\"true\"></i></a>\n                                            " : '-') + "\n                                    </div>\n                                </div>\n                            </div>\n                            <div class=\"col-md-4\" style=\"min-height: 20px\">\n                                <div class=\"row text-center col-lg-offset-1\">\n                                    Search optimization\n                                </div>\n                                <div class=\"col-md-8 col-lg-offset-2 line\" style=\"border-bottom: 3px solid black;\"></div>\n                                <div class=\"col-md-10 col-md-offset-1\" style=\"padding-top: 5px;\">\n                                    <div class=\"row\">Title: " + (data.title || '-') + "</div>\n                                    <div class=\"row\">H1: " + (data.h1 || '-') + "</div>\n                                    <div class=\"row\">Content Length: " + (data.contentLength || '-') + "</div>\n                                    <div class=\"row\">Canonical: \n                                        " + (data.canonical !== '' ?
                "\n                                                <a href=\"" + data.canonical + "\">" + data.canonical + " </a>\n                                                <a href=\"" + data.canonical + "\" target=\"_blank\"><i class=\"fa fa-external-link\" aria-hidden=\"true\"></i></a>  \n                                            " : '-') + "\n                                    </div>\n                                </div>\n                            </div>\n                            <div class=\"col-md-4\" style=\"min-height: 20px\">\n                                <div class=\"row text-center col-lg-offset-1\">\n                                    Robots value\n                                </div>\n                                <div class=\"col-md-8 col-lg-offset-2 line\" style=\"border-bottom: 3px solid black;\"></div>\n                                <div class=\"col-md-10 col-md-offset-1 text-center\" style=\"padding-top: 5px;\">\n                                    <div class=\"row\">Index: " + data.index + "</div>\n                                    <div class=\"row\">Noindex " + data.noindex + "</div>\n                                    <div class=\"row\">Follow " + data.follow + "</div>\n                                    <div class=\"row\">Nofollow " + data.nofollow + "</div>\n                                    <div class=\"row\">Noarchive " + data.noarchive + "</div>\n                                    <div class=\"row\">Nosnippet " + data.nosnippet + "</div>\n                                    <div class=\"row\">Noyaca " + data.noyaca + "</div>\n                                    <div class=\"row\">Noodp " + data.noodp + "</div>\n                                </div>\n                            </div>\n                        </div>\n                    </td>\n                </tr>\n            ";
        };
        CrawlingTable.prototype.drawRows = function (rows) {
            var _this = this;
            rows.forEach(function (value, index) {
                var rowData = _this.getRowDataHTML(value, index);
                _this.addTableRow("\n                    <tr id=\"row_" + index + "\" >\n                        <td>\n                            <a href=\"javascript:void(0)\" onclick=\"crawlingTable.toggleRowData(" + index + ")\">\n                                <i class=\"fa fa-plus-circle icon\" style=\"color: green;\" aria-hidden=\"true\"></i>\n                            </a>\n                        </td>\n                        <td>\n                            <a href=\"" + value.url + "\">" + value.url + " </a>\n                            <a href=\"" + value.url + "\" target=\"_blank\"><i class=\"fa fa-external-link\" aria-hidden=\"true\"></i></a>\n                        </td>\n                        <td>" + value.normalizedWeight + "</td>\n                        <td>\n                            " + _this.getStatusCodeColumn(value) + "\n                        </td>\n                        <td>\n                             <div class=\"row\">\n                                <div class=\"col-md-4\">\n                                     <button type=\"button text-center\" style=\"font-size: 9px; width: 100%;\" class=\"btn btn-primary btn-md\" " + (value.externalCountTotal > 0 || 'disabled="disabled"') + "  onclick=\"popupTable = new Crawling.Popup.ExternalPopupTable('" + _this.apiPath + "', '" + value.url + "', '" + _this.crawlerDomainId + "')\">External (" + value.externalCountTotal + ")</button>\n                                </div>\n                                <div class=\"col-md-4\">\n                                     <button type=\"button text-center\" style=\"font-size: 9px; width: 100%;\" class=\"btn btn-primary btn-md\"  " + (value.internalCountTotal > 0 || 'disabled="disabled"') + " onclick=\"popupTable = new Crawling.Popup.InternalPopupTable('" + _this.apiPath + "', '" + value.url + "', '" + _this.crawlerDomainId + "')\">Internal (" + value.internalCountTotal + ")</button>\n                                </div>\n                                <div class=\"col-md-4\">\n                                     <button type=\"button text-center\" style=\"font-size: 9px; width: 100%;\" class=\"btn btn-primary btn-md\" " + (value.incomingCountTotal > 0 || 'disabled="disabled"') + " onclick=\"popupTable = new Crawling.Popup.IncomingPopupTable('" + _this.apiPath + "', '" + value.url + "', '" + _this.crawlerDomainId + "')\">Incoming (" + value.incomingCountTotal + ")</button>\n                                </div>\n                            </div>\n                        </td>\n                    </tr>\n                    " + rowData + "\n                ");
            });
            $('[data-toggle="tooltip"]').tooltip({
                'template': "<div class=\"tooltip\" role=\"tooltip\"><div class=\"tooltip-arrow\"></div><div class=\"tooltip-inner\" style=\"max-width: 500px;\"></div></div>"
            });
        };
        CrawlingTable.prototype.getStatusCodeColumn = function (value) {
            return "\n                " + (value.httpStatusCode < 300 || value.httpStatusCode >= 400 ?
                "<span style='color: " + this.getStatusCodeWithColor(parseInt(value.httpStatusCode)) + "'>" + value.httpStatusCode + "</span>" :
                "\n                        <span style=\"color: " + this.getStatusCodeWithColor(parseInt(value.httpStatusCode)) + "\">" + value.httpStatusCode + "</span> =>\n                        " + (value.redirectToStatusCode === 0 ?
                    "\n                                <span data-toggle=\"tooltip\" data-placement=\"top\" title=\"" + value.location + "\">???</span>\n                            " :
                    "\n                                <span data-toggle=\"tooltip\" data-placement=\"top\" title=\"" + value.location + "\" style=\"color: " + this.getStatusCodeWithColor(parseInt(value.redirectToStatusCode)) + "\" >" + value.redirectToStatusCode + "</span>\n                            ") + " \n                    ") + "\n            ";
        };
        CrawlingTable.prototype.addFilters = function (request) {
            if ($('#option_search').val() !== "") {
                request.searchParamUrl = $('#option_search').val();
            }
            if ($('#option_filter').val() !== "" && $('#option_filter').val() !== "0") {
                request.customFilter = $('#custom_filter').val();
            }
            return request;
        };
        CrawlingTable.prototype.changeFilter = function () {
            this.page = 1;
            this.display();
        };
        CrawlingTable.prototype.getSearchParamUrl = function () {
            return $('#option_search').val();
        };
        CrawlingTable.prototype.getStatusCodeWithColor = function (statusCode) {
            var color = 'red';
            if (statusCode >= 200 && statusCode < 300) {
                color = "#6fc66f";
            }
            else if (statusCode > 300 && statusCode < 400) {
                color = "#f7a72c";
            }
            return color;
        };
        CrawlingTable.prototype.getStatusCodeIcon = function (statusCode) {
            if (statusCode >= 200 && statusCode < 300) {
                return "/img/ok.png";
            }
            else if (statusCode > 300 && statusCode < 400) {
                return "/img/warning_yellow.png";
            }
            else {
                return "/img/warning.png";
            }
        };
        return CrawlingTable;
    }(Crawling.CrawlingBaseTable));
    Crawling.CrawlingTable = CrawlingTable;
})(Crawling || (Crawling = {}));
