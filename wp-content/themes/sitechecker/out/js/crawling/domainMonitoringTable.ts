/// <reference path="CrawlingBaseTable.ts"/>
/// <reference path="../../../../../../typings/globals/jquery/index.d.ts" />

namespace Crawling {

    export class DomainMonitoringTable extends BaseTable {

        private baseUrl: string;
        private dictionary: any;
        private availablePagesLimit: number;
        private basicTemplateDirectoryUri: string;

        constructor(apiPath: string, baseUrl: string, availablePagesLimit: number, basicTemplateDirectoryUri: string, dictionary: any) {
            super(apiPath);
            this.baseUrl = baseUrl;
            this.availablePagesLimit = availablePagesLimit;
            this.basicTemplateDirectoryUri = basicTemplateDirectoryUri;
            this.dictionary = dictionary;
        }

        getPageData() {
            return this.doRequest(
                this.getRequestData()
            )
        }

        protected processResult(response: any) {
            const data = response.data;

            this.getTBody().empty();
            if (data.length > 0) {
                let monitoringEnable = 0;
                $.each(data, function (key, val) {
                    if(val.monitoring_enabled === 1){
                        monitoringEnable++;
                    }
                });
                if(monitoringEnable>0){
                    this.drawRows(data);
                }else{
                    $('#monitoringTable tbody').append('<tr class="monitoringOff"><td colspan="6">You should enable monitoring on <a href="/crawl-report/">Audit page</a></td></tr>');
                }
            } else {
                this.addTableRow(`
                    <tr>
                        <td colspan="6" class="row text-center">
                            <p style="padding: 10px 0">No data</p>
                        </td>
                    </tr>
                `);
            }
        }

        protected getSortParam(): string {
            return 'weight';
        }

        protected getPagination() {
            return $('#pageTablePagination');
        }

        protected getTable() {
            return $('#monitoringTable');
        }

        protected getTBody() {
            return this.getTable().find("tbody");
        }

        protected getCrawlingDate() {
            return $('#crawling_date');
        }

        protected getRequestData() {
            const requestData = {
                action: "crawler_domain_list",
                sortParam: this.sortParam,
                sortBy: this.sortBy,
                pageSize: this.pageSize,
                page: this.page,
            };

            return requestData;
        }

        public sort(column) {
            if (column === this.sortParam) {
                this.sortBy = this.sortBy === 'asc' ? 'desc' : 'asc';
            } else {
                this.sortBy = 'asc';
                this.sortParam = column;
            }

            this.display();
        };

        getDiffSpan(currentCount,  previousCount): string{
            let diffSpan = "";

            if(previousCount !== null){
                const pagesDiff = parseInt(currentCount) - parseInt(previousCount);

                if(pagesDiff > 0){
                    diffSpan=   `<span>+${pagesDiff}</span>`;
                }else if(pagesDiff === 0 ) {
                    diffSpan = `<span></span>`;
                }else{
                    diffSpan = `<span>${pagesDiff}</span>`;
                }
            }

            return diffSpan;
        }

        isDomainInProgress(domainStatus){
            return parseInt(domainStatus) === 0 || parseInt(domainStatus) === 1 || parseInt(domainStatus) === 4;
        }

        needsDomainUpdateProgress(domainStatus){
            return parseInt(domainStatus) === 0 || parseInt(domainStatus) === 1 || parseInt(domainStatus) === 4;
        }

        drawRows(rows) {
            rows.forEach((value) => {
                const domainStatus = value.domain_status;
                const domainID = value.domain_id;
                let monitoring_enabled,
                    apiPath = "/wp-admin/admin-ajax.php",
                    token = value.token,
                    changedPages = 0,
                    addedPages = 0,
                    deletedPages = 0,
                    allPages = 0;

                if(value.monitoring_enabled == true){
                    $.ajax({
                        url: apiPath,
                        async: false,
                        type: "POST",
                        dataType: "JSON",
                        data: {
                            path: '/getMonitoringStatisticForGraph',
                            method: 'POST',
                            action: 'crawler_request',
                            data: JSON.stringify({
                                token: token,
                                interval: 'month'
                            })
                        },
                        success: function (statistic) {
                            changedPages = statistic['summary'][0]['pagesChangesCount'];
                            addedPages = statistic['summary'][0]['pagesAddedCount'];
                            deletedPages = statistic['summary'][0]['pagesDeletedCount'];
                        }
                    });
                    $.ajax({
                        url: apiPath,
                        async: false,
                        type: "POST",
                        dataType: "JSON",
                        data: {
                            path: '/getMonitoringStatisticAllPagesCountForGraph',
                            method: 'POST',
                            action: 'crawler_request',
                            data: JSON.stringify({
                                token: token,
                                interval: 'month'
                            })
                        },
                        success: function (pages) {
                            allPages = pages['summary'][0]['pagesAllCount'];
                        }
                    });
                    monitoring_enabled = 'checked';
                    this.addTableRow(`
                    <tr class="domain_statistic ${(this.needsDomainUpdateProgress(domainStatus)) ? 'update_progress' : ''}" data-id="${value.domain_id}">
                        <td href="${this.baseUrl}/monitoring/?id=${domainID}" class="crawler-domain-url">
                            <div class="domain_statistic-block">
                                <div class="domain_links">
                                    <a class="statistic_link ${monitoring_enabled}" href="${this.baseUrl}/monitoring/?id=${value.domain_id}">${value.domain_url}</a>
                                    <a class="external-link" href="${value.domain_url}" target="_blank">
                                        <img src="${this.basicTemplateDirectoryUri}/out/img/external-link.svg" alt="external-link" title="external-link">
                                    </a>
                                </div>
                            </div>
                        </td>
                        <td class="crawler-domain_urls">${allPages}</td>
                        <td class="crawler-domain_urls">${addedPages}</td>
                        <td class="crawler-domain_urls">${changedPages}</td>
                        <td class="crawler-domain_urls">${deletedPages}</td>
                        <td class="crawler-domain-recrawl">
                            <div class="crawler-domain_block">
                                <a class="changes_btn ${monitoring_enabled}" href="${this.baseUrl}/monitoring/?id=${domainID}">
                                    <img src="/wp-content/themes/sitechecker/out/img/monitoring__icon-active.svg" alt="chart" title="chart">
                                    <span>View changes</span>
                                </a>
                            </div>
                        </td>
                    </tr>
                `);
                }
            });


            $(".crawler-domain_urls, .crawler-domain-url").on('click', function () {
                let domainLink = $(this).parent('tr');
                document.location.href = domainLink.find('.crawler-domain-url').attr('href');
            });
            $(".external-link").click(function (e) {
                e.stopPropagation();
            });

            $('#loader-list').css('opacity', '0');

            this.startProgressUpdater();
        }

        isValidProgressNum(num){
            return num !== null && !isNaN(num)
        }

        logFailed(log): void {
            console.log("error" + log);
        }

        display() {
            this.getPageData().then(
                data => this.processResult(JSON.parse(data)),
                error => this.logFailed(error)
            )
        };

        startProgressUpdater(){
            const rowsToUpdate = this.getTBody().find('.update_progress');
            const table = this;

            rowsToUpdate.each(function () {
                const currentRow = $(this);

                const id = currentRow.data('id');

                const updater = setInterval(function () {
                    $.ajax({
                        url: "/wp-admin/admin-ajax.php",
                        type: "POST",
                        async:  false,
                        dataType: "JSON",
                        data: {
                            domainId: id,
                            action: 'crawler_in_progress_statistic',
                        },
                        success: function (data) {
                            if(parseInt(data.status) == 2){
                                clearInterval(updater);
                                $('#testMonitoringDomain').hide();
                                table.init();
                                table.updateLimits();
                            }
                        }
                    });
                }, 5000);
            });
        }

        crawlConfirmClose(){
            $('.addCrawl-block').css('display', 'none');
            $('.addCrawl_popup').fadeOut();
        }

        updateLimits(){
            this.doRequest({
                action: "crawler_get_limits"
            }).then(
                resultJSON => {
                    const result = JSON.parse(resultJSON);
                    if(result.success){
                        const availableDomains = parseInt(result.data.availbleMonitoring) < 0 ? 0 : parseInt(result.data.availbleMonitoring)  ;
                        $("#available_domains").text(availableDomains);

                        $('#loader-list').css('opacity', '0');
                        $('.crawlerData').fadeIn();
                    }
                },
                error => {
                    console.log(error);
                }
            )
        }
    }
}