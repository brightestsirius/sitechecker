/// <reference path="CrawlingBaseTable.ts"/>
/// <reference path="../../../../../../typings/globals/jquery/index.d.ts" />

namespace Crawling{

    export class InProgressCrawlingTable extends CrawlingBaseTable {

        private sort_weight: JQuery;
        private sort_url: JQuery;
        private sort_status: JQuery;
        private sort_sitemap: JQuery;
        private domainUrl: string;
        private filterId: number;
        private lastDateFrom: number = 0;

        constructor(apiPath: string, domainUrl: string, token: string) {
            super(apiPath, '/inProgress/getWeightByDomains', token);

            this.domainUrl = domainUrl;
            this.apiPath = apiPath;
            this.sort_url = $('#sort_url');
            this.sort_weight = $('#sort_weight');
            this.sort_status = $('#sort_status_code');
            this.sort_sitemap = $('#sort_sitemap');

            this.pageSize = 50;
        }

        protected getSortParam(): string {
            return 'dateCreate';
        }

        protected getPagination() {
            return $('#pageTablePagination');
        }

        protected getTableElements() {
            return $('.table_elements');
        }

        protected getTable() {
            return $('#pageTable');
        }

        protected getTBody() {
            return this.getTable().find("tbody");
        }

        protected getCrawlingDate() {
            return $('#crawling_date');
        }

        protected setPagination() {
            const html = `
                <div>
                    <div class="dataTables_info" id="pageTable_info" role="status" aria-live="polite">
                        Showing ${((this.page * this.pageSize) - this.pageSize)} to ${this.page * this.pageSize > this.totalCount ? this.totalCount : this.page * this.pageSize}  of ${this.totalCount} entries
                    </div>
                </div>
                <div>
                    <div class="dataTables_paginate paging_simple_numbers" id="pageTable_paginate">
                        <ul class="pagination">
                            <li class="page-item disabled">
                                <span>Page ${this.page} from ${this.totalPages}</span>
                            </li>
                            <li class="paginate_button page-item previous ${ this.page > 1 ? '' : 'disabled'}">
                                <a href="javascript:void(0);" aria-controls="pageTable" class="page-link" ${this.page > 1 ? `onclick="crawlingTable.previousPage();"` : ''}  >Previous</a>
                            </li>
                            <li class="paginate_button page-item ${this.page < this.totalPages ? '' : 'disabled'}">
                                <a href="javascript:void(0);" aria-controls="pageTable"  class="page-link" ${this.page < this.totalPages ? `onclick="crawlingTable.nextPage()"` : ''} >Next</a>
                            </li>
                        </ul>
                    </div>
                </div>
            `;

            // this.getPagination().html(html);
        };

        public checkNewData(){
            const requestBody = this.getRequestData();
            requestBody.lastDateFrom = this.lastDateFrom;

            const request = {
                'data': [
                    requestBody
                ]
            };

            const data = JSON.stringify(request);

            this.crawlerRequest(data).then(
                data => this.processNewRows(JSON.parse(data)),
                error => this.logFailed(error)
            )
        }

        private processNewRows(data: any){
            const result = data[0];

            const {summary} = result;

            this.drawNewRows(summary);
        }

        private drawNewRows(rows){
            $('#pageTable_wrapper').fadeIn();
            $('.table-loading').parent().remove();

            rows.forEach((page, index) => {
                const countOfRows = this.getTBody().find('tr').length;
                //console.log([countOfRows, this.pageSize]);
                if(this.lastDateFrom < parseInt(page.dateCreate)){
                    this.lastDateFrom = parseInt(page.dateCreate);
                }
                let update = true;
                this.getTBody().prepend(this.renderRow(page, index, update));
                if(countOfRows + 1 >= this.pageSize){
                    this.getTable().find('tbody tr:last-of-type').remove();
                }
            });
        }

        protected getRequestData() {
            return this.addFilters({
                url: this.domainUrl,
                sortParam: this.sortParam,
                sortBy: this.sortBy,
                pageSize: this.pageSize,
                page: this.page,
                token: this.token,
            });
        }

        public sort(column) {
            if (column === this.sortParam) {
                this.sortBy = this.sortBy === 'asc' ? 'desc' : 'asc';
            } else {
                this.sortBy = 'asc';
                this.sortParam = column;
            }

            this.sort_url.attr('class', 'sorting');
            this.sort_weight.attr('class', 'sorting');
            this.sort_status.attr('class', 'sorting');
            this.sort_sitemap.attr('class', 'sorting');

            if (column === 'weight') {
                this.sort_weight.attr('class', 'sorting_' + this.sortBy);
            } else if (column === 'http_status_code') {
                this.sort_status.attr('class', 'sorting_' + this.sortBy);
            } else {
                this.sort_url.attr('class', 'sorting_' + this.sortBy);
            }

            this.display();
        };

        toggleRowData(index) {
            const row = $('#row_' + index);
            const rowIcon = row.find('.icon');
            const rowData = $('#row_data_' + index);

            if (row.hasClass("dataShow")) {
                row.removeClass('dataShow');
                rowIcon.text('+').css('font-size', '16px');
                rowData.hide();
            } else {
                row.addClass("dataShow");
                rowIcon.text('-').css('font-size', '25px');
                rowData.show();
            }
        }

        drawRows(rows) {
            rows.forEach((page, index) => {
                if(this.lastDateFrom < parseInt(page.dateDrom)){
                    this.lastDateFrom = parseInt(page.dateDrom);
                }
                let update = false;

                this.addTableRow(this.renderRow(page, index, update));
            });

            $('[data-toggle="tooltip"]').tooltip({
                'template': `<div class="tooltip" role="tooltip"><div class="tooltip-arrow"></div><div class="tooltip-inner" style="max-width: 500px;"></div></div>`
            });
        }

        renderRow(page, index, update){
            $('tr.gradient').css('display','none').fadeIn();
            setInterval(() => {
                $('#row_'+index).css('display','table-row');
            }, 1000);
            const statuses = this.getPageStatuses(page);
            let contentLength,
                contentType,
                updateClass,
                statusColor;
            switch (page.httpStatusCode.toString()) {
                case '200':
                    statusColor = 'success';
                    break;
                case '301':
                    statusColor = 'warning';
                    break;
                case '404':
                    statusColor = 'error';
                    break;
                default:
                    statusColor = 'warning';
                    break;
            }
            if(update == true){
                updateClass = 'gradient';
                contentLength = '<i class="fa fa-spinner fa-spin"></i>';
                contentType = '<i class="fa fa-spinner fa-spin"></i>';
            } else{
                updateClass = 'normal';
                contentLength = '';
                contentType = '';
            }
            return`
                <tr id="row_${index}" class="${updateClass} ${statusColor}">
                    <td>
                        <a href="${page.url}" target="_blank">${page.url}</a>
                        <p>${page.title}</p>
                    </td>
                    <td>${contentLength}</td>
                    <td>
                        ${this.getStatusCodeColumn(page)}
                    </td>
                    <td>${contentType}</td>
                    <td>
                        <div class="statistic_description">
                            ${
                statuses
                    .map(status => `<span class="${status.class}">${status.title}</span>`)
                    .reduce((prev, curr) => prev + curr, '')
                }
                        </div>
                    </td>
                </tr>
                
            `;
        }

        numberFormat(val, decimalPlaces) {
            return parseFloat(parseFloat(val).toFixed(decimalPlaces));
        }

        getPageStatuses(page): any{
            const statuses = [];
            const httpStatusCode = page.httpStatusCode;

            if(httpStatusCode == 200 && page.title !== '' &&  page.contentType === 'text/html'){
                if(page.title === ''){statuses.push({class: "red", 'title': "No Title"})}
                if(page.h1 === ''){statuses.push({class: "red", 'title': "No H1"})}
                if(page.description === ''){statuses.push({class: "red", 'title': "No Description"})}
                if(page.canonical === ''){statuses.push({class: "red", 'title': "Invalid Canonical"})}

                if(page.h1 === page.title){statuses.push({class: "yellow", 'title': "H1 = Title"})}
                if(page.title.length < 30){statuses.push({class: "yellow", 'title': "Short Title"})}
                if(page.title.length > 70){statuses.push({class: "yellow", 'title': "Max Title Length"})}
                if(!page.robotsTxtAll){statuses.push({class: "yellow", 'title': "Blocked URL"})}
            }
            if(httpStatusCode >= 300 && httpStatusCode < 400 ){statuses.push({class: "yellow", 'title': "Redirect"})}

            return statuses;
        }

        getStatusCodeColumn(page: any): string{
            let domain = window.location.protocol + "//" + window.location.host;
            return `
                ${
                page.httpStatusCode < 300 || page.httpStatusCode >= 400 ?
                    `
                        <span style="color: ${this.getStatusCodeIcon(parseInt(page.httpStatusCode))}">${page.httpStatusCode}</span>
                    ` :
                    `
                        <span style="color: ${this.getStatusCodeWithColor(parseInt(page.httpStatusCode))}">${page.httpStatusCode}</span> <img class="crawler_statistics-arrow" src="${domain}/images/arrow_down.svg" alt="arrow" title="arrow">
                        ${
                        page.redirectToStatusCode === 0 ?
                            `
                                <span data-toggle="tooltip" data-placement="top" title="${page.location}">???</span>
                            ` :
                            `
                                <span data-toggle="tooltip" data-placement="top" title="${page.location}" style="color: ${this.getStatusCodeWithColor(parseInt(page.redirectToStatusCode))}" >${page.redirectToStatusCode}</span>
                            `
                        } 
                    `
                }
            `;
        }

        addFilters(request) {
            request.searchParamUrl = $('#crawling_search').val();

            if (this.filterId !== null) {
                request.customFilter = this.filterId;
            }

            return request;
        }

        changeFilter(filterId: number){
            this.filterId = filterId;
            this.page = 1;
            this.display();
        }

        getStatusCodeWithColor(statusCode: number){
            let color = '#f24c27';
            if(statusCode >= 200 && statusCode < 300){
                color = "#56c504";
            }else if(statusCode > 300 && statusCode < 400){
                color = "#fd9425"
            }

            return color;
        }

        getStatusCodeIcon(statusCode: number): string{
            if(statusCode >= 200 && statusCode < 300){
                return "#56c504"
            }else if(statusCode > 300 && statusCode < 400){
                return "#fd9425";
            }else{
                return "#f24c27";
            }
        }
    }
}