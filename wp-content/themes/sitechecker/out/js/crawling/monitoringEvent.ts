/// <reference path="CrawlingBaseTable.ts"/>
/// <reference path="../../../../../../typings/globals/jquery/index.d.ts" />

namespace Crawling {

    export class MonitoringEvent extends BaseTable {

        private token: string;
        private interval: string;
        private filterDate: string;
        protected requestPath: string;

        constructor(apiPath: string, requestPath: string, token: string, interval: string, filterDate: string) {
            super(apiPath);
            this.requestPath = requestPath;
            this.pageSize = 10;
            this.sortBy = 'desc';
            this.sortParam = 'date_create';
            this.page = 1;
            this.token = token;
            this.interval = interval;
            this.filterDate = filterDate;
        }

        protected processResult(response: any) {
            //console.log(response);

            this.totalCount = response.totalCount;
            this.filteredCount = response.filteredCount;
            this.totalPages = response.pages;

            const events = response.summary;

            if (events.length > 0) {
                $('.monitoring_graph').css('display','flex');
                this.drawRows(events);
                if(response.pages === 1){
                    $('.nextEvent button').css('display','none');
                }
            }else if(events.length === 0 && response.pages === 0 ){
                $('.empty_array').fadeIn();
            } else if(events.length === 0 && response.pages > 0){
                $('.nextEvent').css('display','none');
            }
        }

        nextPage(){
            if (this.page * this.pageSize < this.totalCount) {
                this.page++;
                this.display();
            } else{
                $('.nextEvent button, .nextEvent .loader').css('display','none');
            }
        }

        protected getSortParam(): string {
            return 'weight';
        }

        protected getPagination() {
            return $('#pageTablePagination');
        }

        protected getTable() {
            return $('#graph .history__blocks');
        }

        protected getTBody() {
            return this.getTable();
        }

        protected getCrawlingDate() {
            return $('#crawling_date');
        }

        public sort(column) {
            if (column === this.sortParam) {
                this.sortBy = this.sortBy === 'asc' ? 'desc' : 'asc';
            } else {
                this.sortBy = 'asc';
                this.sortParam = column;
            }

            this.display();
        };

        drawRows(events) {
            events.forEach((event) => {
                const row = this.renderEvent(event);
                this.addTableRow(row);
            });
            $('#nextEvent').fadeIn();
            $('.nextEvent .loader').css('display','none');
        }

        logFailed(log): void {
            console.log("error" + log);
        }

        display() {
            this.getPageData().then(
                data => this.processResult(JSON.parse(data)),
                error => this.logFailed(error)
            )
        };

        protected getRequestData() {
            return {
                sortBy: this.sortBy,
                pageSize: this.pageSize,
                page: this.page,
                token: this.token,
                sortParam: this.sortParam,
                interval: this.interval,
                filterDate: this.filterDate,
            }
        }

        getPageData() {
            const request = this.getRequestData();

            //console.log(request);

            const data = JSON.stringify(request);

            return this.monitoringRequest(data);
        }

        monitoringRequest(data){
            return this.doRequest({
                path:this.requestPath,
                action: 'crawler_request',
                data: data,
                method: 'POST'
            })
        }

        renderEvent(event){
            let dateFormat = new Date(event.monitoringStatistic.dateCreate * 1000),
                options = {weekday: "long", year: "numeric", month: "long", day: "numeric", hour: '2-digit', minute: '2-digit'},
                date = dateFormat.toLocaleDateString("en-us",options).replace(",", "");

            const id = event.monitoringStatistic.id;

            const pagesAdded = JSON.parse(event.pagesAdded);
            const pageAddedArray = pagesAdded.map((page) => {
                const pageUrl = page.url;
                return `<li><div class="links"><a href="${pageUrl}" class="internal_link">${pageUrl}</a></div></li>`
            }).join('\n');

            const timeLineNewPages =  `
                <div class="history__block-data added">
                    <div class="top">
                        <img src="/wp-content/themes/sitechecker/out/img_design/monitoring__plus.svg"
                             alt="new pages" title="new pages" class="status">
                        <p>${pagesAdded.length} new pages</p>
                        <div class="arrow">
                            <img src="/wp-content/themes/sitechecker/out/img_design/monitoring__arrow-down.svg"
                                 alt="arrow" title="arrow">
                        </div>
                    </div>
                    <div class="body">
                        <ul>
                            ${pageAddedArray}
                        </ul>
                    </div>
                </div>
            `;

            const pagesDeleted = JSON.parse(event.pagesDeleted);
            const pageDeletedArray = pagesDeleted.map((page) => {
                const pageUrl = page.url;
                return `<li><div><a href="${pageUrl}">${pageUrl}</a></div></li>`
            }).join('\n');

            const timeLineDeletedPages =  `
                <div class="history__block-data deleted">
                    <div class="top">
                        <img src="/wp-content/themes/sitechecker/out/img_design/monitoring__delete.svg"
                             alt="Deleted Pages" title="Deleted Pages" class="status">
                        <p>${pagesDeleted.length} deleted pages</p>
                        <div class="arrow">
                            <img src="/wp-content/themes/sitechecker/out/img_design/monitoring__arrow-down.svg"
                                 alt="arrow" title="arrow">
                        </div>
                    </div>
                    <div class="body">
                        <ul>
                            ${pageDeletedArray}
                        </ul>
                    </div>
                </div>
            `;

            const pagesChanged = JSON.parse(event.pagesChanged);

            function getPagesByChangeTitle(pagesChanged, changeTitle) {
                return pagesChanged.filter((page) => {
                    const pageChangeDataItem = page.change_data.find(pageChange => pageChange.param === changeTitle);
                    return pageChangeDataItem !== undefined;
                }).map((page) => {
                    const pageChangeDataItem = page.change_data.find(pageChange => pageChange.param === changeTitle);
                    return {
                        url: page.url,
                        crc32url: page.crc32url,
                        previous: pageChangeDataItem.previous,
                        changed: pageChangeDataItem.changed,
                        data: pageChangeDataItem.data
                    }
                })
            }

            let resultBlocks = "";

            if(pagesChanged.length > 0){
                const changeParams = [
                    {
                        paramTitle: 'title',
                        image: 'event_html',
                        title: 'Title has changed',
                        needsCheckForTitle: false,
                        changedElements: getPagesByChangeTitle(pagesChanged, 'title')
                    },
                    {
                        paramTitle: 'description',
                        image: 'event_html',
                        title: 'Description has changed',
                        needsCheckForTitle: false,
                        changedElements: getPagesByChangeTitle(pagesChanged, 'description')
                    },
                    {
                        paramTitle: 'h1',
                        image: 'event_html',
                        title: 'H1 has changed',
                        needsCheckForTitle: false,
                        changedElements: getPagesByChangeTitle(pagesChanged, 'h1')
                    },
                    {
                        paramTitle: 'http_status_code',
                        image: 'event_server',
                        title: 'HTTP status code has changed',
                        needsCheckForTitle: false,
                        changedElements: getPagesByChangeTitle(pagesChanged, 'http_status_code')
                    },
                    {
                        paramTitle: 'robots_txt_all',
                        image: 'event_server',
                        title: 'Robots.txt has changed',
                        needsCheckForTitle: true,
                        falseTitle: 'URL disallowed',
                        trueTitle: 'URL allowed',
                        changedElements: getPagesByChangeTitle(pagesChanged, 'robots_txt_all')
                    },
                    {
                        paramTitle: 'robots_txt_content',
                        image: 'event_server',
                        title: 'Robots.txt has changed',
                        needsCheckForTitle: true,
                        falseTitle: 'URL disallowed',
                        trueTitle: 'URL allowed',
                        changedElements: getPagesByChangeTitle(pagesChanged, 'robots_txt_content')
                    },{
                        paramTitle: 'noindex',
                        image: 'event_server',
                        title: 'Index status has changed',
                        needsCheckForTitle: true,
                        falseTitle: 'URL indexation opened by index tag',
                        trueTitle: 'URL indexation closed by noindex tag',
                        changedElements: getPagesByChangeTitle(pagesChanged, 'noindex')
                    }
                ].filter(changeParam => {
                    return changeParam.changedElements.length > 0
                });

                resultBlocks = changeParams.map((changeParam ) => {
                    const changedElements = changeParam.changedElements;

                    const changedArray = changedElements.map((page) => {
                        let previous = page.changed,
                            changed = page.previous,
                            data = page.data,
                            stuff = [],
                            output = '',
                            array = '',
                            elementSring;

                        $(data).each(function(i, item){
                            array = item.text.split("\r\n");
                            $(array).each(function(i,element){
                                elementSring = {string:element, view:item.operation};
                                stuff.push(elementSring);
                            })
                        });

                        $(stuff).each(function(i, item) {
                            let elementString = item.string,
                                elementView = item.view,
                                prevString, prevView,
                                nextString, nextView,
                                secondNextString;

                            if (i !== 0) {
                                prevString = stuff[i - 1].string,
                                    prevView = stuff[i - 1].view;
                            }

                            if (i !== stuff.length - 1) {
                                nextString = stuff[i + 1].string,
                                    nextView = stuff[i + 1].view;
                            }

                            if (i !== stuff.length - 2 && i !== stuff.length - 1) {
                                secondNextString = stuff[i + 2].string;
                            }

                            if (elementView === 'EQUAL' && nextView === 'DELETE' || elementView === 'DELETE' || elementView === 'INSERT' || prevView === 'INSERT') {

                            } else {
                                output = output + '<li class="robots-current">' + elementString + '</li>';
                            }

                            if (item.view === "DELETE") {
                                output = output + '<li class="robots-delete"><span class="prev">' + prevString + '</span><span class="change">' + elementString + '</span><span class="next">' + secondNextString + '</span></li><li class="robots-added"><span class="prev">' + prevString + '</span><span class="change">' + nextString + '</span><span class="next">' + secondNextString + '</span></li>';
                            }
                        });

                        if(data){
                            return `<ul class="robots__changes">${output}</ul>`;
                        } else{
                            if(changeParam.needsCheckForTitle === true){
                                changed = page.changed === false ? changeParam.falseTitle : changeParam.trueTitle ;
                                previous = page.previous === false ? changeParam.falseTitle : changeParam.trueTitle ;
                            }
                            return `
                                <div class="history__block-data change__child">
                                    <div class="top">
                                        <img src="/wp-content/themes/sitechecker/out/img_design/monitoring__edit.svg"
                                             alt="Deleted Pages" title="Deleted Pages" class="status">
                                        <div class="links">
                                            <a href="${page.url}" class="internal_link">${page.url}</a><a
                                                    href="${page.url}"
                                                    target="_blank" class="external_link"><img
                                                        src="/wp-content/themes/sitechecker/out/img_design/external.svg"
                                                        alt="external" title="external"></a>
                                        </div>
                                        <div class="arrow">
                                            <img src="/wp-content/themes/sitechecker/out/img_design/monitoring__arrow-down.svg"
                                                 alt="arrow" title="arrow">
                                        </div>
                                    </div>
                                    <div class="body">
                                        <div class="event__changes">
                                            <div class="changes">
                                                <p>New data:</p>
                                                <span>${changed}</span>
                                                <img src="/wp-content/themes/sitechecker/out/img_design/changed__status.svg"
                                                     alt="Changed status" title="Changed status" class="status">
                                                <p>Previous data:</p>
                                                <span>${previous}</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                        `;
                        }
                    });

                    const changeArrayHTML = changedArray.join('\n');

                    return  `
                        <div class="history__block-data changed">
                            <div class="top">
                                <img src="/wp-content/themes/sitechecker/out/img_design/monitoring__edit.svg"
                                     alt="Changed Pages" title="Changed Pages" class="status">
                                <div class="links"><p>${changeParam.title}</p></div>
                                <div class="changes-count" style="display: ${changeParam.title === 'Robots.txt has changed' ? 'none':'block'}">${changedArray.length} changes</div>
                                <div class="arrow">
                                    <img src="/wp-content/themes/sitechecker/out/img_design/monitoring__arrow-down.svg"
                                         alt="arrow" title="arrow">
                                </div>
                            </div>
                            <div class="body">
                                ${changeArrayHTML}
                            </div>
                        </div>
                `;
                }).join('\n');
            }

            let timeLineHTML,
                newPages,
                deletedPages;

            if (pageAddedArray.length !== 0) {
                newPages = timeLineNewPages;
            } else {
                newPages = ``;
            }

            if (pageDeletedArray.length !== 0) {
                deletedPages = timeLineDeletedPages;
            } else {
                deletedPages = ``;
            }

            if(pageAddedArray.length == 0 && pageDeletedArray.length == 0 && pagesChanged.length == 0){
                timeLineHTML = '';
            } else {
                timeLineHTML = '<div class="history__block" id="'+id+'">' +
                    '<p class="date">'+date+'</p>'+newPages+deletedPages+resultBlocks+'</div>';
            }
            return timeLineHTML;
        }

    }
}