/// <reference path="CrawlingBaseTable.ts"/>
/// <reference path="../../../../../../typings/globals/jquery/index.d.ts" />

namespace Crawling {

    export class MonitoringTabTable extends BaseTable {

        private token: string;
        protected requestPath: string;
        private interval: string;
        private filterDate: string;
        private searchParamUrl: string;
        private isIndex: boolean;

        constructor(apiPath: string, requestPath: string, token: string, interval: string, filterDate: string, searchParamUrl:string, isIndex:boolean) {
            super(apiPath);
            this.requestPath = requestPath;
            this.sortParam = 'date_create';
            this.pageSize = 10;
            this.token = token;
            this.interval = interval;
            this.filterDate = filterDate;
            this.searchParamUrl = searchParamUrl;
            this.isIndex = isIndex;
        }

        protected processResult(response: any) {
            this.totalCount = response.totalCount;
            this.filteredCount = response.filteredCount;
            this.totalPages = response.pages;
            const events = response.summary;
            let searchParam = false;

            if(this.searchParamUrl !== undefined && this.searchParamUrl !== ''){
                searchParam = true;
            }

            if (events.length > 0) {
                $('.empty_array').css('display','none');
                this.drawRows(events);
                $('.monitoring__graph-block').css({
                    'height':'auto',
                    'overflow':'auto',
                    'padding':'15px 0 8px 15px'
                });
                $('.monitoring__filter').css('display','flex');
                $('#next-second').css('display','block');
            }else if(events.length === 0 && response.pages === 0 ){
                $('#next-second, .monitoring__graph-block').css('display','none');
                $('.empty_array').fadeIn();
            } else if(events.length === 0 && response.pages > 0){
                $('#next-second').css('display','none');
                if(searchParam){
                    $('.empty_array').css('display','none');
                    $('.monitoring_tab-second').empty().append('<p style="padding-left: 20px">URL not found in this domain</p>');
                }else{
                    $('.empty_array').fadeIn();
                    $('#next-second').css('display','none');
                }
            }
        }

        nextPage(){
            if (this.page * this.pageSize < this.totalCount) {
                this.page++;
                this.display();
            } else{
                $('.empty_array').fadeIn();
                $('#next-second').css('display','none');
            }
        }

        protected getSortParam(): string {
            return 'weight';
        }

        protected getPagination() {
            return $('#pageTablePagination');
        }

        protected getTable() {
            return $('.monitoring_tab-second');
        }

        protected getTBody() {
            return this.getTable();
        }

        protected getCrawlingDate() {
            return $('#crawling_date');
        }

        public sort(column) {
            if (column === this.sortParam) {
                this.sortBy = this.sortBy === 'asc' ? 'desc' : 'asc';
            } else {
                this.sortBy = 'asc';
                this.sortParam = column;
            }

            this.display();
        };

        drawRows(events) {
            events.forEach((event) => {
                const row = this.renderEvent(event);
                this.addTableRow(row);
            });
            $('#next-second .fa').css('display','none');
        }

        logFailed(log): void {
            console.log("error" + log);
        }

        display() {
            this.getPageData().then(
                data => this.processResult(JSON.parse(data)),
                error => this.logFailed(error)
            )
        };

        protected getRequestData() {
            return {
                sortParam: this.sortParam,
                sortBy: this.sortBy,
                pageSize: this.pageSize,
                page: this.page,
                token: this.token,
                interval: this.interval,
                filterDate: this.filterDate,
                searchParamUrl: this.searchParamUrl,
                isIndex: this.isIndex
            };
        }

        getPageData() {
            const request = this.getRequestData(),
                data = JSON.stringify(request);
            return this.monitoringRequest(data);
        }

        monitoringRequest(data){
            return this.doRequest({
                path:this.requestPath,
                action: 'crawler_request',
                data: data,
                method: 'POST'
            })
        }

        renderEvent(event){
            let dateFormat = new Date(event.monitoringStatistic.dateCreate * 1000),
                options = {weekday: "long", year: "numeric", month: "long", day: "numeric", hour: '2-digit', minute: '2-digit'},
                date = dateFormat.toLocaleDateString("en-us",options).replace(",", "");

            const id = event.monitoringStatistic.id;
            const pagesAdded = JSON.parse(event.pagesAdded);
            const pageAddedArray = pagesAdded.map((page) => {
                const pageUrl = page.url;

                return `<div class="domain_links">
                                            <a href="/page-information/?url=${page.crc32url}&token=${this.token}">${pageUrl}</a>
                                            <a class="external-link" href="${pageUrl}" target="_blank"><img src="/wp-content/themes/sitechecker/out/img/external-link.svg" alt="external-link" title="external-link"></a>
                                        </div>`
            }).join('\n');

            const timeLineNewPages =  `
                <div class="event-box">
                    <div class="event-box__header">
                        <div class="event-box__icon">
                            <img src="/wp-content/themes/sitechecker/out/img/event_add.svg" alt="added_pages" title="added_pages">
                        </div>
                        <div class="event-box__label">
                            <span>New pages</span>
                        </div>
                        <div class="event-box__secondary-section">
                            <div class="event-box__additional-info">
                                <span>${pagesAdded.length} pages</span>
                            </div>
                        </div>
                        <div class="event-box__dropdown-icon">
                            <i class="dropdown-icon--greyscale"></i>
                        </div>
                    </div>
                    <div class="event-box__hidden">
                        <div class="event-box__content">
                            ${pageAddedArray}
                        </div>
                    </div>
                </div>
            `;
            const pagesDeleted = JSON.parse(event.pagesDeleted);
            const pageDeletedArray = pagesDeleted.map((page) => {
                const pageUrl = page.url;

                return `<a href="${pageUrl}" target="_blank">${pageUrl}</a> `
            }).join('\n');
            const timeLineDeletedPages =  `
                <div class="event-box">
                    <div class="event-box__header">
                        <div class="event-box__icon">
                            <img src="/wp-content/themes/sitechecker/out/img/event_delete.svg" alt="deleted_pages" title="deleted_pages">
                        </div>
                        <div class="event-box__label">
                            <span>Deleted pages</span>
                        </div>
                        <div class="event-box__secondary-section">
                            <div class="event-box__additional-info">
                                <span>${pagesDeleted.length} pages</span>
                            </div>
                        </div>
                        <div class="event-box__dropdown-icon">
                            <i class="dropdown-icon--greyscale"></i>
                        </div>
                    </div>
                    <div class="event-box__hidden">
                        <div class="event-box__content">
                            ${pageDeletedArray}
                        </div>
                    </div>
                </div>
            `;

            const pagesChanged = JSON.parse(event.pagesChanged);

            function getPagesByChangeTitle(pagesChanged, changeTitle) {
                return pagesChanged.filter((page) => {
                    const pageChangeDataItem = page.change_data.find(pageChange => pageChange.param === changeTitle);
                    return pageChangeDataItem !== undefined;
                }).map((page) => {
                    const pageChangeDataItem = page.change_data.find(pageChange => pageChange.param === changeTitle);
                    return {
                        url: page.url,
                        crc32url: page.crc32url,
                        previous: pageChangeDataItem.previous,
                        changed: pageChangeDataItem.changed,
                        data: pageChangeDataItem.data
                    }
                })
            }

            let resultBlocks = "";

            if(pagesChanged.length > 0){
                const changeParams = [
                    {
                        paramTitle: 'title',
                        image: 'event_html',
                        title: 'Title has changed',
                        needsCheckForTitle: false,
                        changedElements: getPagesByChangeTitle(pagesChanged, 'title')
                    },
                    {
                        paramTitle: 'description',
                        image: 'event_html',
                        title: 'Description has changed',
                        needsCheckForTitle: false,
                        changedElements: getPagesByChangeTitle(pagesChanged, 'description')
                    },
                    {
                        paramTitle: 'h1',
                        image: 'event_html',
                        title: 'H1 has changed',
                        needsCheckForTitle: false,
                        changedElements: getPagesByChangeTitle(pagesChanged, 'h1')
                    },
                    {
                        paramTitle: 'http_status_code',
                        image: 'event_server',
                        title: 'HTTP status code has changed',
                        needsCheckForTitle: false,
                        changedElements: getPagesByChangeTitle(pagesChanged, 'http_status_code')
                    },
                    {
                        paramTitle: 'robots_txt_all',
                        image: 'event_server',
                        title: 'Index status has changed',
                        needsCheckForTitle: true,
                        falseTitle: 'URL allowed by robots.txt',
                        trueTitle: 'URL disallowed by robots.txt',
                        changedElements: getPagesByChangeTitle(pagesChanged, 'robots_txt_all')
                    },
                    {
                        paramTitle: 'robots_txt_content',
                        image: 'event_server',
                        title: 'Robots.txt has changed',
                        needsCheckForTitle: true,
                        falseTitle: 'URL disallowed',
                        trueTitle: 'URL allowed',
                        changedElements: getPagesByChangeTitle(pagesChanged, 'robots_txt_content')
                    },{
                        paramTitle: 'noindex',
                        image: 'event_server',
                        title: 'Index status has changed',
                        needsCheckForTitle: true,
                        falseTitle: 'URL indexation closed by noindex tag',
                        trueTitle: 'URL indexation opened by index tag',
                        changedElements: getPagesByChangeTitle(pagesChanged, 'noindex')
                    }
                ].filter(changeParam => {
                    return changeParam.changedElements.length > 0
                });

                resultBlocks = changeParams.map((changeParam ) => {
                    const changedElements = changeParam.changedElements;

                    const changedArray = changedElements.map((page) => {
                        let previous = page.changed,
                            changed = page.previous,
                            data = page.data,
                            stuff = [],
                            output = '',
                            array = '',
                            elementSring;

                        $(data).each(function(i, item){
                            array = item.text.split("\r\n");
                            $(array).each(function(i,element){
                                elementSring = {string:element, view:item.operation};
                                stuff.push(elementSring);
                            })
                        });

                        $(stuff).each(function(i, item) {
                            let elementString = item.string,
                                elementView = item.view;

                            if(elementView === "DELETE" && elementString !== ''){
                                output = output+'<li class="robots-delete"><span class="change">'+elementString+'</span></li>';
                            } else if(elementView === 'INSERT' && elementString !== ''){
                                output = output+'<li class="robots-added"><span class="change">'+elementString+'</span></li>';
                            } else if(elementString !== ''){
                                output = output+ '<li class="robots-current">'+elementString+'</li>';
                            }
                        });

                        if(data){
                            return `<div class="robots__changes">${output}</div>`;
                        } else{
                            if(changeParam.needsCheckForTitle === true){
                                changed = page.changed === false ? changeParam.falseTitle : changeParam.trueTitle ;
                                previous = page.previous === false ? changeParam.falseTitle : changeParam.trueTitle ;
                            }
                            return `<div class="event-box">
                                <div class="event-box__header">
                                    <div class="event-box__icon">
                                            <img src="/wp-content/themes/sitechecker/out/img/event_edit.svg" alt="edited_pages" title="edited_pages">
                                        </div>
                                    <div class="event-box__label">
                                        <div class="domain_links">
                                            <a href="/page-information/?url=${page.crc32url}&token=${this.token}">${page.url}</a>
                                            <a class="external-link" href="${page.url}" target="_blank"><img src="/wp-content/themes/sitechecker/out/img/external-link.svg" alt="external-link" title="external-link"></a>
                                        </div>
                                    </div>
                                </div>
                                <div class="separated-blocks">
                                    <div class="separated-blocks__block">
                                        <p class="labeled-value__title">New data</p>
                                        <div class="labeled-value__row">
                                            <p>${changed}</p>
                                        </div>
                                    </div>
                                    <div class="separated-blocks__block">
                                        <p class="labeled-value__title">Previous data</p>
                                        <div class="labeled-value__row">
                                            <p>${previous}</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        `;
                        }
                    });

                    const changeArrayHTML = changedArray.join('\n');

                    return  `
                    <div class="event-box">
                        <div class="event-box__header">
                            <div class="event-box__icon">
                                <img src="/wp-content/themes/sitechecker/out/img/${changeParam.image}.svg" alt="deleted_pages" title="deleted_pages">
                            </div>
                            <div class="event-box__label">
                                <span>${changeParam.title}</span>
                            </div>
                            <div class="event-box__secondary-section">
                                <div class="event-box__additional-info">
                                    <span>${changedArray.length} pages</span>
                                </div>
                            </div>
                            <div class="event-box__dropdown-icon">
                                <i class="dropdown-icon--greyscale"></i>
                            </div>
                        </div>
                        <div class="event-box__hidden">
                            <div class="event-box__content">
                                ${changeArrayHTML}
                            </div>
                        </div>
                    </div>
                `;
                }).join('\n');
            }

            let timeLineHTML,
                newPages,
                deletedPages;

            if (pageAddedArray.length !== 0) {
                newPages = `${timeLineNewPages}`;
            } else {
                newPages = ``;
            }

            if (pageDeletedArray.length !== 0) {
                deletedPages = `${timeLineDeletedPages}`;
            } else {
                deletedPages = ``;
            }

            if(pageAddedArray.length == 0 && pageDeletedArray.length == 0 && pagesChanged.length == 0){
                timeLineHTML = '';
            } else {
                timeLineHTML = '<div class="timeline-entry" id="'+id+'">' +
                    '<div class="timeline-entry__title"><span>'+date+'</span></div>' +
                    '<div class="timeline-entry__data">' +
                    '<div class="newPages">'+newPages+'</div>' +
                    '<div class="deletedPages">'+deletedPages+'</div>' +
                    '<div class="robots">'+resultBlocks+'</div>' +
                    '</div></div>';
            }
            return timeLineHTML;
        }

    }
}