/// <reference path="CrawlingBaseTable.ts"/>
/// <reference path="../../../../../../typings/globals/jquery/index.d.ts" />

namespace Crawling {

    export class MonitoringTable extends BaseTable {

        private token: string;
        private url: number;
        protected requestPath: string;

        constructor(apiPath: string, requestPath: string, token: string, url:number) {
            super(apiPath);
            this.token = token;
            this.requestPath = requestPath;
            this.sortParam = 'date_create';
            this.pageSize = 10;
            this.url = url;
        }

        protected processResult(response: any) {
            this.totalCount = response.totalCount;
            this.filteredCount = response.filteredCount;
            this.totalPages = response.pages;

            const events = response.summary;

            if (events.length > 0) {
                this.drawRows(events);
            }else if(events.length === 0 && response.pages === 0 ){
                $('#next').css('display','none');
                $('.empty_array').fadeIn();
            } else if(events.length === 0 && response.pages > 0){
                $('#next').css('display','none');
                const firstRow = `<div class="event-box first__entry">
                    <div class="event-box__header started"><div class="event-box__icon">
                    <img src="/wp-content/themes/sitechecker/out/img/event_start.svg" alt="deleted_pages" title="deleted_pages"></div>
                    <div class="event-box__label"><span>Sitechecker started monitoring this website</span></div></div></div>`;
                $('.monitoring_tab-first .timeline-entry:last-of-type').append(firstRow);
                $('.first__entry').fadeIn();
            }
        }

        nextPage(){
            if (this.page * this.pageSize < this.totalCount) {
                this.page++;
                this.display();
            } else{
                $('#next').css('display','none');
                const firstRow = `<div class="event-box first__entry">
                    <div class="event-box__header started"><div class="event-box__icon">
                    <img src="/wp-content/themes/sitechecker/out/img/event_start.svg" alt="deleted_pages" title="deleted_pages"></div>
                    <div class="event-box__label"><span>Sitechecker started monitoring this website</span></div></div></div>`;
                $('.monitoring_tab-first .timeline-entry:last-of-type').append(firstRow);
                $('.first__entry').fadeIn();
            }
        }

        protected getSortParam(): string {
            return 'weight';
        }

        protected getPagination() {
            return $('#pageTablePagination');
        }

        protected getTable() {
            return $('.monitoring_tab-first');
        }

        protected getTBody() {
            return this.getTable();
        }

        protected getCrawlingDate() {
            return $('#crawling_date');
        }

        public sort(column) {
            if (column === this.sortParam) {
                this.sortBy = this.sortBy === 'asc' ? 'desc' : 'asc';
            } else {
                this.sortBy = 'asc';
                this.sortParam = column;
            }

            this.display();
        };

        drawRows(events) {
            events.forEach((event) => {
                const row = this.renderEvent(event);
                this.addTableRow(row);
            });
            $('#next .fa').css('display','none');
        }

        logFailed(log): void {
            console.log("error" + log);
        }

        display() {
            this.getPageData().then(
                data => this.processResult(JSON.parse(data)),
                error => this.logFailed(error)
            )
        };

        protected getRequestData() {
            return {
                sortParam: this.sortParam,
                sortBy: this.sortBy,
                pageSize: this.pageSize,
                page: this.page,
                token: this.token,
                url: this.url,
            };
        }

        getPageData() {
            const request = this.getRequestData();

            //console.log(request);

            const data = JSON.stringify(request);

            return this.monitoringRequest(data);
        }

        monitoringRequest(data){
            return this.doRequest({
                path:this.requestPath,
                action: 'crawler_request',
                data: data,
                method: 'POST'
            })
        }

        renderEvent(event){
            let dateFormat = new Date(event.monitoringStatistic.dateCreate * 1000),
                options = {weekday: "long", year: "numeric", month: "long", day: "numeric", hour: '2-digit', minute: '2-digit'},
                date = dateFormat.toLocaleDateString("en-us",options).replace(",", "");

            const id = event.monitoringStatistic.id;

            const pagesAdded = JSON.parse(event.pagesAdded);
            const pageAddedArray = pagesAdded.map((page) => {
                const pageUrl = page.url;

                return `<a href="${pageUrl}" target="_blank">${pageUrl}</a> `
            }).join('\n');

            const timeLineNewPages =  `
                <div class="event-box">
                    <div class="event-box__header">
                        <div class="event-box__icon">
                            <img src="/wp-content/themes/sitechecker/out/img/event_add.svg" alt="added_pages" title="added_pages">
                        </div>
                        <div class="event-box__label">
                            <span>New pages</span>
                        </div>
                        <div class="event-box__secondary-section">
                            <div class="event-box__additional-info">
                                <span>${pagesAdded.length} pages</span>
                            </div>
                        </div>
                        <div class="event-box__dropdown-icon">
                            <i class="dropdown-icon--greyscale"></i>
                        </div>
                    </div>
                    <div class="event-box__hidden">
                        <div class="event-box__content">
                            ${pageAddedArray}
                        </div>
                    </div>
                </div>
            `;

            const pagesDeleted = JSON.parse(event.pagesDeleted);
            const pageDeletedArray = pagesDeleted.map((page) => {
                const pageUrl = page.url;

                return `<a href="${pageUrl}" target="_blank">${pageUrl}</a> `
            }).join('\n');

            const timeLineDeletedPages =  `
                <div class="event-box">
                    <div class="event-box__header">
                        <div class="event-box__icon">
                            <img src="/wp-content/themes/sitechecker/out/img/event_delete.svg" alt="deleted_pages" title="deleted_pages">
                        </div>
                        <div class="event-box__label">
                            <span>Deleted pages</span>
                        </div>
                        <div class="event-box__secondary-section">
                            <div class="event-box__additional-info">
                                <span>${pagesDeleted.length} pages</span>
                            </div>
                        </div>
                        <div class="event-box__dropdown-icon">
                            <i class="dropdown-icon--greyscale"></i>
                        </div>
                    </div>
                    <div class="event-box__hidden">
                        <div class="event-box__content">
                            ${pageDeletedArray}
                        </div>
                    </div>
                </div>
            `;

            const pagesChanged = JSON.parse(event.pagesChanged);

            const pagesChangedBlock = pagesChanged.map((page) => {
                let output = '';
                const pageChangesData = page.change_data.filter(pageChange => Object.keys(pageChange).length > 0);
                const pageChangesBlock = pageChangesData.map((pageChange) => {
                    let image,
                        title,
                        robots,
                        previous = pageChange.changed,
                        changed = pageChange.previous;
                    switch (pageChange.param) {
                        case 'title':
                            image = 'event_html';
                            title = 'Title has changed';
                            break;
                        case 'description':
                            image = 'event_html';
                            title = 'Description has changed';
                            break;
                        case 'h1':
                            image = 'event_html';
                            title = 'H1 has changed';
                            break;
                        case 'http_status_code':
                            image = 'event_server';
                            title = 'HTTP status code has changed';
                            break;
                        case 'robots_txt_all':
                            image = 'event_server';
                            title = 'Robots.txt has changed';
                            if(previous === false){
                                previous = 'URL disallowed';
                            } else {
                                previous = 'URL allowed';
                            }
                            if(changed === false){
                                changed = 'URL disallowed';
                            } else {
                                changed = 'URL allowed';
                            }
                            break;
                        case 'robots_txt_content':
                            image = 'event_server';
                            title = 'robots_txt_content';
                            robots = pageChange.data;
                            let stuff = [],
                                array = '',
                                elementSring;
                            $(robots).each(function(i, item){
                                array = item.text.split("\r\n");
                                $(array).each(function(i,element){
                                    elementSring = {string:element, view:item.operation};
                                    stuff.push(elementSring);
                                })
                            });
                            $(stuff).each(function(i, item){
                                let elementString = item.string,
                                    elementView = item.view;

                                if(elementView === "DELETE" && elementString !== ''){
                                    output = output+'<li class="robots-delete"><span class="change">'+elementString+'</span></li>';
                                } else if(elementView === 'INSERT' && elementString !== ''){
                                    output = output+'<li class="robots-added"><span class="change">'+elementString+'</span></li>';
                                } else if(elementString !== ''){
                                    output = output+ '<li class="robots-current">'+elementString+'</li>';
                                }
                            });
                            break;
                        case 'noindex':
                            image = 'event_server';
                            title = 'Index status has changed';
                            if(previous === true){
                                previous = 'URL indexation closed by noindex tag';
                            } else {
                                previous = 'URL indexation opened by index tag';
                            }
                            if(changed === true){
                                changed = 'URL indexation closed by noindex tag';
                            } else {
                                changed = 'URL indexation opened by index tag';
                            }
                            break;
                        default:
                            image = 'event_edit';
                            break;
                    }
                    return  `
                            <div class="event-box">
                                <div class="event-box__header">
                                    <div class="event-box__icon">
                                        <img src="/wp-content/themes/sitechecker/out/img/${image}.svg" alt="edited_pages" title="edited_pages">
                                    </div>
                                    <div class="event-box__label">
                                        <span>${title}</span>
                                    </div>
                                    <div class="event-box__dropdown-icon">
                                        <i class="dropdown-icon--greyscale"></i>
                                    </div>
                                </div>
                                <div class="event-box__hidden hidden_child">
                                    <div class="event-box__content">
                                        <div class="event-box__data">
                                            <div class="separated-blocks">
                                                <div class="separated-blocks__block">
                                                    <p class="labeled-value__title">New data</p>
                                                    <div class="labeled-value__row">
                                                        <p>${changed}</p>
                                                    </div>
                                                </div>
                                                <div class="separated-blocks__block">
                                                    <p class="labeled-value__title">Previous data</p>
                                                    <div class="labeled-value__row">
                                                        <p>${previous}</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        `;
                }).join('\n');

                if(output.length > 0){
                    return `
                     <div class="event-box">
                        <div class="event-box__header">
                            <div class="event-box__icon">
                                    <img src="/wp-content/themes/sitechecker/out/img/event_edit.svg" alt="edited_pages" title="edited_pages">
                                </div>
                            <div class="event-box__label">
                                <div><a href="${page.url}" target="_blank">${page.url}</a></div>
                            </div>
                            <div class="event-box__secondary-section"></div>
                            <div class="event-box__dropdown-icon">
                                <i class="dropdown-icon--greyscale"></i>
                            </div>
                            </div>
                        <div class="event-box__hidden hidden_parent">
                            <div class="event-box__content">
                                <ul class="robots__changes">${output}</ul>
                            </div>
                        </div>
                    </div>
                `;
                } else{
                    return `
                     <div class="event-box">
                        <div class="event-box__header">
                            <div class="event-box__icon">
                                    <img src="/wp-content/themes/sitechecker/out/img/event_edit.svg" alt="edited_pages" title="edited_pages">
                                </div>
                            <div class="event-box__label">
                                <div><a href="/page-information/?url=${page.crc32url}&token=${this.token}">${page.url}</a> <a href="${page.url}" target="_blank"><img src="/wp-content/themes/sitechecker/out/img/external-link.svg" alt="link" title="link"></a></div>
                            </div>
                            <div class="event-box__secondary-section">
                                <div class="event-box__additional-info">
                                    <span>${pageChangesData.length} changes</span>
                                </div>
                            </div>
                            <div class="event-box__dropdown-icon">
                                <i class="dropdown-icon--greyscale"></i>
                            </div>
                            </div>
                        <div class="event-box__hidden hidden_parent">
                            <div class="event-box__content">
                            ${pageChangesBlock}
                            </div>
                        </div>
                    </div>
                `;
                }
            }).join('\n');

            let timeLineHTML,
                newPages,
                deletedPages;

            if (pageAddedArray.length !== 0) {
                newPages = `${timeLineNewPages}`;
            } else {
                newPages = ``;
            }

            if (pageDeletedArray.length !== 0) {
                deletedPages = `${timeLineDeletedPages}`;
            } else {
                deletedPages = ``;
            }

            if(pageAddedArray.length == 0 && pageDeletedArray.length == 0 && pagesChanged.length == 0){
                timeLineHTML = '';
            } else {
                timeLineHTML = '<div class="timeline-entry" id="'+id+'">' +
                    '<div class="timeline-entry__title"><span>'+date+'</span></div>' +
                    '<div class="timeline-entry__data">' +
                    '<div>'+newPages+'</div>' +
                    '<div>'+deletedPages+'</div>' +
                    '<div>'+pagesChangedBlock+'</div>' +
                    '</div></div>';
            }
            return timeLineHTML;
        }

    }
}