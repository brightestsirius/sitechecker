/// <reference path="CrawlingBaseTable.ts"/>
/// <reference path="../../../../../../typings/globals/jquery/index.d.ts" />

namespace Crawling {

    export class MonitoringTableDesign extends BaseTable {

        private token: string;
        private url: number;
        protected requestPath: string;

        constructor(apiPath: string, requestPath: string, token: string, url:number) {
            super(apiPath);
            this.token = token;
            this.requestPath = requestPath;
            this.sortParam = 'date_create';
            this.pageSize = 10;
            this.url = url;
        }

        protected processResult(response: any) {
            this.totalCount = response.totalCount;
            this.filteredCount = response.filteredCount;
            this.totalPages = response.pages;

            const events = response.summary;

            if (events.length > 0) {
                $('.monitoring_graph').css('display','flex');
                this.drawRows(events);
            }else if(events.length === 0 && response.pages === 0 ){
                $('.empty_array').fadeIn();
            } else if(events.length === 0 && response.pages > 0){
                $('.nextData').css('display','none');
                const firstRow = `<div class="history__block-data first__entry">
                        <div class="top">
                            <img src="/wp-content/themes/sitechecker/out/img_design/monitoring__plus.svg" alt="new pages" title="new pages" class="status">
                            <p>Sitechecker started monitoring this website</p>
                        </div>
                    </div>`;
                $('#calendar .history__blocks .history__block:last-of-type').append(firstRow);
                $('#calendar .first__entry').fadeIn();
            }
        }

        nextPage(){
            if (this.page * this.pageSize < this.totalCount) {
                this.page++;
                this.display();
            }
        }

        protected getSortParam(): string {
            return 'weight';
        }

        protected getPagination() {
            return $('#pageTablePagination');
        }

        protected getTable() {
            return $('#calendar .history__blocks');
        }

        protected getTBody() {
            return this.getTable();
        }

        protected getCrawlingDate() {
            return $('#crawling_date');
        }

        public sort(column) {
            if (column === this.sortParam) {
                this.sortBy = this.sortBy === 'asc' ? 'desc' : 'asc';
            } else {
                this.sortBy = 'asc';
                this.sortParam = column;
            }

            this.display();
        };

        drawRows(events) {
            events.forEach((event) => {
                const row = this.renderEvent(event);
                this.addTableRow(row);
            });
            $('#nextData').fadeIn();
            $('.nextData .loader').css('display','none');
        }

        logFailed(log): void {
            console.log("error" + log);
        }

        display() {
            this.getPageData().then(
                data => this.processResult(JSON.parse(data)),
                error => this.logFailed(error)
            )
        };

        protected getRequestData() {
            return {
                sortParam: this.sortParam,
                sortBy: this.sortBy,
                pageSize: this.pageSize,
                page: this.page,
                token: this.token,
                url: this.url,
            };
        }

        getPageData() {
            const request = this.getRequestData();

            //console.log(request);

            const data = JSON.stringify(request);

            return this.monitoringRequest(data);
        }

        monitoringRequest(data){
            return this.doRequest({
                path:this.requestPath,
                action: 'crawler_request',
                data: data,
                method: 'POST'
            })
        }
        renderEvent(event){
            let dateFormat = new Date(event.monitoringStatistic.dateCreate * 1000),
                options = {weekday: "long", year: "numeric", month: "long", day: "numeric", hour: '2-digit', minute: '2-digit'},
                date = dateFormat.toLocaleDateString("en-us",options).replace(",", "");

            const id = event.monitoringStatistic.id;

            const pagesAdded = JSON.parse(event.pagesAdded);
            const pageAddedArray = pagesAdded.map((page) => {
                const pageUrl = page.url;
                return `<li><div class="links"><a href="${pageUrl}" class="internal_link">${pageUrl}</a></div></li>`
            }).join('\n');

            const timeLineNewPages =  `
                <div class="history__block-data added">
                    <div class="top">
                        <img src="/wp-content/themes/sitechecker/out/img_design/monitoring__plus.svg"
                             alt="new pages" title="new pages" class="status">
                        <p>${pagesAdded.length} new pages</p>
                        <div class="arrow">
                            <img src="/wp-content/themes/sitechecker/out/img_design/monitoring__arrow-down.svg"
                                 alt="arrow" title="arrow">
                        </div>
                    </div>
                    <div class="body">
                        <ul>
                            ${pageAddedArray}
                        </ul>
                    </div>
                </div>
            `;

            const pagesDeleted = JSON.parse(event.pagesDeleted);
            const pageDeletedArray = pagesDeleted.map((page) => {
                const pageUrl = page.url;

                return `<li><div><a href="${pageUrl}">${pageUrl}</a></div></li>`
            }).join('\n');

            const timeLineDeletedPages =  `
                <div class="history__block-data deleted">
                    <div class="top">
                        <img src="/wp-content/themes/sitechecker/out/img_design/monitoring__delete.svg"
                             alt="Deleted Pages" title="Deleted Pages" class="status">
                        <p>${pagesDeleted.length} deleted pages</p>
                        <div class="arrow">
                            <img src="/wp-content/themes/sitechecker/out/img_design/monitoring__arrow-down.svg"
                                 alt="arrow" title="arrow">
                        </div>
                    </div>
                    <div class="body">
                        <ul>
                            ${pageDeletedArray}
                        </ul>
                    </div>
                </div>
            `;

            const pagesChanged = JSON.parse(event.pagesChanged);

            const pagesChangedBlock = pagesChanged.map((page) => {
                let output = '';
                const pageChangesData = page.change_data.filter(pageChange => Object.keys(pageChange).length > 0);
                const pageChangesBlock = pageChangesData.map((pageChange,index) => {
                    let image,
                        title,
                        robots,
                        previous = pageChange.changed,
                        changed = pageChange.previous;
                    switch (pageChange.param) {
                        case 'title':
                            image = 'event_html';
                            title = 'Title has changed';
                            break;
                        case 'description':
                            image = 'event_html';
                            title = 'Description has changed';
                            break;
                        case 'h1':
                            image = 'event_html';
                            title = 'H1 has changed';
                            break;
                        case 'http_status_code':
                            image = 'event_server';
                            title = 'HTTP status code has changed';
                            break;
                        case 'robots_txt_all':
                            image = 'event_server';
                            title = 'Robots.txt has changed';
                            if(previous === false){
                                previous = 'URL disallowed';
                            } else {
                                previous = 'URL allowed';
                            }
                            if(changed === false){
                                changed = 'URL disallowed';
                            } else {
                                changed = 'URL allowed';
                            }
                            break;
                        case 'robots_txt_content':
                            image = 'event_server';
                            title = 'robots_txt_content';
                            robots = pageChange.data;
                            let stuff = [],
                                array = '',
                                elementSring;
                            $(robots).each(function(i, item){
                                array = item.text.split("\r\n");
                                $(array).each(function(i,element){
                                    elementSring = {string:element, view:item.operation};
                                    stuff.push(elementSring);
                                })
                            });
                            $(stuff).each(function(i, item){
                                let elementString = item.string,
                                    elementView = item.view,
                                    prevString, prevView,
                                    nextString, nextView,
                                    secondNextString;

                                if(i !== 0){
                                    prevString = stuff[i-1].string,
                                        prevView = stuff[i-1].view;
                                }

                                if(i !== stuff.length-1){
                                    nextString = stuff[i+1].string,
                                        nextView = stuff[i+1].view;
                                }

                                if(i !== stuff.length-2 && i !== stuff.length-1){
                                    secondNextString = stuff[i+2].string;
                                }

                                if(elementView === 'EQUAL' && nextView === 'DELETE' || elementView === 'DELETE' || elementView === 'INSERT' || prevView === 'INSERT'){

                                } else{
                                    output = output+ '<li class="robots-current">'+elementString+'</li>';
                                }

                                if(item.view === "DELETE"){
                                    output = output+'<li class="robots-delete"><span class="prev">'+prevString+'</span><span class="change">'+elementString+'</span><span class="next">'+secondNextString+'</span></li><li class="robots-added"><span class="prev">'+prevString+'</span><span class="change">'+nextString+'</span><span class="next">'+secondNextString+'</span></li>';
                                }
                            });
                            break;
                        case 'noindex':
                            image = 'event_server';
                            title = 'Index status has changed';
                            if(previous === true){
                                previous = 'URL indexation closed by noindex tag';
                            } else {
                                previous = 'URL indexation opened by index tag';
                            }
                            if(changed === true){
                                changed = 'URL indexation closed by noindex tag';
                            } else {
                                changed = 'URL indexation opened by index tag';
                            }
                            break;
                        default:
                            image = 'event_edit';
                            break;
                    }
                    return  `
                        <div class="history__block-data change__child">
                            <div class="top">
                                <img src="/wp-content/themes/sitechecker/out/img_design/monitoring__edit.svg"
                                     alt="Changed Pages" title="Changed Pages" class="status">
                                <div class="links"><p>${title}</p></div>
                                <div class="arrow">
                                    <img src="/wp-content/themes/sitechecker/out/img_design/monitoring__arrow-down.svg"
                                         alt="arrow" title="arrow">
                                </div>
                            </div>
                            <div class="body">
                                <div class="event__changes">
                                    <div class="changes">
                                        <p>New data:</p>
                                        <span>${changed}</span>
                                        <img src="/wp-content/themes/sitechecker/out/img_design/changed__status.svg"
                                             alt="Changed status" title="Changed status" class="status">
                                        <p>Previous data:</p>
                                        <span>${previous}</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        `;
                }).join('\n');

                if(output.length > 0){
                    return `
                    <div class="history__block-data changed">
                        <div class="top">
                            <img src="/wp-content/themes/sitechecker/out/img_design/monitoring__edit.svg"
                                 alt="Changed Pages" title="Changed Pages" class="status">
                            <div class="links">
                                <a href="${page.url}" class="internal_link">${page.url}</a><a
                                        href="${page.url}"
                                        target="_blank" class="external_link"><img
                                            src="/wp-content/themes/sitechecker/out/img_design/external.svg"
                                            alt="external" title="external"></a>
                            </div>
                            <div class="arrow">
                                <img src="/wp-content/themes/sitechecker/out/img_design/monitoring__arrow-down.svg"
                                     alt="arrow" title="arrow">
                            </div>
                        </div>
                        <div class="body">
                            <ul class="robots__changes">${output}</ul>
                        </div>
                    </div>
                `;
                } else{
                    return `
                    <div class="history__block-data changed">
                        <div class="top">
                            <img src="/wp-content/themes/sitechecker/out/img_design/monitoring__edit.svg"
                                 alt="Changed Pages" title="Changed Pages" class="status">
                            <div class="links">
                                <a href="/page-information/?url=${page.crc32url}&token=${this.token}" class="internal_link">${page.url}</a><a
                                        href="${page.url}"
                                        target="_blank" class="external_link"><img
                                            src="/wp-content/themes/sitechecker/out/img_design/external.svg"
                                            alt="external" title="external"></a>
                            </div>
                            <div class="changes-count">${pageChangesData.length} changes</div>
                            <div class="arrow">
                                <img src="/wp-content/themes/sitechecker/out/img_design/monitoring__arrow-down.svg"
                                     alt="arrow" title="arrow">
                            </div>
                        </div>
                        <div class="body">
                            ${pageChangesBlock}
                        </div>
                    </div>
                `;
                }
            }).join('\n');

            let timeLineHTML,
                newPages,
                deletedPages,
                changesPages;

            if (pageAddedArray.length !== 0) {
                newPages = timeLineNewPages;
            } else {
                newPages = ``;
            }

            if (pageDeletedArray.length !== 0) {
                deletedPages = timeLineDeletedPages;
            } else {
                deletedPages = ``;
            }

            if (pagesChanged.length !== 0) {
                changesPages = pagesChangedBlock;
            } else {
                changesPages = ``;
            }

            if(pageAddedArray.length == 0 && pageDeletedArray.length == 0 && pagesChanged.length == 0){
                timeLineHTML = '';
            } else {
                timeLineHTML = '<div class="history__block" id="'+id+'">' +
                '<p class="date">'+date+'</p>'+newPages+deletedPages+changesPages+'</div>';
            }
            return timeLineHTML;
        }
    }
}