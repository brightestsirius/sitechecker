/// <reference path="CrawlingBaseTable.ts"/>
/// <reference path="../../../../../../typings/globals/jquery/index.d.ts" />

namespace Crawling {

    export class MonitoringTableForPage extends BaseTable {

        private token: string;
        private url: number;
        protected requestPath: string;

        constructor(apiPath: string, requestPath: string, token: string, url: number) {
            super(apiPath);
            this.token = token;
            this.url = url;
            this.requestPath = requestPath;
            this.pageSize = 10;
        }

        protected processResult(response: any) {
            console.log(response);
            this.totalCount = response.totalCount;
            this.filteredCount = response.filteredCount;
            this.totalPages = response.pages;
            const events = response.summary;
            this.drawRows(events);
        }

        nextPage(){
            if (this.page * this.pageSize < this.totalCount) {
                this.page++;
                this.display();
            } else{
                $('#next-second').css('display','none');
                $('.empty_array').fadeIn();
            }
        }

        protected getSortParam(): string {
            return 'date_create';
        }

        protected getPagination() {
            return $('#pageTablePagination');
        }

        protected getTable() {
            return $('.monitoring_tab-second');
        }

        protected getTBody() {
            return this.getTable();
        }

        protected getCrawlingDate() {
            return $('#crawling_date');
        }

        public sort(column) {
            if (column === this.sortParam) {
                this.sortBy = 'desc';
            } else {
                this.sortBy = 'desc';
                this.sortParam = column;
            }

            this.display();
        };

        drawRows(events) {
            if(events.length == 0){
                $('#next-second').css('display','none');
                $('.empty_array').fadeIn();
            } else{
                $('#next-second').css('display','flex');
            }
            events.forEach((event) => {
                const row = this.renderEvent(event);
                this.addTableRow(row);
            });
            $('#next-second .fa').css('display','none');
        }

        logFailed(log): void {
            console.log("error" + log);
        }

        display() {
            this.getPageData().then(
                data => this.processResult(JSON.parse(data)),
                error => this.logFailed(error)
            )
        };

        protected getRequestData() {
            return {
                sortParam: this.sortParam,
                sortBy: this.sortBy,
                pageSize: this.pageSize,
                page: this.page,
                token: this.token,
                url: this.url,
            };
        }

        getPageData() {
            const request = this.getRequestData(),
                data = JSON.stringify(request);
            return this.monitoringRequest(data);
        }

        monitoringRequest(data){
            return this.doRequest({
                path:this.requestPath,
                action: 'crawler_request',
                data: data,
                method: 'POST'
            })
        }

        renderEvent(event){
            let dateFormat = new Date(event.monitoringStatistic.dateCreate * 1000),
                date = dateFormat.toGMTString().split(' ', 5).join(' ');

            const pagesChanged = JSON.parse(event.changeData);
            let resultBlocks = pagesChanged.filter((page) => {
                const pageChangeDataItem = page.param;
                return pageChangeDataItem !== undefined;
            }).map((page) => {
                const pageParam = page.param;
                let image,
                    title,
                    pageChanged = page.changed,
                    pagePrevious = page.previous;

                switch (pageParam) {
                        case 'title':
                            image = 'event_html';
                            title = 'Title has changed';
                            break;
                        case 'description':
                            image = 'event_html';
                            title = 'Description has changed';
                            break;
                        case 'h1':
                            image = 'event_html';
                            title = 'H1 has changed';
                            break;
                        case 'http_status_code':
                            image = 'event_server';
                            title = 'HTTP status code has changed';
                            break;
                        case 'robots_txt_all':
                            image = 'event_server';
                            title = 'Robots.txt has changed';
                            if(pageChanged === false){
                                pageChanged = 'URL disallowed';
                            } else {
                                pageChanged = 'URL allowed';
                            }
                            if(pagePrevious === false){
                                pagePrevious = 'URL disallowed';
                            } else {
                                pagePrevious = 'URL allowed';
                            }
                            break;
                        case 'noindex':
                            image = 'event_server';
                            title = 'Index status has changed';
                            if(pagePrevious === true){
                                pagePrevious = 'URL indexation closed by noindex tag';
                            } else {
                                pagePrevious = 'URL indexation opened by index tag';
                            }
                            if(pageChanged === true){
                                pageChanged = 'URL indexation closed by noindex tag';
                            } else {
                                pageChanged = 'URL indexation opened by index tag';
                            }
                            break;
                        default:
                            image = 'event_edit';
                            break;
                    }

                return `<div class="event-box domain_monitoring">
                            <div class="event-box__head">
                                <div class="event-box__icon">
                                    <img src="/wp-content/themes/sitechecker/out/img/${image}.svg" alt="edited_pages" title="edited_pages">
                                </div>
                                <div class="event-box__label">
                                    <span>${title}</span>
                                </div>
                                <div class="event-box__dropdown-icon">
                                    <i class="dropdown-icon--greyscale"></i>
                                </div>
                            </div>
                            <div class="event-box__hidden hidden_child">
                                <div class="event-box__content">
                                    <div class="event-box__data">
                                        <div class="separated-blocks">
                                            <div class="separated-blocks__block">
                                                <p class="labeled-value__title">New data</p>
                                                <div class="labeled-value__row">
                                                    <p>${pagePrevious}</p>
                                                </div>
                                            </div>
                                            <div class="separated-blocks__block">
                                                <p class="labeled-value__title">Previous data</p>
                                                <div class="labeled-value__row">
                                                    <p>${pageChanged}</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>`;
            }).join('\n');

            let timeLineHTML = '<div class="timeline-entry">' +
                '<div class="timeline-entry__title"><span>'+date+'</span></div>' +
                '<div class="robots">'+resultBlocks+'</div>' +
                '</div>';
            return timeLineHTML;
        }
    }
}