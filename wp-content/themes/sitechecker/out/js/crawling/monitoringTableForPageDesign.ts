/// <reference path="CrawlingBaseTable.ts"/>
/// <reference path="../../../../../../typings/globals/jquery/index.d.ts" />

namespace Crawling {

    export class MonitoringTableForPageDesign extends BaseTable {

        private token: string;
        private url: number;
        protected requestPath: string;

        constructor(apiPath: string, requestPath: string, token: string, url: number) {
            super(apiPath);
            this.token = token;
            this.url = url;
            this.requestPath = requestPath;
            this.pageSize = 10;
        }

        protected processResult(response: any) {
            console.log(response);
            this.totalCount = response.totalCount;
            this.filteredCount = response.filteredCount;
            this.totalPages = response.pages;
            const events = response.summary;
            this.drawRows(events);
        }

        nextPage(){
            if (this.page * this.pageSize < this.totalCount) {
                this.page++;
                this.display();
            } else{
                $('.load__more').css('display','none');
                $('.monitoring__history-point.started').fadeIn();
            }
        }

        protected getSortParam(): string {
            return 'date_create';
        }

        protected getPagination() {
            return $('#pageTablePagination');
        }

        protected getTable() {
            return $('.history__block');
        }

        protected getTBody() {
            return this.getTable();
        }

        protected getCrawlingDate() {
            return $('#crawling_date');
        }

        public sort(column) {
            if (column === this.sortParam) {
                this.sortBy = 'desc';
            } else {
                this.sortBy = 'desc';
                this.sortParam = column;
            }

            this.display();
        };

        drawRows(events) {
            if(events.length == 0){
                $('.load__more').css('display','none');
                $('.monitoring__history-point.started').fadeIn();
            } else{
                $('.load__more').css('display','flex');
            }
            events.forEach((event) => {
                const row = this.renderEvent(event);
                this.addTableRow(row);
            });
        }

        logFailed(log): void {
            console.log("error" + log);
        }

        display() {
            this.getPageData().then(
                data => this.processResult(JSON.parse(data)),
                error => this.logFailed(error)
            )
        };

        protected getRequestData() {
            return {
                sortParam: this.sortParam,
                sortBy: this.sortBy,
                pageSize: this.pageSize,
                page: this.page,
                token: this.token,
                url: this.url,
            };
        }

        getPageData() {
            const request = this.getRequestData(),
                data = JSON.stringify(request);
            return this.monitoringRequest(data);
        }

        monitoringRequest(data){
            return this.doRequest({
                path:this.requestPath,
                action: 'crawler_request',
                data: data,
                method: 'POST'
            })
        }

        renderEvent(event){
            const weekday = ["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"],
                monthNames = ["January", "February", "March", "April", "May", "June",
                    "July", "August", "September", "October", "November", "December"
                ];

            function formatAMPM(date) {
                let hours = date.getHours(),
                    minutes = date.getMinutes(),
                    ampm = hours >= 12 ? 'pm' : 'am';
                hours = hours % 12;
                hours = hours ? hours : 12;
                minutes = minutes < 10 ? '0'+minutes : minutes;
                return hours + ':' + minutes + ' ' + ampm;
            }

            let dateFormat = new Date(event.dateCreate * 1000),
                date = formatAMPM(dateFormat),
                dateDay = weekday[dateFormat.getDay()]+' '+monthNames[dateFormat.getMonth()]+' '+dateFormat.getDate()+', '+dateFormat.getFullYear();

            const pagesChanged = JSON.parse(event.changeData);
            let resultBlocks = pagesChanged.filter((page) => {
                const pageChangeDataItem = page.param;
                return pageChangeDataItem !== undefined;
            }).map((page) => {
                const pageParam = page.param;
                let image,
                    title,
                    pageChanged = page.changed,
                    pagePrevious = page.previous;

                if(pageChanged === ''){
                    pageChanged = '<p class="message__error">No '+pageParam+'</p>'
                }
                if(pagePrevious === ''){
                    pagePrevious = '<p class="message__error">No '+pageParam+'</p>'
                }

                switch (pageParam) {
                    case 'title':
                        image = 'event_html';
                        title = 'Title has changed';
                        break;
                    case 'description':
                        image = 'event_html';
                        title = 'Description has changed';
                        break;
                    case 'h1':
                        image = 'event_html';
                        title = 'H1 has changed';
                        break;
                    case 'http_status_code':
                        image = 'event_server';
                        title = 'HTTP status code has changed';
                        break;
                    case 'robots_txt_all':
                        image = 'event_server';
                        title = 'Robots.txt has changed';
                        if(pageChanged === false){
                            pageChanged = 'URL disallowed';
                        } else {
                            pageChanged = 'URL allowed';
                        }
                        if(pagePrevious === false){
                            pagePrevious = 'URL disallowed';
                        } else {
                            pagePrevious = 'URL allowed';
                        }
                        break;
                    case 'noindex':
                        image = 'event_server';
                        title = 'Index status has changed';
                        if(pagePrevious === true){
                            pagePrevious = 'URL indexation closed by noindex tag';
                        } else {
                            pagePrevious = 'URL indexation opened by index tag';
                        }
                        if(pageChanged === true){
                            pageChanged = 'URL indexation closed by noindex tag';
                        } else {
                            pageChanged = 'URL indexation opened by index tag';
                        }
                        break;
                    default:
                        image = 'event_edit';
                        break;
                }

                return `<div class="event-box domain_monitoring">
                            <div class="header"><img src="/wp-content/themes/sitechecker/out/img_design/monitoring_status.svg" alt="status" title="${title}">${title}<p>${date}</p>
                            <svg width="9" height="5" viewBox="0 0 9 5" fill="none" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" clip-rule="evenodd" d="M4.499 0L9 2.5V4.896L4.499 2.38L0 5V2.5L4.499 0Z" transform="translate(9 5) rotate(-180)" fill="#cccccc"></path></svg></div>
                            <div class="body">
                                <div class="container">
                                    <div class="changes">
                                        <p class="${image}">${pagePrevious}</p>
                                        <img src="/wp-content/themes/sitechecker/out/img_design/arrow_monitoring.svg" alt="arrow" title="arrow">
                                        <p class="${image}">${pageChanged}</p>
                                    </div>
                                </div>
                            </div>
                        </div>`;
            }).join('\n');

            let timeLineHTML = '',
                historyPoint = $('.monitoring__history-point[name="'+dateDay+'"]');
            if(historyPoint.length >0){
                historyPoint.find('.monitoring__history-event').append(resultBlocks);
            }else{
                timeLineHTML = '<div class="monitoring__history-point" name="'+dateDay+'">' +
                    '<p class="history-point-date">'+dateDay+'</p>' +
                    '<div class="monitoring__history-event">'+resultBlocks+'</div></div>';
            }
            $('.monitoring__history-content').fadeIn();
            return timeLineHTML;
        }
    }
}