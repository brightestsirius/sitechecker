/// <reference path="../popup/IncomingPopupTable.ts"/>
namespace Crawling.Popup{
    import IncomingPopupTable = Crawling.Popup.IncomingPopupTable;

    export class IncomingTable extends IncomingPopupTable {

        constructor(apiPath: string, url: string, token: string) {
            super(apiPath, url, token);
        }

        setPagination() {
            const html = `
                <div>
                    <div class="dataTables_info" id="pageTable_info" role="status" aria-live="polite">
                        Showing ${((this.page * this.pageSize) - this.pageSize)} to ${this.page * this.pageSize > this.totalCount ? this.totalCount : this.page * this.pageSize}  of ${this.totalCount} entries
                    </div>
                </div>
                <div>
                    <div class="dataTables_paginate paging_simple_numbers" id="pageTable_paginate">
                        <ul class="pagination">
                            <li class="page-item disabled">
                                <span>Page ${this.page} from ${this.totalPages}</span>
                            </li>
                            <li class="paginate_button page-item previous ${ this.page > 1 ? '' : 'disabled'}">
                                <a href="javascript:void(0);" aria-controls="pageTable" class="page-link" ${this.page > 1 ? `onclick="popoupTable.previousPage();"` : ''}  >Previous</a>
                            </li>
                            <li class="paginate_button page-item ${this.page < this.totalPages ? '' : 'disabled'}">
                                <a href="javascript:void(0);" aria-controls="pageTable"  class="page-link" ${this.page < this.totalPages ? `onclick="popoupTable.nextPage()"` : ''} >Next</a>
                            </li>
                        </ul>
                    </div>
                </div>
            `;

            this.getPagination().html(html);
        }
    }
}