/// <reference path="PopupTable.ts"/>
namespace Crawling.Popup{
    export class ExternalPopupTable extends PopupTable {



        constructor(apiPath: string, url: string, token: string) {
            super(apiPath, '/getExternalLinks', url, token);
            $('.popup__export').click(function () {
                exportLinks('external', url, token);
            });
            $('#popup_table tbody').empty();
        }

        public getSortParam(): string {
            return 'url';
        }

        drawRows(rows) {
            $('.crawler_popup-title').text('External links');
            $('#overview__popup').removeClass('duplicate__popup').fadeIn().css('display','flex');
            rows.forEach((row, index) => {
                const rowData = this.getPopupRowDataHTML(row, index);
                const tr = `
                    <tr id="popup_row_${index}" >
                        <td>
                            <div class="external-link">
                            <a href="/page-information/?url=${row.url}&token=${this.token}">${row.url}</a>
                            <a href="${row.url}" target="_blank"><img src="/wp-content/themes/sitechecker/out/img_design/external.svg" alt="external" title="external"></a>
                            </div>
                        </td>
                        <td>${row.link_anchor || '-'}</td>
                    </tr>
                    ${rowData}
                `;
                this.addTableRow(tr);
            });

            $('.additional_param').hide();//если ссылки внутренние дополнительные колонки не нужны
        }
    }
}