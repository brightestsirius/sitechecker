/// <reference path="PopupTable.ts"/>

namespace Crawling.Popup{

    export class IncomingPopupTable extends PopupTable {

        constructor(apiPath: string, url: string, token: string) {
            super(apiPath , '/getIncomingLinks', url, token);
            $('.popup__export').click(function () {
                exportLinks('incoming', url, token);
            });
            $('#popup_table tbody').empty();
        }

        getSortParam(): string{
            return 'weight';
        }

        drawRows(rows) {
            $('.crawler_popup-title').text('Anchors');
            $('#overview__popup').removeClass('duplicate__popup').fadeIn().css('display','flex');
            rows.forEach((row, index) => {
                const rowData = this.getPopupRowDataHTML(row, index);
                const tr = `
                    <tr id="popup_row_${index}" >
                        <td>
                            <div class="external-link">
                            <a href="/page-information/?url=${row.url}&token=${this.token}">${row.url}</a>
                            <a href="${row.url}" target="_blank"><img src="/wp-content/themes/sitechecker/out/img_design/external.svg" alt="external" title="external"></a>
                            </div>
                        </td>
                        <td>${row.link_anchor || '-'}</td>
                        <td>${parseInt(row.normalized_weight)}</td>
                        <td>
                            <span style="color: ${this.getStatusCodeIcon(parseInt(row.http_status_code))}">${row.http_status_code}</span>
                        </td>
                    </tr>
                    ${rowData}
                `;

                $('.additional_param').show();

                this.addTableRow(tr);
            });
        }
    }
}
