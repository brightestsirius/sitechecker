var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var Crawling;
(function (Crawling) {
    var Popup;
    (function (Popup) {
        var PopupTable = (function (_super) {
            __extends(PopupTable, _super);
            function PopupTable(apiPath, url) {
                var _this = _super.call(this, apiPath) || this;
                _this.url = url;
                _this.sortParam = 'url';
                _this.getPopup().show();
                _this.display();
                return _this;
            }
            PopupTable.prototype.getPopup = function () {
                return $('#popup');
            };
            PopupTable.prototype.getLoader = function () {
                return $('#popupLoader');
            };
            PopupTable.prototype.sort = function (column) { };
            PopupTable.prototype.getPagination = function () {
                return $('#popup_pagination');
            };
            PopupTable.prototype.getTableElements = function () {
                return $('.popup_table_elements');
            };
            PopupTable.prototype.getTable = function () {
                return $('#popupTable');
            };
            PopupTable.prototype.getTBody = function () {
                return this.getTable().find("tbody");
            };
            PopupTable.prototype.getCrawlingDate = function () {
                return $('#popupCrawlingDate');
            };
            PopupTable.prototype.setPagination = function () {
                var html = "\n                <div class=\"col-sm-5\">\n                    <div class=\"dataTables_info\" id=\"pageTable_info\" role=\"status\" aria-live=\"polite\">\n                        Showing " + ((this.page * this.pageSize) - this.pageSize) + " to " + (this.page * this.pageSize > this.totalCount ? this.totalCount : this.page * this.pageSize) + "  of " + this.totalCount + " entries\n                    </div>\n                </div>\n                <div class=\"col-sm-7\">\n                    <div class=\"dataTables_paginate paging_simple_numbers\" id=\"pageTable_paginate\">\n                        <ul class=\"pagination\">\n                            <li class=\"page-item disabled\">\n                                <span>Page " + this.page + " from " + this.totalPages + "</span>\n                            </li>\n                            <li class=\"paginate_button page-item previous " + (this.page > 1 ? '' : 'disabled') + "\">\n                                <a href=\"javascript:void(0);\" aria-controls=\"pageTable\" class=\"page-link\" " + (this.page > 1 ? "onclick=\"popupTable.previousPage();\"" : '') + "  >Previous</a>\n                            </li>\n                            <li class=\"paginate_button page-item " + (this.page < this.totalPages ? '' : 'disabled') + "\">\n                                <a href=\"javascript:void(0);\" aria-controls=\"pageTable\"  class=\"page-link\" " + (this.page < this.totalPages ? "onclick=\"popupTable.nextPage()\"" : '') + " >Next</a>\n                            </li>\n                        </ul>\n                    </div>\n                </div>\n            ";
                this.getPagination().html(html);
            };
            ;
            PopupTable.prototype.getPopupRowDataHTML = function (data, index) {
                var content = Object.keys(data).map(function (key) {
                    var value = data[key];
                    return "\n                <div class=\"col-xs-12 col-sm-6 col-lg-4\">\n                    <div class=\"detail_title\">" + key + ": " + value + "</div>\n                </div>\n            ";
                }).reduce(function (previousValue, currentValue) {
                    return previousValue + currentValue;
                });
                return "\n            <tr id=\"popup_row_data_" + index + "\" style=\"display: none\">\n                <td colspan=\"6\">\n                   <div class=\"container-fluid\">\n                        " + content + "\n                    </div>\n                </td>\n            </tr>\n        ";
            };
            PopupTable.prototype.getRequestData = function () {
                return this.addFilters({
                    'url': this.url,
                    'sortParam': this.sortParam,
                    'sortBy': this.sortBy,
                    'pageSize': this.pageSize,
                    'page': this.page
                });
            };
            PopupTable.prototype.close = function () {
                this.getPopup().hide();
            };
            PopupTable.prototype.toggleRowData = function (index) {
                var row = $('#popup_row_' + index);
                var rowIcon = row.find('.icon');
                var rowData = $('#popup_row_data_' + index);
                if (row.hasClass("dataShow")) {
                    row.removeClass('dataShow');
                    rowIcon.removeClass("fa-minus-circle");
                    rowIcon.addClass("fa-plus-circle");
                    rowIcon.css("color", "green");
                    rowData.hide();
                }
                else {
                    row.addClass("dataShow");
                    rowIcon.removeClass("fa-plus-circle");
                    rowIcon.addClass("fa-minus-circle");
                    rowIcon.css("color", "red");
                    rowData.show();
                }
            };
            PopupTable.prototype.parseCrawledDate = function (_a) {
                var timestamp = _a.checked;
                return new Date(timestamp * 1000);
            };
            PopupTable.prototype.addFilters = function (request) {
                return request;
            };
            return PopupTable;
        }(Crawling.BaseTable));
        Popup.PopupTable = PopupTable;
    })(Popup = Crawling.Popup || (Crawling.Popup = {}));
})(Crawling || (Crawling = {}));
