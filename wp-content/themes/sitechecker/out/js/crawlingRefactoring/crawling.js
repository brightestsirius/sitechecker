var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var Crawling;
(function (Crawling) {
    var BaseTable = /** @class */ (function () {
        function BaseTable(baseUrls) {
            this.pageSize = 50;
            this.sortBy = 'desc';
            this.page = 1;
            this.totalPages = 0;
            this.totalCount = 0;
            this.filteredCount = 0;
            this.baseUrls = baseUrls;
            this.sortParam = this.getSortParam();
        }
        BaseTable.prototype.init = function () {
            this.display();
        };
        BaseTable.prototype.nextPage = function () {
            if (this.page * this.pageSize < this.totalCount) {
                this.page++;
                this.display();
            }
        };
        BaseTable.prototype.previousPage = function () {
            if (this.page > 1) {
                this.page--;
                this.display();
            }
        };
        BaseTable.prototype.changePageSize = function (pageSize) {
            this.pageSize = pageSize;
            this.page = 1;
            this.display();
        };
        BaseTable.prototype.doRequest = function (data) {
            if (data === void 0) { data = {}; }
            return $.post(this.getUrlForRequest(), data);
        };
        BaseTable.prototype.addTableRow = function (tr) {
            this.getTBody().append(tr);
        };
        return BaseTable;
    }());
    Crawling.BaseTable = BaseTable;
})(Crawling || (Crawling = {}));
/// <reference path="baseTable.ts"/>
var Crawling;
(function (Crawling) {
    var CrawlingBaseTable = /** @class */ (function (_super) {
        __extends(CrawlingBaseTable, _super);
        function CrawlingBaseTable(baseUrls, requestPath, token) {
            var _this = _super.call(this, baseUrls) || this;
            _this.token = token;
            _this.requestPath = requestPath;
            return _this;
        }
        CrawlingBaseTable.prototype.getLoader = function () {
            return $('.loader');
        };
        CrawlingBaseTable.prototype.processResult = function (data) {
            var result = data[0];
            var message = result.message, summary = result.summary;
            if (message !== 'success') {
                this.getCrawlingDate().css("color", "red");
                this.getCrawlingDate().html("Crawling in progress");
            }
            else {
                this.totalCount = result.totalCount;
                this.filteredCount = result.filteredCount;
                this.totalPages = result.pages;
                this.setPagination();
                this.getTBody().empty();
                if (summary.length > 0) {
                    this.drawRows(summary);
                }
                else {
                    $('#popup_table').css('display', 'table');
                    this.addTableRow("\n                        <tr>\n                            <td colspan=\"5\">\n                                <p class=\"no__data\">No data</p>\n                            </td>\n                        </tr>\n                    ");
                }
            }
            this.getTableElements().show();
            this.hideLoader();
        };
        ;
        CrawlingBaseTable.prototype.hideLoader = function () {
            this.getLoader().css('opacity', '0');
        };
        CrawlingBaseTable.prototype.getStatusCodeWithColor = function (statusCode) {
            var color = 'red';
            if (statusCode >= 200 && statusCode < 300) {
                color = "#6fc66f";
            }
            else if (statusCode > 300 && statusCode < 400) {
                color = "#f7a72c";
            }
            return color;
        };
        CrawlingBaseTable.prototype.getStatusCodeIcon = function (statusCode) {
            if (statusCode >= 200 && statusCode < 300) {
                return "#56c504";
            }
            else if (statusCode > 300 && statusCode < 400) {
                return "#fd9425";
            }
            else {
                return "#f24c27";
            }
        };
        CrawlingBaseTable.prototype.logFailed = function (log) {
            this.getCrawlingDate().css("color", "red");
            this.getCrawlingDate().html("Failed to get data");
            console.log(log);
        };
        CrawlingBaseTable.prototype.display = function () {
            var _this = this;
            this.getLoader().css('opacity', '1');
            this.getPageData().then(function (data) { return _this.processResult(data); }, function (error) { return _this.logFailed(error); });
        };
        ;
        CrawlingBaseTable.prototype.getPageData = function () {
            var request = {
                'data': [
                    this.getRequestData()
                ]
            };
            var data = JSON.stringify(request);
            return this.crawlerRequest(data);
        };
        CrawlingBaseTable.prototype.crawlerRequest = function (data) {
            return this.doRequest({
                path: this.requestPath,
                data: data,
                method: 'POST'
            });
        };
        CrawlingBaseTable.prototype.getUrlForRequest = function () {
            return this.baseUrls.crawlRequest;
        };
        return CrawlingBaseTable;
    }(Crawling.BaseTable));
    Crawling.CrawlingBaseTable = CrawlingBaseTable;
})(Crawling || (Crawling = {}));
/// <reference path="CrawlingBaseTable.ts"/>
/// <reference path="../../../../../../typings/globals/jquery/index.d.ts" />
var Crawling;
(function (Crawling) {
    var CrawlingDomainTable = /** @class */ (function (_super) {
        __extends(CrawlingDomainTable, _super);
        function CrawlingDomainTable(baseUrls, availablePagesLimit) {
            var _this = _super.call(this, baseUrls) || this;
            _this.availablePagesLimit = availablePagesLimit;
            return _this;
        }
        CrawlingDomainTable.prototype.getPageData = function () {
            return this.doRequest(this.getRequestData());
        };
        CrawlingDomainTable.prototype.processResult = function (response) {
            var data = response.data;
            this.getTBody().empty();
            if (data.length > 0) {
                this.drawRows(data);
            }
        };
        CrawlingDomainTable.prototype.getSortParam = function () {
            return 'weight';
        };
        CrawlingDomainTable.prototype.getPagination = function () {
            return $('#pageTablePagination');
        };
        CrawlingDomainTable.prototype.getTable = function () {
            return $('#domainsTable');
        };
        CrawlingDomainTable.prototype.getTBody = function () {
            return this.getTable().find("tbody");
        };
        CrawlingDomainTable.prototype.getCrawlingDate = function () {
            return $('#crawling_date');
        };
        CrawlingDomainTable.prototype.getRequestData = function () {
            return {
                sortParam: this.sortParam,
                sortBy: this.sortBy,
                pageSize: this.pageSize,
                page: this.page
            };
        };
        CrawlingDomainTable.prototype.sort = function (column) {
            if (column === this.sortParam) {
                this.sortBy = this.sortBy === 'asc' ? 'desc' : 'asc';
            }
            else {
                this.sortBy = 'asc';
                this.sortParam = column;
            }
            this.display();
        };
        ;
        CrawlingDomainTable.prototype.getDiffSpan = function (currentCount, previousCount) {
            var diffSpan = "";
            if (previousCount !== null) {
                var pagesDiff = parseInt(currentCount) - parseInt(previousCount);
                if (pagesDiff > 0) {
                    diffSpan = "+" + pagesDiff;
                }
                else if (pagesDiff === 0) {
                    diffSpan = "";
                }
                else {
                    diffSpan = "" + pagesDiff;
                }
            }
            return diffSpan;
        };
        CrawlingDomainTable.prototype.isDomainInProgress = function (domainStatus) {
            if (parseInt(domainStatus) === 5) {
                return 'expired';
            }
            else if (parseInt(domainStatus) === 0) {
                return 'waiting';
            }
            else if (parseInt(domainStatus) === 1 || parseInt(domainStatus) === 4) {
                return 'inprogress';
            }
            else {
                return 'finished';
            }
        };
        CrawlingDomainTable.prototype.isQueueCount = function (domainStatus, queueCount) {
            if (parseInt(domainStatus) === 2 && queueCount > 0) {
                return 'flex';
            }
            else {
                return 'none';
            }
        };
        CrawlingDomainTable.prototype.needsDomainUpdateProgress = function (domainStatus) {
            return parseInt(domainStatus) === 0 || parseInt(domainStatus) === 1 || parseInt(domainStatus) === 4;
        };
        CrawlingDomainTable.prototype.drawRows = function (rows) {
            var _this = this;
            var baseUrls = this.baseUrls;
            rows.forEach(function (value) {
                var diffSpanCount = _this.getDiffSpan(value.domain_count, value.domain_previous_count);
                var domainStatus = value.domain_status;
                var favicon_url = value.favicon_url;
                var domain_last_crawling = value.domain_last_crawling;
                if (domain_last_crawling == null) {
                    domain_last_crawling = new Date();
                }
                else {
                    domain_last_crawling = new Date(domain_last_crawling * 1000);
                }
                var dateFormat = domain_last_crawling, options = { year: "numeric", month: "short", day: "numeric", hour: '2-digit', minute: '2-digit' }, date = dateFormat.toLocaleDateString("en-us", options);
                _this.addTableRow("\n                    <tr data-id=\"" + value.domain_id + "\" name=\"" + _this.isDomainInProgress(domainStatus) + "\" class=\"" + (value.monitoring_enabled === 1 ? '' : 'monitoring__disable') + " " + ((_this.needsDomainUpdateProgress(domainStatus)) ? 'update_progress' : '') + "\">\n                        <td>\n                            <div class=\"dashboard__url\">\n                                <div class=\"dashboard__favicon\" style=\"background-image: url(" + (favicon_url !== null ? favicon_url : '') + ")\"></div>\n                                <div class=\"external-link\"><a href=\"" + _this.baseUrls.crawlReportDomain + "?id=" + value.domain_id + "\" class=\"external-link__val\">" + value.domain_url + "</a><a href=\"" + value.domain_url + "\" target=\"_blank\"><img src=\"/wp-content/themes/sitechecker/out/img_design/external.svg\" alt=\"external\" title=\"external\"></a></div>\n                                <p class=\"dashboard__url-testCase\">" + (_this.isDomainInProgress(domainStatus) === 'inprogress' ? 'In Progress' : 'Waiting Slot') + "</p>\n                            </div>\n                        </td>\n                        <td colspan=\"" + (_this.isDomainInProgress(domainStatus) === 'expired' ? '2' : '1') + "\">\n                            <div class=\"dashboard__values\">\n                            <p class=\"" + ((_this.needsDomainUpdateProgress(domainStatus)) ? 'update_progress_count' : '') + "\">\n                            " + (_this.needsDomainUpdateProgress(domainStatus) ?
                    _this.isValidProgressNum(value.in_progress_count) ? value.in_progress_count : 0
                    :
                        _this.isValidProgressNum(value.domain_count) ? value.domain_count : '-') + "\n                            </p>\n                            <span class=\"" + ((_this.needsDomainUpdateProgress(domainStatus)) ? 'update_progress_count_previous' : '') + "\">" + (!_this.needsDomainUpdateProgress(domainStatus) ? diffSpanCount : '') + "</span>\n                            <div class=\"queue__count\" style=\"display: " + _this.isQueueCount(domainStatus, value.in_queue_count) + "\">\n                            <svg version=\"1.1\" id=\"Layer_1\" xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" x=\"0px\" y=\"0px\"\n\t viewBox=\"0 0 286.054 286.054\" style=\"enable-background:new 0 0 286.054 286.054;\" xml:space=\"preserve\">\n<g>\n\t<path style=\"fill:#FFC107;\" d=\"M143.027,0C64.04,0,0,64.04,0,143.027c0,78.996,64.04,143.027,143.027,143.027\n\t\tc78.996,0,143.027-64.022,143.027-143.027C286.054,64.04,222.022,0,143.027,0z M143.027,259.236\n\t\tc-64.183,0-116.209-52.026-116.209-116.209S78.844,26.818,143.027,26.818s116.209,52.026,116.209,116.209\n\t\tS207.21,259.236,143.027,259.236z M143.036,62.726c-10.244,0-17.995,5.346-17.995,13.981v79.201c0,8.644,7.75,13.972,17.995,13.972\n\t\tc9.994,0,17.995-5.551,17.995-13.972V76.707C161.03,68.277,153.03,62.726,143.036,62.726z M143.036,187.723\n\t\tc-9.842,0-17.852,8.01-17.852,17.86c0,9.833,8.01,17.843,17.852,17.843s17.843-8.01,17.843-17.843\n\t\tC160.878,195.732,152.878,187.723,143.036,187.723z\"/>\n</g>\n</svg><p class=\"tooltip\"><a href=\"/plans/\">Upgrade your plan</a>, to crawl the whole website.</p>\n                            </div>\n                            </div>\n                            <p class=\"dashboard__url-expire\">Outdated data. <a href=\"" + _this.baseUrls.reCrawlDomain + "?&id=" + value.domain_id + "\">Please start a new audit</a>.</p>\n                        </td>\n                        <td style=\"display: " + (_this.isDomainInProgress(domainStatus) === 'expired' ? 'none' : '') + "\">\n                            <div class=\"dashboard__score\">\n                                " + _this.isValidPageScore(domainStatus, value.main_page_score, value.domain_id, value.domain_url) + "\n                            </div>\n                        </td>\n                        <td>\n                            <div class=\"dashboard__diagram\">\n                                " + _this.activityChanges(value.last_week_changes_activity) + "\n                            </div>\n                        </td>\n                        <td>\n                            " + date + "\n                        </td>\n                        <td>\n                            <div class=\"dashboard__links\">\n                                <a href=\"" + _this.baseUrls.crawlReportDomain + "?id=" + value.domain_id + "\" class=\"btn audit__btn\">\n                                    <svg width=\"24\" height=\"23\" viewBox=\"0 0 24 23\" fill=\"none\" xmlns=\"http://www.w3.org/2000/svg\">\n                                        <path d=\"M10.2347 8.37864L8.70534 6.88655C9.1759 6.19789 9.41117 5.39446 9.41117 4.59103C9.41117 2.06596 7.29366 0 4.70559 0C2.11751 0 0 2.06596 0 4.59103C0 7.1161 2.11751 9.18207 4.70559 9.18207C5.52906 9.18207 6.35254 8.95251 7.05838 8.49341L8.5877 9.9855C8.82298 10.215 9.52881 10.6742 10.2347 9.9855C10.7052 9.52639 10.7052 8.83774 10.2347 8.37864ZM4.70559 6.88655C3.41155 6.88655 2.35279 5.85357 2.35279 4.59103C2.35279 3.3285 3.41155 2.29552 4.70559 2.29552C5.99962 2.29552 7.05838 3.3285 7.05838 4.59103C7.05838 5.85357 5.99962 6.88655 4.70559 6.88655Z\" transform=\"translate(12.9403 12.6254)\" fill=\"#ffffff\"/>\n                                        <path d=\"M17.646 0H1.1764C0.470559 0 0 0.459103 0 1.14776V21.8074C0 22.4961 0.470559 22.9552 1.1764 22.9552H13.7638C11.8816 21.6926 10.5876 19.6267 10.5876 17.2164C10.5876 13.4288 13.7638 10.3298 17.646 10.3298C17.9989 10.3298 18.4694 10.3298 18.8223 10.4446V1.14776C18.8223 0.459103 18.3518 0 17.646 0ZM9.41117 9.18207H3.52919C2.82335 9.18207 2.35279 8.60819 2.35279 8.03431C2.35279 7.34565 2.82335 6.88655 3.52919 6.88655H9.41117C10.117 6.88655 10.5876 7.34565 10.5876 8.03431C10.5876 8.60819 10.117 9.18207 9.41117 9.18207ZM15.2932 4.59103H3.52919C2.82335 4.59103 2.35279 4.01715 2.35279 3.44327C2.35279 2.75462 2.82335 2.29552 3.52919 2.29552H15.2932C15.999 2.29552 16.4696 2.75462 16.4696 3.44327C16.4696 4.01715 15.999 4.59103 15.2932 4.59103Z\" fill=\"#ffffff\"/>\n                                    </svg>\n                                    Audit\n                                </a>\n                                <a href=\"" + _this.baseUrls.monitoringUserDomain + "?id=" + value.domain_id + "\" class=\"btn monitoring__btn\" data-id=\"" + value.domain_id + "\">\n                                    <svg width=\"20\" height=\"23\" viewBox=\"0 0 20 23\" fill=\"none\" xmlns=\"http://www.w3.org/2000/svg\">\n                                        <path d=\"M9.96031 22.8655C11.2493 22.8655 12.3039 21.8365 12.3039 20.5789H7.61671C7.61671 21.8365 8.67133 22.8655 9.96031 22.8655ZM17.577 16.0058V9.71784C17.577 6.17368 15.1162 3.3155 11.718 2.5152V1.71491C11.718 0.800292 10.8978 0 9.96031 0C9.02287 0 8.20261 0.800292 8.20261 1.71491V2.5152C4.80439 3.3155 2.3436 6.17368 2.3436 9.71784V16.0058L0 18.2924V19.4357H19.9206V18.2924L17.577 16.0058Z\" fill=\"#ffffff\"/>\n                                    </svg>\n                                    Monitoring\n                                </a>\n                            </div>\n                        </td>\n                        <td>\n                        <div class=\"dashboard__actions-btn\">\n                        <div class=\"dashboard__monitoring\">\n                            <div class=\"dashboard__monitoring-switch\" data-id=\"" + value.domain_id + "\" class=\"dashboard__monitoring-switch\" name=\"" + (value.monitoring_enabled === 1 ? 'checked' : 'unchecked') + "\">\n                            <svg width=\"20\" height=\"23\" viewBox=\"0 0 20 23\" fill=\"none\" xmlns=\"http://www.w3.org/2000/svg\" style=\"display: " + (value.monitoring_enabled === 1 ? 'block' : 'none') + "\" class=\"checked\">\n                            <path d=\"M10.75 20C11.8824 20 12.8088 19.1 12.8088 18H8.69118C8.69118 19.1 9.61765 20 10.75 20ZM17.4412 14V8.5C17.4412 5.4 15.2794 2.9 12.2941 2.2V1.5C12.2941 0.7 11.5735 0 10.75 0C9.92647 0 9.20588 0.7 9.20588 1.5V2.2C6.22059 2.9 4.05882 5.4 4.05882 8.5V14L2 16V17H19.5V16L17.4412 14Z\" fill=\"black\"/><path fill-rule=\"evenodd\" clip-rule=\"evenodd\" d=\"M5.6 1.4L4.2 0C1.8 1.8 0.2 4.6 0 7.8H2C2.2 5.1 3.5 2.8 5.6 1.4V1.4Z\" fill=\"black\"/><path fill-rule=\"evenodd\" clip-rule=\"evenodd\" d=\"M19 7.8H21C20.8 4.6 19.3 1.8 16.9 0L15.5 1.4C17.5 2.8 18.8 5.1 19 7.8V7.8Z\" fill=\"black\"/>\n                            </svg>\n                            <svg class=\"unchecked\" style=\"display: " + (value.monitoring_enabled === 0 ? 'block' : 'none') + "\" width=\"18\" height=\"20\" viewBox=\"0 0 18 20\" fill=\"none\" xmlns=\"http://www.w3.org/2000/svg\">\n<path fill-rule=\"evenodd\" clip-rule=\"evenodd\" d=\"M8.5 20C9.6 20 10.5 19.1 10.5 18H6.5C6.5 19.1 7.4 20 8.5 20ZM15 8.5C15 5.4 12.9 2.9 9.99998 2.2V1.5C9.99998 0.7 9.29998 0 8.49998 0C7.69998 0 6.99998 0.7 6.99998 1.5V2.2C6.49998 2.3 5.99998 2.5 5.59998 2.7L15 12.1V8.5ZM14.7 17L16.7 19L18 17.7L1.3 1L0 2.3L2.9 5.2C2.3 6.2 2 7.3 2 8.5V14L0 16V17H14.7Z\" fill=\"#CCCCCC\"/>\n</svg>\n                            <span class=\"tooltip\"><span>" + (value.monitoring_enabled === 1 ? 'Stop' : 'Start') + "</span> tracking changes critical for SEO</span>\n                            </div>\n                            </div>\n                        <div class=\"share__website\" name=\"" + value.domain_url + "\" data-id=\"" + value.domain_id + "\" data-target=\"" + parseInt(value.crawling_sharing_enabled) + "\">\n                            <svg name=\"" + value.token + "\" width=\"15\" height=\"15\" viewBox=\"0 0 15 15\" fill=\"none\" xmlns=\"http://www.w3.org/2000/svg\">\n                                <path d=\"M12.2417 9.53342C11.3658 9.53342 10.5933 9.94715 10.0886 10.5807L5.40714 8.20691C5.46842 7.98026 5.51248 7.74609 5.51248 7.49963C5.51248 7.23133 5.46016 6.97873 5.38716 6.73294L10.0479 4.37007C10.5499 5.03232 11.3424 5.46517 12.2423 5.46517C13.7668 5.46517 15 4.24243 15 2.73225C15 1.22343 13.7668 0 12.2424 0C10.7214 0 9.48681 1.22343 9.48681 2.73222C9.48681 2.97937 9.53089 3.21422 9.59284 3.44156L4.91209 5.81537C4.40669 5.18114 3.63275 4.76605 2.75555 4.76605C1.23247 4.76605 0 5.99014 0 7.49963C0 9.00911 1.23251 10.2325 2.75555 10.2325C3.65685 10.2325 4.4487 9.79834 4.95269 9.13543L9.61139 11.4983C9.53839 11.7434 9.4854 11.998 9.4854 12.267C9.4854 13.7765 10.72 15 12.241 15C13.7654 15 14.9986 13.7765 14.9986 12.267C14.9993 10.7562 13.7661 9.53342 12.2417 9.53342Z\" fill=\"#BBBBBB\"/>\n                            </svg>\n                            <span class=\"tooltip\">Share website</span>\n                        </div>\n                        <a class=\"recrawl__website\" href=\"" + _this.baseUrls.reCrawlDomain + "?&id=" + value.domain_id + "\">\n                            <svg width=\"16\" height=\"15\" viewBox=\"0 0 16 15\" fill=\"none\" xmlns=\"http://www.w3.org/2000/svg\">\n                                <path d=\"M2.48186 8.4375H0.587178H0.529053V13.125L1.91749 11.8491C3.27779 13.7559 5.5081 15 8.02903 15C11.854 15 15.0096 12.1369 15.4709 8.4375H13.5762C13.1299 11.0981 10.8162 13.125 8.02903 13.125C6.05466 13.125 4.31654 12.1069 3.31342 10.5666L5.62997 8.4375H2.48186Z\" fill=\"#BBBBBB\"/>\n                                <path d=\"M8.02901 0C4.20402 0 1.04841 2.86312 0.587158 6.56248H2.48184C2.92809 3.90186 5.24183 1.87499 8.02901 1.87499C10.0624 1.87499 11.8446 2.95405 12.8327 4.57124L10.8415 6.56248H12.7165H13.5762H15.4709H15.529V1.87499L14.1865 3.21749C12.8318 1.27218 10.579 0 8.02901 0Z\" fill=\"#BBBBBB\"/>\n                            </svg>\n                            <span class=\"tooltip\">Run new audit</span>\n                            </a>\n                        <div class=\"delete__website\" name=\"" + value.domain_id + "\">\n                            <svg width=\"13\" height=\"15\" viewBox=\"0 0 13 15\" fill=\"none\" xmlns=\"http://www.w3.org/2000/svg\">\n                                <path fill-rule=\"evenodd\" clip-rule=\"evenodd\" d=\"M1.36239 13.3335C1.36239 14.2501 2.11238 15.0001 3.02905 15.0001H9.69569C10.6124 15.0001 11.3624 14.2501 11.3624 13.3335V3.3335H1.36239V13.3335ZM12.1957 0.833331H9.27902L8.44569 0H4.27904L3.44571 0.833331H0.529053V2.49999H12.1957V0.833331Z\" fill=\"#BBBBBB\"/>\n                            </svg>\n                            <span class=\"tooltip\">Delete website</span>\n                            </div>\n                            </div>        \n                        </td>\n                    </tr>\n                ");
            });
            var table = $('.dashboard__table');
            if (table.length > 0) {
                if (table.hasClass('free__user')) {
                    var recrawl = $('.recrawl__website');
                    recrawl.find('.tooltip').text('Upgrade your account');
                    recrawl.click(function (e) {
                        e.preventDefault();
                    });
                }
            }
            $('.dashboard__monitoring-switch').click(function () {
                var monitoringEnableDisablePath = baseUrls.monitoringEnable, action = 'monitoring_enable', btn = $(this), status = btn.attr('name'), domainID = btn.attr('data-id');
                if (status === 'checked') {
                    action = 'monitoring_disable';
                    monitoringEnableDisablePath = baseUrls.monitoringDisable;
                }
                $(btn).css('pointer-events', 'none');
                $.ajax({
                    url: monitoringEnableDisablePath,
                    type: "POST",
                    dataType: "JSON",
                    data: {
                        id: domainID
                    },
                    success: function (data) {
                        console.log(data);
                        $.ajax({
                            url: baseUrls.getLimits,
                            type: "GET",
                            dataType: "JSON",
                            success: function (limits) {
                                console.log(limits);
                                var monitoring = limits.data.availbleMonitoring;
                                $('#available_monitorings').text(monitoring);
                                if (monitoring > 0) {
                                    $('.monitoring__limit-banner').fadeOut();
                                    $('.dashboard__monitoring-switch[name="exceeded"]').attr('name', 'unchecked').find('.tooltip').html('<span>Start</span> tracking changes critical for SEO');
                                }
                                if (data.success === true) {
                                    if (action === 'monitoring_enable') {
                                        btn.attr('name', 'checked');
                                        $('tr[data-id="' + domainID + '"]').removeClass('monitoring__disable ');
                                        btn.find('.checked').css('display', 'block');
                                        btn.find('.unchecked').css('display', 'none');
                                        btn.find('.tooltip span').text('Stop');
                                    }
                                    else if (action === 'monitoring_disable') {
                                        $('tr[data-id="' + domainID + '"]').addClass('monitoring__disable ');
                                        btn.attr('name', 'unchecked');
                                        btn.find('.unchecked').css('display', 'block');
                                        btn.find('.checked').css('display', 'none');
                                        btn.find('.tooltip span').text('Start');
                                    }
                                }
                                else if (data.success === false && data.status === 'no exceeded') {
                                    btn.attr('name', 'exceeded');
                                    btn.parent().find('.tooltip').text('Upgrade Plan');
                                    $('.monitoring__limit-banner').fadeIn().css('display', 'flex').append('<script>dataLayer.push({ \'event\': \'autoEvent\', \'eventCategory\': \'Monitoring limited popup shown\', \'eventAction\': \'shown\' });</script>');
                                }
                                $(btn).css('pointer-events', 'auto');
                            }
                        });
                    }
                });
            });
            $('#enable_sharing').change(function () {
                $.ajax({
                    url: baseUrls.enableDisableSharing,
                    type: "POST",
                    dataType: "JSON",
                    data: {
                        id: $('.share__popup').attr('data-id'),
                        action: 'enable_disable_crawling_sharing'
                    },
                    success: function (data) {
                        console.log(data);
                        var popupId = $('.share__popup').attr('data-id'), img_attr = $('.share__website[data-id=' + popupId + ']');
                        if (data.enabled === 1) {
                            img_attr.attr('data-target', '1');
                            $('.share__popup-body button').removeClass('btn-disabled');
                        }
                        else {
                            img_attr.attr('data-target', '0');
                            $('.share__popup-body button').addClass('btn-disabled');
                        }
                    }
                });
                return false;
            });
            $('.share__website').click(function () {
                var enabled_sharing = $(this).attr('data-target');
                if (parseInt(enabled_sharing) === 1) {
                    $("#enable_sharing").prop("checked", true);
                    $('.share__popup-body button').removeClass('btn-disabled');
                }
                else {
                    $('.share__popup-body button').addClass('btn-disabled');
                    $("#enable_sharing").prop("checked", false);
                }
                var domainId = $(this).attr('data-id'), token = $(this).find('svg').attr('name'), baseUrl = window.location.protocol + '//' + document.location.hostname + '/tool/crawl-report-domain/', domainUrl = $(this).attr('name');
                $('#share_url').val(baseUrl + '?token=' + token);
                $('.share__popup-header span').text(domainUrl);
                $('.share__popup').attr('data-id', domainId).fadeIn().css('display', 'flex');
                function share__invit() {
                    var btn = $('#share_invite'), sharing_email = $('#share_email');
                    if (sharing_email.val() === '') {
                        $(sharing_email).attr('placeholder', 'Please, type email.');
                    }
                    else {
                        btn.addClass('generating').text('Inviting');
                        $.ajax({
                            url: baseUrls.sendSharingInvite,
                            type: "POST",
                            dataType: "JSON",
                            data: {
                                email: sharing_email.val(),
                                link: $('#share_url').val()
                            },
                            success: function (data) {
                                function btnRestart() {
                                    btn.text('Invite');
                                    $('.share__popup-body').append('<p class="invited_urls">' + $('#share_email').val() + ' was invited.</p>');
                                    $('#share_email').val('');
                                    return false;
                                }
                                if (data.success === true) {
                                    btn.removeClass('generating').text('Invited!');
                                    setTimeout(btnRestart, 1000);
                                }
                            }
                        });
                    }
                }
                $('#share_invite').click(function () {
                    share__invit();
                    return false;
                });
                $('#share_email').keypress(function (e) {
                    if (e.which == 13) {
                        share__invit();
                        e.preventDefault();
                        return false;
                    }
                });
                $('.share__popup-header img').click(function () {
                    $('.share__popup').fadeOut();
                    $('#share_url, #share_email').val('');
                    $('.invited_urls').remove();
                });
            });
            this.startProgressUpdater();
        };
        CrawlingDomainTable.prototype.activityChanges = function (changes) {
            changes = JSON.parse(changes);
            if (changes !== null && changes.length > 0) {
                var changesBlock_1 = '', changesLength = changes.length, maxCount_1 = 0;
                $.each(changes, function () {
                    var value = parseInt(this.pages_changed_count);
                    if (value > maxCount_1) {
                        maxCount_1 = value;
                    }
                });
                $.each(changes.reverse(), function () {
                    var value = parseInt(this.pages_changed_count), percentage = 0;
                    percentage = ((value * 100) / maxCount_1);
                    changesBlock_1 = changesBlock_1 + '<div class="dashboard__diagram-block"><span class="dashboard__diagram-val" style="height: ' + percentage + '%"></span></div>';
                });
                if (changesLength < 7) {
                    var index = 7 - changesLength, i = 0;
                    while (i < index) {
                        changesBlock_1 = changesBlock_1 + '<div class="dashboard__diagram-block"><span class="dashboard__diagram-val"></span></div>';
                        i++;
                    }
                }
                return changesBlock_1;
            }
            else {
                var changesBlock = '', i = 1;
                while (i < 8) {
                    changesBlock = changesBlock + '<div class="dashboard__diagram-block"><span class="dashboard__diagram-val"></span></div>';
                    i++;
                }
                return changesBlock;
            }
        };
        CrawlingDomainTable.prototype.isValidPageScore = function (status, num, id, url) {
            var baseUrls = this.baseUrls;
            var endColor = 'ok';
            function scoreColor(data) {
                if (data < 33) {
                    endColor = 'error';
                }
                else if (data >= 33 && data <= 65) {
                    endColor = 'warning';
                }
                else if (data > 65) {
                    endColor = 'ok';
                }
            }
            if (num === null || isNaN(num) || parseInt(status) === 0 || parseInt(status) === 1) {
                var host = document.location.host || document.location.hostname, statusUrl = document.location.protocol + "//api1." + host + "/api/v1/full/all/" + url;
                var statusRequest = $.ajax({
                    url: statusUrl,
                    method: 'GET',
                    dataType: 'json',
                    xhrFields: {
                        withCredentials: true
                    }
                });
                statusRequest.done(function (response) {
                    $.ajax({
                        url: baseUrls.updateDomainMainPageScore,
                        type: "POST",
                        async: false,
                        dataType: "JSON",
                        data: {
                            id: id,
                            score: response.data.evaluation
                        },
                        success: function () {
                            scoreColor(response.data.evaluation);
                            return '<span class="' + endColor + '">' + response.data.evaluation + '</span><span>/100</span>';
                        }
                    });
                });
                return '<img class="dashboard__score-loading" src="/wp-content/themes/sitechecker/out/img_design/loading-blue.svg">';
            }
            else {
                scoreColor(num);
                return '<span class="' + endColor + '">' + num + '</span><span>/100</span>';
            }
        };
        CrawlingDomainTable.prototype.isValidProgressNum = function (num) {
            return num !== null && !isNaN(num);
        };
        CrawlingDomainTable.prototype.logFailed = function (log) {
            console.log("error" + log);
        };
        CrawlingDomainTable.prototype.display = function () {
            var _this = this;
            this.getPageData().then(function (data) { return _this.processResult(data); }, function (error) { return _this.logFailed(error); });
        };
        ;
        CrawlingDomainTable.prototype.startProgressUpdater = function () {
            var rowsToUpdate = this.getTBody().find('.update_progress');
            var table = this;
            var baseUrls = this.baseUrls;
            rowsToUpdate.each(function () {
                var currentRow = $(this);
                var spanToUpdateCount = currentRow.find('.update_progress_count');
                var statusToUpdate = currentRow.attr('name');
                function countUpdater(blockName, value) {
                    var $countToBlock = blockName, countTo = value;
                    if (countTo === null || isNaN(countTo)) {
                        countTo = 0;
                    }
                    $({
                        countNum: blockName.text()
                    }).animate({
                        countNum: countTo
                    }, {
                        duration: 2000,
                        easing: 'swing',
                        step: function () {
                            $countToBlock.text(Math.floor(this.countNum));
                        },
                        complete: function () {
                            $countToBlock.text(this.countNum);
                        }
                    });
                }
                var id = currentRow.data('id');
                var updater = setInterval(function () {
                    $.ajax({
                        url: baseUrls.inProgressStatistic,
                        type: "POST",
                        async: false,
                        dataType: "JSON",
                        data: {
                            domainId: id
                        },
                        success: function (data) {
                            if (parseInt(data.status) === 0) {
                                countUpdater(spanToUpdateCount, parseInt(data.in_progress_count));
                                currentRow.attr('name', 'waiting');
                                currentRow.find('.dashboard__url-testCase').text('Waiting slot');
                            }
                            else if (parseInt(data.status) === 1) {
                                countUpdater(spanToUpdateCount, parseInt(data.in_progress_count));
                                currentRow.attr('name', 'inprogress');
                                currentRow.find('.dashboard__url-testCase').text('In progress');
                            }
                            else if (parseInt(data.status) == 2) {
                                currentRow.find('.dashboard__url-testCase').text('');
                                clearInterval(updater);
                                $('.dashboard__testCase').hide();
                                table.init();
                                table.updateLimits();
                            }
                            else {
                                currentRow.find('.dashboard__url-testCase').text('');
                            }
                        }
                    });
                }, 5000);
            });
        };
        CrawlingDomainTable.prototype.crawlConfirmClose = function () {
            $('.addCrawl-block').css('display', 'none');
            $('.addCrawl_popup').fadeOut();
        };
        CrawlingDomainTable.prototype.updateLimits = function () {
            $.ajax({
                url: this.baseUrls.getLimits,
                type: "GET",
                dataType: "JSON",
                success: function (resultJSON) {
                    var result = resultJSON;
                    if (result.success) {
                        var availableDomains = parseInt(result.data.availableDomains) < 0 ? 0 : parseInt(result.data.availableDomains), availableMonitoringDomains = parseInt(result.data.availbleMonitoring) < 0 ? 0 : parseInt(result.data.availbleMonitoring);
                        this.availablePagesLimit = parseInt(result.data.availablePages) < 0 ? 0 : parseInt(result.data.availablePages);
                        $("#available_pages").text(this.availablePagesLimit);
                        $("#available_domains").text(availableDomains);
                        $("#available_monitorings").text(availableMonitoringDomains);
                        var monitoring__close = $('.monitoring__limit-banner').hasClass('monitoring__close'), audit__close = $('.audit__limit-banner').hasClass('audit__close');
                        if (availableDomains > 0) {
                            $('.audit__limit-banner').fadeOut();
                        }
                        if (availableDomains <= 0 && !audit__close) {
                            $('.audit__limit-banner').fadeIn().css('display', 'flex');
                            $('.audit__limits-container').append('<script>dataLayer.push({ \'event\': \'autoEvent\', \'eventCategory\': \'Crawling limited popup shown\', \'eventAction\': \'shown\' });</script>');
                        }
                        else if (availableMonitoringDomains <= 0 && !monitoring__close && !audit__close) {
                            $('.monitoring__limit-banner').fadeIn().css('display', 'flex');
                            $('.audit__limits-container').append('<script>dataLayer.push({ \'event\': \'autoEvent\', \'eventCategory\': \'Monitoring limited popup shown\', \'eventAction\': \'shown\' });</script>');
                        }
                        else if (availableMonitoringDomains > 0) {
                            $('.monitoring__limit-banner').fadeOut();
                        }
                        var pagesLimit_1 = this.availablePagesLimit, slider_1 = $('#settings__pages');
                        $('#settings__pages-value').val(pagesLimit_1);
                        setTimeout(function () {
                            slider_1.data('slider').options.max = pagesLimit_1;
                            slider_1.slider('setValue', pagesLimit_1);
                        }, 1000);
                        if (window.location.href.indexOf('create') > 0) {
                            var create = localStorage.getItem('create'), createLogin = localStorage.getItem('createLogin');
                            if (createLogin == 'Shown' || $('.dashboard__testCase').is(":visible")) {
                                if (create == 'Shown') {
                                    localStorage.setItem('create', 'false');
                                    localStorage.setItem('createLogin', 'false');
                                    $('.addCrawl_popup .login_popup-close').click(function () {
                                        $('.addCrawl-block').css('display', 'none');
                                        $('.addCrawl_popup').fadeOut();
                                    });
                                    setTimeout(this.crawlConfirmClose, 3000);
                                }
                            }
                            else if (availableDomains !== 0) {
                                if (create == 'Shown') {
                                    localStorage.setItem('create', 'false');
                                    localStorage.setItem('createLogin', 'false');
                                    $('.addCrawl_popup .login_popup-close').click(function () {
                                        $('.addCrawl-block').css('display', 'none');
                                        $('.addCrawl_popup').fadeOut();
                                    });
                                    setTimeout(this.crawlConfirmClose, 3000);
                                }
                            }
                        }
                        $('#loader-list').css('opacity', '0');
                        $('.crawlerData').fadeIn();
                    }
                }
            });
        };
        CrawlingDomainTable.prototype.getUrlForRequest = function () {
            return this.baseUrls.crawlDomainList;
        };
        return CrawlingDomainTable;
    }(Crawling.BaseTable));
    Crawling.CrawlingDomainTable = CrawlingDomainTable;
})(Crawling || (Crawling = {}));
/// <reference path="CrawlingBaseTable.ts"/>
/// <reference path="../../../../../../typings/globals/jquery/index.d.ts" />
var Crawling;
(function (Crawling) {
    var CrawlingTable = /** @class */ (function (_super) {
        __extends(CrawlingTable, _super);
        function CrawlingTable(baseUrls, domainUrl, token) {
            var _this = _super.call(this, baseUrls, '/getWeightByDomains', token) || this;
            _this.domainUrl = domainUrl;
            _this.sort_url = $('#sort_url');
            _this.sort_weight = $('#sort_weight');
            _this.sort_status = $('#sort_status_code');
            _this.sort_sitemap = $('#sort_sitemap');
            return _this;
        }
        CrawlingTable.prototype.getSortParam = function () {
            return 'weight';
        };
        CrawlingTable.prototype.getPagination = function () {
            return $('#pageTablePagination');
        };
        CrawlingTable.prototype.getTableElements = function () {
            return $('.table_elements');
        };
        CrawlingTable.prototype.getTable = function () {
            return $('#pageTable');
        };
        CrawlingTable.prototype.getTBody = function () {
            return this.getTable().find("tbody");
        };
        CrawlingTable.prototype.getCrawlingDate = function () {
            return $('#crawling_date');
        };
        CrawlingTable.prototype.setPagination = function () {
            var html = "\n                <div>\n                    <div class=\"dataTables_info\" id=\"pageTable_info\" role=\"status\" aria-live=\"polite\">\n                        Showing " + ((this.page * this.pageSize) - this.pageSize) + " to " + (this.page * this.pageSize > this.totalCount ? this.totalCount : this.page * this.pageSize) + "  of " + this.totalCount + " entries\n                    </div>\n                </div>\n                <div>\n                    <div class=\"dataTables_paginate paging_simple_numbers\" id=\"pageTable_paginate\">\n                        <ul class=\"pagination\">\n                            <li class=\"page-item disabled\">\n                                <span>Page " + this.page + " from " + this.totalPages + "</span>\n                            </li>\n                            <li class=\"paginate_button page-item previous " + (this.page > 1 ? '' : 'disabled') + "\">\n                                <a href=\"javascript:void(0);\" aria-controls=\"pageTable\" class=\"page-link\" " + (this.page > 1 ? "onclick=\"crawlingTable.previousPage();\"" : '') + "  >Previous</a>\n                            </li>\n                            <li class=\"paginate_button page-item " + (this.page < this.totalPages ? '' : 'disabled') + "\">\n                                <a href=\"javascript:void(0);\" aria-controls=\"pageTable\"  class=\"page-link\" " + (this.page < this.totalPages ? "onclick=\"crawlingTable.nextPage()\"" : '') + " >Next</a>\n                            </li>\n                        </ul>\n                    </div>\n                </div>\n            ";
            this.getPagination().html(html);
        };
        ;
        CrawlingTable.prototype.getRequestData = function () {
            return this.addFilters({
                url: this.domainUrl,
                sortParam: this.sortParam,
                sortBy: this.sortBy,
                pageSize: this.pageSize,
                page: this.page,
                token: this.token
            });
        };
        CrawlingTable.prototype.sort = function (column) {
            if (column === this.sortParam) {
                this.sortBy = this.sortBy === 'asc' ? 'desc' : 'asc';
            }
            else {
                this.sortBy = 'asc';
                this.sortParam = column;
            }
            this.sort_url.attr('class', 'sorting');
            this.sort_weight.attr('class', 'sorting');
            this.sort_status.attr('class', 'sorting');
            this.sort_sitemap.attr('class', 'sorting');
            if (column === 'weight') {
                this.sort_weight.attr('class', 'sorting_' + this.sortBy);
            }
            else if (column === 'http_status_code') {
                this.sort_status.attr('class', 'sorting_' + this.sortBy);
            }
            else {
                this.sort_url.attr('class', 'sorting_' + this.sortBy);
            }
            this.display();
        };
        ;
        CrawlingTable.prototype.toggleRowData = function (index) {
            var row = $('#row_' + index);
            var rowIcon = row.find('.icon');
            var rowData = $('#row_data_' + index);
            if (row.hasClass("dataShow")) {
                row.removeClass('dataShow');
                rowIcon.text('+').css('font-size', '16px');
                rowData.hide();
            }
            else {
                row.addClass("dataShow");
                rowIcon.text('-').css('font-size', '25px');
                rowData.show();
            }
        };
        CrawlingTable.prototype.drawRows = function (rows) {
            var _this = this;
            $('#loader').css('opacity', '0');
            $('#pageTable_wrapper').fadeIn();
            var duplicate_titles = [];
            var duplicate_h1 = [];
            var duplicate_description = [];
            var baseUrls = this.baseUrls;
            var domainURL = this.domainUrl;
            var token = this.token;
            var duplicate_titles_storage = JSON.parse(localStorage.getItem('duplicate_titles_storage_' + token)), duplicate_h1_storage = JSON.parse(localStorage.getItem('duplicate_h1_storage_' + token)), duplicate_description_storage = JSON.parse(localStorage.getItem('duplicate_description_storage_' + token));
            if (duplicate_titles_storage === null) {
                $.ajax({
                    url: baseUrls.crawlRequest,
                    type: "POST",
                    async: false,
                    dataType: "JSON",
                    data: {
                        path: '/getWeightByDomains',
                        method: 'POST',
                        data: JSON.stringify({
                            data: [{
                                    url: domainURL,
                                    token: token,
                                    customFilter: "3"
                                }]
                        })
                    },
                    success: function (data) {
                        var duplicateTitlesUrls = data[0]['summary'];
                        $.each(duplicateTitlesUrls, function (key, value) {
                            duplicate_titles.push(value['url']);
                        });
                        localStorage.setItem('duplicate_titles_storage_' + token, JSON.stringify(duplicate_titles));
                        duplicate_titles_storage = JSON.parse(localStorage.getItem('duplicate_titles_storage_' + token));
                    }
                });
            }
            if (duplicate_h1_storage === null) {
                $.ajax({
                    url: baseUrls.crawlRequest,
                    type: "POST",
                    async: false,
                    dataType: "JSON",
                    data: {
                        path: '/getWeightByDomains',
                        method: 'POST',
                        data: JSON.stringify({
                            data: [{
                                    url: domainURL,
                                    token: token,
                                    customFilter: "27"
                                }]
                        })
                    },
                    success: function (data) {
                        var duplicateH1Urls = data[0]['summary'];
                        $.each(duplicateH1Urls, function (key, value) {
                            duplicate_h1.push(value['url']);
                        });
                        localStorage.setItem('duplicate_h1_storage_' + token, JSON.stringify(duplicate_h1));
                        duplicate_h1_storage = JSON.parse(localStorage.getItem('duplicate_h1_storage_' + token));
                    }
                });
            }
            if (duplicate_description_storage === null) {
                $.ajax({
                    url: baseUrls.crawlRequest,
                    type: "POST",
                    async: false,
                    dataType: "JSON",
                    data: {
                        path: '/getWeightByDomains',
                        method: 'POST',
                        data: JSON.stringify({
                            data: [{
                                    url: domainURL,
                                    token: token,
                                    customFilter: "32"
                                }]
                        })
                    },
                    success: function (data) {
                        var duplicateDescriptionUrls = data[0]['summary'];
                        $.each(duplicateDescriptionUrls, function (key, value) {
                            duplicate_description.push(value['url']);
                        });
                        localStorage.setItem('duplicate_description_storage_' + token, JSON.stringify(duplicate_description));
                        duplicate_description_storage = JSON.parse(localStorage.getItem('duplicate_description_storage_' + token));
                    }
                });
            }
            rows.forEach(function (page, index) {
                _this.addTableRow(_this.renderRow(page, index, duplicate_titles_storage, duplicate_h1_storage, duplicate_description_storage));
            });
        };
        CrawlingTable.prototype.renderRow = function (page, index, duplicate_titles, duplicate_h1, duplicate_description) {
            var statuses = this.getPageStatuses(page, duplicate_titles, duplicate_h1, duplicate_description);
            var finalStatuses = this.getUnactivePageStatuses(statuses);
            return "\n                <tr id=\"row_" + index + "\">\n                    <td class=\"overview__weight-td\">\n                        " + this.numberFormat(page.normalizedWeight, 1) + "\n                    </td>\n                    <td class=\"overview__link-td\">\n                        <div class=\"external-link\"><a href=\"" + this.baseUrls.pageInformation + "?url=" + page.crc32Url + "&token=" + this.token + "\">" + page.url + "</a><a href=\"" + page.url + "\" target=\"_blank\"><img src=\"/wp-content/themes/sitechecker/out/img_design/external.svg\" alt=\"external\" title=\"external\"></a></div>\n                       <p class=\"title\">" + this.titleExist(page.title) + "</p>\n                    </td>\n                    <td class=\"overview__status-td\"> \n                        " + this.getStatusCodeColumn(page) + "\n                    </td>\n                    <td class=\"overview__links-td\">\n                        <p class=\"crawled__links-btn " + (page.externalCountTotal === 0 ? 'empty' : '') + "\" onclick=\"popupTable = new Crawling.Popup.ExternalPopupTable('" + this.baseUrls.crawlRequest + "', '" + page.id + "', '" + this.token + "')\">External " + (page.externalCountTotal !== 0 ? '(' + page.externalCountTotal + ')' : '') + "</p>\n                        <p class=\"crawled__links-btn " + (page.internalCountTotal === 0 ? 'empty' : '') + "\" onclick=\"popupTable = new Crawling.Popup.InternalPopupTable('" + this.baseUrls.crawlRequest + "', '" + page.id + "', '" + this.token + "')\">Internal " + (page.internalCountTotal !== 0 ? '(' + page.internalCountTotal + ')' : '') + "</p>\n                        <p class=\"crawled__links-btn " + (page.incomingCountTotal === 0 ? 'empty' : '') + "\" onclick=\"popupTable = new Crawling.Popup.IncomingPopupTable('" + this.baseUrls.crawlRequest + "', '" + page.id + "', '" + this.token + "')\">Anchors " + (page.incomingCountTotal !== 0 ? '(' + page.incomingCountTotal + ')' : '') + "</p>\n                        <p class=\"crawled__links-btn duplicated__links-title\" onclick=\"popupTable = new Crawling.Popup.DuplicatedLinks('" + this.baseUrls.crawlRequest + "', '" + page.id + "', '" + this.token + "', 'title')\">Show Duplicates</p>\n                        <p class=\"crawled__links-btn duplicated__links-h1\" onclick=\"popupTable = new Crawling.Popup.DuplicatedLinks('" + this.baseUrls.crawlRequest + "', '" + page.id + "', '" + this.token + "', 'h1')\">Show Duplicates</p>\n                        <p class=\"crawled__links-btn duplicated__links-descript\" onclick=\"popupTable = new Crawling.Popup.DuplicatedLinks('" + this.baseUrls.crawlRequest + "', '" + page.id + "', '" + this.token + "', 'description')\">Show Duplicates</p>\n                    </td>\n                    <td class=\"overview__errors-td\">\n                        <div class=\"messages\">\n                            " + finalStatuses
                .map(function (status) { return "<a name=\"" + status.id + "\" class=\"" + status["class"] + " " + status.name + "\">" + status.title + "</a>"; })
                .reduce(function (prev, curr) { return prev + curr; }, '') + "\n                        </div>\n                    </td>\n                </tr>\n            ";
        };
        CrawlingTable.prototype.titleExist = function (val) {
            if (val !== undefined) {
                return val;
            }
            else {
                return '';
            }
        };
        CrawlingTable.prototype.numberFormat = function (val, decimalPlaces) {
            if (isNaN(parseFloat(val))) {
                return '0';
            }
            else if (val !== null) {
                return parseFloat(parseFloat(val).toFixed(decimalPlaces));
            }
        };
        CrawlingTable.prototype.getUnactivePageStatuses = function (statuses) {
            var filterId = this.filterId, filterIdArray = ['0', '1', '67', '68', '69', '70'];
            if (filterId !== undefined) {
                var filterArray_1 = filterId;
                if (typeof (filterId) !== 'number') {
                    filterArray_1 = filterId.split(',');
                    $.each(filterArray_1, function (ind, val) {
                        if ($.inArray(val, filterIdArray) < 0) {
                            $.each(statuses, function (index, value) {
                                if ($.inArray(value.id, filterArray_1) == -1) {
                                    statuses[index]['name'] = 'unactive';
                                }
                                else {
                                    statuses[index]['name'] = 'active';
                                }
                            });
                        }
                    });
                }
                else {
                    $.each(statuses, function (index, value) {
                        if ($.inArray(value.id, filterArray_1) == -1) {
                            statuses[index]['name'] = 'unactive';
                        }
                        else {
                            statuses[index]['name'] = 'active';
                        }
                    });
                }
            }
            return statuses;
        };
        CrawlingTable.prototype.getPageStatuses = function (page, duplicate_titles, duplicate_h1, duplicate_description) {
            var statuses = [];
            var httpStatusCode = page.httpStatusCode;
            var contentLength = page.contentLength, textContentLength = page.textContentLength;
            if (httpStatusCode == 200) {
                if (page.contentType === 'text/html') {
                    if (page.canonicalCount === 0) {
                        statuses.push({ "class": "blue", 'title': "No Canonical", 'id': '58' });
                    }
                    else {
                        if (page.canonical === '') {
                            statuses.push({ "class": "blue", 'title': "Empty Canonical", 'id': '65' });
                        }
                    }
                    if (page.canonical != page.url && page.canonical != '') {
                        statuses.push({ "class": "yellow", 'title': "Canonical ≠ URL", 'id': '9' });
                    }
                    if (page.titleCount === 0) {
                        statuses.push({ "class": "red", 'title': "No Title Tag", 'id': '59' });
                    }
                    else {
                        if (page.title === '') {
                            statuses.push({ "class": "red", 'title': "Empty Title", 'id': '43' });
                        }
                        if (page.titleCount > 1) {
                            statuses.push({ "class": "red", 'title': "TWO TITLE TAGS", 'id': '60' });
                        }
                        if (duplicate_titles.includes(page.url)) {
                            statuses.push({ "class": "yellow", 'title': "DUPLICATE TITLE", 'id': '3' });
                        }
                        if (page.title.length > 70) {
                            statuses.push({ "class": "blue", 'title': "LONG TITLE", 'id': '44' });
                        }
                        if (page.title.length < 30) {
                            statuses.push({ "class": "blue", 'title': "SHORT TITLE", 'id': '45' });
                        }
                    }
                    if (page.h1Count === 0) {
                        statuses.push({ "class": "red", 'title': "NO H1 TAG", 'id': '57' });
                    }
                    else {
                        if (page.h1 === '') {
                            statuses.push({ "class": "red", 'title': "Empty H1", 'id': '46' });
                        }
                        if (page.h1Count > 1) {
                            statuses.push({ "class": "red", 'title': "TWO H1 TAGS", 'id': '31' });
                        }
                        if (duplicate_h1.includes(page.url)) {
                            statuses.push({ "class": "yellow", 'title': "DUPLICATE H1", 'id': '27' });
                        }
                        if (page.h1.length > 70) {
                            statuses.push({ "class": "blue", 'title': "LONG H1", 'id': '47' });
                        }
                        if (page.h1.length < 5) {
                            statuses.push({ "class": "blue", 'title': "SHORT H1", 'id': '48' });
                        }
                        if (page.h1 === page.title) {
                            statuses.push({ "class": "blue", 'title': "H1 = TITLE", 'id': '5' });
                        }
                    }
                    if (page.descriptionCount === 0) {
                        statuses.push({ "class": "red", 'title': "NO DESCRIPTION TAG", 'id': '62' });
                    }
                    else {
                        if (page.description === '') {
                            statuses.push({ "class": "red", 'title': "Empty Description", 'id': '49' });
                        }
                        if (page.descriptionCount > 1) {
                            statuses.push({ "class": "red", 'title': "TWO DESCRIPTION TAGS", 'id': '63' });
                        }
                        if (duplicate_description.includes(page.url)) {
                            statuses.push({ "class": "yellow", 'title': "DUPLICATE DESCRIPTION", 'id': '32' });
                        }
                        if (page.description.length > 320) {
                            statuses.push({ "class": "blue", 'title': "LONG DESCRIPTION", 'id': '50' });
                        }
                        if (page.description.length < 70) {
                            statuses.push({ "class": "blue", 'title': "SHORT DESCRIPTION", 'id': '51' });
                        }
                        if (page.description === page.title) {
                            statuses.push({ "class": "blue", 'title': "DESCRIPTION = TITLE", 'id': '66' });
                        }
                    }
                }
                if ((contentLength === 0) || (((textContentLength / (contentLength / 100)) < 10))) {
                    statuses.push({ "class": "blue", 'title': "LOW CODE RATIO", 'id': '30' });
                }
                if (contentLength < 500) {
                    statuses.push({ "class": "blue", 'title': "THIN PAGE", 'id': '33' });
                }
            }
            if (httpStatusCode >= 300 && page.redirectToStatusCode >= 300) {
                statuses.push({ "class": "red", 'title': "Redirect Chain", 'id': '42' });
            }
            if (httpStatusCode >= 500 && httpStatusCode < 599) {
                statuses.push({ "class": "red", 'title': "502 Server Error", 'id': '26' });
            }
            if (page.bodyCount > 1) {
                statuses.push({ "class": "red", 'title': "Duplicate Body", 'id': '36' });
            }
            if (page.canonical !== '' && httpStatusCode !== 200) {
                statuses.push({ "class": "yellow", 'title': "NON-200 CANONICAL", 'id': '10' });
            }
            if (httpStatusCode >= 400 && httpStatusCode < 499) {
                statuses.push({ "class": "yellow", 'title': "404 Page", 'id': '25' });
            }
            if (page.url.length > 80) {
                statuses.push({ "class": "blue", 'title': "LONG URL", 'id': '35' });
            }
            if (httpStatusCode == 301) {
                statuses.push({ "class": "blue", 'title': "301 Redirect", 'id': '6' });
            }
            if (httpStatusCode == 302) {
                statuses.push({ "class": "blue", 'title': "302 Redirect", 'id': '64' });
            }
            if (httpStatusCode >= 303 && httpStatusCode < 400) {
                statuses.push({ "class": "blue", 'title': "3XX Redirect", 'id': '24' });
            }
            if (!page.robotsTxtAll) {
                statuses.push({ "class": "blue", 'title': "DISALLOWED", 'id': '22' });
            }
            if (page.noindex === true) {
                statuses.push({ "class": "blue", 'title': "Noindex", 'id': '28' });
            }
            if (page.nofollow === true) {
                statuses.push({ "class": "blue", 'title': "Nofollow", 'id': '13' });
            }
            return statuses;
        };
        CrawlingTable.prototype.getStatusCodeColumn = function (page) {
            var domain = window.location.protocol + "//" + window.location.host;
            return "\n                " + (page.httpStatusCode < 300 || page.httpStatusCode >= 400 ?
                "\n                        <span style=\"color: " + this.getStatusCodeIcon(parseInt(page.httpStatusCode)) + "\">" + page.httpStatusCode + "</span>\n                    " :
                "\n                        <span style=\"color: " + this.getStatusCodeWithColor(parseInt(page.httpStatusCode)) + "\">" + page.httpStatusCode + "</span> <img class=\"crawler_statistics-arrow\" src=\"/wp-content/themes/sitechecker/out/img_design/arrow__down.svg\" alt=\"arrow\" title=\"arrow\">\n                        " + (page.redirectToStatusCode === 0 ?
                    "\n                                <span data-toggle=\"tooltip\" data-placement=\"top\" title=\"" + page.location + "\">???</span>\n                            " :
                    "\n                                <span data-toggle=\"tooltip\" data-placement=\"top\" title=\"" + page.location + "\" style=\"color: " + this.getStatusCodeWithColor(parseInt(page.redirectToStatusCode)) + "\" >" + page.redirectToStatusCode + "</span>\n                            ") + " \n                    ") + "\n            ";
        };
        CrawlingTable.prototype.addFilters = function (request) {
            request.searchParamUrl = $('#crawling_search').val();
            if (this.filterId !== null) {
                request.customFilter = this.filterId;
            }
            return request;
        };
        CrawlingTable.prototype.changeFilter = function (filterId, filterTitle, filterDescription, whatItIsImportant, pageSize) {
            $('.overview__table').removeClass('overview__count').attr('name', filterId);
            $('#crawling_search').val('');
            $('#pageTablePagination').css('opacity', '1');
            var param = $('.overview__tables .title__name');
            param.find('span').text(filterTitle).attr('name', filterId);
            if (filterDescription !== 'undefined') {
                param.find('.tooltip').text(filterDescription);
                param.find('.description').fadeIn();
            }
            else {
                param.find('.description').css('display', 'none');
            }
            if (whatItIsImportant !== undefined) {
                param.find('.important__overview-name').addClass('show').css('display', 'block');
                $('.important__overview-content').addClass('show').find('p').text(whatItIsImportant);
            }
            else {
                param.find('.important__overview-name').removeClass('show').css('display', 'none');
                $('.important__overview-content').removeClass('open').removeClass('show').css('display', 'none');
            }
            $('#pageTable tbody').empty().append('<tr class="loading"><td colspan="5"><div class="loading"><img src="/wp-content/themes/sitechecker/out/img_design/loading-blue.svg" alt="loading" title="loading"></div></td></tr>');
            if (filterId !== '0') {
                $('.overview__block, .overview__graph').css('display', 'none');
            }
            else {
                $('.overview__block, .overview__graph').fadeIn().css('display', 'flex');
                this.filterId = 1;
            }
            this.filterId = filterId;
            this.page = 1;
            if (pageSize) {
                this.pageSize = pageSize;
            }
            this.display();
        };
        CrawlingTable.prototype.getStatusCodeWithColor = function (statusCode) {
            var color = '#FF4B55';
            if (statusCode >= 200 && statusCode < 300) {
                color = "#50D166";
            }
            else if (statusCode > 300 && statusCode < 400) {
                color = "#FFB300";
            }
            return color;
        };
        CrawlingTable.prototype.getStatusCodeIcon = function (statusCode) {
            if (statusCode >= 200 && statusCode < 300) {
                return "#50D166";
            }
            else if (statusCode > 300 && statusCode < 400) {
                return "#FFB300";
            }
            else {
                return "#FF4B55";
            }
        };
        return CrawlingTable;
    }(Crawling.CrawlingBaseTable));
    Crawling.CrawlingTable = CrawlingTable;
})(Crawling || (Crawling = {}));
/// <reference path="CrawlingBaseTable.ts"/>
/// <reference path="../../../../../../typings/globals/jquery/index.d.ts" />
var Crawling;
(function (Crawling) {
    var InProgressCrawlingTable = /** @class */ (function (_super) {
        __extends(InProgressCrawlingTable, _super);
        function InProgressCrawlingTable(baseUrls, domainUrl, token) {
            var _this = _super.call(this, baseUrls, '/inProgress/getWeightByDomains', token) || this;
            _this.lastDateFrom = 0;
            _this.domainUrl = domainUrl;
            _this.sort_url = $('#sort_url');
            _this.sort_weight = $('#sort_weight');
            _this.sort_status = $('#sort_status_code');
            _this.sort_sitemap = $('#sort_sitemap');
            _this.pageSize = 10;
            return _this;
        }
        InProgressCrawlingTable.prototype.getSortParam = function () {
            return 'dateCreate';
        };
        InProgressCrawlingTable.prototype.getPagination = function () {
            return $('#pageTablePagination');
        };
        InProgressCrawlingTable.prototype.getTableElements = function () {
            return $('.table_elements');
        };
        InProgressCrawlingTable.prototype.getTable = function () {
            return $('#pageTable');
        };
        InProgressCrawlingTable.prototype.getTBody = function () {
            return this.getTable().find("tbody");
        };
        InProgressCrawlingTable.prototype.getCrawlingDate = function () {
            return $('#crawling_date');
        };
        InProgressCrawlingTable.prototype.setPagination = function () {
            var html = "\n                <div>\n                    <div class=\"dataTables_info\" id=\"pageTable_info\" role=\"status\" aria-live=\"polite\">\n                        Showing " + ((this.page * this.pageSize) - this.pageSize) + " to " + (this.page * this.pageSize > this.totalCount ? this.totalCount : this.page * this.pageSize) + "  of " + this.totalCount + " entries\n                    </div>\n                </div>\n                <div>\n                    <div class=\"dataTables_paginate paging_simple_numbers\" id=\"pageTable_paginate\">\n                        <ul class=\"pagination\">\n                            <li class=\"page-item disabled\">\n                                <span>Page " + this.page + " from " + this.totalPages + "</span>\n                            </li>\n                            <li class=\"paginate_button page-item previous " + (this.page > 1 ? '' : 'disabled') + "\">\n                                <a href=\"javascript:void(0);\" aria-controls=\"pageTable\" class=\"page-link\" " + (this.page > 1 ? "onclick=\"crawlingTable.previousPage();\"" : '') + "  >Previous</a>\n                            </li>\n                            <li class=\"paginate_button page-item " + (this.page < this.totalPages ? '' : 'disabled') + "\">\n                                <a href=\"javascript:void(0);\" aria-controls=\"pageTable\"  class=\"page-link\" " + (this.page < this.totalPages ? "onclick=\"crawlingTable.nextPage()\"" : '') + " >Next</a>\n                            </li>\n                        </ul>\n                    </div>\n                </div>\n            ";
            // this.getPagination().html(html);
        };
        ;
        InProgressCrawlingTable.prototype.checkNewData = function () {
            var _this = this;
            var requestBody = this.getRequestData();
            requestBody.lastDateFrom = this.lastDateFrom;
            var request = {
                'data': [
                    requestBody
                ]
            };
            var data = JSON.stringify(request);
            this.crawlerRequest(data).then(function (data) { return _this.processNewRows(data); }, function (error) { return _this.logFailed(error); });
        };
        InProgressCrawlingTable.prototype.processNewRows = function (data) {
            var result = data[0];
            var summary = result.summary;
            this.drawNewRows(summary);
        };
        InProgressCrawlingTable.prototype.drawNewRows = function (rows) {
            var _this = this;
            $('#pageTable tbody tr.loading').css('display', 'none');
            rows.forEach(function (page, index) {
                var countOfRows = _this.getTBody().find('tr').length;
                if (_this.lastDateFrom < parseInt(page.dateCreate)) {
                    _this.lastDateFrom = parseInt(page.dateCreate);
                }
                var update = true;
                _this.getTBody().prepend(_this.renderRow(page, index, update));
                if (countOfRows + 1 >= _this.pageSize) {
                    _this.getTable().find('tbody tr:last-of-type').remove();
                }
            });
        };
        InProgressCrawlingTable.prototype.getRequestData = function () {
            return this.addFilters({
                url: this.domainUrl,
                sortParam: this.sortParam,
                sortBy: this.sortBy,
                pageSize: this.pageSize,
                page: this.page,
                token: this.token
            });
        };
        InProgressCrawlingTable.prototype.sort = function (column) {
            if (column === this.sortParam) {
                this.sortBy = this.sortBy === 'asc' ? 'desc' : 'asc';
            }
            else {
                this.sortBy = 'asc';
                this.sortParam = column;
            }
            this.sort_url.attr('class', 'sorting');
            this.sort_weight.attr('class', 'sorting');
            this.sort_status.attr('class', 'sorting');
            this.sort_sitemap.attr('class', 'sorting');
            if (column === 'weight') {
                this.sort_weight.attr('class', 'sorting_' + this.sortBy);
            }
            else if (column === 'http_status_code') {
                this.sort_status.attr('class', 'sorting_' + this.sortBy);
            }
            else {
                this.sort_url.attr('class', 'sorting_' + this.sortBy);
            }
            this.display();
        };
        ;
        InProgressCrawlingTable.prototype.toggleRowData = function (index) {
            var row = $('#row_' + index);
            var rowIcon = row.find('.icon');
            var rowData = $('#row_data_' + index);
            if (row.hasClass("dataShow")) {
                row.removeClass('dataShow');
                rowIcon.text('+').css('font-size', '16px');
                rowData.hide();
            }
            else {
                row.addClass("dataShow");
                rowIcon.text('-').css('font-size', '25px');
                rowData.show();
            }
        };
        InProgressCrawlingTable.prototype.drawRows = function (rows) {
            var _this = this;
            rows.forEach(function (page, index) {
                if (_this.lastDateFrom < parseInt(page.dateDrom)) {
                    _this.lastDateFrom = parseInt(page.dateDrom);
                }
                var update = false;
                _this.addTableRow(_this.renderRow(page, index, update));
            });
        };
        InProgressCrawlingTable.prototype.renderRow = function (page, index, update) {
            var _this = this;
            $('tr.gradient').css('display', 'none').fadeIn().css('display', 'table-row');
            var statuses = this.getPageStatuses(page);
            var contentLength, updateClass, statusColor;
            switch (page.httpStatusCode.toString()) {
                case '200':
                    statusColor = 'success';
                    break;
                case '301':
                    statusColor = 'warning';
                    break;
                case '404':
                    statusColor = 'error';
                    break;
                default:
                    statusColor = 'warning';
                    break;
            }
            if (update == true) {
                contentLength = '<svg class="lds-message" width="100%" height="100%" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 100 100" preserveAspectRatio="xMidYMid" style="background: none;"><g transform="translate(25 50)"><circle cx="0" cy="0" r="6" fill="#267DFF" transform="scale(0.966961 0.966961)"><animateTransform attributeName="transform" type="scale" begin="-0.3333333333333333s" calcMode="spline" keySplines="0.3 0 0.7 1;0.3 0 0.7 1" values="0;1;0" keyTimes="0;0.5;1" dur="1s" repeatCount="indefinite"></animateTransform></circle></g><g transform="translate(50 50)"><circle cx="0" cy="0" r="6" fill="#267DFF" transform="scale(0.597389 0.597389)"><animateTransform attributeName="transform" type="scale" begin="-0.16666666666666666s" calcMode="spline" keySplines="0.3 0 0.7 1;0.3 0 0.7 1" values="0;1;0" keyTimes="0;0.5;1" dur="1s" repeatCount="indefinite"></animateTransform></circle></g><g transform="translate(75 50)"><circle cx="0" cy="0" r="6" fill="#267DFF" transform="scale(0.146741 0.146741)"><animateTransform attributeName="transform" type="scale" begin="0s" calcMode="spline" keySplines="0.3 0 0.7 1;0.3 0 0.7 1" values="0;1;0" keyTimes="0;0.5;1" dur="1s" repeatCount="indefinite"></animateTransform>\n                                </circle></g></svg>';
            }
            else {
                contentLength = '';
            }
            return "\n                <tr id=\"row_" + index + "\" class=\"gradient " + statusColor + "\">\n                    <td>\n                        <div class=\"loading\">" + contentLength + "</div>\n                    </td>\n                    <td>\n                        <div class=\"external-link\"><a href=\"" + page.url + "\" target=\"_blank\">" + page.url + "</a><a href=\"" + page.url + "\" target=\"_blank\"><img src=\"/wp-content/themes/sitechecker/out/img_design/external.svg\" alt=\"external\" title=\"external\"></a></div>\n                    </td>\n                    <td class=\"overview__status-td\">\n                        " + this.getStatusCodeColumn(page) + "\n                    </td>\n                    <td><div class=\"loading\">\n                    <svg class=\"lds-message\" width=\"100%\" height=\"100%\" xmlns=\"http://www.w3.org/2000/svg\"\n                         xmlns:xlink=\"http://www.w3.org/1999/xlink\" viewBox=\"0 0 100 100\" preserveAspectRatio=\"xMidYMid\"\n                         style=\"background: none;\">\n                        <g transform=\"translate(25 50)\">\n                            <circle cx=\"0\" cy=\"0\" r=\"6\" fill=\"#267DFF\" transform=\"scale(0.966961 0.966961)\">\n                                <animateTransform attributeName=\"transform\" type=\"scale\" begin=\"-0.3333333333333333s\"\n                                                  calcMode=\"spline\" keySplines=\"0.3 0 0.7 1;0.3 0 0.7 1\" values=\"0;1;0\"\n                                                  keyTimes=\"0;0.5;1\" dur=\"1s\"\n                                                  repeatCount=\"indefinite\"></animateTransform>\n                            </circle>\n                        </g>\n                        <g transform=\"translate(50 50)\">\n                            <circle cx=\"0\" cy=\"0\" r=\"6\" fill=\"#267DFF\" transform=\"scale(0.597389 0.597389)\">\n                                <animateTransform attributeName=\"transform\" type=\"scale\" begin=\"-0.16666666666666666s\"\n                                                  calcMode=\"spline\" keySplines=\"0.3 0 0.7 1;0.3 0 0.7 1\" values=\"0;1;0\"\n                                                  keyTimes=\"0;0.5;1\" dur=\"1s\"\n                                                  repeatCount=\"indefinite\"></animateTransform>\n                            </circle>\n                        </g>\n                        <g transform=\"translate(75 50)\">\n                            <circle cx=\"0\" cy=\"0\" r=\"6\" fill=\"#267DFF\" transform=\"scale(0.146741 0.146741)\">\n                                <animateTransform attributeName=\"transform\" type=\"scale\" begin=\"0s\" calcMode=\"spline\"\n                                                  keySplines=\"0.3 0 0.7 1;0.3 0 0.7 1\" values=\"0;1;0\" keyTimes=\"0;0.5;1\"\n                                                  dur=\"1s\" repeatCount=\"indefinite\"></animateTransform>\n                            </circle>\n                        </g>\n                    </svg></div></td>\n                    <td>\n                        <div class=\"messages\">\n                            " + statuses
                .map(function (status) { return "<a href=\"" + _this.baseUrls.pageInformation + "?url=" + page.crc32Url + "&token=" + _this.token + "\" class=\"" + status["class"] + "\">" + status.title + "</a>"; })
                .reduce(function (prev, curr) { return prev + curr; }, '') + "\n                        </div>\n                    </td>\n                </tr>\n            ";
        };
        InProgressCrawlingTable.prototype.numberFormat = function (val, decimalPlaces) {
            return parseFloat(parseFloat(val).toFixed(decimalPlaces));
        };
        InProgressCrawlingTable.prototype.getPageStatuses = function (page) {
            var statuses = [];
            var httpStatusCode = page.httpStatusCode;
            if (httpStatusCode == 200 && page.contentType === 'text/html') {
                if (page.canonicalCount === 0) {
                    statuses.push({ "class": "blue", 'title': "No Canonical" });
                }
                else {
                    if (page.canonical != page.url) {
                        statuses.push({ "class": "yellow", 'title': "Canonical ≠ URL" });
                    }
                    if (page.canonical === '') {
                        statuses.push({ "class": "blue", 'title': "Empty Canonical" });
                    }
                }
                if (page.titleCount === 0) {
                    statuses.push({ "class": "red", 'title': "No Title Tag" });
                }
                else {
                    if (page.title === '') {
                        statuses.push({ "class": "red", 'title': "Empty Title" });
                    }
                    if (page.titleCount > 1) {
                        statuses.push({ "class": "red", 'title': "TWO TITLE TAGS" });
                    }
                    if (page.title.length > 70) {
                        statuses.push({ "class": "blue", 'title': "LONG TITLE" });
                    }
                    if (page.title.length < 30) {
                        statuses.push({ "class": "blue", 'title': "SHORT TITLE" });
                    }
                }
                if (page.h1Count === 0) {
                    statuses.push({ "class": "red", 'title': "NO H1 TAG" });
                }
                else {
                    if (page.h1 === '') {
                        statuses.push({ "class": "blue", 'title': "Empty H1" });
                    }
                    if (page.h1Count > 1) {
                        statuses.push({ "class": "red", 'title': "TWO H1 TAGS" });
                    }
                    if (page.h1.length > 70) {
                        statuses.push({ "class": "blue", 'title': "LONG H1" });
                    }
                    if (page.h1.length < 30) {
                        statuses.push({ "class": "blue", 'title': "SHORT H1" });
                    }
                    if (page.h1 === page.title) {
                        statuses.push({ "class": "blue", 'title': "H1 = TITLE" });
                    }
                }
                if (page.descriptionCount === 0) {
                    statuses.push({ "class": "red", 'title': "NO DESCRIPTION TAG" });
                }
                else {
                    if (page.description === '') {
                        statuses.push({ "class": "blue", 'title': "Empty Description" });
                    }
                    if (page.descriptionCount > 1) {
                        statuses.push({ "class": "red", 'title': "TWO DESCRIPTION TAGS" });
                    }
                    if (page.description.length > 320) {
                        statuses.push({ "class": "blue", 'title': "LONG DESCRIPTION" });
                    }
                    if (page.description.length < 70) {
                        statuses.push({ "class": "blue", 'title': "SHORT DESCRIPTION" });
                    }
                    if (page.description === page.title) {
                        statuses.push({ "class": "blue", 'title': "DESCRIPTION = TITLE" });
                    }
                }
                if (page.bodyCount < 500) {
                    statuses.push({ "class": "blue", 'title': "THIN PAGE" });
                }
                if ((Math.round((page.textContentLength / page.contentLength) * 100)) < 10) {
                    statuses.push({ "class": "blue", 'title': "LOW CODE RATIO" });
                }
                if (page.url.length > 80) {
                    statuses.push({ "class": "blue", 'title': "LONG URL" });
                }
            }
            if (httpStatusCode == 301) {
                statuses.push({ "class": "blue", 'title': "301 Redirect" });
            }
            if (httpStatusCode == 302) {
                statuses.push({ "class": "blue", 'title': "302 Redirect" });
            }
            if (httpStatusCode >= 303 && httpStatusCode < 400) {
                statuses.push({ "class": "blue", 'title': "3XX Redirect" });
            }
            if (httpStatusCode >= 400 && httpStatusCode < 499) {
                statuses.push({ "class": "yellow", 'title': "404 Page" });
            }
            if (httpStatusCode >= 300 && page.redirectToStatusCode >= 300) {
                statuses.push({ "class": "red", 'title': "Redirect Chain" });
            }
            if (httpStatusCode >= 500 && httpStatusCode < 599) {
                statuses.push({ "class": "red", 'title': "502 Server Error" });
            }
            if (page.noindex === true) {
                statuses.push({ "class": "blue", 'title': "Noindex" });
            }
            if (page.nofollow === true) {
                statuses.push({ "class": "blue", 'title': "Nofollow" });
            }
            return statuses;
        };
        InProgressCrawlingTable.prototype.getStatusCodeColumn = function (page) {
            var domain = window.location.protocol + "//" + window.location.host;
            return "\n                " + (page.httpStatusCode < 300 || page.httpStatusCode >= 400 ?
                "\n                        <span style=\"color: " + this.getStatusCodeIcon(parseInt(page.httpStatusCode)) + "\">" + page.httpStatusCode + "</span>\n                    " :
                "\n                        <span style=\"color: " + this.getStatusCodeWithColor(parseInt(page.httpStatusCode)) + "\">" + page.httpStatusCode + "</span> <img class=\"crawler_statistics-arrow\" src=\"" + domain + "/images/arrow_down.svg\" alt=\"arrow\" title=\"arrow\">\n                        " + (page.redirectToStatusCode === 0 ?
                    "\n                                <span data-toggle=\"tooltip\" data-placement=\"top\" title=\"" + page.location + "\">???</span>\n                            " :
                    "\n                                <span data-toggle=\"tooltip\" data-placement=\"top\" title=\"" + page.location + "\" style=\"color: " + this.getStatusCodeWithColor(parseInt(page.redirectToStatusCode)) + "\" >" + page.redirectToStatusCode + "</span>\n                            ") + " \n                    ") + "\n            ";
        };
        InProgressCrawlingTable.prototype.addFilters = function (request) {
            request.searchParamUrl = $('#crawling_search').val();
            if (this.filterId !== null) {
                request.customFilter = this.filterId;
            }
            return request;
        };
        InProgressCrawlingTable.prototype.changeFilter = function (filterId) {
            this.filterId = filterId;
            this.page = 1;
            this.pageSize = 10;
            this.display();
        };
        InProgressCrawlingTable.prototype.getStatusCodeWithColor = function (statusCode) {
            var color = '#f24c27';
            if (statusCode >= 200 && statusCode < 300) {
                color = "#56c504";
            }
            else if (statusCode > 300 && statusCode < 400) {
                color = "#fd9425";
            }
            return color;
        };
        InProgressCrawlingTable.prototype.getStatusCodeIcon = function (statusCode) {
            if (statusCode >= 200 && statusCode < 300) {
                return "#56c504";
            }
            else if (statusCode > 300 && statusCode < 400) {
                return "#fd9425";
            }
            else {
                return "#f24c27";
            }
        };
        return InProgressCrawlingTable;
    }(Crawling.CrawlingBaseTable));
    Crawling.InProgressCrawlingTable = InProgressCrawlingTable;
})(Crawling || (Crawling = {}));
/// <reference path="CrawlingBaseTable.ts"/>
/// <reference path="../../../../../../typings/globals/jquery/index.d.ts" />
var Crawling;
(function (Crawling) {
    var MonitoringTableForPage = /** @class */ (function (_super) {
        __extends(MonitoringTableForPage, _super);
        function MonitoringTableForPage(baseUrls, requestPath, token, url) {
            var _this = _super.call(this, baseUrls) || this;
            _this.token = token;
            _this.url = url;
            _this.requestPath = requestPath;
            _this.pageSize = 10;
            return _this;
        }
        MonitoringTableForPage.prototype.processResult = function (response) {
            this.totalCount = response.totalCount;
            this.filteredCount = response.filteredCount;
            this.totalPages = response.pages;
            var events = response.summary;
            this.drawRows(events);
        };
        MonitoringTableForPage.prototype.nextPage = function () {
            if (this.page * this.pageSize < this.totalCount) {
                this.page++;
                this.display();
            }
            else {
                $('.load__more').css('display', 'none');
                $('.monitoring__history-point.started').fadeIn();
            }
        };
        MonitoringTableForPage.prototype.getSortParam = function () {
            return 'date_create';
        };
        MonitoringTableForPage.prototype.getPagination = function () {
            return $('#pageTablePagination');
        };
        MonitoringTableForPage.prototype.getTable = function () {
            return $('.history__block');
        };
        MonitoringTableForPage.prototype.getTBody = function () {
            return this.getTable();
        };
        MonitoringTableForPage.prototype.getCrawlingDate = function () {
            return $('#crawling_date');
        };
        MonitoringTableForPage.prototype.sort = function (column) {
            if (column === this.sortParam) {
                this.sortBy = 'desc';
            }
            else {
                this.sortBy = 'desc';
                this.sortParam = column;
            }
            this.display();
        };
        ;
        MonitoringTableForPage.prototype.drawRows = function (events) {
            var _this = this;
            if (events.length == 0) {
                $('.load__more').css('display', 'none');
                $('.monitoring__history-point.started').fadeIn();
            }
            else {
                $('.load__more').css('display', 'flex');
            }
            events.forEach(function (event) {
                var row = _this.renderEvent(event);
                _this.addTableRow(row);
            });
        };
        MonitoringTableForPage.prototype.logFailed = function (log) {
            console.log("error" + log);
        };
        MonitoringTableForPage.prototype.display = function () {
            var _this = this;
            this.getPageData().then(function (data) { return _this.processResult(data); }, function (error) { return _this.logFailed(error); });
        };
        ;
        MonitoringTableForPage.prototype.getRequestData = function () {
            return {
                sortParam: this.sortParam,
                sortBy: this.sortBy,
                pageSize: this.pageSize,
                page: this.page,
                token: this.token,
                url: this.url
            };
        };
        MonitoringTableForPage.prototype.getPageData = function () {
            var request = this.getRequestData(), data = JSON.stringify(request);
            return this.monitoringRequest(data);
        };
        MonitoringTableForPage.prototype.monitoringRequest = function (data) {
            return this.doRequest({
                path: this.requestPath,
                action: 'crawler_request',
                data: data,
                method: 'POST'
            });
        };
        MonitoringTableForPage.prototype.renderEvent = function (event) {
            var weekday = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"], monthNames = ["January", "February", "March", "April", "May", "June",
                "July", "August", "September", "October", "November", "December"
            ];
            function formatAMPM(date) {
                var hours = date.getHours(), minutes = date.getMinutes(), ampm = hours >= 12 ? 'pm' : 'am';
                hours = hours % 12;
                hours = hours ? hours : 12;
                minutes = minutes < 10 ? '0' + minutes : minutes;
                return hours + ':' + minutes + ' ' + ampm;
            }
            var dateFormat = new Date(event.dateCreate * 1000), date = formatAMPM(dateFormat), dateDay = weekday[dateFormat.getDay()] + ' ' + monthNames[dateFormat.getMonth()] + ' ' + dateFormat.getDate() + ', ' + dateFormat.getFullYear();
            var pagesChanged = JSON.parse(event.changeData);
            var resultBlocks = pagesChanged.filter(function (page) {
                var pageChangeDataItem = page.param;
                return pageChangeDataItem !== undefined;
            }).map(function (page) {
                var pageParam = page.param;
                var image, title, className, pageChanged = page.changed, pagePrevious = page.previous;
                switch (pageParam) {
                    case 'title':
                        image = 'event_html';
                        title = 'Title has changed';
                        className = 'title';
                        break;
                    case 'description':
                        image = 'event_html';
                        title = 'Description has changed';
                        className = 'description';
                        break;
                    case 'text_content':
                        image = 'event_html';
                        title = 'Content has changed';
                        className = 'text_content';
                        break;
                    case 'h1':
                        image = 'event_html';
                        title = 'H1 has changed';
                        className = 'h1';
                        break;
                    case 'http_status_code':
                        image = 'event_server';
                        title = 'HTTP status code has changed';
                        className = 'http_status_code';
                        break;
                    case 'robots_txt_all':
                        image = 'event_server';
                        title = 'Robots.txt has changed';
                        className = 'robots_txt_all';
                        if (pageChanged === false) {
                            pageChanged = 'URL disallowed';
                        }
                        else {
                            pageChanged = 'URL allowed';
                        }
                        if (pagePrevious === false) {
                            pagePrevious = 'URL disallowed';
                        }
                        else {
                            pagePrevious = 'URL allowed';
                        }
                        break;
                    case 'noindex':
                        image = 'event_server';
                        title = 'Index status has changed';
                        className = 'noindex';
                        if (pagePrevious === true) {
                            pagePrevious = 'URL indexation closed by noindex tag';
                        }
                        else {
                            pagePrevious = 'URL indexation opened by index tag';
                        }
                        if (pageChanged === true) {
                            pageChanged = 'URL indexation closed by noindex tag';
                        }
                        else {
                            pageChanged = 'URL indexation opened by index tag';
                        }
                        break;
                    default:
                        image = 'event_edit';
                        break;
                }
                return "<div class=\"event-box domain_monitoring\">\n                            <div class=\"header\"><img src=\"/wp-content/themes/sitechecker/out/img_design/monitoring_status.svg\" alt=\"status\" title=\"" + title + "\">" + title + "<p>" + date + "</p>\n                            <svg width=\"9\" height=\"5\" viewBox=\"0 0 9 5\" fill=\"none\" xmlns=\"http://www.w3.org/2000/svg\"><path fill-rule=\"evenodd\" clip-rule=\"evenodd\" d=\"M4.499 0L9 2.5V4.896L4.499 2.38L0 5V2.5L4.499 0Z\" transform=\"translate(9 5) rotate(-180)\" fill=\"#cccccc\"></path></svg></div>\n                            <div class=\"body\">\n                                <div class=\"container\">\n                                    <div class=\"changes\">\n                                        <p class=\"" + className + "\">" + pagePrevious + "</p>\n                                        <img src=\"/wp-content/themes/sitechecker/out/img_design/arrow_monitoring.svg\" alt=\"arrow\" title=\"arrow\">\n                                        <p class=\"" + className + "\">" + pageChanged + "</p>\n                                    </div>\n                                </div>\n                            </div>\n                        </div>";
            }).join('\n');
            var timeLineHTML = '', historyPoint = $('.monitoring__history-point[name="' + dateDay + '"]');
            if (historyPoint.length > 0) {
                historyPoint.find('.monitoring__history-event').append(resultBlocks);
            }
            else {
                timeLineHTML = '<div class="monitoring__history-point" name="' + dateDay + '">' +
                    '<p class="history-point-date">' + dateDay + '</p>' +
                    '<div class="monitoring__history-event">' + resultBlocks + '</div></div>';
            }
            $('.monitoring__history-content').fadeIn();
            return timeLineHTML;
        };
        MonitoringTableForPage.prototype.getUrlForRequest = function () {
            return this.baseUrls.crawlRequest;
        };
        return MonitoringTableForPage;
    }(Crawling.BaseTable));
    Crawling.MonitoringTableForPage = MonitoringTableForPage;
})(Crawling || (Crawling = {}));
/// <reference path="CrawlingBaseTable.ts"/>
/// <reference path="../../../../../../typings/globals/jquery/index.d.ts" />
var Crawling;
(function (Crawling) {
    var MonitoringTabTable = /** @class */ (function (_super) {
        __extends(MonitoringTabTable, _super);
        function MonitoringTabTable(baseUrls, requestPath, token, interval, filterDate, searchParamUrl, isIndex, dateFrom, dateTill) {
            var _this = _super.call(this, baseUrls) || this;
            _this.requestPath = requestPath;
            _this.sortParam = 'date_create';
            _this.pageSize = 10;
            _this.token = token;
            _this.interval = interval;
            _this.filterDate = filterDate;
            _this.searchParamUrl = searchParamUrl;
            _this.isIndex = isIndex;
            _this.dateFrom = dateFrom;
            _this.dateTill = dateTill;
            return _this;
        }
        MonitoringTabTable.prototype.processResult = function (response) {
            this.totalCount = response.totalCount;
            this.filteredCount = response.filteredCount;
            this.totalPages = response.pages;
            var events = response.summary;
            var searchParam = false;
            if (this.searchParamUrl !== undefined && this.searchParamUrl !== '' || this.isIndex === true) {
                searchParam = true;
            }
            if (events.length > 0) {
                $('.history__block').removeClass('empty');
                if (this.filterDate !== undefined && this.filterDate !== '') {
                    $('.load__more-filter').removeClass('active').fadeIn();
                }
                else {
                    $('.load__more').removeClass('active').fadeIn();
                }
                this.drawRows(events);
            }
            else if (events.length === 0 && response.pages === 0) {
                $('.load__more, .load__more-filter, .monitoring-loader, .monitoring__graph').css('display', 'none');
                $('.monitoring__history-point.started').fadeIn();
                $('.load__more, .load__more-filter, .monitoring-loader').css('display', 'none');
            }
            else if (events.length === 0 && response.pages > 0) {
                if (searchParam) {
                    $('.monitoring__history-point.started, .load__more, .monitoring-loader').css('display', 'none');
                    $('.history__block').empty().addClass('empty').append('<p class="not__found">URL not found in this domain</p>');
                }
                else {
                    $('.monitoring__history-point.started').fadeIn();
                    $('.load__more, .load__more-filter, .monitoring-loader').css('display', 'none');
                }
            }
        };
        MonitoringTabTable.prototype.nextPage = function () {
            if (this.page * this.pageSize < this.totalCount) {
                this.page++;
                this.display();
            }
            else {
                $('.monitoring__history-point.started').fadeIn();
                $('.load__more, .load__more-filter').css('display', 'none');
            }
        };
        MonitoringTabTable.prototype.getSortParam = function () {
            return 'weight';
        };
        MonitoringTabTable.prototype.getPagination = function () {
            return $('#pageTablePagination');
        };
        MonitoringTabTable.prototype.getTable = function () {
            return $('.history__block');
        };
        MonitoringTabTable.prototype.getTBody = function () {
            return this.getTable();
        };
        MonitoringTabTable.prototype.getCrawlingDate = function () {
            return $('#crawling_date');
        };
        MonitoringTabTable.prototype.sort = function (column) {
            if (column === this.sortParam) {
                this.sortBy = this.sortBy === 'asc' ? 'desc' : 'asc';
            }
            else {
                this.sortBy = 'asc';
                this.sortParam = column;
            }
            this.display();
        };
        ;
        MonitoringTabTable.prototype.drawRows = function (events) {
            var _this = this;
            events.forEach(function (event) {
                var row = _this.renderEvent(event);
                _this.addTableRow(row);
            });
            $('#next-second .fa').css('display', 'none');
        };
        MonitoringTabTable.prototype.logFailed = function (log) {
            console.log("error" + log);
        };
        MonitoringTabTable.prototype.display = function () {
            var _this = this;
            this.getPageData().then(function (data) { return _this.processResult(data); }, function (error) { return _this.logFailed(error); });
        };
        ;
        MonitoringTabTable.prototype.getRequestData = function () {
            return {
                sortParam: this.sortParam,
                sortBy: this.sortBy,
                pageSize: this.pageSize,
                page: this.page,
                token: this.token,
                interval: this.interval,
                filterDate: this.filterDate,
                searchParamUrl: this.searchParamUrl,
                isIndex: this.isIndex,
                dateFrom: this.dateFrom,
                dateTill: this.dateTill
            };
        };
        MonitoringTabTable.prototype.getPageData = function () {
            var request = this.getRequestData(), data = JSON.stringify(request);
            return this.monitoringRequest(data);
        };
        MonitoringTabTable.prototype.monitoringRequest = function (data) {
            return this.doRequest({
                path: this.requestPath,
                action: 'crawler_request',
                data: data,
                method: 'POST'
            });
        };
        MonitoringTabTable.prototype.renderEvent = function (event) {
            var _this = this;
            var weekday = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"], monthNames = ["January", "February", "March", "April", "May", "June",
                "July", "August", "September", "October", "November", "December"
            ];
            function formatAMPM(date) {
                var hours = date.getHours(), minutes = date.getMinutes(), ampm = hours >= 12 ? 'pm' : 'am';
                hours = hours % 12;
                hours = hours ? hours : 12;
                minutes = minutes < 10 ? '0' + minutes : minutes;
                return hours + ':' + minutes + ' ' + ampm;
            }
            var dateFormat = new Date(event.monitoringStatistic.dateCreate * 1000), date = formatAMPM(dateFormat), dateDay = weekday[dateFormat.getDay()] + ' ' + monthNames[dateFormat.getMonth()] + ' ' + dateFormat.getDate() + ', ' + dateFormat.getFullYear();
            var id = event.monitoringStatistic.id;
            var pagesAdded = JSON.parse(event.pagesAdded);
            var pagesAddedMore = 0, pagesAddedDisplay = 'none';
            if (pagesAdded.length > 50) {
                pagesAddedMore = pagesAdded.length - 50;
                pagesAddedDisplay = 'block';
            }
            var pageAddedArray = pagesAdded.map(function (page, index) {
                var pageUrl = page.url;
                if (index < 50) {
                    return "<li>\n                            <div class=\"external-link\"><a href=\"" + _this.baseUrls.pageInformation + "?url=" + page.crc32url + "&token=" + _this.token + "\">" + page.url + "</a><a href=\"" + pageUrl + "\" target=\"_blank\"><img src=\"/wp-content/themes/sitechecker/out/img_design/external.svg\" alt=\"external\" title=\"external\"></a></div>\n                        </li>";
                }
            }).join('\n');
            var timeLineNewPages = "\n                <div class=\"header\">\n                    <img src=\"/wp-content/themes/sitechecker/out/img_design/monitoring_add.svg\" alt=\"\">" + pagesAdded.length + " new pages\n                    <p class=\"date\">" + date + "</p>\n                    <svg width=\"9\" height=\"5\" viewBox=\"0 0 9 5\" fill=\"none\" xmlns=\"http://www.w3.org/2000/svg\">\n<path fill-rule=\"evenodd\" clip-rule=\"evenodd\" d=\"M4.499 0L9 2.5V4.896L4.499 2.38L0 5V2.5L4.499 0Z\" transform=\"translate(9 5) rotate(-180)\" fill=\"#cccccc\"/></svg></div>\n                <div class=\"body\">\n                    <div class=\"container\">\n                        <ul class=\"added\">\n                            " + pageAddedArray + "\n                        </ul>\n                        <p class=\"more\" style=\"display: " + pagesAddedDisplay + "\">and " + pagesAddedMore + " more pages</p>\n                    </div>\n                </div>\n            ";
            var pagesDeleted = JSON.parse(event.pagesDeleted);
            var pagesDeletedMore = 0, pagesDeletedDisplay = 'none';
            if (pagesDeleted.length > 50) {
                pagesDeletedMore = pagesDeleted.length - 50;
                pagesDeletedDisplay = 'block';
            }
            var pageDeletedArray = pagesDeleted.map(function (page, index) {
                var pageUrl = page.url;
                if (index < 50) {
                    return "<li><span>" + pageUrl + "</span></li> ";
                }
            }).join('\n');
            var timeLineDeletedPages = "\n                <div class=\"header\">\n                    <img src=\"/wp-content/themes/sitechecker/out/img_design/monitoring_delete.svg\" alt=\"\">" + pagesDeleted.length + " deleted pages\n                    <p class=\"date\">" + date + "</p>\n                    <svg width=\"9\" height=\"5\" viewBox=\"0 0 9 5\" fill=\"none\" xmlns=\"http://www.w3.org/2000/svg\">\n<path fill-rule=\"evenodd\" clip-rule=\"evenodd\" d=\"M4.499 0L9 2.5V4.896L4.499 2.38L0 5V2.5L4.499 0Z\" transform=\"translate(9 5) rotate(-180)\" fill=\"#cccccc\"/></svg></div>\n                <div class=\"body\">\n                    <div class=\"container\">\n                        <ul class=\"deleted\">\n                            " + pageDeletedArray + "\n                        </ul>\n                        <p class=\"more\" style=\"display: " + pagesDeletedDisplay + "\">and " + pagesDeletedMore + " more pages</p>\n                    </div>\n                </div>\n            ";
            var pagesBroken = JSON.parse(event.pagesBroken);
            var pagesBrokenMore = 0, pagesBrokenDisplay = 'none';
            if (pagesBroken.length > 50) {
                pagesBrokenMore = pagesBroken.length - 50;
                pagesBrokenDisplay = 'block';
            }
            var pageBrokenArray = pagesBroken.map(function (page, index) {
                var pageUrl = page.url;
                if (index < 50) {
                    return "<li><span>" + pageUrl + "</span></li> ";
                }
            }).join('\n');
            var timeLineBrokenPages = "\n                <div class=\"header\">\n                    <img src=\"/wp-content/themes/sitechecker/out/img_design/monitoring_delete.svg\" alt=\"\">" + pagesBroken.length + " broken pages\n                    <p class=\"date\">" + date + "</p>\n                    <svg width=\"9\" height=\"5\" viewBox=\"0 0 9 5\" fill=\"none\" xmlns=\"http://www.w3.org/2000/svg\">\n<path fill-rule=\"evenodd\" clip-rule=\"evenodd\" d=\"M4.499 0L9 2.5V4.896L4.499 2.38L0 5V2.5L4.499 0Z\" transform=\"translate(9 5) rotate(-180)\" fill=\"#cccccc\"/></svg></div>\n                <div class=\"body\">\n                    <div class=\"container\">\n                        <ul class=\"deleted\">\n                            " + pageBrokenArray + "\n                        </ul>\n                        <p class=\"more\" style=\"display: " + pagesBrokenDisplay + "\">and " + pagesBrokenMore + " more pages</p>\n                    </div>\n                </div>\n            ";
            var pagesOrphan = JSON.parse(event.pagesOrphan);
            var pagesOrphanMore = 0, pagesOrphanDisplay = 'none';
            if (pagesOrphan.length > 50) {
                pagesOrphanMore = pagesOrphan.length - 50;
                pagesOrphanDisplay = 'block';
            }
            var pageOrphanArray = pagesOrphan.map(function (page, index) {
                var pageUrl = page.url;
                if (index < 50) {
                    return "<li><span>" + pageUrl + "</span></li> ";
                }
            }).join('\n');
            var timeLineOrphanPages = "\n                <div class=\"header\">\n                    <img src=\"/wp-content/themes/sitechecker/out/img_design/monitoring_delete.svg\" alt=\"\">" + pagesOrphan.length + " orphan pages\n                    <p class=\"date\">" + date + "</p>\n                    <svg width=\"9\" height=\"5\" viewBox=\"0 0 9 5\" fill=\"none\" xmlns=\"http://www.w3.org/2000/svg\">\n<path fill-rule=\"evenodd\" clip-rule=\"evenodd\" d=\"M4.499 0L9 2.5V4.896L4.499 2.38L0 5V2.5L4.499 0Z\" transform=\"translate(9 5) rotate(-180)\" fill=\"#cccccc\"/></svg></div>\n                <div class=\"body\">\n                    <div class=\"container\">\n                        <ul class=\"deleted\">\n                            " + pageOrphanArray + "\n                        </ul>\n                        <p class=\"more\" style=\"display: " + pagesOrphanDisplay + "\">and " + pagesOrphanMore + " more pages</p>\n                    </div>\n                </div>\n            ";
            var pagesChanged = JSON.parse(event.pagesChanged);
            function getPagesByChangeTitle(pagesChanged, changeTitle) {
                return pagesChanged.filter(function (page) {
                    var pageChangeDataItem = page.change_data.find(function (pageChange) { return pageChange.param === changeTitle; });
                    return pageChangeDataItem !== undefined;
                }).map(function (page) {
                    var pageChangeDataItem = page.change_data.find(function (pageChange) { return pageChange.param === changeTitle; });
                    return {
                        url: page.url,
                        crc32url: page.crc32url,
                        previous: pageChangeDataItem.previous,
                        changed: pageChangeDataItem.changed,
                        data: pageChangeDataItem.data
                    };
                });
            }
            var resultBlocks = "";
            if (pagesChanged.length > 0) {
                var changeParams = [
                    {
                        paramTitle: 'title',
                        image: 'title',
                        title: 'Title has changed',
                        needsCheckForTitle: false,
                        changedElements: getPagesByChangeTitle(pagesChanged, 'title')
                    },
                    {
                        paramTitle: 'description',
                        image: 'title',
                        title: 'Description has changed',
                        needsCheckForTitle: false,
                        changedElements: getPagesByChangeTitle(pagesChanged, 'description')
                    },
                    {
                        paramTitle: 'text_content',
                        image: 'title',
                        title: 'Content has changed',
                        needsCheckForTitle: false,
                        changedElements: getPagesByChangeTitle(pagesChanged, 'text_content')
                    },
                    {
                        paramTitle: 'h1',
                        image: 'title',
                        title: 'H1 has changed',
                        needsCheckForTitle: false,
                        changedElements: getPagesByChangeTitle(pagesChanged, 'h1')
                    },
                    {
                        paramTitle: 'http_status_code',
                        image: 'status',
                        title: 'HTTP status code has changed',
                        needsCheckForTitle: false,
                        changedElements: getPagesByChangeTitle(pagesChanged, 'http_status_code')
                    },
                    {
                        paramTitle: 'robots_txt_all',
                        image: 'status',
                        title: 'Index status has changed',
                        needsCheckForTitle: true,
                        falseTitle: '<span class="allow">URL allowed by robots.txt</span>',
                        trueTitle: '<span class="disallow">URL disallowed by robots.txt</span>',
                        changedElements: getPagesByChangeTitle(pagesChanged, 'robots_txt_all')
                    },
                    {
                        paramTitle: 'robots_txt_content',
                        image: 'robots',
                        title: 'Robots.txt has changed',
                        needsCheckForTitle: true,
                        falseTitle: '<span class="disallow">URL disallowed</span>',
                        trueTitle: '<span class="allow">URL allowed</span>',
                        changedElements: getPagesByChangeTitle(pagesChanged, 'robots_txt_content')
                    }, {
                        paramTitle: 'noindex',
                        image: 'index',
                        title: 'Index status has changed',
                        needsCheckForTitle: true,
                        falseTitle: '<span class="disallow">URL indexation closed by noindex tag</span>',
                        trueTitle: '<span class="allow">URL indexation opened by index tag</span>',
                        changedElements: getPagesByChangeTitle(pagesChanged, 'noindex')
                    },
                    {
                        paramTitle: 'www_redirect',
                        image: 'status',
                        title: 'WWW redirect has changed',
                        needsCheckForTitle: true,
                        falseTitle: '<span class="disallow">www / non-www redirect is not working</span>',
                        trueTitle: '<span class="allow">www / non-www redirect is working</span>',
                        changedElements: getPagesByChangeTitle(pagesChanged, 'www_redirect')
                    },
                    {
                        paramTitle: 'https_redirect',
                        image: 'status',
                        title: 'HTTPS redirect has changed',
                        needsCheckForTitle: true,
                        falseTitle: '<span class="disallow">HTTP to HTTPS redirect is not working</span>',
                        trueTitle: '<span class="allow">HTTP to HTTPS redirect is working</span>',
                        changedElements: getPagesByChangeTitle(pagesChanged, 'https_redirect')
                    }
                ].filter(function (changeParam) {
                    return changeParam.changedElements.length > 0;
                });
                resultBlocks = changeParams.map(function (changeParam) {
                    var changedElements = changeParam.changedElements;
                    var changedArray = changedElements.map(function (page, index) {
                        var previous = page.changed, changed = page.previous, data = page.data, stuff = [], output = '', array = '', elementSring;
                        $(data).each(function (i, item) {
                            if (changeParam.paramTitle === 'text_content') {
                                array = item.text.split("\n");
                            }
                            else {
                                array = item.text.split("\r\n");
                            }
                            $(array).each(function (i, element) {
                                element = element.trim();
                                elementSring = { string: element, view: item.operation };
                                stuff.push(elementSring);
                            });
                        });
                        $(stuff).each(function (i, item) {
                            var elementStrings = item.string.split("\n"), elementView = item.view;
                            $.each(elementStrings, function (i, elementString) {
                                if (elementView === "DELETE" && elementString !== '') {
                                    if (changeParam.paramTitle === 'text_content') {
                                        output = output + '<li class="robots-delete"><code><xmp>' + elementString + '</xmp></code></li>';
                                    }
                                    else {
                                        output = output + '<li class="robots-delete">' + elementString + '</li>';
                                    }
                                }
                                else if (elementView === 'INSERT' && elementString !== '') {
                                    if (changeParam.paramTitle === 'text_content') {
                                        output = output + '<li class="robots-added"><code><xmp>' + elementString + '</xmp></code></li>';
                                    }
                                    else {
                                        output = output + '<li class="robots-added">' + elementString + '</li>';
                                    }
                                }
                                else if (elementString !== '') {
                                    if (changeParam.paramTitle === 'text_content') {
                                        output = output + '<li class="robots-current"><code><xmp>' + elementString + '</xmp></code></li>';
                                    }
                                    else {
                                        output = output + '<li class="robots-current">' + elementString + '</li>';
                                    }
                                }
                            });
                        });
                        if (data) {
                            if (changeParam.paramTitle === 'text_content') {
                                return "<div class=\"body\">\n                                <div class=\"container\">\n                                    <div class=\"url\">URL: <div class=\"external-link\"><a href=\"" + _this.baseUrls.pageInformation + "?url=" + page.crc32url + "&token=" + _this.token + "\">" + page.url + "</a><a href=\"" + page.url + "\" target=\"_blank\"><img src=\"/wp-content/themes/sitechecker/out/img_design/external.svg\" alt=\"external\" title=\"external\"></a></div></div>\n                                    <div class=\"robots__changes\">" + output + "</div>\n                                </div></div>";
                            }
                            else {
                                return "<div class=\"body\">\n                                <div class=\"container\"><div class=\"robots__changes\">" + output + "</div></div></div>";
                            }
                        }
                        else {
                            if (changeParam.needsCheckForTitle === true) {
                                changed = page.changed === false ? changeParam.falseTitle : changeParam.trueTitle;
                                previous = page.previous === false ? changeParam.falseTitle : changeParam.trueTitle;
                            }
                            if (index < 50) {
                                return "\n                                <div class=\"body\">\n                                    <div class=\"container\">\n                                        <div class=\"url\">URL: <div class=\"external-link\"><a href=\"" + _this.baseUrls.pageInformation + "?url=" + page.crc32url + "&token=" + _this.token + "\">" + page.url + "</a><a href=\"" + page.url + "\" target=\"_blank\"><img src=\"/wp-content/themes/sitechecker/out/img_design/external.svg\" alt=\"external\" title=\"external\"></a></div></div>\n                                        <div class=\"changes\">\n                                            <p class=\"" + changeParam.paramTitle + "\">" + previous + "</p>\n                                            <img src=\"/wp-content/themes/sitechecker/out/img_design/arrow_monitoring.svg\" alt=\"arrow\" title=\"arrow\">\n                                            <p class=\"" + changeParam.paramTitle + "\">" + changed + "</p>\n                                        </div>\n                                    </div>\n                                </div>\n                            ";
                            }
                        }
                    });
                    var changeArrayHTML = changedArray.join('\n');
                    var changeWorld = changedArray.length === 1 ? 'change' : 'changes', pagesChangesMore = 0, pagesChangesDisplay = 'none';
                    if (changedArray.length > 50) {
                        pagesChangesMore = changedArray.length - 50;
                        pagesChangesDisplay = 'block';
                    }
                    return "\n                    <div class=\"monitoring__history-event\">\n                        <div class=\"header\"><img src=\"/wp-content/themes/sitechecker/out/img_design/monitoring_" + changeParam.image + ".svg\" alt=\"" + changeParam.image + "\" title=\"" + changeParam.title + "\">" + changeParam.title + "\n                        <p class=\"changes\">" + changedArray.length + " " + changeWorld + "</p>\n                        <p>" + date + "</p>\n                        <svg width=\"9\" height=\"5\" viewBox=\"0 0 9 5\" fill=\"none\" xmlns=\"http://www.w3.org/2000/svg\"><path fill-rule=\"evenodd\" clip-rule=\"evenodd\" d=\"M4.499 0L9 2.5V4.896L4.499 2.38L0 5V2.5L4.499 0Z\" transform=\"translate(9 5) rotate(-180)\" fill=\"#cccccc\"/></svg></div>\n                        " + changeArrayHTML + "\n                        <div class=\"body\" style=\"display: " + pagesChangesDisplay + "\">\n                            <div class=\"container\">\n                                <p class=\"more_changes\">and " + pagesChangesMore + " more pages</p>\n                            </div>\n                        </div>\n                    </div>\n                ";
                }).join('\n');
            }
            var timeLineHTML, newPages, deletedPages, brokenPages, orphanPages;
            if (pageAddedArray.length !== 0) {
                newPages = "<div class=\"monitoring__history-event added\">" + timeLineNewPages + "</div>";
            }
            else {
                newPages = "";
            }
            if (pageDeletedArray.length !== 0) {
                deletedPages = "<div class=\"monitoring__history-event deleted\">" + timeLineDeletedPages + "</div>";
            }
            else {
                deletedPages = "";
            }
            if (pagesBroken.length !== 0) {
                brokenPages = "<div class=\"monitoring__history-event\">" + timeLineBrokenPages + "</div>";
            }
            else {
                brokenPages = "";
            }
            if (pagesOrphan.length !== 0) {
                orphanPages = "<div class=\"monitoring__history-event\">" + timeLineOrphanPages + "</div>";
            }
            else {
                orphanPages = "";
            }
            if (pageAddedArray.length == 0 && pageDeletedArray.length == 0 && pagesChanged.length == 0 && pagesBroken.length == 0 && pagesOrphan.length == 0) {
                timeLineHTML = '';
            }
            else {
                var historyPoint = $('.monitoring__history-point[name="' + dateDay + '"]');
                if (historyPoint.length > 0) {
                    historyPoint.find('.content').append(deletedPages + newPages + brokenPages + orphanPages + resultBlocks);
                }
                else {
                    timeLineHTML = '<div class="monitoring__history-point" name="' + dateDay + '">' +
                        '<p class="history-point-date">' + dateDay + '</p>' +
                        '<div class="content">' + deletedPages + newPages + brokenPages + orphanPages + resultBlocks + '</div></div>';
                }
            }
            $('.monitoring-loader').css('display', 'none');
            $('.monitoring__history-content').fadeIn();
            $('.robots__changes .robots-current').each(function () {
                var element = $(this), nextElement = element.next();
                if (nextElement.hasClass('robots-added') || nextElement.hasClass('robots-delete')) {
                    element.css({
                        'height': 'auto'
                    });
                }
            });
            return timeLineHTML;
        };
        MonitoringTabTable.prototype.getUrlForRequest = function () {
            return this.baseUrls.crawlRequest;
        };
        return MonitoringTabTable;
    }(Crawling.BaseTable));
    Crawling.MonitoringTabTable = MonitoringTabTable;
})(Crawling || (Crawling = {}));
/// <reference path="../CrawlingBaseTable.ts"/>
var Crawling;
(function (Crawling) {
    var Popup;
    (function (Popup) {
        var PopupTable = /** @class */ (function (_super) {
            __extends(PopupTable, _super);
            function PopupTable(urlForRequest, requestPath, id, token) {
                var _this = _super.call(this, {}, requestPath, token) || this;
                _this.id = id;
                _this.urlForRequest = urlForRequest;
                $('#popup_search').val('');
                _this.getPopup().show();
                _this.display();
                return _this;
            }
            PopupTable.prototype.display = function () {
                var _this = this;
                this.getLoader().css('opacity', '1');
                this.getPageData().then(function (data) { return _this.processResult(data); }, function (error) { return _this.logFailed(error); });
            };
            ;
            PopupTable.prototype.getPopup = function () {
                if (!$('main').hasClass('domainPage')) {
                    $('body').css('overflow', 'hidden');
                }
                return $('#popup');
            };
            PopupTable.prototype.sort = function (column) { };
            PopupTable.prototype.getPagination = function () {
                return $('#popup_pagination');
            };
            PopupTable.prototype.getTableElements = function () {
                return $('.popup_table_elements');
            };
            PopupTable.prototype.getTable = function () {
                return $('#popup_table');
            };
            PopupTable.prototype.getTBody = function () {
                return this.getTable().find("tbody");
            };
            PopupTable.prototype.getCrawlingDate = function () {
                return $('#popupCrawlingDate');
            };
            PopupTable.prototype.setPagination = function () {
                var html = "\n                <div class=\"dataTables_info\" id=\"pageTable_info\" role=\"status\" aria-live=\"polite\">\n                    Showing " + ((this.page * this.pageSize) - this.pageSize) + " to " + (this.page * this.pageSize > this.totalCount ? this.totalCount : this.page * this.pageSize) + "  of " + this.totalCount + " entries\n                </div>\n                <div class=\"dataTables_paginate paging_simple_numbers\" id=\"pageTable_paginate\">\n                    <ul class=\"pagination\">\n                        <li class=\"page-item disabled\">\n                            <span>Page " + this.page + " from " + this.totalPages + "</span>\n                        </li>\n                        <li class=\"paginate_button page-item previous " + (this.page > 1 ? '' : 'disabled') + "\">\n                            <a href=\"javascript:void(0);\" aria-controls=\"pageTable\" class=\"page-link\" " + (this.page > 1 ? "onclick=\"popupTable.previousPage();\"" : '') + "  >Previous</a>\n                        </li>\n                        <li class=\"paginate_button page-item " + (this.page < this.totalPages ? '' : 'disabled') + "\">\n                            <a href=\"javascript:void(0);\" aria-controls=\"pageTable\"  class=\"page-link\" " + (this.page < this.totalPages ? "onclick=\"popupTable.nextPage()\"" : '') + " >Next</a>\n                        </li>\n                    </ul>\n                </div>\n            ";
                this.getPagination().html(html);
            };
            ;
            PopupTable.prototype.getPopupRowDataHTML = function (data, index) {
                var content = Object.keys(data).map(function (key) {
                    var value = data[key];
                    return "\n                <div class=\"col-xs-12 col-sm-6 col-lg-4\">\n                    <div class=\"detail_title\">" + key + ": " + value + "</div>\n                </div>\n            ";
                }).reduce(function (previousValue, currentValue) {
                    return previousValue + currentValue;
                });
                return "\n                <tr id=\"popup_row_data_" + index + "\" style=\"display: none\">\n                    <td colspan=\"5\">\n                       <div>\n                            " + content + "\n                        </div>\n                    </td>\n                </tr>\n            ";
            };
            PopupTable.prototype.getRequestData = function () {
                return this.addFilters({
                    id: this.id,
                    sortParam: this.sortParam,
                    sortBy: this.sortBy,
                    pageSize: this.pageSize,
                    page: this.page,
                    token: this.token
                });
            };
            PopupTable.prototype.close = function () {
                this.getPopup().hide();
            };
            PopupTable.prototype.toggleRowData = function (index) {
                var row = $('#popup_row_' + index);
                var rowIcon = row.find('.icon');
                var rowData = $('#popup_row_data_' + index);
                if (row.hasClass("dataShow")) {
                    row.removeClass('dataShow');
                    rowIcon.removeClass("fa-minus");
                    rowIcon.addClass("fa-plus");
                    rowData.hide();
                }
                else {
                    row.addClass("dataShow");
                    rowIcon.removeClass("fa-plus");
                    rowIcon.addClass("fa-minus");
                    rowData.show();
                }
            };
            PopupTable.prototype.parseCrawledDate = function (_a) {
                var timestamp = _a.checked;
                return new Date(timestamp * 1000);
            };
            PopupTable.prototype.addFilters = function (request) {
                request.searchParamUrl = $('#popup_search').val();
                return request;
            };
            PopupTable.prototype.getUrlForRequest = function () {
                return this.urlForRequest;
            };
            return PopupTable;
        }(Crawling.CrawlingBaseTable));
        Popup.PopupTable = PopupTable;
    })(Popup = Crawling.Popup || (Crawling.Popup = {}));
})(Crawling || (Crawling = {}));
/// <reference path="PopupTable.ts"/>
var Crawling;
(function (Crawling) {
    var Popup;
    (function (Popup) {
        var ExternalPopupTable = /** @class */ (function (_super) {
            __extends(ExternalPopupTable, _super);
            function ExternalPopupTable(urlForRequest, url, token) {
                var _this = _super.call(this, urlForRequest, 'getExternalLinks', url, token) || this;
                $('.popup__export').click(function () {
                    exportLinks('external', url, token);
                });
                $('#popup_table tbody').empty();
                return _this;
            }
            ExternalPopupTable.prototype.getSortParam = function () {
                return 'url';
            };
            ExternalPopupTable.prototype.drawRows = function (rows) {
                var _this = this;
                $('.crawler_popup-title').text('External links');
                $('#overview__popup').fadeIn().css('display', 'flex');
                rows.forEach(function (row, index) {
                    var rowData = _this.getPopupRowDataHTML(row, index);
                    var tr = "\n                    <tr id=\"popup_row_" + index + "\" >\n                        <td>\n                            <div class=\"external-link\">\n                            <a href=\"/tool/page-information/?url=" + row.crc32Url + "&token=" + _this.token + "\">" + row.url + "</a>\n                            <a href=\"" + row.url + "\" target=\"_blank\"><img src=\"/wp-content/themes/sitechecker/out/img_design/external.svg\" alt=\"external\" title=\"external\"></a>\n                            </div>\n                        </td>\n                        <td>" + (row.link_anchor || '-') + "</td>\n                    </tr>\n                    " + rowData + "\n                ";
                    _this.addTableRow(tr);
                });
                $('.additional_param').hide(); //если ссылки внутренние дополнительные колонки не нужны
            };
            return ExternalPopupTable;
        }(Popup.PopupTable));
        Popup.ExternalPopupTable = ExternalPopupTable;
    })(Popup = Crawling.Popup || (Crawling.Popup = {}));
})(Crawling || (Crawling = {}));
/// <reference path="../popup/ExternalPopupTable.ts"/>
var Crawling;
(function (Crawling) {
    var Popup;
    (function (Popup) {
        var ExternalPopupTable = Crawling.Popup.ExternalPopupTable;
        var ExternalTable = /** @class */ (function (_super) {
            __extends(ExternalTable, _super);
            function ExternalTable(baseUrls, url, token) {
                return _super.call(this, baseUrls, url, token) || this;
            }
            ExternalTable.prototype.setPagination = function () {
                var html = "\n                <div>\n                    <div class=\"dataTables_info\" id=\"pageTable_info\" role=\"status\" aria-live=\"polite\">\n                        Showing " + ((this.page * this.pageSize) - this.pageSize) + " to " + (this.page * this.pageSize > this.totalCount ? this.totalCount : this.page * this.pageSize) + "  of " + this.totalCount + " entries\n                    </div>\n                </div>\n                <div>\n                    <div class=\"dataTables_paginate paging_simple_numbers\" id=\"pageTable_paginate\">\n                        <ul class=\"pagination\">\n                            <li class=\"page-item disabled\">\n                                <span>Page " + this.page + " from " + this.totalPages + "</span>\n                            </li>\n                            <li class=\"paginate_button page-item previous " + (this.page > 1 ? '' : 'disabled') + "\">\n                                <a href=\"javascript:void(0);\" aria-controls=\"pageTable\" class=\"page-link\" " + (this.page > 1 ? "onclick=\"popoupTable.previousPage();\"" : '') + "  >Previous</a>\n                            </li>\n                            <li class=\"paginate_button page-item " + (this.page < this.totalPages ? '' : 'disabled') + "\">\n                                <a href=\"javascript:void(0);\" aria-controls=\"pageTable\"  class=\"page-link\" " + (this.page < this.totalPages ? "onclick=\"popoupTable.nextPage()\"" : '') + " >Next</a>\n                            </li>\n                        </ul>\n                    </div>\n                </div>\n            ";
                this.getPagination().html(html);
            };
            return ExternalTable;
        }(ExternalPopupTable));
        Popup.ExternalTable = ExternalTable;
    })(Popup = Crawling.Popup || (Crawling.Popup = {}));
})(Crawling || (Crawling = {}));
/// <reference path="PopupTable.ts"/>
var Crawling;
(function (Crawling) {
    var Popup;
    (function (Popup) {
        var IncomingPopupTable = /** @class */ (function (_super) {
            __extends(IncomingPopupTable, _super);
            function IncomingPopupTable(urlForRequest, url, token) {
                var _this = _super.call(this, urlForRequest, 'getIncomingLinks', url, token) || this;
                $('.popup__export').click(function () {
                    exportLinks('incoming', url, token);
                });
                $('#popup_table tbody').empty();
                return _this;
            }
            IncomingPopupTable.prototype.getSortParam = function () {
                return 'weight';
            };
            IncomingPopupTable.prototype.drawRows = function (rows) {
                var _this = this;
                $('.crawler_popup-title').text('Anchors');
                $('#overview__popup').fadeIn().css('display', 'flex');
                rows.forEach(function (row, index) {
                    var rowData = _this.getPopupRowDataHTML(row, index);
                    var tr = "\n                    <tr id=\"popup_row_" + index + "\" >\n                        <td>\n                            <div class=\"external-link\">\n                            <a href=\"/tool/page-information/?url=" + row.crc32Url + "&token=" + _this.token + "\">" + row.url + "</a>\n                            <a href=\"" + row.url + "\" target=\"_blank\"><img src=\"/wp-content/themes/sitechecker/out/img_design/external.svg\" alt=\"external\" title=\"external\"></a>\n                            </div>\n                        </td>\n                        <td>" + (row.link_anchor || '-') + "</td>\n                        <td>" + parseInt(row.normalized_weight) + "</td>\n                        <td>\n                            <span style=\"color: " + _this.getStatusCodeIcon(parseInt(row.http_status_code)) + "\">" + row.http_status_code + "</span>\n                        </td>\n                    </tr>\n                    " + rowData + "\n                ";
                    $('.additional_param').show();
                    _this.addTableRow(tr);
                });
            };
            return IncomingPopupTable;
        }(Popup.PopupTable));
        Popup.IncomingPopupTable = IncomingPopupTable;
    })(Popup = Crawling.Popup || (Crawling.Popup = {}));
})(Crawling || (Crawling = {}));
/// <reference path="../popup/IncomingPopupTable.ts"/>
var Crawling;
(function (Crawling) {
    var Popup;
    (function (Popup) {
        var IncomingPopupTable = Crawling.Popup.IncomingPopupTable;
        var IncomingTable = /** @class */ (function (_super) {
            __extends(IncomingTable, _super);
            function IncomingTable(baseUrls, url, token) {
                return _super.call(this, baseUrls, url, token) || this;
            }
            IncomingTable.prototype.setPagination = function () {
                var html = "\n                <div>\n                    <div class=\"dataTables_info\" id=\"pageTable_info\" role=\"status\" aria-live=\"polite\">\n                        Showing " + ((this.page * this.pageSize) - this.pageSize) + " to " + (this.page * this.pageSize > this.totalCount ? this.totalCount : this.page * this.pageSize) + "  of " + this.totalCount + " entries\n                    </div>\n                </div>\n                <div>\n                    <div class=\"dataTables_paginate paging_simple_numbers\" id=\"pageTable_paginate\">\n                        <ul class=\"pagination\">\n                            <li class=\"page-item disabled\">\n                                <span>Page " + this.page + " from " + this.totalPages + "</span>\n                            </li>\n                            <li class=\"paginate_button page-item previous " + (this.page > 1 ? '' : 'disabled') + "\">\n                                <a href=\"javascript:void(0);\" aria-controls=\"pageTable\" class=\"page-link\" " + (this.page > 1 ? "onclick=\"popoupTable.previousPage();\"" : '') + "  >Previous</a>\n                            </li>\n                            <li class=\"paginate_button page-item " + (this.page < this.totalPages ? '' : 'disabled') + "\">\n                                <a href=\"javascript:void(0);\" aria-controls=\"pageTable\"  class=\"page-link\" " + (this.page < this.totalPages ? "onclick=\"popoupTable.nextPage()\"" : '') + " >Next</a>\n                            </li>\n                        </ul>\n                    </div>\n                </div>\n            ";
                this.getPagination().html(html);
            };
            return IncomingTable;
        }(IncomingPopupTable));
        Popup.IncomingTable = IncomingTable;
    })(Popup = Crawling.Popup || (Crawling.Popup = {}));
})(Crawling || (Crawling = {}));
/// <reference path="PopupTable.ts"/>
var Crawling;
(function (Crawling) {
    var Popup;
    (function (Popup) {
        var InternalPopupTable = /** @class */ (function (_super) {
            __extends(InternalPopupTable, _super);
            function InternalPopupTable(urlForRequest, url, token) {
                var _this = _super.call(this, urlForRequest, 'getInternalLinks', url, token) || this;
                $('.popup__export').click(function () {
                    exportLinks('internal', url, token);
                });
                $('#popup_table tbody').empty();
                return _this;
            }
            InternalPopupTable.prototype.getSortParam = function () {
                return 'weight';
            };
            InternalPopupTable.prototype.getPath = function () {
                return '/getInternalLinks';
            };
            InternalPopupTable.prototype.drawRows = function (rows) {
                var _this = this;
                $('.crawler_popup-title').text('Internal links');
                $('#overview__popup').fadeIn().css('display', 'flex');
                rows.forEach(function (row, index) {
                    var rowData = _this.getPopupRowDataHTML(row, index);
                    var tr = "\n                    <tr id=\"popup_row_" + index + "\" >\n                        <td>\n                            <div class=\"external-link\">\n                            <a href=\"/tool/page-information/?url=" + row.crc32Url + "&token=" + _this.token + "\">" + row.url + "</a>\n                            <a href=\"" + row.url + "\" target=\"_blank\"><img src=\"/wp-content/themes/sitechecker/out/img_design/external.svg\" alt=\"external\" title=\"external\"></a>\n                            </div>\n                        </td>\n                        <td>" + (row.link_anchor || '-') + "</td>\n                        <td>" + _this.numberFormat(row.normalized_weight) + "</td>\n                        <td>\n                            <span style=\"color: " + _this.getStatusCodeIcon(parseInt(row.http_status_code)) + "\">" + row.http_status_code + "</span>\n                        </td>\n                    </tr>\n                    " + rowData + "\n                ";
                    $('.additional_param').show();
                    _this.addTableRow(tr);
                });
            };
            InternalPopupTable.prototype.numberFormat = function (val) {
                if (isNaN(parseInt(val))) {
                    return '0';
                }
                else if (val !== null) {
                    return parseInt(val);
                }
            };
            return InternalPopupTable;
        }(Popup.PopupTable));
        Popup.InternalPopupTable = InternalPopupTable;
    })(Popup = Crawling.Popup || (Crawling.Popup = {}));
})(Crawling || (Crawling = {}));
/// <reference path="../popup/InternalPopupTable.ts"/>
var Crawling;
(function (Crawling) {
    var Popup;
    (function (Popup) {
        var InternalPopupTable = Crawling.Popup.InternalPopupTable;
        var InternalTable = /** @class */ (function (_super) {
            __extends(InternalTable, _super);
            function InternalTable(baseUrls, url, token) {
                return _super.call(this, baseUrls, url, token) || this;
            }
            InternalTable.prototype.setPagination = function () {
                var html = "\n                <div>\n                    <div class=\"dataTables_info\" id=\"pageTable_info\" role=\"status\" aria-live=\"polite\">\n                        Showing " + ((this.page * this.pageSize) - this.pageSize) + " to " + (this.page * this.pageSize > this.totalCount ? this.totalCount : this.page * this.pageSize) + "  of " + this.totalCount + " entries\n                    </div>\n                </div>\n                <div>\n                    <div class=\"dataTables_paginate paging_simple_numbers\" id=\"pageTable_paginate\">\n                        <ul class=\"pagination\">\n                            <li class=\"page-item disabled\">\n                                <span>Page " + this.page + " from " + this.totalPages + "</span>\n                            </li>\n                            <li class=\"paginate_button page-item previous " + (this.page > 1 ? '' : 'disabled') + "\">\n                                <a href=\"javascript:void(0);\" aria-controls=\"pageTable\" class=\"page-link\" " + (this.page > 1 ? "onclick=\"popoupTable.previousPage();\"" : '') + "  >Previous</a>\n                            </li>\n                            <li class=\"paginate_button page-item " + (this.page < this.totalPages ? '' : 'disabled') + "\">\n                                <a href=\"javascript:void(0);\" aria-controls=\"pageTable\"  class=\"page-link\" " + (this.page < this.totalPages ? "onclick=\"popoupTable.nextPage()\"" : '') + " >Next</a>\n                            </li>\n                        </ul>\n                    </div>\n                </div>\n            ";
                this.getPagination().html(html);
            };
            return InternalTable;
        }(InternalPopupTable));
        Popup.InternalTable = InternalTable;
    })(Popup = Crawling.Popup || (Crawling.Popup = {}));
})(Crawling || (Crawling = {}));
/// <reference path="PopupTable.ts"/>
var Crawling;
(function (Crawling) {
    var Popup;
    (function (Popup) {
        var DuplicatedLinks = /** @class */ (function (_super) {
            __extends(DuplicatedLinks, _super);
            function DuplicatedLinks(apiPath, url, token, paramForSearch) {
                var _this = _super.call(this, apiPath, '/getDuplicatedLinks', url, token) || this;
                $('#popup_table tbody').empty();
                _this.paramForSearch = paramForSearch;
                _this.display();
                return _this;
            }
            DuplicatedLinks.prototype.init = function () { };
            DuplicatedLinks.prototype.getSortParam = function () {
                return 'url';
            };
            DuplicatedLinks.prototype.getRequestData = function () {
                var requestData = _super.prototype.getRequestData.call(this);
                requestData.param = this.paramForSearch;
                return requestData;
            };
            DuplicatedLinks.prototype.drawRows = function (rows) {
                var _this = this;
                $('#overview__popup').addClass('duplicate__popup').fadeIn().css('display', 'flex');
                var param = 'Title', val = 'title';
                if (this.paramForSearch === 'h1') {
                    param = 'H1';
                    val = 'h1';
                }
                else if (this.paramForSearch === 'description') {
                    param = 'Description';
                    val = 'description';
                }
                $('.crawler_popup-title').html(param + ': <span>' + rows[0][val] + '</span>');
                rows.forEach(function (row, index) {
                    var rowData = _this.getPopupRowDataHTML(row, index);
                    var tr = "\n                    <tr id=\"popup_row_" + index + "\" >\n                        <td>\n                            <div class=\"external-link\">\n                            <a href=\"/tool/page-information/?url=" + row.crc32Url + "&token=" + _this.token + "\">" + row.url + "</a>\n                            <a href=\"" + row.url + "\" target=\"_blank\"><img src=\"/wp-content/themes/sitechecker/out/img_design/external.svg\" alt=\"external\" title=\"external\"></a>\n                            </div>\n                        </td>\n                    </tr>\n                    " + rowData + "\n                ";
                    _this.addTableRow(tr);
                });
                $('.additional_param').hide(); //если ссылки внутренние дополнительные колонки не нужны
            };
            return DuplicatedLinks;
        }(Popup.PopupTable));
        Popup.DuplicatedLinks = DuplicatedLinks;
    })(Popup = Crawling.Popup || (Crawling.Popup = {}));
})(Crawling || (Crawling = {}));
//# sourceMappingURL=crawling.js.map