/// <reference path="CrawlingBaseTable.ts"/>
/// <reference path="../../../../../../typings/globals/jquery/index.d.ts" />

namespace Crawling{

    export class CrawlingTable extends CrawlingBaseTable {

        private sort_weight: JQuery;
        private sort_url: JQuery;
        private sort_status: JQuery;
        private sort_sitemap: JQuery;
        private domainUrl: string;
        private filterId: number;

        constructor(baseUrls: any, domainUrl: string, token: string) {
            super(baseUrls, '/getWeightByDomains', token);

            this.domainUrl = domainUrl;
            this.sort_url = $('#sort_url');
            this.sort_weight = $('#sort_weight');
            this.sort_status = $('#sort_status_code');
            this.sort_sitemap = $('#sort_sitemap');
        }

        protected getSortParam(): string {
            return 'weight';
        }

        protected getPagination() {
            return $('#pageTablePagination');
        }

        protected getTableElements() {
            return $('.table_elements');
        }

        protected getTable() {
            return $('#pageTable');
        }

        protected getTBody() {
            return this.getTable().find("tbody");
        }

        protected getCrawlingDate() {
            return $('#crawling_date');
        }

        protected setPagination() {
            const html = `
                <div>
                    <div class="dataTables_info" id="pageTable_info" role="status" aria-live="polite">
                        Showing ${((this.page * this.pageSize) - this.pageSize)} to ${this.page * this.pageSize > this.totalCount ? this.totalCount : this.page * this.pageSize}  of ${this.totalCount} entries
                    </div>
                </div>
                <div>
                    <div class="dataTables_paginate paging_simple_numbers" id="pageTable_paginate">
                        <ul class="pagination">
                            <li class="page-item disabled">
                                <span>Page ${this.page} from ${this.totalPages}</span>
                            </li>
                            <li class="paginate_button page-item previous ${ this.page > 1 ? '' : 'disabled'}">
                                <a href="javascript:void(0);" aria-controls="pageTable" class="page-link" ${this.page > 1 ? `onclick="crawlingTable.previousPage();"` : ''}  >Previous</a>
                            </li>
                            <li class="paginate_button page-item ${this.page < this.totalPages ? '' : 'disabled'}">
                                <a href="javascript:void(0);" aria-controls="pageTable"  class="page-link" ${this.page < this.totalPages ? `onclick="crawlingTable.nextPage()"` : ''} >Next</a>
                            </li>
                        </ul>
                    </div>
                </div>
            `;

            this.getPagination().html(html);
        };

        protected getRequestData() {
            return this.addFilters({
                url: this.domainUrl,
                sortParam: this.sortParam,
                sortBy: this.sortBy,
                pageSize: this.pageSize,
                page: this.page,
                token: this.token
            });
        }

        public sort(column) {
            if (column === this.sortParam) {
                this.sortBy = this.sortBy === 'asc' ? 'desc' : 'asc';
            } else {
                this.sortBy = 'asc';
                this.sortParam = column;
            }

            this.sort_url.attr('class', 'sorting');
            this.sort_weight.attr('class', 'sorting');
            this.sort_status.attr('class', 'sorting');
            this.sort_sitemap.attr('class', 'sorting');

            if (column === 'weight') {
                this.sort_weight.attr('class', 'sorting_' + this.sortBy);
            } else if (column === 'http_status_code') {
                this.sort_status.attr('class', 'sorting_' + this.sortBy);
            } else {
                this.sort_url.attr('class', 'sorting_' + this.sortBy);
            }

            this.display();
        };

        toggleRowData(index) {
            const row = $('#row_' + index);
            const rowIcon = row.find('.icon');
            const rowData = $('#row_data_' + index);

            if (row.hasClass("dataShow")) {
                row.removeClass('dataShow');
                rowIcon.text('+').css('font-size', '16px');
                rowData.hide();
            } else {
                row.addClass("dataShow");
                rowIcon.text('-').css('font-size', '25px');
                rowData.show();
            }
        }

        drawRows(rows) {
            $('#loader').css('opacity', '0');
            $('#pageTable_wrapper').fadeIn();

            const duplicate_titles = [];
            const duplicate_h1 = [];
            const duplicate_description = [];
            const baseUrls = this.baseUrls;
            const domainURL = this.domainUrl;
            const token = this.token;

            let duplicate_titles_storage = JSON.parse(localStorage.getItem('duplicate_titles_storage_'+token)),
                duplicate_h1_storage = JSON.parse(localStorage.getItem('duplicate_h1_storage_'+token)),
                duplicate_description_storage = JSON.parse(localStorage.getItem('duplicate_description_storage_'+token));

            if(duplicate_titles_storage === null){
                $.ajax({
                    url: baseUrls.crawlRequest,
                    type: "POST",
                    async: false,
                    dataType: "JSON",
                    data: {
                        path: '/getWeightByDomains',
                        method: 'POST',
                        data: JSON.stringify({
                            data: [{
                                url: domainURL,
                                token: token,
                                customFilter: "3"
                            }]
                        })
                    },
                    success: function (data) {
                        let duplicateTitlesUrls = data[0]['summary'];
                        $.each(duplicateTitlesUrls, function (key, value) {
                            duplicate_titles.push(value['url']);
                        });
                        localStorage.setItem('duplicate_titles_storage_'+token, JSON.stringify(duplicate_titles));
                        duplicate_titles_storage = JSON.parse(localStorage.getItem('duplicate_titles_storage_'+token));
                    }
                });
            }
            if(duplicate_h1_storage === null){
                $.ajax({
                    url: baseUrls.crawlRequest,
                    type: "POST",
                    async: false,
                    dataType: "JSON",
                    data: {
                        path: '/getWeightByDomains',
                        method: 'POST',
                        data: JSON.stringify({
                            data: [{
                                url: domainURL,
                                token: token,
                                customFilter: "27"
                            }]
                        })
                    },
                    success: function (data) {
                        let duplicateH1Urls = data[0]['summary'];
                        $.each(duplicateH1Urls, function (key, value) {
                            duplicate_h1.push(value['url']);
                        });
                        localStorage.setItem('duplicate_h1_storage_'+token, JSON.stringify(duplicate_h1));
                        duplicate_h1_storage = JSON.parse(localStorage.getItem('duplicate_h1_storage_'+token));
                    }
                });
            }
            if(duplicate_description_storage === null){
                $.ajax({
                    url: baseUrls.crawlRequest,
                    type: "POST",
                    async: false,
                    dataType: "JSON",
                    data: {
                        path: '/getWeightByDomains',
                        method: 'POST',
                        data: JSON.stringify({
                            data: [{
                                url: domainURL,
                                token: token,
                                customFilter: "32"
                            }]
                        })
                    },
                    success: function (data) {
                        let duplicateDescriptionUrls = data[0]['summary'];
                        $.each(duplicateDescriptionUrls, function (key, value) {
                            duplicate_description.push(value['url']);
                        });
                        localStorage.setItem('duplicate_description_storage_'+token, JSON.stringify(duplicate_description));
                        duplicate_description_storage = JSON.parse(localStorage.getItem('duplicate_description_storage_'+token));
                    }
                });
            }
            rows.forEach((page, index) => {
                this.addTableRow(this.renderRow(page, index, duplicate_titles_storage, duplicate_h1_storage, duplicate_description_storage));
            });
        }

        renderRow(page, index, duplicate_titles, duplicate_h1, duplicate_description){
            const statuses = this.getPageStatuses(page, duplicate_titles, duplicate_h1, duplicate_description);
            const finalStatuses = this.getUnactivePageStatuses(statuses);
            return`
                <tr id="row_${index}">
                    <td class="overview__weight-td">
                        ${this.numberFormat(page.normalizedWeight, 1)}
                    </td>
                    <td class="overview__link-td">
                        <div class="external-link"><a href="${this.baseUrls.pageInformation}?url=${page.crc32Url}&token=${this.token}">${page.url}</a><a href="${page.url}" target="_blank"><img src="/wp-content/themes/sitechecker/out/img_design/external.svg" alt="external" title="external"></a></div>
                       <p class="title">${this.titleExist(page.title)}</p>
                    </td>
                    <td class="overview__status-td"> 
                        ${this.getStatusCodeColumn(page)}
                    </td>
                    <td class="overview__links-td">
                        <p class="crawled__links-btn ${page.externalCountTotal === 0 ? 'empty' : ''}" onclick="popupTable = new Crawling.Popup.ExternalPopupTable('${this.baseUrls.crawlRequest}', '${page.id}', '${this.token}')">External ${page.externalCountTotal !== 0 ? '('+page.externalCountTotal+')' : ''}</p>
                        <p class="crawled__links-btn ${page.internalCountTotal === 0 ? 'empty' : ''}" onclick="popupTable = new Crawling.Popup.InternalPopupTable('${this.baseUrls.crawlRequest}', '${page.id}', '${this.token}')">Internal ${page.internalCountTotal !== 0 ? '('+page.internalCountTotal+')' : ''}</p>
                        <p class="crawled__links-btn ${page.incomingCountTotal === 0 ? 'empty' : ''}" onclick="popupTable = new Crawling.Popup.IncomingPopupTable('${this.baseUrls.crawlRequest}', '${page.id}', '${this.token}')">Anchors ${page.incomingCountTotal !== 0 ? '('+page.incomingCountTotal+')' : ''}</p>
                        <p class="crawled__links-btn duplicated__links-title" onclick="popupTable = new Crawling.Popup.DuplicatedLinks('${this.baseUrls.crawlRequest}', '${page.id}', '${this.token}', 'title')">Show Duplicates</p>
                        <p class="crawled__links-btn duplicated__links-h1" onclick="popupTable = new Crawling.Popup.DuplicatedLinks('${this.baseUrls.crawlRequest}', '${page.id}', '${this.token}', 'h1')">Show Duplicates</p>
                        <p class="crawled__links-btn duplicated__links-descript" onclick="popupTable = new Crawling.Popup.DuplicatedLinks('${this.baseUrls.crawlRequest}', '${page.id}', '${this.token}', 'description')">Show Duplicates</p>
                    </td>
                    <td class="overview__errors-td">
                        <div class="messages">
                            ${
                finalStatuses
                    .map(status => `<a name="${status.id}" class="${status.class} ${status.name}">${status.title}</a>`)
                    .reduce((prev, curr) => prev + curr, '')
                }
                        </div>
                    </td>
                </tr>
            `;
        }

        titleExist(val) {
            if(val !== undefined){
                return val;
            }else{
                return '';
            }
        }

        numberFormat(val, decimalPlaces) {
            if(isNaN(parseFloat(val))){
                return '0';
            }else if (val !== null){
                return parseFloat(parseFloat(val).toFixed(decimalPlaces));
            }
        }

        getUnactivePageStatuses(statuses){
            let filterId = this.filterId,
                filterIdArray = ['0','1','67','68','69','70'];
            if(filterId !== undefined){
                let filterArray = filterId;
                if (typeof(filterId) !== 'number'){
                    filterArray = filterId.split(',');
                    $.each(filterArray, function(ind, val) {
                        if($.inArray(val, filterIdArray) < 0){
                            $.each(statuses, function( index, value ) {
                                if($.inArray(value.id, filterArray) == -1){
                                    statuses[index]['name'] = 'unactive';
                                }else{
                                    statuses[index]['name'] = 'active';
                                }
                            });
                        }
                    });
                }else{
                    $.each(statuses, function( index, value ) {
                        if($.inArray(value.id, filterArray) == -1){
                            statuses[index]['name'] = 'unactive';
                        }else{
                            statuses[index]['name'] = 'active';
                        }
                    });
                }
            }
            return statuses;
        }

        getPageStatuses(page, duplicate_titles, duplicate_h1, duplicate_description): any{
            const statuses = [];
            const httpStatusCode = page.httpStatusCode;
            let contentLength = page.contentLength,
                textContentLength = page.textContentLength;

            if(httpStatusCode == 200){
                if(page.contentType === 'text/html'){
                    if(page.canonicalCount === 0){
                        statuses.push({class: "blue", 'title': "No Canonical", 'id': '58'})
                    }else{
                        if(page.canonical === ''){statuses.push({class: "blue", 'title': "Empty Canonical", 'id': '65'})}
                    }

                    if(page.canonical != page.url && page.canonical != ''){statuses.push({class: "yellow", 'title': "Canonical ≠ URL", 'id': '9'})}

                    if(page.titleCount === 0){
                        statuses.push({class: "red", 'title': "No Title Tag", 'id': '59'})
                    }else{
                        if(page.title === ''){statuses.push({class: "red", 'title': "Empty Title", 'id': '43'})}
                        if(page.titleCount > 1){statuses.push({class: "red", 'title': "TWO TITLE TAGS", 'id': '60'})}
                        if(duplicate_titles.includes(page.url)){statuses.push({class: "yellow", 'title': "DUPLICATE TITLE", 'id': '3'})}
                        if(page.title.length > 70){statuses.push({class: "blue", 'title': "LONG TITLE", 'id': '44'})}
                        if(page.title.length < 30){statuses.push({class: "blue", 'title': "SHORT TITLE", 'id': '45'})}
                    }

                    if(page.h1Count === 0){
                        statuses.push({class: "red", 'title': "NO H1 TAG", 'id': '57'})
                    }else{
                        if(page.h1 === ''){statuses.push({class: "red", 'title': "Empty H1", 'id': '46'})}
                        if(page.h1Count > 1){statuses.push({class: "red", 'title': "TWO H1 TAGS", 'id': '31'})}
                        if(duplicate_h1.includes(page.url)){statuses.push({class: "yellow", 'title': "DUPLICATE H1", 'id': '27'})}
                        if(page.h1.length > 70){statuses.push({class: "blue", 'title': "LONG H1", 'id': '47'})}
                        if(page.h1.length < 5){statuses.push({class: "blue", 'title': "SHORT H1", 'id': '48'})}
                        if(page.h1 === page.title){statuses.push({class: "blue", 'title': "H1 = TITLE", 'id': '5'})}
                    }

                    if(page.descriptionCount === 0){
                        statuses.push({class: "red", 'title': "NO DESCRIPTION TAG", 'id': '62'})
                    }else{
                        if(page.description === ''){statuses.push({class: "red", 'title': "Empty Description", 'id': '49'})}
                        if(page.descriptionCount > 1){statuses.push({class: "red", 'title': "TWO DESCRIPTION TAGS", 'id': '63'})}
                        if(duplicate_description.includes(page.url)){statuses.push({class: "yellow", 'title': "DUPLICATE DESCRIPTION", 'id': '32'})}
                        if(page.description.length > 320){statuses.push({class: "blue", 'title': "LONG DESCRIPTION", 'id': '50'})}
                        if(page.description.length < 70){statuses.push({class: "blue", 'title': "SHORT DESCRIPTION", 'id': '51'})}
                        if(page.description === page.title){statuses.push({class: "blue", 'title': "DESCRIPTION = TITLE", 'id': '66'})}
                    }
                }
                if((contentLength === 0) || (((textContentLength/(contentLength/100))<10))){statuses.push({class: "blue", 'title': "LOW CODE RATIO", 'id': '30'})}
                if(contentLength < 500){statuses.push({class: "blue", 'title': "THIN PAGE", 'id': '33'})}
            }

            if(httpStatusCode >= 300 && page.redirectToStatusCode >=300){statuses.push({class: "red", 'title': "Redirect Chain", 'id': '42'})}
            if(httpStatusCode >= 500 && httpStatusCode < 599 ){statuses.push({class: "red", 'title': "502 Server Error", 'id': '26'})}
            if(page.bodyCount > 1 ){statuses.push({class: "red", 'title': "Duplicate Body", 'id': '36'})}

            if(page.canonical !== '' && httpStatusCode !== 200){statuses.push({class: "yellow", 'title': "NON-200 CANONICAL", 'id': '10'})}
            if(httpStatusCode >= 400 && httpStatusCode < 499 ){statuses.push({class: "yellow", 'title': "404 Page", 'id': '25'})}

            if(page.url.length > 80){statuses.push({class: "blue", 'title': "LONG URL", 'id': '35'})}
            if (httpStatusCode == 301){statuses.push({class: "blue", 'title': "301 Redirect", 'id': '6'})}
            if (httpStatusCode == 302){statuses.push({class: "blue", 'title': "302 Redirect", 'id': '64'})}
            if(httpStatusCode >= 303 && httpStatusCode < 400 ){statuses.push({class: "blue", 'title': "3XX Redirect", 'id': '24'})}
            if(!page.robotsTxtAll){statuses.push({class: "blue", 'title': "DISALLOWED", 'id': '22'})}
            if(page.noindex === true){statuses.push({class: "blue", 'title': "Noindex", 'id': '28'})}
            if(page.nofollow === true){statuses.push({class: "blue", 'title': "Nofollow", 'id': '13'})}

            return statuses;
        }

        getStatusCodeColumn(page: any): string{
            let domain = window.location.protocol + "//" + window.location.host;
            return `
                ${
                page.httpStatusCode < 300 || page.httpStatusCode >= 400 ?
                    `
                        <span style="color: ${this.getStatusCodeIcon(parseInt(page.httpStatusCode))}">${page.httpStatusCode}</span>
                    ` :
                    `
                        <span style="color: ${this.getStatusCodeWithColor(parseInt(page.httpStatusCode))}">${page.httpStatusCode}</span> <img class="crawler_statistics-arrow" src="/wp-content/themes/sitechecker/out/img_design/arrow__down.svg" alt="arrow" title="arrow">
                        ${
                        page.redirectToStatusCode === 0 ?
                            `
                                <span data-toggle="tooltip" data-placement="top" title="${page.location}">???</span>
                            ` :
                            `
                                <span data-toggle="tooltip" data-placement="top" title="${page.location}" style="color: ${this.getStatusCodeWithColor(parseInt(page.redirectToStatusCode))}" >${page.redirectToStatusCode}</span>
                            `
                        } 
                    `
                }
            `;
        }

        addFilters(request) {
            request.searchParamUrl = $('#crawling_search').val();

            if (this.filterId !== null) {
                request.customFilter = this.filterId;
            }

            return request;
        }

        changeFilter(filterId: number, filterTitle: string, filterDescription: string, whatItIsImportant: string, pageSize: number){
            $('.overview__table').removeClass('overview__count').attr('name',filterId);
            $('#crawling_search').val('');
            $('#pageTablePagination').css('opacity','1');
            let param = $('.overview__tables .title__name');
            param.find('span').text(filterTitle).attr('name',filterId);
            if(filterDescription !== 'undefined'){
                param.find('.tooltip').text(filterDescription);
                param.find('.description').fadeIn();
            }else{
                param.find('.description').css('display','none');
            }
            if(whatItIsImportant !== undefined){
                param.find('.important__overview-name').addClass('show').css('display','block');
                $('.important__overview-content').addClass('show').find('p').text(whatItIsImportant);
            }else{
                param.find('.important__overview-name').removeClass('show').css('display','none');
                $('.important__overview-content').removeClass('open').removeClass('show').css('display','none');
            }
            $('#pageTable tbody').empty().append('<tr class="loading"><td colspan="5"><div class="loading"><img src="/wp-content/themes/sitechecker/out/img_design/loading-blue.svg" alt="loading" title="loading"></div></td></tr>');
            if(filterId !== '0'){
                $('.overview__block, .overview__graph').css('display','none');
            }else{
                $('.overview__block, .overview__graph').fadeIn().css('display','flex');
                this.filterId = 1;
            }
            this.filterId = filterId;
            this.page = 1;
            if(pageSize){
                this.pageSize = pageSize;
            }
            this.display();
        }

        getStatusCodeWithColor(statusCode: number){
            let color = '#FF4B55';
            if(statusCode >= 200 && statusCode < 300){
                color = "#50D166";
            }else if(statusCode > 300 && statusCode < 400){
                color = "#FFB300"
            }

            return color;
        }

        getStatusCodeIcon(statusCode: number): string{
            if(statusCode >= 200 && statusCode < 300){
                return "#50D166"
            }else if(statusCode > 300 && statusCode < 400){
                return "#FFB300";
            }else{
                return "#FF4B55";
            }
        }
    }
}
