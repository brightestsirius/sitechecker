/// <reference path="CrawlingBaseTable.ts"/>
/// <reference path="../../../../../../typings/globals/jquery/index.d.ts" />

namespace Crawling {

    export class MonitoringTabTable extends BaseTable {

        private token: string;
        protected requestPath: string;
        private interval: string;
        private filterDate: string;
        private searchParamUrl: string;
        private isIndex: boolean;
        private dateFrom: string;
        private dateTill: string;

        constructor(baseUrls: any, requestPath: string, token: string, interval: string, filterDate: string, searchParamUrl: string, isIndex: boolean, dateFrom: string, dateTill: string) {
            super(baseUrls);
            this.requestPath = requestPath;
            this.sortParam = 'date_create';
            this.pageSize = 10;
            this.token = token;
            this.interval = interval;
            this.filterDate = filterDate;
            this.searchParamUrl = searchParamUrl;
            this.isIndex = isIndex;
            this.dateFrom = dateFrom;
            this.dateTill = dateTill;
        }

        protected processResult(response: any) {
            this.totalCount = response.totalCount;
            this.filteredCount = response.filteredCount;
            this.totalPages = response.pages;
            const events = response.summary;
            let searchParam = false;

            if (this.searchParamUrl !== undefined && this.searchParamUrl !== '' || this.isIndex === true) {
                searchParam = true;
            }

            if (events.length > 0) {
                $('.history__block').removeClass('empty');
                if (this.filterDate !== undefined && this.filterDate !== '') {
                    $('.load__more-filter').removeClass('active').fadeIn();
                } else {
                    $('.load__more').removeClass('active').fadeIn();
                }
                this.drawRows(events);
            } else if (events.length === 0 && response.pages === 0) {
                $('.load__more, .load__more-filter, .monitoring-loader, .monitoring__graph').css('display', 'none');
                $('.monitoring__history-point.started').fadeIn();
                $('.load__more, .load__more-filter, .monitoring-loader').css('display', 'none');
            } else if (events.length === 0 && response.pages > 0) {
                if (searchParam) {
                    $('.monitoring__history-point.started, .load__more, .monitoring-loader').css('display', 'none');
                    $('.history__block').empty().addClass('empty').append('<p class="not__found">URL not found in this domain</p>');
                } else {
                    $('.monitoring__history-point.started').fadeIn();
                    $('.load__more, .load__more-filter, .monitoring-loader').css('display', 'none');
                }
            }
        }

        nextPage() {
            if (this.page * this.pageSize < this.totalCount) {
                this.page++;
                this.display();
            } else {
                $('.monitoring__history-point.started').fadeIn();
                $('.load__more, .load__more-filter').css('display', 'none');
            }
        }

        protected getSortParam(): string {
            return 'weight';
        }

        protected getPagination() {
            return $('#pageTablePagination');
        }

        protected getTable() {
            return $('.history__block');
        }

        protected getTBody() {
            return this.getTable();
        }

        protected getCrawlingDate() {
            return $('#crawling_date');
        }

        public sort(column) {
            if (column === this.sortParam) {
                this.sortBy = this.sortBy === 'asc' ? 'desc' : 'asc';
            } else {
                this.sortBy = 'asc';
                this.sortParam = column;
            }

            this.display();
        };

        drawRows(events) {
            events.forEach((event) => {
                const row = this.renderEvent(event);
                this.addTableRow(row);
            });
            $('#next-second .fa').css('display', 'none');
        }

        logFailed(log): void {
            console.log("error" + log);
        }

        display() {
            this.getPageData().then(
                data => this.processResult(data),
                error => this.logFailed(error)
            )
        };

        protected getRequestData() {
            return {
                sortParam: this.sortParam,
                sortBy: this.sortBy,
                pageSize: this.pageSize,
                page: this.page,
                token: this.token,
                interval: this.interval,
                filterDate: this.filterDate,
                searchParamUrl: this.searchParamUrl,
                isIndex: this.isIndex,
                dateFrom: this.dateFrom,
                dateTill: this.dateTill
            };
        }

        getPageData() {
            const request = this.getRequestData(), data = JSON.stringify(request);
            return this.monitoringRequest(data);
        }

        monitoringRequest(data) {
            return this.doRequest({
                path: this.requestPath,
                action: 'crawler_request',
                data: data,
                method: 'POST'
            })
        }

        renderEvent(event) {

            const weekday = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"],
                monthNames = ["January", "February", "March", "April", "May", "June",
                    "July", "August", "September", "October", "November", "December"
                ];

            function formatAMPM(date) {
                let hours = date.getHours(),
                    minutes = date.getMinutes(),
                    ampm = hours >= 12 ? 'pm' : 'am';
                hours = hours % 12;
                hours = hours ? hours : 12;
                minutes = minutes < 10 ? '0' + minutes : minutes;
                return hours + ':' + minutes + ' ' + ampm;
            }

            let dateFormat = new Date(event.monitoringStatistic.dateCreate * 1000),
                date = formatAMPM(dateFormat),
                dateDay = weekday[dateFormat.getDay()] + ' ' + monthNames[dateFormat.getMonth()] + ' ' + dateFormat.getDate() + ', ' + dateFormat.getFullYear();

            const id = event.monitoringStatistic.id;
            const pagesAdded = JSON.parse(event.pagesAdded);
            let pagesAddedMore = 0,
                pagesAddedDisplay = 'none';

            if(pagesAdded.length > 50){
                pagesAddedMore = pagesAdded.length - 50;
                pagesAddedDisplay = 'block'
            }
            const pageAddedArray = pagesAdded.map((page, index) => {
                const pageUrl = page.url;
                if(index < 50) {
                    return `<li>
                            <div class="external-link"><a href="${this.baseUrls.pageInformation}?url=${page.crc32url}&token=${this.token}">${page.url}</a><a href="${pageUrl}" target="_blank"><img src="/wp-content/themes/sitechecker/out/img_design/external.svg" alt="external" title="external"></a></div>
                        </li>`
                }
            }).join('\n');

            const timeLineNewPages = `
                <div class="header">
                    <img src="/wp-content/themes/sitechecker/out/img_design/monitoring_add.svg" alt="">${pagesAdded.length} new pages
                    <p class="date">${date}</p>
                    <svg width="9" height="5" viewBox="0 0 9 5" fill="none" xmlns="http://www.w3.org/2000/svg">
<path fill-rule="evenodd" clip-rule="evenodd" d="M4.499 0L9 2.5V4.896L4.499 2.38L0 5V2.5L4.499 0Z" transform="translate(9 5) rotate(-180)" fill="#cccccc"/></svg></div>
                <div class="body">
                    <div class="container">
                        <ul class="added">
                            ${pageAddedArray}
                        </ul>
                        <p class="more" style="display: ${pagesAddedDisplay}">and ${pagesAddedMore} more pages</p>
                    </div>
                </div>
            `;

            const pagesDeleted = JSON.parse(event.pagesDeleted);
            let pagesDeletedMore = 0,
                pagesDeletedDisplay = 'none';

            if(pagesDeleted.length > 50){
                pagesDeletedMore = pagesDeleted.length - 50;
                pagesDeletedDisplay = 'block'
            }

            const pageDeletedArray = pagesDeleted.map((page, index) => {
                const pageUrl = page.url;
                if(index < 50){
                    return `<li><span>${pageUrl}</span></li> `;
                }
            }).join('\n');

            const timeLineDeletedPages = `
                <div class="header">
                    <img src="/wp-content/themes/sitechecker/out/img_design/monitoring_delete.svg" alt="">${pagesDeleted.length} deleted pages
                    <p class="date">${date}</p>
                    <svg width="9" height="5" viewBox="0 0 9 5" fill="none" xmlns="http://www.w3.org/2000/svg">
<path fill-rule="evenodd" clip-rule="evenodd" d="M4.499 0L9 2.5V4.896L4.499 2.38L0 5V2.5L4.499 0Z" transform="translate(9 5) rotate(-180)" fill="#cccccc"/></svg></div>
                <div class="body">
                    <div class="container">
                        <ul class="deleted">
                            ${pageDeletedArray}
                        </ul>
                        <p class="more" style="display: ${pagesDeletedDisplay}">and ${pagesDeletedMore} more pages</p>
                    </div>
                </div>
            `;

            const pagesBroken = JSON.parse(event.pagesBroken);
            let pagesBrokenMore = 0,
                pagesBrokenDisplay = 'none';

            if(pagesBroken.length > 50){
                pagesBrokenMore = pagesBroken.length - 50;
                pagesBrokenDisplay = 'block'
            }
            const pageBrokenArray = pagesBroken.map((page, index) => {
                const pageUrl = page.url;
                if(index < 50){
                    return `<li><span>${pageUrl}</span></li> `
                }
            }).join('\n');

            const timeLineBrokenPages = `
                <div class="header">
                    <img src="/wp-content/themes/sitechecker/out/img_design/monitoring_delete.svg" alt="">${pagesBroken.length} broken pages
                    <p class="date">${date}</p>
                    <svg width="9" height="5" viewBox="0 0 9 5" fill="none" xmlns="http://www.w3.org/2000/svg">
<path fill-rule="evenodd" clip-rule="evenodd" d="M4.499 0L9 2.5V4.896L4.499 2.38L0 5V2.5L4.499 0Z" transform="translate(9 5) rotate(-180)" fill="#cccccc"/></svg></div>
                <div class="body">
                    <div class="container">
                        <ul class="deleted">
                            ${pageBrokenArray}
                        </ul>
                        <p class="more" style="display: ${pagesBrokenDisplay}">and ${pagesBrokenMore} more pages</p>
                    </div>
                </div>
            `;

            const pagesOrphan = JSON.parse(event.pagesOrphan);

            let pagesOrphanMore = 0,
                pagesOrphanDisplay = 'none';

            if(pagesOrphan.length > 50){
                pagesOrphanMore = pagesOrphan.length - 50;
                pagesOrphanDisplay = 'block'
            }
            const pageOrphanArray = pagesOrphan.map((page, index) => {
                const pageUrl = page.url;
                if(index < 50){
                    return `<li><span>${pageUrl}</span></li> `
                }
            }).join('\n');

            const timeLineOrphanPages = `
                <div class="header">
                    <img src="/wp-content/themes/sitechecker/out/img_design/monitoring_delete.svg" alt="">${pagesOrphan.length} orphan pages
                    <p class="date">${date}</p>
                    <svg width="9" height="5" viewBox="0 0 9 5" fill="none" xmlns="http://www.w3.org/2000/svg">
<path fill-rule="evenodd" clip-rule="evenodd" d="M4.499 0L9 2.5V4.896L4.499 2.38L0 5V2.5L4.499 0Z" transform="translate(9 5) rotate(-180)" fill="#cccccc"/></svg></div>
                <div class="body">
                    <div class="container">
                        <ul class="deleted">
                            ${pageOrphanArray}
                        </ul>
                        <p class="more" style="display: ${pagesOrphanDisplay}">and ${pagesOrphanMore} more pages</p>
                    </div>
                </div>
            `;

            const pagesChanged = JSON.parse(event.pagesChanged);

            function getPagesByChangeTitle(pagesChanged, changeTitle) {
                return pagesChanged.filter((page) => {
                    const pageChangeDataItem = page.change_data.find(pageChange => pageChange.param === changeTitle);
                    return pageChangeDataItem !== undefined;
                }).map((page) => {
                    const pageChangeDataItem = page.change_data.find(pageChange => pageChange.param === changeTitle);
                    return {
                        url: page.url,
                        crc32url: page.crc32url,
                        previous: pageChangeDataItem.previous,
                        changed: pageChangeDataItem.changed,
                        data: pageChangeDataItem.data
                    }
                })
            }

            let resultBlocks = "";

            if (pagesChanged.length > 0) {
                const changeParams = [
                    {
                        paramTitle: 'title',
                        image: 'title',
                        title: 'Title has changed',
                        needsCheckForTitle: false,
                        changedElements: getPagesByChangeTitle(pagesChanged, 'title')
                    },
                    {
                        paramTitle: 'description',
                        image: 'title',
                        title: 'Description has changed',
                        needsCheckForTitle: false,
                        changedElements: getPagesByChangeTitle(pagesChanged, 'description')
                    },
                    {
                        paramTitle: 'text_content',
                        image: 'title',
                        title: 'Content has changed',
                        needsCheckForTitle: false,
                        changedElements: getPagesByChangeTitle(pagesChanged, 'text_content')
                    },
                    {
                        paramTitle: 'h1',
                        image: 'title',
                        title: 'H1 has changed',
                        needsCheckForTitle: false,
                        changedElements: getPagesByChangeTitle(pagesChanged, 'h1')
                    },
                    {
                        paramTitle: 'http_status_code',
                        image: 'status',
                        title: 'HTTP status code has changed',
                        needsCheckForTitle: false,
                        changedElements: getPagesByChangeTitle(pagesChanged, 'http_status_code')
                    },
                    {
                        paramTitle: 'robots_txt_all',
                        image: 'status',
                        title: 'Index status has changed',
                        needsCheckForTitle: true,
                        falseTitle: '<span class="allow">URL allowed by robots.txt</span>',
                        trueTitle: '<span class="disallow">URL disallowed by robots.txt</span>',
                        changedElements: getPagesByChangeTitle(pagesChanged, 'robots_txt_all')
                    },
                    {
                        paramTitle: 'robots_txt_content',
                        image: 'robots',
                        title: 'Robots.txt has changed',
                        needsCheckForTitle: true,
                        falseTitle: '<span class="disallow">URL disallowed</span>',
                        trueTitle: '<span class="allow">URL allowed</span>',
                        changedElements: getPagesByChangeTitle(pagesChanged, 'robots_txt_content')
                    }, {
                        paramTitle: 'noindex',
                        image: 'index',
                        title: 'Index status has changed',
                        needsCheckForTitle: true,
                        falseTitle: '<span class="disallow">URL indexation closed by noindex tag</span>',
                        trueTitle: '<span class="allow">URL indexation opened by index tag</span>',
                        changedElements: getPagesByChangeTitle(pagesChanged, 'noindex')
                    },
                    {
                        paramTitle: 'www_redirect',
                        image: 'status',
                        title: 'WWW redirect has changed',
                        needsCheckForTitle: true,
                        falseTitle: '<span class="disallow">www / non-www redirect is not working</span>',
                        trueTitle: '<span class="allow">www / non-www redirect is working</span>',
                        changedElements: getPagesByChangeTitle(pagesChanged, 'www_redirect')
                    },
                    {
                        paramTitle: 'https_redirect',
                        image: 'status',
                        title: 'HTTPS redirect has changed',
                        needsCheckForTitle: true,
                        falseTitle: '<span class="disallow">HTTP to HTTPS redirect is not working</span>',
                        trueTitle: '<span class="allow">HTTP to HTTPS redirect is working</span>',
                        changedElements: getPagesByChangeTitle(pagesChanged, 'https_redirect')
                    }
                ].filter(changeParam => {
                    return changeParam.changedElements.length > 0
                });

                resultBlocks = changeParams.map((changeParam) => {
                    const changedElements = changeParam.changedElements;
                    const changedArray = changedElements.map((page,index) => {
                        let previous = page.changed,
                            changed = page.previous,
                            data = page.data,
                            stuff = [],
                            output = '',
                            array = '',
                            elementSring;

                        $(data).each(function (i, item: any) {
                            if (changeParam.paramTitle === 'text_content') {
                                array = item.text.split("\n");
                            } else {
                                array = item.text.split("\r\n");
                            }
                            $(array).each(function (i, element) {
                                element = element.trim();
                                elementSring = {string: element, view: item.operation};
                                stuff.push(elementSring);
                            })
                        });

                        $(stuff).each(function (i, item: any) {
                            let elementStrings = item.string.split("\n"),
                                elementView = item.view;
                            $.each(elementStrings, function (i, elementString) {
                                if (elementView === "DELETE" && elementString !== '') {
                                    if (changeParam.paramTitle === 'text_content') {
                                        output = output + '<li class="robots-delete"><code><xmp>' + elementString + '</xmp></code></li>';
                                    } else {
                                        output = output + '<li class="robots-delete">' + elementString + '</li>';
                                    }
                                } else if (elementView === 'INSERT' && elementString !== '') {
                                    if (changeParam.paramTitle === 'text_content') {
                                        output = output + '<li class="robots-added"><code><xmp>' + elementString + '</xmp></code></li>';
                                    } else {
                                        output = output + '<li class="robots-added">' + elementString + '</li>';
                                    }
                                } else if (elementString !== '') {
                                    if (changeParam.paramTitle === 'text_content') {
                                        output = output + '<li class="robots-current"><code><xmp>' + elementString + '</xmp></code></li>';
                                    } else {
                                        output = output + '<li class="robots-current">' + elementString + '</li>';
                                    }
                                }
                            });
                        });

                        if (data) {
                            if (changeParam.paramTitle === 'text_content') {
                                return `<div class="body">
                                <div class="container">
                                    <div class="url">URL: <div class="external-link"><a href="${this.baseUrls.pageInformation}?url=${page.crc32url}&token=${this.token}">${page.url}</a><a href="${page.url}" target="_blank"><img src="/wp-content/themes/sitechecker/out/img_design/external.svg" alt="external" title="external"></a></div></div>
                                    <div class="robots__changes">${output}</div>
                                </div></div>`;
                            } else {
                                return `<div class="body">
                                <div class="container"><div class="robots__changes">${output}</div></div></div>`;
                            }
                        } else {
                            if (changeParam.needsCheckForTitle === true) {
                                changed = page.changed === false ? changeParam.falseTitle : changeParam.trueTitle;
                                previous = page.previous === false ? changeParam.falseTitle : changeParam.trueTitle;
                            }
                            if(index < 50) {
                                return `
                                <div class="body">
                                    <div class="container">
                                        <div class="url">URL: <div class="external-link"><a href="${this.baseUrls.pageInformation}?url=${page.crc32url}&token=${this.token}">${page.url}</a><a href="${page.url}" target="_blank"><img src="/wp-content/themes/sitechecker/out/img_design/external.svg" alt="external" title="external"></a></div></div>
                                        <div class="changes">
                                            <p class="${changeParam.paramTitle}">${previous}</p>
                                            <img src="/wp-content/themes/sitechecker/out/img_design/arrow_monitoring.svg" alt="arrow" title="arrow">
                                            <p class="${changeParam.paramTitle}">${changed}</p>
                                        </div>
                                    </div>
                                </div>
                            `;
                            }
                        }
                    });

                    const changeArrayHTML = changedArray.join('\n');
                    let changeWorld = changedArray.length === 1 ? 'change' : 'changes',
                        pagesChangesMore = 0,
                        pagesChangesDisplay = 'none';

                    if(changedArray.length > 50){
                        pagesChangesMore = changedArray.length - 50;
                        pagesChangesDisplay = 'block'
                    }
                    return `
                    <div class="monitoring__history-event">
                        <div class="header"><img src="/wp-content/themes/sitechecker/out/img_design/monitoring_${changeParam.image}.svg" alt="${changeParam.image}" title="${changeParam.title}">${changeParam.title}
                        <p class="changes">${changedArray.length} ${changeWorld}</p>
                        <p>${date}</p>
                        <svg width="9" height="5" viewBox="0 0 9 5" fill="none" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" clip-rule="evenodd" d="M4.499 0L9 2.5V4.896L4.499 2.38L0 5V2.5L4.499 0Z" transform="translate(9 5) rotate(-180)" fill="#cccccc"/></svg></div>
                        ${changeArrayHTML}
                        <div class="body" style="display: ${pagesChangesDisplay}">
                            <div class="container">
                                <p class="more_changes">and ${pagesChangesMore} more pages</p>
                            </div>
                        </div>
                    </div>
                `;
                }).join('\n');
            }

            let timeLineHTML,
                newPages,
                deletedPages,
                brokenPages,
                orphanPages;

            if (pageAddedArray.length !== 0) {
                newPages = `<div class="monitoring__history-event added">${timeLineNewPages}</div>`;
            } else {
                newPages = ``;
            }

            if (pageDeletedArray.length !== 0) {
                deletedPages = `<div class="monitoring__history-event deleted">${timeLineDeletedPages}</div>`;
            } else {
                deletedPages = ``;
            }

            if (pagesBroken.length !== 0) {
                brokenPages = `<div class="monitoring__history-event">${timeLineBrokenPages}</div>`;
            } else {
                brokenPages = ``;
            }

            if (pagesOrphan.length !== 0) {
                orphanPages = `<div class="monitoring__history-event">${timeLineOrphanPages}</div>`;
            } else {
                orphanPages = ``;
            }

            if (pageAddedArray.length == 0 && pageDeletedArray.length == 0 && pagesChanged.length == 0 && pagesBroken.length == 0 && pagesOrphan.length == 0) {
                timeLineHTML = '';
            } else {
                let historyPoint = $('.monitoring__history-point[name="' + dateDay + '"]');
                if (historyPoint.length > 0) {
                    historyPoint.find('.content').append(deletedPages + newPages + brokenPages + orphanPages + resultBlocks);
                } else {
                    timeLineHTML = '<div class="monitoring__history-point" name="' + dateDay + '">' +
                        '<p class="history-point-date">' + dateDay + '</p>' +
                        '<div class="content">' + deletedPages + newPages + brokenPages + orphanPages + resultBlocks + '</div></div>';
                }
            }

            $('.monitoring-loader').css('display', 'none');
            $('.monitoring__history-content').fadeIn();

            $('.robots__changes .robots-current').each(function () {
                let element = $(this),
                    nextElement = element.next();
                if (nextElement.hasClass('robots-added') || nextElement.hasClass('robots-delete')) {
                    element.css({
                        'height': 'auto'
                    })
                }
            });

            return timeLineHTML;
        }

        getUrlForRequest() {
            return this.baseUrls.crawlRequest;
        }
    }
}