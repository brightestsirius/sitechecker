/// <reference path="PopupTable.ts"/>
namespace Crawling.Popup{
    export class DuplicatedLinks extends PopupTable {

        private readonly paramForSearch: string;

        constructor(apiPath: string, url: string, token: string, paramForSearch: string) {
            super(apiPath, '/getDuplicatedLinks', url, token);

            $('#popup_table tbody').empty();
            this.paramForSearch = paramForSearch;

            this.display();
        }

        init(){}

        public getSortParam(): string {
            return 'url';
        }


        getRequestData() {
            const requestData = super.getRequestData();
            requestData.param = this.paramForSearch;
            return requestData
        }

        drawRows(rows) {
            $('#overview__popup').addClass('duplicate__popup').fadeIn().css('display','flex');
            let param = 'Title',
                val = 'title';
            if(this.paramForSearch === 'h1'){
                param = 'H1';
                val = 'h1';
            }else if(this.paramForSearch === 'description'){
                param = 'Description';
                val = 'description';
            }
            $('.crawler_popup-title').html(param+': <span>'+rows[0][val]+'</span>');
            rows.forEach((row, index) => {
                const rowData = this.getPopupRowDataHTML(row, index);
                const tr = `
                    <tr id="popup_row_${index}" >
                        <td>
                            <div class="external-link">
                            <a href="/tool/page-information/?url=${row.crc32Url}&token=${this.token}">${row.url}</a>
                            <a href="${row.url}" target="_blank"><img src="/wp-content/themes/sitechecker/out/img_design/external.svg" alt="external" title="external"></a>
                            </div>
                        </td>
                    </tr>
                    ${rowData}
                `;
                this.addTableRow(tr);
            });

            $('.additional_param').hide();//если ссылки внутренние дополнительные колонки не нужны
        }
    }
}