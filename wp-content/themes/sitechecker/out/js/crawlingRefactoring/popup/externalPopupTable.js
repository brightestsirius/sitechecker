var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/// <reference path="PopupTable.ts"/>
var Crawling;
(function (Crawling) {
    var Popup;
    (function (Popup) {
        var ExternalPopupTable = (function (_super) {
            __extends(ExternalPopupTable, _super);
            function ExternalPopupTable(apiPath, url) {
                return _super.call(this, apiPath, url) || this;
            }
            ExternalPopupTable.prototype.getPath = function () {
                return '/getExternalLinks';
            };
            ExternalPopupTable.prototype.drawRows = function (rows) {
                var _this = this;
                rows.forEach(function (row, index) {
                    var rowData = _this.getPopupRowDataHTML(row, index);
                    var tr = "\n                    <tr id=\"popup_row_" + index + "\" >\n                        <td>\n                            <a href=\"#\">\n                                <i class=\"fa fa-plus-circle icon\" onclick=\"this.toggleRowData(" + index + ")\"  style=\"color: green;\" aria-hidden=\"true\"></i>\n                            </a>\n                        </td>\n                        <td>" + row.url + "</td>\n                    </tr>\n                    " + rowData + "\n                ";
                    _this.addTableRow(tr);
                });
                $('.additional_param').hide(); //если ссылки внутренние дополнительные колонки не нужны
            };
            return ExternalPopupTable;
        }(Popup.PopupTable));
        Popup.ExternalPopupTable = ExternalPopupTable;
    })(Popup = Crawling.Popup || (Crawling.Popup = {}));
})(Crawling || (Crawling = {}));
