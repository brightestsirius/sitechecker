/// <reference path="PopupTable.ts"/>
namespace Crawling.Popup{

    export class InternalPopupTable extends PopupTable {

        constructor(urlForRequest: string, url: string, token: string) {
            super(urlForRequest, 'getInternalLinks', url, token);
            $('.popup__export').click(function () {
                exportLinks('internal', url, token);
            });
            $('#popup_table tbody').empty();
        }

        getSortParam(): string{
            return 'weight';
        }

        getPath() {
            return '/getInternalLinks';
        }

        drawRows(rows) {
            $('.crawler_popup-title').text('Internal links');
            $('#overview__popup').fadeIn().css('display','flex');
            rows.forEach((row, index) => {
                const rowData = this.getPopupRowDataHTML(row, index);
                const tr = `
                    <tr id="popup_row_${index}" >
                        <td>
                            <div class="external-link">
                            <a href="/tool/page-information/?url=${row.crc32Url}&token=${this.token}">${row.url}</a>
                            <a href="${row.url}" target="_blank"><img src="/wp-content/themes/sitechecker/out/img_design/external.svg" alt="external" title="external"></a>
                            </div>
                        </td>
                        <td>${row.link_anchor || '-'}</td>
                        <td>${this.numberFormat(row.normalized_weight)}</td>
                        <td>
                            <span style="color: ${this.getStatusCodeIcon(parseInt(row.http_status_code))}">${row.http_status_code}</span>
                        </td>
                    </tr>
                    ${rowData}
                `;
                $('.additional_param').show();
                this.addTableRow(tr);
            });
        }

        numberFormat(val) {
            if(isNaN(parseInt(val))){
                return '0';
            }else if (val !== null){
                return parseInt(val);
            }
        }
    }
}