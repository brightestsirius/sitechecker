/// <reference path="../CrawlingBaseTable.ts"/>

namespace Crawling.Popup{

    export abstract class PopupTable extends CrawlingBaseTable {

        protected id: string;
        protected urlForRequest: string;

        constructor(urlForRequest: any, requestPath: string, id: string, token: string) {
            super({}, requestPath, token);
            this.id = id;
            this.urlForRequest = urlForRequest;
            $('#popup_search').val('');

            this.getPopup().show();

            this.display();
        }

        display() {

            this.getLoader().css('opacity','1');

            this.getPageData().then(
                data => this.processResult(data),
                error => this.logFailed(error)
            )
        };

        getPopup() {
            if(!$('main').hasClass('domainPage')){
                $('body').css('overflow', 'hidden');
            }
            return $('#popup');
        }

        public sort(column) {}

        getPagination() {
            return $('#popup_pagination');
        }

        getTableElements() {
            return $('.popup_table_elements');
        }

        getTable() {
            return $('#popup_table');
        }

        getTBody() {
            return this.getTable().find("tbody");
        }

        getCrawlingDate() {
            return $('#popupCrawlingDate');
        }

        setPagination() {
            const html = `
                <div class="dataTables_info" id="pageTable_info" role="status" aria-live="polite">
                    Showing ${((this.page * this.pageSize) - this.pageSize)} to ${this.page * this.pageSize > this.totalCount ? this.totalCount : this.page * this.pageSize}  of ${this.totalCount} entries
                </div>
                <div class="dataTables_paginate paging_simple_numbers" id="pageTable_paginate">
                    <ul class="pagination">
                        <li class="page-item disabled">
                            <span>Page ${this.page} from ${this.totalPages}</span>
                        </li>
                        <li class="paginate_button page-item previous ${ this.page > 1 ? '' : 'disabled'}">
                            <a href="javascript:void(0);" aria-controls="pageTable" class="page-link" ${this.page > 1 ? `onclick="popupTable.previousPage();"` : ''}  >Previous</a>
                        </li>
                        <li class="paginate_button page-item ${this.page < this.totalPages ? '' : 'disabled'}">
                            <a href="javascript:void(0);" aria-controls="pageTable"  class="page-link" ${this.page < this.totalPages ? `onclick="popupTable.nextPage()"` : ''} >Next</a>
                        </li>
                    </ul>
                </div>
            `;

            this.getPagination().html(html);
        };

        getPopupRowDataHTML(data, index) {
            const content = Object.keys(data).map(key => {
                const value = data[key];

                return `
                <div class="col-xs-12 col-sm-6 col-lg-4">
                    <div class="detail_title">${key}: ${value}</div>
                </div>
            `;
            }).reduce((previousValue, currentValue) => {
                return previousValue + currentValue;
            });

            return `
                <tr id="popup_row_data_${index}" style="display: none">
                    <td colspan="5">
                       <div>
                            ${content}
                        </div>
                    </td>
                </tr>
            `;
        }

        getRequestData() {
            return this.addFilters({
                id: this.id,
                sortParam: this.sortParam,
                sortBy: this.sortBy,
                pageSize: this.pageSize,
                page: this.page,
                token: this.token,
            });
        }

        public close() {
            this.getPopup().hide();
        }

        toggleRowData(index) {
            const row = $('#popup_row_' + index);
            const rowIcon = row.find('.icon');
            const rowData = $('#popup_row_data_' + index);

            if (row.hasClass("dataShow")) {
                row.removeClass('dataShow');
                rowIcon.removeClass("fa-minus");
                rowIcon.addClass("fa-plus");
                rowData.hide();
            } else {
                row.addClass("dataShow");
                rowIcon.removeClass("fa-plus");
                rowIcon.addClass("fa-minus");
                rowData.show();
            }
        }

        parseCrawledDate({checked: timestamp}): Date {
            return new Date(timestamp * 1000);
        }

        private addFilters(request) {
            request.searchParamUrl = $('#popup_search').val();
            return request;
        }

        getUrlForRequest() {
            return this.urlForRequest;
        }
    }
}