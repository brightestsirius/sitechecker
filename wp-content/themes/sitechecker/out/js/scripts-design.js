const keys = {},
    url__params = window.location.href,
    url_param = new URL(url__params);
let cookiesPopup = localStorage.getItem('cookiesPopup'),
    domain_loc = window.location.href;

$(document).ready( function() {
    let checkOne = $('.checkOne'),
        auditOne = $('.auditOne'),
        monitoringOne = $('.monitoringOne'),
        testimonialImg1 = $('.testimonial__1'),
        testimonialImg2 = $('.testimonial__2'),
        testimonialImg3 = $('.testimonial__3');

    if(checkOne.length){
        checkOne.attr('src','/wp-content/themes/sitechecker/out/img_design/checkOne.svg');
    }
    if(auditOne.length){
        auditOne.attr('src','/wp-content/themes/sitechecker/out/img_design/auditOne.svg');
    }
    if(monitoringOne.length){
        monitoringOne.attr('src','/wp-content/themes/sitechecker/out/img_design/monitoringOne.svg');
    }
    if(testimonialImg1.length){
        testimonialImg1.attr('src',testimonialImg1.attr('data-src'));
    }
    if(testimonialImg2.length){
        testimonialImg2.attr('src',testimonialImg2.attr('data-src'));
    }
    if(testimonialImg3.length){
        testimonialImg3.attr('src',testimonialImg3.attr('data-src'));
    }
    $('head').append('<link rel=\'stylesheet\' id=\'theme-my-login-css\'  href=\'/wp-content/plugins/theme-my-login/theme-my-login.css?ver=6.4.9\' media=\'all\' />' +
        '<link rel=\'stylesheet\' id=\'advanced-pdf-generator-css\'  href=\'/wp-content/plugins/advanced-pdf-generator/public/css/advanced-pdf-generator-public.css?ver=0.2.0\' media=\'all\' />\n' +
        '<link rel=\'stylesheet\' id=\'advanced-pdf-generator-swal-css\'  href=\'https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.11.4/sweetalert2.min.css?ver=0.2.0\' media=\'all\' />' +
        '<link rel=\'stylesheet\' id=\'pdfg-front-css-css\'  href=\'/wp-content/plugins/html-pdf-generator/assets/css/pdfg-front.css?ver=1\' media=\'all\' />' +
        '<link rel=\'stylesheet\' id=\'wp-postratings-css\'  href=\'/wp-content/plugins/wp-postratings/css/postratings-css.css?ver=1.85\' media=\'all\' />' +
        '<link rel=\'stylesheet\' id=\'the_champ_frontend_css-css\'  href=\'/wp-content/plugins/super-socializer/css/front.css?ver=7.12.6\' media=\'all\' />' +
        '<script type=\'text/javascript\' src=\'https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.11.4/sweetalert2.all.min.js?ver=0.2.0\'></script>' +
        '<script type=\'text/javascript\' src=\'/wp-content/plugins/advanced-pdf-generator/public/js/advanced-pdf-generator-public.js?ver=0.2.0\'></script>');

    if (localStorage.getItem('checkoutThread') === null) {
        localStorage.setItem('checkoutThread','5');
    }else{
        let value = localStorage.getItem('checkoutThread'),
            slider = $('#checkout__thread');
        if(value === '0'){
            value = '5';
        }else if(value === '40'){
            value = '35';
        }
        if(slider.length > 0){
            slider.slider('refresh');
            slider.slider('setValue', value, true);
            pricingValue(value,slider);
        }
    }

    $('#checkout__thread').change(function () {
        let slider = $(this),
            value = slider.val();

        localStorage.setItem('checkoutThread',value);
        pricingValue(value,slider);
    });
});

function pricingValue(value,slider){
    let sale = $('.pricing__sale'),
        domains__count = $('.domains__count'),
        startup__cost = $('.startup__cost'),
        bussines__cost = $('.bussines__cost'),
        enterprise__cost = $('.enterprise__cost'),
        startup__btn_1 = $('.startup__btn input[type="hidden"]'),
        startup__btn_2 = $('.startup__btn input[type="submit"]'),
        bussines__btn_1 = $('.bussines__btn input[type="hidden"]'),
        bussines__btn_2 = $('.bussines__btn input[type="submit"]'),
        enterprise__btn_1 = $('.enterprise__btn input[type="hidden"]'),
        enterprise__btn_2 = $('.enterprise__btn input[type="submit"]'),
        startup__save = $('.startup__save'),
        growing__save = $('.growing__save'),
        business__save = $('.business__save');

    if(value === '0' || value === '5'){
        $('.single').css('display', 'inline-block');
        $('.plural').css('display', 'none');
    }else{
        $('.single').css('display', 'none');
        $('.plural').css('display', 'inline-block');
    }

    switch (value){
        case '0':
            sale.fadeOut();
            slider.slider('refresh');
            slider.slider('setValue', 5, true);
            break;
        case '5':
            domains__count.text('1');
            sale.fadeOut();

            startup__cost.text($('#level__btn-20').text());
            startup__btn_1.val('20');
            startup__btn_2.attr('name','20');

            bussines__cost.text($('#level__btn-27').text());
            bussines__btn_1.val('27');
            bussines__btn_2.attr('name','27');

            enterprise__cost.text($('#level__btn-34').text());
            enterprise__btn_1.val('34');
            enterprise__btn_2.attr('name','34');

            break;
        case '10':
            domains__count.text('2');
            sale.fadeIn().css('display','flex');

            startup__cost.text($('#level__btn-21').text());
            startup__btn_1.val('21');
            startup__btn_2.attr('name','21');

            bussines__cost.text($('#level__btn-28').text());
            bussines__btn_1.val('28');
            bussines__btn_2.attr('name','28');

            enterprise__cost.text($('#level__btn-35').text());
            enterprise__btn_1.val('35');
            enterprise__btn_2.attr('name','35');

            startup__save.text('28');
            growing__save.text('114');
            business__save.text('358');
            break;
        case '15':
            domains__count.text('5');
            sale.fadeIn().css('display','flex');

            startup__cost.text($('#level__btn-22').text());
            startup__btn_1.val('22');
            startup__btn_2.attr('name','22');

            bussines__cost.text($('#level__btn-29').text());
            bussines__btn_1.val('29');
            bussines__btn_2.attr('name','29');

            enterprise__cost.text($('#level__btn-36').text());
            enterprise__btn_1.val('36');
            enterprise__btn_2.attr('name','36');

            startup__save.text('70');
            growing__save.text('285');
            business__save.text('895');
            break;
        case '20':
            domains__count.text('10');
            sale.fadeIn().css('display','flex');

            startup__cost.text($('#level__btn-23').text());
            startup__btn_1.val('23');
            startup__btn_2.attr('name','23');

            bussines__cost.text($('#level__btn-30').text());
            bussines__btn_1.val('30');
            bussines__btn_2.attr('name','30');

            enterprise__cost.text($('#level__btn-37').text());
            enterprise__btn_1.val('37');
            enterprise__btn_2.attr('name','37');

            startup__save.text('140');
            growing__save.text('570');
            business__save.text('1790');
            break;
        case '25':
            domains__count.text('25');
            sale.fadeIn().css('display','flex');

            startup__cost.text($('#level__btn-24').text());
            startup__btn_1.val('24');
            startup__btn_2.attr('name','24');

            bussines__cost.text($('#level__btn-31').text());
            bussines__btn_1.val('31');
            bussines__btn_2.attr('name','31');

            enterprise__cost.text($('#level__btn-38').text());
            enterprise__btn_1.val('38');
            enterprise__btn_2.attr('name','38');

            startup__save.text('350');
            growing__save.text('1425');
            business__save.text('4475');
            break;
        case '30':
            domains__count.text('50');
            sale.fadeIn().css('display','flex');

            startup__cost.text($('#level__btn-25').text());
            startup__btn_1.val('25');
            startup__btn_2.attr('name','25');

            bussines__cost.text($('#level__btn-32').text());
            bussines__btn_1.val('32');
            bussines__btn_2.attr('name','32');

            enterprise__cost.text($('#level__btn-39').text());
            enterprise__btn_1.val('39');
            enterprise__btn_2.attr('name','39');

            startup__save.text('700');
            growing__save.text('2850');
            business__save.text('8950');
            break;
        case '35':
            domains__count.text('100');
            sale.fadeIn().css('display','flex');

            startup__cost.text($('#level__btn-26').text());
            startup__btn_1.val('26');
            startup__btn_2.attr('name','26');

            bussines__cost.text($('#level__btn-33').text());
            bussines__btn_1.val('33');
            bussines__btn_2.attr('name','33');

            enterprise__cost.text($('#level__btn-40').text());
            enterprise__btn_1.val('40');
            enterprise__btn_2.attr('name','40');

            startup__save.text('1400');
            growing__save.text('5700');
            business__save.text('17900');
            break;
        case '40':
            $('.show_limits').click();
            slider.slider('refresh');
            slider.slider('setValue', 35, true);
            break;
    }
}

if(url__params.indexOf("login_popup") > 0){
    setTimeout(function () {
        $('.signin__btn').click();
    },300);
    let redirect = url_param.searchParams.get("redirect_to");
    $('.tml-submit-wrap input[name="redirect_to"]').val(redirect);
}

if(url__params.indexOf("signup_popup") > 0){
    setTimeout(function () {
        $('.signup__btn').click();
    },300);
    let redirect = url_param.searchParams.get("redirect_to");
    $('.tml-submit-wrap input[name="redirect_to"]').val(redirect);
}

if(cookiesPopup !== 'Shown'){
    $('.footer__cookie').fadeIn();
    $('.footer__cookie-close').click(function () {
        cookiesPopup = localStorage.setItem('cookiesPopup', 'Shown');
        $('.footer__cookie').fadeOut();
    })
}

if(url__params.indexOf('relogin=1') > 0){
    setTimeout(function () {
        $('.signin__btn').click();
    },300);
}

if(url__params.indexOf('pdfBanner') > 0){
    $('.pdf_banner').css('display','flex');
    dataLayer.push({ 'event': 'autoEvent', 'eventCategory': 'PDF limited popup shown', 'eventAction': 'shown' });
    domain_loc = domain_loc.replace('?pdfBanner','');
    window.history.replaceState(domain_loc, '', domain_loc);
}
$('.pdf_banner-close').click(function () {
    $('.pdf_banner, .pdf_banner-report, .limitRegistartion').addClass('fade_out');
});

if(url__params.indexOf('product_tour') > 0){
    setTimeout(function () {
        $('.product__tour').click();
        domain_loc = domain_loc.replace('?product_tour','');
        window.history.replaceState(domain_loc, '', domain_loc);
    },300)
}

if(url__params.indexOf('checkemail=confirm') > 0){
    $('.resetpass__popup').fadeIn().css('display','flex');
    disableScroll();
    function sentEmail() {
        $('.circle-loader').toggleClass('load-complete');
        $('.checkmark').toggle();
        setTimeout(closePopup,4000);
    }
    function closePopup(){
        $('.login__popup-close').click();
        domain_loc = domain_loc.replace('?checkemail=confirm','');
        window.history.replaceState(domain_loc, '', domain_loc);
    }
    setTimeout(sentEmail,1000);
}

if(url__params.indexOf('resetpass=complete') > 0){
    $('.newpass__popup').fadeIn().css('display','flex');
    disableScroll();
    function sentEmail() {
        $('.circle-loader').toggleClass('load-complete');
        $('.checkmark').toggle();
        setTimeout(closePopup,4000);
    }
    function closePopup(){
        $('.login__popup-close').click();
        domain_loc = domain_loc.replace('?resetpass=complete','');
        window.history.replaceState(domain_loc, '', domain_loc);
    }
    setTimeout(sentEmail,1000);
    $('.signin__btn').click(function () {
        $('.newpass__popup').fadeOut();
        $('.login__popup').css('display','flex');
        disableScroll();
    });
}

let cookies = localStorage.getItem('cookies');
if(cookies !== 'Shown'){
    $('.footer__cookie').fadeIn();
    cookies = localStorage.setItem('cookies', 'Shown');
    $('.footer__cookie-close').click(function () {
        $('.footer__cookie').fadeOut();
    })
}

function preventDefault(e) {
    e = e || window.event;
    if (e.preventDefault)
        e.preventDefault();
    e.returnValue = false;
}

function preventDefaultForScrollKeys(e) {
    if (keys[e.keyCode]) {
        preventDefault(e);
        return false;
    }
}

function disableScroll() {
    if (window.addEventListener) // older FF
        window.addEventListener('DOMMouseScroll', preventDefault, false);
    window.onwheel = preventDefault; // modern standard
    window.onmousewheel = document.onmousewheel = preventDefault; // older browsers, IE
    window.ontouchmove  = preventDefault; // mobile
    document.onkeydown  = preventDefaultForScrollKeys;
}

function enableScroll() {
    if (window.removeEventListener)
        window.removeEventListener('DOMMouseScroll', preventDefault, false);
    window.onmousewheel = document.onmousewheel = null;
    window.onwheel = null;
    window.ontouchmove = null;
    document.onkeydown = null;
}

$('.form__input input').focus(function(){
    let input = $(this).parents('.form__input');
    input.addClass('active').find('span').css({
        'transform': 'translate3d(0, -14px, 0)',
        'top': '0',
        'left': '-32px',
        'font-size': '8px'
    });
    if($(this).parent().parent().hasClass('form__error') || $(this).parent().parent().hasClass('form__warning')){
        input.find('span').css({
            'color': 'transparent'
        })
    }
}).focusout(function(){
    let input = $(this).parents('.form__input');
    input.removeClass('active').find('span').css({
        'transform': 'translate3d(0, 0, 0)',
        'top': '9px',
        'left': '0',
        'font-size': '12px',
        'color': '#666666'
    });
    if($(this).val() !== ''){
        input.find('span').css({
            'color': 'transparent'
        })
    }
});

$('#pmpro_account-change-password').click(function(){
    let userEmail = $('#user__email').text();
    $('.form__lostpass input[type="email"]').val(userEmail);
    $('.form__lostpass').submit();
    $(this).find('span').css('display','none');
    $(this).find('img').fadeIn();
});

$('.login__popup-close').click(function () {
    $('.login__popup, .resetpass__popup, .newpass__popup').fadeOut();
    let form = $('.article__seo-search button'),
        loading = form.find('img.loading'),
        arrow = form.find('img.arrow');
    if(form.length > 0){
        arrow.fadeIn();
        loading.css('display','none');
    }
    enableScroll();
    function popup__clean() {
        $('.signup__block').css({
            'right':'-400px'
        }).removeClass('bounceInRight').removeClass('animated');
        if (window.matchMedia('(max-width: 460px)').matches) {
            $('.login__popup-block').css({
                'height':'395px'
            });
        } else {
            $('.login__popup-block').css({
                'height':'460px'
            });
        }
        $('.resetpass__block').css({
            'left':'-400px'
        }).removeClass('bounceInLeft').removeClass('animated');
        $('.signin__block').css({
            'left':'0'
        }).removeClass('bounceInLeft').removeClass('animated');
    }
    setTimeout(popup__clean,500);
});

function seoReportPoupop(){
    $('.popup__signin').click(function () {
        $('.signup__block').css({
            'right':'-650px'
        }).removeClass('bounceInRight').removeClass('animated');
        if (window.matchMedia('(max-width: 460px)').matches) {
            $('.login__popup-block').css({
                'height':'525px'
            });
        } else {
            $('.login__popup-block').css({
                'height':'550px'
            });
        }
        $('.signin__block').css({
            'left':'0'
        }).addClass('bounceInLeft').addClass('animated');
    });
    $('.popup__signup').click(function () {
        $('.signup__block').css({
            'right':'0',
            'top': '80px'
        }).addClass('bounceInRight').addClass('animated');
        if (window.matchMedia('(max-width: 460px)').matches) {
            $('.login__popup-block').css({
                'height':'525px'
            });
        } else {
            $('.login__popup-block').css({
                'height':'550px'
            });
        }
        $('.signin__block, .resetpass__block').css({
            'left':'-650px'
        }).removeClass('bounceInLeft').removeClass('animated');
    });
    $('.popup__pass').click(function () {
        $('.signin__block').css({
            'left':'-650px'
        });
        if (window.matchMedia('(max-width: 460px)').matches) {
            $('.login__popup-block').css({
                'height':'525px'
            });
        } else {
            $('.login__popup-block').css({
                'height':'550px'
            });
        }
        $('.resetpass__block').css({
            'left':'0',
            'top': '160px'
        }).addClass('bounceInLeft').addClass('animated');
    });
}

$('.signin__btn').click(function () {
    if($('.header__user').hasClass('seo_report_page')){
        $('.tml-submit-wrap.registr-tml-submit-wrap input[name="redirect_to').val(window.location.href);
        $('.login__popup').addClass('limitPopup').css('display','flex');
        $('.signup__block').css({
            'right':'-650px'
        }).removeClass('bounceInRight').removeClass('animated');
        $('.resetpass__block').css({
            'left':'-650px'
        }).removeClass('bounceInLeft').removeClass('animated');
        if (window.matchMedia('(max-width: 460px)').matches) {
            $('.login__popup-block').css({
                'height':'525px'
            });
        } else {
            $('.login__popup-block').css({
                'height':'550px'
            });
        }
        $('.signin__block').css({
            'left':'0'
        }).removeClass('bounceInLeft').removeClass('animated');

        seoReportPoupop();
    }else{
        $('.login__popup').fadeIn().css('display','flex');
        $('.header__menu-mob').removeClass('open');
        if (window.matchMedia('(max-width: 965px)').matches) {
            $('.header__menu-mobile').fadeOut();
        }
    }
    disableScroll();
});
$('.signup__btn').click(function () {
    if($('.header__user').hasClass('seo_report_page')){
        $('.tml-submit-wrap.registr-tml-submit-wrap input[name="redirect_to').val(window.location.href);
        $('.login__popup').addClass('limitPopup').css('display','flex');
        $('.signup__block').css({
            'right':'0',
            'top': '80px'
        }).addClass('animated');
        if (window.matchMedia('(max-width: 460px)').matches) {
            $('.login__popup-block').css({
                'height':'525px'
            });
        } else {
            $('.login__popup-block').css({
                'height':'550px'
            });
        }
        $('.signin__block, .resetpass__block').css({
            'left':'-650px'
        }).removeClass('bounceInLeft').removeClass('animated');

        seoReportPoupop();
    }else {
        $('.signup__block').css({
            'right': '0'
        }).addClass('bounceInRight');
        if (window.matchMedia('(max-width: 460px)').matches) {
            $('.login__popup-block').css({
                'height': '470px'
            });
        } else {
            if ($('html').attr('lang') === 'ru') {
                $('.login__popup-block').css({
                    'height': '550px'
                });
            } else {
                $('.login__popup-block').css({
                    'height': '535px'
                });
            }
        }
        $('.signin__block, .resetpass__block').css({
            'left': '-400px'
        }).removeClass('bounceInLeft').removeClass('animated');
        $('.login__popup').fadeIn().css('display', 'flex');
        $('.header__menu-mob').removeClass('open');
        if (window.matchMedia('(max-width: 965px)').matches) {
            $('.header__menu-mobile').fadeOut();
        }
    }
    disableScroll();
});

$('.form__signin').submit(function (e) {
    e.preventDefault();
    let form = $(this),
        email = form.find('input.email'),
        pass = form.find('input.pass');
    form.find('.form__input').removeClass('form__error');
    form.find('.login_popup-error').fadeOut();
    if(email.val() === ''){
        email.parent().parent().addClass('form__error');
        form.find('.error_emailEmpty').fadeIn();
    }
    else if(email.val().indexOf('@') < 0 || email.val().indexOf('.') < 0){
        email.parent().parent().addClass('form__error');
        form.find('.error_emailIncorrect').fadeIn();
    }
    else{
        $.ajax({
            url : '/wp-admin/admin-ajax.php',
            async: false,
            type: "POST",
            data: {'action': 'check_username', email: email.val(), pass: pass.val()},
            dataType: "json",
            success: function(response) {
                form.find('.form__input').removeClass('form__warning');
                if(response === 0){
                    email.parent().parent().addClass('form__warning');
                    form.find('.error_emailNotExist').fadeIn();
                } else if(response === 1) {
                    pass.parent().parent().addClass('form__warning');
                    form.find('.error_passNotExist').fadeIn();
                } else{
                    localStorage.setItem('visitorExistingCustomer', 'Yes');
                    form.find('.btn').css('color','transparent');
                    form.find('.loader').fadeIn();
                    form.unbind('submit').submit();
                }
            }
        });
    }
});
$('.form__signup').submit(function (e) {
    e.preventDefault();
    let form = $(this);
    form.find('.form__input').removeClass('form__error');
    form.find('.login_popup-error').fadeOut();
    let email = form.find('input.email'),
        pass = form.find('input.pass');
    if(email.val() === ''){
        email.parent().parent().addClass('form__error');
        form.find('.error_emailEmpty').fadeIn();
    } else if(email.val().indexOf('@') < 0 || email.val().indexOf('.') < 0){
        email.parent().parent().addClass('form__error');
        form.find('.error_emailIncorrect').fadeIn();
    }
    else if(pass.val() === '' || pass.val().length < 6 || pass.val().indexOf(' ') > 0){
        pass.parent().parent().addClass('form__error');
        form.find('.error_passEmpty').fadeIn();
    } else{
        $.ajax({
            url : '/wp-admin/admin-ajax.php',
            async: false,
            type: "POST",
            data: {'action': 'check_username', email: email.val(), pass: pass.val()},
            dataType: "json",
            success: function(response) {
                form.find('.form__input').removeClass('form__warning');
                console.log(response);
                if(response === 1 || response === 2){
                    email.parent().parent().addClass('form__warning');
                    form.find('.error_emailExist').fadeIn();
                } else{
                    form.find('.btn').css('color','transparent');
                    form.find('.loader').fadeIn();
                    let today = new Date(),
                        dayOfYear = Math.ceil((today - new Date(today.getFullYear(),0,1)) / 86400000);
                    localStorage.setItem('visitorCohortDay', dayOfYear);
                    form.unbind('submit').submit();
                }
            }
        });
    }
});
$('.form__lostpass').submit(function (e) {
    e.preventDefault();
    let form = $(this);
    form.find('.form__input').removeClass('form__error');
    form.find('.login_popup-error').fadeOut();
    let email = form.find('input.email');
    if(email.val() === ''){
        email.parent().parent().addClass('form__error');
        form.find('.error_emailEmpty').fadeIn();
    } else if(email.val().indexOf('@') < 0 || email.val().indexOf('.') < 0){
        email.parent().parent().addClass('form__error');
        form.find('.error_emailIncorrect').fadeIn();
    } else{
        $.ajax({
            url : '/wp-admin/admin-ajax.php',
            async: false,
            type: "POST",
            data: {'action': 'check_username', email: email.val(), pass: ''},
            dataType: "json",
            success: function(response) {
                form.find('.form__input').removeClass('form__warning');
                if(response === 0){
                    email.parent().parent().addClass('form__warning');
                    form.find('.error_emailNotExist').fadeIn();
                } else{
                    form.find('.btn').css('color','transparent');
                    form.find('.loader').fadeIn();
                    form.unbind('submit').submit();
                }
            }
        });
    }
});
$('.newpass-form').submit(function (e) {
    e.preventDefault();
    let form = $(this);
    form.find('.form__input').removeClass('form__error').find('.error').fadeOut();
    let pass = form.find('input.pass');
    if(pass.val() === '' || pass.val().length < 6){
        pass.parent().parent().addClass('form__error').find('.error').fadeIn();
    } else{
        form.find('.btn').css('color','transparent');
        form.find('.loader').fadeIn();
        form.unbind('submit').submit();
    }
});

$( window ).resize(function() {
    if (window.matchMedia('(min-width: 965px)').matches) {
        $('.header__menu-mobile').fadeIn();
    }else{
        $('.header__menu-mobile').css('display','none');
    }
});

$('.popup__signup').click(function () {
    $('.signup__block').css({
        'right':'0'
    }).addClass('bounceInRight').addClass('animated');
    if (window.matchMedia('(max-width: 460px)').matches) {
        $('.login__popup-block').css({
            'height':'470px'
        });
    } else {
        if($('html').attr('lang') === 'ru'){
            $('.login__popup-block').css({
                'height':'550px'
            });
        }else{
            $('.login__popup-block').css({
                'height':'535px'
            });
        }
    }
    $('.signin__block, .resetpass__block').css({
        'left':'-400px'
    }).removeClass('bounceInLeft').removeClass('animated');
});
$('.popup__signin').click(function () {
    $('.signup__block').css({
        'right':'-400px'
    }).removeClass('bounceInRight').removeClass('animated');
    if (window.matchMedia('(max-width: 460px)').matches) {
        $('.login__popup-block').css({
            'height':'410px'
        });
    } else {
        $('.login__popup-block').css({
            'height':'460px'
        });
    }
    $('.signin__block').css({
        'left':'0'
    }).addClass('bounceInLeft').addClass('animated');
});
$('.popup__pass').click(function () {
    $('.signin__block').css({
        'left':'-400px'
    });
    if (window.matchMedia('(max-width: 460px)').matches) {
        $('.login__popup-block').css({
            'height': '295px'
        });
    } else {
        $('.login__popup-block').css({
            'height':'340px'
        });
    }
    $('.resetpass__block').css({
        'left':'0'
    }).addClass('bounceInLeft').addClass('animated');
});

$('.lang__selected').click(function (e) {
    e.preventDefault();
    let lang_menu = $(this).parent();
    if(lang_menu.hasClass('open')){
        lang_menu.removeClass('open').find('.sub__lang').fadeOut(200);
    } else{
        lang_menu.addClass('open').find('.sub__lang').fadeIn(200);
    }
});

$('.footer__lang').click(function () {
    if($(this).hasClass('active')){
        $(this).removeClass('active');
        $('.sub__lang').fadeOut();
    } else{
        $(this).addClass('active');
        $('.sub__lang').fadeIn();
    }
});

$('.header__menu-mob').click(function () {
    $(this).addClass('open');
    $('.header__menu-mobile').fadeIn().css('display','flex');
    $('.mobile__close').click(function () {
        $('.header__menu-mobile').fadeOut();
        $(this).removeClass('open');
    })
});
$('.article__seo-search input').focusin(function () {
    $('.article__seo-search').addClass('active');
}).focusout(function () {
    $('.article__seo-search').removeClass('active');
});

$('.article__menu-small').click(function () {
    if($(this).hasClass('active')){
        $(this).removeClass('active');
        $('.article__menu-block').fadeOut(200);
    }else{
        $(this).addClass('active');
        $('.article__menu-block').fadeIn(200);
    }
});

$(".article__content-body a, .article__menu-block a").click(function() {
    let elementClick = $(this).attr("href"),
        destination = ($(elementClick).offset().top)-10,
        menu = $('.article__menu-small');
    jQuery("html:not(:animated),body:not(:animated)").animate({
        scrollTop: destination
    }, 800);
    if(menu.hasClass('active')){
        menu.removeClass('active');
        $('.article__menu-block').fadeOut(200);
    }
    return false;
});

if($('.main').hasClass('article__page')){
    let documentHeight = $( document ).height(),
        footerHeight = $('.footer').height(),
        contentHeight = $('.article__content-block').height(),
        morePostsHeight = $('.articles__footer').height(),
        stopMenu = footerHeight+contentHeight+morePostsHeight;
    $(window).scroll(function() {
        if ($(this).scrollTop() >= 1000 && $(this).scrollTop()<stopMenu){
            $('.article__menu-small').fadeIn();
        } else{
            $('.article__menu-small').fadeOut();
        }
    });
}

function copyToClipboard(element, block) {
    let $temp = $("<input>"),
        text = $(element).text();
    $("body").append($temp);
    $temp.val(text).select();
    document.execCommand("copy");
    $temp.remove();
    $(block).addClass('copied');
    function removeClass() {
        $(block).removeClass('copied');
    }
    setTimeout(removeClass, 700);
}
function copy(element, block) {
    let $temp = $("<input>"),
        text = element;
    $("body").append($temp);
    $temp.val(text).select();
    document.execCommand("copy");
    $temp.remove();
    $(block).addClass('copied');
    function removeClass() {
        $(block).removeClass('copied');
    }
    setTimeout(removeClass, 700);
}

$('.monitoring_navigation').click(function () {
    let nav = $('.monitoring__nav');
    if(nav.hasClass('active')){
        nav.removeClass('active');
    } else{
        nav.addClass('active');
    }
});
$('.domains-tab').click(function () {
    let block = $(this);
    if(block.hasClass('domains')){
        block.removeClass('domains');
    } else{
        block.addClass('domains');
    }
});
if (!window.matchMedia('(max-width: 965px)').matches) {
    $(".user-bell").mouseenter(function() {
        let list = $('.user-bell_list'),
            bell = $(this);
        if(list.hasClass('active')){
            list.removeClass('active')
        }else{
            let newsCount = $(this).attr('name');
            list.addClass('active');
            if(bell.hasClass('request')){
                bell.removeClass('request');
                $.ajax({
                    url: "/wp-admin/admin-ajax.php",
                    type: "POST",
                    dataType: "JSON",
                    data: {
                        newsCount: newsCount,
                        action: 'update_news'
                    },
                    success: function (data) {
                        console.log(data);
                    }
                });
            }
        }
    }).mouseleave(function () {
        let list = $('.user-bell_list');
        list.removeClass('active');
        $('.bellPosts').empty();
    });
}

const crawlerNotificationsToggle = $('#crawl_report-notification');
crawlerNotificationsToggle.change(function () {
    const apiPath = "/wp-admin/admin-ajax.php";
    crawlerNotificationsToggle.attr("disabled", true);
    function finishCrawlerNotificationsToggle(data) {
        console.log(data);
        crawlerNotificationsToggle.removeAttr("disabled");
    }
    $.ajax({
        url: apiPath,
        type: "POST",
        dataType: "JSON",
        data: {
            action: 'crawler_notification_toggle'
        },
        success: finishCrawlerNotificationsToggle,
        error: finishCrawlerNotificationsToggle
    });
});

if(!$('.header__user').hasClass('not__login') && url__params.indexOf('tool') <= 0){
    const apiPath = "/wp-admin/admin-ajax.php";
    $.ajax({
        url: apiPath,
        type: "POST",
        dataType: "JSON",
        data: {
            action: 'crawler_domain_list'
        },
        success: function(data){
            $('.header_dashboard__list').empty();
        $.each( data.data, function( key, value ) {
            let domainId = value.domain_id,
                domainUrl = value.domain_url,
                domainLogo = value.favicon_url;

            domainUrl = domainUrl.replace(/(^\w+:|^)\/\//, '');

            $('.header_dashboard__list').append('<a href="/tool/crawl-report-domain/?id='+domainId+'"><span class="logo" style="background-image: url('+domainLogo+')"></span><span class="url">'+domainUrl+'</span></a>');
        });
        }
    });
}else{
    $('.pmpro_form').submit(function (e) {
        let form = $(this),
            planId = form.find('input[name="level_id"]').val();
        e.preventDefault();
        setTimeout(function () {
            $('.signin__btn').click();
        },300);
        let redirect = url__params+'?plan='+planId;
        $('.tml-submit-wrap input[name="redirect_to"]').val(redirect);
        $('.tml-submit-wrap.registr-tml-submit-wrap input[name="redirect_to').val(redirect+'&registration');
    })
}

$('.form__Check').submit(function (e) {
    e.preventDefault();
    let form = $(this),
        input = $(this).find('input'),
        url = input.val().replace(/^\s+/, '').toLowerCase(),
        loading = $(this).find('img.loading'),
        arrow = $(this).find('img.arrow'),
        btn__text = $(this).find('button span');
    const apiPath = "/wp-admin/admin-ajax.php";
    if(url === ''){
        form.addClass('no_valid')
    }else{
        arrow.css('display','none');
        btn__text.css('display','none');
        loading.fadeIn();
        form.removeClass('no_valid').addClass('checking');
        let language = $('html')[0].lang;
        language = '/'+language+'/';
        if(language == '/en/'){
            language = '/';
        }
        let seo__link = form.attr('name'),
            link = window.location.protocol + '//' + window.location.hostname + language + seo__link+'/' + url;
        window.location.href = link;
    }
});

$('#refresh__report').click(function () {
    let url = $('#sitechecker_input input').val(),
        language = $('html')[0].lang;
    language = '/'+language+'/';
    if(language == '/en/'){
        language = '/';
    }
    let link = window.location.protocol + '//' + window.location.hostname + language + 'seo-report/' + url;
    window.location.href = link;
});
$('#copyReportLink').click(function () {
    let button = $(this),
        url = window.location.href;
    button.find('.tooltip').text('URL copied!');
    copy(url, button);
    function refreshCopied(){
        button.find('.tooltip').text('Copy report URL');
    }
    setTimeout(refreshCopied,3000);
});

$('.delete_acc_btn').click(function () {
    $('.delete-acc__popup').fadeIn().css('display','flex');
    disableScroll();
});
$('.delete-acc-block .login__popup-close, .delete-acc-block .popup-close').click(function () {
    $('.delete-acc__popup').fadeOut();
    enableScroll();
});

$('.acc-cancel').click(function () {
    $('.cancel-acc__popup').fadeIn().css('display','flex');
    disableScroll();
});
$('.cancel-acc__popup .login__popup-close, .cancel-acc__popup .popup-cancel-close').click(function () {
    $('.cancel-acc__popup').fadeOut();
    enableScroll();
});

function convertDate(dateFormat) {
    let date = new Date(dateFormat);
    const weekday = ["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"],
        monthNames = ["January", "February", "March", "April", "May", "June",
            "July", "August", "September", "October", "November", "December"
        ];
    return weekday[date.getDay()]+' '+monthNames[date.getMonth()]+' '+date.getDate()+', '+date.getFullYear()+', '+formatAMPM(date);
}

function formatAMPM(date) {
    let hours = date.getHours(),
        minutes = date.getMinutes(),
        ampm = hours >= 12 ? 'pm' : 'am';
    hours = hours % 12;
    hours = hours ? hours : 12;
    minutes = minutes < 10 ? '0'+minutes : minutes;
    return hours + ':' + minutes + ' ' + ampm;
}
$('.monitoring__graph-pages .monitoring__filter-index').change(function () {
    if($(this).hasClass('active')){
        $(this).removeClass('active');
    }else{
        $(this).addClass('active');
    }
    $('.highcharts-legend-item.highcharts-series-0').click();
});

$('.monitoring__graph-data div').click(function () {
    if($(this).hasClass('active')){
        $(this).removeClass('active');
    }else{
        $(this).addClass('active');
    }
    if($(this).hasClass('changes')){
        $('.highcharts-legend-item.highcharts-series-1').click();
    }else if($(this).hasClass('added')){
        $('.highcharts-legend-item.highcharts-series-2').click();
    }else if($(this).hasClass('deleted')){
        $('.highcharts-legend-item.highcharts-series-3').click();
    }
});
$('.product__menu-switch').click(function () {
    let productMenu = $('.product__menu');
    if(productMenu.hasClass('active')){
        productMenu.removeClass('active');
    }else{
        productMenu.addClass('active');
    }
});

$('body').on('click','.history__block-data .top',function () {
    let block = $(this).parent();
    if(block.hasClass('active')){
        block.removeClass('active');
    } else{
        block.addClass('active');
    }
}).on('click', '.monitoring__history-event .header', function () {
    let block = $(this).parent();
    if(block.hasClass('open')){
        block.removeClass('open').find('.body').animate({height: 0}, 300);
    }else{
        block.addClass('open').find('.body').each(function () {
            let elementAutoHeight = $(this).css('height', 'auto').height();
            $(this).height(0).animate({height: elementAutoHeight}, 300);
        });
    }
}).on('click', '.validation__param_block .title', function () {
    let block = $(this).parent();
    if(block.hasClass('open')){
        block.removeClass('open').find('div.description').animate({height: 0}, 300);
    }else{
        block.addClass('open').find('div.description').each(function () {
            let elementAutoHeight = $(this).css('height', 'auto').height();
            $(this).height(0).animate({height: elementAutoHeight}, 300);
        });
    }
}).on('click', '.validation__block-title', function () {
    let block = $(this);
    function setAutoHeight() {
        block.next().css('height', 'auto');
    }
    if(block.hasClass('closed')){
        block.removeClass('closed');
        let autoHeight = block.next().css('height', 'auto').height();
        block.next().css('height', 0).removeClass('closed').animate({ 'min-height': autoHeight, 'opacity': 1}, 500);
        setTimeout(setAutoHeight, 1000)
    }else{
        block.addClass('closed').next().addClass('closed').animate({ 'min-height': 0, 'height': 0, 'opacity': 0 }, 500);
    }
}).on('click', '.report__sidenav-title', function () {
    let block = $(this);
    if(block.hasClass('closed')){
        block.removeClass('closed');
        let autoHeight = block.next().css('height', 'auto').height();
        block.next().height(0).removeClass('closed').animate({height: autoHeight}, 500);
        $(this).find('.showing').fadeOut();
    }else{
        block.addClass('closed').next().addClass('closed').animate({height: 0}, 500);
        $(this).find('.showing').fadeIn();
    }
}).on('click','.expand-headers',function () {
    let table = $(this);
    if(table.hasClass('clicked')){
        table.removeClass('clicked');
        table.parent().find('tr:nth-child(n+6)').fadeOut();
        table.find('span').text('Show more');
    }else{
        table.find('span').text('Show less');
        table.addClass('clicked');
        table.parent().find('tr').fadeIn().css('display','table-row');
    }
}).on('click','.expand',function () {
    let table = $(this);
    if(table.hasClass('clicked')){
        table.removeClass('clicked');
        table.parent().find('tr:nth-child(n+12)').fadeOut();
        table.css('display','block');
        table.find('span').text('Show more');
    }else{
        table.find('span').text('Show less');
        table.addClass('clicked');
        table.parent().find('tr').fadeIn().css('display','table-row');
    }
}).on('click','.report__sidenav-link',function () {
    let elementClick = $(this).attr('data-index');
        if(elementClick === '2.1'){
            elementClick = '2-1';
        }
        else if(elementClick === '13.1'){
            elementClick = '13-1';
        }
    let destination = $('.report__validations #result-'+elementClick+'').offset().top;
    $("html:not(:animated),body:not(:animated)").animate({
        scrollTop: destination
    }, 800);
    return false;
}).on('click','.responses__row',function () {
    if($(this).hasClass('responses__row-demo')){
        $('.form__input').addClass('shake active');
        setTimeout(function () {
            $('.form__input').removeClass('shake active');
        },1000)
    }else{
        let elementClick = $(this).attr('data-index');
        if(elementClick === '2.1'){
            elementClick = '2-1';
        }else if(elementClick === '13.1'){
            elementClick = '13-1';
        }
        let destination = $('.report__validations #result-'+elementClick+'').offset().top;
        $("html:not(:animated),body:not(:animated)").animate({
            scrollTop: destination
        }, 800);
        return false;
    }
}).on('click', '.overview__sidenav-title', function () {
    let block = $(this);
    if(block.hasClass('closed')){
        block.removeClass('closed');
        let autoHeight = block.next().css('height', 'auto').height();
        block.next().height(0).removeClass('closed').animate({height: autoHeight}, 500);
    }else{
        block.addClass('closed');
        block.next().addClass('closed').animate({height: 0}, 500);
    }
}).on('change', '.dashboard__monitoring-switch input', function () {
    let monitoringStatus = $(this).is(':checked');
    if(monitoringStatus === false){
        $(this).parent().parent().parent().addClass('monitoring__disable');
    } else{
        $(this).parent().parent().parent().removeClass('monitoring__disable');
    }
}).on('click', '.dashboard__settings', function (event) {
    event.stopPropagation();
    let settings = $(this),
        settingsPopup = settings.next();
    if(settings.hasClass('active')){
        settings.removeClass('active');
        settingsPopup.fadeOut();
    }else{
        settings.addClass('active');
        settingsPopup.fadeIn().css('display','flex');
    }
}).on('click', '.dashboard__settings-data', function (event) {
    event.stopPropagation();
}).on('click', '.copy_url', function () {
    console.log('copied!');
    let $temp = $("<input>"),
        text = $('#share_url').val();

    $("body").append($temp);
    $temp.val(text).select();
    document.execCommand("copy");
    $temp.remove();
    $(this).text('Copied!');

    function changeText(){
        $('.copy_url').text('Copy');
    }
    setTimeout(changeText, 2000);
}).on('click', 'tr.monitoring__disable .monitoring__btn', function (e) {
    e.preventDefault();
    let id = $(this).attr('data-id'),
        ring = $('.dashboard__monitoring-switch[data-id="'+id+'"]');
    ring.addClass('ringing');
    setTimeout(function () {
        ring.removeClass('ringing');
    },3000)
});

$('.settings-btn').click(function () {
    if($(this).hasClass('open')){
        $(this).removeClass('open');
        $('.settings-content').fadeOut();
    }else{
        let threadValue = $('#settings__thread'),
            currentValue = threadValue.slider('getValue');
        $('.settings-content').fadeIn().css('display','flex');
        $(this).addClass('open');
        threadValue.slider('refresh');
        threadValue.slider('setValue', currentValue);
    }
});

$(window).click(function() {
    let addDomainSettings = $('.settings-btn'),
        settings = $('.dashboard__settings');
    if(addDomainSettings.hasClass('open')){
        addDomainSettings.removeClass('open');
        $('.settings-content').fadeOut();
    }
    if(settings.hasClass('active')){
        addDomainSettings.removeClass('open');
        settings.removeClass('active');
        $('.dashboard__settings-data').fadeOut();
    }
});
$('.dashboard__header').click(function (event) {
    event.stopPropagation();
});
$('.dashboard__header-form input').focusin(function () {
    if(!$('.dashboard__header-settings').hasClass('free__user')){
        $('.settings-btn').addClass('activate').click();
    }
}).focusout(function () {
    $('.settings-btn').removeClass('activate');
});

$('#menucontainer').click(function(event){
    event.stopPropagation();
});

if (window.matchMedia('(max-width: 640px)').matches) {
    $('.product__menu-block a.active').click(function (e) {
        e.preventDefault();
        let parent = $(this).parent(),
            menu = parent.find('a');
        if(parent.hasClass('opened')){
            $(menu).each(function () {
                if(!$(this).hasClass('active')){
                    $(this).fadeOut();
                }
            });
            parent.removeClass('opened');
        }else{
            $(menu).each(function () {
                if(!$(this).hasClass('active')){
                    $(this).fadeIn().css('display','flex');
                }
            });
            parent.addClass('opened');
        }
    })
}

let sidebarWidth = $('.report__sideblock').width();
$('.report__sidenav, .report__sideblock').css({
    'min-width': sidebarWidth,
    'width': sidebarWidth
});

let checkAccordion = $('#check-accordion'),
    auditAccordion = $('#audit-accordion'),
    monitoringAccordion = $('#monitoring-accordion'),
    auditLandingAccordion = $('#auditLanding-accordion');

function accordionChange(elem, accordion, block){
    let imgSrc = '/wp-content/themes/sitechecker/out/img_design/'+elem.attr('aria-labelledby')+'.svg';
    $('.'+block+' img').attr('src',imgSrc);
    $(this).parent().addClass('active');
    let pevious = accordion.find('.collapse.show');
    pevious.collapse('hide');
    pevious.parent().removeClass('active');
}

checkAccordion.on('show.bs.collapse','.collapse', function() {
    accordionChange($(this), checkAccordion,'illustrationCheck');
});
auditAccordion.on('show.bs.collapse','.collapse', function() {
    accordionChange($(this), auditAccordion,'illustrationAudit');
});
monitoringAccordion.on('show.bs.collapse','.collapse', function() {
    accordionChange($(this), monitoringAccordion,'illustrationMonitoring');
});
auditLandingAccordion.on('show.bs.collapse','.collapse', function() {
    $('.audit__five-images img').css('display','none');
    $('.'+$(this).attr('aria-labelledby')).fadeIn();
    $(this).parent().addClass('active');
    let pevious = auditLandingAccordion.find('.collapse.show');
    pevious.collapse('hide');
    pevious.parent().removeClass('active');
});

$('.audit__form').submit(function (e) {
    e.preventDefault();
    let form = $(this),
        apiPath = "/wp-admin/admin-ajax.php",
        input = $(this).find('input'),
        url = input.val().replace(/\s/g, "").toLowerCase(),
        loading = $(this).find('img.loading'),
        arrow = $(this).find('img.arrow'),
        button = $(this).find('button').attr('class');

    function auditForm(){
        arrow.css('display','none');
        loading.fadeIn();
        form.removeClass('no_valid').removeClass('no_limits').addClass('checking');
        $.ajax({
            url: apiPath,
            type: "POST",
            dataType: "JSON",
            async: true,
            data: {
                action: 'crawler_validate_domain_request',
                url: url
            },
            success: function (data) {
                console.log(data);
                if(data.status === false){
                    form.removeClass('no_limits').removeClass('checking').addClass('no_valid');
                    arrow.fadeIn();
                    loading.css('display','none');
                } else{
                    let language = $('html')[0].lang;
                    console.log(language);
                    language = '/'+language+'/';
                    if(language == '/en/'){
                        language = '/';
                    }
                    input.val(data.valid);

                    if(button === 'sitechecker_reg'){
                        $('.signup__btn').click();
                        $('.tml-submit-wrap input[name="redirect_to"]').val("/tool/create_user_domain/?url="+data.valid);
                    }else if(button === 'sitechecker_reg-monitoring'){
                        $('.signup__btn').click();
                        $('.tml-submit-wrap input[name="redirect_to"]').val("/tool/create_user_domain/?url="+data.valid+"&monitoringEnabled=1");
                    }else if(button === 'sitechecker_crawl'){
                        window.location.href = "/tool/create_user_domain/?url="+data.valid;
                    }else if(button === 'limits__btn'){
                        window.location.href = "/tool/crawl-report/";
                    } else if(button === 'sitechecker_monitoring'){
                        window.location.href = "/tool/create_user_domain/?url="+data.valid+"&monitoringEnabled=1";
                    }
                }
            }
        });
    }

    if(url === ''){
        form.removeClass('no_limits').addClass('no_valid');
    }else{
        if(!$('.header__user').hasClass('not__login')){
            $.ajax({
                url: apiPath,
                type: "POST",
                dataType: "JSON",
                data: {
                    action: "crawler_get_limits",
                },
                success: function (limits) {

                    if(button === 'sitechecker_monitoring'){
                        if(limits.data.availbleMonitoring === 0){
                            form.removeClass('no_valid').addClass('no_limits');
                        }else{
                            auditForm();
                        }
                    }else {
                        if(limits.data.availableDomains === 0 || limits.data.availablePages === 0){
                            form.removeClass('no_valid').addClass('no_limits');
                        }else{
                            auditForm();
                        }
                    }
                }
            });
        }else{
            auditForm();
        }
    }
});
$('.monitoring__form').submit(function (e) {
    e.preventDefault();
    let form = $(this),
        apiPath = "/wp-admin/admin-ajax.php",
        input = $(this).find('input'),
        url = input.val().replace(/\s/g, "").toLowerCase(),
        loading = $(this).find('img.loading'),
        arrow = $(this).find('img.arrow'),
        button = $(this).find('button').attr('class');

    function monitoringForm() {
        arrow.css('display','none');
        loading.fadeIn();
        form.removeClass('no_valid').addClass('checking');
        $.ajax({
            url: apiPath,
            type: "POST",
            dataType: "JSON",
            async: true,
            data: {
                action: 'crawler_validate_domain_request',
                url: url
            },
            success: function (data) {
                console.log(data);
                if (data.status === false) {
                    form.removeClass('checking').addClass('no_valid');
                    arrow.fadeIn();
                    loading.css('display', 'none');
                } else {
                    let language = $('html')[0].lang;
                    console.log(language);
                    language = '/' + language + '/';
                    if (language == '/en/') {
                        language = '/';
                    }
                    input.val(data.valid);
                    if (button === 'sitechecker_reg-monitoring') {
                        $('.signin__btn').click();
                        $('.tml-submit-wrap input[name="redirect_to"]').val("/tool/create_user_domain/?url=" + data.valid + "&monitoringEnabled=1");
                    } else if (button === 'limits__btn') {
                        window.location.href = "/tool/crawl-report/";
                    } else if (button === 'sitechecker_monitoring') {
                        window.location.href = "/tool/create_user_domain/?url=" + data.valid + "&monitoringEnabled=1";
                    }
                }
            }
        });
    }

    if(url === ''){
        form.removeClass('no_limits').addClass('no_valid');
    }else {

        if(!$('.header__user').hasClass('not__login')){
            $.ajax({
                url: apiPath,
                type: "POST",
                dataType: "JSON",
                data: {
                    action: "crawler_get_limits",
                },
                success: function (limits) {
                    console.log(limits);
                    if(limits.data.availbleMonitoring === 0 || limits.data.availableDomains === 0 || limits.data.availablePages === 0){
                        form.removeClass('no_valid').addClass('no_limits');
                    }else{
                        monitoringForm();
                    }
                }
            });
        }else{
            monitoringForm();
        }
    }
});

$('.article__content-block code').each(function () {
    let code = $(this);
    code.addClass('line-numbers language-javascript').wrap('<pre class="line-numbers"></pre>');
    $('.line-numbers').click(function () {
        let code = $(this).find('code');
        copyToClipboard(code, $(this));
    });
});
$('.line-numbers').click(function () {
    let code = $(this).find('code');
    copyToClipboard(code, $(this));
});

if(window.matchMedia('(max-width: 640px)').matches){
    $('.about__team-container').owlCarousel({
        loop:true,
        nav:false,
        margin: 10,
        responsive:{
            0:{
                items:1
            }
        }
    });
}

$('.contacts__form').on('submit', function (e) {
    e.preventDefault();
    $('.contacts__form-input').removeClass('error');
    let name = $('.contacts__form-name').val(),
        email = $('.contacts__form-email').val(),
        message = $('.contacts__form-message').val(),
        button = $(this).find('button');
    if(name === ''){
        $('.contacts__form_name').addClass('error');
    }else if(email === '' || email.indexOf('.') === -1){
        $('.contacts__form_email').addClass('error');
    }else{
        button.find('span').css('display','none');
        button.find('.loading').fadeIn();
        $.ajax({
            url: "/wp-admin/admin-ajax.php",
            type: "POST",
            dataType: "JSON",
            data: {
                email: email,
                name: name,
                message: message,
                action: 'contact_form'
            },
            success: function (data) {
                $('.contacts__form-name, .contacts__form-email, .contacts__form-message').val('');
                button.find('.loading').css('display','none');
                button.find('span').text('Thank you. Your message sent!').fadeIn();
                setTimeout(function () {
                    button.find('span').text('send message');
                }, 3000);
            }
        });
    }
});

$( "#level_checkout input" ).change(function() {
    if($('#level_checkout input').is(":checked")){
        function rotation(elem){
            $(elem).addClass('rotate');
        }
        rotation($('.levels_block-side:first-of-type .card'));
        setTimeout(rotation, 50, $('.levels_block-side:nth-child(2) .card'));
        setTimeout(rotation, 100, $('.levels_block-side:nth-child(3) .card'));
        setTimeout(rotation, 150, $('.levels_block-side:nth-child(4) .card'));
        setTimeout(rotation, 200, $('.levels_block-side:nth-child(5) .card'));
    } else{
        function rotation_remove(elem){
            $(elem).removeClass('rotate');
        }
        rotation_remove($('.levels_block-side:first-of-type .card'));
        setTimeout(rotation_remove, 50, $('.levels_block-side:nth-child(2) .card'));
        setTimeout(rotation_remove, 100, $('.levels_block-side:nth-child(3) .card'));
        setTimeout(rotation_remove, 150, $('.levels_block-side:nth-child(4) .card'));
        setTimeout(rotation_remove, 200, $('.levels_block-side:nth-child(5) .card'));
    }
});

$('.front__action-audit').click(function () {
    $('.signup__btn').click();
    $('.tml-submit-wrap input[name="redirect_to"]').val("/tool/crawl-report-domain/?id=16972");
});
$('.front__action-monitoring').click(function () {
    $('.signup__btn').click();
    $('.tml-submit-wrap input[name="redirect_to"]').val("/tool/monitoring/?id=16972");
});
function auditCounting(blockName,count) {
    let $countToBlock = $('#'+blockName),
        countTo = count;
    $({
        countNum: $countToBlock.text()
    }).animate({
            countNum: countTo
        },
        {
            duration: 1000,
            easing: 'swing',
            step: function () {
                $countToBlock.text(Math.floor(this.countNum));
            },
            complete: function () {
                $countToBlock.text(this.countNum);
            }
        });
}
if($('.audit__landing').length>0){
    $(window).scroll(function() {
        if($('.audit__nine').length>0){
            let audit__nine =  $('.audit__nine').offset().top;
            if ($(this).scrollTop() >= audit__nine-400) {
                auditCounting('audit__stat1',7652);
                auditCounting('audit__stat2',2318231);
                auditCounting('audit__stat3',3264834);
                return false;
            }
        }
    });
}
if($('.article__page').length>0){
    let star = $('.article__info .stars span'),
        starCount = $('.stars__block em strong:first-of-type').text();
    star.text(starCount);
}

$('#toOldDesign').click(function () {
    document.cookie="design_new=0;path=/";
    location.reload();
});

$('.domains__tab button, .domains__info-text button').click(function () {
    $('.domains__tab button').removeClass('active');
    let dataID = $(this).attr('data-id');
    if(dataID){
        $('.'+dataID).addClass('active');
    } else{
        $(this).addClass('active');
    }

    if($(this).hasClass('anchor-tab') || dataID === 'anchor-tab'){
        $('.anchors_export').fadeIn(200);
    }else{
        $('.anchors_export').fadeOut(200);
    }
});

$('.fix').click(function () {
    let block =  $(this);
    if(block.hasClass('open')){
        block.removeClass('open');
        if(block.parent().hasClass('issues')){
            block.prev().find('li:nth-child(n+6)').fadeOut();
            block.find('span').text('Show more');
        } else if(block.parent().hasClass('anchors')){
            block.prev().find('tr:nth-child(n+6)').fadeOut();
            if(block.prev().hasClass('optimization_data')){
                block.find('span').text('Show');
            } else{
                block.find('span').text('Show more');
            }
        }
    } else{
        block.addClass('open');
        if(block.parent().hasClass('issues')){
            console.log('issue');
            block.prev().find('li:nth-child(n+6)').fadeIn();
            block.find('span').text('Show less');
        } else if(block.parent().hasClass('anchors')){
            console.log('anchors');
            block.prev().find('tr:nth-child(n+6)').css('display','table-row');
            if(block.prev().hasClass('optimization_data')){
                block.find('span').text('Hide');
            } else{
                block.find('span').text('Show less');
            }
        }
    }
});
$('.close__popup-btn').click(function () {
    $('#overview__popup').fadeOut();
    $('body').css('overflow', 'auto');
});

$('#settings__pages-value').change(function () {
    let setValue = $(this).val(),
        slider = $('#settings__pages');
    slider.slider('setValue', setValue);
});
$('#settings__pages').change(function () {
    $('#settings__pages-value').val($(this).val());
});

let getUrlParameter = function getUrlParameter(sParam) {
    let sPageURL = decodeURIComponent(window.location.search.substring(1));
    if(sPageURL){
        let sURLVariables = sPageURL.split('&'),
            sParameterName,
            i;

        for (i = 0; i < sURLVariables.length; i++) {
            sParameterName = sURLVariables[i].split('=');

            if (sParameterName[0] === sParam) {
                return sParameterName[1] === undefined ? true : sParameterName[1];
            }
        }
    }
};

let planID = getUrlParameter('plan'),
    planRegistration = getUrlParameter('registration'),
    cookiesPlans = localStorage.getItem('cookiesPlans'),
    socialPaypalLogin = localStorage.getItem('socialPaypalLogin'),
    socialPaypalRegistration = localStorage.getItem('socialPaypalRegistration'),
    socialPlan = localStorage.getItem('socialPlan');

if(planID === '1'){
    $('#pricing__btns .pmpro_form[name="1"]').click();
} else if(planID){
    if(!planRegistration){
        $('.paypal_redirect p.title').text('You are logged in now');
    }
    function redirection(){
        $('.paypal_redirect').css('display','flex');
        setTimeout(function () {
            $('.pmpro_form[name="'+planID+'"]').submit();
        },300);
        let planText = '?plan='+planID;
        if(planRegistration){
            console.log('planRegistration');
            planText = '?plan='+planID+'&registration';
        }
        domain_loc = domain_loc.replace(planText,'');
        window.history.replaceState(domain_loc, '', domain_loc);
    }
    setTimeout(redirection, 2000);
}

function redirectionSocial(){
    $('.paypal_redirect').css('display','flex');
    $('.pmpro_btn-submit-checkout[name="'+socialPlan+'"]').click();
}
if(socialPaypalRegistration == 'signupPaypal'){
    setTimeout(redirectionSocial, 2000);
    socialPaypalRegistration = localStorage.setItem('socialPaypalRegistration', 'false');
} else if(socialPaypalLogin == 'loginPaypal'){
    $('.paypal_redirect p.title').text('You are logged in now');
    setTimeout(redirectionSocial, 2000);
    socialPaypalLogin = localStorage.setItem('socialPaypalLogin', 'false');
}

$('#logout').click(function () {
    cookiesPlans = localStorage.setItem('cookiesPlans', 'false');
});

$('.searching_url .fa-search').click(function () {
    $(this).css('display', 'none');
    $('.pageTable_filter').css('display', 'flex');
});

$('.pageTable_filter .fa-times').click(function () {
    $('.pageTable_filter').css('display', 'none');
    $('.searching_url .fa-search').css('display', 'block');
});
$('.heateor_ss_social_login_optin').attr('checked',true);

$('.file_upload__gray').click(function () {
    $('#pdf__branding input').click();
});
$('#pdf__branding input').change(function () {
    $('#pdf__branding').submit();
});
$('#pdf__branding').submit(function (e) {
    if($(this).hasClass('free__plan')){
        e.preventDefault();
        window.location.href = '/plans/?pdfBranding';
    }else{
        $('.file_upload-btn').addClass('load');
        let apiPath = "/wp-admin/admin-ajax.php";
        $.ajax({
            url: apiPath,
            type: "POST",
            dataType: "JSON",
            data: {
                action: 'set__branding'
            },
            success: function (data) {
                console.log(data);
            }
        });
    }
});
$('.delete__pdf-branding').click(function () {
    let apiPath = "/wp-admin/admin-ajax.php";
    $('.delete__branding').css('display','none');
    $(this).find('.loading').fadeIn();
    $.ajax({
        url: apiPath,
        type: "POST",
        dataType: "JSON",
        data: {
            action: 'delete__branding'
        },
        success: function (data) {
            console.log(data);
            location.reload();
        }
    });
});

if(url__params.indexOf('pdfBranding') > 0){
    $('.pdf_banner__branding').css('display','flex');
    dataLayer.push({ 'event': 'autoEvent', 'eventCategory': 'PDF Branding limited popup shown', 'eventAction': 'shown' });
    domain_loc = domain_loc.replace('?pdfBranding','');
    window.history.replaceState(domain_loc, '', domain_loc);
}
$('.pdf_banner__branding-close').click(function () {
    $('.pdf_banner__branding').addClass('fade_out');
});

function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#img__prev')
                .attr('src', e.target.result);
        };
        reader.readAsDataURL(input.files[0]);
    }
}
$('.important__overview-name').click(function() {
    let content = $('.important__overview-content');
    if(content.hasClass('open')){
        content.removeClass('open').css('display','none')
    }else{
        content.addClass('open').fadeIn();
    }

});
$('.language_menu').click(function(event){
    event.stopPropagation();
    if($('.language_menu>li').hasClass('active')){
        $('.language_menu .sub-menu').hide();
        $('.language_menu>li').removeClass('active');
    } else{
        $('.language_menu .sub-menu').show();
        $('.language_menu>li').addClass('active');
    }
});

$('#tour__owners').click(function () {
    $('.tour__popup-container').addClass('tour');
    $('.tour__first').css('display','none');
    $('.tour__second').css('display','block');
});
$('#tour__specialists').click(function () {
    $('.tour__popup-container').addClass('tour');
    $('.tour__first').css('display','none');
    $('.tour__third').css('display','block');
});
$('#tour__agencies').click(function () {
    $('.tour__popup-container').addClass('tour');
    $('.tour__first').css('display','none');
    $('.tour__four').css('display','block');
});

$('.tour__popup-close').click(function () {
    $('.tour__popup').fadeOut();
    setTimeout(function () {
        $('.tour__popup-container').removeClass('tour');
        $('.tour__first').css('display','block');
        $('.tour__second, .tour__third, .tour__four').css('display','none');
    }, 500);
});

$('.back__to__tour').click(function () {
    $('.tour__popup-container').removeClass('tour');
    $('.tour__first').css('display','block');
    $('.tour__second, .tour__third, .tour__four').css('display','none');
});

$('.product__tour').click(function () {
    $('.tour__popup').fadeIn().css('display','flex');
});

$("#settings__pages").slider({
    tooltip: 'always'
});
$('.backlinks__select').select2({
    minimumResultsForSearch: -1
});

$('.semscale_logedin').click(function (e) {
    e.preventDefault();
    $('.signup__btn').click();
    if($(this).hasClass('semscale_request')){
        $('.tml-submit-wrap input[name="redirect_to"]').val('https://surveys.hotjar.com/s?siteId=623968&surveyId=130484');
    }else{
        $('.tml-submit-wrap input[name="redirect_to"]').val('/tools/seo-platform/');
    }
});


if($(".pricingAll_block" ).length > 0){
    $(window).scroll(function(){
        let pricing_table = $( ".pricingAll_block" ),
            pricing_table__height = pricing_table.outerHeight(),
            pricing_table_offset = pricing_table.offset().top,
            pricing__tr = $('.pricingAll_block tr:first-of-type'),
            column_width = pricing__tr.outerWidth(),
            column_height = pricing__tr.outerHeight(),
            column_width_1 = $('.pricingAll_block tr:first-of-type td:first-of-type').outerWidth(),
            moreHeight = pricing_table_offset+(pricing_table__height-231);
        if($(window).scrollTop() >= pricing_table_offset+2 && $(window).scrollTop() < moreHeight){
            $('.tableScroll').css({
                'width': column_width,
                'height': column_height-10,
                'overflow': 'hidden'
            }).addClass('show').find('td:first-of-type').css({
                'width': column_width_1
            });
        }else if($(window).scrollTop() > moreHeight){
            $('.tableScroll').removeClass('show');
        }else{
            $('.tableScroll').removeClass('show');
        }
    });
}

$('.select2-active').select2();
$('#contact_tel').select2({
    placeholder: "Phone code"
});
$('#contact_employees').select2({
    placeholder: "Number of Employees"
});
$('#contact_industry').select2({
    placeholder: "Company Industry"
});
$('#contact_department').select2({
    placeholder: "Your Department"
});
$('#contact_job').select2({
    placeholder: "Job Title"
});

$('.show_limits').click(function () {
    $('.checkout__popup_container').fadeIn().css('display','flex');
    if (window.matchMedia('(min-width: 460px)').matches) {
        disableScroll();
    }
});
$('.checkout__popup-close').click(function () {
    $('.checkout__popup_container').fadeOut();
    enableScroll();
});

$('.checkout__popup-body').on('submit', function (e) {
    e.preventDefault();
    $('.contacts__form-input').removeClass('error');
    let name = $('.checkout__name').val().replace(/\s/g, ""),
        lastname = $('.checkout__lastname').val().replace(/\s/g, ""),
        email = $('.checkout__email').val().replace(/\s/g, ""),
        contact_tel = $('#contact_tel').val(),
        checkout__tel = $('.checkout__tel').val(),
        checkout__companyName = $('.checkout__companyName').val(),
        checkout__companySite = $('.checkout__companySite').val(),
        contact_employees = $('#contact_employees').val(),
        contact_industry = $('#contact_industry').val(),
        contact_department = $('#contact_department').val(),
        contact_job = $('#contact_job').val(),
        checkout__message = $('.checkout__message').val().replace(/\s/g, ""),
        button = $(this).find('button[type="submit"]');

    $('.validation').removeClass('error');
    button.removeClass('error');

    if(name === ''){
        $('.validation_name').addClass('error');
        button.addClass('error');
    } else if(lastname === ''){
        $('.validation_lastname').addClass('error');
        button.addClass('error');
    }else if(email === ''){
        $('.validation_email').addClass('error');
        button.addClass('error');
    } else if(checkout__message === ''){
        $('.validation_message').addClass('error');
        button.addClass('error');
    } else{
        button.removeClass('error').find('span').css('display','none');
        button.find('.loading').fadeIn();

        $.ajax({
            url: "/wp-admin/admin-ajax.php",
            type: "POST",
            dataType: "JSON",
            data: {
                name: name,
                lastname: lastname,
                email: email,
                contact_tel: contact_tel,
                checkout__tel: checkout__tel,
                checkout__companyName: checkout__companyName,
                checkout__companySite: checkout__companySite,
                contact_employees: contact_employees,
                contact_industry: contact_industry,
                contact_department: contact_department,
                contact_job: contact_job,
                checkout__message: checkout__message,
                action: 'checkout_form'
            },
            success: function () {
                button.find('.loading').css('display','none');
                button.find('span').text('Sent!').fadeIn();
                setTimeout(function () {
                    button.find('span').text('Send');
                    $('.checkout__popup-close').click();
                    let popup = $('.resetpass__popup');
                    popup.find('.title').text('Thank you! Your message has been sent successfully.').css({
                        'font-size': '17px',
                        'padding': '0 40px',
                        'line-height': '25px',
                        'margin-bottom': '30px'
                    });
                    popup.find('.description').css('display','none');
                    popup.fadeIn().css('display','flex');
                    disableScroll();
                    function sentEmail() {
                        $('.circle-loader').toggleClass('load-complete');
                        $('.checkmark').toggle();
                        setTimeout(closePopup,4000);
                    }
                    function closePopup(){
                        $('.login__popup-close').click();
                    }
                    setTimeout(sentEmail,1000);
                }, 1000);
            }
        });
    }
});

$('.checkout__testimonial').owlCarousel({
    loop:true,
    nav:false,
    margin: 10,
    autoplay: true,
    autoplayTimeout:7000,
    autoplayHoverPause: true,
    responsive:{
        0:{
            items:1
        }
    }
});
$(".scrollto").click(function() {
    let elementClick = $(this).attr("name"),
        destination = $(elementClick).offset().top;
    jQuery("html:not(:animated),body:not(:animated)").animate({
        scrollTop: destination
    }, 800);
    return false;
});
$('.knbs__latest-block').each(function () {
    let element = $(this).find('.post__ratings em strong:first-of-type').text();
    $(this).find('.post__ratings-result').text(element);
});
$('.cat__prev').each(function () {
    let element = $(this).find('.post__ratings em strong:first-of-type').text();
    $(this).find('.post__ratings-result').text(element);
});

$('#latest').change(function () {
    $('#viewed, #rated').attr('checked',false);
    let element = $(this).is(":checked"),
        state = 'latest';
    element === true ? state = 'latest' : state = 'unlatest';
    get_ajax_posts(state);
});

$('#viewed').change(function () {
    $('#latest, #rated').attr('checked',false);
    let element = $(this).is(":checked"),
        state = 'viewed';
    element === true ? state = 'viewed' : state = 'unviewed';
    get_ajax_posts(state);
});

$('#rated').change(function () {
    $('#viewed, #latest').attr('checked',false);
    let element = $(this).is(":checked"),
        state = 'rated';
    element === true ? state = 'rated' : state = 'unrated';
    get_ajax_posts(state);
});

function get_ajax_posts(state) {
    let category = $('.cat__filters').attr('id');
    $('.cat__container').css('margin-bottom','100px');
    $('.pagination').css('display','none');
    if(state !== 'unviewed' || state !== 'unrated' || state !== 'unlatest'){
        $.ajax({
            type: 'POST',
            url: '/wp-admin/admin-ajax.php',
            dataType: "json",
            data: { action : 'get_ajax_posts', id : category },
            success: function( response ) {
                $('.cat__result').empty();
                if(state === 'viewed'){
                    $.each( response, function( key, value ) {
                        value = value.sort(function(a, b) {
                            return a.view - b.view;
                        });
                        append(value.reverse());
                    } );
                }else if(state === 'rated'){
                    $.each( response, function( key, value ) {
                        value = value.sort(function(a, b) {
                            let rating_a = a.rate,
                                element_a = document.createElement("div"),
                                rating_b = b.rate,
                                element_b = document.createElement("div");
                            $(element_a).html(rating_a);
                            $(element_b).html(rating_b);
                            rating_a = $(element_a).find('em strong:first-of-type').text();
                            rating_b = $(element_b).find('em strong:first-of-type').text();
                            return rating_a - rating_b;
                        });
                        append(value.reverse());
                    } );
                }else{
                    $.each( response, function( key, value ) {
                        append(value);
                    } );
                }

                function append(value) {
                    $.each( value, function( data, val ) {
                        let title = val.title,
                            image = val.img,
                            permalink = val.permalink,
                            rate = val.rate,
                            view = val.view,
                            date = val.date,
                            category = $('.cat__name h1').text();
                        $('.cat__result').append('<div class="cat__prev"><div class="image" style="background-image: url('+image+')"></div><div class="content"><a href="'+permalink+'" class="title">'+title+'</a><div class="data"><div class="article__info"><div class="date"><span>'+date+'</span></div><div class="watches"><svg width="22" height="16" viewBox="0 0 22 16" fill="none" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" clip-rule="evenodd" d="M10.999 4.5C9.339 4.5 8 5.84 8 7.5C8 9.161 9.339 10.501 10.999 10.501C12.66 10.501 14 9.161 14 7.5C14 5.84 12.66 4.5 10.999 4.5ZM10.999 12.5C8.241 12.5 6 10.26 6 7.5C6 4.74 8.241 2.5 10.999 2.5C13.76 2.5 16 4.74 16 7.5C16 10.26 13.76 12.5 10.999 12.5ZM10.999 0C6 0 1.73 3.11 0 7.5C1.73 11.89 6 15 10.999 15C16 15 20.271 11.89 22 7.5C20.271 3.11 16 0 10.999 0Z" transform="translate(0 0.5)" fill="#E6E6E6"></path></svg><span>'+view+'</span></div><div class="stars"><svg width="19" height="17" viewBox="0 0 19 17" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M9.50018 0L12.4355 5.59599L19 6.49359L14.2499 10.8492L15.3712 17L9.50018 14.0962L3.62879 17L4.75009 10.8492L0 6.49359L6.56448 5.59599L9.50018 0Z" fill="#E6E6E6"></path></svg><div class="post__ratings">'+rate+'</div><span class="post__ratings-result">1</span></div></div></div></div></div>');
                    });
                    $('.cat__prev').each(function () {
                        let element = $(this).find('.post__ratings em strong:first-of-type').text();
                        $(this).find('.post__ratings-result').text(element);
                    });
                }
            }
        });
    }
}