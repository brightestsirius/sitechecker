const login_error = window.location.href;
var keys = {};

function copyToClipboard(element) {
    var $temp = $("<input>");
    $("body").append($temp);
    var text = $(element).text();
    $temp.val(text).select();
    document.execCommand("copy");
    $temp.remove();
};

let cookiesPopup = localStorage.getItem('cookiesPopup');
if(cookiesPopup !== 'Shown'){
    $('.footer__cookie').fadeIn();
    cookiesPopup = localStorage.setItem('cookiesPopup', 'Shown');
    $('.footer__cookie-close').click(function () {
        $('.footer__cookie').fadeOut();
    })
}

if(login_error.indexOf('recrawlPopup') > 0){
    let recrawlPopup = localStorage.getItem('recrawlPopup');
    if(recrawlPopup == 'Shown'){
        localStorage.setItem('recrawlPopup', 'false');
        function recrawlConfirfmsClose(){
            $('.recrawl-block').css('display', 'none');
            $('.recrawl_popup').fadeOut();
        }
        $('.recrawl_popup .login_popup-close').click(function(){
            recrawlConfirmsClose();
        });
        setTimeout(recrawlConfirmsClose, 3000);
    }
}

if(login_error.indexOf('checkemail=confirm') > 0){
    let checkemailpopup =  localStorage.getItem('CheckemailPopup');
    if(checkemailpopup !== 'Shown'){
        $('.checkemail').css('display', 'flex');
        $('.checkemail-block').fadeIn().css('display', 'flex');
        $('.checkemail-sent').css('display', 'block');
        $('.checkemail-complete').css('display', 'none');
        localStorage.setItem('CheckemailPopup', 'Shown');
        function checkemailConfirmClose(){
            $('.checkemail-block').css('display', 'none');
            $('.checkemail').fadeOut();
        }
        $('.checkemail .login_popup-close').click(function(){
            checkemailConfirmClose();
        });
        setTimeout(checkemailConfirmClose, 3000);
    }
}

$('#pmpro_account-change-password').click(function(){
    let userEmail = $('.account-table_email .user-email').text();
    $('.user_login-lostpass').val(userEmail);
    $('.popup__lostpass-btn').click();
});

if(login_error.indexOf('resetpass=complete') > 0){

    var resetpassPopup = localStorage.getItem('ResetpassPopup');

    if(resetpassPopup !== 'Shown'){
        $('.checkemail').css('display', 'flex');
        $('.checkemail-block').fadeIn().css('display', 'flex');
        $('.checkemail-sent').css('display', 'none');
        $('.checkemail-complete').css('display', 'block');
        localStorage.setItem('ResetpassPopup', 'Shown');
    }

    function checkemailConfirmClose(){
        $('.checkemail-block').css('display', 'none');
        $('.checkemail').fadeOut();
    };

    $('.checkemail .login_popup-close').click(function(){
        checkemailConfirmClose();
    });

    setTimeout(checkemailConfirmClose, 3000);
}

$('html').click(function() {
    $('.language_menu>li>ul').hide();
    $('.language_menu>li>ul, .language_menu>li').removeClass('active');
    if(window.matchMedia('(max-width: 970px)').matches){
        $('.header_links').hide();
        $('#burger').removeClass('opened');
        $('#burger span.middle-bar').css('display','block');
        $('main').removeClass('menu_active');
        enableScroll();
    }
});

$('.home_first-search input, .search input').focus(function(){
    $('.home_first-search, .search').css('box-shadow', '0 1px 5px 0 rgba(24, 24, 24, 0.2), 0 3px 4px 0 rgba(24, 24, 24, 0.12), 0 2px 4px 0 rgba(24, 24, 24, 0.14)');
}).focusout(function(){
    $('.home_first-search, .search').css('box-shadow', '0 1px 3px 0 rgba(24, 24, 24, 0.2), 0 2px 2px 0 rgba(24, 24, 24, 0.12), 0 0 2px 0 rgba(24, 24, 24, 0.14)');
});

$('.language_menu').click(function(event){
    event.stopPropagation();
    if($('.language_menu>li').hasClass('active')){
        $('.language_menu .sub-menu').hide();
        $('.language_menu>li').removeClass('active');
    } else{
        $('.language_menu .sub-menu').show();
        $('.language_menu>li').addClass('active');
    }
});

$('#burger').click(function(event){
    event.stopPropagation();
    if($('.header_links').is(':visible')){
        $('.header_links').hide();
        $('#burger').removeClass('opened');
        $('#burger span.middle-bar').css('display','block');
        $('.header_btn-lang').css('top','-2px');
        $('main').removeClass('menu_active');
        $('.header').css('position','relative');
    } else{
        $('.header').css({
            'position':'fixed',
            'top':'0'
        });
        $('.header_links').toggle(300).css('display','flex');
        $('#burger').addClass('opened');
        $('#burger span.middle-bar').css('display','none');
        $('.header_btn-lang').css('top','-14px');
        $('main').addClass('menu_active');
    }
});

$(window).on('resize', function(){
    let win = $(this);
    if (win.width() >= 971) {
        $('.header_links').css('display','flex');
    } else{
        $('.header_links').css('display','none');
    }
    if(win.width() <= 1564){
        $('.monitoring__nav').removeClass('active');
    }
});

$('.owl-carousel.owl-theme').owlCarousel({
    loop:true,
    nav:true,
    dots: true,
    responsive:{
        0:{
            items:1
        },
        1000:{
            items:1
        }
    }
});

$('.owl-next, .owl-prev').html('<img src="/images/arrow_down.svg" alt="arrow" />');

if (window.matchMedia('(max-width: 600px)').matches) {
    $('.home_about .wrapper').owlCarousel({
        loop: true,
        nav: false,
        dots: true,
        responsive:{
            0:{
                items:1
            },
            1000:{
                items:1
            }
        }
    })
}

$(".toggle-accordion").on("click", function() {
    var accordionId = $(this).attr("accordion-id"),
        numPanelOpen = $(accordionId + ' .collapse.in').length;

    $(this).toggleClass("active");

    if (numPanelOpen == 0) {
        openAllPanels(accordionId);
    } else {
        closeAllPanels(accordionId);
    }
});

openAllPanels = function(aId) {
    $(aId + ' .panel-collapse:not(".in")').collapse('show');
};
closeAllPanels = function(aId) {
    console.log("setAllPanelclose");
    $(aId + ' .panel-collapse.in').collapse('hide');
};

$(".scrollto").click(function() {
    let elementClick = $(this).attr("id"),
        destination = $(elementClick).offset().top;
    if($(this).is( ":button" )){
        destination = destination-10;
    }
    jQuery("html:not(:animated),body:not(:animated)").animate({
        scrollTop: destination
    }, 800);
    return false;
});

$('.descriptions_tabs span').click(function(){

    let description_id = $(this).attr('id');

    $('.descriptions_tabs span').removeClass('active');
    $(this).addClass('active');

    $('.descriptions_content .descriptions_content-block').css('display','none');

    $('.descriptions_content').find('#'+description_id).fadeIn().css('display','flex');

});

$('#order-form_btn').click(function(){

    var technical = $('#technical_input').val();

    if(technical !== ''){
        $('.siteUrl').val(technical);
    }

    $('.popup_order').css('display', 'flex');
    $('.popup_order .landing_form-form').fadeIn(300);
    disableScroll();

    $('.popup_order-close').click(function(){
        $('.popup_order').fadeOut(200);
        $('.popup_order .landing_form-form').css('display', 'none');
        enableScroll();
    });

});

$('form input[type=email], form input[type=text], form textarea').focus(function(){
    $(this).parents('fieldset').find('label').css({
        'transform': 'translate3d(0, -14px, 0)',
        'font-size': '9px',
        'top': '15px',
        'color': '#495c6a',
        'z-index': '10',
    })
}).focusout(function(){
    $(this).parents('fieldset').find('label').css({
        'transform': 'translate3d(0, 0, 0)',
        'font-size': '14px',
        'top': '0',
        'color': 'transparent',
        'z-index': '-1',
    })
});

$('.login_popup-block form input[type=password]').focusin(function(){
    $(this).css('padding-top', '7px');
    $(this).parent('p').find('label').css({
        'transform': 'translate3d(0, -14px, 0)',
        'top': '12px',
        'color': '#495c6a',
        'z-index': '10',
    })
}).focusout(function(){
    $(this).css('padding-top', '0');
    $(this).parent('p').find('label').css({
        'transform': 'translate3d(0, 0, 0)',
        'top': '0',
        'color': 'transparent',
        'z-index': '-1',
    })
});


$(document).ready(function () {

    function focusing(){
        $( ".sitechecker_crawl" ).focusin(function() {
            $('.typewrite').css('display','none');
        }).focusout(function () {
            if($('#sitechecker_input').val() == ''){
                $('.typewrite').css('display','block');
            }else{
                $('.typewrite').css('display','none');
            }
        });
    }
    function keyup(){
        $("#sitechecker_input").keyup(function(event) {
            let socialCrawler = localStorage.getItem('socialCrawler'),
                socialCrawlerRedirect = localStorage.getItem('socialCrawlerRedirect');
            $('.home_first-search').removeClass('no_valid');
            if (event.keyCode === 13) {
                let apiPath = "/wp-admin/admin-ajax.php";
                let url = $('#sitechecker_input').val().replace(/\s/g, "");
                if($('#sitechecker_reg').length){
                    let registr = $('#sitechecker_reg');
                    registr.find('span').text('Validating');
                    registr.find('.fa').css('display', 'inline-block');
                    $.ajax({
                        url: apiPath,
                        type: "POST",
                        dataType: "JSON",
                        async: true,
                        data: {
                            action: 'crawler_validate_domain_request',
                            url: url
                        },
                        success: function (data) {
                            if(data.status === false){
                                $('.home_first-search').addClass('no_valid');
                                registr.find('span').text('Start');
                                registr.find('.fa').css('display', 'none');
                            } else{
                                let crawlUrl = '/crawl-report?url='+data.valid+'&create';
                                if(registr.attr('name') === 'monitoringEnable'){
                                    crawlUrl = '/crawl-report?url='+data.valid+'&create&monitoring_enabled=1';
                                }

                                $('.theChampLoginSvg').click(function () {
                                    socialCrawler = localStorage.setItem('socialCrawler', 'socialCrawler');
                                    socialCrawlerRedirect = localStorage.setItem('socialCrawlerRedirect', crawlUrl);
                                });
                                localStorage.setItem('create', 'Shown');
                                $('#sitechecker_input').val(data.valid);
                                registr.find('span').text('Validate');
                                registr.find('.fa').css('display', 'none');
                                $('.tml-submit-wrap input[name="redirect_to"]').val(crawlUrl);
                                $('.signup').click();

                            }
                        }
                    });
                } else if($('#sitechecker_crawl').length){
                    let btn = $('#sitechecker_crawl');
                    btn.find('span').text('Validating');
                    btn.find('.fa').css('display', 'inline-block');
                    $.ajax({
                        url: apiPath,
                        type: "POST",
                        dataType: "JSON",
                        async: true,
                        data: {
                            action: 'crawler_validate_domain_request',
                            url: url
                        },
                        success: function (data) {
                            if(data.status === false){
                                $('.home_first-search').addClass('no_valid');
                                btn.find('span').text('Start');
                                btn.find('.fa').css('display', 'none');
                            } else{
                                localStorage.setItem('create', 'Shown');
                                $('#sitechecker_input').val(data.valid);
                                btn.find('span').text('Validating');
                                btn.find('.fa').css('display', 'inline-block');
                                if(btn.attr('name') === 'monitoringEnable'){
                                    window.location.href = "/crawl-report?url="+data.valid+"&create&monitoring_enabled=1";
                                }else{
                                    window.location.href = "/crawl-report?url="+data.valid+"&create";
                                }

                            }
                        }
                    });
                } else {
                    let language = $('html')[0].lang;
                    language = '/'+language+'/';
                    if(language == '/en/'){
                        language = '/';
                    }
                    let link = window.location.protocol + '//' + window.location.hostname + language + 'seo-report/' + url;
                    if(url == ''){
                        $('.home_first-search').addClass('no_valid');
                    } else{
                        window.location.href = link;
                    }

                }
            }
        });
    }
    setTimeout(focusing,1000);
    setTimeout(keyup,1000);

    let getUrlParameter = function getUrlParameter(sParam) {
        let sPageURL = decodeURIComponent(window.location.search.substring(1));
        if(sPageURL){
            let sURLVariables = sPageURL.split('&'),
                sParameterName,
                i;

            for (i = 0; i < sURLVariables.length; i++) {
                sParameterName = sURLVariables[i].split('=');

                if (sParameterName[0] === sParam) {
                    return sParameterName[1] === undefined ? true : sParameterName[1];
                }
            }
        }
    };

    let planID = getUrlParameter('plan');
    let planRegistration = getUrlParameter('registration');
    let cookiesPlans = localStorage.getItem('cookiesPlans');
    if(planID){
        if(!planRegistration){
            $('.paypal_redirect p.title').text('You are logged in now');
        }
        function redirection(){
            $('.header, .footer, .levels_main').css('display','none');
            $('.paypal_redirect').css('display','flex');
            $('.pmpro_btn-submit-checkout[name="'+planID+'"]').click();
        }
        if(cookiesPlans !== 'Shown'){
            setTimeout(redirection, 2000);
            cookiesPlans = localStorage.setItem('cookiesPlans', 'Shown');
        }
    }

    let socialPaypalLogin = localStorage.getItem('socialPaypalLogin'),
        socialPaypalRegistration = localStorage.getItem('socialPaypalRegistration'),
        socialPlan = localStorage.getItem('socialPlan');

    function redirectionSocial(){
        $('.header, .footer, .levels_main').css('display','none');
        $('.paypal_redirect').css('display','flex');
        $('.pmpro_btn-submit-checkout[name="'+socialPlan+'"]').click();
    }
    if(socialPaypalRegistration == 'signupPaypal'){
        setTimeout(redirectionSocial, 2000);
        socialPaypalRegistration = localStorage.setItem('socialPaypalRegistration', 'false');
    } else if(socialPaypalLogin == 'loginPaypal'){
        $('.paypal_redirect p.title').text('You are logged in now');
        setTimeout(redirectionSocial, 2000);
        socialPaypalLogin = localStorage.setItem('socialPaypalLogin', 'false');
    }

    $('#logout').click(function () {
        cookiesPlans = localStorage.setItem('cookiesPlans', 'false');
    });

    $('#level_checkout input').prop('checked',false);

    if(login_error.indexOf('register_popup') > 0){
        $('.signup').click();
    }

    if(login_error.indexOf('login_popup') > 0){
        $('.signin').click();
    }

    $('#sitechecker_check-landing').click(function(){
        $('.home_first-search').removeClass('no_valid');
        let language = $('html')[0].lang;
        language = '/'+language+'/';
        if(language == '/en/'){
            language = '/';
        }

        let url = $('#sitechecker_input').val(),
            link = window.location.protocol + '//' + window.location.hostname + language + 'seo-report/' + url;
        if(url == ''){
            $('.home_first-search').addClass('no_valid');
        } else{
            window.location.href = link;
        }

    });


    $('.code').click(function(){

        var current = $(this);
        var code = current.find('code');
        current.addClass('active');

        var removeActive = function(){
            current.removeClass('active');
        };

        setTimeout(removeActive, 2000);

        copyToClipboard(code);

    });

    if (window.matchMedia('(min-width: 800px)').matches) {

        $('.home_description-img').click(function(){

            if($(this).hasClass('display')){
                $(this).removeClass('display');
                enableScroll();
            } else{
                $(this).find('.description-img').css('display', 'none');
                $(this).addClass('display').find('.description-img').fadeIn();
                disableScroll();
            }

        });
    };

    let str = $('.header_btn').attr('name');
    var ret = '';
    if(str){
        ret = str.split(" ");
    }

    if(ret.length == 10){
        $('.language_menu>li').addClass('emptyMenu').click(function(event){event.stopPropagation(); return false;});
    }

    $.each(ret, function( index, value ) {
        switch(value) {
            case 'ru':
                $('.language_menu>li>ul li a[title=ru]').parent().remove();
                break;
            case 'de':
                $('.language_menu>li>ul li a[title=ge]').parent().remove();
                break;
            case 'fr':
                $('.language_menu>li>ul li a[title=fr]').parent().remove();
                break;
            case 'en':
                $('.language_menu>li>ul li a[title=en]').parent().remove();
                break;
            case 'nl':
                $('.language_menu>li>ul li a[title=nl]').parent().remove();
                break;
            case 'it':
                $('.language_menu>li>ul li a[title=it]').parent().remove();
                break;
            case 'es':
                $('.language_menu>li>ul li a[title=es]').parent().remove();
                break;
            case 'no':
                $('.language_menu>li>ul li a[title=no]').parent().remove();
                break;
            case 'pt':
                $('.language_menu>li>ul li a[title=pt]').parent().remove();
                break;
            case 'sv':
                $('.language_menu>li>ul li a[title=sv]').parent().remove();
                break;
        }
    });

});

//resetpass btn in page
$('#resetpassform').on('submit', function() {

    var user_pass = $('.password-input-wrapper input[name=pass1]').val();
    localStorage.setItem('ResetpassPopup', 'false');
    if(user_pass == ''){
        $('.login__popup-error').fadeIn().html(pass__empty);
        return false;
    } else if (user_pass.indexOf(' ') >= 0){
        $('.login__popup-error').fadeIn().html(pass__spaces);
        return false;
    }
});

function preventDefault(e) {
    e = e || window.event;
    if (e.preventDefault)
        e.preventDefault();
    e.returnValue = false;
}

function preventDefaultForScrollKeys(e) {
    if (keys[e.keyCode]) {
        preventDefault(e);
        return false;
    }
}

function disableScroll() {
    if (window.addEventListener) // older FF
        window.addEventListener('DOMMouseScroll', preventDefault, false);
    window.onwheel = preventDefault; // modern standard
    window.onmousewheel = document.onmousewheel = preventDefault; // older browsers, IE
    window.ontouchmove  = preventDefault; // mobile
    document.onkeydown  = preventDefaultForScrollKeys;
}

function enableScroll() {
    if (window.removeEventListener)
        window.removeEventListener('DOMMouseScroll', preventDefault, false);
    window.onmousewheel = document.onmousewheel = null;
    window.onwheel = null;
    window.ontouchmove = null;
    document.onkeydown = null;
}

var logout_clicks = 0;

$('.logout_url').click(function(event){
    logout_clicks++;
    if(logout_clicks > 1){
        event.preventDefault();
    }
});

$('.domain_statistic-href').click(function(){
    window.location = $(this).attr('href');
    return false;
});

$('.external-link').click(function(e){
    e.stopPropagation();
    window.open($(this).attr('href'), '_blank');
    return false;
});

const crawlerNotificationsToggle = $('#crawl_report-notification');

crawlerNotificationsToggle.change(function () {
    $('#loader-crawler').css('opacity', '1');
    const apiPath = "/wp-admin/admin-ajax.php";
    crawlerNotificationsToggle.attr("disabled", true);
    function finishCrawlerNotificationsToggle(data) {
        console.log(data);
        $('#loader-crawler').css('opacity', '0');
        crawlerNotificationsToggle.removeAttr("disabled");
    }
    $.ajax({
        url: apiPath,
        type: "POST",
        dataType: "JSON",
        data: {
            action: 'crawler_notification_toggle'
        },
        success: finishCrawlerNotificationsToggle,
        error: finishCrawlerNotificationsToggle
    });
});

var theLanguage, location;

$('.language_menu .sub-menu li').each(function(){
    var link = $(this).find('a').attr('href');
    $(this).click(function(){
        if($(this).find('a').attr('title') !== 'en') {
            document.location.href = link;
        } else{
            theLanguage = $('html').attr('lang');
            location = window.location.href.replace('/'+theLanguage+'/', '/en/');
            document.location.href = location;
        }
    })
});

$('.language_menu .sub-menu li a[title=en]').click(function(event){
    event.preventDefault();
    theLanguage = $('html').attr('lang');
    location = window.location.href.replace('/'+theLanguage+'/', '/en/');
    window.location.href = location;
});

$('.en_class').click(function (e) {
    e.preventDefault();
    let url = $(this).attr('href');
    url = '/en/'+url;
    window.location.href = url;
});

$('[data-toggle="tooltip"]').tooltip();

$('.triangle').click(function () {
    if($(this).parents('th').hasClass('sorting_asc')){
        $(this).css({
            'border-color': 'transparent transparent #0286bf transparent',
            'border-width': '0 5px 5px 5px'
        });
    } else if($(this).parents('th').hasClass('sorting_desc')){
        $(this).css({
            'border-color': '#0286bf transparent transparent transparent',
            'border-width': '5px 5px 0 5px'
        });
    }
});

$('.searching_url .fa-search').click(function () {
    $(this).css('display', 'none');
    $('.pageTable_filter').css('display', 'flex');
});

$('.pageTable_filter .fa-close').click(function () {
    $('.pageTable_filter').css('display', 'none');
    $('.searching_url .fa-search').css('display', 'block');
});

$('#sitechecker_crawl').click(function () {
    let apiPath = "/wp-admin/admin-ajax.php",
        url = $('#sitechecker_input').val().replace(/\s/g, ""),
        crawl = $(this);
    $('.home_first-search').removeClass('no_valid');
    crawl.find('span').text('Validating');
    crawl.find('.fa').css('display', 'inline-block');
    $.ajax({
        url: apiPath,
        type: "POST",
        dataType: "JSON",
        async: true,
        data: {
            action: 'crawler_validate_domain_request',
            url: url
        },
        success: function (data) {
            if(data.status === false){
                $('.home_first-search').addClass('no_valid');
                crawl.find('span').text('Start');
                crawl.find('.fa').css('display', 'none');
            } else{
                localStorage.setItem('create', 'Shown');
                $('#sitechecker_input').val(data.valid);
                crawl.find('span').text('Validating');
                crawl.find('.fa').css('display', 'inline-block');
                if(crawl.attr('name') === 'monitoringEnable'){
                    window.location.href = "/crawl-report?url="+data.valid+"&create&monitoring_enabled=1";
                }else{
                    window.location.href = "/crawl-report?url="+data.valid+"&create";
                }
            }
        }
    });
});

$('.redirect_plans').click(function (e) {
    e.preventDefault();
    window.location.href = "/plans/";
});

$('.show_alert').click(function (e) {
    e.preventDefault();
    $('.upgrade_plans').fadeIn();
});

$('.login__popup-close').click(function () {
    enableScroll();
    $('body').removeClass('popup__open');
    $('.header__popup, .login__popup-block').fadeOut(200);
});

$(".crawler-domain-urls, .crawler-domain-url").on('click', function (e) {
    var domainLink = $(this).parent('tr');
    if(!$(domainLink).hasClass('update_progress')){
        document.location.href = domainLink.find('.crawler-domain-url').attr('href');
    }
});

if ($('.btn_gradient').length){
    $('.btn_gradient').mousemove(function(e) {
        const x = e.pageX - e.target.offsetLeft;
        const y = e.pageY - e.target.offsetTop;
        e.target.style.setProperty('--x', `${ x }px`);
        e.target.style.setProperty('--y', `${ y }px`)
    });
}

$('.pages_more-show').click(function () {
    if($(this).hasClass('more')){
        $(this).removeClass('more');
        $(this).find('span').text('Show more');
        $(this).find('i').removeClass('fa-chevron-up');
        $(this).find('i').addClass('fa-chevron-down');
        $('.pages_home .pages_more .pages_more-wrap .block:nth-child(n+4)').hide();
    } else{
        $(this).addClass('more');
        $(this).find('span').text('Show less');
        $(this).find('i').removeClass('fa-chevron-down');
        $(this).find('i').addClass('fa-chevron-up');
        $('.pages_home .pages_more .pages_more-wrap .block').show();
    }
});

$('.pages_watcher-stars').text($('.post-ratings strong:first-of-type').text());

$('.pages_more-wrap .block').click(function() {
    let link = $(this).find('a').attr('href');
    window.location.href = link;
});

$( "#level_checkout input" ).change(function() {
    if($('#level_checkout input').is(":checked")){
        function rotation(elem){
            $(elem).addClass('rotate');
        }
        rotation($('.levels_block-side:first-of-type .card'));
        setTimeout(rotation, 50, $('.levels_block-side:nth-child(2) .card'));
        setTimeout(rotation, 100, $('.levels_block-side:nth-child(3) .card'));
    } else{
        function rotation_remove(elem){
            $(elem).removeClass('rotate');
        }
        rotation_remove($('.levels_block-side:first-of-type .card'));
        setTimeout(rotation_remove, 50, $('.levels_block-side:nth-child(2) .card'));
        setTimeout(rotation_remove, 100, $('.levels_block-side:nth-child(3) .card'));
    }
});

$('.payment-name_block').click(function () {
    let levelName = $(this).attr('name');
    $('.level_inner').css('display','none');
    $('.'+levelName+'Level').css('display','block');
    $('.payment-name_block').css('box-shadow','none');
    $('.payment-name_block[name="'+levelName+'"]').css('box-shadow','inset 0.5px 1px 2px 0 rgba(0, 0, 0, 0.3)');
});

let pdfBanner = localStorage.getItem('pdfBanner');

if(pdfBanner == 0){
    $('.pdf_banner').css('display','flex');
    localStorage.setItem('pdfBanner', +pdfBanner+1);
    dataLayer.push({ 'event': 'autoEvent', 'eventCategory': 'PDF limited popup shown', 'eventAction': 'shown' });
}

$('.pdf_banner-close').click(function () {
    let el = $('.pdf_banner');
    let curHeight = el.height();
    el.height(curHeight).animate({height: 0}, 200);
    localStorage.setItem('pdfBanner', +pdfBanner+1);
});

$('.all_features p').click(function () {
    let el = $('.all_features ul'),
        fullHeight = el.css('height', 'auto').height(),
        curHeight = el.height();

    if($(this).hasClass('open')){
        $(this).removeClass('open');
        el.height(fullHeight).animate({height: 0}, 200);
    } else{
        $(this).addClass('open');
        el.height(0).animate({height: fullHeight}, 200);
    }
});

$('.save__pdf-link').click(function () {
    if(pdfBanner > 0 || pdfBanner == 0){
        localStorage.setItem('pdfBanner', +pdfBanner+1);
    } else{
        localStorage.setItem('pdfBanner', 0);
    }
});

$('.monitoring_tabs p').click(function () {
    $('.monitoring_tabs p').removeClass('active');
    let active_tab = $(this).attr('class');
    $('.monitoring_tab').css('display','none');
    $('.'+active_tab+'-tab').fadeIn();
    $(this).addClass('active');
});

$('body').on('click', '.event-box__header', function () {
    $(this).find('.dropdown-icon--greyscale').removeClass('shown');

    let el = $(this).next('.event-box__hidden');
    let curHeight = el.height();
    if(el.hasClass('open')){
        $(this).removeClass('header__open');
        el.removeClass('open');
        let autoHeight = el.css('height', 'auto').height();
        el.height(curHeight).animate({height: 0}, 300);
        if(el.hasClass('hidden_child')){
            let parent = el.parent().parent().parent();
            let parentHeight = parent.height();
            parent.animate({height: (parentHeight - autoHeight)}, 300);
        }
        if(el.find('.hidden_child').length){
            el.find('.event-box__header').removeClass('header__open').find('.dropdown-icon--greyscale').removeClass('shown');
            el.find('.hidden_child').removeClass('open').animate({height: 0}, 300);
        }
    } else{
        $(this).addClass('header__open');
        $(this).find('.dropdown-icon--greyscale').addClass('shown');
        el.addClass('open');
        let autoHeight = el.css('height', 'auto').height();
        el.height(curHeight).animate({height: autoHeight}, 300);
        if(el.hasClass('hidden_child')){
            let parent = el.parent().parent().parent();
            let parentHeight = parent.height();
            parent.animate({height: (parentHeight + autoHeight)}, 300);
        }
    }
});

$('.domains__tab button, .domains__info-text button').click(function () {
    $('.domains__tab button').removeClass('active');
    let dataID = $(this).attr('data-id');
    if(dataID){
        $('.'+dataID).addClass('active');
    } else{
        $(this).addClass('active');
    }

    if($(this).hasClass('anchor-tab') || dataID === 'anchor-tab'){
        $('.anchors_export').fadeIn(200);
    }else{
        $('.anchors_export').fadeOut(200);
    }
});

$('.fix').click(function () {
    let block =  $(this);
    if(block.hasClass('open')){
        block.removeClass('open');
        if(block.parent().hasClass('issues')){
            block.prev().find('li:nth-child(n+6)').hide(300);
            block.find('span').text('Show more');
        } else if(block.parent().hasClass('anchors')){
            block.prev().find('tr:nth-child(n+6)').hide(300);
            if(block.prev().hasClass('optimization_data')){
                block.find('span').text('Show');
            } else{
                block.find('span').text('Show more');
            }
        }
    } else{
        block.addClass('open');
        if(block.parent().hasClass('issues')){
            block.prev().find('li:nth-child(n+6)').show(300);
            block.find('span').text('Show less');
        } else if(block.parent().hasClass('anchors')){
            block.prev().find('tr:nth-child(n+6)').show(300);
            if(block.prev().hasClass('optimization_data')){
                block.find('span').text('Hide');
            } else{
                block.find('span').text('Show less');
            }
        }
    }
});

$('body').on('click', '.copy_url', function () {
    console.log('copied!');
    let $temp = $("<input>"),
        text = $('#share_url').val();

    $("body").append($temp);
    $temp.val(text).select();
    document.execCommand("copy");
    $temp.remove();
    $(this).text('Copied!');

    function changeText(){
        $('.copy_url').text('Copy');
    }
    setTimeout(changeText, 2000);
});

$('#contact__form').on('submit', function (e) {
    e.preventDefault();
    $('.contact__form-error').css('display','none');
    let name = $('#contact__form-name').val(),
        email = $('#contact__form-email').val(),
        message = $('#contact__form-mess').val();
    if(name === ''){
        $('.contact__form-error[name="name"]').text('Please, enter your name').fadeIn();
    }else if(email === '' || email.indexOf('.') === -1){
        $('.contact__form-error[name="email"]').text('Please, enter valid email').fadeIn();
    }else{
        $('#contact__form-btn').val('Sending');
        $.ajax({
            url: "/wp-admin/admin-ajax.php",
            type: "POST",
            dataType: "JSON",
            data: {
                email: email,
                name: name,
                message: message,
                action: 'contact_form'
            },
            success: function (data) {
                $('#contact__form input, #contact__form textarea').val('');
                $('#contact__form-btn').val('Send');
                $('.popup_order').css('display','none');
                let closing_popup = function(){
                    $('.popup_sent').fadeOut();
                    $('.popup_sent-block').css('display','none');
                    enableScroll();
                };
                $('.popup_sent').css('display', 'flex');
                $('.popup_sent-block').fadeIn().css('display', 'flex');
                disableScroll();
                setTimeout(closing_popup, 3000);
                $('.popup_sent-close').click(function(){
                    $('.popup_sent').fadeOut();
                    $('.popup_sent-block').css('display','none');
                    enableScroll();
                });
            }
        });
    }
});
$('.monitoring_navigation').click(function () {
    let nav = $('.monitoring__nav');
    if(nav.hasClass('active')){
        nav.removeClass('active');
    } else{
        nav.addClass('active');
    }
});
$('body').on('click','#popup__audit', function (e) {
    e.preventDefault();
    let url = $('#sitechecker_input').val();
    $('.theChampLoginSvg').click(function () {
        socialCrawler = localStorage.setItem('socialCrawler', 'socialCrawler');
        socialCrawlerRedirect = localStorage.setItem('socialCrawlerRedirect', '/crawl-report?url='+url+'&create');
    });
    $('.tml-submit-wrap input[name="redirect_to"]').val('/crawl-report?url='+url+'&create');
    $('.signup').click();
});

$(".user-bell").mouseenter(function() {
    let list = $('.user-bell_list'),
        bell = $(this);
    if(list.hasClass('active')){
        list.removeClass('active')
    }else{
        let newsCount = $(this).attr('name');
        list.addClass('active');
        if(bell.hasClass('request')){
            bell.removeClass('request');
            $.ajax({
                url: "/wp-admin/admin-ajax.php",
                type: "POST",
                dataType: "JSON",
                data: {
                    newsCount: newsCount,
                    action: 'update_news'
                },
                success: function (data) {
                    console.log(data);
                }
            });
        }
    }
}).mouseleave(function () {
    let list = $('.user-bell_list');
    list.removeClass('active');
    $('.bellPosts').empty();
});

$('.checkUserDesign').change(function () {
    let checking = $(this).is(':checked');
    if(checking === true){
        checking = 1;
    }else if (checking === false){
        checking = 0;
    }
    document.cookie="design_new="+checking+";path=/";
    location.reload();
});
$('.loginCheckUserDesign').change(function () {
    let checking = $(this).is(':checked'),
        apiPath = "/wp-admin/admin-ajax.php";
    $.ajax({
        url: apiPath,
        type: "POST",
        dataType: "JSON",
        data: {
            action: 'new_design',
            checking: checking
        },
        success: function (data) {
            console.log(data);
            location.reload();
        }
    });
});
$('.checkNewDesign').change(function () {
    let checking = $(this).is(':checked'),
        apiPath = "/wp-admin/admin-ajax.php";
    console.log(checking);
    $.ajax({
        url: apiPath,
        type: "POST",
        dataType: "JSON",
        data: {
            action: 'new_design',
            checking: checking
        },
        success: function (data) {
            console.log(data);
            window.location.href = window.location.href;
        }
    });
});

function sent_Popup() {
    if ($('.wp_crm_error_messages').text() == '') {
        $('.popup_sent').css('display', 'flex');
        $('.popup_sent-block').fadeIn().css('display', 'flex');
        disableScroll();

        var closing_popup = function () {
            $('.popup_sent').fadeOut();
            $('.popup_sent-block').css('display', 'none');
            enableScroll();
        };

        setTimeout(closing_popup, 3000);

        $('.popup_sent-close').click(function () {
            $('.popup_sent').fadeOut();
            $('.popup_sent-block').css('display', 'none');
            enableScroll();
        })
    }
    ;
};