let errorStorage      = 0,
    warningStorage    = 0,
    noticeStorage     = 0,
    passedStorage     = 18,
    evaluationStorage = 100,
    requestsStorage   = [],
    validationsArray = [],
    validationsName = [
        'parameterInformation',
        'parameterTitle',
        'parameterDescription',
        'parameterH1H6',
        'parameterContent',
        'parameterCanonicallink',
        'parameterAlternatelink',
        'parameterPagination',
        'parameterIndexation',
        'parameterVulnerability',
        'parameterBot',
        'parameterExternallinks',
        'parameterSubdomainlinks',
        'parameterInternallinks',
        'parameterMobileSpeed',
        'parameterDesktopSpeed',
        'parameterFavicon',
        'parameterImages'
    ],
    siteUrl;

const debugMode = false;
const isMtfIframe = window.location.pathname.slice(0,4) === '/mtf';
const host = document.location.host || document.location.hostname;

const loader         = $('.cprogress__loader'),
    circle           = $('.cprogress__circle'),
    percents         = $('.cprogress__percents').children('span'),
    bar              = $('.cprogress__bar'),
    indexBlock       = $('.cprogress-index').find('span'),
    sizeBlock        = $('.cprogress-size').find('span'),
    statusBlock      = $('.cprogress-status').find('span');


let circleAnimation = new ProgressBar.Circle('#progressbar', {
    color: '#9f9f9f',
    strokeWidth: 8,
    trailWidth: 8,
    easing: 'easeInOut',
    duration: 1400,
    text: {
        autoStyleContainer: false
    },
    from: { color: '#ff4b55', width: 8 },
    to: { color: '#ff4b55', width: 8 },
    step: function(state, circle) {
        circle.path.setAttribute('stroke', state.color);
        circle.path.setAttribute('stroke-width', state.width);
        let value = Math.round(circle.value() * 100);
        circle.setText(value+ '%');

    }
});
circleAnimation.text.style.fontFamily = '"Poppins", sans-serif';
circleAnimation.text.style.fontSize = '40px';
circleAnimation.text.style.top = '45%';
circleAnimation.animate(0);

const selectedValidations = [1, 2, 3, 4, 5];

const validations = {
    1: {
        name: trans['validations']['names'][1],
        group: {
            'n-0': trans['validations'][0],
            'n-1': trans['validations'][1],
            'n-2': trans['validations'][2],
            'n-3': trans['validations'][3],
            'n-4': trans['validations'][4]
        }
    },
    2: {
        name: trans['validations']['names'][2],
        group: {
            'n-16': trans['validations'][16],
            'n-17': trans['validations'][17]
        }
    },
    3: {
        name: trans['validations']['names'][3],
        group: {
            'n-5': trans['validations'][5],
            'n-6': trans['validations'][6],
            'n-7': trans['validations'][7],
            'n-8': trans['validations'][8],
            'n-9': trans['validations'][9],
            'n-10': trans['validations'][10]
        }
    },
    4: {
        name: trans['validations']['names'][4],
        group: {
            'n-11': trans['validations'][11],
            'n-12': trans['validations'][12],
            'n-13': trans['validations'][13]
        }
    },
    5: {
        name: trans['validations']['names'][5],
        group: {
            'n-14': trans['validations'][14],
            'n-15': trans['validations'][15]
        }
    }
};

let header_links = [],
    product_links = [];

$('.header .products li').each(function () {
    let element = $(this).find('a');
    header_links.push(element.attr('href'));
});

product_links[0] = header_links[3];
product_links[1] = header_links[1];
product_links[2] = header_links[4];
product_links[3] = header_links[2];

$('.limit__popup .products li').each(function (index) {
    let element = $(this).find('a');
    element.attr('href',product_links[index]);
});

function getHostName(url) {
    let match = url.match(/:\/\/(www[0-9]?\.)?(.[^/:]+)/i);
    if (match != null && match.length > 2 && typeof match[2] === 'string' && match[2].length > 0) {
        return match[2];
    }
    else {
        return null;
    }
}

let errorsCount = 0,
    warningsCount = 0;

function countAnimation(element, count) {
    if(element === 'score__errors' || element === 'score__warnings'){
        if(element === 'score__errors'){
            errorsCount = count;
        }else if(element === 'score__warnings'){
            warningsCount = count;
        }
        let finalCount = ((errorsCount+warningsCount)*100)/60;
        circleAnimation.animate(finalCount/100);
    }

    $({
        countNum: $('#'+element).text()
    }).animate({
            countNum: count
        },
        {
            duration: 2000,
            easing: 'swing',
            step: function () {
                $('#'+element).text(Math.floor(this.countNum));
            },
            complete: function () {
                $('#'+element).text(this.countNum);
            }
        }
    );
}

let allValidations = 0,
    passedChecks   = 0;

let validationResult = new Map();

$(document).ready(function() {
    allValidations = countValidations();

    $(function(){
        if (getAndCheckQuery()) {
            executeQuery();
        }
    });
});

function getCookie(name) {
    var matches = document.cookie.match(new RegExp(
        "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
    ));
    return matches ? decodeURIComponent(matches[1]) : false;
}

function setCookie(name,value,days) {
    var expires = "";
    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days*24*60*60*1000));
        expires = "; expires=" + date.toUTCString();
    }
    document.cookie = name + "=" + value + expires + "; path=/";
}

function executeQuery() {
    let query = getAndCheckQuery();
    if (!query || selectedValidations.length === 0) {
        return;
    }

    $('.container__results .cloudflare').remove();

    if (!isMtfIframe) {
        changeState('seo-report-demo/' + query);
        $('.tml-submit-wrap input[name="redirect_to"]').val("/tool/create_user_domain/?url="+query);
    }

    const statusUrl = `${document.location.protocol}//api1.${host}/api/v1/status/${query}`;
    let statusRequest = $.ajax({
        url: statusUrl,
        method: 'GET',
        dataType: 'json',
        xhrFields: {
            withCredentials: true
        }
    });

    statusRequest.done(function(response) {
        if (response !== undefined) {
            if ("htmlStatus" in response && response.htmlStatus) {
                statusBlock.html(response.htmlStatus);
            }

            if ("url" in response && response.url && query !== response.url) {
                query = response.url;
                changeState('seo-report-demo/' + query);
            }
        }

        if (response === undefined || response.status === undefined || response.status !== 200 || !response.url) {
            renderAbsolutelyFail("htmlStatus" in response && response.htmlStatus);
            if ("error" in response) {
                $('.report__container').addClass('result__error');
                countAnimation('score__all', 0);
                renderResponsesNavRow('error', 0, 0, response.error, trans['Error'], false);
            }
        } else {
            executeAllValidations(query);
        }

        addValidationResult('status', response);
        logStatus(response)
    });

    statusRequest.fail(function(jqXHR, textStatus) {
        renderAbsolutelyFail();
    });
}

function addValidationResult(num, response) {
    validationResult.set(num, response);
}

function countValidations() {
    let count = 0;

    $.each(selectedValidations, function(key, validationsGroupIndex) {
        $.each(validations[validationsGroupIndex]['group'], function() {
            ++count;
        });
    });

    return count;
}

function logStatus(statusRequest) {
    $.ajax({
        url: '/tool/crawler_api/log_on_page_check/',
        type: 'POST',
        dataType: 'JSON',
        data: statusRequest,
        success: function success(data) {
            console.log(data);
        }
    });
}

function executeAllValidations(query) {

    $.each(selectedValidations, function (key, validationsGroupIndex) {

        $.each(validations[validationsGroupIndex]['group'], function (validationIndex, validationName) {
            validationIndex = Number(validationIndex.split('-')[1]);
            requestsStorage[validationIndex] = $.ajax({
                url: getDomain(validationIndex) + `/api/v1/full/${validationIndex}/${query}`,
                method: 'GET',
                dataType: 'json',
                xhrFields: {
                    withCredentials: true
                },
                crossDomain: true,
                success: function(response){
                    let params = {
                        0: validationsGroupIndex,
                        1: validationIndex,
                        2: query,
                        3: response
                    };

                    ++passedChecks;

                    if (debugMode) {
                        console.log(`check ${passedChecks} of ${allValidations}`);
                    }

                    addValidationResult(validationIndex, response);

                    if (allValidations === passedChecks) {
                        hideLoader();
                        renderResult(params, true);
                    } else {
                        renderResult(params, true);
                    }
                },
                error: function (jqXHR, textStatus) {
                    renderFail(validationIndex, textStatus, allValidations);
                }
            });

        });
    });
}

function renderResult(params, lastIteration = false, miniTool = false) {
    if(miniTool) {
        $('.result').append('<div id="result-' + params[1] + '"><div class="validation__param-body"></div></div>');
    }

    let validationsGroupIndex = params[0],
        validationIndex       = params[1],
        response              = params[3],
        array                 = response.data.checks,
        evaluation            = response.data.evaluation,
        mainBlock = $(`#result-${validationIndex}`),
        resultBlock = mainBlock.find('.validation__param-body');

    updateEvaluation(evaluation);

    $.each(array, function(key, obj) {
        let importance = obj.importance,
            subgroup = obj.description ? obj.description : obj.subgroup,
            title = obj.title,
            query = params[2],
            description = obj.label,
            fix = obj.fix;

            if(!miniTool) {
                if (validationIndex === 8) {
                    renderIndex(evaluation == 100);
                }
                if (validationIndex === 0) {
                    switch (key) {
                        case 2:
                            sizeBlock.html(`<span class="code--${importance}">${title}</span>`);
                            break;
                    }
                }
                renderPageScore(importance, lastIteration);
            }

            if (validationIndex === 0 && key === 0) {
                return;
            }

            if (validationIndex === 0 && key === 4) {
                return;
            }

            if (validationIndex === 0 && key === 3) {
                return;
            }

            if (validationIndex === 16 && key !== 2) {
                return;
            }

            switch (importance) {
                case 'error':
                case 'warning':
                case 'notice':
                    renderResponsesNavRow(importance, key, validationIndex, title, obj.subgroup, obj.description);
            }
    });
}

function renderIndex(indexable) {
    if (indexable) {
        indexBlock
            .addClass('indexable--ok')
            .text(trans['Indexable']);
    } else {
        indexBlock
            .addClass('indexable--error')
            .text(trans['Not indexable']);
    }
}

function updateEvaluation(evaluation) {
    evaluationStorage -= (100 - evaluation);
    if (evaluationStorage < 0) {
        evaluationStorage = 0;
    }
}

function renderPageScore(importance, lastIteration = false) {
    switch (importance) {
        case 'error':
            ++errorStorage;
            break;
        case 'warning':
            ++warningStorage;
            break;
        case 'notice':
            ++noticeStorage;
    }

    countAnimation('score__errors', errorStorage);
    countAnimation('score__warnings', warningStorage);

    if (lastIteration) {
        let score = evaluationStorage;

        if (evaluationStorage < 0 || evaluationStorage === 0) {
            evaluationStorage = 0;
        }
        countAnimation('score__all', score);
    }
}

function renderResponsesNavRow(importance, key, validationIndex, title, subgroup, description) {
    let responsesContainer = $(`.responses--${importance}`),
        responsesBlock = $(`.responses--${importance} .responses--content`),
        message;
    $('.report__response .loading').css('display','none');
    responsesContainer.fadeIn();

    if (title instanceof Array || title instanceof Object) {
        message = subgroup + ': ' + description;
    } else {
        message = subgroup + ': ' + title;
    }

    let renderedBlock = $('<div/>', {
        class: 'responses__row responses__row-demo',
        'data-index': validationIndex,
        html: message
    }).appendTo(responsesBlock);

    renderedBlock.slideDown(300);
}

let login = $('.seo_demo_login'),
    loginBlock = $('.seo_demo-login'),
    loginBlockWidth = loginBlock.outerWidth(),
    loginBlockHeight = loginBlock.outerHeight(),
    loginPosition = login.offset().top;
if (window.matchMedia('(min-width: 769px)').matches) {
    $(window).scroll(function(){
        let resultHeight = $('.report__results-demo').outerHeight();
        if($(window).scrollTop() >= loginPosition && resultHeight>=loginBlockHeight){
            loginBlock.width(loginBlockWidth);
            login.css({
                'position': 'fixed',
                'top': '0'
            });
        }else{
            login.css('position','relative');
        }
    });
}

function hideLoader(status) {
    let endColor = '#d7d7d7',
        score = evaluationStorage;

    if (evaluationStorage < 0 || evaluationStorage === 0) {
        evaluationStorage = 0;
    }
    if(evaluationStorage<33){
        endColor = 'error';
    }else if(evaluationStorage>=33 && evaluationStorage<=65){
        endColor = 'warning';
    }else if(evaluationStorage>65){
        endColor = 'ok';
    }
    $('.score__all-animation').fadeOut();
    function finalScore(){
        if(status === 'fail') {
            score = 0;
        }
        console.log('finalScore: '+score);
        countAnimation('score__all', score);
        $('#score__all').addClass(endColor);
    }
    setTimeout(finalScore,300);

    let validationString = '';
    let clientID = ga.getAll()[0].get('clientId');

    console.log(validationsArray);
    $.each(validationsName, function( i, val ) {
        $.each(validationsArray, function( index, value ) {
            window.dataLayer[validationsName[value]] = 'false';
            validationString = validationString+"v=1&tid=UA-82230026-1&cid="+clientID+"&t=event&ec="+validationsName[value]+"&ea=false&el="+location.href+"\r\n";
        });
        window.dataLayer[validationsName[i]] = 'true';
        validationString = validationString+"v=1&tid=UA-82230026-1&cid="+clientID+"&t=event&ec="+validationsName[i]+"&ea=true&el="+location.href+"\r\n";
    });
    let statusCode = $('#result-0 .statusHtml').text();

    window.dataLayer.push({
        'statusCode': statusCode
    });

    validationString = validationString+"v=1&tid=UA-82230026-1&cid="+clientID+"&t=event&ec=statusCode&ea="+statusCode+"\r\n";
    $.ajax({
        async: true,
        crossDomain: true,
        url: "https://www.google-analytics.com/batch",
        type: "POST",
        data: validationString,
        success: function (data) {}
    });
}

function renderFail(validationIndex, textStatus, allValidations) {
    console.log(`${validationIndex} validation failed: ${textStatus}`);

    validationsArray.push(validationIndex);

    ++passedChecks;

    if (debugMode) {
        console.log(`${passedChecks}/${allValidations}`);
    }

    if (allValidations === passedChecks) {
        hideLoader();
    }
}

function abortRequest() {
    $.each(requestsStorage, function (index, request) {
        request.abort();
    });
}

function getAndCheckQuery() {
    let query = $('#sitechecker_input__value').text();

    if (!query || query.length === 0) {
        return;
    }
    return query;
}

function renderAbsolutelyFail(withoutStatus = false) {
    hideLoader('fail');
    indexBlock.html(`<span class="code--error">${trans['Error']}</span>`);
    sizeBlock.html(`<span class="code--error">${trans['Error']}</span>`);
    if (!withoutStatus) {
        statusBlock.html(`<span class="code--error">${trans['Error']}</span>`);
    }
}

function changeState(newState) {
    if (newState.charAt(0) !== '/') {
        newState = `/${newState}`;
    }

    if (locale && locale !== default_locale) {
        newState = `/${locale}${newState}`;
    }

    window.history.replaceState(window.history.state, '', newState);
}

jQuery.fn.rotate = function(degrees) {
    $(this).css({'transform': `rotate(${degrees}deg)`});
    return $(this);
};

function getDomain(num) {
    let result;
    switch (num) {
        case 0:
        case 1:
        case 2:
        case 3:
        case 4:
        case 5:
            result = 'api1.' + host;
            break;
        case 6:
        case 7:
        case 8:
        case 9:
        case 10:
        case 11:
            result = 'api2.' + host;
            break;
        case 12:
        case 13:
        case 14:
        case 15:
        case 16:
        case 17:
            result = 'api3.' + host;
            break;
        default:
            result = host;
    }

    return document.location.protocol + '//' + result;
}