<?php 
/*
	Template Name: Seo blog
*/
?>
<?php get_header(); ?>

<main>
	<div class="wrapper">
		<div class="blog_slider">
			<div class="owl-carousel owl-theme">
				<?php
	              global $post;
	              $tmp_post = $post;
	              $args = array(
	                'posts_per_page' => 10
	              );
	              $myposts = get_posts($args);

	              foreach ($myposts as $post) {
	                        setup_postdata($post);
	                        ?>
	                        <div class="item" style="background-image: url(<?php echo the_post_thumbnail_url(); ?>)">
						    	<div class="item_data">
						    		<p class="item_tags">
						    			<?php $tags = wp_get_post_tags($post->ID);
	                                      foreach ($tags as $tag) {
	                                          	$tag_link = get_tag_link( $tag->term_id );
	                                        	echo '<a href="'.$tag_link.'">' . $tag->name . '</a>';
	                                      }
	                                  ?>
						    		</p>
						    		<a href="<?php the_permalink(); ?>" class="item_title">
						    			<?php the_title(); ?>
						    		</a>
						    		<p class="item_content">
						    			<?php the_excerpt(); ?>
						    		</p>
						    	</div>
						    </div>
	                        <?php
	                    }
	              $post = $tmp_post;
	            ?>
			</div>
		</div>
		<div class="blog_tags">
			<?php if (have_posts()): while (have_posts()) : the_post(); ?>
				<?php the_content(); ?>
			<?php endwhile; ?>
			<?php else: ?>
			<?php endif; ?>
		</div>
		<div class="blog_container">
			<div class="blog_container-posts">
				<?php query_posts('post_type=post&post_status=publish&posts_per_page=3&paged='. get_query_var('paged')); ?>
				<?php if (have_posts()): while (have_posts()) : the_post(); ?>
                        
                        <div class="blog_post">
							<a href="<?php the_permalink(); ?>" class="blog_post-img" style="background-image: url(<?php echo the_post_thumbnail_url(); ?>)"></a>
							<div class="blog_post-tags">
								<?php $tags = wp_get_post_tags($post->ID);
		                          	foreach ($tags as $tag) {
		                              	$tag_link = get_tag_link( $tag->term_id );
		                            	echo '<a href="'.$tag_link.'">' . $tag->name . '</a>';
		                          	}
		                      	?>
							</div>
							<a href="<?php the_permalink(); ?>" class="blog_post-title"><?php the_title(); ?></a>
							<p class="blog_post-data"><?php the_date(); ?></p>
							<p class="blog_post-text"><?php the_excerpt(); ?></p>
							<a href="<?php the_permalink(); ?>" class="read-more">Read more</a>
						</div>

                <?php endwhile; ?>

                <?php endif; ?>
                <?php get_template_part('pagination'); ?>

			</div>
			<?php get_sidebar(); ?>
		</div>
	</div>
</main>

<?php get_footer(); ?>
