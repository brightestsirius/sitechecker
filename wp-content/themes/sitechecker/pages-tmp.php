<?php
    $pages_array = [
        0 => 'robots-tester',
        1 => '404-error',
        2 => 'what-is-favicon',
        3 => 'what-is-url',
        4 => 'h1-tag',
        5 => 'meta-title',
        6 => 'meta-tag-description',
        7 => 'http-vs-https',
        8 => 'www-vs-non-www',
        9 => 'canonical-url',
        10 => 'internal-links',
        11 => 'alt-tags',
        12 => 'http-status-codes',
        13 => 'pagination',
        14 => 'external-links',
        15 => 'page-size',
        16 => 'google-index'
    ];
    $path_only = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
    $trigger = false;
    foreach ($pages_array as $page){
        if(strpos($path_only, $page)){
            $trigger = true;
        }
    };
    $template_directory_uri = get_template_directory_uri();
    $meta = get_post_meta_all(get_option('page_on_front'));
    $dict = getApiDictionary();
    $tt = function ($key) use ($dict) {
        return $dict[$key] ?? $key;
    };
?>

<script>
    const locale = "<?=qtranxf_getLanguage();?>";
    const default_locale = "<?=qtranxf_getLanguageDefault();?>";
    const trans = JSON.parse('<?=json_encode(getApiDictionary(), JSON_HEX_APOS | JSON_HEX_AMP | JSON_HEX_QUOT);?>'.replace(/\\'/g, "\'"));
    const lang_cookie_name = "qtrans_front_language";
</script>
<?php if (have_posts()): while (have_posts()) : the_post(); ?>
    <main class="home pages_home">
        <div class="pages_breadcrumbs">
            <div class="wrapper">
                <a href="<?=home_url();?>/">Home</a>
                <span>/</span>
                <a href="/knowledge-base/"><?=t($meta['user.header.knowledge.base']);?></a>
                <span>/</span>
                <?php
                $categories = get_the_category();
                $category_name = $categories[0]->cat_name;
                if($category_name){
                    echo '<a href="'.get_category_link($categories[0]->cat_ID).'">'.$category_name.'</a><span>/</span>';
                }
                ?>
                <a><?php echo get_post_meta($post->ID, 'page.h1', true) ?></a>
            </div>
        </div>
        <?php if ($trigger) : ?>
            <div class="page_search pages_search">
                <div class="wrapper">
                    <p id = "minitool-title"></p>
                    <div class="home_first-search" style="max-width: 920px">
                        <input id="minitool_input" type="text" style="width:100%">
                        <button id="minitool_check-landing"><span id="minitool-span"><!--Test--></span></button>
                    </div>
                    <div class="container" id="result-container">
                        <div class="loader" style="margin-top: 0px">
                            <div class="animated-background">
                                <div class="background-masker lvl-0"></div>
                                <div class="background-masker lvl-1-1"></div>
                                <div class="background-masker lvl-1-2"></div>
                                <div class="background-masker lvl-1-3"></div>
                                <div class="background-masker lvl-2"></div>
                                <div class="background-masker lvl-3-1"></div>
                                <div class="background-masker lvl-3-2"></div>
                                <div class="background-masker lvl-4"></div>
                                <div class="background-masker lvl-5-1"></div>
                                <div class="background-masker lvl-5-2"></div>
                                <div class="background-masker lvl-6"></div>
                                <div class="background-masker lvl-7-1"></div>
                                <div class="background-masker lvl-7-2"></div>
                                <div class="background-masker lvl-8"></div>
                            </div>
                        </div>
                        <div class="container__results">
                            <div class="container__result" id="result-template">
                                <div class="result">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php endif; ?>
        <?php if($post->ID === 1063){ ?>
            <div class="speed__test">
                <div class="pages_wrapper">
                    <p class="speed__test-title">Website speed test</p>
                    <form action="" id="speed__test-form">
                        <p class="error invalid"><img src="<?= $template_directory_uri; ?>/out/img_design/error_close.svg" alt="error" title="error"><span>URL is not valid</span></p>
                        <p class="error valid"><img src="<?= $template_directory_uri; ?>/out/img_design/valid__url.svg" alt="valid" title="valid"><span>URL is valid!</span></p>
                        <input type="text" id="speed__test-url" placeholder="Enter URL to test">
                        <button type="submit">Start test</button>
                    </form>
                    <div class="speed__testData">
                        <div class="speed__test-info">
                            <div class="speed__test-average">
                                <div class="title">Page Speed</div>
                                <div class="info">
                                    <div class="info__title">Average</div>
                                    <div class="info__description">
                                        <div>First contentful paint: <span>2.6s</span></div>
                                        <div>DOM content loaded: <span>1.1s</span></div>
                                    </div>
                                </div>
                            </div>
                            <div class="speed__test-distributions">
                                <div class="title">Page Load Distributions</div>
                                <div class="info">
                                    <div class="speed__test-graph">
                                        <div class="graph__block">
                                            <p class="title">FCP</p>
                                            <div class="graph">
                                                <div class="graph__section fast"><span>30%</span><div class="tooltip"></div></div>
                                                <div class="graph__section average"><span>40%</span><div class="tooltip"></div></div>
                                                <div class="graph__section slow"><span>30%</span><div class="tooltip"></div></div>
                                            </div>
                                        </div>
                                        <div class="graph__block">
                                            <p class="title">DCL</p>
                                            <div class="graph">
                                                <div class="graph__labels">
                                                    <span>0%</span>
                                                    <span>25%</span>
                                                    <span>50%</span>
                                                    <span>75%</span>
                                                </div>
                                                <div class="graph__section fast"><span>30%</span><div class="tooltip"></div></div>
                                                <div class="graph__section average"><span>40%</span><div class="tooltip"></div></div>
                                                <div class="graph__section slow"><span>30%</span><div class="tooltip"></div></div>
                                            </div>
                                        </div>
                                        <div class="graph__description">
                                            <div class="graph__description-block">
                                                <span></span>
                                                <p>Fast</p>
                                            </div>
                                            <div class="graph__description-block">
                                                <span></span>
                                                <p>Average</p>
                                            </div>
                                            <div class="graph__description-block">
                                                <span></span>
                                                <p>Slow</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="speed__test-fix">
                            <div class="fix__container">
                                <p class="title">Optimization Suggestions</p>
                                <div class="fix__containers"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php } ?>
        <div class="pages_wrapper">
            <h1><?php echo get_post_meta($post->ID, 'page.h1', true) ?></h1>
            <div class="pages_watcher">
                <img src="<?=$template_directory_uri;?>/out/img/page_watches.svg" alt="page_watches" title="page_watches">
                <span><?php setPostViews(get_the_ID()); echo getPostViews(get_the_ID());?></span>
                <img src="<?=$template_directory_uri;?>/out/img/page_stars.svg" alt="page_stars" title="page_stars">
                <span class="pages_watcher-stars"></span>
            </div>
            <div class="pages_img">
                <img src="<?php echo get_template_directory_uri(); ?>/out/img/img-border.svg" alt="img-border" title="img-border">
                <img src="<?php echo get_the_post_thumbnail_url(); ?>" alt="<?php echo get_post_meta($post->ID, 'page.image.alt', true) ?>" title="<?php echo get_post_meta($post->ID, 'page.image.title', true) ?>" class="page_img-image">
            </div>
            <?php the_content(); ?>
            <?php if(function_exists('the_ratings')) { the_ratings(); } ?>
        </div>
        <div class="page_search pages_search">
            <div class="wrapper">
                <?php $frontpage_id = get_option( 'page_on_front' ); ?>
                <h2><?php echo get_post_meta($frontpage_id, 'main.h1', true) ?></h2>
                <p><?php echo get_post_meta($frontpage_id, 'main.h2', true) ?></p>
                <div class="home_first-search">
                    <input id="sitechecker_input" type="text" placeholder="<?php echo get_post_meta($frontpage_id, 'main.input', true) ?>">
                    <button id="sitechecker_check-landing"><span><?php echo get_post_meta($frontpage_id, 'main.button', true) ?></span></button>
                </div>
            </div>
        </div>
        <div class="pages_more">
            <div class="wrapper">
                <p class="pages_more-title">Interesting now</p>
                <?php
                $paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
                $args = array(
                    'posts_per_page' => 6,
                    'post__not_in' => array(17,15),
                    'post_type' => array('page','post'),
                    'paged' => $paged,
                    'meta_query' => array(
                        array(
                            'key' => 'page.about'
                        )
                    )
                );
                $query = new WP_Query($args);
                ?>
                <?php if ( $query->have_posts() ) : ?>
                    <div class="pages_more-wrap">
                        <?php while ( $query->have_posts() ) : $query->the_post(); ?>
                            <div class="block">
                                <div class="block_img" style="background: url(<?php the_post_thumbnail_url(); ?>);background-size: cover;background-position: center center;"></div>
                                <a href="<?php the_permalink(); ?>"><?php echo get_post_meta( get_the_ID(), 'page.h1', true); ?></a>
                                <?php
                                $string = get_post_meta( get_the_ID(), 'page.about', true);
                                $maxLength = 200;
                                if (strlen($string) > $maxLength) {
                                    $stringCut = substr($string, 0, $maxLength);
                                    $string = substr($stringCut, 0, strrpos($stringCut, ' '));
                                }
                                ?>
                                <p><?php echo $string ?></p>
                            </div>
                        <?php endwhile;wp_reset_postdata(); ?>
                        <!-- end loop -->
                    </div>
                <?php else : ?>
                    <p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
                <?php endif; ?>
                <p class="pages_more-show"><span>Show more</span> <i class="fa fa-chevron-down"></i></p>
            </div>
        </div>
    </main>
<?php endwhile; ?>
<?php else: ?>
<?php endif; ?>
