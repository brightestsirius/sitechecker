<?php
/*
	Template Name: Page Template
*/
$template_directory_uri = get_template_directory_uri();
get_header('tmp_design');
get_template_part('design/pages');
get_footer('tmp_design'); ?>
<script src="<?=$template_directory_uri;?>/out/js/validations__design-min.js?<?php echo mt_rand(100000,999999); ?>"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/highstock/6.0.3/highstock.js"></script>
<?php if($post->ID === 1063){ ?>
    <script>
        $('#speed__test-form').submit(function (e) {
            e.preventDefault();
            let form = $(this),
                btn = $(this).find('button'),
                urlData =  $(this).find('input').val(),
                apiPath = "/wp-admin/admin-ajax.php";
            form.removeClass('no_valid');
            $('.speed__testData').fadeOut();
            if(urlData === ''){
                form.addClass('no_valid');
            } else{
                btn.find('.arrow').css('display','none');
                btn.find('.loading').fadeIn();
                $.ajax({
                    url: apiPath,
                    type: "POST",
                    dataType: "JSON",
                    async: true,
                    data: {
                        action: 'crawler_validate_domain_request',
                        url: urlData
                    },
                    success: function (data) {
                        if(data.status === false){
                            form.addClass('invalid');
                            form.find('input').val('').attr('placeholder', trans['Minitools.Error.Placeholder']);
                            btn.find('.arrow').fadeIn();
                            btn.find('.loading').css('display','none');
                        } else{
                            let statusUrl = `${document.location.protocol}//api1.${host}/api/v1/full/28/${urlData}`,
                                statusRequest = $.ajax({
                                    url: statusUrl,
                                    method: 'GET',
                                    dataType: 'json',
                                    xhrFields: {
                                        withCredentials: true
                                    }
                                });
                            statusRequest.done(function(response) {
                                let data = response.data.checks;
                                btn.find('span').fadeIn();
                                btn.find('.loading').css('display','none');
                                $('.fix__containers').empty();
                                function importance(status, data){
                                    switch (status){
                                        case 'warning':
                                            return '<div class="importance-warning">'+data+'</div>';
                                            break;
                                        case 'ok':
                                            return '<div class="importance-ok">'+data+'</div>';
                                            break;
                                        case 'error':
                                            return '<div class="importance-error">'+data+'</div>';
                                            break;
                                    }
                                }
                                let average = data[3]['title'],
                                    average_status = data[3]['importance'],
                                    FCP = data[1]['title'],
                                    FCP_status = data[1]['importance'],
                                    DCL = data[2]['title'],
                                    DCL_status = data[2]['importance'],
                                    FCP__graphFast = data[4]['title'][0]['proportion'],
                                    FCP__graphFastTooltip = data[4]['title'][0]['tooltip'],
                                    FCP__graphFastMax = data[4]['title'][0]['max'],
                                    FCP__graphAverage = data[4]['title'][1]['proportion'],
                                    FCP__graphAverageTooltip = data[4]['title'][1]['tooltip'],
                                    FCP__graphAverageMax = data[4]['title'][1]['max'],
                                    FCP__graphSlow = data[4]['title'][2]['proportion'],
                                    FCP__graphSlowTooltip = data[4]['title'][2]['tooltip'],
                                    FCP__graphSlowMax = data[4]['title'][2]['min'],
                                    DCL__graphFast = data[5]['title'][0]['proportion'],
                                    DCL__graphFastTooltip = data[5]['title'][0]['tooltip'],
                                    DCL__graphFastMax = data[5]['title'][0]['max'],
                                    DCL__graphAverage = data[5]['title'][1]['proportion'],
                                    DCL__graphAverageTooltip = data[5]['title'][1]['tooltip'],
                                    DCL__graphAverageMax = data[5]['title'][1]['max'],
                                    DCL__graphSlow = data[5]['title'][2]['proportion'],
                                    DCL__graphSlowTooltip = data[5]['title'][2]['tooltip'],
                                    DCL__graphSlowMax = data[5]['title'][2]['min'],
                                    i = 6;
                                if(FCP.indexOf('First contentful paint') < 0){
                                    i = 1,
                                        average = data[0]['title'],
                                        average_status = data[0]['importance'],
                                        FCP = data[0]['title'];
                                    $('.info__description').css('display','none');
                                    $('.speed__test-info>div .info').css({
                                        'height':'auto',
                                        'padding-bottom':'1px'
                                    });
                                    $('.info__title').addClass('optimize');
                                }else{
                                    $('.info__description').css('display','block');
                                    $('.info__title').removeClass('optimize');
                                }
                                $('.info__title').html(importance(average_status, average));
                                $('.info__description div:first-of-type').html(importance(FCP_status, FCP));
                                $('.info__description div:nth-child(2)').html(importance(DCL_status, DCL));
                                if(FCP__graphFast === undefined){
                                    $('.speed__test-distributions').css('display','none');
                                } else{
                                    $('.speed__test-distributions').css('display','block');
                                    $('.graph__block:first-of-type .fast').css('width',(FCP__graphFast*100).toFixed(0)+'%').find('span').text((FCP__graphFast*100).toFixed(0)+'%');
                                    $('.graph__block:first-of-type .average').css('width',(FCP__graphAverage*100).toFixed(0)+'%').find('span').text((FCP__graphAverage*100).toFixed(0)+'%');
                                    $('.graph__block:first-of-type .slow').css('width',(FCP__graphSlow*100).toFixed(0)+'%').find('span').text((FCP__graphSlow*100).toFixed(0)+'%');
                                    $('.graph__block:nth-child(2) .fast').css('width',(DCL__graphFast*100).toFixed(0)+'%').find('span').text((DCL__graphFast*100).toFixed(0)+'%');
                                    $('.graph__block:nth-child(2) .average').css('width',(DCL__graphAverage*100).toFixed(0)+'%').find('span').text((DCL__graphAverage*100).toFixed(0)+'%');
                                    $('.graph__block:nth-child(2) .slow').css('width',(DCL__graphSlow*100).toFixed(0)+'%').find('span').text((DCL__graphSlow*100).toFixed(0)+'%');
                                    function tooltip(block, text, param1, param2){
                                        block.html(replaceTag(text));
                                        block.find('.percent:first-of-type').text((param1*100).toFixed(0)+'%');
                                        block.find('.percent:nth-child(2)').text(param2/1000+'s');
                                    }
                                    function replaceTag(text){
                                        return text.replace(/&lt;/g, '<').replace(/&gt;/g, '>').replace(/&quot;/g, '"');
                                    }
                                    tooltip($('.graph__block:first-of-type .fast .tooltip'), FCP__graphFastTooltip, FCP__graphFast, FCP__graphFastMax);
                                    tooltip($('.graph__block:first-of-type .average .tooltip'), FCP__graphAverageTooltip, FCP__graphAverage, FCP__graphAverageMax);
                                    tooltip($('.graph__block:first-of-type .slow .tooltip'), FCP__graphSlowTooltip ,FCP__graphSlow, FCP__graphSlowMax);
                                    tooltip($('.graph__block:nth-child(2) .fast .tooltip'), DCL__graphFastTooltip, DCL__graphFast, DCL__graphFastMax);
                                    tooltip($('.graph__block:nth-child(2) .average .tooltip'), DCL__graphAverageTooltip, DCL__graphAverage, DCL__graphAverageMax);
                                    tooltip($('.graph__block:nth-child(2) .slow .tooltip'), DCL__graphSlowTooltip, DCL__graphSlow, DCL__graphSlowMax);
                                }
                                while (i < data.length) {
                                    let fixText = 'Show how to fix';
                                    if(data[i]['importance'] === 'ok'){
                                        fixText = 'Show more';
                                    }
                                    $('.fix__containers').append('<div class="fix__container-block"><p>'+importance(data[i]['importance'],data[i]['title'])+'</p><div class="fix">'+data[i]['fix']+'</div><div class="fix__link">'+fixText+' <img src="/wp-content/themes/sitechecker/out/img_design/arrow__down-blue.svg" alt="arrow" title="arrow"></div></div>');
                                    i++;
                                }
                                $('.fix__link').click(function () {
                                    let fix__block = $(this).parent();
                                    if(fix__block.hasClass('open')){
                                        fix__block.removeClass('open');
                                        fix__block.find('.fix').hide(300);
                                    }else{
                                        fix__block.addClass('open');
                                        fix__block.find('.fix').show(300);
                                    }
                                });
                                $('.speed__testData').show(200);
                                let resultContainer = $('.speed__testData_container'),
                                    destination = resultContainer.offset().top-30;
                                jQuery("html:not(:animated),body:not(:animated)").animate({
                                    scrollTop: destination
                                }, 800);
                                btn.find('.arrow').fadeIn();
                                btn.find('.loading').css('display','none');
                            });
                        }
                    }
                });
            }
        });
    </script>
<?php } ?>
<?php if($post->ID === 2833){ ?>
    <script>
        $('#backlinks__form').submit(function (e) {
            e.preventDefault();
            let form = $(this),
                btn = $(this).find('button'),
                urlData =  $(this).find('input').val(),
                apiPath = "/wp-admin/admin-ajax.php",
                backlickCheck = $('.backlinks__select').val();
            form.removeClass('no_valid');
            $('.backlinks__data').fadeOut();
            if(urlData === ''){
                form.addClass('no_valid');
            } else{
                btn.find('.arrow').css('display','none');
                btn.find('.loading').fadeIn();
                $.ajax({
                    url: apiPath,
                    type: "POST",
                    dataType: "JSON",
                    async: true,
                    data: {
                        action: 'crawler_validate_domain_request',
                        url: urlData
                    },
                    success: function (data) {
                        if(data.status === false){
                            form.addClass('no_valid');
                            btn.find('.arrow').fadeIn();
                            btn.find('.loading').css('display','none');
                        } else{
                            let statusUrl = `${document.location.protocol}//api1.${host}/api/v1/full/${backlickCheck}/${urlData}`,
                                statusRequest = $.ajax({
                                    url: statusUrl,
                                    method: 'GET',
                                    dataType: 'json',
                                    xhrFields: {
                                        withCredentials: true
                                    }
                                });
                            statusRequest.done(function(response) {
                                btn.find('.arrow').fadeIn();
                                btn.find('.loading').css('display','none');
                                $('.backlinks__data-table tbody').empty();
                                let checks = response.data.checks,
                                    allUrls = 0,
                                    dofollowUrls = 0;
                                function formatDate(date) {
                                    let monthNames = [
                                        "January", "February", "March",
                                        "April", "May", "June", "July",
                                        "August", "September", "October",
                                        "November", "December"
                                    ],
                                        day = date.getDate(),
                                        monthIndex = date.getMonth(),
                                        year = date.getFullYear();

                                    return  monthNames[monthIndex]+ ' ' + day + ', ' + year;
                                }
                                $.each(checks, function( key, value ) {
                                    switch (value.label){
                                        case "backlinks" :
                                            $('#total__backlinks').text(value.title).addClass(value.importance);
                                            allUrls = value.title;
                                            break;
                                        case "refdomains" :
                                            $('#refdomains').text(value.title).addClass(value.importance);
                                            break;
                                        case "refips" :
                                            $('#refips').text(value.title).addClass(value.importance);
                                            break;
                                        case "dofollow" :
                                            $('#percent').html(value.title).addClass(value.importance);
                                            dofollowUrls = value.title;
                                            break;
                                        case "Backlinks" :
                                            let follow = value.title.nofollow,
                                                followText = 'dofollow',
                                                date = new Date(value.title.first_detected);
                                            if(follow === true){
                                                followText = 'nofollow';
                                            }
                                            $('.backlinks__data-table tbody').append('<tr><td><a href="'+value.title.url+'" target="_blank">'+value.title.url+'</a></td><td class="anchor">'+value.title.anchor_text+'</td><td><p class="'+followText+'">'+followText+'</p></td><td>'+value.title.domain_rating+'</td><td>'+value.title.ahrefs_rank+'</td><td>'+value.title.total_links+'</td><td>'+formatDate(date)+'</td></tr>');
                                            break;
                                        case "Ahrefs Rank" :
                                            let ahrefs = [],
                                                ahrefsKey = [];
                                            $.each(value.title, function( key, value ) {
                                                ahrefsKey.push(key);
                                                ahrefs.push(value.length);
                                            });
                                            Highcharts.chart('url__graph', {
                                                chart: {
                                                    type: 'column',
                                                    style: {
                                                        fontFamily: 'Roboto'
                                                    }
                                                },
                                                title: {
                                                    text: ''
                                                },
                                                xAxis: {
                                                    categories: ahrefsKey,
                                                    gridLineColor: '#ffffff',
                                                    minorTickLength: 0,
                                                    tickLength: 0
                                                },
                                                yAxis: {
                                                    labels: {
                                                        enabled: false
                                                    },
                                                    min: 0,
                                                    title: {
                                                        text: ''
                                                    },
                                                    gridLineColor: '#ffffff'
                                                },
                                                legend: {
                                                    enabled: false
                                                },
                                                tooltip: {
                                                    padding: 10,
                                                    shared: true,
                                                    borderWidth: 0,
                                                    backgroundColor: 'white',
                                                    shadow: false,
                                                    style: {
                                                        lineHeight: '20px'
                                                    }
                                                },
                                                plotOptions: {
                                                    column: {
                                                        stacking: 'normal'
                                                    }
                                                },
                                                series: [
                                                    {
                                                        name: 'URL rating distribution',
                                                        data: ahrefs,
                                                        color: '#00438b'
                                                    }]
                                            });
                                            break;
                                        case "Domain Rating" :
                                            let domains = [],
                                                domainsKey = [];
                                            $.each(value.title, function( key, value ) {
                                                domainsKey.push(key);
                                                domains.push(value.length);
                                            });
                                            Highcharts.chart('domain__graph', {
                                                chart: {
                                                    type: 'column',
                                                    style: {
                                                        fontFamily: 'Roboto'
                                                    }
                                                },
                                                title: {
                                                    text: ''
                                                },
                                                xAxis: {
                                                    categories: domainsKey,
                                                    gridLineColor: '#ffffff',
                                                    minorTickLength: 0,
                                                    tickLength: 0
                                                },
                                                yAxis: {
                                                    labels: {
                                                        enabled: false
                                                    },
                                                    min: 0,
                                                    title: {
                                                        text: ''
                                                    },
                                                    gridLineColor: '#ffffff'
                                                },
                                                legend: {
                                                    enabled: false
                                                },
                                                tooltip: {
                                                    padding: 10,
                                                    shared: true,
                                                    borderWidth: 0,
                                                    backgroundColor: 'white',
                                                    shadow: false,
                                                    style: {
                                                        lineHeight: '20px'
                                                    }
                                                },
                                                plotOptions: {
                                                    column: {
                                                        stacking: 'normal'
                                                    }
                                                },
                                                series: [
                                                    {
                                                        name: 'Domain rating distribution',
                                                        data: domains,
                                                        color: '#0066FF'
                                                    }]
                                            });
                                            break;
                                        default :
                                            break;
                                    }
                                });
                                Highcharts.chart('follow__graph', {
                                    chart: {
                                        plotBackgroundColor: null,
                                        plotBorderWidth: null,
                                        plotShadow: false,
                                        type: 'pie',
                                        style: {
                                            fontFamily: 'Roboto'
                                        }
                                    },
                                    title: {
                                        text: ''
                                    },
                                    tooltip: {
                                        padding: 10,
                                        borderWidth: 0,
                                        backgroundColor: 'white',
                                        shadow: false,
                                        style: {
                                            lineHeight: '20px'
                                        }
                                    },
                                    plotOptions: {
                                        pie: {
                                            dataLabels: {
                                                enabled: false
                                            },
                                            showInLegend: true,
                                            cursor: 'pointer',
                                            borderWidth: 4
                                        }
                                    },
                                    legend: {
                                        align: 'right',
                                        verticalAlign: 'middle',
                                        layout: 'vertical',
                                        itemMarginBottom: 8,
                                        useHTML: true
                                    },
                                    series: [
                                        {
                                            type: 'pie',
                                            name: 'Links',
                                            innerSize: '50%',
                                            colorByPoint: true,
                                            data: [
                                                {
                                                    name: 'Dofollow',
                                                    y: dofollowUrls,
                                                    color: '#73DA85'
                                                },
                                                {
                                                    name: 'Nofollow',
                                                    y: allUrls-dofollowUrls,
                                                    color: '#4D5C8B'
                                                }
                                            ]
                                        }
                                    ]
                                });
                                let resultContainer = $('.backlinks__data'),
                                    destination = ($('.backlinks__data_container').offset().top-30);
                                resultContainer.fadeIn();
                                jQuery("html:not(:animated),body:not(:animated)").animate({
                                    scrollTop: destination
                                }, 800);
                            });
                        }
                    }
                });
            }
        });
    </script>
<?php } ?>
<script src="<?=$template_directory_uri;?>/out/js/miniTools-min.js?<?php echo mt_rand(100000,999999); ?>"></script>
<script src="<?=$template_directory_uri;?>/out/js/action-min.js?<?php echo mt_rand(100000,999999); ?>"></script>
<link href="<?=$template_directory_uri;?>/out/js/fancybox/jquery.fancybox.css?v=2.1.7" rel="stylesheet" type="text/css" media="none" onload="if(media!='screen')media='screen'">
<script type="text/javascript" src="<?=$template_directory_uri;?>/out/js/fancybox/jquery.fancybox.pack.js?v=2.1.7"></script>
<script type="application/ld+json">
    <?php
        $categories = get_the_category();
        $category_name = $categories[0]->cat_name;
        if($category_name) {
            $category_link = get_category_link($categories[0]->cat_ID);
            $schemaBreadcrumbData = [
                "@context" => "http://schema.org",
                "@type" => "BreadcrumbList",
                "itemListElement" => [
                    [
                        "@type" => "ListItem",
                        "position" => 1,
                        "item" => [
                            "@id" => get_home_url(),
                            "name" => "Home"
                        ]
                    ],
                    [
                        "@type" => "ListItem",
                        "position" => 2,
                        "item" => [
                            "@id" => get_site_url() . "/knowledge-base/",
                            "name" => "Knowledge Base"
                        ]
                    ],
                    [
                        "@type" => "ListItem",
                        "position" => 3,
                        "item" =>
                            [
                                "@id" => $category_link,
                                "name" => $category_name
                            ]
                    ]


                ]
            ];

            print_r(json_encode(
                $schemaBreadcrumbData,
                JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE
            ));
        }
    ?>
</script>
<script type="application/ld+json">
    <?php
        $content = get_the_content();
        $content = wp_strip_all_tags($content);
        $content = str_replace(']]>', ']]&gt;', $content);
        $content = preg_replace('#\s*\[.+\]\s*#U', ' ', $content);
        $content = trim($content, "\xC2\xA0");
        $content = htmlspecialchars_decode($content);
//        $content = addslashes($content);
        $content = html_entity_decode($content);
        $content = trim($content);

        $content = preg_replace("/[\r\n]+/", "\n", $content);
        $content = str_replace("\n", "", $content);
        $content = str_replace("\t", "", $content);
        $content = htmlspecialchars($content);

        $schemaOrgData = [
            "@context" => "https://schema.org",
            "@type" => "Article",
            "mainEntityOfPage" => [
                "@type" => "WebPage",
                "@id" => get_permalink()
            ],
            "url" => get_permalink(),
            "datePublished" => date("Y-m-d",  strtotime(get_the_date())),
            "dateModified" => date("Y-m-d",  strtotime(get_the_modified_date())),
            "headline" => get_post_meta($post->ID, 'page.h1', true),
            "wordCount" => prefix_wcount(),
            "image" => [
                "@type" => "ImageObject",
                "url" => get_the_post_thumbnail_url()
            ],
            "articleBody" => $content,
            "author" => [
                "@type" => "Person",
                "name" => "Sitechecker.pro"
            ],
            "publisher" => [
                "@type" => "Organization",
                "name" => "Sitechecker.pro",
                "logo" => [
                    "@type" => "ImageObject",
                    "url" => "https://sitechecker.pro/wp-content/uploads/2018/11/Logo.png"
                ]
            ]
        ];

        echo (json_encode(
            $schemaOrgData,
            JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE
        ));
    ?>
</script>