<?php
/*
	Template Name: Article Page Design
*/
$template_directory_uri = get_template_directory_uri();
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>

    <?php $template_directory_uri = get_template_directory_uri();?>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900" rel="stylesheet">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css">

    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.4/css/select2.min.css" rel="stylesheet">
    <link rel="stylesheet"
          href="<?= $template_directory_uri; ?>/out/styles/prism.css">
    <link rel="stylesheet"
          href="<?= $template_directory_uri; ?>/out/styles/design.min.css?<?php echo mt_rand(100000, 999999); ?>">
    <?php wp_head(); ?>
</head>
<body>
<header>
    <div class="header">
        <div class="wrapper">
            <?php if(is_front_page()) { ?>
                <div class="header__logo">
                    <img src="<?= $template_directory_uri; ?>/out/img_design/logo.svg" alt="logo" title="logo">
                </div>
            <?php } else { ?>
                <a href="<?=home_url();?>/" class="header__logo">
                    <img src="<?= $template_directory_uri; ?>/out/img_design/logo.svg" alt="logo" title="logo">
                </a>
            <?php } ?>
            <div class="header__menu-mobile">
                <div class="mobile__close">
                    <span></span>
                    <span></span>
                </div>
                <div class="header__menu">
                    <ul>
                        <li class="menu-item-has-children">
                            <span>Products</span>
                            <ul class="sub-menu products">
                                <li><a href="">
                                        <img src="<?= $template_directory_uri; ?>/out/img_design/check__icon-active.svg"
                                             alt="products" title="products">
                                        <div class="products__description">
                                            <p>Website Live Check</p>
                                            <span>Find the mistakes in technical SEO for specific URL</span>
                                        </div>
                                    </a>
                                </li>
                                <li><a href="">
                                        <img src="<?= $template_directory_uri; ?>/out/img_design/audit__icon-active.svg"
                                             alt="products" title="products">
                                        <div class="products__description">
                                            <p>Website Audit</p>
                                            <span>Analyze all website pages in one window</span>
                                        </div>
                                    </a>
                                </li>
                                <li><a href="">
                                        <img src="<?= $template_directory_uri; ?>/out/img_design/monitoring__icon-active.svg"
                                             alt="products" title="products">
                                        <div class="products__description">
                                            <p>Website Monitoring</p>
                                            <span>Monitor website changes in real time</span>
                                        </div>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li><a href="/knowledge-base-design/">Knowledge Base</a></li>
                        <li><a href="/plans/">Pricing</a></li>
                        <?php
                        if ( is_user_logged_in() ) {
                        $userViewPosts = get_user_meta( get_current_user_id(), 'user_view_posts', true );

                        if($userViewPosts == ''){
                            update_user_meta( get_current_user_id(), 'user_view_posts', array('0'));
                        }

                        if(is_single()){
                            if (!in_array($pageID, $userViewPosts)){
                                array_push($userViewPosts, $pageID);
                                update_user_meta( get_current_user_id(), 'user_view_posts', $userViewPosts);
                                $userViewPosts = get_user_meta( get_current_user_id(), 'user_view_posts', true );
                            }
                        }

                        $args = array(
                            'numberposts' => 100,
                            'sort_order'   => 'DESC',
                            'sort_column' => 'date',
                            'post_type'    => 'post',
                            'post_status'  => 'publish'
                        );
                        $pages = get_posts( $args );
                        if(sizeof($pages)>0){
                            $showNews = 'flex';
                        }else{
                            $showNews = 'none';
                        }
                        $content = '';
                        $newsCount = get_user_meta( get_current_user_id(), 'news_count', true );
                        if($newsCount == ''){
                            update_user_meta( get_current_user_id(), 'news_count', sizeof($pages));
                            $bellPosts = sizeof($pages);
                        }else{
                            if($newsCount<sizeof($pages)){
                                $updateClass = 'request';
                                $updateNews = sizeof($pages);
                                $bellPosts = sizeof($pages) - $newsCount;
                            }else if($newsCount === sizeof($pages)){
                                $bellPosts = sizeof($pages);
                            }
                            else{
                                $updateClass = 'non_request';
                            }
                        }
                        foreach( $pages as $key=>$post ){
                            $background = '';
                            if($bellPosts>0){
                                for ($x = 0; $x < $bellPosts; $x++) {
                                    if($key == $x){
                                        $background = 'new';
                                    }
                                }
                            }
                            if (in_array($post->ID, $userViewPosts)){
                                $post_seen = 'seen';
                            }else{
                                $post_seen = 'notSeen';
                            }
                            $content .= '<div class="user-bell_news '.$background .' '.$post_seen.'"><a href="'.get_the_permalink().'">'.get_the_title().'</a>';
                            if(has_excerpt()) {
                                $content .= '<p>'.get_the_excerpt().'</p>';
                            }
                            $content .= '</div>';
                        }
                        wp_reset_postdata();
                        ?>
                            <li style="display:<?php echo $showNews ?>" class="user-bell <?php echo $updateClass?>" name="<?php echo $updateNews ?>">
                                <span class="user-bell-news">News</span><span class="bellPosts"><?php echo $bellPosts ?></span><div class="user-bell_list"><?php echo $content ?></div></li>
                        <?php } ?>
                    </ul>
                </div>
                <?php
                if ( is_user_logged_in() ) { ?>
                    <div class="header__user-block">
                        <a href="/crawl-report/" class="header_dashboard">
                            <svg width="12" height="8" viewBox="0 0 12 8" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M11.3536 4.35355C11.5488 4.15829 11.5488 3.84171 11.3536 3.64645L8.17157 0.464466C7.97631 0.269204 7.65973 0.269204 7.46447 0.464466C7.2692 0.659728 7.2692 0.976311 7.46447 1.17157L10.2929 4L7.46447 6.82843C7.2692 7.02369 7.2692 7.34027 7.46447 7.53553C7.65973 7.7308 7.97631 7.7308 8.17157 7.53553L11.3536 4.35355ZM0 4.5L11 4.5V3.5L0 3.5L0 4.5Z" fill="#0066FF"/>
                            </svg>Dashboard</a>
                        <div class="header__user">
                            <div class="header__user-info">
                                <div class="header__user-img"
                                     style="background-image: url(https://assets.vogue.com/photos/5afedb86ba60382efa9e6c18/master/pass/00-promo-image-adriana-lima-cannes-diary.jpg")></div>
                                <p class="header__user-name"><?=$current_user->user_email?></p>
                                <svg width="9" height="5" viewBox="0 0 9 5" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path fill-rule="evenodd" clip-rule="evenodd" d="M4.499 0L9 2.5V4.896L4.499 2.38L0 5V2.5L4.499 0Z" transform="translate(9 5) rotate(-180)" fill="#495c6a"/>
                            </div>
                            <div class="header__user-menu">
                                <ul>
                                    <li><a href="/account-design/">Personal Area</a></li>
                                    <li><a href="/affiliate-report/">Affiliate Report</a></li>
                                    <li><a href="/plans/">Upgrade Plan</a></li>
                                    <li><a href="<?php echo wp_logout_url( $logout_url ) ?>">Log out</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                <?php } else{ ?>
                    <div class="header__user not__login">
                        <button class="signin__btn">Sign in</button>
                    </div>
                <?php } ?>
            </div>
            <?php if ( !is_user_logged_in() ) { ?>
                <button class="ipad signin__btn">Sign in</button>
            <?php } ?>
            <div class="header__menu-mob">
                <span></span>
                <span></span>
                <span></span>
            </div>
        </div>
    </div>
</header>
<main>
    <div class="main article__page">
        <div class="wrapper">
            <div class="article__tags">
                <a href="">#content-marketing</a>
                <a href="">#content-marketing</a>
                <a href="">#content-marketing</a>
                <a href="">#content-marketing</a>
            </div>
            <h1 class="article__title">Interview with Mike Gingerich, President of Digital Hill Multimedia</h1>
            <div class="article__info">
                <div class="date">
                    <img src="<?= $template_directory_uri; ?>/out/img_design/article__clock.svg" alt="time"
                         title="time">
                    <span>June 18, 2018</span>
                </div>
                <div class="watches">
                    <img src="<?= $template_directory_uri; ?>/out/img_design/article__watch.svg" alt="watches"
                         title="watches">
                    <span>1275</span>
                </div>
                <div class="stars">
                    <img src="<?= $template_directory_uri; ?>/out/img_design/article__star.svg" alt="stars"
                         title="stars">
                    <span>45</span>
                </div>
            </div>
            <div class="article__content">
                <div class="article__content-title">
                    <img src="<?= $template_directory_uri; ?>/out/img_design/article__table.svg" alt="Table of Contents"
                         title="Table of Contents">
                    Table of Contents
                </div>
                <div class="article__content-body">
                    <a name=".first">What is the purpose of Poetry Analysis?</a>
                    <ul>
                        <li><a name=".first">Why write these papers?</a></li>
                        <li><a name=".first">What can you write about?</a></li>
                        <li><a name=".first">How can you get there</a></li>
                    </ul>
                    <a name=".first">What it looks like?</a>
                    <ul>
                        <li><a name=".first">In introduction</a></li>
                        <li><a name=".first">In main body mention</a></li>
                        <li><a name=".first">In conclusion</a></li>
                    </ul>
                </div>
            </div>
            <div class="article__menu-small">
                <button class="article__menu-img">
                    <svg width="24" height="20" viewBox="0 0 24 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M3 0H1C0.4 0 0 0.4 0 1V3C0 3.6 0.4 4 1 4H3C3.6 4 4 3.6 4 3V1C4 0.4 3.6 0 3 0Z" fill="#0066FF"/>
                        <path d="M3 0H1C0.4 0 0 0.4 0 1V3C0 3.6 0.4 4 1 4H3C3.6 4 4 3.6 4 3V1C4 0.4 3.6 0 3 0Z" transform="translate(0 8)" fill="#0066FF"/>
                        <path d="M3 0H1C0.4 0 0 0.4 0 1V3C0 3.6 0.4 4 1 4H3C3.6 4 4 3.6 4 3V1C4 0.4 3.6 0 3 0Z" transform="translate(0 16)" fill="#0066FF"/>
                        <path d="M15 0H1C0.4 0 0 0.4 0 1V3C0 3.6 0.4 4 1 4H15C15.6 4 16 3.6 16 3V1C16 0.4 15.6 0 15 0Z" transform="translate(8)" fill="#0066FF"/>
                        <path d="M15 0H1C0.4 0 0 0.4 0 1V3C0 3.6 0.4 4 1 4H15C15.6 4 16 3.6 16 3V1C16 0.4 15.6 0 15 0Z" transform="translate(8 8)" fill="#0066FF"/>
                        <path d="M15 0H1C0.4 0 0 0.4 0 1V3C0 3.6 0.4 4 1 4H15C15.6 4 16 3.6 16 3V1C16 0.4 15.6 0 15 0Z" transform="translate(8 16)" fill="#0066FF"/>
                    </svg>
                </button>
                <div class="article__menu-block">
                    <a name=".first">What is the purpose of Poetry Analysis?</a>
                    <ul>
                        <li><a name=".first">Why write these papers?</a></li>
                        <li><a name=".first">What can you write about?</a></li>
                        <li><a name=".first">How can you get there</a></li>
                    </ul>
                    <a name=".first">What it looks like?</a>
                    <ul>
                        <li><a name=".first">In introduction</a></li>
                        <li><a name=".first">In main body mention</a></li>
                        <li><a name=".first">In conclusion</a></li>
                    </ul>
                </div>
            </div>
            <div class="article__podcast">
                <iframe width="100%" height="166" scrolling="no" frameborder="no" allow="autoplay" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/452578407&color=%230066ff&auto_play=false&hide_related=false&show_comments=true&show_user=true&show_reposts=false&show_teaser=true"></iframe>
            </div>
            <h2 class="first">Title Headline 2 (H2)</h2>
            <p>A  cause and effect essay is a kind of academic writing where essay writers must determine the cause and effect of different actions, situations, and phenomenon. It usually has a standard 5-paragraph structure and requires students to involve their analytical skills in writing. <a href="">Teachers</a> of different subjects can assign cause and effect essay writing because this kind of work is a good evaluation of material awareness and critical skills of students.</p>
            <p>Cause and effect essay have several sub-types. For example, the essay can be focused only on highlighting of causes of some phenomenon. Or, on the contrary, the aim of the essay may be to determine the effects of certain events. And, of course, there are essay topics that oblige writer to examine both causes and effects of the of the phenomenon.</p>
            <div class="article__img-sm">
                <img src="http://www.mirocars.com/wp-content/uploads/2017-Tesla-Model-X-P100D.jpg">
                <p>Description under the small size image</p>
            </div>
            <p>All these cause and effect essay topic sub categories you can meet in your institution. So, if your home task is to complete an essay of this type, you must come up with original and effective ideas of topics. If you have no ideas, essay help service will help you. It is a common fact, that to write a successful essay that will deserve the highest grades, students must be well-motivated to write it. </p>
            <pre class="line-numbers">
                <code class="language-css">
        .example {
            font-family: Georgia, Times, serif;
            color: #555;
            text-align: center;
        }
                </code>
            </pre>
            <pre class="line-numbers">
                <code class="language-markup">
        &lt;!doctype html&gt;
        &lt;html lang="en"&gt;
        &lt;head>
            &lt;meta charset="UTF-8"&gt;
        &lt;/head&gt;
        &lt;body&gt;
            &lt;img src="example.png"&gt;
        &lt;/body&gt;
        &lt;/html&gt;
                </code>
            </pre>
            <pre class="line-numbers">
                <code class="line-numbers language-javascript">
        $('.lang__selected').click(function (e) {
            e.preventDefault();
            let lang_menu = $(this).parent();
            if(lang_menu.hasClass('open')){
                lang_menu.removeClass('open').find('.sub__lang').fadeOut(200);
            } else{
                lang_menu.addClass('open').find('.sub__lang').fadeIn(200);
            }
        });
                </code>
            </pre>
            <h3>Title Headline 3 (H3)</h3>
            <p>A  cause and effect essay is a kind of academic writing where essay writers must determine the cause and effect of different actions, situations, and phenomenon. It usually has a standard 5-paragraph structure and requires students to involve their analytical skills in writing. Teachers of different subjects can assign cause and effect essay writing because this kind of work is a good evaluation of material awareness and critical skills of students.</p>
        </div>
            <div class="article__img-md">
                <img src="http://insideevs.com/wp-content/uploads/2015/12/Blue-Sunset.jpg">
                <p>Description under the medium size image</p>
            </div>
        <div class="wrapper">
            <p>Cause and effect essay have several sub-types. For example, the essay can be focused only on highlighting of causes of some phenomenon. Or, on the contrary, the aim of the essay may be to determine the effects of certain events. And, of course, there are essay topics that oblige writer to examine both causes and effects of the of the phenomenon.</p>
            <p>All these cause and effect essay topic sub categories you can meet in your institution. So, if your home task is to complete an essay of this type, you must come up with original and effective ideas of topics. If you have no ideas, essay help service will help you. It is a common fact, that to write a successful essay that will deserve the highest grades, students must be well-motivated to write it.</p>
            <div class="article__divider">
                <span></span>
                <span></span>
                <span></span>
            </div>
            <p>How to use these topic ideas?</p>
            <p>Before you start working on cause and effect essay outline the first thing you must do is to choose a winning topic. At our website, we present top ideas for cause and effect essay topics. For your convenience, we have divided them into thematic subgroups. So, find a general theme (we provide 10 general topics below) and pick up the topic that will be the most appealing for you! The preparations of cause and effect paper have never been so simple before!</p>
        </div>
            <div class="article__img-lg">
                <div>
                    <img src="http://1920x1080hdwallpapers.com/image/201612/cars/8183/tesla-serpentine-france-overcast.jpg">
                </div>
                <p>Description under the fullscreen image</p>
            </div>
        <div class="wrapper">
            <div class="article__testimonial">
                <p>You can take the whole example of cause and effect essay writing ideas and develop it in your paper. It is also possible to take our topic and rewrite it to fit your task. Use the topics below to fight with creativity block during your work on cause and effect essay.</p>
                <div class="article__testimonial-user">
                    <div class="testimonial-user-img">
                        <img src="https://ksassets.timeincuk.net/wp/uploads/sites/46/2016/05/Best-Beauty-Cannes-Film-Festival-2016-Adriana-Lima--e1501163450723.jpg" alt="">
                    </div>
                    <div class="testimonial-user-info">
                        <p>Arya Stark</p>
                        <span>Game of Thrones</span>
                    </div>
                </div>
            </div>
            <div class="article__video">
                <iframe width="854" height="480" src="https://www.youtube.com/embed/qszGzNoopTc" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
            </div>
            <p>To sum up, it is important to note that reasons of long terms smoking addiction take their roots mostly in student years. People start to smoke to get new experience and to find a way to reduce stresses and feel relaxed. Consequently, those who have been smoking for years start to experience nicotine addiction, so they must supply their organism with cigarette smoke to fulfill the feeling of starvation.</p>
            <ul>
                <li>What is the reason of popularity of movies based on comic books? What is the reason of popularity of movies based on comic books? What is the reason of popularity of movies based on comic books?</li>
                <li>How listening to favorite music affect the person?</li>
                <li>Why are summer music festivals so popular among youth?</li>
                <li>How free music downloading effects the artist</li>
            </ul>
        </div>
            <div class="article__banner">
                <p>Do you need SEO-help with your Website?</p>
                <p>Check out these example persuasive essays.</p>
                <a href="" class="btn">Yes, i need seo-help</a>
            </div>
        <div class="wrapper">
            <h4>Headline 4 (H4)</h4>
            <p>After you selected a topic for cause and effect essay you can start working on its outline. Brainstorm your thoughts on the issue and make a plan of your paper. Look at the sample below to see how can you embody your ideas in certain structure:</p>
            <ul class="numbers">
                <li><a href="">How regular workout improves the productivity of a person?</a></li>
                <li>Causes and effects of skipping physical education lessons in school.</li>
                <li>What are effects of doing sports professionally?</li>
                <li>Causes and effects of sports violence.</li>
            </ul>
            <div class="article__share">
                <button class="fb">
                    <img src="<?= $template_directory_uri; ?>/out/img_design/fb.svg" alt="fb"
                         title="fb">
                    <span>Share</span>
                </button>
                <button class="twitter">
                    <img src="<?= $template_directory_uri; ?>/out/img_design/twitter.svg" alt="twitter"
                         title="twitter">
                    <span>Tweet</span>
                </button>
                <button class="google">
                    <img src="<?= $template_directory_uri; ?>/out/img_design/vk.svg" alt="google"
                         title="google">
                    <span>Share</span>
                </button>
            </div>
        </div>
            <div class="article__seo">
                <p>Get Free Website SEO Score Online</p>
                <p>Improve your SEO rating with the best website checker</p>
                <div class="article__seo-search">
                    <input type="text" placeholder="Enter URL for analysis">
                    <button><img src="<?= $template_directory_uri; ?>/out/img_design/arrow.svg" alt="arrow"
                                 title="arrow"></button>
                </div>
            </div>
        <div class="wrapper">
            <div class="article__more">
                <p class="article__more-title">You may also like</p>
                <hr>
            </div>
        </div>
        <div class="article__more-container">
            <div class="article__more-block">
                <div class="img__block" style="background-image: url(http://www.mirocars.com/wp-content/uploads/2017-Tesla-Model-X-P100D.jpg)"></div>
                <a href="/article-page/" class="title">The Beginner's Guide to Lead Generation Process</a>
                <span class="info__block">
                    <div class="watches">
                        <img src="<?= $template_directory_uri; ?>/out/img_design/article__watch.svg" alt="watches"
                             title="watches">
                        <span>1275</span>
                    </div>
                    <div class="stars">
                        <img src="<?= $template_directory_uri; ?>/out/img_design/article__star.svg" alt="stars"
                             title="stars">
                        <span>45</span>
                    </div>
                    <a href="" class="tag">#content-marketing</a>
                </span>
            </div>
            <div class="article__more-block">
                <div class="img__block" style="background-image: url(http://www.mirocars.com/wp-content/uploads/2017-Tesla-Model-X-P100D.jpg)"></div>
                <a href="/article-page/" class="title">The Beginner's Guide to Lead Generation Process</a>
                <span class="info__block">
                    <div class="watches">
                        <img src="<?= $template_directory_uri; ?>/out/img_design/article__watch.svg" alt="watches"
                             title="watches">
                        <span>1275</span>
                    </div>
                    <div class="stars">
                        <img src="<?= $template_directory_uri; ?>/out/img_design/article__star.svg" alt="stars"
                             title="stars">
                        <span>45</span>
                    </div>
                    <a href="" class="tag">#content-marketing</a>
                </span>
            </div>
            <div class="article__more-block">
                <div class="img__block" style="background-image: url(http://www.mirocars.com/wp-content/uploads/2017-Tesla-Model-X-P100D.jpg)"></div>
                <a href="/article-page/" class="title">The Overview of Content Marketing Strategy and Its Impact on SEO of Content Marketing Strategy and Its Impact on SEO</a>
                <span class="info__block">
                    <div class="watches">
                        <img src="<?= $template_directory_uri; ?>/out/img_design/article__watch.svg" alt="watches"
                             title="watches">
                        <span>1275</span>
                    </div>
                    <div class="stars">
                        <img src="<?= $template_directory_uri; ?>/out/img_design/article__star.svg" alt="stars"
                             title="stars">
                        <span>45</span>
                    </div>
                    <a href="" class="tag">#content-marketing</a>
                </span>
            </div>
            <div class="article__more-block">
                <div class="img__block" style="background-image: url(http://www.mirocars.com/wp-content/uploads/2017-Tesla-Model-X-P100D.jpg)"></div>
                <a href="/article-page/" class="title">The Overview of Content Marketing Strategy and Its Impact on SEO of Content Marketing Strategy and Its Impact on SEO</a>
                <span class="info__block">
                    <div class="watches">
                        <img src="<?= $template_directory_uri; ?>/out/img_design/article__watch.svg" alt="watches"
                             title="watches">
                        <span>1275</span>
                    </div>
                    <div class="stars">
                        <img src="<?= $template_directory_uri; ?>/out/img_design/article__star.svg" alt="stars"
                             title="stars">
                        <span>45</span>
                    </div>
                    <a href="" class="tag">#content-marketing</a>
                </span>
            </div>
        </div>
        <button class="btn more__articles">Show more articles</button>
    </div>
</main>
<footer>
    <div class="footer white__bg">
        <div class="wrapper">
            <div class="footer__logo">
                <?php if(is_front_page()) { ?>
                    <img src="<?= $template_directory_uri; ?>/out/img_design/logo.svg" alt="logo" title="logo">
                <?php } else { ?>
                    <a href="<?=home_url();?>/">
                        <img src="<?= $template_directory_uri; ?>/out/img_design/logo.svg" alt="logo" title="logo">
                    </a>
                <?php } ?>
                <p>© 2018 Sitechecker. All rights reserved.</p>
            </div>
            <div class="footer__menu">
                <p>Products</p>
                <ul>
                    <li>
                        <a href="<?=home_url();?>/">Live Website Check</a>
                    </li>
                    <li>
                        <a href="/website-crawler/">Website Audit</a>
                    </li>
                    <li>
                        <a href="/website-monitoring/">Website Monitoring</a>
                    </li>
                </ul>
            </div>
            <div class="footer__menu">
                <p>Company</p>
                <ul>
                    <li>
                        <a href="/about-us/">About</a>
                    </li>
                    <li>
                        <a href="/affiliate-program/">Partner program</a>
                    </li>
                    <li>
                        <a href="https://www.facebook.com/sitecheckerpro/">Facebook</a>
                    </li>
                </ul>
            </div>
            <div class="footer__menu">
                <p>Resources</p>
                <ul>
                    <li>
                        <a href="https://sitechecker.zendesk.com/hc/en-us">Support</a>
                    </li>
                    <li>
                        <a href="/contact-us/">Contact</a>
                    </li>
                    <li>
                        <a href="/knowledge-base/">Knowledge base</a>
                    </li>
                </ul>
            </div>
            <div class="footer__menu">
                <p>Legality</p>
                <ul>
                    <li>
                        <a href="/terms-and-conditions/">Terms & Conditions</a>
                    </li>
                    <li>
                        <a href="/privacy-policy/">Privacy Policy</a>
                    </li>
                </ul>
            </div>
            <div class="footer__lang">
                <ul>
                    <li class="lang__selected">
                        <img src="<?= $template_directory_uri; ?>/out/img_design/en.svg" alt="English" title="English">
                        <span>English</span>
                        <img class="lang__arrow"
                             src="<?= $template_directory_uri; ?>/out/img_design/arrow__down.svg" alt="arrow"
                             title="arrow">
                    </li>
                    <ul class="sub__lang">
                        <li>
                            <a href="">
                                <img src="<?= $template_directory_uri; ?>/out/img_design/ge.svg" alt="Deutsch"
                                     title="Deutsch">
                                <span>Deutsch</span>
                            </a>
                        </li>
                        <li>
                            <a href="">
                                <img src="<?= $template_directory_uri; ?>/out/img_design/ru.svg" alt="Русский"
                                     title="Русский">
                                <span>Русский</span>
                            </a>
                        </li>
                        <li>
                            <a href="">
                                <img src="<?= $template_directory_uri; ?>/out/img_design/fr.svg" alt="Français"
                                     title="Français">
                                <span>Français</span>
                            </a>
                        </li>
                        <li>
                            <a href="">
                                <img src="<?= $template_directory_uri; ?>/out/img_design/it.svg" alt="Italiano"
                                     title="Italiano">
                                <span>Italiano</span>
                            </a>
                        </li>
                        <li>
                            <a href="">
                                <img src="<?= $template_directory_uri; ?>/out/img_design/ne.svg" alt="Nederlands"
                                     title="Nederlands">
                                <span>Nederlands</span>
                            </a>
                        </li>
                        <li>
                            <a href="">
                                <img src="<?= $template_directory_uri; ?>/out/img_design/sp.svg" alt="Español"
                                     title="Español">
                                <span>Español</span>
                            </a>
                        </li>
                        <li>
                            <a href="">
                                <img src="<?= $template_directory_uri; ?>/out/img_design/no.svg" alt="Norwegian"
                                     title="Norwegian">
                                <span>Norwegian</span>
                            </a>
                        </li>
                        <li>
                            <a href="">
                                <img src="<?= $template_directory_uri; ?>/out/img_design/pt.svg" alt="Português"
                                     title="Português">
                                <span>Português</span>
                            </a>
                        </li>
                        <li>
                            <a href="">
                                <img src="<?= $template_directory_uri; ?>/out/img_design/sw.svg" alt="Svenska"
                                     title="Svenska">
                                <span>Svenska</span>
                            </a>
                        </li>
                    </ul>
                </ul>
            </div>
        </div>
    </div>
</footer>
<div class="login__popup">
    <div class="login__popup-block">
        <img src="<?= $template_directory_uri; ?>/out/img_design/close.svg" alt="close" title="close" class="login__popup-close">
        <div class="signin__block">
            <?=do_shortcode('[theme-my-login instance=1 default_action=login show_title=0 show_reg_link=0]');?>
            <div class="login__popup-actions">
                <div class="popup-actions__wrapper">
                    <button class="popup__signup" type="button">Create account</button>
                    <button class="popup__pass" type="button">Forgot password?</button>
                </div>
            </div>
        </div>
        <div class="signup__block">
            <?=do_shortcode('[theme-my-login instance=2 default_action=register show_title=0 show_pass_link=0 show_log_link=0]');?>
            <div class="login__terms">
                By clicking button "Create account", "Create with Facebook" or "Create with Google" you agree to our <a
                    href="">Terms and Conditions</a> and <a href="">Privacy Policy</a>. We’ll occasionally send you account related and promo emails.
            </div>
            <div class="login__popup-actions">
                <div class="popup-actions__wrapper">
                    <p>Already have an account?</p> <button class="popup__signin">Sign In</button>
                </div>
            </div>
        </div>
        <div class="resetpass__block">
            <?=do_shortcode('[theme-my-login instance=3 default_action=lostpassword show_title=0 show_reg_link=0 show_log_link=0]');?>
            <div class="login__popup-actions">
                <div class="popup-actions__wrapper">
                    <p>Not a member yet?</p> <button class="popup__signup">Create account</button>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="resetpass__popup">
    <div class="resetpass__popup-block">
        <img src="<?= $template_directory_uri; ?>/out/img_design/close.svg" alt="close" title="close" class="login__popup-close">
        <div class="circle-loader">
            <div class="checkmark draw"></div>
        </div>
        <p class="title">Check Your Email</p>
        <div class="description">
            We have sent you a new link to change your password. Check your email and follow instructions <img src="<?= $template_directory_uri; ?>/out/img_design/envelope.svg" alt="envelope" title="envelope">
        </div>
    </div>
</div>
<div class="newpass__popup">
    <div class="resetpass__popup-block">
        <img src="<?= $template_directory_uri; ?>/out/img_design/close.svg" alt="close" title="close" class="login__popup-close">
        <div class="circle-loader">
            <div class="checkmark draw"></div>
        </div>
        <p class="title">Your password has been reset successfully!</p>
        </div>
    </div>
</div>
<script src="<?= $template_directory_uri; ?>/out/js/jquery-3.2.1.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/bootstrap-select.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.4.1/jquery.cookie.js"></script>
<script src="http://code.highcharts.com/stock/highstock.js"></script>
<script src="<?= $template_directory_uri; ?>/out/js/prism.js"></script>
<script src="<?= $template_directory_uri; ?>/out/js/scripts-design-min.js?<?php echo mt_rand(100000, 999999); ?>"></script>
</body>
</html>

<?php wp_footer(); ?>