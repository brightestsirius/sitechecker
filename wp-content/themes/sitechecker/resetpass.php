<?php
/*
	Template Name: Resetpass
*/
get_header('tmp_design')
?>
<main class="resetpass">
    <div class="main">
        <div class="wrapper">
            <?php if (have_posts()): while (have_posts()) : the_post(); ?>
                <?php the_content(); ?>
            <?php endwhile; ?>
            <?php else: ?>
            <?php endif; ?>
        </div>
    </div>
</main>
<?php
get_footer('tmp_design');
?>