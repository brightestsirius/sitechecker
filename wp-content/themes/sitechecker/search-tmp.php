<?php
    $template_directory_uri = get_template_directory_uri();
    $meta = get_post_meta_all(get_option('page_on_front'));
?>

<main class="home knowledge">
    <div class="wrapper">
        <h1><?php echo get_the_title( 1446 ) ?></h1>
        <!-- search -->
        <form class="search" method="get" action="<?php echo home_url(); ?>" role="search">
            <img src="<?=$template_directory_uri;?>/out/img/search.svg" alt="search" title="search">
            <input class="search-input" type="search" name="s" placeholder="What are you looking for?">
            <button class="search-submit btn_gradient" type="submit" role="button"><span>Search</span></button>
        </form>
        <!-- /search -->
        <div class="pages_wrapper-results">
            <p class="search_results">
                <?php
                if($wp_query->found_posts == 0){
                    echo '<p class="search_results">Unfortunately, we didn\'t find your request.</p><p>Suggestions:</p><ul><li>Use chat at the right if you have a specific question</li><li>Use <a href="https://www.google.com/" target="_blank">google search</a> to collect other guides</li></ul>';
                } else{
                    echo '<p class="search_results"><span>'.$wp_query->found_posts.'</span> Search Results for <span>'.get_search_query().'</span></p>';
                }
                ?>
        </div>
        <div class="pages_more-wrap">
            <?php get_template_part('loop'); ?>
        </div>
        <?php get_template_part('pagination'); ?>
    </div>
</main>