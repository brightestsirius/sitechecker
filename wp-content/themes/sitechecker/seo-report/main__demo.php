<div class="seo_report-demo <?php if ( is_user_logged_in() ) { echo 'loged_in'; } ?>">
<?php
    /*
        Template Name: Seo-Report Demo
    */
$template_directory_uri = get_template_directory_uri();
?>
<?php
    get_header('tmp_design');
    include_once ('validations__demo.php');
    get_footer('tmp_design');
?>
</div>
<script>
    const locale = "<?=qtranxf_getLanguage();?>";
    const default_locale = "<?=qtranxf_getLanguageDefault();?>";
    const trans = JSON.parse('<?=json_encode(getApiDictionary(), JSON_HEX_APOS | JSON_HEX_AMP | JSON_HEX_QUOT);?>'.replace(/\\'/g, "\'"));
    const lang_cookie_name = "qtrans_front_language";
</script>
<script src="<?=$template_directory_uri;?>/out/js/seo_report_demo-min.js?<?php echo mt_rand(100000,999999); ?>"></script>
<?php
if (get_current_user_id() == 0) {
    echo '<script>const check_cookie_name = "validations_count"</script>';
}
?>