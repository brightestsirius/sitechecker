<?php 
/*
	Template Name: Seo-Report
*/
?>
<!doctype html>
<html <?php language_attributes(); ?> class="no-js">
<head>
    <?php
    $template_directory_uri = get_template_directory_uri();
    ?>
    <meta charset="<?php bloginfo('charset'); ?>">

    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" rel="stylesheet">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css">
    <link rel="stylesheet" href="<?=$template_directory_uri;?>/out/styles/main.min.css?<?php echo mt_rand(100000,999999); ?>">

    <meta name="google-site-verification" content="eQF_wkzvhKYn757NRtDGzu-1PMBkEEsflHZs3QMHu_s" />
    <meta name="msvalidate.01" content="0FD73A58AA195EF7A0EC234993CBDA1E" />

    <meta name="yandex-verification" content="f4f76edefaf924fe" />

    <!-- Chatra {literal} -->
    <script>
        (function(d, w, c) {
            w.ChatraID = 'v9EhBWLGqJHgHyrKG';
            var s = d.createElement('script');
            w[c] = w[c] || function() {
                (w[c].q = w[c].q || []).push(arguments);
            };
            s.async = true;
            s.src = (d.location.protocol === 'https:' ? 'https:': 'http:')
                + '//call.chatra.io/chatra.js';
            if (d.head) d.head.appendChild(s);
        })(document, window, 'Chatra');
    </script>
    <!-- /Chatra {/literal} -->
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push(
            {'gtm.start': new Date().getTime(),event:'gtm.js'}
        );var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-TLCC7S4');</script>

    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push(
            {'gtm.start': new Date().getTime(),event:'gtm.js'}
        );var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-N2SCB2J');</script>
    <!-- End Google Tag Manager -->

    <?php wp_head(); ?>

    <script src="<?=$template_directory_uri;?>/out/js/iframeResizer/iframeResizer.contentWindow.min.js?<?php echo mt_rand(100000,999999); ?>"></script>
</head>
<body>

<style>
    .home_first-search {
        display: none;
    }
</style>

<?php include_once 'validations.php' ;?>

</body>

<script src="<?=$template_directory_uri;?>/out/js/jquery-3.2.1.min.js"></script>
<script src="<?=$template_directory_uri;?>/out/js/owl.carousel.min-min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/bootstrap-select.min.js"></script>
<script src="<?=$template_directory_uri;?>/out/js/scripts-min.js?<?php echo mt_rand(100000,999999); ?>"></script>

</body>

<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-TLCC7S4"
                  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-N2SCB2J"
                  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

<?php include_once ('scripts.php') ;?>

</html>