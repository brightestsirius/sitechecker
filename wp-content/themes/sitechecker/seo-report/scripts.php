<?php if (isset($wp->query_vars['seo-report']) && $wp->query_vars['seo-report']) { ?>
    <script>
        const locale = "<?=qtranxf_getLanguage();?>";
        const default_locale = "<?=qtranxf_getLanguageDefault();?>";
        const trans = JSON.parse('<?=json_encode(getApiDictionary(), JSON_HEX_APOS | JSON_HEX_AMP | JSON_HEX_QUOT);?>'.replace(/\\'/g, "\'"));
        const lang_cookie_name = "qtrans_front_language";
    </script>

    <link href="<?=$template_directory_uri;?>/out/js/fancybox/jquery.fancybox.css?v=2.1.7" rel="stylesheet" type="text/css" media="none" onload="if(media!='screen')media='screen'">
    <script type="text/javascript" src="<?=$template_directory_uri;?>/out/js/fancybox/jquery.fancybox.pack.js?v=2.1.7"></script>
    <link href="<?=$template_directory_uri;?>/out/js/fancybox/helpers/jquery.fancybox-buttons.css?v=1.0.5" rel="stylesheet" type="text/css" media="screen" />
    <script type="text/javascript" src="<?=$template_directory_uri;?>/out/js/fancybox/helpers/jquery.fancybox-buttons.js?v=1.0.5" async></script>
    <link href="<?=$template_directory_uri;?>/out/js/fancybox/helpers/jquery.fancybox-thumbs.css?v=1.0.7" rel="stylesheet" type="text/css" media="none" onload="if(media!='screen')media='screen'">
    <script type="text/javascript" src="<?=$template_directory_uri;?>/out/js/fancybox/helpers/jquery.fancybox-thumbs.js?v=1.0.7" async></script>

    <script src="<?=$template_directory_uri;?>/out/js/action-min.js?<?php echo mt_rand(100000,999999); ?>"></script>
    <script src="<?=$template_directory_uri;?>/out/js/validations.js?<?php echo mt_rand(100000,999999); ?>"></script>

<?php } ?>

<!--preload images-->
<div style="display: none; position: absolute">
    <img src="/assets/images/image_error.png">
    <img src="/assets/images/ok.svg">
    <img src="/assets/images/info.svg">
    <img src="/assets/images/error.svg">
    <img src="/assets/images/unlink.svg">
    <img src="/assets/images/nofollow.svg">
    <img src="/assets/images/notice.svg">
    <img src="/assets/images/warning.svg">
    <img src="/assets/images/keyboard-arrow-right.svg">
    <img src="/assets/images/keyboard-arrow-down.svg">
    <img src="/assets/images/keyboard-arrow-up.svg">
    <img src="/assets/images/no-data.svg">
    <img src="/assets/images/arrow-drop-down.svg">
    <img src="/assets/images/arrow-drop-up.svg">
    <img src="/assets/images/question.svg">
</div>