<style>
    body {
        background: #eef3fa;
    }

    .home_first-search {
        max-width: 100%;
    }

    .home_first.seo-report {
        height: auto;
        justify-content: baseline;
    }

    .home_first-search input {
        width: 100%;
    }

    #result-container {
        display: none;
    }

    .language_menu > li > ul li {
        display: block !important;
    }
</style>

<?php
$template_directory_uri = get_template_directory_uri();
$meta = get_post_meta_all(get_option('page_on_front'));
$dict = getApiDictionary();
$tt = function ($key) use ($dict) {
    return $dict[$key] ?? $key;
};
if (is_user_logged_in()) {
    $userDomains = getDomainsByUserId(get_current_user_id());
    $availableLimit = getAvailableLimitByUser(get_current_user_id());
    $userAvaliableURL = $availableLimit->getAvailablePages();
    $userAvaliableDomains = $availableLimit->getAvailableDomains();
}
?>

<script>
    const USERCRAWLERDOMAINS = '<?=sizeof($userDomains)?>',
        USERAVALIABLEURL = '<?=$userAvaliableURL?>',
        USERAVALIABLEDOMAINS = '<?=$userAvaliableDomains?>';

    function getHostName(url) {
        var match = url.match(/:\/\/(www[0-9]?\.)?(.[^/:]+)/i);
        if (match != null && match.length > 2 && typeof match[2] === 'string' && match[2].length > 0) {
            return match[2];
        }
        else {
            return null;
        }
    }
</script><?php if (is_user_logged_in()) { ?>
    <div class="monitoring__nav">
    <div class="monitoring__nav-tab monitoring_navigation">
        <div class="img">
            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Layer_1" x="0px" y="0px" viewBox="0 0 377 377" style="enable-background:new 0 0 377 377;" xml:space="preserve" width="512px" height="512px">
                            <g>
                                <rect x="75" y="73.5" width="180" height="30" fill="#cccccc"/>
                                <rect x="75" y="273.5" width="302" height="30" fill="#cccccc"/>
                                <rect x="75" y="173.5" width="230" height="30" fill="#cccccc"/>
                            </g>
                            </svg>
        </div>
    </div>
    <a href="/" class="monitoring__nav-tab check-tab active">
        <div class="img">
            <svg width="14" height="22" viewBox="0 0 14 22" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M13.4194 11.0403L8.47127 8.03611L10.9658 1.76489C11.0428 1.60111 11.086 1.41656 11.086 1.22222C11.086 0.547556 10.5667 0 9.9227 0C9.63218 0.00488889 9.3895 0.101444 9.19699 0.261556C9.15848 0.293333 9.13515 0.312889 9.11531 0.334889L0.364705 9.01267C0.0975201 9.27789 -0.0343224 9.65922 0.00768055 10.0418C0.0496835 10.4243 0.263198 10.7641 0.580554 10.9584L5.5299 13.9651L3.00622 20.3084C2.79271 20.8413 2.96655 21.4573 3.41925 21.7849C3.61993 21.9291 3.85328 22 4.08196 22C4.37132 22 4.66067 21.8863 4.88469 21.6651L13.6353 12.9849C13.9025 12.7197 14.0343 12.3383 13.9923 11.9558C13.9503 11.5732 13.7368 11.2334 13.4194 11.0403Z"
                      fill="#CCCCCC"/>
            </svg>
        </div>
        <p>Live Check</p>
    </a>
    <a href="/crawl-report/" class="monitoring__nav-tab audit-tab domains-tab">
        <div>
            <div class="img">
                <svg width="24" height="23" viewBox="0 0 24 23" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M10.2347 8.37864L8.70534 6.88655C9.1759 6.19789 9.41117 5.39446 9.41117 4.59103C9.41117 2.06596 7.29366 0 4.70559 0C2.11751 0 0 2.06596 0 4.59103C0 7.1161 2.11751 9.18207 4.70559 9.18207C5.52906 9.18207 6.35254 8.95251 7.05838 8.49341L8.5877 9.9855C8.82298 10.215 9.52881 10.6742 10.2347 9.9855C10.7052 9.52639 10.7052 8.83774 10.2347 8.37864ZM4.70559 6.88655C3.41155 6.88655 2.35279 5.85357 2.35279 4.59103C2.35279 3.3285 3.41155 2.29552 4.70559 2.29552C5.99962 2.29552 7.05838 3.3285 7.05838 4.59103C7.05838 5.85357 5.99962 6.88655 4.70559 6.88655Z"
                          transform="translate(12.9403 12.6254)" fill="#CCCCCC"/>
                    <path d="M17.646 0H1.1764C0.470559 0 0 0.459103 0 1.14776V21.8074C0 22.4961 0.470559 22.9552 1.1764 22.9552H13.7638C11.8816 21.6926 10.5876 19.6267 10.5876 17.2164C10.5876 13.4288 13.7638 10.3298 17.646 10.3298C17.9989 10.3298 18.4694 10.3298 18.8223 10.4446V1.14776C18.8223 0.459103 18.3518 0 17.646 0ZM9.41117 9.18207H3.52919C2.82335 9.18207 2.35279 8.60819 2.35279 8.03431C2.35279 7.34565 2.82335 6.88655 3.52919 6.88655H9.41117C10.117 6.88655 10.5876 7.34565 10.5876 8.03431C10.5876 8.60819 10.117 9.18207 9.41117 9.18207ZM15.2932 4.59103H3.52919C2.82335 4.59103 2.35279 4.01715 2.35279 3.44327C2.35279 2.75462 2.82335 2.29552 3.52919 2.29552H15.2932C15.999 2.29552 16.4696 2.75462 16.4696 3.44327C16.4696 4.01715 15.999 4.59103 15.2932 4.59103Z"
                          fill="#CCCCCC"/>
                </svg>
            </div>
            <p>Audit</p>
        </div>
    </a>
    <a href="/monitoring-report/" class="monitoring__nav-tab monitor-tab domains-tab">
        <div>
            <div class="img">
                <svg width="20" height="23" viewBox="0 0 20 23" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M9.96031 22.8655C11.2493 22.8655 12.3039 21.8365 12.3039 20.5789H7.61671C7.61671 21.8365 8.67133 22.8655 9.96031 22.8655ZM17.577 16.0058V9.71784C17.577 6.17368 15.1162 3.3155 11.718 2.5152V1.71491C11.718 0.800292 10.8978 0 9.96031 0C9.02287 0 8.20261 0.800292 8.20261 1.71491V2.5152C4.80439 3.3155 2.3436 6.17368 2.3436 9.71784V16.0058L0 18.2924V19.4357H19.9206V18.2924L17.577 16.0058Z"
                          fill="#CCCCCC"/>
                </svg>
            </div>
            <p>Monitoring</p>
        </div>
    </a>
</div>
<?php } ?>
<main class="home">
    <div class="home_first seo-report">
        <div class="wrapper">
            <div class="home_first-search">
                <input id="sitechecker_input" type="text" placeholder="<?= t($meta['main.input']); ?>"
                       value="<?= $wp->query_vars['seo-report']; ?>">
                <button id="sitechecker_check" class="btn_gradient"><span><?= t($meta['main.button']); ?></span>
                </button>
            </div>

            <!--Validations-->
            <div class="container" id="result-container">
                <div class="container__left-col">
                    <div class="container__main-header">
                        <?= $tt('Quick navigation'); ?>
                    </div>
                    <span id="04fd2e2c-8765-3929-8650-def6b1190264"></span>
                    <div class="sidenav">
                    </div>
                </div>

                <div class="container__right-col">
                    <div class="container__results">
                        <div class="container__main-header">
                            <?= $tt('Results'); ?>
                        </div>

                        <div class="row--material row--cprogress">
                            <div class="cprogress-wrapper">
                                <div class="cprogress">
                                    <div class="cprogress__circle cprogress__circle--hidden">
                                        <div class="cprogress__loader"></div>
                                        <div class="cprogress__percents cprogress__percents--hidden"><span>100</span>/100
                                        </div>
                                        <div class="cprogress__bar cprogress__bar--hidden"></div>
                                    </div>
                                </div>
                                <div class="cprogress-wrapper__title"><?= $tt('Page score'); ?></div>
                            </div>

                            <div class="cprogress-infos">
                                <a href="#" class="cprogress-domain" id="cprogress-domain" target="_blank"></a>
                                <div class="cprogress-index"><?= $tt('Form.Page is'); ?>: <span class="indexable"
                                                                                                data-index="8"><?= $tt('Waiting'); ?></span>
                                </div>
                                <div class="cprogress-size"><?= $tt('Form.Page size'); ?>: <span
                                            class="indexable"><?= $tt('Waiting'); ?></span></div>
                                <div class="cprogress-status"><?= $tt('Form.Page status codes'); ?>: <span
                                            class="indexable"><?= $tt('Waiting'); ?></span></div>
                                <div class="cprogress-info cprogress-info--errors"><?= $tt('Critical Errors'); ?> <span>0</span>
                                </div>
                                <div class="cprogress-info cprogress-info--warnings"><?= $tt('Warnings'); ?>
                                    <span>0</span></div>
                                <div class="cprogress-info cprogress-info--notice"><?= $tt('Notice'); ?> <span>0</span>
                                </div>
                                <?php
                                $current_user = wp_get_current_user();
                                $customerMembershipStatus = $current_user->membership_level->name;
                                if ($customerMembershipStatus == 'Free' || !is_user_logged_in()) {
                                    echo '<a href="/plans/" class="save__pdf save__pdf-link" target="_blank">Download PDF Report <i class="fa fa-download"></i></a>';
                                } else {
                                    echo '<button id="save__pdf" class="save__pdf" disabled>Download PDF Report <i class="fa fa-download"></i></button>';
                                }
                                ?>
                            </div>
                        </div>
                            <span id="1f8f64e2-5566-4e3d-7beb-61d1ec843215"></span>
                        <div class="responses-wrapper">
                            <div class="responses responses--error">
                                <div class="responses__header"><?= $tt('Critical Errors'); ?></div>
                            </div>
                            <div class="responses responses--warning">
                                <div class="responses__header"><?= $tt('Warnings'); ?></div>
                            </div>
                            <div class="responses responses--notice">
                                <div class="responses__header"><?= $tt('Notice'); ?></div>
                            </div>
                        </div>
                        <span id="15a9c402-3417-ff10-c423-b43c99ac9abd"></span>
                        <div class="container__result" id="result-template">
                            <div class="result"></div>
                            <div class="loader">
                                <div class="animated-background">
                                    <div class="background-masker lvl-0"></div>
                                    <div class="background-masker lvl-1-1"></div>
                                    <div class="background-masker lvl-1-2"></div>
                                    <div class="background-masker lvl-1-3"></div>
                                    <div class="background-masker lvl-2"></div>
                                    <div class="background-masker lvl-3-1"></div>
                                    <div class="background-masker lvl-3-2"></div>
                                    <div class="background-masker lvl-4"></div>
                                    <div class="background-masker lvl-5-1"></div>
                                    <div class="background-masker lvl-5-2"></div>
                                    <div class="background-masker lvl-6"></div>
                                    <div class="background-masker lvl-7-1"></div>
                                    <div class="background-masker lvl-7-2"></div>
                                    <div class="background-masker lvl-8"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <span id="ad2c00a2-a6ac-b833-3706-3c30e30d292d"></span>
                </div>
                <div class="clear"></div>
            </div>
        </div>
    </div>
    <div class="scroll_up">
        <img src="/wp-content/uploads/2018/04/up-button-2.png" alt="scroll" title="scroll">
    </div>
</main>