<?php
    $template_directory_uri = get_template_directory_uri();
    $metaDesign = get_post_meta_all(url_to_postid( '/header-footer-design/' ));
    $meta = get_post_meta_all(get_option('page_on_front'));
    $dict = getApiDictionary();
    $tt = function ($key) use ($dict) {
        return $dict[$key] ?? $key;
    };
    if (is_user_logged_in()) {
        $userDomains = getDomainsByUserId(get_current_user_id());
        $availableLimit = getAvailableLimitByUser(get_current_user_id());
        $userAvaliableURL = $availableLimit->getAvailablePages();
        $userAvaliableDomains = $availableLimit->getAvailableDomains();
        $current_user = get_current_user_id();
        $customerMembershipStatus = $current_user->membership_level->name;
    }
?>
<script>
    const USERCRAWLERDOMAINS = '<?=sizeof($userDomains)?>',
        USERAVALIABLEURL = '<?=$userAvaliableURL?>',
        USERAVALIABLEDOMAINS = '<?=$userAvaliableDomains?>';

    function getHostName(url) {
        var match = url.match(/:\/\/(www[0-9]?\.)?(.[^/:]+)/i);
        if (match != null && match.length > 2 && typeof match[2] === 'string' && match[2].length > 0) {
            return match[2];
        }
        else {
            return null;
        }
    }
</script>
<?php
    $url = $wp->query_vars['seo-report-demo'];
    $url = str_replace('?limitRegistration','',$url);
?>
<main class="page">
    <div class="wrapper">
        <div class="report__container seo_demo_container">
            <div class="seo_demo_wrapper">
                <div class="report__results report__results-demo">
                    <div class="overview__header report__header report__header_demo">
                        <p class="title" id="sitechecker_input__value"><?php echo $url; ?></p>
                        <div class="progressbar_text">
                            <div id="progressbar" class="progressbar"><span>Errors</span></div>
                            <p><b>We have found technical SEO issues</b> on your website’s page.<br><span>Sign up to get detailed suggestions</span> on how to fix each of the errors.</p>
                        </div>
                    </div>
                    <div class="overview__block overview__block-report">
                        <div class="overview__block-data">
                            <div class="title"><span>Page Score</span>
                                <button type="button" class="question">
                                    <svg width="22" height="22" viewBox="0 0 22 22" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path d="M11 17.3379C11.5933 17.3379 12.0742 16.8569 12.0742 16.2637C12.0742 15.6704 11.5933 15.1895 11 15.1895C10.4067 15.1895 9.92578 15.6704 9.92578 16.2637C9.92578 16.8569 10.4067 17.3379 11 17.3379Z" fill="#D8D8D8"/>
                                        <path d="M11 0C4.92061 0 0 4.91979 0 11C0 17.0794 4.91979 22 11 22C17.0794 22 22 17.0802 22 11C22 4.92061 17.0802 0 11 0ZM11 20.2812C5.87052 20.2812 1.71875 16.1302 1.71875 11C1.71875 5.87052 5.86983 1.71875 11 1.71875C16.1295 1.71875 20.2812 5.86983 20.2812 11C20.2812 16.1295 16.1302 20.2812 11 20.2812Z" fill="#D8D8D8"/>
                                        <path d="M11 5.52148C9.10456 5.52148 7.5625 7.06355 7.5625 8.95898C7.5625 9.43362 7.94724 9.81836 8.42188 9.81836C8.89651 9.81836 9.28125 9.43362 9.28125 8.95898C9.28125 8.01127 10.0523 7.24023 11 7.24023C11.9477 7.24023 12.7188 8.01127 12.7188 8.95898C12.7188 9.9067 11.9477 10.6777 11 10.6777C10.5254 10.6777 10.1406 11.0625 10.1406 11.5371V13.6855C10.1406 14.1602 10.5254 14.5449 11 14.5449C11.4746 14.5449 11.8594 14.1602 11.8594 13.6855V12.2877C13.3403 11.9052 14.4375 10.5578 14.4375 8.95898C14.4375 7.06355 12.8954 5.52148 11 5.52148Z" fill="#D8D8D8"/>
                                    </svg>
                                    <span class="tooltip">Page Score is our internal rate based on combination of valuation of critical errors, warnings, notices and Google Page Speed results.</span>
                                </button>
                            </div>
                            <div class="value">
                                <div class="score__all-animation"></div>
                                <span id="score__all">0</span>
                                <span class="score"><i>/</i>100</span>
                            </div>
                        </div>
                        <div class="overview__block-data">
                            <div class="title"><span>Critical Errors</span>
                                <button type="button" class="question">
                                    <svg width="22" height="22" viewBox="0 0 22 22" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path d="M11 17.3379C11.5933 17.3379 12.0742 16.8569 12.0742 16.2637C12.0742 15.6704 11.5933 15.1895 11 15.1895C10.4067 15.1895 9.92578 15.6704 9.92578 16.2637C9.92578 16.8569 10.4067 17.3379 11 17.3379Z" fill="#D8D8D8"/>
                                        <path d="M11 0C4.92061 0 0 4.91979 0 11C0 17.0794 4.91979 22 11 22C17.0794 22 22 17.0802 22 11C22 4.92061 17.0802 0 11 0ZM11 20.2812C5.87052 20.2812 1.71875 16.1302 1.71875 11C1.71875 5.87052 5.86983 1.71875 11 1.71875C16.1295 1.71875 20.2812 5.86983 20.2812 11C20.2812 16.1295 16.1302 20.2812 11 20.2812Z" fill="#D8D8D8"/>
                                        <path d="M11 5.52148C9.10456 5.52148 7.5625 7.06355 7.5625 8.95898C7.5625 9.43362 7.94724 9.81836 8.42188 9.81836C8.89651 9.81836 9.28125 9.43362 9.28125 8.95898C9.28125 8.01127 10.0523 7.24023 11 7.24023C11.9477 7.24023 12.7188 8.01127 12.7188 8.95898C12.7188 9.9067 11.9477 10.6777 11 10.6777C10.5254 10.6777 10.1406 11.0625 10.1406 11.5371V13.6855C10.1406 14.1602 10.5254 14.5449 11 14.5449C11.4746 14.5449 11.8594 14.1602 11.8594 13.6855V12.2877C13.3403 11.9052 14.4375 10.5578 14.4375 8.95898C14.4375 7.06355 12.8954 5.52148 11 5.52148Z" fill="#D8D8D8"/>
                                    </svg>
                                    <span class="tooltip">The most critical errors on a page that need to be fixed.</span>
                                </button>
                            </div>
                            <div class="value">
                                <div class="score__all-animation"></div>
                                <span class="errors" id="score__errors">0</span>
                                <span class="score"><i>/</i>60</span>
                            </div>
                        </div>
                        <div class="overview__block-data">
                            <div class="title"><span>Warnings</span>
                                <button type="button" class="question">
                                    <svg width="22" height="22" viewBox="0 0 22 22" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path d="M11 17.3379C11.5933 17.3379 12.0742 16.8569 12.0742 16.2637C12.0742 15.6704 11.5933 15.1895 11 15.1895C10.4067 15.1895 9.92578 15.6704 9.92578 16.2637C9.92578 16.8569 10.4067 17.3379 11 17.3379Z" fill="#D8D8D8"/>
                                        <path d="M11 0C4.92061 0 0 4.91979 0 11C0 17.0794 4.91979 22 11 22C17.0794 22 22 17.0802 22 11C22 4.92061 17.0802 0 11 0ZM11 20.2812C5.87052 20.2812 1.71875 16.1302 1.71875 11C1.71875 5.87052 5.86983 1.71875 11 1.71875C16.1295 1.71875 20.2812 5.86983 20.2812 11C20.2812 16.1295 16.1302 20.2812 11 20.2812Z" fill="#D8D8D8"/>
                                        <path d="M11 5.52148C9.10456 5.52148 7.5625 7.06355 7.5625 8.95898C7.5625 9.43362 7.94724 9.81836 8.42188 9.81836C8.89651 9.81836 9.28125 9.43362 9.28125 8.95898C9.28125 8.01127 10.0523 7.24023 11 7.24023C11.9477 7.24023 12.7188 8.01127 12.7188 8.95898C12.7188 9.9067 11.9477 10.6777 11 10.6777C10.5254 10.6777 10.1406 11.0625 10.1406 11.5371V13.6855C10.1406 14.1602 10.5254 14.5449 11 14.5449C11.4746 14.5449 11.8594 14.1602 11.8594 13.6855V12.2877C13.3403 11.9052 14.4375 10.5578 14.4375 8.95898C14.4375 7.06355 12.8954 5.52148 11 5.52148Z" fill="#D8D8D8"/>
                                    </svg>
                                    <span class="tooltip">The number of issues of medium severity detected on your page during the last audit.</span>
                                </button>
                            </div>
                            <div class="value">
                                <div class="score__all-animation"></div>
                                <span class="warning" id="score__warnings">0</span>
                                <span class="score"><i>/</i>60</span>
                            </div>
                        </div>
                        <div class="overview__block-data">
                            <p class="cprogress-status">Status code <span></span></p>
                            <p class="cprogress-index">Page is <span></span></p>
                            <p class="cprogress-size">Page size is <span></span></p>
                        </div>
                    </div>
                    <div class="report__results-container">
                        <div class="report__response">
                            <img src="/wp-content/themes/sitechecker/out/img_design/loading-blue.svg" alt="arrow" title="arrow" class="loading">
                            <div class="report__response-block errors responses--error">
                                <p class="title">Critical Errors</p>
                                <div class="responses--content"></div>
                            </div>
                            <div class="report__response-block warnings responses--warning">
                                <p class="title">Warnings</p>
                                <div class="responses--content"></div>
                            </div>
                        </div>
                        <div class="report__validations"></div>
                    </div>
                </div>
                <div class="seo_demo-login">
                    <div class="login__popup-block seo_demo_login">
                        <div class="limit__popup">
                            <div class="text">
                                <p>Get free access to</p>
                            </div>
                            <ul class="sub-menu products">
                                <li><a href="">
                                        <img src="<?= $template_directory_uri; ?>/out/img_design/check__icon-active.svg"
                                             alt="products" title="products">
                                        <div class="products__description">
                                            <p><?=t($metaDesign['audit_title']);?></p>
                                            <span><?=t($metaDesign['audit_description']);?></span>
                                        </div>
                                    </a>
                                </li>
                                <li><a href="">
                                        <img src="<?= $template_directory_uri; ?>/out/img_design/audit__icon-active.svg"
                                             alt="products" title="products">
                                        <div class="products__description">
                                            <p><?=t($metaDesign['health_checker_title']);?></p>
                                            <span><?=t($metaDesign['health_checker_description']);?></span>
                                        </div>
                                    </a>
                                </li>
                                <li><a href="">
                                        <img src="<?= $template_directory_uri; ?>/out/img_design/link-symbol.svg"
                                             alt="products" title="products">
                                        <div class="products__description">
                                            <p><?=t($metaDesign['backlink_title']);?></p>
                                            <span><?=t($metaDesign['backlink_description']);?></span>
                                        </div>
                                    </a>
                                </li>
                                <li><a href="">
                                        <img src="<?= $template_directory_uri; ?>/out/img_design/monitoring__icon-active.svg"
                                             alt="products" title="products">
                                        <div class="products__description">
                                            <p><?=t($metaDesign['monitor_title']);?></p>
                                            <span><?=t($metaDesign['monitor_description']);?></span>
                                        </div>
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <div class="limit__popup-sign">
                            <div class="signup__block">
                                <?=do_shortcode('[theme-my-login instance=2 default_action=register show_title=0 show_pass_link=0 show_log_link=0]');?>
                                <div class="login__terms"><?=t($metaDesign['terms_and_conditions']);?></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>