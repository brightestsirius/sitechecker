<?php
    $template_directory_uri = get_template_directory_uri();
    $dict = getApiDictionary();
    $tt = function ($key) use ($dict) {
        return $dict[$key] ?? $key;
    };
    if (is_user_logged_in()) {
        $userDomains = getDomainsByUserId(get_current_user_id());
        $availableLimit = getAvailableLimitByUser(get_current_user_id());
        $userAvaliableURL = $availableLimit->getAvailablePages();
        $userAvaliableDomains = $availableLimit->getAvailableDomains();
        $current_user = get_current_user_id();
        $customerMembershipStatus = $current_user->membership_level->name;
    }
?>
<script>
    const USERCRAWLERDOMAINS = '<?=sizeof($userDomains)?>',
        USERAVALIABLEURL = '<?=$userAvaliableURL?>',
        USERAVALIABLEDOMAINS = '<?=$userAvaliableDomains?>';

    function getHostName(url) {
        var match = url.match(/:\/\/(www[0-9]?\.)?(.[^/:]+)/i);
        if (match != null && match.length > 2 && typeof match[2] === 'string' && match[2].length > 0) {
            return match[2];
        }
        else {
            return null;
        }
    }
</script>
<?php
    $url = $wp->query_vars['seo-report'];
    $url = str_replace('?limitRegistration','',$url);
?>
<main class="page">
    <div class="wrapper">
        <div class="banner__limit-domains limitRegistration">
            <div>
                We just launched technical SEO audit for 20 pages of <?php echo parse_url($url, PHP_URL_HOST); ?>. <a href="/tool/crawl-report/">View report</a>
                <img src="<?= $template_directory_uri; ?>/out/img_design/close.svg" alt="close" title="close" class="pdf_banner-close">
            </div>
        </div>
        <div class="report__container">
            <div class="report__sideblock">
                <div class="report__sidenav"></div>
            </div>
            <div class="report__results">
                <div class="overview__header report__header">
                    <form action="" id="sitechecker_input" class="form__Check" name="seo-report">
                        <span class="error">Must be a valid URL with http:// or https://</span>
                        <input class="title" value="<?php echo $url; ?>" id="sitechecker_input__value">
                        <button type="submit" class="btn"><span>Check</span><img src="<?= $template_directory_uri; ?>/out/img_design/arrow.svg" alt="arrow" title="arrow" class="arrow"><img src="<?= $template_directory_uri; ?>/out/img_design/loading.svg" alt="arrow" title="arrow" class="loading"></button>
                    </form>
                    <div class="overview__header-btn report__header-btn">
                        <span style="display: none" id="reportNewDesign">true</span>
                        <button type="button" id="refresh__report">
                            <svg width="16" height="15" viewBox="0 0 16 15" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M2.48186 8.4375H0.587178H0.529053V13.125L1.91749 11.8491C3.27779 13.7559 5.5081 15 8.02903 15C11.854 15 15.0096 12.1369 15.4709 8.4375H13.5762C13.1299 11.0981 10.8162 13.125 8.02903 13.125C6.05466 13.125 4.31654 12.1069 3.31342 10.5666L5.62997 8.4375H2.48186Z" fill="#BBBBBB"/>
                                <path d="M8.02901 0C4.20402 0 1.04841 2.86312 0.587158 6.56248H2.48184C2.92809 3.90186 5.24183 1.87499 8.02901 1.87499C10.0624 1.87499 11.8446 2.95405 12.8327 4.57124L10.8415 6.56248H12.7165H13.5762H15.4709H15.529V1.87499L14.1865 3.21749C12.8318 1.27218 10.579 0 8.02901 0Z" fill="#BBBBBB"/>
                            </svg>
                            <span class="tooltip">Refresh results</span>
                        </button>
                        <button type="button" id="copyReportLink">
                            <svg width="15" height="15" viewBox="0 0 15 15" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M12.2417 9.53342C11.3658 9.53342 10.5933 9.94715 10.0886 10.5807L5.40714 8.20691C5.46842 7.98026 5.51248 7.74609 5.51248 7.49963C5.51248 7.23133 5.46016 6.97873 5.38716 6.73294L10.0479 4.37007C10.5499 5.03232 11.3424 5.46517 12.2423 5.46517C13.7668 5.46517 15 4.24243 15 2.73225C15 1.22343 13.7668 0 12.2424 0C10.7214 0 9.48681 1.22343 9.48681 2.73222C9.48681 2.97937 9.53089 3.21422 9.59284 3.44156L4.91209 5.81537C4.40669 5.18114 3.63275 4.76605 2.75555 4.76605C1.23247 4.76605 0 5.99014 0 7.49963C0 9.00911 1.23251 10.2325 2.75555 10.2325C3.65685 10.2325 4.4487 9.79834 4.95269 9.13543L9.61139 11.4983C9.53839 11.7434 9.4854 11.998 9.4854 12.267C9.4854 13.7765 10.72 15 12.241 15C13.7654 15 14.9986 13.7765 14.9986 12.267C14.9993 10.7562 13.7661 9.53342 12.2417 9.53342Z" fill="#BBBBBB"/>
                            </svg>
                            <span class="tooltip">Copy report URL</span>
                        </button>
                        <?php
                        if(is_user_logged_in()) {
                            $current_user = wp_get_current_user();
                            $customerMembershipStatus = $current_user->membership_level->name;
                            if($customerMembershipStatus === 'Free'){ ?>
                                <a href="/plans/?pdfBanner" id="save__pdf">
                                    <svg class="downloadPDF" width="15" height="15" viewBox="0 0 15 15" fill="none"
                                         xmlns="http://www.w3.org/2000/svg">
                                        <path d="M12.1576 9.55936V12.9859H1.95998V9.55936H0V13.9931C0 14.549 0.438148 14.9998 0.981451 14.9998H13.1362C13.679 14.9998 14.1176 14.5495 14.1176 13.9931V9.55936H12.1576Z"
                                              fill="#BBBBBB"/>
                                        <path d="M6.87916 9.21973L4.07307 5.73469C4.07307 5.73469 3.64612 5.32038 4.1091 5.32038C4.57207 5.32038 5.69032 5.32038 5.69032 5.32038C5.69032 5.32038 5.69032 5.04168 5.69032 4.61187C5.69032 3.38647 5.69032 1.15634 5.69032 0.24718C5.69032 0.24718 5.62752 0 5.98972 0C6.35485 0 7.95408 0 8.21551 0C8.47645 0 8.47061 0.208152 8.47061 0.208152C8.47061 1.0898 8.47061 3.39698 8.47061 4.58235C8.47061 4.96663 8.47061 5.21581 8.47061 5.21581C8.47061 5.21581 9.36735 5.21581 9.93013 5.21581C10.4919 5.21581 10.0689 5.64962 10.0689 5.64962C10.0689 5.64962 7.68146 8.907 7.34847 9.24875C7.10895 9.49643 6.87916 9.21973 6.87916 9.21973Z"
                                              fill="#BBBBBB"/>
                                    </svg>
                                    <img src="<?= $template_directory_uri; ?>/out/img_design/loading-blue.svg"
                                         alt="arrow" title="arrow" class="loading">
                                    <span class="tooltip">Download as PDF</span>
                                </a>
                            <?php } else { ?>
                                <button type="button" id="save__pdf" class="inprogress">
                                    <svg class="downloadPDF" width="15" height="15" viewBox="0 0 15 15" fill="none"
                                         xmlns="http://www.w3.org/2000/svg">
                                        <path d="M12.1576 9.55936V12.9859H1.95998V9.55936H0V13.9931C0 14.549 0.438148 14.9998 0.981451 14.9998H13.1362C13.679 14.9998 14.1176 14.5495 14.1176 13.9931V9.55936H12.1576Z"
                                              fill="#BBBBBB"/>
                                        <path d="M6.87916 9.21973L4.07307 5.73469C4.07307 5.73469 3.64612 5.32038 4.1091 5.32038C4.57207 5.32038 5.69032 5.32038 5.69032 5.32038C5.69032 5.32038 5.69032 5.04168 5.69032 4.61187C5.69032 3.38647 5.69032 1.15634 5.69032 0.24718C5.69032 0.24718 5.62752 0 5.98972 0C6.35485 0 7.95408 0 8.21551 0C8.47645 0 8.47061 0.208152 8.47061 0.208152C8.47061 1.0898 8.47061 3.39698 8.47061 4.58235C8.47061 4.96663 8.47061 5.21581 8.47061 5.21581C8.47061 5.21581 9.36735 5.21581 9.93013 5.21581C10.4919 5.21581 10.0689 5.64962 10.0689 5.64962C10.0689 5.64962 7.68146 8.907 7.34847 9.24875C7.10895 9.49643 6.87916 9.21973 6.87916 9.21973Z"
                                              fill="#BBBBBB"/>
                                    </svg>
                                    <img src="<?= $template_directory_uri; ?>/out/img_design/loading-blue.svg"
                                         alt="arrow" title="arrow" class="loading">
                                    <span class="tooltip">Download as PDF</span>
                                </button>
                            <?php }
                        }else{ ?>
                            <a href="/plans/?pdfBanner" id="save__pdf">
                                <svg class="downloadPDF" width="15" height="15" viewBox="0 0 15 15" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M12.1576 9.55936V12.9859H1.95998V9.55936H0V13.9931C0 14.549 0.438148 14.9998 0.981451 14.9998H13.1362C13.679 14.9998 14.1176 14.5495 14.1176 13.9931V9.55936H12.1576Z" fill="#BBBBBB"/>
                                    <path d="M6.87916 9.21973L4.07307 5.73469C4.07307 5.73469 3.64612 5.32038 4.1091 5.32038C4.57207 5.32038 5.69032 5.32038 5.69032 5.32038C5.69032 5.32038 5.69032 5.04168 5.69032 4.61187C5.69032 3.38647 5.69032 1.15634 5.69032 0.24718C5.69032 0.24718 5.62752 0 5.98972 0C6.35485 0 7.95408 0 8.21551 0C8.47645 0 8.47061 0.208152 8.47061 0.208152C8.47061 1.0898 8.47061 3.39698 8.47061 4.58235C8.47061 4.96663 8.47061 5.21581 8.47061 5.21581C8.47061 5.21581 9.36735 5.21581 9.93013 5.21581C10.4919 5.21581 10.0689 5.64962 10.0689 5.64962C10.0689 5.64962 7.68146 8.907 7.34847 9.24875C7.10895 9.49643 6.87916 9.21973 6.87916 9.21973Z" fill="#BBBBBB"/>
                                </svg><img src="<?= $template_directory_uri; ?>/out/img_design/loading-blue.svg" alt="arrow" title="arrow" class="loading">
                                <span class="tooltip">Download as PDF</span>
                            </a>
                        <?php } ?>
                    </div>
                </div>
                <?php
                if(is_user_logged_in()) {
                    $user_branding_show = get_user_meta(get_current_user_id(), 'user_pdf_branding_show', true);
                    $user_branding_url = get_user_meta(get_current_user_id(), 'user_pdf_branding', true);
                    $current_user = wp_get_current_user();
                    $customerMembershipStatus = $current_user->membership_level->name;
                    if ($user_branding_show !== '1' && $user_branding_url && $customerMembershipStatus !== 'Free') {
                        echo '<span id="user_branding_url">' . $user_branding_url . '</span>';
                    }
                }
                ?>
                <div class="overview__block overview__block-report">
                    <div class="overview__block-data">
                        <div class="title"><span>Page Score</span>
                            <button type="button" class="question">
                                <svg width="22" height="22" viewBox="0 0 22 22" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M11 17.3379C11.5933 17.3379 12.0742 16.8569 12.0742 16.2637C12.0742 15.6704 11.5933 15.1895 11 15.1895C10.4067 15.1895 9.92578 15.6704 9.92578 16.2637C9.92578 16.8569 10.4067 17.3379 11 17.3379Z" fill="#D8D8D8"/>
                                    <path d="M11 0C4.92061 0 0 4.91979 0 11C0 17.0794 4.91979 22 11 22C17.0794 22 22 17.0802 22 11C22 4.92061 17.0802 0 11 0ZM11 20.2812C5.87052 20.2812 1.71875 16.1302 1.71875 11C1.71875 5.87052 5.86983 1.71875 11 1.71875C16.1295 1.71875 20.2812 5.86983 20.2812 11C20.2812 16.1295 16.1302 20.2812 11 20.2812Z" fill="#D8D8D8"/>
                                    <path d="M11 5.52148C9.10456 5.52148 7.5625 7.06355 7.5625 8.95898C7.5625 9.43362 7.94724 9.81836 8.42188 9.81836C8.89651 9.81836 9.28125 9.43362 9.28125 8.95898C9.28125 8.01127 10.0523 7.24023 11 7.24023C11.9477 7.24023 12.7188 8.01127 12.7188 8.95898C12.7188 9.9067 11.9477 10.6777 11 10.6777C10.5254 10.6777 10.1406 11.0625 10.1406 11.5371V13.6855C10.1406 14.1602 10.5254 14.5449 11 14.5449C11.4746 14.5449 11.8594 14.1602 11.8594 13.6855V12.2877C13.3403 11.9052 14.4375 10.5578 14.4375 8.95898C14.4375 7.06355 12.8954 5.52148 11 5.52148Z" fill="#D8D8D8"/>
                                </svg>
                                <span class="tooltip">Page Score is our internal rate based on combination of valuation of critical errors, warnings, notices and Google Page Speed results.</span>
                            </button>
                        </div>
                        <div class="value">
                            <div class="score__all-animation"></div>
                            <span id="score__all">0</span>
                            <span class="score"><i>/</i>100</span>
                        </div>
                    </div>
                    <div class="overview__block-data">
                        <div class="title"><span>Critical Errors</span>
                            <button type="button" class="question">
                                <svg width="22" height="22" viewBox="0 0 22 22" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M11 17.3379C11.5933 17.3379 12.0742 16.8569 12.0742 16.2637C12.0742 15.6704 11.5933 15.1895 11 15.1895C10.4067 15.1895 9.92578 15.6704 9.92578 16.2637C9.92578 16.8569 10.4067 17.3379 11 17.3379Z" fill="#D8D8D8"/>
                                    <path d="M11 0C4.92061 0 0 4.91979 0 11C0 17.0794 4.91979 22 11 22C17.0794 22 22 17.0802 22 11C22 4.92061 17.0802 0 11 0ZM11 20.2812C5.87052 20.2812 1.71875 16.1302 1.71875 11C1.71875 5.87052 5.86983 1.71875 11 1.71875C16.1295 1.71875 20.2812 5.86983 20.2812 11C20.2812 16.1295 16.1302 20.2812 11 20.2812Z" fill="#D8D8D8"/>
                                    <path d="M11 5.52148C9.10456 5.52148 7.5625 7.06355 7.5625 8.95898C7.5625 9.43362 7.94724 9.81836 8.42188 9.81836C8.89651 9.81836 9.28125 9.43362 9.28125 8.95898C9.28125 8.01127 10.0523 7.24023 11 7.24023C11.9477 7.24023 12.7188 8.01127 12.7188 8.95898C12.7188 9.9067 11.9477 10.6777 11 10.6777C10.5254 10.6777 10.1406 11.0625 10.1406 11.5371V13.6855C10.1406 14.1602 10.5254 14.5449 11 14.5449C11.4746 14.5449 11.8594 14.1602 11.8594 13.6855V12.2877C13.3403 11.9052 14.4375 10.5578 14.4375 8.95898C14.4375 7.06355 12.8954 5.52148 11 5.52148Z" fill="#D8D8D8"/>
                                </svg>
                                <span class="tooltip">The most critical errors on a page that need to be fixed.</span>
                            </button>
                        </div>
                        <div class="value">
                            <div class="score__all-animation"></div>
                            <span class="errors" id="score__errors">0</span>
                            <span class="score"><i>/</i>60</span>
                        </div>
                    </div>
                    <div class="overview__block-data">
                        <div class="title"><span>Warnings</span>
                            <button type="button" class="question">
                                <svg width="22" height="22" viewBox="0 0 22 22" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M11 17.3379C11.5933 17.3379 12.0742 16.8569 12.0742 16.2637C12.0742 15.6704 11.5933 15.1895 11 15.1895C10.4067 15.1895 9.92578 15.6704 9.92578 16.2637C9.92578 16.8569 10.4067 17.3379 11 17.3379Z" fill="#D8D8D8"/>
                                    <path d="M11 0C4.92061 0 0 4.91979 0 11C0 17.0794 4.91979 22 11 22C17.0794 22 22 17.0802 22 11C22 4.92061 17.0802 0 11 0ZM11 20.2812C5.87052 20.2812 1.71875 16.1302 1.71875 11C1.71875 5.87052 5.86983 1.71875 11 1.71875C16.1295 1.71875 20.2812 5.86983 20.2812 11C20.2812 16.1295 16.1302 20.2812 11 20.2812Z" fill="#D8D8D8"/>
                                    <path d="M11 5.52148C9.10456 5.52148 7.5625 7.06355 7.5625 8.95898C7.5625 9.43362 7.94724 9.81836 8.42188 9.81836C8.89651 9.81836 9.28125 9.43362 9.28125 8.95898C9.28125 8.01127 10.0523 7.24023 11 7.24023C11.9477 7.24023 12.7188 8.01127 12.7188 8.95898C12.7188 9.9067 11.9477 10.6777 11 10.6777C10.5254 10.6777 10.1406 11.0625 10.1406 11.5371V13.6855C10.1406 14.1602 10.5254 14.5449 11 14.5449C11.4746 14.5449 11.8594 14.1602 11.8594 13.6855V12.2877C13.3403 11.9052 14.4375 10.5578 14.4375 8.95898C14.4375 7.06355 12.8954 5.52148 11 5.52148Z" fill="#D8D8D8"/>
                                </svg>
                                <span class="tooltip">The number of issues of medium severity detected on your page during the last audit.</span>
                            </button>
                        </div>
                        <div class="value">
                            <div class="score__all-animation"></div>
                            <span class="warning" id="score__warnings">0</span>
                            <span class="score"><i>/</i>60</span>
                        </div>
                    </div>
                    <div class="overview__block-data">
                        <p class="cprogress-status">Status code <span></span></p>
                        <p class="cprogress-index">Page is <span></span></p>
                        <p class="cprogress-size">Page size is <span></span></p>
                    </div>
                </div>
                <div class="report__results-container">
                    <div class="report__response">
                        <img src="/wp-content/themes/sitechecker/out/img_design/loading-blue.svg" alt="arrow" title="arrow" class="loading">
                        <div class="report__response-block errors responses--error">
                            <p class="title">Critical Errors</p>
                            <div class="responses--content"></div>
                        </div>
                        <div class="report__response-block warnings responses--warning">
                            <p class="title">Warnings</p>
                            <div class="responses--content"></div>
                        </div>
                    </div>
                    <span id="35d2c960-b996-089b-0f47-1eb90e35224a"></span>
                    <script type="application/javascript">
                        function checkData(data){
                            if (data === ''){
                                return 'false';
                            } else{
                                return data;
                            }
                        }
                        var d = document,
                            s=d.createElement('script'),
                            SEOURL = d.getElementById('sitechecker_input__value').value,
                            SEOURL = SEOURL.replace(/(^\w+:|^)\/\//, ''),
                            SEOURL = SEOURL.replace(/\//g, ''),
                            userLanguage = d.documentElement.lang,
                            userId = checkData(USERID),
                            userEmail = checkData(USEREMAIL),
                            userLifetimeOrders = checkData(USERLIFETIMEORDERS),
                            userLifetimeValue = checkData(USERLIFETIMEVALUE),
                            userCrawlerDomains = checkData(USERCRAWLERDOMAINS),
                            userMembeshipStatus = checkData(USERMEMBERSHIPSTATUS),
                            userAvaliableURL = checkData(USERAVALIABLEURL),
                            userAvaliableDomains = checkData(USERAVALIABLEDOMAINS);
                        console.log('https://t.sitechecker.pro/zJH7xk?se_referrer=' + encodeURIComponent(document.referrer) + '&default_keyword=' + encodeURIComponent(document.title) + '&'+window.location.search.replace('?', '&')+'&frm=script&_cid=35d2c960-b996-089b-0f47-1eb90e35224a&extra_param_1='+SEOURL+'&extra_param_2='+userMembeshipStatus+'&extra_param_3='+userId+'&extra_param_4='+userEmail+'&extra_param_5='+userLifetimeOrders+'&extra_param_6='+userLifetimeValue+'&extra_param_7='+userCrawlerDomains+'&extra_param_8='+userLanguage+'&extra_param_9='+userAvaliableURL+'&extra_param_10='+userAvaliableDomains);
                        s.src='https://t.sitechecker.pro/zJH7xk?se_referrer=' + encodeURIComponent(document.referrer) + '&default_keyword=' + encodeURIComponent(document.title) + '&'+window.location.search.replace('?', '&')+'&frm=script&_cid=35d2c960-b996-089b-0f47-1eb90e35224a&extra_param_1='+SEOURL+'&extra_param_2='+userMembeshipStatus+'&extra_param_3='+userId+'&extra_param_4='+userEmail+'&extra_param_5='+userLifetimeOrders+'&extra_param_6='+userLifetimeValue+'&extra_param_7='+userCrawlerDomains+'&extra_param_8='+userLanguage+'&extra_param_9='+userAvaliableURL+'&extra_param_10='+userAvaliableDomains;
                        d.getElementById("35d2c960-b996-089b-0f47-1eb90e35224a").appendChild(s);
                    </script>
                    <div class="report__validations"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="audit__banner">
        <span id="2cdaa279-069a-e75d-d4c3-f5679dbfaa18"></span>
        <script type="application/javascript">
            function checkData(data){
                if (data === ''){
                    return 'false';
                } else{
                    return data;
                }
            }
            var d = document,
                s=d.createElement('script'),
                SEOURL = d.getElementById('sitechecker_input__value').value,
                SEOURL = SEOURL.replace(/(^\w+:|^)\/\//, ''),
                SEOURL = SEOURL.replace(/\//g, ''),
                userLanguage = d.documentElement.lang,
                userId = checkData(USERID),
                userEmail = checkData(USEREMAIL),
                userLifetimeOrders = checkData(USERLIFETIMEORDERS),
                userLifetimeValue = checkData(USERLIFETIMEVALUE),
                userCrawlerDomains = checkData(USERCRAWLERDOMAINS),
                userMembeshipStatus = checkData(USERMEMBERSHIPSTATUS),
                userAvaliableURL = checkData(USERAVALIABLEURL),
                userAvaliableDomains = checkData(USERAVALIABLEDOMAINS);
            s.src='https://t.sitechecker.pro/QJqr9Q?se_referrer=' + encodeURIComponent(document.referrer) + '&default_keyword=' + encodeURIComponent(document.title) + '&'+window.location.search.replace('?', '&')+'&frm=script&_cid=2cdaa279-069a-e75d-d4c3-f5679dbfaa18&extra_param_1='+SEOURL+'&extra_param_2='+userMembeshipStatus+'&extra_param_3='+userId+'&extra_param_4='+userEmail+'&extra_param_5='+userLifetimeOrders+'&extra_param_6='+userLifetimeValue+'&extra_param_7='+userCrawlerDomains+'&extra_param_8='+userLanguage+'&extra_param_9='+userAvaliableURL+'&extra_param_10='+userAvaliableDomains;
            d.getElementById("2cdaa279-069a-e75d-d4c3-f5679dbfaa18").appendChild(s);
        </script>
    </div>
</main>