<div class="blog_container-sidebar">
	<div class="sidebar_block">
		<p class="sidebar_block-title">
			Latest posts
		</p>
		<?php
              global $post;
              $tmp_post = $post;
              $args = array(
                'posts_per_page' => 3
              );
              $myposts = get_posts($args);

              foreach ($myposts as $post) {
                        setup_postdata($post);
                        ?>
                            <a href="<?php the_permalink(); ?>" class="sidebar_block-post">
								<div style="background-image: url(<?php echo the_post_thumbnail_url(); ?>)"></div>
								<div class="sidebar_block-data">
									<p><?php the_title(); ?></p>
									<span><?php the_date(); ?></span>
								</div>
							</a>
                        <?php
                    }
              $post = $tmp_post;
        ?>
	</div>
	<div class="sidebar_block">
		<p class="sidebar_block-title">
			Tags
		</p>
		<div class="sidebar_tags">
			<?php 
				$tags = get_tags();
				foreach ($tags as $tag){
					$tag_link = get_tag_link($tag->term_id);

					$html .= "<a href='{$tag_link}' title='{$tag->name} Tag'>";
					$html .= "{$tag->name}</a>";
				}
				echo $html;
			 ?>
		</div>
	</div>
</div>