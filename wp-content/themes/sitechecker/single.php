<?php get_header(); ?>

<?php if (have_posts()): while (have_posts()) : the_post(); ?>
<main>
	<div class="wrapper">
		<div class="breadcrumbs">
			<a href="/">Home</a>
			<a><?php the_title(); ?></a>
		</div>
		<div class="blog_container">
			<div class="blog_container-posts">
				<div class="blog_post">
					<a class="blog_post-img" style="background-image: url(<?php echo the_post_thumbnail_url(); ?>)"></a>
					<a class="blog_post-title single_post"><?php the_title(); ?></a>
					<p class="blog_post-data"><?php the_date(); ?></p>
					<div class="blog_post-contant single_post">
						<?php the_content(); ?>
					</div>
					<div class="blog_bottom-tags">
						<?php $tags = wp_get_post_tags($post->ID);
				          	foreach ($tags as $tag) {
				              	$tag_link = get_tag_link( $tag->term_id );
					            echo '<a href="'.$tag_link.'">' . $tag->name . '</a>';
				          	}
				      	?>
					</div>
				</div>
			</div>
			<?php get_sidebar(); ?>
		</div>
        <div id="comments">
            <?php if ( comments_open() || get_comments_number() ) :
                comments_template();
            endif; ?>
        </div>
	</div>
</main>

<?php endwhile; ?>
<?php endif; ?>

<?php get_footer(); ?>