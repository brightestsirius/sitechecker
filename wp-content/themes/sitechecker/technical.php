<?php
/*
	Template Name: Technical Page
*/
?>
<?php get_header(); ?>

    <script>
        function sent_Popup(){
            if($('.wp_crm_error_messages').text() == ''){
                $('.popup_sent').css('display', 'flex');
                $('.popup_sent-block').fadeIn().css('display', 'flex');
                disableScroll();

                var closing_popup = function(){
                    $('.popup_sent').fadeOut();
                    $('.popup_sent-block').css('display','none');
                    enableScroll();
                };

                setTimeout(closing_popup, 3000);

                $('.popup_sent-close').click(function(){
                    $('.popup_sent').fadeOut();
                    $('.popup_sent-block').css('display','none');
                    enableScroll();
                })
            };
        };
    </script>

<?php
$meta = get_post_meta_all($post->ID);
$front = get_post_meta_all(get_option('page_on_front'));
$template_directory_uri = get_template_directory_uri();
?>

    <div class="paypal_redirect">
        <p class="title">You are logged in now</p>
        <p>We are redirecting you to Websites Dashboard</p>
        <div class="spinWrap">
            <p class="spinnerImage"></p>
            <p class="loader"></p>
        </div>
    </div>
    <main class="technical">
        <div class="technical_first technical_second">
            <div class="wrapper">
                <h1><?=t($meta['technical_seo_title'])?></h1>
                <div class="technical_first-about <?php echo get_locale(); ?>">
                    <p><img src="<?=$template_directory_uri;?>/out/img/technical_check.svg" alt="technical_check" title="technical_check"><?=t($meta['landing.header.text.1']);?></p>
                    <p><img src="<?=$template_directory_uri;?>/out/img/technical_check.svg" alt="technical_check" title="technical_check"><?=t($meta['landing.header.text.2']);?></p>
                    <p><img src="<?=$template_directory_uri;?>/out/img/technical_check.svg" alt="technical_check" title="technical_check"><?=t($meta['landing.header.text.3']);?></p>
                </div>
                <?php if(is_page($front_second)){
                    if(is_user_logged_in()){
                        $page = 'sitechecker_crawl';
                    } else{
                        $page = 'sitechecker_reg';
                    }
                } else{$page = 'sitechecker_check-landing';} ?>
                <div class="home_first-search">
                    <input id="sitechecker_input" type="text" placeholder="<?=t($front['main.input']);?>" class="<?php echo $page; ?>">
                    <button id="<?php echo $page; ?>"><span><?=t($front['main.button']);?></span><i class="fa fa-spinner fa-spin"></i></button>
                </div>
                <div class="wrapper home_first-down">
                    <a class="scrollto" name=".technical_connect"><?=t($meta['landing.dropdown.text']);?> <svg xmlns="http://www.w3.org/2000/svg" width="10" height="7" viewBox="0 0 10 7">
                            <g fill="#495C6A" fill-rule="evenodd">
                                <path d="M5.104 7l1.088-1.142L1.296.718.21 1.86z"/>
                                <path d="M10 1.857L8.912.715l-4.895 5.14 1.088 1.143z"/>
                            </g>
                        </svg></a>
                </div>
            </div>
        </div>
        <div class="technical_connect">
            <div class="wrapper">
                <div class="technical_connect-info">
                    <p><?=t($meta['landing.diagram.title']);?></p>
                    <p><?=t($meta['landing.diagram.text']);?></p>
                </div>
                <div class="technical_connect-block">
                    <div class="connect_bg">
                        <div class="connect_bg-circle">
                            <img src="<?=$template_directory_uri;?>/out/img/connection-box.svg" alt="connection-box" title="connection-box">
                        </div>
                        <div class="connect_bg-data">
                            <span><?=t($meta['landing.diagram.block1']);?></span>
                        </div>
                        <div class="connect_bg-data">
                            <span><?=t($meta['landing.diagram.block2']);?></span>
                        </div>
                        <div class="connect_bg-data">
                            <span><?=t($meta['landing.diagram.block3']);?></span>
                        </div>
                        <div class="connect_bg-data">
                            <span><?=t($meta['landing.diagram.block4']);?></span>
                        </div>
                        <div class="connect_bg-data">
                            <span><?=t($meta['landing.diagram.block5']);?></span>
                        </div>
                        <div class="connect_bg-data">
                            <span><?=t($meta['landing.diagram.block6']);?></span>
                        </div>
                        <div class="connect_bg-data">
                            <span><?=t($meta['landing.diagram.block7']);?></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="landing_about">
            <div class="wrapper">
                <h2><?=t($meta['landing.about.title']);?></h2>
                <div class="landing_about-block">
                    <div class="landing_block-img">
                        <img src="<?=$template_directory_uri;?>/out/img/landing_about_1.svg" title="landing_about" alt="landing_about">
                    </div>
                    <div class="landing_block-text">
                        <h3><?=t($meta['landing.about.header.1']);?></h3>
                        <p><?=t($meta['landing.about.content.1']);?></p>
                    </div>
                </div>
                <div class="landing_about-block">
                    <div class="landing_block-text">
                        <h3><?=t($meta['landing.about.header.2']);?></h3>
                        <p><?=t($meta['landing.about.content.2']);?></p>
                    </div>
                    <div class="landing_block-img">
                        <img src="<?=$template_directory_uri;?>/out/img/landing_about_2.svg" title="landing_about" alt="landing_about">
                    </div>
                </div>
            </div>
        </div>
        <div class="technical_features">
            <div class="wrapper">
                <h2><?=t($meta['landing.icons.title']);?></h2>
                <div class="technical_features-block">
                    <div class="technical-feat">
                        <p><?=t($meta['landing.icon1']);?></p>
                        <img src="<?=$template_directory_uri;?>/out/img/technical_feature1.svg" alt="technical_feature" title="technical_feature">
                    </div>
                    <div class="technical-feat">
                        <p><?=t($meta['landing.icon2']);?></p>
                        <img src="<?=$template_directory_uri;?>/out/img/technical_feature2.svg" alt="technical_feature" title="technical_feature">
                    </div>
                    <div class="technical-feat">
                        <p><?=t($meta['landing.icon3']);?></p>
                        <img src="<?=$template_directory_uri;?>/out/img/technical_feature3.svg" alt="technical_feature" title="technical_feature">
                    </div>
                    <div class="technical-feat">
                        <p><?=t($meta['landing.icon4']);?></p>
                        <img src="<?=$template_directory_uri;?>/out/img/technical_feature4.svg" alt="technical_feature" title="technical_feature">
                    </div>
                </div>
            </div>
        </div>
    </main>

    <div class="popup_order">
        <div class="landing_form-form">
            <div class="form_header">
                <p><?=t($meta['order.form.title']);?></p>
                <img src="<?=$template_directory_uri;?>/out/img/close.svg" alt="close" title="close" class="popup_order-close">
            </div>
            <?php if (have_posts()): while (have_posts()) : the_post(); ?>
                <?php the_content(); ?>
            <?php endwhile; ?>
            <?php else: ?>
            <?php endif; ?>
        </div>
    </div>

    <div class="popup_sent">
        <div class="popup_sent-block">
            <img src="<?=$template_directory_uri;?>/out/img/img-close.svg" alt="close" title="close" class="popup_sent-close">
            <img src="<?=$template_directory_uri;?>/out/img/letter.svg" alt="sent-mail" title="sent-mail" class="popup_sent-img">
            <p><?=t($meta['sent.form']);?></p>
        </div>
    </div>

<?php get_footer(); ?>